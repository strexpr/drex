package exprs

import org.scalatest._
import theory.StdCharPred
import theory.CharPred
import theory.CharOffset

class DReXConsistencySuite extends FunSuite {

    val cpTrue = StdCharPred.TRUE
    val lower = new CharPred('a', 'z')
    val digit = new CharPred('0', '9')
    val ab = new CharPred('a', 'b')

    val symAllToA = Symbol(cpTrue, "a")
    val symLowerToA = Symbol(lower, "a")
    val symABToA = Symbol(ab, "a")
    val symDigitToA = Symbol(digit, "a")

    val allToA = Iter(symAllToA)
    val lowerToA = Iter(symLowerToA)
    val numToA = Iter(symDigitToA)
    val epsilonToA = Epsilon("a")


    test("Symbol") {
        assert(Symbol(StdCharPred.TRUE, "a").consistent)
    }

    test("Iter") {
        assert(allToA.consistent)
        assert(!Iter(epsilonToA).consistent)
    }

    test("Combine") {
        assert(!Combine(allToA, lowerToA).consistent)
        assert(Combine(lowerToA, lowerToA).consistent)
    }

    test("Split") {
        assert(!Split(lowerToA, lowerToA).consistent)
        assert(Split(lowerToA, epsilonToA).consistent)
        assert(Split(lowerToA, numToA).consistent)
    }

    test("ChainedSum") {
        val split1 = Split(symLowerToA, symLowerToA);
        val split2 = Split(symLowerToA, symABToA);

        assert(ChainedSum(split1, SymbolR(lower)).consistent);
        assert(!ChainedSum(split2, SymbolR(lower)).consistent);
        assert(!ChainedSum(lowerToA, Star(SymbolR(lower))).consistent)

        val (digitEps, digitId) = (Symbol(digit, ""), Symbol(digit, CharOffset.IDENTITY))
        val (lowerEps, lowerId) = (Symbol(lower, ""), Symbol(lower, CharOffset.IDENTITY))
        val echoR = Split(Split(Iter(digitEps), lowerEps), Split(Iter(digitId), lowerId))
        val echoL = Split(Split(Iter(digitEps), lowerId), Split(Iter(digitId), lowerEps))
        val dStarL = Concat(Star(SymbolR(digit)), SymbolR(lower))
        val subExpr = Combine(echoR, echoL)
        val shuffle = ChainedSum(subExpr, dStarL)
        assert(shuffle.consistent);
    }

    test("Restrict") {
        val res1 = Restrict(allToA, Concat(Star(SymbolR(lower)), SymbolR(digit)))
        val res2 = Restrict(allToA, SymbolR(cpTrue))

        assert(res1.consistent)
        assert(Split(res1, res1).consistent)
        assert(Split(res2, res2).consistent)
        assert(Split(res2, allToA).consistent)
        assert(!Split(allToA, allToA).consistent)
    }
}
