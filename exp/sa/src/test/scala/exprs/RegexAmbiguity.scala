package exprs

import org.scalatest._
import theory.StdCharPred

class RegexAmbiguitySuite extends FunSuite {

    val empty = Empty
    val epsilon = EpsilonR
    val alpha = SymbolR(StdCharPred.ALPHA)
    val sigma = SymbolR(StdCharPred.TRUE)
    val digit = SymbolR(StdCharPred.NUM)
    val emptySymbol = SymbolR(StdCharPred.FALSE)

    test("Empty regex is always unambiguous") {
        assert(empty.unambiguous)
    }

    test("Epsilon regex is always unambiguous") {
        assert(epsilon.unambiguous)
    }

    test("Symbol regex is always unambiguous") {
        assert(alpha.unambiguous)
        assert(sigma.unambiguous)
        assert(emptySymbol.unambiguous)
    }

    test("Concat regex is unambiguous if every string is at most uniquely decomposable") {
        val aa = Concat(alpha, alpha)
        val an = Concat(alpha, digit)
        val as = Star(alpha)
        val asas = Concat(as, as)
        val af = Concat(alpha, empty)

        assert(aa.unambiguous)
        assert(an.unambiguous)
        assert(as.unambiguous)
        assert(!asas.unambiguous)
        assert(af.unambiguous)
    }

    test("Star regex is unambiguous if every string is at most uniquely decomposable") {
        val as = Star(alpha)
        val ass = Star(as)
        val emptyS = Star(Empty)
        val epsilonS = Star(EpsilonR)

        assert(as.unambiguous)
        assert(!ass.unambiguous)
        assert(emptyS.unambiguous)
        assert(!epsilonS.unambiguous)
    }

    test("Star regex is unambiguous if every string is matched by at most one of the arguments") {
        val an = Union(alpha, digit)
        val anan = Union(an, an)

        assert(an.unambiguous)
        assert(!anan.unambiguous)
    }

    test("regression-1") {
        val epsilon = EpsilonR
        val starEpsilon = Star(epsilon)
        val concat1 = Concat(epsilon, starEpsilon)
        val r = Concat(epsilon, concat1)

        assert(!starEpsilon.unambiguous)
        assert(!concat1.unambiguous)
        assert(!r.unambiguous)
    }
}
