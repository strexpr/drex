package synth

import exprs._
import evaluators.dp._
import org.scalatest._

import theory.CharPred
import theory.CharOffset
import theory.CharSolver
import theory.StdCharPred

class BlindEnumeratorSuite extends FunSuite {

  val solver = new CharSolver()
  val lower = new CharPred('a', 'z')
  val notLower = solver.MkNot(lower)
  val charPreds = Set(lower, notLower)

  val toUpper = new CharOffset('A' - 'a')
  val id = new CharOffset(0)
  val charFuncs = Set(toUpper, id)

  val strings = Set("", "ABC") 

  val baseExprs : Set[Expression] = Set(Bottom)



  test("Test blind enumeration") {
    val grammar = Grammar.standardGrammar(strings, baseExprs, charPreds, charFuncs)
    val blindEnumerator = new BlindEnumerator(grammar)
    blindEnumerator.enumerate()
    blindEnumerator.enumerateBySize().take(3)
  }

  test("Test blind synthesis: Size 2") {
    val grammar = Grammar.standardGrammar(strings, baseExprs, charPreds, charFuncs)
    val blindEnumerator = new BlindEnumerator(grammar)

    val ioExamples = List(IO("abc", "ABC")).map{ AtomicExample(_) }
    val exampleStructure = And(ioExamples)
    val res = blindEnumerator.synthesizeFor(exampleStructure)

    // println(res)
    assert(res.consistent)
    assert(DReXDPEvaluator.evaluate("abc", res) == Some("ABC"))
  }

  test("Test blind synthesis: Size 4") {
    val grammar = Grammar.standardGrammar(strings, baseExprs, charPreds, charFuncs)
    val blindEnumerator = new BlindEnumerator(grammar)

    val ioExamples = List(IO("abc", "ABC"), IO("123abc", "123ABC")).map{ AtomicExample(_) }
    val exampleStructure = And(ioExamples)
    val res = blindEnumerator.synthesizeFor(exampleStructure)

    // println(res)
    assert(res.consistent)
    assert(DReXDPEvaluator.evaluate("abc", res) == Some("ABC"))
    assert(DReXDPEvaluator.evaluate("123abc", res) == Some("123ABC"))
  }

  test("Test blind synthesis: Size 5") {
    val grammar = Grammar.standardGrammar(strings, baseExprs, charPreds, charFuncs)
    val blindEnumerator = new BlindEnumerator(grammar)

    val ioExamples = List(IO("abc", "ABC"), IO("123abc", "123ABC")).map { AtomicExample(_) }
    val definedExamples = List(
      Not(AtomicExample(Defined("abc123"))),
      Not(AtomicExample(Defined("1a2b")))
    )
    val examples = ioExamples ++ definedExamples
    val exampleStructure = And(examples)
    val res = blindEnumerator.synthesizeFor(exampleStructure)

    // println(res)
    assert(res.consistent)
    assert(DReXDPEvaluator.evaluate("abc", res) == Some("ABC"))
    assert(DReXDPEvaluator.evaluate("123abc", res) == Some("123ABC"))
    assert(DReXDPEvaluator.evaluate("abc123", res).isEmpty)
    assert(DReXDPEvaluator.evaluate("1a2b", res).isEmpty)
  }
}
