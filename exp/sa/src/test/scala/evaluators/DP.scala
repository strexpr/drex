package evaluators.dp

import exprs._
import org.scalatest._
import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

import theory.CharPred
import theory.CharOffset
import theory.StdCharPred

class RegexSuite extends FunSuite {

    val aPred = new CharPred('a')
    val bPred = new CharPred('b')
    val digitPred = new CharPred('0', '9')
    val lowerPred = new CharPred('a', 'z')

    val empty = Empty
    val epsilon = EpsilonR
    val lowercase = SymbolR(lowerPred)
    val digit = SymbolR(digitPred)
    val ld = Union(lowercase, digit)
    val dld = Concat(digit, ld)
    val digitS = Star(digit)
    val sigma = SymbolR(StdCharPred.TRUE)
    val sigmaS = Star(sigma)
    val a = SymbolR(aPred)
    val b = SymbolR(bPred)
    val d_and_sigma = Intersection(digit, sigma)

    test("Empty regex should not match anything") {
        assert(!RegexDPEvaluator.evaluate("", empty))
        assert(!RegexDPEvaluator.evaluate("abc", empty))
    }

    test("Epsilon regex should match only empty string") {
        assert(RegexDPEvaluator.evaluate("", epsilon))
        assert(!RegexDPEvaluator.evaluate("a", epsilon))
        assert(!RegexDPEvaluator.evaluate("ab", epsilon))
    }

    test("Symbol regex should only match the appropriate single character") {
        assert(!RegexDPEvaluator.evaluate("", lowercase))
        assert(RegexDPEvaluator.evaluate("a", lowercase))
        assert(RegexDPEvaluator.evaluate("z", lowercase))
        assert(!RegexDPEvaluator.evaluate("`", lowercase))
        assert(!RegexDPEvaluator.evaluate("{", lowercase))
        assert(!RegexDPEvaluator.evaluate("A", lowercase))
        assert(!RegexDPEvaluator.evaluate("ab", lowercase))

        assert(RegexDPEvaluator.evaluate("9", digit))
        assert(RegexDPEvaluator.evaluate("0", digit))
        assert(!RegexDPEvaluator.evaluate("09", digit))
        assert(!RegexDPEvaluator.evaluate("", digit))

        assert(RegexDPEvaluator.evaluate("0", sigma))
        assert(RegexDPEvaluator.evaluate("a", sigma))
        assert(RegexDPEvaluator.evaluate("{", sigma))
        assert(!RegexDPEvaluator.evaluate("", sigma))
        assert(!RegexDPEvaluator.evaluate("abc", sigma))

        assert(!RegexDPEvaluator.evaluate("", a))
        assert(RegexDPEvaluator.evaluate("a", a))
        assert(!RegexDPEvaluator.evaluate("aa", a))
        assert(!RegexDPEvaluator.evaluate("b", a))
        assert(!RegexDPEvaluator.evaluate("ab", a))
        assert(!RegexDPEvaluator.evaluate("ba", a))
        assert(!RegexDPEvaluator.evaluate("bb", a))

        assert(!RegexDPEvaluator.evaluate("", b))
        assert(!RegexDPEvaluator.evaluate("a", b))
        assert(!RegexDPEvaluator.evaluate("aa", b))
        assert(RegexDPEvaluator.evaluate("b", b))
        assert(!RegexDPEvaluator.evaluate("ab", b))
        assert(!RegexDPEvaluator.evaluate("ba", b))
        assert(!RegexDPEvaluator.evaluate("bb", b))
    }

    test("Union regex should only match strings that match either of the arguments") {
        assert(RegexDPEvaluator.evaluate("a", ld))
        assert(RegexDPEvaluator.evaluate("6", ld))
        assert(!RegexDPEvaluator.evaluate("A", ld))
        assert(!RegexDPEvaluator.evaluate("05", ld))

        val a_or_b = new Union(a, b)
        assert(!RegexDPEvaluator.evaluate("", a_or_b))
        assert(RegexDPEvaluator.evaluate("b", a_or_b))
        assert(!RegexDPEvaluator.evaluate("aa", a_or_b))
        assert(RegexDPEvaluator.evaluate("a", a_or_b))
        assert(!RegexDPEvaluator.evaluate("ab", a_or_b))
        assert(!RegexDPEvaluator.evaluate("ba", a_or_b))
        assert(!RegexDPEvaluator.evaluate("bb", a_or_b))
    }

    test("Intersection regex should only match strings that match both of the arguments") {
        assert(!RegexDPEvaluator.evaluate("a", d_and_sigma))
        assert(RegexDPEvaluator.evaluate("0", d_and_sigma))
        assert(!RegexDPEvaluator.evaluate("aa", d_and_sigma))
    }

    test("Concat regex should only match strings that are concatenation of strings matching individual expressions") {
        assert(RegexDPEvaluator.evaluate("00", dld))
        assert(RegexDPEvaluator.evaluate("0a", dld))
        assert(!RegexDPEvaluator.evaluate("aa", dld))
    }

    test("Star regex should only match arbitrary concatentation of strings matching the base") {
        assert(RegexDPEvaluator.evaluate("9", digitS))
        assert(RegexDPEvaluator.evaluate("0", digitS))
        assert(RegexDPEvaluator.evaluate("09", digitS))
        assert(!RegexDPEvaluator.evaluate("a", digitS))
        assert(!RegexDPEvaluator.evaluate("ab", digitS))
        assert(RegexDPEvaluator.evaluate("", digitS))

        assert(RegexDPEvaluator.evaluate("0", sigmaS))
        assert(RegexDPEvaluator.evaluate("09", sigmaS))
        assert(RegexDPEvaluator.evaluate("09999", sigmaS))

        val a_or_b = Union(a, b)
        val as = Star(a)
        val a_or_bs = Star(a_or_b)

        assert(RegexDPEvaluator.evaluate("", as))
        assert(RegexDPEvaluator.evaluate("a", as))
        assert(!RegexDPEvaluator.evaluate("b", as))
        assert(RegexDPEvaluator.evaluate("aa", as))
        assert(!RegexDPEvaluator.evaluate("ab", as))
        assert(!RegexDPEvaluator.evaluate("ba", as))
        assert(!RegexDPEvaluator.evaluate("bb", as))
        assert(!RegexDPEvaluator.evaluate("aaaaababa", as))
        assert(RegexDPEvaluator.evaluate("aaaaa", as))

        assert(RegexDPEvaluator.evaluate("", a_or_bs))
        assert(RegexDPEvaluator.evaluate("a", a_or_bs))
        assert(RegexDPEvaluator.evaluate("b", a_or_bs))
        assert(RegexDPEvaluator.evaluate("aa", a_or_bs))
        assert(RegexDPEvaluator.evaluate("ab", a_or_bs))
        assert(RegexDPEvaluator.evaluate("ba", a_or_bs))
        assert(RegexDPEvaluator.evaluate("bb", a_or_bs))
        assert(RegexDPEvaluator.evaluate("aaaababa", a_or_bs))
        assert(!RegexDPEvaluator.evaluate("aa01abb", a_or_bs))
    }

    test("Complement regex should negagte matches") {
        val as = Star(a)
        val asc = Complement(as)
        assert(RegexDPEvaluator.evaluate("b", asc))
        assert(RegexDPEvaluator.evaluate("ab", asc))
        assert(!RegexDPEvaluator.evaluate("", asc))
        assert(!RegexDPEvaluator.evaluate("a", asc))
        assert(!RegexDPEvaluator.evaluate("aaa", asc))

        val ascc = Complement(asc)
        assert(!RegexDPEvaluator.evaluate("b", ascc))
        assert(!RegexDPEvaluator.evaluate("ab", ascc))
        assert( RegexDPEvaluator.evaluate("", ascc))
        assert( RegexDPEvaluator.evaluate("a", ascc))
        assert( RegexDPEvaluator.evaluate("aaa", ascc))

        val ab = Concat(a, b)
        val abs = Star(ab)
        val absc = Complement(abs)
        assert(!RegexDPEvaluator.evaluate("", absc))
        assert( RegexDPEvaluator.evaluate("a", absc))
        assert(!RegexDPEvaluator.evaluate("ab", absc))
        assert( RegexDPEvaluator.evaluate("aba", absc))
        assert(!RegexDPEvaluator.evaluate("abab", absc))
        assert( RegexDPEvaluator.evaluate("ababs", absc))
        assert( RegexDPEvaluator.evaluate("ababsc", absc))
    }
}

class RegexSpec extends Properties("Regex") {

    property("Empty") = forAll { a:String => !RegexDPEvaluator.evaluate(a, Empty) }
    property("Epsilon") = forAll { a:String => RegexDPEvaluator.evaluate(a, EpsilonR) == (a.length == 0) }

    property("Symbol") = forAll { 
        (c1:Char, c2:Char, a:String) => {
            val cp = if (c1 < c2) new CharPred(c1, c2) else new CharPred(c2, c1)
            val r = SymbolR(cp)

            val lengthConstraint = !RegexDPEvaluator.evaluate(a, r) || (a.length == 1)
            val charConstraint = !RegexDPEvaluator.evaluate(a, r) || cp.isSatisfiedBy(a(0))

            lengthConstraint && charConstraint
        }
    }

}

class DReXSuite extends FunSuite {
    val digitEps = Symbol(StdCharPred.NUM, "")
    val digitId = Symbol(StdCharPred.NUM, CharOffset.IDENTITY)
    val aa = Symbol(new CharPred('a'), "a")
    val ae = Symbol(new CharPred('a'), "")
    val ab = Symbol(new CharPred('a'), "b")
    val ba = Symbol(new CharPred('b'), "a")
    val be = Symbol(new CharPred('b'), "")
    val bb = Symbol(new CharPred('b'), "b")
    val cc = Symbol(new CharPred('c'), "c")
    val nn = Symbol(new CharPred('n'), "n")
    val id = Iter(Symbol(StdCharPred.TRUE, CharOffset.IDENTITY))


    test("Bottom never produces outputs") {
        assert(DReXDPEvaluator.evaluate("", Bottom).isEmpty)
        assert(DReXDPEvaluator.evaluate("a", Bottom).isEmpty)
        assert(DReXDPEvaluator.evaluate("b", Bottom).isEmpty)
        assert(DReXDPEvaluator.evaluate("ab", Bottom).isEmpty)
        assert(DReXDPEvaluator.evaluate("aaaabbbbb", Bottom).isEmpty)
    }

    test("Epsilon only produces the appropriate output on the empty string") {
        assert(DReXDPEvaluator.evaluate("", Epsilon("abc")).get == "abc")
        assert(DReXDPEvaluator.evaluate("", Epsilon("")).get == "")
        assert(DReXDPEvaluator.evaluate("a", Epsilon("abc")).isEmpty)
        assert(DReXDPEvaluator.evaluate("b", Epsilon("abc")).isEmpty)
        assert(DReXDPEvaluator.evaluate("ab", Epsilon("abc")).isEmpty)
        assert(DReXDPEvaluator.evaluate("aaaabbbbb", Epsilon("abc")).isEmpty)
    }

    test("Symbol only produces the appropriate output on symbol") {
        assert(DReXDPEvaluator.evaluate("", ab).isEmpty)
        assert(DReXDPEvaluator.evaluate("aa", ab).isEmpty)
        assert(DReXDPEvaluator.evaluate("b", ab).isEmpty)
        assert(DReXDPEvaluator.evaluate("ab", ab).isEmpty)
        assert(DReXDPEvaluator.evaluate("ba", ab).isEmpty)
        assert(DReXDPEvaluator.evaluate("bb", ab).isEmpty)
        assert(DReXDPEvaluator.evaluate("a", ab).get == "b")

        assert(DReXDPEvaluator.evaluate("", ba).isEmpty)
        assert(DReXDPEvaluator.evaluate("aa", ba).isEmpty)
        assert(DReXDPEvaluator.evaluate("ab", ba).isEmpty)
        assert(DReXDPEvaluator.evaluate("ba", ba).isEmpty)
        assert(DReXDPEvaluator.evaluate("bb", ba).isEmpty)
        assert(DReXDPEvaluator.evaluate("a", ba).isEmpty)
        assert(DReXDPEvaluator.evaluate("b", ba).get == "a")
    }

    test("Else") {
        val abba = Else(ab, ba)

        assert(DReXDPEvaluator.evaluate("", abba).isEmpty)
        assert(DReXDPEvaluator.evaluate("aa", abba).isEmpty)
        assert(DReXDPEvaluator.evaluate("ab", abba).isEmpty)
        assert(DReXDPEvaluator.evaluate("ba", abba).isEmpty)
        assert(DReXDPEvaluator.evaluate("bb", abba).isEmpty)
        assert(DReXDPEvaluator.evaluate("a", abba).get == "b")
        assert(DReXDPEvaluator.evaluate("b", abba).get == "a")
    }

    test("Iter") {
        val abba = Else(ab, ba)
        val abs = Iter(ab)
        val abbas = Iter(abba)

        assert(DReXDPEvaluator.evaluate("", abs).get == "")
        assert(DReXDPEvaluator.evaluate("a", abs).get == "b")
        assert(DReXDPEvaluator.evaluate("aa", abs).get == "bb")
        assert(DReXDPEvaluator.evaluate("aaaaa", abs).get == "bbbbb")
        assert(DReXDPEvaluator.evaluate("b", abs).isEmpty)
        assert(DReXDPEvaluator.evaluate("ab", abs).isEmpty)
        assert(DReXDPEvaluator.evaluate("ba", abs).isEmpty)
        assert(DReXDPEvaluator.evaluate("bb", abs).isEmpty)
        assert(DReXDPEvaluator.evaluate("aaaaababa", abs).isEmpty)

        assert(DReXDPEvaluator.evaluate("", abbas).get == "")
        assert(DReXDPEvaluator.evaluate("a", abbas).get == "b")
        assert(DReXDPEvaluator.evaluate("b", abbas).get == "a")
        assert(DReXDPEvaluator.evaluate("aa", abbas).get == "bb")
        assert(DReXDPEvaluator.evaluate("ab", abbas).get == "ba")
        assert(DReXDPEvaluator.evaluate("ba", abbas).get == "ab")
        assert(DReXDPEvaluator.evaluate("bb", abbas).get == "aa")
        assert(DReXDPEvaluator.evaluate("aa", abbas).get == "bb")
        assert(DReXDPEvaluator.evaluate("aaaababa", abbas).get == "bbbbabab")
        assert(DReXDPEvaluator.evaluate("1234", abbas).isEmpty)
    }

    test("Split") {
        val abba = Else(ab, ba)
        val abbas = Iter(abba)
        val aabb = Else(aa, bb)
        val aabbs = Iter(aabb)
        val de = Symbol(new CharPred('d'), "")
        val deaabbs = Split(de, aabbs)
        val t = Split(abbas, deaabbs)

        assert(DReXDPEvaluator.evaluate(""  , t).isEmpty)
        assert(DReXDPEvaluator.evaluate("a" , t).isEmpty)
        assert(DReXDPEvaluator.evaluate("b" , t).isEmpty)
        assert(DReXDPEvaluator.evaluate("aa", t).isEmpty)
        assert(DReXDPEvaluator.evaluate("ab", t).isEmpty)
        assert(DReXDPEvaluator.evaluate("ba", t).isEmpty)
        assert(DReXDPEvaluator.evaluate("bb", t).isEmpty)

        assert(DReXDPEvaluator.evaluate("d", t).get == "")
        assert(DReXDPEvaluator.evaluate("aabbadabba", t).get == "bbaababba")
    }

    test("Combine") {
        val t = Combine(id, id)

        assert(DReXDPEvaluator.evaluate(""  , t).get ==  "")
        assert(DReXDPEvaluator.evaluate("a" , t).get ==  "aa")
        assert(DReXDPEvaluator.evaluate("b" , t).get ==  "bb")
        assert(DReXDPEvaluator.evaluate("aa", t).get ==  "aaaa")
        assert(DReXDPEvaluator.evaluate("ab", t).get ==  "abab")
        assert(DReXDPEvaluator.evaluate("ba", t).get ==  "baba")
        assert(DReXDPEvaluator.evaluate("bb", t).get ==  "bbbb")
    }

    test("ChainedSum") {
        val aes = Iter(digitEps)
        val aas = Iter(digitId)
        val aesbe = Split(aes, be)
        val aasbb = Split(aas, bb)

        val echoR = Split(aesbe, aasbb)
        val echoL = Split(aasbb, aesbe)

        val subexprList = Combine(echoR, echoL)
        val shuffle = ChainedSum(subexprList, aesbe.domain)

        assert(DReXDPEvaluator.evaluate("", shuffle).isEmpty)
        assert(DReXDPEvaluator.evaluate("1b2b3b4b5b6b7b", shuffle).get 
            == "2b1b3b2b4b3b5b4b6b5b7b6b")
        assert(DReXDPEvaluator.evaluate("1b2b3b4b5b6b7b8b9b10b11b12b13b", shuffle).get 
            == "2b1b3b2b4b3b5b4b6b5b7b6b8b7b9b8b10b9b11b10b12b11b13b12b")

        // DP evaluator just gives up on anything too large
        val longInput = "1b2b3b4b5b6b7b8b9b10b11b12b13b" * 10 
        assert(!DReXDPEvaluator.evaluate(longInput, shuffle).isEmpty)
    }

    test("Restrict") {
        val pred08 = new CharPred('0', '8')
        val pred9 = new CharPred('9')

        val s1 = Symbol(pred08, new CharOffset(1))
        val s2 = Symbol(pred9, new CharOffset(-9))
        val rot = Else(s1, s2)
        
        val caesar = Iter(rot)
        val regex08s = Star(SymbolR(pred08))
        val regex9 = SymbolR(pred9)
        val regex08s9 = Concat(regex08s, regex9)
        val caesar08s9 = Restrict(caesar, regex08s9)

        assert(DReXDPEvaluator.evaluate("06010154", caesar08s9).isEmpty)
        assert(DReXDPEvaluator.evaluate("060101549", caesar08s9).get == "171212650")

        val id08s9 = Restrict(id, regex08s9)
        assert(DReXDPEvaluator.evaluate("06010154", id08s9).isEmpty)
        assert(DReXDPEvaluator.evaluate("060101549", id08s9).get == "060101549")

        val alternate = Iter(Split(id08s9, caesar08s9))
        assert(DReXDPEvaluator.evaluate("06010154", alternate).isEmpty)
        assert(DReXDPEvaluator.evaluate("060101549", alternate).isEmpty)

        assert(DReXDPEvaluator.evaluate("060101549060101549", alternate).get
            == "060101549171212650")
        assert(DReXDPEvaluator.evaluate("060101549060101549060101549060101549", alternate).get
            == "060101549171212650060101549171212650")
    }


    test("drex-core-regression-1") {
        val aStar = Iter(aa)
        val test = Split(aa, Split(aStar, nn))
        val iterTest = Iter(test)
        assert(DReXDPEvaluator.evaluate("anan", iterTest).get == "anan")
    }

    val ar = SymbolR(new CharPred('a'))
    val br = SymbolR(new CharPred('b'))
    val cr = SymbolR(new CharPred('c'))
    val nr = SymbolR(new CharPred('n'))

    test("drex-core-regression-2") {
        val anr = Concat(ar, nr)
        val test = Restrict(id, anr)
        val iterTest = Iter(test)
        assert(DReXDPEvaluator.evaluate("anan", iterTest).get == "anan")
    }

    test("drex-core-regression-3") {
        val epsilonOrCopyA = Else(Epsilon(""), aa)

        val aStar = Iter(aa)
        val bStar = Iter(bb)
        val aStarBStar = Split(aStar, bStar)

        val ab = SymbolR(new CharPred('a', 'b'))
        val l3 = Concat(ar, Concat(ab, br))
        val right = Restrict(aStarBStar, l3)
        val test = Split(epsilonOrCopyA, right)

        assert(DReXDPEvaluator.evaluate("aabb", test).get == "aabb")
    }

    test("drex-core-regression-4") {
        val copyBOrC = Else(bb, cc)
        val epsilonOrCopyA = Else(Epsilon(""), aa)

        val babb = Concat(Concat(Concat(br, ar), br), br)
        val abac = Concat(Concat(Concat(ar, br), ar), cr)
        val regex = Union(babb, abac)
        val aStar = Star(ar)
        val bOrC = Union(br, cr)
        val aStarBC = Concat(aStar, bOrC)

        val idAStar = Iter(aa)
        val idAStarBOrC = Split(idAStar, copyBOrC)
        val expr = Split(idAStarBOrC, idAStarBOrC)
        val cs = ChainedSum(expr, aStarBC)

        val right = Restrict(cs, regex)
        val test = Split(epsilonOrCopyA, right)

        assert(DReXDPEvaluator.evaluate("ababb", test).get == "abababb")
    }

    test("drex-core-regression-5") {
        val copyBOrC = Else(bb, cc)
        val epsilonOrCopyA = Else(Epsilon(""), aa)

        val babab = Concat(br, Concat(ar, Concat(br, Concat(ar, br))))
        val ababac = Concat(ar, Concat(br, Concat(ar, Concat(br, Concat(ar, cr)))))

        val regex = Union(babab, ababac)
        val aStar = Star(ar)
        val bOrC = Union(br, cr)
        val aStarBC = Concat(aStar, bOrC)

        val idAStar = Iter(aa)
        val idAStarBOrC = Split(idAStar, copyBOrC)
        val expr = Split(idAStarBOrC, idAStarBOrC)
        val cs = ChainedSum(expr, aStarBC)
        val right = Restrict(cs, regex)
        val test = Split(epsilonOrCopyA, right)

        assert(DReXDPEvaluator.evaluate("ababab", test).get == "abababab")
    }

    test("drex-core-regression-6") {
        val iterBot = Iter(Bottom)
        assert(iterBot.consistent)
        assert(DReXDPEvaluator.evaluate("", iterBot).get == "")
        assert(DReXDPEvaluator.evaluate("sldlkasd", iterBot).isEmpty)
    }

    test("drex-core-regression-7") {
        val p09IdStar = Iter(digitId)

        val p9 = new CharPred('9')
        val p9IdStar = Iter(Symbol(p9, CharOffset.IDENTITY))

        val iter = true
        val iterBotE1 = 
            if (iter) {
                Split(Iter(Bottom), Epsilon("1"))
            } else {
                Split(Epsilon(""), Epsilon("1"))
            }

        val e1 = Split(p9IdStar, iterBotE1)
        val e2 = Restrict(e1, SymbolR(p9))
        val e = Split(p09IdStar, e2)

        assert(DReXDPEvaluator.evaluate("999999999999999", e).get == "9999999999999991")
    }

    test("drex-core-regression-8") {
        val swallowChar = Symbol(StdCharPred.TRUE, "")

        // Both (iter bot) and (epsilon "") display the bug
        // val iterBot = Iter(Bottom)
        val iterBot = Epsilon("")

        val ids = Iter(id)
        val idTail = Split(swallowChar, ids)
        val idTailPrime = Split(idTail, iterBot)

        // This does not display the bug
        // Expression idTailPrime = idTail

        val restrict = Restrict(idTailPrime, SymbolR(StdCharPred.TRUE))
        val f = Iter(restrict)

        assert(DReXDPEvaluator.evaluate("bc", f).get == "")
    } 
}
