package synth

import exprs._
import evaluators.dp.{RegexDPEvaluator, DReXDPEvaluator}

abstract class Example[Type <: Expression] {
  def check(expr: Type) : Boolean
}
case class IO(input: String, output: String) extends Example[DReXExpr] {
  def check(expr: DReXExpr) : Boolean = DReXDPEvaluator.evaluate(input, expr) == Some(output)
}
case class Defined(input: String) extends Example[DReXExpr] {
  def check(expr: DReXExpr) : Boolean = !DReXDPEvaluator.evaluate(input, expr).isEmpty
}
case class Contains(input: String) extends Example[Regex] {
  def check(expr: Regex) : Boolean = RegexDPEvaluator.evaluate(input, expr)
}

abstract class ExampleStructure[Type <: Expression] {
  def check(e: Type) : Boolean = ExampleStructure.check(e, this)
}
case class AtomicExample[Type <: Expression](e : Example[Type]) extends ExampleStructure[Type]
case class And[Type <: Expression](conjuncts: Seq[ExampleStructure[Type]]) extends ExampleStructure[Type]
case class Or[Type <: Expression](disjuncts: Seq[ExampleStructure[Type]]) extends ExampleStructure[Type]
case class Not[Type <: Expression](base: ExampleStructure[Type]) extends ExampleStructure[Type]

object ExampleStructure {
  def check[Type <: Expression](expr: Type, es: ExampleStructure[Type]) : Boolean = es match {
    case AtomicExample(e) => e.check(expr)
    case And(conjuncts) => conjuncts.forall(_.check(expr))
    case Or(disjuncts) => disjuncts.exists(_.check(expr))
    case Not(base) => !base.check(expr)
  }
}
