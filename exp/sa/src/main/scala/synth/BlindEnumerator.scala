package synth

import exprs._
import scala.collection.mutable.Map

class BlindEnumerator[Type <: Expression](grammar: Grammar[Type]) {

  val cachedExprs : Map[(NonTerminal, Int), Stream[Expression]] = Map()

  def enumerateBySize() : Stream[Stream[Type]] = {
    Stream.from(1).map(size => {
      ofSize(grammar.startNonTerminal, size).map(_.asInstanceOf[Type])
    })
  }
  def enumerate() : Stream[Type] = {
    enumerateBySize.flatten
  }

  def synthesizeFor(exampleStructure : ExampleStructure[Type]) : Type  = {
      enumerate()
        .filter(x => exampleStructure.check(x))
        .head
  }

  def ofSize(nt: NonTerminal, size: Int) : Stream[Expression] = {
    if (!(cachedExprs contains (nt, size))) {
      if (size == 1) {
        cachedExprs((nt, size)) = grammar.rules(nt)
          .collect{ case x:ExprRewrite => x.expr }
          .toStream
      } else {
        // non-expr rewrites
        val nonExprRewrites = grammar.rules(nt).filter{ _.rewriteType != RewriteType.Expression }.toStream
        cachedExprs((nt, size)) = (
          for {
            rew <- nonExprRewrites;
            nts = rew.nonTerminals.toList;
            sizes <- BlindEnumerator.partition(size - 1, nts.length)
          } yield {
            BlindEnumerator.product((nts, sizes).zipped map ofSize)
              .map(rew.instantiateNTs(_))
              .filter(x => x.consistent)
          } 
        ).flatten
      }
    }
    cachedExprs((nt, size))
  }
}

object BlindEnumerator {
  def partition(size: Int, n: Int) : Stream[List[Int]] = {
    assert(size >= 0 && n >= 0)
    if (n > size || (n == 0 && size > 0)) { 
      // Too little or too much to go around
      Stream()
    } else if (size == n) {
      Stream(List.fill(n)(1))
    } else if (n == 1) {
      Stream(List(size))
    } else {
      for {
        first <- (1 to size).toStream;
        rest <- partition(size - first, n - 1)
      } yield {
        first :: rest
      }
    }
  }

  def product(streams: List[Stream[Expression]]) : Stream[List[Expression]] = {
    if (streams.size == 0)
      Stream(Nil)
    else {
      streams.head.flatMap(e => product(streams.tail).map{ l => e :: l })
    }
  }
}
