package synth

import exprs._
import scala.collection.mutable.Stack
import theory.{CharPred,CharFunc}


sealed abstract class NonTerminal(name: String)
case class RegexNT(name: String) extends NonTerminal(name)
case class DReXNT(name: String) extends NonTerminal(name)

object RewriteType {
    sealed trait ValueType
    case object NT extends ValueType
    case object Function extends ValueType
    case object Expression extends ValueType
    case object Let extends ValueType
    case object Variable extends ValueType
}
sealed abstract class Rewrite {
    val rewriteType : RewriteType.ValueType
    val nonTerminals : Seq[NonTerminal]

    def instantiateNTs(substitutions: Stack[Expression],
        variableValuations: Map[String, Expression]) : Expression 

    def instantiateNTs(substitutions: List[Expression],
        variableValuations: Map[String, Expression] = Map()) 
    : Expression = {
        instantiateNTs(Stack() ++ substitutions, variableValuations)
    }
}
case class NTRewrite(nt: NonTerminal) extends Rewrite {
    override val rewriteType = RewriteType.NT
    override lazy val nonTerminals : Seq[NonTerminal] = Seq(nt)

    def instantiateNTs(substitutions: Stack[Expression],
        variableValuations: Map[String, Expression]) : Expression = substitutions.pop
}
case class ExprRewrite(val expr: Expression) extends Rewrite {
    override val rewriteType = RewriteType.Expression
    override lazy val nonTerminals : Seq[NonTerminal] = Nil

    def instantiateNTs(substitutions: Stack[Expression],
        variableValuations: Map[String, Expression]) : Expression = expr
}

case class VariableRewrite(name: String) extends Rewrite {
    override val rewriteType = RewriteType.Variable
    override lazy val nonTerminals : Seq[NonTerminal] = Nil

    def instantiateNTs(substitutions: Stack[Expression], 
        variableValuations: Map[String, Expression]) : Expression = variableValuations(name)
}
case class LetRewrite(
    variable: String,
    variableRewrite: Rewrite,
    exprRewrite: Rewrite)
extends Rewrite {
    override val rewriteType = RewriteType.Let
    override lazy val nonTerminals : Seq[NonTerminal] = variableRewrite.nonTerminals ++ exprRewrite.nonTerminals

    def instantiateNTs(substitutions: Stack[Expression], variableValuations: Map[String, Expression]) : Expression =
    {
        val e = variableRewrite.instantiateNTs(substitutions, variableValuations)
        exprRewrite.instantiateNTs(substitutions, variableValuations + (variable -> e))
    }
}


// sealed abstract class FunctionRewrite(val children: Rewrite*) // https://issues.scala-lang.org/browse/SI-7436
sealed abstract class FunctionRewrite(val children: Seq[Rewrite])
extends Rewrite {
    val arity = children.length
    override val rewriteType = RewriteType.Function
    lazy val nonTerminals : Seq[NonTerminal] = children.flatMap { _.nonTerminals }

    def build(ch: Seq[Expression]) : Expression

    val associative = false
    val commutative = false

    def instantiateNTs(substitutions: Stack[Expression],
        variableValuations: Map[String, Expression]) : Expression =
        build(children.map{ c => c.instantiateNTs(substitutions, variableValuations) })
}
sealed abstract class FunctionTemplate1[ArgType, RetType <: Expression](func: ArgType => RetType, child: Rewrite)
extends FunctionRewrite(Seq(child)) {
    def build(ch: Seq[Expression]) : RetType = {
        assert(ch.length == 1)
        func(ch(0).asInstanceOf[ArgType])
    }
}
sealed abstract class FunctionTemplate2[ArgType1, ArgType2, RetType <: Expression](
    func: (ArgType1, ArgType2) => RetType,
    child1: Rewrite,
    child2: Rewrite,
    override val associative: Boolean = false,
    override val commutative: Boolean = false)
extends FunctionRewrite(Seq(child1, child2)) {
    def build(ch: Seq[Expression]) : RetType = {
        assert(ch.length == 2)
        func(ch(0).asInstanceOf[ArgType1], ch(1).asInstanceOf[ArgType2])
    }
}

case class ConcatTemplate(left: Rewrite, right: Rewrite)
    extends FunctionTemplate2(Concat.apply, left, right)
case class StarTemplate(base: Rewrite)
    extends FunctionTemplate1(Star.apply, base)
case class UnionTemplate(left: Rewrite, right: Rewrite)
    extends FunctionTemplate2(Union.apply, left, right, associative=(left == right), commutative=(left == right))
case class IntersectionTemplate(left: Rewrite, right: Rewrite)
    extends FunctionTemplate2(Intersection.apply, left, right, associative=(left == right), commutative=(left == right))
case class ComplementTemplate(base: Rewrite)
    extends FunctionTemplate1(Complement.apply, base)
case class DomainTemplate(base: Rewrite)
    extends FunctionTemplate1({ x:DReXExpr => x.domain }, base)

case class SplitTemplate(left: Rewrite, right: Rewrite)
    extends FunctionTemplate2(Split.apply _, left, right)
case class LSplitTemplate(left: Rewrite, right: Rewrite)
    extends FunctionTemplate2(LSplit.apply, left, right)
case class IterTemplate(base: Rewrite)
    extends FunctionTemplate1(Iter.apply, base)
case class LIterTemplate(base: Rewrite)
    extends FunctionTemplate1(LIter.apply, base)
case class ElseTemplate(left: Rewrite, right: Rewrite)
    extends FunctionTemplate2(Else.apply, left, right, commutative=(left == right), associative=(left == right))
case class CombineTemplate(left: Rewrite, right: Rewrite)
    extends FunctionTemplate2(Combine.apply, left, right, associative=(left == right))
case class ChainedSumTemplate(base: Rewrite, regex: Rewrite)
    extends FunctionTemplate2(ChainedSum.apply, base, regex)
case class LChainedSumTemplate(base: Rewrite, regex: Rewrite)
    extends FunctionTemplate2(LChainedSum.apply, base, regex)
case class RestrictTemplate(base: Rewrite, regex: Rewrite)
    extends FunctionTemplate2(Restrict.apply, base, regex)
case class ComposeTemplate(inner: Rewrite, outer: Rewrite)
    extends FunctionTemplate2(Compose.apply, inner, outer)


class Grammar[Type <: Expression](
    val nonTerminals: Set[NonTerminal],
    val startNonTerminal: NonTerminal,
    val rules: Map[NonTerminal, Set[Rewrite]]
) {
    assert(nonTerminals contains startNonTerminal)
}

object Grammar {
    def standardGrammar(
        strings: Traversable[String],
        baseExprs: Traversable[Expression] = Seq(),
        charPreds: Traversable[CharPred],
        charFuncs: Traversable[CharFunc]) 
    : Grammar[DReXExpr] = {

        val drexNT = DReXNT("DReXStart")
        val regexNT = RegexNT("RegexStart")

        val drexNTR = NTRewrite(drexNT)
        val regexNTR = NTRewrite(regexNT)

        val drexProductions : Set[Rewrite] = 
            Set(
                SplitTemplate(drexNTR, drexNTR),
                LSplitTemplate(drexNTR, drexNTR),
                IterTemplate(drexNTR),
                LIterTemplate(drexNTR),
                ElseTemplate(drexNTR, drexNTR),
                CombineTemplate(drexNTR, drexNTR),
                ChainedSumTemplate(drexNTR, regexNTR),
                LChainedSumTemplate(drexNTR, regexNTR),
                RestrictTemplate(drexNTR, regexNTR)
                // ComposeTemplate
            ) ++ (
                baseExprs.filter{ _.isInstanceOf[DReXExpr] } ++
                strings.map { s => Epsilon(s) } ++
                charPreds.flatMap(cp => charFuncs.map {cf => Symbol(cp, cf) })
            ).map{ ExprRewrite(_) }

        val regexProductions : Set[Rewrite] =
            Set(
                ExprRewrite(EpsilonR),
                ConcatTemplate(regexNTR, regexNTR),
                StarTemplate(regexNTR),
                UnionTemplate(regexNTR, regexNTR),
                IntersectionTemplate(regexNTR, regexNTR),
                ComplementTemplate(regexNTR),
                DomainTemplate(drexNTR)
            ) ++ (
                baseExprs.filter{ _.isInstanceOf[Regex] } ++
                charPreds.map(SymbolR(_))
            ).map{ ExprRewrite(_) }

        val productions : Map[NonTerminal, Set[Rewrite]] = Map(
            drexNT -> drexProductions,
            regexNT -> regexProductions
        )

        new Grammar(Set(regexNT, drexNT), drexNT, productions)
    }
}
