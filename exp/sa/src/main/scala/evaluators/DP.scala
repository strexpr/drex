package evaluators.dp

import exprs._
import scala.collection.mutable.{Map,HashMap}

object RegexDPEvaluator {

    type MemoRecord = Set[(Int, Int)]

    private[dp] def fillEvalMap(str: List[Char], evalMaps: Map[Regex, MemoRecord], r: Regex) {
        for (c <- r.children; if !(evalMaps contains c))
            fillEvalMap(str, evalMaps, c)

        evalMaps(r) = r match {
            case Empty => Set()
            case EpsilonR => (0 to str.length).map(x => (x, x)).toSet
            case SymbolR(charPred) => (0 until str.length)
                .filter(x => charPred.isSatisfiedBy(str(x)))
                .map(x => (x, x + 1))
                .toSet
            case Concat(left, right) =>
                for {
                    (i, j) <- evalMaps(left);
                    (k, l) <- evalMaps(right);
                    if j == k
                } yield (i, l)
            case Star(base) => {
                var closure = (0 to str.length).map(x => (x, x)).toSet
                var new_ones : MemoRecord = Set()
                do {
                    new_ones =
                        for {
                            (i, j) <- closure;
                            (k, l) <- evalMaps(base);
                            if j == k && !(closure contains (i, l))
                        } yield (i, l)
                    closure = closure.union(new_ones)
                } while (new_ones.size != 0)
                closure
            }
            case Union(left, right) => evalMaps(left).union(evalMaps(right))
            case Intersection(left, right) => evalMaps(left).intersect(evalMaps(right))
            case Complement(base) => (
                for {
                    i <- 0 to str.length;
                    j <- 0 to str.length;
                    if !(evalMaps(base) contains (i, j))
                } yield (i, j)
                ).toSet
        }
    }

    def evaluate(str: List[Char], regex: Regex) : Boolean = {
        val evalMaps : Map[Regex, MemoRecord] = new HashMap
        fillEvalMap(str, evalMaps, regex)
        evalMaps(regex).contains((0, str.length))
    }

    def evaluate(str: String, regex: Regex) : Boolean = evaluate(str.toList, regex)
}

object DReXDPEvaluator {
    abstract class LazyCharList {
        def ++(that: LazyCharList) = Append(this, that)
        def toList() : List[Char] = this match {
            case ConcreteList(content) => content
            case Append(a, b) => a.toList ++ b.toList
        }
    }
    case class ConcreteList(content: List[Char]) extends LazyCharList
    case class Append(a: LazyCharList, b: LazyCharList) extends LazyCharList

    // type Output = List[Char]
    // val emptyOutput = Nil
    // def toOutput(v: List[Char]) = v

    type Output = LazyCharList
    val emptyOutput = ConcreteList(Nil)
    def toOutput(v: List[Char]) : Output = ConcreteList(v)

    type Input = List[Char]
    type MemoRecord = Map[(Int, Int), Output]
    type RegexMemoRecord = RegexDPEvaluator.MemoRecord

    private def toMutableMap[A,B](x: Traversable[(A, B)]) : Map[A,B] =
        Map() ++ x

    private[dp] def fillEvalMap(
        str: Input,
        evalMaps: Map[DReXExpr, MemoRecord],
        regexEvalMaps: Map[Regex, RegexMemoRecord], expr: DReXExpr)
    {
        for (c <- expr.children; if !(evalMaps contains c))
            fillEvalMap(str, evalMaps, regexEvalMaps, c)

        evalMaps(expr) = expr match {
            case Epsilon(out) => toMutableMap{ (0 to str.length).map(x => ((x, x), toOutput(out))) }
            case Symbol(cp, cfs) => toMutableMap {
                (0 until str.length).filter(x => cp.isSatisfiedBy(str(x))) // Positions in the string
                    .map(x => ((x, x+1), toOutput(cfs.map{ cf => cf.instantiateWith(str(x)) })))
            }
            case Bottom => Map()
            case Split(left, right) => evalSplit(left, right, evalMaps, false)
            case LSplit(left, right) => evalSplit(left, right, evalMaps, true)

            case Iter(base) => evalIter(str, base, evalMaps, false)
            case LIter(base) => evalIter(str, base, evalMaps, true)

            case Else(left, right) => evalMaps(left) ++ evalMaps(right)
            case Combine(left, right) =>
                evalMaps(left).map{ case (k,o) => (k, o ++ evalMaps(right)(k)) }

            case ChainedSum(base, regex) => evalChainedSum(str, base, regex, evalMaps, regexEvalMaps, false)
            case LChainedSum(base, regex) => evalChainedSum(str, base, regex, evalMaps, regexEvalMaps, true)

            case Restrict(base, regex) => {
                RegexDPEvaluator.fillEvalMap(str, regexEvalMaps, regex)
                evalMaps(base).filter{ case (k,v) => regexEvalMaps(regex) contains k }
            }
            case Compose(inner, outer) => ???
        }
    }

    def evaluate(str: Input, expr: DReXExpr) : Option[String] = {
        val evalMaps : Map[DReXExpr, MemoRecord] = new HashMap
        val regexEvalMaps : Map[Regex, RegexMemoRecord] = new HashMap

        fillEvalMap(str, evalMaps, regexEvalMaps, expr)
        evalMaps(expr).get((0, str.length)).map(_.toList.mkString)
    }

    def evaluate(str: String, expr: DReXExpr) : Option[String] = evaluate(str.toList, expr)

    private def evalSplit(
        left: DReXExpr,
        right: DReXExpr,
        evalMaps: Map[DReXExpr, MemoRecord],
        reverse : Boolean)
    : MemoRecord =  {
        for {
            ((i, j), o1) <- evalMaps(left);
            ((k, l), o2) <- evalMaps(right);
            if j == k
        } yield ((i, l), if (reverse) o2 ++ o1 else o1 ++ o2)
    }

    private def evalIter(
        str: Input,
        base: DReXExpr,
        evalMaps: Map[DReXExpr, MemoRecord],
        reverse : Boolean) : MemoRecord = {

        var closure : MemoRecord = toMutableMap{ (0 to str.length).map(x => ((x, x), emptyOutput)) }
        var new_ones : MemoRecord = Map()
        do {
            new_ones = for {
                ((i, j), o1) <- closure;
                ((k, l), o2) <- evalMaps(base);
                if j == k && !(closure contains ((i, l)))
            } yield (i, l) -> (if (reverse) o2 ++ o1 else o1 ++ o2)
            closure = closure ++ new_ones
        } while (new_ones.size != 0)
        closure
    }

    private def evalChainedSum(
        str: Input,
        base: DReXExpr,
        regex: Regex,
        evalMaps: Map[DReXExpr, MemoRecord],
        regexEvalMaps: Map[Regex, RegexMemoRecord],
        reverse : Boolean) : MemoRecord = {

        RegexDPEvaluator.fillEvalMap(str, regexEvalMaps, regex)

        val baseEval = 
            evalMaps(base)
                .groupBy({ case ((i, j), o) => i })
                .withDefaultValue(Map())

        var new_ones : Map[Int, MemoRecord] = Map()
        for {
            (i, j) <- regexEvalMaps(regex)
            (k, l) <- regexEvalMaps(regex)
            if k == j
        } {
            if (!(new_ones contains j))
                new_ones(j) = Map()
            new_ones(j) += ((i, l) -> evalMaps(base)((i, l)))
        }

        var closure : MemoRecord = evalMaps(base)
        do {
            val old_ones = new_ones
            new_ones = Map()
            for ((mid_point, old_records) <- old_ones) {
                for (
                    ((i, new_mid_point), o1) <- old_records;
                    ((_, end_point), o2)  <- baseEval(mid_point);
                    if end_point > new_mid_point
                ) {
                    val new_output = if (reverse) o2 ++ o1 else o1 ++ o2
                    val new_record = ((i, end_point), new_output)
                    if (!(new_ones contains new_mid_point))
                        new_ones(new_mid_point) = Map()
                    new_ones(new_mid_point) += new_record
                }
            }

            for ((_, records) <- new_ones) {
                closure ++= records
            }
        } while(new_ones.size != 0)
        closure
    }
}
