package exprs

import theory.CharPred
import theory.CharSolver
import automata.sfa.SFA
import automata.sfa.SFAInputMove
import collection.JavaConversions._

sealed abstract class Regex extends Expression {
    lazy val automata : SFA[CharPred, Character] = Regex.automata(this)
    lazy val children : Set[Regex] = Regex.children(this)
    lazy val ambiguousInput : Option[List[Character]] = Regex.ambiguousInput(this)
    lazy val unambiguous : Boolean = children.forall{x => x.unambiguous} && ambiguousInput.isEmpty
    lazy val consistent : Boolean = unambiguous

    def contains(other: Regex) : Boolean = Regex.containment(this, other)
    val exprType : ExpressionType = DReXType
}

case object Empty extends Regex
case object EpsilonR extends Regex

case class SymbolR(charPred: CharPred) extends Regex
case class Concat(left: Regex, right: Regex) extends Regex
case class Star(base: Regex) extends Regex
case class Union(left: Regex, right: Regex) extends Regex
case class Intersection(left: Regex, right: Regex) extends Regex

case class Complement(base: Regex) extends Regex

object Regex {
    val charSolver : CharSolver = new CharSolver()

    def ambiguousInput(regex: Regex) : Option[List[Character]] = regex match {
        case Star(base) => {
            if (base.automata.accepts(Nil, charSolver)) {
                Some(Nil)
            } else {
                Option(regex.automata.getAmbiguousInput(charSolver)).map(_.toList)
            }
        }
        case _ => Option(regex.automata.getAmbiguousInput(charSolver)).map(_.toList)
    }

    def containment(sup: Regex, sub: Regex) : Boolean = {
        SFA.difference(sub.automata, sup.automata, charSolver).isEmpty
    }

    def equivalent(left: Regex, right: Regex) : Boolean =
        SFA.areEquivalent(left.automata, right.automata, charSolver)

    def automata(regex: Regex) : SFA[CharPred, Character] = regex match {
        case Empty => SFA.getEmptySFA(charSolver)
        case EpsilonR => SFA.MkSFA(Nil, 0, List(0:java.lang.Integer), charSolver)
        case SymbolR(charPred) => SFA.MkSFA(
            List(new SFAInputMove[CharPred, Character](0, 1, charPred)),
            0,
            List(1:java.lang.Integer),
            charSolver
        )
        case Concat(left, right) => left.automata.concatenateWith(right.automata, charSolver)
        case Star(base) => SFA.star(base.automata, charSolver)
        case Union(left, right) => left.automata.unionWith(right.automata, charSolver)
        case Intersection(left, right) => left.automata.intersectionWith(right.automata, charSolver)
        case Complement(base) => SFA.complementOf(base.automata, charSolver)
    }

    def children(regex: Regex) : Set[Regex] = regex match {
        case Empty => Set()
        case EpsilonR => Set()
        case SymbolR(_) => Set()

        case Concat(left, right) => Set(left, right)
        case Star(base) => Set(base)
        case Union(left, right) => Set(left, right)
        case Intersection(left, right) => Set(left, right)
        case Complement(base) => Set(base)
    }
}
