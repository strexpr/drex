package exprs

abstract class Expression {
    val exprType : ExpressionType
    val consistent : Boolean
}

sealed abstract class ExpressionType
case object RegexType extends ExpressionType
case object DReXType extends ExpressionType
