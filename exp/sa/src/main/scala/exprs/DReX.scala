package exprs

import theory.CharPred
import theory.CharFunc
import theory.CharConstant
import scala.collection.immutable.Set

sealed abstract class DReXExpr extends Expression {
    lazy val domain : Regex = DReXExpr.domain(this)
    lazy val consistent : Boolean = DReXExpr.consistent(this)
    lazy val children : Set[DReXExpr] = DReXExpr.children(this)
    val exprType : ExpressionType = DReXType
}

case class Epsilon(out: List[Char]) extends DReXExpr
case class Symbol(charPred: CharPred, charFuncs: List[CharFunc]) extends DReXExpr
case object Bottom extends DReXExpr

case class Split(left: DReXExpr, right: DReXExpr) extends DReXExpr
case class LSplit(left: DReXExpr, right: DReXExpr) extends DReXExpr

case class Iter(base: DReXExpr) extends DReXExpr
case class LIter(base: DReXExpr) extends DReXExpr

case class Else(left: DReXExpr, right: DReXExpr) extends DReXExpr
case class Combine(left: DReXExpr, right: DReXExpr) extends DReXExpr

case class ChainedSum(base: DReXExpr, regex: Regex) extends DReXExpr
case class LChainedSum(base: DReXExpr, regex: Regex) extends DReXExpr

case class Restrict(base: DReXExpr, regex: Regex) extends DReXExpr
case class Compose(inner: DReXExpr, outer: DReXExpr) extends DReXExpr

// All functionality
object DReXExpr {
    def consistent(expr:DReXExpr) : Boolean = expr match {
        case Epsilon(_) => true
        case Bottom => true
        case Symbol(_, _) => true

        case Split(left, right) => left.consistent && right.consistent && expr.domain.unambiguous
        case LSplit(left, right) => left.consistent && right.consistent && expr.domain.unambiguous

        case Iter(base) => base.consistent && expr.domain.unambiguous
        case LIter(base) => base.consistent && expr.domain.unambiguous

        case Else(left, right) => left.consistent && right.consistent && expr.domain.unambiguous
        case Combine(left, right) => left.consistent && right.consistent && Regex.equivalent(left.domain, right.domain)

        case ChainedSum(base, regex) => 
            base.consistent && Regex.equivalent(base.domain, Concat(regex, regex)) && expr.domain.unambiguous
        case LChainedSum(base, regex) => 
            base.consistent && Regex.equivalent(base.domain, Concat(regex, regex)) && expr.domain.unambiguous

        case Restrict(base, regex) => base.consistent && base.domain.contains(regex)
        case Compose(inner, outer) => ???
    }

    def domain(expr:DReXExpr) : Regex = expr match {
        case Epsilon(out) => EpsilonR
        case Symbol(charPred, _) => SymbolR(charPred)
        case Bottom => Empty
        case Split(left, right) => Concat(left.domain, right.domain)
        case LSplit(left, right) => Concat(left.domain, right.domain)

        case Iter(base) => Star(base.domain)
        case LIter(base) => Star(base.domain)

        case Else(left, right) => Union(left.domain, right.domain)
        case Combine(left, right) => Intersection(left.domain, right.domain)

        case ChainedSum(base, regex) => Concat(regex, Concat(regex, Star(regex)))
        case LChainedSum(base, regex) => Concat(regex, Concat(regex, Star(regex)))

        case Restrict(base, regex) => regex
        case Compose(inner, outer) => inner.domain
    }

    def children(expr: DReXExpr) : Set[DReXExpr] = expr match {
        case Epsilon(out) => Set()
        case Symbol(charPred, _) => Set()
        case Bottom => Set()
        case Split(left, right) => Set(left, right)
        case LSplit(left, right) => Set(left, right)

        case Iter(base) => Set(base)
        case LIter(base) => Set(base)

        case Else(left, right) => Set(left, right)
        case Combine(left, right) => Set(left, right)

        case ChainedSum(base, regex) => Set(base)
        case LChainedSum(base, regex) => Set(base)

        case Restrict(base, regex) => Set(base)
        case Compose(inner, outer) => Set(inner, outer)
    }
}

// Some convinience factory methods
case object Epsilon {
    def apply(out: String) = new Epsilon(out.toList)
}
case object Symbol {
    def apply(charPred: CharPred, charFunc: CharFunc) = new Symbol(charPred, List(charFunc))
    def apply(charPred: CharPred, str: String) = new Symbol(charPred, str.map(x => new CharConstant(x)).toList)
}

