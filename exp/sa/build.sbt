lazy val root = (project in file(".")).
  settings(
    name := "sa",
    version := "0.1"
  )


unmanagedSourceDirectories in Compile += baseDirectory.value / "../../symbolicautomata/SVPAlib/src/"

unmanagedSourceDirectories in Compile += baseDirectory.value / "../../symbolicautomata/BooleanAlgebras/src/theory"

// scalacOptions += "-explaintypes"
scalacOptions += "-deprecation"
scalacOptions += "-feature"
scalacOptions += "-unchecked"

libraryDependencies += "com.google.guava" % "guava" % "19.0"
libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.3.2"

libraryDependencies += "org.scalatest" % "scalatest_2.10" % "2.0" % "test"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.13.0" % "test"

testOptions in Test += Tests.Argument(TestFrameworks.ScalaCheck, "-verbosity", "3")
