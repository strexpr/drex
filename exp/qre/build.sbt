lazy val root = (project in file(".")).
  settings(
    name := "edu/upenn/cis/qre/core/qre",
    version := "0.1"
  )

scalaVersion := "2.12.0"
scalacOptions += "-explaintypes"

scalacOptions += "-deprecation"
scalacOptions += "-feature"
scalacOptions += "-unchecked"
scalacOptions += "-Xlint"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1" % "test"
