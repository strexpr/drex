package edu.upenn.cis.qre
package symbol

abstract class Predicate[T, P <: Predicate[T, P]] extends (T => Boolean) {

  def witness: Option[T]
  def isSat: Boolean = witness.isDefined

  def &&(that: P): P
  def unary_! : P
  def ||(that: P) = !((!this) && !that)

  def -(that: P) = this && !that

}