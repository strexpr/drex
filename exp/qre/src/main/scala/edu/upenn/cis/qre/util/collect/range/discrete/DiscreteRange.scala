package edu.upenn.cis.qre
package util
package collect
package range
package discrete

sealed abstract class DiscreteRange[A](implicit val dom: DiscreteDomain[A]) extends (A => Boolean) {

  def apply(a: A): Boolean
  def witness: A
  def getLo: Option[A]
  def getHi: Option[A]

  def &&(that: DiscreteRange[A]): Option[DiscreteRange[A]] = {
    require(this.dom == that.dom)

    val lo = (getLo, that.getLo) match {
      case (Some(l1), Some(l2)) => Some(dom.max(l1, l2))
      case (lo1 @ Some(_), None) => lo1
      case (None, lo2 @ Some(_)) => lo2
      case (None, None) => None
    }

    val hi = (getHi, that.getHi) match {
      case (Some(h1), Some(h2)) => Some(dom.min(h1, h2))
      case (hi1 @ Some(_), None) => hi1
      case (None, hi2 @ Some(_)) => hi2
      case (None, None) => None
    }

    (lo, hi) match {
      case (Some(l), Some(h)) if dom.lteq(l, h) => Some(BoundBound(l, h))
      case (Some(l), Some(h)) => None
      case (Some(l), None) => Some(BoundInf(l))
      case (None, Some(h)) => Some(InfBound(h))
      case (None, None) => Some(InfInf())
    }
  }

}

final case class InfInf[A]()(implicit dom: DiscreteDomain[A]) extends DiscreteRange[A] {
  override def apply(a: A): Boolean = true
  override def witness: A = dom.someValue
  override def getLo: Option[A] = None
  override def getHi: Option[A] = None
}

final case class InfBound[A](hi: A)(implicit dom: DiscreteDomain[A]) extends DiscreteRange[A] {
  require(dom.maxValue.forall(max => dom.lt(hi, max)))
  override def apply(a: A): Boolean = dom.lteq(a, hi)
  override def witness: A = hi
  override def getLo: Option[A] = None
  override def getHi: Option[A] = Some(hi)
}

final case class BoundBound[A](lo: A, hi: A)(implicit dom: DiscreteDomain[A]) extends DiscreteRange[A] {
  require(dom.minValue.forall(min => dom.lt(min, lo)))
  require(dom.lteq(lo, hi))
  require(dom.maxValue.forall(max => dom.lt(hi, max)))
  override def apply(a: A): Boolean = dom.lteq(lo, a) && dom.lteq(a, hi)
  override def witness: A = lo
  override def getLo: Option[A] = Some(lo)
  override def getHi: Option[A] = Some(hi)
}

final case class BoundInf[A](lo: A)(implicit dom: DiscreteDomain[A]) extends DiscreteRange[A] {
  require(dom.minValue.forall(min => dom.lt(min, lo)))
  override def apply(a: A): Boolean = dom.lteq(lo, a)
  override def witness: A = lo
  override def getLo: Option[A] = Some(lo)
  override def getHi: Option[A] = None
}

object DiscreteRange {

  def apply[A](olo: Option[A], ohi: Option[A])(implicit dom: DiscreteDomain[A]): DiscreteRange[A] = (olo, ohi) match {
    case (None, None) => InfInf()
    case (None, Some(hi)) =>
      if (dom.maxValue.forall(max => dom.lt(hi, max))) {
        InfBound(hi)
      } else {
        InfInf()
      }
    case (Some(lo), None) =>
      if (dom.minValue.forall(min => dom.lt(min, lo))) {
        BoundInf(lo)
      } else {
        InfInf()
      }
    case (Some(lo), Some(hi)) =>
      require(dom.lteq(lo, hi))
      val loGood = dom.minValue.forall(min => dom.lt(min, lo))
      val hiGood = dom.maxValue.forall(max => dom.lt(hi, max))
      (loGood, hiGood) match {
        case (true, true) => BoundBound(lo, hi)
        case (true, false) => BoundInf(lo)
        case (false, true) => InfBound(hi)
        case (false, false) => InfInf()
      }
  }

  def apply[A](v: A)(implicit dom: DiscreteDomain[A]): DiscreteRange[A] = DiscreteRange(Some(v), Some(v))

  def apply[A](lo: A, hi: A)(implicit dom: DiscreteDomain[A]): DiscreteRange[A] = {
    require(dom.lteq(lo, hi))
    DiscreteRange(Some(lo), Some(hi))
  }

  def areAdjacent[A](range1: DiscreteRange[A], range2: DiscreteRange[A]): Boolean = {
    require(range1.dom == range2.dom)
    implicit val dom = range1.dom
    (range1.getHi, range2.getLo) match {
      case (Some(hi1), Some(lo2)) => dom.next(hi1) == lo2
      case _ => false
    }
  }

  def ltEq[A](range1: DiscreteRange[A], range2: DiscreteRange[A]): Boolean = {
    require(range1.dom == range2.dom)
    implicit val dom = range1.dom
    (range1.getHi, range2.getLo) match {
      case (Some(hi1), Some(lo2)) => dom.lteq(hi1, lo2)
      case _ => false
    }
  }

  def disjoint[A](range1: DiscreteRange[A], range2: DiscreteRange[A]): Boolean = (range1 && range2).isEmpty

}
