package edu.upenn.cis.qre
package expr

import eval.Evaluator
import sfa.SFA
import symbol.Predicate

import scala.util.Try

sealed trait DReX[A, P <: Predicate[A, P], +B] extends ARE[A, P, Seq[B]]

case class DReXBase[A, P <: Predicate[A, P], +B](override val p: P, override val f: A => Seq[B])
  extends Base[A, P, Seq[B]](p, f) with DReX[A, P, B]
case class DReXEps[A, P <: Predicate[A, P], +B](bs: Seq[B])
  extends Epsilon[A, P, Seq[B]](bs) with DReX[A, P, B]
case class DReXBot[A, P <: Predicate[A, P], +B]() extends Bot[A, P, Seq[B]] with DReX[A, P, B]

case class DReXUnion[A, P <: Predicate[A, P], +B](override val e1: DReX[A, P, B], override val e2: DReX[A, P, B])
  extends Union(e1, e2) with DReX[A, P, B]
case class DReXCombine[A, P <: Predicate[A, P], B](override val e1: DReX[A, P, B], override val e2: DReX[A, P, B])
  extends Intersection(e1, e2, (s1: Seq[B], s2: Seq[B]) => s1 ++ s2) with DReX[A, P, B]

case class DReXSplit[A, P <: Predicate[A, P], B](override val e1: DReX[A, P, B], override val e2: DReX[A, P, B])
  extends Concat(e1, e2, (s1: Seq[B], s2: Seq[B]) => s1 ++ s2) with DReX[A, P, B]
case class DReXLeftSplit[A, P <: Predicate[A, P], B](override val e1: DReX[A, P, B], override val e2: DReX[A, P, B])
  extends Concat(e1, e2, (s1: Seq[B], s2: Seq[B]) => s2 ++ s1) with DReX[A, P, B]
case class DReXIter[A, P <: Predicate[A, P], B](override val e: DReX[A, P, B])
  extends Iter(Seq(), e, (s: Seq[B], se: Seq[B]) => s ++ se)
case class DReXLeftIter[A, P <: Predicate[A, P], B](override val e: DReX[A, P, B])
  extends Iter(Seq(), e, (s: Seq[B], se: Seq[B]) => se ++ s)

case class DReXChain[A, P <: Predicate[A, P], B](e: DReX[A, P, B], r: Regex[A, P])
  extends ARE[A, P, Seq[B]] with DReX[A, P, B] {
  override def evaluator[T]: Evaluator[A, P, Seq[B], T] = ??? // TODO!
  override def consistencyWitness: Try[SFA[A, P, _]] = ??? // TODO!
}

case class DReXLeftChain[A, P <: Predicate[A, P], B](e: DReX[A, P, B], r: Regex[A, P])
  extends ARE[A, P, Seq[B]] with DReX[A, P, B] {
  override def evaluator[T]: Evaluator[A, P, Seq[B], T] = ??? // TODO!
  override def consistencyWitness: Try[SFA[A, P, _]] = ??? // TODO!
}
