package edu.upenn.cis.qre
package expr

import eval._
import sfa.SFA
import symbol.Predicate

import scala.util.{Failure, Success, Try}

/**
  * An abstract regular expression (ARE) transforms sequences of [[A]]s into values of type [[B]].
  * @tparam A type of sequence elements
  * @tparam P type of predicates being applied to sequence elements
  * @tparam B type of values produced
  */
trait ARE[A, P <: Predicate[A, P], +B] extends (Seq[A] => Option[B]) {

  def evaluator[T]: Evaluator[A, P, B, T]
  def consistencyWitness: Try[SFA[A, P, _]]

  def apply(as: Seq[A]): Option[B] = consistencyWitness match {
    case Success(_) =>
      val evalr = evaluator[Unit]
      var ans = evalr.start(())
      for (a <- as) {
        ans = evalr.symbol(a)
      }
      ans.map(_._1)
    case Failure(_) => ??? // TODO!
  }

}

class Base[A, P <: Predicate[A, P], +B](val p: P, val f: A => B) extends ARE[A, P, B] {
  override def evaluator[T]: Evaluator[A, P, B, T] = new BaseEvaluator(this)
  override lazy val consistencyWitness: Try[SFA[A, P, _]] = ??? // TODO!
}

class Epsilon[A, P <: Predicate[A, P], +B](val b: B) extends ARE[A, P, B] {
  override def evaluator[T]: Evaluator[A, P, B, T] = new EpsEvaluator(this)
  override lazy val consistencyWitness: Try[SFA[A, P, _]] = ??? // TODO!
}

class Bot[A, P <: Predicate[A, P], +B]() extends ARE[A, P, B] {
  override def evaluator[T]: Evaluator[A, P, B, T] = new BotEvaluator(this)
  override lazy val consistencyWitness: Try[SFA[A, P, _]] = ??? // TODO!
}

class Union[A, P <: Predicate[A, P], +B](val e1: ARE[A, P, B], val e2: ARE[A, P, B]) extends ARE[A, P, B] {
  override def evaluator[T]: Evaluator[A, P, B, T] = new UnionEvaluator(this)
  override lazy val consistencyWitness: Try[SFA[A, P, _]] = ??? // TODO!
}

class Intersection[A, P <: Predicate[A, P], B1, B2, +B](
  val e1: ARE[A, P, B1],
  val e2: ARE[A, P, B2],
  val f: (B1, B2) => B
) extends ARE[A, P, B] {
  override def evaluator[T]: Evaluator[A, P, B, T] = new IntersectionEvaluator(this)
  override def consistencyWitness: Try[SFA[A, P, _]] = ??? // TODO!
}

class Concat[A, P <: Predicate[A, P], B1, B2, +B](
  val e1: ARE[A, P, B1],
  val e2: ARE[A, P, B2],
  val f: (B1, B2) => B
) extends ARE[A, P, B] {
  override def evaluator[T]: Evaluator[A, P, B, T] = new ConcatEvaluator(this)
  override def consistencyWitness: Try[SFA[A, P, _]] = ??? // TODO!
}

class Iter[A, P <: Predicate[A, P], Be, B](
  val b0: B,
  val e: ARE[A, P, Be],
  val f: (B, Be) => B
) extends ARE[A, P, B] {
  override def evaluator[T]: Evaluator[A, P, B, T] = new IterEvaluator(this)
  override def consistencyWitness: Try[SFA[A, P, _]] = ??? // TODO!
}
