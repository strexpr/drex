package edu.upenn.cis.qre.util.collect

import scala.collection.mutable.ListBuffer
import scala.collection.{IndexedSeqLike, IterableLike, Seq, mutable}

sealed abstract class LazySeq[+A](val length: Int)
  extends Iterable[A]
    with IndexedSeq[A]
    with IndexedSeqLike[A, LazySeq[A]]
    with IterableLike[A, LazySeq[A]] {

  require(length >= 0)

  override def iterator: Iterator[A] = new Iterator[A]{
    var iter: Iterator[A] = Iterator.empty
    var stack: List[LazySeq[A]] = List(LazySeq.this)

    def prepareNext(): Unit = {
      if (!iter.hasNext) {
        while (stack.nonEmpty && stack.head.isInstanceOf[SeqCat[A]]) {
          val SeqCat(als1, als2) = stack.head
          stack = stack.tail
          if (als2.nonEmpty) stack = als2 :: stack
          if (als1.nonEmpty) stack = als1 :: stack
        }

        if (stack.nonEmpty) {
          val SeqBase(as) = stack.head
          iter = as.iterator
          stack = stack.tail
        }
      }
    }

    override def hasNext: Boolean = {
      prepareNext()
      iter.hasNext
    }

    override def next(): A = {
      prepareNext()
      iter.next()
    }

  }

  override def apply(idx: Int): A = {
    if (idx < 0 || length <= idx) {
      throw new IndexOutOfBoundsException(s"Accessing element at index $idx of sequence $length elements long.")
    }

    var currIdx = idx
    var curr = this
    while (!curr.isInstanceOf[SeqBase[A]]) {
      val SeqCat(als1, als2) = curr
      if (currIdx < als1.length) {
        curr = als1
      } else {
        curr = als2
        currIdx -= als1.length
      }
    }

    val SeqBase(as) = curr
    as(currIdx)
  }

  override protected[this] def newBuilder: mutable.Builder[A, LazySeq[A]] = ??? // TODO!

  /* def ++(that: LazySeq[_ <: A]): LazySeq[A] = {
    if (this.length == 0) that
    else if (that.length == 0) this
    else SeqCat(this, that)
  } */

}

case class SeqBase[+A](as: Seq[A]) extends LazySeq[A](as.size)
case class SeqCat[+A](als1: LazySeq[A], als2: LazySeq[A]) extends LazySeq[A](als1.length + als2.length)

object LazySeq {

  def toList[A](seq: LazySeq[A]): List[A] = {
    val ans = ListBuffer.empty[A]
    var stack: List[LazySeq[A]] = List(seq)
    while (stack.nonEmpty) {
      stack.head match {
        case SeqBase(as) if as.isEmpty => stack = stack.tail
        case SeqBase(as) if as.nonEmpty =>
          ans ++= as
          stack = stack.tail
        case SeqCat(als1, als2) =>
          stack = als1 :: als2 :: stack.tail
      }
    }
    ans.result()
  }

}
