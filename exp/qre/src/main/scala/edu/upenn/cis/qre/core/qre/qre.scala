package edu.upenn.cis.qre.core.qre

import _root_.edu.upenn.cis.qre.eval.Evaluator
import _root_.edu.upenn.cis.qre.symbol.Predicate
import _root_.edu.upenn.cis.qre.core.term.{Term, UntypedParameter}
import _root_.edu.upenn.cis.qre.expr.Regex

import scala.util.Try

sealed abstract class QRE[A, P <: Predicate[A, P], B] {

  def evaluator[T]: Evaluator[A, P, B, T] = ???
  def eval(seq: Seq[A]): Term[B] = ???

  lazy val param: Set[UntypedParameter] = ???
  lazy val domainType: Try[Regex[A, P]] = ???

}

case class Base[A, P <: Predicate[A, P], B](p: P, f: A => B) extends QRE[A, P, B] {
  require(p ne null)
}
case class Epsilon[A, P <: Predicate[A, P], B](t: Term[B]) extends QRE[A, P, B]
case class Bottom[A, P <: Predicate[A, P], B]() extends QRE[A, P, B]

case class TryElse[A, P<: Predicate[A, P], B](e1: QRE[A, P, B], e2: QRE[A, P, B]) extends QRE[A, P, B] {
  require(e1 ne null)
  require(e2 ne null)
}

abstract class Split[A, P <: Predicate[A, P], B1, B2, B](val e1: QRE[A, P, B1], val e2: QRE[A, P, B2])
  extends QRE[A, P, B] {
  require(e1 ne null)
  require(e2 ne null)
  def combine(t1: Term[B1], tf: Term[B2]): Term[B]
}