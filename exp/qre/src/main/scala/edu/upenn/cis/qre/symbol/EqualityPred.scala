package edu.upenn.cis.qre
package symbol

import edu.upenn.cis.qre.core.util.Enumerable

sealed abstract class EqualityPred[T](set: Set[T])
extends Predicate[T, EqualityPred[T]] {
  require(set ne null)
}

case class MatchAll[T](set: Set[T])(implicit val enumerable: Enumerable[T])
extends EqualityPred(set) {

  def apply(a: T) = set.contains(a)
  lazy val witness = set find (_ => true)

  def &&(that: EqualityPred[T]) = {
    that match {
      case MatchAll(thatSet) => MatchAll(set | thatSet)
      case RejectAll(thatSet) => MatchAll(set &~ thatSet)
    }
  }

  def unary_! = RejectAll(set)

}

object MatchAll {

}

case class RejectAll[T](set: Set[T])(implicit val enumerable: Enumerable[T])
extends EqualityPred(set) {

  def apply(a: T) = !(set contains a)
  lazy val witness = enumerable.allTs find apply

  def &&(that: EqualityPred[T]) = {
    that match {
      case MatchAll(thatSet) => MatchAll(thatSet &~ set)
      case RejectAll(thatSet) => RejectAll(set | thatSet)
    }
  }

  def unary_! = MatchAll(set)

}