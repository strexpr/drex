package edu.upenn.cis.qre.eval

object dp {

  def intervals: Stream[(Int, Int)] = {
    def subIntervals(n: Int): Stream[(Int, Int)] = {
      (n to 1 by -1).map((_, n)).toStream
    }
    Stream.from(1).flatMap(subIntervals(_))
  }

}
