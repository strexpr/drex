package edu.upenn.cis.qre
package sfa

import symbol.Predicate
import util.UPair

import scala.collection.mutable

abstract class State(val isInitial: Boolean, val isFinal: Boolean)
sealed case class WrapperState[+A](
  a: A,
  override val isInitial: Boolean,
  override val isFinal: Boolean
) extends State(isInitial, isFinal)

sealed class SFA[A, P <: Predicate[A, P], Q <: State](
  val states: Set[Q],
  val trans: Q => Map[P, Set[Q]],
  val epsTrans: Q => Set[Q]
)(implicit val someP: P) {

  require(states.flatMap(trans(_).values).flatten.subsetOf(states))
  require(states.flatMap(epsTrans).subsetOf(states))

  lazy val initialStates: Set[Q] = states.filter(_.isInitial)
  lazy val finalStates: Set[Q] = states.filter(_.isFinal)

  lazy val epsClosure: Q => Set[Q] = {
    val closure = (for (q <- states)
                   yield {
                     val cq0 = mutable.HashSet[Q](q)
                     cq0 ++= epsTrans(q)
                     q -> cq0
                   }).toMap

    var changed = true
    while (changed) {
      changed = false
      for (q1 <- states) {
        val cq1 = closure(q1)
        cq1.toSet.flatMap(q2 => closure(q2))
                 .foreach { q3 => changed = cq1.add(q3) || changed }
      }
    }

    closure.mapValues(_.toSet)
  }

  def epsClosure(set: Set[Q]): Set[Q] = set.flatMap(epsClosure(_))

  def accepts(seq: Seq[A]): Boolean = {
    var activeStates = epsClosure(initialStates)
    for (a <- seq) {
      activeStates = for {
                       q <- activeStates
                       (p, succs) <- trans(q) if p(a)
                       qP <- succs
                       qPP <- epsClosure(qP)
                     } yield qPP
    }
    activeStates.exists(_.isFinal)
  }

  lazy val reachableStates: Map[Q, List[A]] = {
    val ans = new mutable.HashMap[Q, List[A]]()
    initialStates.foreach(ans.put(_, List()))

    val queue = new collection.mutable.Queue[Q]()
    queue ++= ans.keys
    while (queue.nonEmpty) {
      val head = queue.dequeue
      val headWitness = ans(head)

      val epsSuccs = epsClosure(head) -- ans.keys
      epsSuccs.foreach(ans.put(_, headWitness))
      queue ++= epsSuccs

      val transHead = trans(head).filterKeys(_.isSat)
                                 .mapValues(_ -- ans.keys)
      for ((p, outs) <- transHead) {
        val pw = p.witness.get
        val succs = outs -- ans.keys
        succs.foreach(ans.put(_, pw :: headWitness))
        queue ++= succs
      }
    }

    ans.mapValues(_.reverse).toMap
  }

  lazy val coreachableStates: Map[Q, List[A]] = {
    val ans = new mutable.HashMap[Q, List[A]]()
    finalStates.foreach(ans.put(_, List()))

    val queue = new collection.mutable.Queue[Q]()
    queue ++= ans.keys
    while (queue.nonEmpty) {
      queue.dequeue
      for {
        q <- states -- ans.keys
        (p, targets) <- trans(q) if p.isSat
        intersection = targets & ans.keySet if intersection.nonEmpty
      } {
        ans += q -> (p.witness.get :: ans(intersection.head))
        queue += q
      }
    }

    ans.toMap
  }

  lazy val trim: SFA[A, P, Q] = {
    val newStates = reachableStates.keySet & coreachableStates.keySet
    val newTrans = newStates.map(q => q -> trans(q).filterKeys(_.isSat)
                                                   .mapValues(_.filter(newStates))).toMap
    val newEpsTrans = newStates.map(q => q -> epsTrans(q).filter(newStates)).toMap
    new SFA(newStates, newTrans, newEpsTrans)
  }

  lazy val flatten: SFA[A, P, WrapperState[Int]] = {
    val stateMap = new mutable.HashMap[Q, WrapperState[Int]]()
    for (q <- states) {
      stateMap += (q -> WrapperState(stateMap.size, q.isInitial, q.isFinal))
    }

    val newTrans = states.map(q => stateMap(q) -> trans(q).mapValues(_.map(stateMap))).toMap
    val newEpsTrans = states.map(q => stateMap(q) -> epsTrans(q).map(stateMap)).toMap

    new SFA(stateMap.values.toSet, newTrans, newEpsTrans)
  }

  lazy val ambiguousWitness: Option[List[A]] = {
    val witnesses = new mutable.HashMap[UPair[Q], List[A]]()

    for {
      q1 <- initialStates
      q2 <- initialStates if q2 != q1
    } {
      witnesses += UPair(q1, q2) -> List()
    }

    for {
      (q1, w) <- reachableStates
      q2 <- epsTrans(q1)
    } {
      witnesses += UPair(q1, q2) -> w
    }

    for {
      (q1, w) <- reachableStates
      (p, q1succs) <- trans(q1) if p.isSat
      qs1 <- q1succs
      qs2 <- q1succs if qs1 != qs2
    } {
      witnesses += UPair(qs1, qs2) -> (p.witness.get :: w)
    }

    for {
      (q1, w) <- reachableStates
      (p1, q1succs1) <- trans(q1) if p1.isSat
      (p2, q1succs2) <- trans(q1) if p2.isSat
      if p1 != p2 && (p1 && p2).isSat
      qs1 <- q1succs1
      qs2 <- q1succs2
    } {
      witnesses += UPair(qs1, qs2) -> ((p1 && p2).witness.get :: w)
    }

    val queue = new mutable.Queue[UPair[Q]]()
    queue ++= witnesses.keys
    while (queue.nonEmpty) {
      val head @ UPair(q1, q2) = queue.dequeue
      val w = witnesses(head)

      def eps(q1: Q, q2: Q): Unit = {
        for (q2p <- epsTrans(q2) if !witnesses.contains(UPair(q1, q2p))) {
          witnesses += UPair(q1, q2p) -> w
          queue += UPair(q1, q2p)
        }
      }
      eps(q1, q2)
      eps(q2, q1)

      for {
        p1 <- trans(q1).keys
        p2 <- trans(q2).keys
        if (p1 && p2).isSat
        q1s <- trans(q1)(p1)
        q2s <- trans(q2)(p2)
        if !witnesses.contains(UPair(q1s, q2s))
      } {
        val p12w = (p1 && p2).witness.get
        witnesses += UPair(q1s, q2s) -> (p12w :: w)
        queue += UPair(q1s, q2s)
      }
    }

    witnesses.filterKeys(_.both(_.isFinal))
             .values
             .headOption
             .map(_.reverse)
  }

  lazy val isAmbiguous: Boolean = ambiguousWitness.nonEmpty

  lazy val trueP: P = someP || !someP

  lazy val minTerms: Set[P] = {
    var ans = Set(trueP)
    val ps = states.flatMap(trans(_).keys)
    ps.foreach { p => ans = ans.map(_ && p) ++ ans.map(_ && !p) }
    ans
  }

}

object SFA {

  def base[A, P <: Predicate[A, P]](p: P): SFA[A, P, WrapperState[Boolean]] = {
    require(p ne null)
    type Q = WrapperState[Boolean]
    implicit val someP = p

    val q0 = WrapperState(false, isInitial = true, isFinal = false)
    val q1 = WrapperState(true, isInitial = false, isFinal = true)
    val states = Set(q0, q1)

    val trans: Map[Q, Map[P, Set[Q]]] = Map(q0 -> Map(p -> Set(q1)), q1 -> Map())
    val epsTrans: Map[Q, Set[Q]] = Map(q0 -> Set(), q1 -> Set())

    new SFA(states, trans, epsTrans)
  }

  def epsilon[A, P <: Predicate[A, P]](implicit someP: P): SFA[A, P, WrapperState[Unit]] = {
    type Q = WrapperState[Unit]
    val q0: Q = WrapperState((), isInitial = true, isFinal = true)
    val states: Set[Q] = Set(q0)
    new SFA(states, Map(q0 -> Map()), Map(q0 -> Set()))
  }

  def bottom[A, P <: Predicate[A, P]](implicit someP: P): SFA[A, P, WrapperState[Unit]] = {
    type Q = WrapperState[Unit]
    val q0: Q = WrapperState((), isInitial = true, isFinal = false)
    val states: Set[Q] = Set(q0)
    new SFA(states, Map(q0 -> Map()), Map(q0 -> Set()))
  }

  def union[A, P <: Predicate[A, P], Q1 <: State, Q2 <: State](
    sfa1: SFA[A, P, Q1],
    sfa2: SFA[A, P, Q2]
  ): SFA[A, P, WrapperState[Either[Q1, Q2]]] = {
    type Qout = WrapperState[Either[Q1, Q2]]
    implicit val someP = sfa1.someP

    def f1(q1: Q1): Qout = WrapperState(Left(q1), q1.isInitial, q1.isFinal)
    def f2(q2: Q2): Qout = WrapperState(Right(q2), q2.isInitial, q2.isFinal)

    val states = sfa1.states.map(f1) ++ sfa2.states.map(f2)

    def t(q: Qout): Map[P, Set[Qout]] = q match {
      case WrapperState(Left(q1), _, _) => sfa1.trans(q1).mapValues(_.map(f1))
      case WrapperState(Right(q2), _, _) => sfa2.trans(q2).mapValues(_.map(f2))
    }
    val trans = states.map(q => q -> t(q)).toMap

    def et(q: Qout): Set[Qout] = q match {
      case WrapperState(Left(q1), _, _) => sfa1.epsTrans(q1).map(f1)
      case WrapperState(Right(q2), _, _) => sfa2.epsTrans(q2).map(f2)
    }
    val epsTrans = states.map(q => q -> et(q)).toMap

    new SFA(states, trans, epsTrans)
  }

  /// Computes the intersection of two [[SFA]]s. Does not necessarily preserve unambiguity.
  def intersection[A, P <: Predicate[A, P], Q1 <: State, Q2 <: State](
    sfa1: SFA[A, P, Q1],
    sfa2: SFA[A, P, Q2]
  ): SFA[A, P, WrapperState[(Q1, Q2)]] = {
    type Qout = WrapperState[(Q1, Q2)]
    implicit val someP = sfa1.someP

    def f(q1: Q1, q2: Q2): Qout = {
      val isInitial = q1.isInitial && q2.isInitial
      val isFinal = q1.isFinal && q2.isFinal
      WrapperState((q1, q2), isInitial, isFinal)
    }

    val states = (sfa1.states, sfa2.states).zipped.map(f)

    def t(q: Qout): Map[P, Set[Qout]] = {
      val WrapperState((q1, q2), _, _) = q
      for {
        (p1, tg1) <- sfa1.trans(q1)
        (p2, tg2) <- sfa2.trans(q2)
      } yield (p1 && p2) -> (tg1, tg2).zipped.map(f)
    }
    val trans = states.map(q => q -> t(q)).toMap

    def et(q: Qout): Set[Qout] = {
      val WrapperState((q1, q2), _, _) = q
      sfa1.epsTrans(q1).map(f(_, q2)) ++ sfa2.epsTrans(q2).map(f(q1, _))
    }
    val epsTrans = states.map(q => q -> et(q)).toMap

    new SFA[A, P, Qout](states, trans, epsTrans)
  }

  def concat[A, P <: Predicate[A, P], Q1 <: State, Q2 <: State](
    sfa1: SFA[A, P, Q1],
    sfa2: SFA[A, P, Q2]
  ): SFA[A, P, WrapperState[Either[Q1, Q2]]] = {
    type Qout = WrapperState[Either[Q1, Q2]]
    implicit val someP = sfa1.someP

    val f1: (Q1 => Qout) = q => WrapperState(Left(q), q.isInitial, isFinal = false)
    val f2: (Q2 => Qout) = q => WrapperState(Right(q), isInitial = false, isFinal = q.isFinal)

    val states = sfa1.states.map(f1) ++ sfa2.states.map(f2)

    def t(q: Qout): Map[P, Set[Qout]] = q match {
      case WrapperState(Left(q1), _, _) => sfa1.trans(q1).mapValues(_.map(f1))
      case WrapperState(Right(q2), _, _) => sfa2.trans(q2).mapValues(_.map(f2))
    }
    val trans = states.map(q => q -> t(q)).toMap

    def et(q: Qout): Set[Qout] = q match {
      case WrapperState(Left(q1), _, _) if q1.isFinal => sfa1.epsTrans(q1).map(f1) ++ sfa2.initialStates.map(f2)
      case WrapperState(Left(q1), _, _) => sfa1.epsTrans(q1).map(f1)
      case WrapperState(Right(q2), _, _) => sfa2.epsTrans(q2).map(f2)
    }
    val epsTrans = states.map(q => q -> et(q)).toMap

    new SFA(states, trans, epsTrans)
  }

  def star[A, P <: Predicate[A, P], Q <: State](sfa: SFA[A, P, Q]): SFA[A, P, WrapperState[Either[Unit, Q]]] = {
    type Qout = WrapperState[Either[Unit, Q]]
    implicit val someP = sfa.someP

    val f: (Q => Qout) = q => WrapperState(Right(q), isInitial = false, isFinal = false)
    val q0 = WrapperState(Left(()), isInitial = true, isFinal = true)
    val states = sfa.states.map(f) + q0

    def t(q: Qout): Map[P, Set[Qout]] = q match {
      case WrapperState(Left(()), _, _) => Map()
      case WrapperState(Right(qr), _, _) => sfa.trans(qr).mapValues(_.map(f))
    }
    val trans = states.map(q => q -> t(q)).toMap

    def et(q: Qout): Set[Qout] = q match {
      case WrapperState(Left(()), _, _) => sfa.initialStates.map(f)
      case WrapperState(Right(qr), _, _) if qr.isFinal => sfa.epsTrans(qr).map(f) ++ Set(q0)
      case WrapperState(Right(qr), _, _) => sfa.epsTrans(qr).map(f)
    }
    val epsTrans = states.map(q => q -> et(q)).toMap

    new SFA(states, trans, epsTrans)
  }

  def minusWitness[A, P <: Predicate[A, P], Q1 <: State, Q2 <: State](
    sfa1: SFA[A, P, Q1],
    sfa2: SFA[A, P, Q2]
  ): Option[List[A]] = {
    def isAns(q1: Q1, q2s: Set[Q2]): Boolean = q1.isFinal && q2s.forall(!_.isFinal)

    val witnesses = new mutable.HashMap[(Q1, Set[Q2]), List[A]]()
    val q20 = sfa2.epsClosure(sfa2.initialStates)
    for (q1 <- sfa1.initialStates) {
      if (isAns(q1, q20)) {
        return Some(List())
      }
      witnesses += (q1, q20) -> List()
    }

    val queue = new mutable.Queue[(Q1, Set[Q2])]()
    queue ++= witnesses.keys
    while (queue.nonEmpty) {
      val (q1, q2s) = queue.dequeue()
      for {
        p1 <- sfa1.trans(q1).keys
        q1p <- sfa1.trans(q1)(p1)
        p2 <- sfa2.minTerms
        p12 = p1 && p2
        if p12.isSat
      } {
        val a = p12.witness.get
        val q2sp = q2s.flatMap(q2 => sfa2.trans(q2).filterKeys(_(a)).values.flatten)
        if (!witnesses.contains(q1p, q2sp)) {
          queue += ((q1p, q2sp))
          witnesses += (q1p, q2sp) -> (a :: witnesses((q1p, q2sp)))
          if (isAns(q1p, q2sp)) {
            return Some((a :: witnesses((q1p, q2sp))).reverse)
          }
        }
      }
    }

    None
  }

  def inequivalenceWitness[A, P <: Predicate[A, P], Q1 <: State, Q2 <: State](
    sfa1: SFA[A, P, Q1],
    sfa2: SFA[A, P, Q2]
  ): Option[List[A]] = minusWitness(sfa1, sfa2) match {
    case w12 @ Some(_) => w12
    case _ => minusWitness(sfa2, sfa1)
  }

  def areInequivalent[A, P <: Predicate[A, P], Q1 <: State, Q2 <: State](
    sfa1: SFA[A, P, Q1],
    sfa2: SFA[A, P, Q2]
  ): Boolean = inequivalenceWitness(sfa1, sfa2).nonEmpty

}
