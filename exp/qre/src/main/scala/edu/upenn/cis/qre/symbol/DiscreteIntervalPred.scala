package edu.upenn.cis.qre
package symbol

import edu.upenn.cis.qre.util.collect.range.discrete.{DiscreteDomain, DiscreteRange}
import util.collect.range.{continuous, _}

case class DiscreteIntervalPred[A](ranges: Seq[DiscreteRange[A]])(implicit val dom: DiscreteDomain[A])
extends Predicate[A, DiscreteIntervalPred[A]] {

  require(ranges.forall(_.dom == dom))
  for ((prev, curr) <- ranges.zip(ranges.tail)) {
    require(DiscreteRange.ltEq(prev, curr))
    require(!DiscreteRange.areAdjacent(prev, curr))
  }

  override def apply(a: A): Boolean = ranges.exists(_(a))
  override def witness: Option[A] = ranges.headOption.map(_.witness)

  override def &&(that: DiscreteIntervalPred[A]): DiscreteIntervalPred[A] = {
    require(this.dom == that.dom)
    val ranges = for {
                   thisRange <- this.ranges
                   thatRange <- that.ranges
                   thisAndThat = thisRange && thatRange
                   if thisAndThat.nonEmpty
                 } yield thisAndThat.get
    DiscreteIntervalPred(ranges)
  }

  override def unary_! : DiscreteIntervalPred[A] = {
    if (ranges.isEmpty) {
      return DiscreteIntervalPred(Seq(continuous.InfInf()))
    }

    val l1 = ranges.head.getLo match {
      case None => Seq()
      case Some(lo) => Seq(DiscreteRange(None, dom.previous(lo)))
    }
    val l2 = for ((prev, curr) <- ranges.zip(ranges.tail))
             yield {
               val Some(prevHi) = prev.getHi
               val Some(currLo) = curr.getLo
               DiscreteRange(dom.next(prevHi), dom.previous(currLo))
             }
    val l3 = ranges.last.getHi match {
      case None => Seq()
      case Some(hi) => Seq(DiscreteRange(dom.next(hi), None))
    }

    DiscreteIntervalPred(l1 ++ l2 ++ l3)
  }

}

object DiscreteIntervalPred {

  def apply[A](v: A)(implicit dom: DiscreteDomain[A]): DiscreteIntervalPred[A] = {
    DiscreteIntervalPred(Seq(DiscreteRange(v)))
  }

  def apply[A](lo: A, hi: A)(implicit dom: DiscreteDomain[A]): DiscreteIntervalPred[A] = {
    DiscreteIntervalPred(Seq(DiscreteRange(lo, hi)))
  }

}
