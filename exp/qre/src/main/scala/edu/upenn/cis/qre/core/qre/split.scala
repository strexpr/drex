package edu.upenn.cis.qre.core.qre

import edu.upenn.cis.qre.symbol.Predicate
import edu.upenn.cis.qre.core.term.{Parameter, Term}

case class SplitRight[A, P <: Predicate[A, P], B1, B2](
  override val e1: QRE[A, P, B1],
  p1: Parameter[B1],
  override val e2: QRE[A, P, B2]
) extends Split[A, P, B1, B2, B2](e1, e2) {
  require(p1 ne null)
  override def combine(t1: Term[B1], t2: Term[B2]): Term[B2] = t2.subst(p1, t1)
}

case class SplitLeft[A, P <: Predicate[A, P], B1, B2](
  override val e1: QRE[A, P, B1],
  p2: Parameter[B2],
  override val e2: QRE[A, P, B2]
) extends Split[A, P, B1, B2, B1](e1, e2) {
  require(p2 ne null)
  override def combine(t1: Term[B1], t2: Term[B2]): Term[B1] = t1.subst(p2, t2)
}