package edu.upenn.cis.qre.core.util

import scala.math.Numeric._

trait BoundedIntegral[T] extends Integral[T] {
  def minValue: T
  def maxValue: T
}

object BoundedIntegral {

  trait ByteIsBoundedIntegral extends BoundedIntegral[Byte] with ByteIsIntegral {
    def minValue: Byte = Byte.MinValue
    def maxValue: Byte = Byte.MaxValue
  }
  implicit object ByteIsBoundedIntegral extends ByteIsBoundedIntegral with Ordering.ByteOrdering

  trait CharIsBoundedIntegral extends BoundedIntegral[Char] with CharIsIntegral {
    def minValue: Char = Char.MinValue
    def maxValue: Char = Char.MaxValue
  }
  implicit object CharIsBoundedIntegral extends CharIsBoundedIntegral with Ordering.CharOrdering

  trait IntIsBoundedIntegral extends BoundedIntegral[Int] with IntIsIntegral {
    def minValue: Int = Int.MinValue
    def maxValue: Int = Int.MaxValue
  }
  implicit object IntIsBoundedIntegral extends IntIsBoundedIntegral with Ordering.IntOrdering

  trait LongIsBoundedIntegral extends BoundedIntegral[Long] with LongIsIntegral {
    def minValue: Long = Long.MinValue
    def maxValue: Long = Long.MaxValue
  }
  implicit object LongIsBoundedIntegral extends LongIsBoundedIntegral with Ordering.LongOrdering

  trait ShortIsBoundedIntegral extends BoundedIntegral[Short] with ShortIsIntegral {
    def minValue: Short = Short.MinValue
    def maxValue: Short = Short.MaxValue
  }
  implicit object ShortIsBoundedIntegral extends ShortIsBoundedIntegral with Ordering.ShortOrdering

}