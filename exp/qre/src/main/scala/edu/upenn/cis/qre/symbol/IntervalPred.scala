package edu.upenn.cis.qre
package symbol

// import com.google.common.collect.ImmutableRangeSet

/* sealed abstract class IntervalPred[A <: Ordered[A]](ranges: ImmutableRangeSet[A])
extends Predicate[A, IntervalPred[A]] {

  def apply(a: A) = ranges.contains(a)
  abstract def witness: Option[A]

}

sealed case class IntervalPred[A <: Ordered[A]](
  ranges: ImmutableRangeSet[A]
) extends Predicate[A, IntervalPred[A]] {

  require(ranges ne null)

  def apply(a: A) = ranges.contains(a)
  lazy val witness: Option[A] = ??? // TODO!

  def &&(that: IntervalPred[A]): IntervalPred[A] = ??? // TODO!

  lazy val unary_! = IntervalPred(ranges.complement)

} */
