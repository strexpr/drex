package edu.upenn.cis.qre.core.util

import scala.collection.immutable.NumericRange.Inclusive

trait Enumerable[T] {
  def allTs: Stream[T]
}

object Enumerable {

  trait ByteIsEnumerable extends Enumerable[Byte] {
    def allTs: Stream[Byte] = new Inclusive(Byte.MinValue, Byte.MaxValue, 1).toStream.map(b => b.asInstanceOf[Byte])
  }
  implicit object ByteIsEnumerable extends ByteIsEnumerable

  trait CharIsEnumerable extends Enumerable[Char] {
    def allTs: Stream[Char] = new Inclusive(Char.MinValue, Char.MaxValue, 1).toStream.map(b => b.asInstanceOf[Char])
  }
  implicit object CharIsEnumerable extends CharIsEnumerable

  trait IntIsEnumerable extends Enumerable[Int] {
    def allTs: Stream[Int] = new Inclusive(Int.MinValue, Int.MaxValue, 1).toStream
  }
  implicit object IntIsEnumerable extends IntIsEnumerable

  trait LongIsEnumerable extends Enumerable[Long] {
    def allTs: Stream[Long] = new Inclusive(Long.MinValue, Long.MaxValue, 1).toStream
  }
  implicit object LongIsEnumerable extends LongIsEnumerable

  trait ShortIsEnumerable extends Enumerable[Short] {
    def allTs: Stream[Short] = new Inclusive(Short.MinValue, Short.MaxValue, 1).toStream.map(b => b.asInstanceOf[Short])
  }
  implicit object ShortIsEnumerable extends ShortIsEnumerable

}