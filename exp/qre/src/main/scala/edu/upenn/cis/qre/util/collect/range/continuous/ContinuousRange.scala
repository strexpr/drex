package edu.upenn.cis.qre.util.collect.range.continuous

sealed trait BoundType
case object Open extends BoundType
case object Closed extends BoundType

sealed abstract class ContinuousRange[A](implicit val dom: ContinuousDomain[A]) extends (A => Boolean) {

  def apply(a: A): Boolean
  def witness: A
  def getLo: Option[(BoundType, A)]
  def getHi: Option[(BoundType, A)]

}

final case class InfInf[A]()(implicit dom: ContinuousDomain[A]) extends ContinuousRange[A] {
  override def apply(a: A): Boolean = true
  override def witness: A = dom.someValue
  override def getLo: Option[A] = None
  override def getHi: Option[A] = None
}

final case class InfBound[A](hi: A, hiType: BoundType)(implicit dom: ContinuousDomain[A]) extends ContinuousRange[A] {
  require(dom.maxValue.forall(max => dom.lt(hi, max)))
  override def apply(a: A): Boolean = dom.lteq(a, hi)
  override def witness: A = hi
  override def getLo: Option[A] = None
  override def getHi: Option[A] = Some(hi)
}

final case class BoundBound[A](
                                loType: BoundType,
                                lo: A,
                                hi: A,
                                hiType: BoundType
                              )(implicit dom: ContinuousDomain[A]) extends ContinuousRange[A] {
  require(dom.minValue.forall(min => dom.lt(min, lo)))
  require(dom.lteq(lo, hi))
  require(dom.maxValue.forall(max => dom.lt(hi, max)))
  override def apply(a: A): Boolean = dom.lteq(lo, a) && dom.lteq(a, hi)
  override def witness: A = lo
  override def getLo: Option[A] = Some(lo)
  override def getHi: Option[A] = Some(hi)
}

final case class BoundInf[A](loType: BoundType, lo: A)(implicit dom: ContinuousDomain[A]) extends ContinuousRange[A] {
  require(dom.minValue.forall(min => dom.lt(min, lo)))
  override def apply(a: A): Boolean = dom.lteq(lo, a)
  override def witness: A = lo
  override def getLo: Option[A] = Some(lo)
  override def getHi: Option[A] = None
}

object ContinuousRange {

}
