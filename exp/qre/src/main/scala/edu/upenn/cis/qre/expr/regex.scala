package edu.upenn.cis.qre
package expr

import symbol.Predicate

sealed trait Regex[A, P <: Predicate[A, P]] extends ARE[A, P, Unit]

case class RegexBase[A, P <: Predicate[A, P]](override val p: P) extends Base[A, P, Unit](p, _ => ()) with Regex[A, P]
case class RegexEps[A, P <: Predicate[A, P]]() extends Epsilon[A, P, Unit](()) with Regex[A, P]
case class RegexBot[A, P <: Predicate[A, P]]() extends Bot[A, P, Unit] with Regex[A, P]

case class RegexUnion[A, P <: Predicate[A, P]](r1: Regex[A, P], r2: Regex[A, P])
  extends Union(r1, r2) with Regex[A, P]

case class RegexConcat[A, P <: Predicate[A, P]](r1: Regex[A, P], r2: Regex[A, P])
  extends Concat(r1, r2, (_: Unit, _: Unit) => ()) with Regex[A, P]
case class RegexStar[A, P <: Predicate[A, P]](r: Regex[A, P])
  extends Iter((), r, (_: Unit, _: Unit) => ())
