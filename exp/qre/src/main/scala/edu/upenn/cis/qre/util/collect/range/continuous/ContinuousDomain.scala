package edu.upenn.cis.qre.util.collect.range.continuous

import scala.math.Ordering._

trait ContinuousDomain[A] extends Ordering[A] {

  def maxValue: Option[A]
  def someValue: A
  def minValue: Option[A]

  /**
    * Given two values, [[ a1 ]] and [[ a2 ]], such that [[ a1 <= a2 ]], returns a value [[ v ]] such that
    * [[ a1 <= v && v <= a2 ]].
    * @param a1 left end-point of interval
    * @param a2 right end-point of interval
    * @throws IllegalArgumentException if [[ a1 > a2 ]]
    */
  def interior(a1: A, a2: A): A

}

object ContinuousDomain {

  trait FloatContinuousDomain extends ContinuousDomain[Float] with FloatOrdering {

    def maxValue: Option[Float] = Some(scala.Float.MaxValue)
    def someValue: Float = 0.0f
    def minValue: Option[Float] = Some(scala.Float.MinValue)

    override def interior(a1: Float, a2: Float) = {
      require(a1 <= a2)
      (a1 / 2) + (a2 / 2)
    }

  }
  implicit object FloatCD extends FloatContinuousDomain

  trait DoubleContinuousDomain extends ContinuousDomain[Double] with DoubleOrdering {

    def maxValue: Option[Double] = Some(scala.Double.MaxValue)
    def someValue: Double = 0.0
    def minValue: Option[Double] = Some(scala.Double.MinValue)

    override def interior(a1: Double, a2: Double) = {
      require(a1 <= a2)
      (a1 / 2) + (a2 / 2)
    }

  }
  implicit object DoubleCD extends DoubleContinuousDomain

  trait BigDecimalContinuousDomain extends ContinuousDomain[BigDecimal] with BigDecimalOrdering {

    def maxValue: Option[BigDecimal] = None
    def someValue: BigDecimal = 0
    def minValue: Option[BigDecimal] = None

    override def interior(a1: BigDecimal, a2: BigDecimal) = {
      require(a1 <= a2)
      (a1 / 2) + (a2 / 2)
    }

  }
  implicit object BigDecimalCD extends BigDecimalContinuousDomain

}
