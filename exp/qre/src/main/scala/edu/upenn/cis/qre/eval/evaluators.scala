package edu.upenn.cis.qre
package eval

import expr._
import symbol.Predicate

abstract class Evaluator[A, P <: Predicate[A, P], +B, T](val e: ARE[A, P, B]) {
  def start(stack: T): Option[(B, T)]
  def symbol(a: A): Option[(B, T)]
}

class BaseEvaluator[A, P <: Predicate[A, P], B, T](override val e: Base[A, P, B]) extends Evaluator[A, P, B, T](e) {

  var ot: Option[T] = None

  override def start(stack: T): Option[(B, T)] = {
    ot.foreach(throw new java.lang.AssertionError("Unreachable code"))
    ot = Some(stack)
    None
  }

  override def symbol(a: A): Option[(B, T)] = ot.map(t => {
    ot = None
    (e.f(a), t)
  })

}

class EpsEvaluator[A, P <: Predicate[A, P], B, T](override val e: Epsilon[A, P, B]) extends Evaluator[A, P, B, T](e) {
  override def start(stack: T): Option[(B, T)] = Some((e.b, stack))
  override def symbol(a: A): Option[(B, T)] = None
}

class BotEvaluator[A, P <: Predicate[A, P], B, T](override val e: Bot[A, P, B]) extends Evaluator[A, P, B, T](e) {
  override def start(stack: T): Option[(B, T)] = None
  override def symbol(a: A): Option[(B, T)] = None
}

class UnionEvaluator[A, P <: Predicate[A, P], B, T](override val e: Union[A, P, B])
extends Evaluator[A, P, B, T](e) {

  val m1: Evaluator[A, P, B, T] = e.e1.evaluator
  val m2: Evaluator[A, P, B, T] = e.e2.evaluator

  override def start(stack: T): Option[(B, T)] = process(m1.start(stack), m2.start(stack))
  override def symbol(a: A): Option[(B, T)] = process(m1.symbol(a), m2.symbol(a))

  def process(r1: Option[(B, T)], r2: Option[(B, T)]): Option[(B, T)] = (r1, r2) match {
    case (None, None) => None
    case (Some(t1), None) => Some(t1)
    case (None, Some(t2)) => Some(t2)
    case (Some(t1), Some(t2)) => throw new java.lang.AssertionError("Unreachable code!")
  }

}

class IntersectionEvaluator[A, P <: Predicate[A, P], B1, B2, B, T](override val e: Intersection[A, P, B1, B2, B])
extends Evaluator[A, P, B, T](e) {

  val m1: Evaluator[A, P, B1, T] = e.e1.evaluator
  val m2: Evaluator[A, P, B2, T] = e.e2.evaluator

  override def start(stack: T): Option[(B, T)] = process(m1.start(stack), m2.start(stack))
  override def symbol(a: A): Option[(B, T)] = process(m1.symbol(a), m2.symbol(a))

  def process(r1: Option[(B1, T)], r2: Option[(B2, T)]): Option[(B, T)] = (r1, r2) match {
    case (None, None) => None
    case (Some((b1, st1)), Some((b2, st2))) if st1 == st2 => Some((e.f(b1, b2), st1))
    case _ => throw new java.lang.AssertionError("Unreachable code!")
  }

}

class ConcatEvaluator[A, P <: Predicate[A, P], B1, B2, B, T](override val e: Concat[A, P, B1, B2, B])
extends Evaluator[A, P, B, T](e) {

  val m1: Evaluator[A, P, B1, T] = e.e1.evaluator
  val m2: Evaluator[A, P, B2, (B1, T)] = e.e2.evaluator

  override def start(stack: T): Option[(B, T)] = m1.start(stack) match {
    case Some(t1) => m2.start(t1).map(t2 => (e.f(t2._2._1, t2._1), t2._2._2))
    case None => None
  }

  override def symbol(a: A): Option[(B, T)] = {
    val r1 = m1.symbol(a)
    val r2 = m2.symbol(a)
    val r2Prime = r1.flatMap(m2.start)
    (r2, r2Prime) match {
      case (Some((t2, (t1, stack))), None) => Some((e.f(t1, t2), stack))
      case (None, Some((t2, (t1, stack)))) => Some((e.f(t1, t2), stack))
      case (None, None) => None
      case (Some(t2), Some(t2Prime)) => throw new java.lang.AssertionError("Unreachable code")
    }
  }

}

class IterEvaluator[A, P <: Predicate[A, P], Be, B, T](override val e: Iter[A, P, Be, B])
extends Evaluator[A, P, B, T](e) {

  val me: Evaluator[A, P, Be, (B, T)] = e.e.evaluator

  override def start(stack: T): Option[(B, T)] = {
    val res = (e.b0, stack)
    val meRes = me.start(res)
    assert(meRes.isEmpty)
    Some(res)
  }

  override def symbol(a: A): Option[(B, T)] = {
    me.symbol(a) match {
      case Some((be, (b, stack))) =>
        val res = e.f(b, be)
        val meRes2 = me.start((res, stack))
        assert(meRes2.isEmpty)
        Some((res, stack))
      case None => None
    }
  }

}
