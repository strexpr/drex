package edu.upenn.cis.qre.core.util

object Util {

  def map2[T, U](seq: Seq[T], f2: (T, T) => U): Seq[U] = {
    val seq2 = seq.drop(1)
    seq.zip(seq2).map(tt => f2(tt._1, tt._2))
  }

  def forall2[T](seq: Seq[T], pred2: (T, T) => Boolean): Boolean = map2(seq, pred2).forall(b => b)

}