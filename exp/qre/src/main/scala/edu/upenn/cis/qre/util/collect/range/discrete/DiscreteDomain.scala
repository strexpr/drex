package edu.upenn.cis.qre
package util
package collect
package range
package discrete

import scala.math.Ordering._

trait DiscreteDomain[A] extends Ordering[A] {

  def distance(start: A, end: A): Long

  def maxValue: Option[A]
  def someValue: A
  def minValue: Option[A]

  def next(value: A): Option[A]
  def previous(value: A): Option[A]

}

object DiscreteDomain {

  trait UnitDiscreteDomain extends DiscreteDomain[Unit] with UnitOrdering {
    override def distance(start: Unit, end: Unit): Long = 0

    override val maxValue: Option[Unit] = Some(())
    override val someValue: Unit = ()
    override val minValue: Option[Unit] = Some(())

    override def next(value: Unit): Option[Unit] = None
    override def previous(value: Unit): Option[Unit] = None
  }
  implicit object UnitDD extends UnitDiscreteDomain

  trait BooleanDiscreteDomain extends DiscreteDomain[Boolean] with BooleanOrdering {
    def toInt(value: Boolean): Int = if (value) 1 else 0

    override def distance(start: Boolean, end: Boolean): Long = toInt(end) - toInt(start)

    override val maxValue: Option[Boolean] = Some(true)
    override val someValue: Boolean = true
    override val minValue: Option[Boolean] = Some(false)

    override def next(value: Boolean): Option[Boolean] = if (value) None else Some(true)
    override def previous(value: Boolean): Option[Boolean] = if (value) Some(false) else None
  }
  implicit object BooleanDD extends BooleanDiscreteDomain

  trait ByteDiscreteDomain extends DiscreteDomain[Byte] with ByteOrdering {
    override def distance(start: Byte, end: Byte): Long = end - start

    override val maxValue: Option[Byte] = Some(scala.Byte.MaxValue)
    override val someValue: Byte = 0
    override val minValue: Option[Byte] = Some(scala.Byte.MinValue)

    override def next(value: Byte): Option[Byte] = {
      if (value < maxValue.get) {
        Some((value + 1).asInstanceOf[Byte])
      } else {
        None
      }
    }
    override def previous(value: Byte): Option[Byte] = {
      if (value > minValue.get) {
        Some((value - 1).asInstanceOf[Byte])
      } else {
        None
      }
    }
  }
  implicit object ByteDD extends ByteDiscreteDomain

  trait CharDiscreteDomain extends DiscreteDomain[Char] with CharOrdering {
    override def distance(start: Char, end: Char): Long = end - start

    override val maxValue: Option[Char] = Some(scala.Char.MaxValue)
    override val someValue: Char = 'a'
    override val minValue: Option[Char] = Some(scala.Char.MinValue)

    override def next(value: Char): Option[Char] = {
      if (value < maxValue.get) {
        Some((value + 1).asInstanceOf[Char])
      } else {
        None
      }
    }
    override def previous(value: Char): Option[Char] = {
      if (value > minValue.get) {
        Some((value - 1).asInstanceOf[Char])
      } else {
        None
      }
    }
  }
  implicit object CharDD extends CharDiscreteDomain

  trait ShortDiscreteDomain extends DiscreteDomain[Short] with ShortOrdering {
    override def distance(start: Short, end: Short): Long = end - start

    override val maxValue: Option[Short] = Some(scala.Short.MaxValue)
    override val someValue: Short = 0
    override val minValue: Option[Short] = Some(scala.Short.MinValue)

    override def next(value: Short): Option[Short] = {
      if (value < maxValue.get) {
        Some((value + 1).asInstanceOf[Short])
      } else {
        None
      }
    }
    override def previous(value: Short): Option[Short] = {
      if (value > minValue.get) {
        Some((value - 1).asInstanceOf[Short])
      } else {
        None
      }
    }
  }
  implicit object ShortDD extends ShortDiscreteDomain

  trait IntDiscreteDomain extends DiscreteDomain[Int] with IntOrdering {
    override def distance(start: Int, end: Int): Long = end - start

    override val maxValue: Option[Int] = Some(scala.Int.MaxValue)
    override val someValue: Int = 0
    override val minValue: Option[Int] = Some(scala.Int.MinValue)

    override def next(value: Int): Option[Int] = if (value < maxValue.get) Some(value + 1) else None
    override def previous(value: Int): Option[Int] = if (value > minValue.get) Some(value - 1) else None
  }
  implicit object IntDD extends IntDiscreteDomain

  trait LongDiscreteDomain extends DiscreteDomain[Long] with LongOrdering {
    override def distance(start: Long, end: Long): Long = end - start

    override val maxValue: Option[Long] = Some(scala.Long.MaxValue)
    override val someValue: Long = 0
    override val minValue: Option[Long] = Some(scala.Long.MinValue)

    override def next(value: Long): Option[Long] = if (value < maxValue.get) Some(value + 1) else None
    override def previous(value: Long): Option[Long] = if (value > minValue.get) Some(value - 1) else None
  }
  implicit object LongDD extends LongDiscreteDomain

  trait BigIntDiscreteDomain extends DiscreteDomain[BigInt] with BigIntOrdering {
    override def distance(start: BigInt, end: BigInt): Long = {
      if (end - start > scala.Long.MaxValue) {
        scala.Long.MaxValue
      } else if (end - start < scala.Long.MinValue) {
        scala.Long.MinValue
      } else {
        (end - start).longValue()
      }
    }

    override val maxValue: Option[BigInt] = None
    override val someValue: BigInt = scala.math.BigInt(0)
    override val minValue: Option[BigInt] = None

    override def next(value: BigInt): Option[BigInt] = Some(value + 1)
    override def previous(value: BigInt): Option[BigInt] = Some(value - 1)
  }
  implicit object BigIntDD extends BigIntDiscreteDomain

}
