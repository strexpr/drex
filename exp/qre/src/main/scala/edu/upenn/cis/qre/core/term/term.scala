package edu.upenn.cis.qre.core.term

import scala.util.{Try, Success, Failure}

abstract class UntypedParameter
abstract class Parameter[+C] extends UntypedParameter
sealed case class WrapperParameter[+C, +T](t: T) extends Parameter[C]

abstract class Term[+C] {

  def parameters: Set[UntypedParameter]
  def eval: Try[C]
  def subst[PC](p: Parameter[PC], t: Term[PC]): Term[C]

  def isGround = parameters.isEmpty

}

class IncompleteTermException extends RuntimeException

sealed case class ConstantTerm[+C](c: C) extends Term[C] {
  override lazy val parameters = Set.empty.asInstanceOf[Set[UntypedParameter]]
  override lazy val eval = Success(c)
  override def subst[PC](p: Parameter[PC], t: Term[PC]) = this
}

sealed case class ParameterTerm[+C](p: Parameter[C]) extends Term[C] {
  require(p ne null)
  override lazy val parameters = Set(p).asInstanceOf[Set[UntypedParameter]]
  override lazy val eval = Failure(new IncompleteTermException)
  override def subst[PC](parameter: Parameter[PC], term: Term[PC]) =
    if (p == parameter) term.asInstanceOf[Term[C]] else this
}

abstract class BinaryTerm[C1, C2, C](val t1: Term[C1], val t2: Term[C2]) extends Term[C] {

  require(t1 ne null)
  require(t2 ne null)

  def op(c1: C1, c2: C2): C
  def constructTerm(t1: Term[C1], t2: Term[C2]): BinaryTerm[C1, C2, C]

  override lazy val parameters = t1.parameters union t2.parameters

  override lazy val eval = for {
    v1 <- t1.eval
    v2 <- t2.eval
  } yield op(v1, v2)

  override def subst[PC](p: Parameter[PC], t: Term[PC]) = {
    val t1p = t1.subst(p, t)
    val t2p = t2.subst(p, t)
    if ((t1p ne t1) || (t2p ne t2)) constructTerm(t1p, t2p) else this
  }

}

sealed case class MaxTerm(override val t1: Term[Double], override val t2: Term[Double])
extends BinaryTerm[Double, Double, Double](t1, t2) {
  override def op(c1: Double, c2: Double) = math.max(c1, c2)
  override def constructTerm(t1: Term[Double], t2: Term[Double]) = MaxTerm(t1, t2)
}

sealed case class MinTerm(override val t1: Term[Double], override val t2: Term[Double])
extends BinaryTerm[Double, Double, Double](t1, t2) {
  override def op(c1: Double, c2: Double) = math.min(c1, c2)
  override def constructTerm(t1: Term[Double], t2: Term[Double]) = MinTerm(t1, t2)
}

sealed case class MultTerm(override val t1: Term[Double], override val t2: Term[Double])
extends BinaryTerm[Double, Double, Double](t1, t2) {
  override def op(c1: Double, c2: Double) = c1 * c2
  override def constructTerm(t1: Term[Double], t2: Term[Double]) = MultTerm(t1, t2)
}

sealed case class PlusTerm(override val t1: Term[Double], override val t2: Term[Double])
extends BinaryTerm[Double, Double, Double](t1, t2) {
  override def op(c1: Double, c2: Double) = c1 + c2
  override def constructTerm(t1: Term[Double], t2: Term[Double]) = PlusTerm(t1, t2)
}

sealed case class ConcatTerm(override val t1: Term[String], override val t2: Term[String])
extends BinaryTerm[String, String, String](t1, t2) {
  override def op(c1: String, c2: String) = c1 + c2
  override def constructTerm(t1: Term[String], t2: Term[String]) = ConcatTerm(t1, t2)
}