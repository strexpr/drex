package edu.upenn.cis.qre
package util

case class UPair[A](a: A, b: A) {

  override def equals(obj: Any): Boolean = obj match {
    case that: UPair[A] => (this eq that) ||
                           (a == that.a && b == that.b) ||
                           (a == that.b && b == that.a)
    case _ => false
  }

  override val hashCode: Int = Math.min((a, b).hashCode(), (b, a).hashCode())

  def either(pred: A => Boolean): Boolean = pred(a) || pred(b)
  def both(pred: A => Boolean): Boolean = pred(a) && pred(b)

}
