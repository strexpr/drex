package edu.upenn.cis.qre.util.collect.range.discrete

import org.scalatest.FlatSpec

class DiscreteRangeSpec extends FlatSpec {

  val abc = DiscreteRange('a', 'c')

  "DiscreteRange('a', 'c')" should "accept 'a', 'b', and 'c'" in {
    assert(('a' to 'c').forall(abc))
  }

  it should "reject all characters other than 'a', 'b', and 'c'" in {
    assert(('d' to 'z').forall(ch => !abc(ch)))
    assert(('A' to 'Z').forall(ch => !abc(ch)))
    assert(('0' to '9').forall(ch => !abc(ch)))
  }

}
