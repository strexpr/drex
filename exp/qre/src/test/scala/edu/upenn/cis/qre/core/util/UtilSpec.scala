package edu.upenn.cis.qre.core.util

import org.scalatest.FlatSpec

class UtilSpec extends FlatSpec {

  val f: (Int, Int) => Int = (x, y) => y - x

  behavior of "Util.map2"

  it should "apply to each subsequent pair" in {
    assert(Util.map2(1 :: 2 :: 10 :: List(), f) == 1 :: 8 :: List())
  }

  it should "be empty for small lists" in {
    assert(Util.map2(List(), f).isEmpty)
    assert(Util.map2(3 :: List(), f).isEmpty)
  }

  it should "respect laziness" in {
    def gen(n: Int): Stream[Int] = n #:: gen(n + 1)
    val stream = gen(0)
    val diff = Util.map2(stream, f)
    assert(diff.take(10).forall(_ == 1))
  }

}