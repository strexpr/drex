package edu.upenn.cis.qre.core.symbol

import org.scalatest.FlatSpec

class EqualityPredSpec extends FlatSpec {

  behavior of "MatchAll"

  it should "be false if empty" in {
    val p0: EqualityPred[Int] = MatchAll(Set())
    assert(!p0.isSat)
    assert(!p0.apply(0))
    assert(!p0.apply(1))
    assert(!p0.apply(2))
  }

  it should "accept elements" in {
    val set: Set[Int] = Set(1, 2, 3, 4, 5)
    val p5: EqualityPred[Int] = MatchAll(set)
    assert(p5.isSat)
    assert(set.contains(p5.witness.get))
    for (v <- -10 until 10) {
      assert(set(v) == p5.apply(v))
    }
  }

  behavior of "RejectAll"

}