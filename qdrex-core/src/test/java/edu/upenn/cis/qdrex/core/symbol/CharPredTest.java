package edu.upenn.cis.qdrex.core.symbol;

import static edu.upenn.cis.qdrex.core.symbol.Predicate.and;
import static edu.upenn.cis.qdrex.core.symbol.Predicate.areEquivalent;
import static edu.upenn.cis.qdrex.core.symbol.Predicate.or;
import static edu.upenn.cis.qdrex.core.symbol.Predicate.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class CharPredTest {

    @Test
    public void solverTestBasic() {
        CharPred isDigit = CharPred.span('0','9');
        assertTrue(isDigit.test('5'));
        assertTrue(isDigit.test('0'));
        assertTrue(isDigit.test('9'));
        assertFalse(isDigit.test('a'));

        CharPred notIsDigit = not(isDigit);
        assertFalse(notIsDigit.test('5'));
        assertFalse(notIsDigit.test('0'));
        assertFalse(notIsDigit.test('9'));
        assertTrue(notIsDigit.test('a'));

        CharPred empty = and(isDigit, notIsDigit);
        CharPred full = or(isDigit, notIsDigit);

        assertFalse(empty.isSat());
        assertTrue(areEquivalent(full, CharPred.TRUE));

        assertTrue(CharPred.ALPHA.test('a'));
        assertFalse(CharPred.ALPHA.test('3'));
        assertTrue(CharPred.ALPHA_NUM.test('4'));
        assertTrue(CharPred.NUM.test('4'));
        assertFalse(CharPred.NUM.test('a'));
    }
	
    @Test
    public void test1() {
        CharPred p = CharPred.FALSE;
        assertFalse(p.test('a'));
        assertFalse(p.test('b'));
        assertFalse(p.test('A'));
        assertFalse(p.test('B'));
        assertFalse(p.test('0'));
        assertFalse(p.test('1'));
        assertFalse(p.test(' '));
        assertFalse(p.test('\t'));
        assertFalse(p.test('\n'));
    }

    @Test
    public void test2() {
        CharPred p = CharPred.of('a');
        assertTrue(p.test('a'));
        assertFalse(p.test('b'));
        assertFalse(p.test('A'));
        assertFalse(p.test('B'));
        assertFalse(p.test('0'));
        assertFalse(p.test('1'));
        assertFalse(p.test(' '));
        assertFalse(p.test('\t'));
        assertFalse(p.test('\n'));
    }

    @Test
    public void test3() {
        CharPred p = CharPred.span('a', 'z');
        assertTrue(p.test('a'));
        assertTrue(p.test('b'));
        assertFalse(p.test('A'));
        assertFalse(p.test('B'));
        assertFalse(p.test('0'));
        assertFalse(p.test('1'));
        assertFalse(p.test(' '));
        assertFalse(p.test('\t'));
        assertFalse(p.test('\n'));
    }


    @Test
    public void test7() {
        CharPred p = CharPred.ALPHA;
        assertTrue(p.test('a'));
        assertTrue(p.test('b'));
        assertTrue(p.test('A'));
        assertTrue(p.test('B'));
        assertFalse(p.test('0'));
        assertFalse(p.test('1'));
        assertFalse(p.test(' '));
        assertFalse(p.test('\t'));
        assertFalse(p.test('\n'));
    }

    @Test
    public void test8() {
        CharPred p = CharPred.NUM;
        assertFalse(p.test('a'));
        assertFalse(p.test('b'));
        assertFalse(p.test('A'));
        assertFalse(p.test('B'));
        assertTrue(p.test('0'));
        assertTrue(p.test('1'));
        assertFalse(p.test(' '));
        assertFalse(p.test('\t'));
        assertFalse(p.test('\n'));
    }

    @Test
    public void test9() {
        CharPred p = CharPred.ALPHA_NUM;
        assertTrue(p.test('a'));
        assertTrue(p.test('b'));
        assertTrue(p.test('A'));
        assertTrue(p.test('B'));
        assertTrue(p.test('0'));
        assertTrue(p.test('1'));
        assertFalse(p.test(' '));
        assertFalse(p.test('\t'));
        assertFalse(p.test('\n'));
    }

}
