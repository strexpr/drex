package edu.upenn.cis.qdrex.core.qre;

import edu.upenn.cis.qdrex.core.regex.Regex;
import static edu.upenn.cis.qdrex.core.regex.Regex.getInequivalenceWitness;
import edu.upenn.cis.qdrex.core.symbol.CharPred;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class TryElseTest {

    public static final QRE<CharPred, Character, String> ATOU =
            Base.string(CharPred.ALPHA, c -> Character.toUpperCase(c));
    public static final QRE<CharPred, Character, String> LTOU =
            Base.string(CharPred.LOWER_ALPHA, c -> Character.toUpperCase(c));
    public static final QRE<CharPred, Character, String> UTOL =
            Base.string(CharPred.UPPER_ALPHA, c -> Character.toLowerCase(c));

    public static final QRE<CharPred, Character, String> TE1 = new TryElse<>(LTOU, UTOL);

    @Test
    public void testFJAnalyze() {
        assertTrue(TE1.fjAnalyze().isUnambiguous());

        Regex<CharPred, Character> te1d = TE1.fjAnalyze().getDomainType();
        Regex<CharPred, Character> alpha = new edu.upenn.cis.qdrex.core.regex.Base<>(CharPred.ALPHA);
        assertFalse(getInequivalenceWitness(te1d, alpha).isPresent());
    }

}
