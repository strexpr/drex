package edu.upenn.cis.qdrex.core.util.collect.multisets;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BSTNodeTest {

    public void insert(List<Integer> values, int newValue, int newCount) {
        checkNotNull(values);
        checkArgument(newCount >= 0);

        for (int i = 0; i < newCount; i++) {
            values.add(newValue);
        }

        Collections.sort(values);
    }

    public void verify(BSTNode node, List<Integer> trueList) {
        long n = node.getTreeCount();
        assertEquals(trueList.size(), n);

        if (n > 0) {
            for (double o = 0; o <= 1; o += 0.1) {
                int index = (int)Math.floor(o * (n - 1));
                long expected = trueList.get(index);
                long actual = node.getRank(o);
                assertEquals(expected, actual);
            }
        }
    }

    @Test
    public void test1() {
        Random random = new Random(0);

        for (int test = 0; test < 100; test++) {
            List<Integer> trueList = new ArrayList<>();

            int value = random.nextInt();
            int count = random.nextInt(100);

            BSTNode node = new BSTNode(value, count);
            insert(trueList, value, count);
            verify(node, trueList);

            for (int i = 0; i < 100; i++) {
                value = random.nextInt();
                count = random.nextInt(100);

                node = node.insert(value, count);
                insert(trueList, value, count);
                verify(node, trueList);
            }
        }
    }

}
