package edu.upenn.cis.qdrex.core.symbol;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import static edu.upenn.cis.qdrex.core.util.collect.CollectionUtil.ALL_POSITIVE_INTEGERS;

public class EqualityPredTest {

    public void testPredTrue(EqualityPred<Integer> pred) {
        for (int i = 0; i < 10; i++) {
            assertTrue(pred.test(i));
        }
        assertTrue(pred.getWitness().isPresent());
    }

    public void testPredFalse(EqualityPred<Integer> pred) {
        for (int i = 0; i < 10; i++) {
            assertFalse(pred.test(i));
        }
        assertFalse(pred.getWitness().isPresent());
    }

    @Test
    public void test1() {
        EqualityPred<Integer> truePred = EqualityPred.truePred(ALL_POSITIVE_INTEGERS);
        EqualityPred<Integer> falsePred = EqualityPred.falsePred(ALL_POSITIVE_INTEGERS);

        testPredTrue(truePred);
        testPredFalse(falsePred);
        testPredFalse(truePred.not());
        testPredTrue(falsePred.not());
    }

    @Test
    public void test2() {
        EqualityPred<Integer> pred0 = EqualityPred.equalToPositiveInt(0);
        EqualityPred<Integer> predN0 = pred0.not();

        assertTrue(pred0.test(0));
        assertFalse(predN0.test(0));
        for (int i = 1; i < 10; i++) {
            assertFalse(pred0.test(i));
            assertTrue(predN0.test(i));
        }
        assertTrue(pred0.getWitness().isPresent());
        assertTrue(predN0.getWitness().isPresent());

        testPredTrue(pred0.or(predN0));
        testPredFalse(pred0.or(predN0).not());
        testPredFalse(pred0.and(predN0));
        testPredTrue(pred0.and(predN0).not());
    }

    @Test
    public void test3() {
        EqualityPred<Integer> pred0 = EqualityPred.equalToPositiveInt(0);
        EqualityPred<Integer> pred5 = EqualityPred.equalToPositiveInt(5);

        EqualityPred<Integer> predN0 = pred0.not();
        EqualityPred<Integer> predN5 = pred5.not();

        testPredFalse(pred0.and(pred5));
        assertTrue(Predicate.areEquivalent(pred0, pred0.and(predN5)));
        assertTrue(Predicate.areEquivalent(pred5, pred5.and(predN0)));
    }

}
