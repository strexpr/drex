package edu.upenn.cis.qdrex.core.util;

import java.util.NoSuchElementException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class EitherTest {

    @Test
    public void eitherTestL1() {
        Either<String, Void> l = Either.left("");

        assertTrue(l.isLeft());
        assertFalse(l.isRight());

        assertEquals("", l.getLeft());
    }

    @Test(expected = NoSuchElementException.class)
    public void eitherTestL2() {
        Either<String, Void> l = Either.left("");
        l.getRight();
    }

    @Test
    public void eitherTestR1() {
        Either<Void, String> r = Either.right("");

        assertFalse(r.isLeft());
        assertTrue(r.isRight());

        assertEquals("", r.getRight());
    }

    @Test(expected = NoSuchElementException.class)
    public void eitherTestR2() {
        Either<Void, String> r = Either.right("");
        assertEquals("", r.getLeft());
    }

}
