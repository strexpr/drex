package edu.upenn.cis.qdrex.core.qre;

import edu.upenn.cis.qdrex.core.qre.arith.Max;
import edu.upenn.cis.qdrex.core.qre.arith.Plus;
import edu.upenn.cis.qdrex.core.regex.Regex;
import static edu.upenn.cis.qdrex.core.regex.RegexTest.A_A_STAR_A;
import edu.upenn.cis.qdrex.core.regex.Star;
import edu.upenn.cis.qdrex.core.symbol.CharPred;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.ParameterTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.util.function.ConstantFunction;
import com.google.common.base.Optional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Test;

public class QRETest {

    @Test
    public void testRegexToQRE() {
        QRE<CharPred, Character, Integer> e = QREUtil.regexToConst(A_A_STAR_A, 3, Integer.class);

        Optional<Term<Integer>> res3 = e.eval('a', 'b', 'c');
        Term<Integer> res3Term = res3.get();
        assertEquals(3, res3Term.eval().longValue());

        Optional<Term<Integer>> res1 = e.eval('a');
        assertFalse(res1.isPresent());
    }

    @Test
    public void testStringLength() {
        QRE<CharPred, Character, Double> c1 = new Base<>(CharPred.TRUE, ConstantFunction.of(1.0), Double.class);
        QRE<CharPred, Character, Double> strlen = Plus.iterPlus(c1, false);

        assertFalse(strlen.eval().isPresent());
        assertEquals(3, strlen.eval('a', 'b', 'c').get().eval().longValue());
        assertEquals(7, strlen.eval('a', 'b', 'c', 'a', '0', '2', '2').get().eval().longValue());
    }

    @Test
    public void testLongestWord() {
        CharPred pw = CharPred.SPACES.not();

        Regex<CharPred, Character> spacePlus = Star.plus(new edu.upenn.cis.qdrex.core.regex.Base<>(CharPred.SPACES));
        Parameter<Double> p = Parameter.of(Double.class, 0);
        Term<Double> pt = new ParameterTerm(p);
        QRE<CharPred, Character, Double> spaceP = QREUtil.regexToTerm(spacePlus, pt, 0.0, Double.class);

        QRE<CharPred, Character, Double> c1 = new Base<>(pw, ConstantFunction.of(1.0), Double.class);
        QRE<CharPred, Character, Double> wlen1 = Plus.iterPlus(c1, false);
        QRE<CharPred, Character, Double> wlen = new ToSplit<>(wlen1, p, spaceP);
        assertEquals(3, wlen.eval('a', 'b', 'c', ' ').get().eval().longValue());

        QRE<CharPred, Character, Double> longestWord = Max.iterMax(wlen, false);
        assertEquals(3, longestWord.eval('a', 'b', 'c', ' ').get().eval().longValue());
        assertEquals(4, longestWord.eval('a', 'b', 'c', ' ', 'a', 'b', 'c', 'd', ' ').get().eval().longValue());
        assertEquals(4, longestWord.eval('a', 'b', 'c', 'd', ' ', 'a', 'b', 'c', ' ').get().eval().longValue());
    }

}
