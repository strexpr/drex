package edu.upenn.cis.qdrex.core.regex;

import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.qre.QREUtil;
import edu.upenn.cis.qdrex.core.symbol.CharPred;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.util.function.ConstantFunction;
import com.google.common.base.Optional;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class RegexTest {

    public static final Regex<CharPred, Character> A = new Base<>(CharPred.ALPHA);
    public static final Regex<CharPred, Character> A_A_STAR_A = A.concat(A).concat(A.star());
    public static final Regex<CharPred, Character> STAR_A_A_A = A.star().concat(A).concat(A);
    public static final Regex<CharPred, Character> UNION = A.star().union(A_A_STAR_A);

    @Test
    public void testA() {
        assertFalse(A.accepts());
        assertFalse(A.accepts('0'));
        assertTrue(A.accepts('a'));
        assertTrue(A.accepts('Y'));
        assertFalse(A.accepts('a', 'a'));
        assertFalse(A.accepts('a', 'c'));
        assertFalse(A.accepts('0', '9'));
        assertFalse(A.accepts('a', '0'));
        assertFalse(A.accepts('0', 'a'));
        assertFalse(A.accepts('a', 'C', 'w'));
    }

    @Test
    public void testAAstarA() {
        assertFalse(A_A_STAR_A.accepts());
        assertFalse(A_A_STAR_A.accepts('0'));
        assertFalse(A_A_STAR_A.accepts('a'));
        assertFalse(A_A_STAR_A.accepts('Y'));
        assertTrue(A_A_STAR_A.accepts('a', 'a'));
        assertTrue(A_A_STAR_A.accepts('a', 'c'));
        assertFalse(A_A_STAR_A.accepts('0', '9'));
        assertFalse(A_A_STAR_A.accepts('a', '0'));
        assertFalse(A_A_STAR_A.accepts('0', 'a'));
        assertTrue(A_A_STAR_A.accepts('a', 'C', 'w'));
    }

    @Test
    public void testStarAAA() {
        assertFalse(STAR_A_A_A.accepts());
        assertFalse(STAR_A_A_A.accepts('0'));
        assertFalse(STAR_A_A_A.accepts('a'));
        assertFalse(STAR_A_A_A.accepts('Y'));
        assertTrue(STAR_A_A_A.accepts('a', 'a'));
        assertTrue(STAR_A_A_A.accepts('a', 'c'));
        assertFalse(STAR_A_A_A.accepts('0', '9'));
        assertFalse(STAR_A_A_A.accepts('a', '0'));
        assertFalse(STAR_A_A_A.accepts('0', 'a'));
        assertTrue(STAR_A_A_A.accepts('a', 'C', 'w'));
    }

    @Test
    public void testUnion() {
        assertTrue(UNION.accepts());
        assertFalse(UNION.accepts('0'));
        assertTrue(UNION.accepts('a'));
        assertTrue(UNION.accepts('Y'));
        assertTrue(UNION.accepts('a', 'a'));
        assertTrue(UNION.accepts('a', 'c'));
        assertFalse(UNION.accepts('0', '9'));
        assertFalse(UNION.accepts('a', '0'));
        assertFalse(UNION.accepts('0', 'a'));
        assertTrue(UNION.accepts('a', 'C', 'w'));
    }

    @Test
    public void testAmbiguityCheck() {
        assertFalse(A.getAmbiguousWitness().isPresent());
        assertFalse(A_A_STAR_A.getAmbiguousWitness().isPresent());
        assertFalse(STAR_A_A_A.getAmbiguousWitness().isPresent());
        assertTrue(UNION.getAmbiguousWitness().isPresent());
    }

    @Test
    public void testEquivalenceCheck() {
        assertFalse(Regex.getInequivalenceWitness(A, A).isPresent());
        assertTrue(Regex.getInequivalenceWitness(A, A_A_STAR_A).isPresent());
        assertTrue(Regex.getInequivalenceWitness(A, STAR_A_A_A).isPresent());
        assertTrue(Regex.getInequivalenceWitness(A, UNION).isPresent());

        assertTrue(Regex.getInequivalenceWitness(A_A_STAR_A, A).isPresent());
        assertFalse(Regex.getInequivalenceWitness(A_A_STAR_A, A_A_STAR_A).isPresent());
        assertFalse(Regex.getInequivalenceWitness(A_A_STAR_A, STAR_A_A_A).isPresent());
        assertTrue(Regex.getInequivalenceWitness(A_A_STAR_A, UNION).isPresent());

        assertTrue(Regex.getInequivalenceWitness(STAR_A_A_A, A).isPresent());
        assertFalse(Regex.getInequivalenceWitness(STAR_A_A_A, A_A_STAR_A).isPresent());
        assertFalse(Regex.getInequivalenceWitness(STAR_A_A_A, STAR_A_A_A).isPresent());
        assertTrue(Regex.getInequivalenceWitness(STAR_A_A_A, UNION).isPresent());

        assertTrue(Regex.getInequivalenceWitness(UNION, A).isPresent());
        assertTrue(Regex.getInequivalenceWitness(UNION, A_A_STAR_A).isPresent());
        assertTrue(Regex.getInequivalenceWitness(UNION, STAR_A_A_A).isPresent());
        assertFalse(Regex.getInequivalenceWitness(UNION, UNION).isPresent());
    }

}
