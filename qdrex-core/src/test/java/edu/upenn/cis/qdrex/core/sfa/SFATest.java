package edu.upenn.cis.qdrex.core.sfa;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Table;
import edu.upenn.cis.qdrex.core.symbol.CharPred;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class SFATest {

    public static SFA<CharPred, Character> buildSFAAlpha() {
        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);
        ImmutableSet<State> states = ImmutableSet.of(q0, q1);

        ImmutableTable<State, CharPred, ImmutableSet<State>> transitions =
                ImmutableTable.of(q0, CharPred.ALPHA, ImmutableSet.of(q1));

        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = ImmutableMap.of();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    @Test
    public void test1() {
        SFA<CharPred, Character> sfaAlpha = buildSFAAlpha();

        assertTrue(sfaAlpha.accepts('a'));
        assertTrue(sfaAlpha.accepts('w'));

        assertFalse(sfaAlpha.accepts());
        assertFalse(sfaAlpha.accepts('0'));
        assertFalse(sfaAlpha.accepts('a', 'a'));
        assertFalse(sfaAlpha.accepts('a', 'b'));
        assertFalse(sfaAlpha.accepts('.', 'w', 'z'));

        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);

        assertEquals(ImmutableSet.of(q0), sfaAlpha.epsilonClosure(q0));
        assertEquals(ImmutableSet.of(q1), sfaAlpha.epsilonClosure(q1));

        assertFalse(sfaAlpha.getAmbiguousWitness().isPresent());

        assertFalse(SFA.getInequivalenceWitness(sfaAlpha, sfaAlpha).isPresent());
    }

    public static SFA<CharPred, Character> buildSFAAlphaXDigit1() {
        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);
        State q2 = State.of(2, false, true);
        ImmutableSet<State> states = ImmutableSet.of(q0, q1, q2);

        Table<State, CharPred, ImmutableSet<State>> trans = HashBasedTable.create();
        trans.put(q0, CharPred.ALPHA, ImmutableSet.of(q1));
        trans.put(q0, CharPred.XDIGIT, ImmutableSet.of(q2));
        ImmutableTable<State, CharPred, ImmutableSet<State>> transitions =
                ImmutableTable.copyOf(trans);

        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = ImmutableMap.of();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    @Test
    public void test2() {
        SFA<CharPred, Character> sfaAlphaXDigit1 = buildSFAAlphaXDigit1();

        assertTrue(sfaAlphaXDigit1.accepts('a'));
        assertTrue(sfaAlphaXDigit1.accepts('w'));
        assertTrue(sfaAlphaXDigit1.accepts('0'));

        assertFalse(sfaAlphaXDigit1.accepts());
        assertFalse(sfaAlphaXDigit1.accepts('a', 'a'));
        assertFalse(sfaAlphaXDigit1.accepts('a', 'b'));
        assertFalse(sfaAlphaXDigit1.accepts('.', 'w', 'z'));

        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);
        State q2 = State.of(2, false, true);

        assertEquals(ImmutableSet.of(q0), sfaAlphaXDigit1.epsilonClosure(q0));
        assertEquals(ImmutableSet.of(q1), sfaAlphaXDigit1.epsilonClosure(q1));
        assertEquals(ImmutableSet.of(q2), sfaAlphaXDigit1.epsilonClosure(q2));

        assertTrue(sfaAlphaXDigit1.getAmbiguousWitness().isPresent());

        assertFalse(SFA.getInequivalenceWitness(sfaAlphaXDigit1, sfaAlphaXDigit1).isPresent());
        SFA<CharPred, Character> sfaAlpha = buildSFAAlpha();
        assertTrue(SFA.getInequivalenceWitness(sfaAlpha, sfaAlphaXDigit1).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaAlphaXDigit1, sfaAlpha).isPresent());
    }

    public static SFA<CharPred, Character> buildSFAAlphaXDigit2() {
        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);
        ImmutableSet<State> states = ImmutableSet.of(q0, q1);

        Table<State, CharPred, ImmutableSet<State>> trans = HashBasedTable.create();
        trans.put(q0, CharPred.ALPHA, ImmutableSet.of(q1));
        trans.put(q0, CharPred.XDIGIT, ImmutableSet.of(q1));
        ImmutableTable<State, CharPred, ImmutableSet<State>> transitions =
                ImmutableTable.copyOf(trans);

        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = ImmutableMap.of();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    @Test
    public void test3() {
        SFA<CharPred, Character> sfaAlphaXDigit2 = buildSFAAlphaXDigit2();

        assertTrue(sfaAlphaXDigit2.accepts('a'));
        assertTrue(sfaAlphaXDigit2.accepts('w'));
        assertTrue(sfaAlphaXDigit2.accepts('0'));

        assertFalse(sfaAlphaXDigit2.accepts());
        assertFalse(sfaAlphaXDigit2.accepts('a', 'a'));
        assertFalse(sfaAlphaXDigit2.accepts('a', 'b'));
        assertFalse(sfaAlphaXDigit2.accepts('.', 'w', 'z'));

        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);

        assertEquals(ImmutableSet.of(q0), sfaAlphaXDigit2.epsilonClosure(q0));
        assertEquals(ImmutableSet.of(q1), sfaAlphaXDigit2.epsilonClosure(q1));

        assertTrue(sfaAlphaXDigit2.getAmbiguousWitness().isPresent());

        assertFalse(SFA.getInequivalenceWitness(sfaAlphaXDigit2, sfaAlphaXDigit2).isPresent());
        SFA<CharPred, Character> sfaAlpha = buildSFAAlpha();
        SFA<CharPred, Character> sfaAlphaXDigit1 = buildSFAAlphaXDigit1();
        assertTrue(SFA.getInequivalenceWitness(sfaAlpha, sfaAlphaXDigit2).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaAlphaXDigit2, sfaAlpha).isPresent());
        assertFalse(SFA.getInequivalenceWitness(sfaAlphaXDigit1, sfaAlphaXDigit2).isPresent());
        assertFalse(SFA.getInequivalenceWitness(sfaAlphaXDigit2, sfaAlphaXDigit1).isPresent());
    }

    public static SFA<CharPred, Character> buildSFAXDigit() {
        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);
        ImmutableSet<State> states = ImmutableSet.of(q0, q1);

        ImmutableTable<State, CharPred, ImmutableSet<State>> transitions =
                ImmutableTable.of(q0, CharPred.XDIGIT, ImmutableSet.of(q1));

        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = ImmutableMap.of();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    public static SFA<CharPred, Character> buildSFAAlphaXDigit3() {
        SFA<CharPred, Character> sfaAlpha = buildSFAAlpha();
        SFA<CharPred, Character> sfaXDigit = buildSFAXDigit();
        return SFAUtil.buildUnionSFA(sfaAlpha, sfaXDigit);
    }

    @Test
    public void test4() {
        SFA<CharPred, Character> sfaAlphaXDigit3 = buildSFAAlphaXDigit3();

        assertTrue(sfaAlphaXDigit3.accepts('a'));
        assertTrue(sfaAlphaXDigit3.accepts('w'));
        assertTrue(sfaAlphaXDigit3.accepts('0'));

        assertFalse(sfaAlphaXDigit3.accepts());
        assertFalse(sfaAlphaXDigit3.accepts('a', 'a'));
        assertFalse(sfaAlphaXDigit3.accepts('a', 'b'));
        assertFalse(sfaAlphaXDigit3.accepts('.', 'w', 'z'));

        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);

        assertEquals(ImmutableSet.of(q0), sfaAlphaXDigit3.epsilonClosure(q0));
        assertEquals(ImmutableSet.of(q1), sfaAlphaXDigit3.epsilonClosure(q1));

        assertTrue(sfaAlphaXDigit3.getAmbiguousWitness().isPresent());

        assertFalse(SFA.getInequivalenceWitness(sfaAlphaXDigit3, sfaAlphaXDigit3).isPresent());
        SFA<CharPred, Character> sfaAlpha = buildSFAAlpha();
        SFA<CharPred, Character> sfaAlphaXDigit1 = buildSFAAlphaXDigit1();
        SFA<CharPred, Character> sfaAlphaXDigit2 = buildSFAAlphaXDigit2();
        assertTrue(SFA.getInequivalenceWitness(sfaAlpha, sfaAlphaXDigit3).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaAlphaXDigit3, sfaAlpha).isPresent());
        assertFalse(SFA.getInequivalenceWitness(sfaAlphaXDigit1, sfaAlphaXDigit3).isPresent());
        assertFalse(SFA.getInequivalenceWitness(sfaAlphaXDigit3, sfaAlphaXDigit1).isPresent());
        assertFalse(SFA.getInequivalenceWitness(sfaAlphaXDigit2, sfaAlphaXDigit3).isPresent());
        assertFalse(SFA.getInequivalenceWitness(sfaAlphaXDigit3, sfaAlphaXDigit2).isPresent());
    }

    public static SFA<CharPred, Character> buildSFAXDigitStar() {
        return SFAUtil.buildStarSFA(buildSFAXDigit());
    }

    @Test
    public void test5() {
        SFA<CharPred, Character> sfaXDigitStar = buildSFAXDigitStar();

        assertTrue(sfaXDigitStar.accepts('a'));
        assertTrue(sfaXDigitStar.accepts('0'));
        assertTrue(sfaXDigitStar.accepts());
        assertTrue(sfaXDigitStar.accepts('a', 'a'));
        assertTrue(sfaXDigitStar.accepts('a', 'b'));

        assertFalse(sfaXDigitStar.accepts('w'));
        assertFalse(sfaXDigitStar.accepts('.', 'w', 'z'));
        assertFalse(sfaXDigitStar.accepts('a', '0', 'z'));

        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);

        assertEquals(ImmutableSet.of(q0), sfaXDigitStar.epsilonClosure(q0));
        assertEquals(ImmutableSet.of(q1), sfaXDigitStar.epsilonClosure(q1));

        assertFalse(sfaXDigitStar.getAmbiguousWitness().isPresent());

        assertFalse(SFA.getInequivalenceWitness(sfaXDigitStar, sfaXDigitStar).isPresent());
        SFA<CharPred, Character> sfaAlpha = buildSFAAlpha();
        SFA<CharPred, Character> sfaAlphaXDigit1 = buildSFAAlphaXDigit1();
        SFA<CharPred, Character> sfaAlphaXDigit2 = buildSFAAlphaXDigit2();
        SFA<CharPred, Character> sfaAlphaXDigit3 = buildSFAAlphaXDigit3();
        assertTrue(SFA.getInequivalenceWitness(sfaAlpha, sfaXDigitStar).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaXDigitStar, sfaAlpha).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaAlphaXDigit1, sfaXDigitStar).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaXDigitStar, sfaAlphaXDigit1).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaAlphaXDigit2, sfaXDigitStar).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaXDigitStar, sfaAlphaXDigit2).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaAlphaXDigit3, sfaXDigitStar).isPresent());
        assertTrue(SFA.getInequivalenceWitness(sfaXDigitStar, sfaAlphaXDigit3).isPresent());
        assertFalse(SFA.getInequivalenceWitness(sfaXDigitStar, sfaXDigitStar).isPresent());
        assertFalse(SFA.getInequivalenceWitness(sfaXDigitStar, sfaXDigitStar).isPresent());
    }

}
