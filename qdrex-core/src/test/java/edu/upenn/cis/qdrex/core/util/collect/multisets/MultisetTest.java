package edu.upenn.cis.qdrex.core.util.collect.multisets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class MultisetTest {

    public void insert(List<Double> values, double newValue) {
        values.add(newValue);
        Collections.sort(values);
    }

    public void verify(Multiset m, List<Double> trueList) {
        long n = m.size();
        assertEquals(trueList.size(), n);

        if (n > 0) {
            for (double o = 0; o <= 1; o += 0.1) {
                int index = (int)Math.floor(o * (n - 1));
                double expected = trueList.get(index);
                double actual = m.rank(o);

                double bound1 = (1 - Approx.getEpsilon()) * expected;
                double bound2 = (1 + Approx.getEpsilon()) * expected;

                assertTrue(Math.min(bound1, bound2) <= actual);
                assertTrue(actual <= Math.max(bound1, bound2));
            }
        }
    }

    @Test
    public void test1() {
        Random random = new Random(0);

        for (int test = 0; test < 100; test++) {
            List<Double> trueList = new ArrayList<>();

            double value = random.nextDouble() * 1000;
            if (random.nextBoolean()) {
                value = -value;
            }

            Multiset m = Multiset.EMPTY;
            m = m.insert(value);
            insert(trueList, value);
            verify(m, trueList);

            for (int i = 0; i < 100; i++) {
                value = random.nextDouble() * 1000;
                if (random.nextBoolean()) {
                    value = -value;
                }

                m = m.insert(value);
                insert(trueList, value);
                verify(m, trueList);
            }
        }
    }

}
