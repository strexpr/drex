package edu.upenn.cis.qdrex.core.regex;

import edu.upenn.cis.qdrex.core.symbol.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import edu.upenn.cis.qdrex.core.sfa.SFA;
import java.io.Serializable;
import com.google.common.base.Optional;

public abstract class Regex<P extends Predicate<P, T>, T> implements Serializable {

    public enum Type {
        BASE, EPSILON, BOTTOM, UNION, CONCAT, STAR
    }

    public final Type type;

    private /* mutable */ Optional<ImmutableList<T>> ambiguousWitness;

    public Regex(Type type) {
        this.type = type;
        this.ambiguousWitness = null;
    }

    public abstract int size();
    public abstract <R> R invokeVisitor(RegexVisitor<P, T, R> visitor);
    public abstract SFA<P, T> buildSFA();

    @Override
    public abstract String toString();

    public Regex<P, T> concat(Regex<P, T> other) {
        return Concat.of(this, other);
    }

    public Regex<P, T> union(Regex<P, T> other) {
        return Union.of(this, other);
    }

    public Regex<P, T> star() {
        return new Star<>(this);
    }

    public boolean accepts(T... ts) {
        return buildSFA().accepts(ts);
    }

    public boolean accepts(Iterable<? extends T> w) {
        return buildSFA().accepts(w);
    }

    public Optional<ImmutableList<T>> getAmbiguousWitness() {
        if (ambiguousWitness == null) {
            SFA<P, T> sfa = buildSFA();
            ambiguousWitness = sfa.getAmbiguousWitness();
        }
        return ambiguousWitness;
    }

    public static <P extends Predicate<P, T>, T> Optional<ImmutableList<T>>
    getInequivalenceWitness(Regex<P, T> r1, Regex<P, T> r2) {
        SFA<P, T> sfa1 = r1.buildSFA();
        SFA<P, T> sfa2 = r2.buildSFA();
        return SFA.getInequivalenceWitness(sfa1, sfa2);
    }

    public static <P extends Predicate<P, T>, T> ImmutableSet<P>
    collectPredicates(Regex<P, T> r) {
        CollectPredicatesVisitor<P, T> visitor = new CollectPredicatesVisitor<>();
        return visitor.visit(r);
    }

    public static class CollectPredicatesVisitor<P extends Predicate<P, T>, T>
    extends RegexVisitor<P, T, ImmutableSet<P>> {

        @Override
        public ImmutableSet<P> visitBase(Base<P, T> base) {
            return ImmutableSet.of(base.pred);
        }

        @Override
        public ImmutableSet<P> visitEpsilon(Epsilon<P, T> epsilon) {
            return ImmutableSet.of();
        }

        @Override
        public ImmutableSet<P> visitBottom(Bottom<P, T> bottom) {
            return ImmutableSet.of();
        }

        @Override
        public ImmutableSet<P> visitUnion(Union<P, T> union) {
            ImmutableSet<P> r1Ans = visit(union.r1);
            ImmutableSet<P> r2Ans = visit(union.r2);
            return Sets.union(r1Ans, r2Ans).immutableCopy();
        }

        @Override
        public ImmutableSet<P> visitConcat(Concat<P, T> concat) {
            ImmutableSet<P> r1Ans = visit(concat.r1);
            ImmutableSet<P> r2Ans = visit(concat.r2);
            return Sets.union(r1Ans, r2Ans).immutableCopy();
        }

        @Override
        public ImmutableSet<P> visitStar(Star<P, T> star) {
            return visit(star.r);
        }

    }

}
