package edu.upenn.cis.qdrex.core.term.arith;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.IncompleteTermException;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import static com.google.common.base.Preconditions.checkNotNull;

public class MTermSingleton extends Term<Multiset> {

    public final Term<Double> element;

    private MTermSingleton(Term<Double> element) {
        super(Multiset.class);
        this.element = checkNotNull(element);
    }

    public static Term<Multiset> of(Term<Double> element) {
        if (element.isGround()) {
            Multiset m = Multiset.of(element.eval());
            return ConstantTerm.of(m);
        } else {
            return new MTermSingleton(element);
        }
    }

    @Override
    public boolean isGround() {
        return element.isGround();
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        return element.getParameters();
    }

    @Override
    public Multiset eval() throws IncompleteTermException {
        return Multiset.of(element.eval());
    }

    @Override
    public <PC> Term<Multiset> subst(Parameter<PC> p, Term<PC> t) {
        Term<Double> elementPrime = element.subst(p, t);
        return elementPrime != element ? MTermSingleton.of(elementPrime) : this;
    }

    @Override
    public int getBytes() {
        return 4 + element.getBytes();
    }

    @Override
    public String toString() {
        return String.format("(MTermSingleton %s)", element);
    }

}
