package edu.upenn.cis.qdrex.core.qre.arith;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import edu.upenn.cis.qdrex.core.qre.Iter;
import edu.upenn.cis.qdrex.core.qre.UnaryOp;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermRank;
import edu.upenn.cis.qdrex.core.term.arith.MTermSingleton;
import edu.upenn.cis.qdrex.core.term.arith.MTermUnion;
import static com.google.common.base.Preconditions.checkArgument;

public class Rank<P extends Predicate<P, T>, T> extends UnaryOp<P, T, Multiset, Double> {

    public final double o;

    public Rank(QRE<P, T, Multiset> f, double o) {
        super(f, Double.class);
        checkArgument(0 <= o && o <= 1);
        this.o = o;
    }

    @Override
    public Term<Double> op(Term<Multiset> tf) {
        return ITermRank.of(tf, o);
    }

    @Override
    public ImmutableSet<Parameter> getParametersInt() {
        return f.getParameters();
    }

    @Override
    public String toString() {
        return String.format("(Rank %s %f)", f, o);
    }

    public static <P extends Predicate<P, T>, T> IterRank<P, T> iterRank(QRE<P, T, Double> f, double o) {
        return new IterRank(f, o);
    }

    public static class IterRank<P extends Predicate<P, T>, T> extends Iter<P, T, Multiset, Double, Double> {

        public final double o;

        public IterRank(QRE<P, T, Double> f, double o) {
            super(ConstantTerm.of(Multiset.EMPTY), f, false, Double.class);
            this.o = o;
            checkArgument(0 <= o && o <= 1);
        }

        @Override
        public Term<Multiset> combine(Term<Multiset> tr, Term<Double> tf) {
            return MTermUnion.of(tr, MTermSingleton.of(tf));
        }

        @Override
        public Term<Double> finalize(Term<Multiset> tr) {
            return ITermRank.of(tr, o);
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            return f.getParameters();
        }

        @Override
        public String toString() {
            return String.format("(Rank.IterRank %s)", f);
        }

    }

}
