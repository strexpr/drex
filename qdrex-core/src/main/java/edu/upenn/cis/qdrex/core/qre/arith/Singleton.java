package edu.upenn.cis.qdrex.core.qre.arith;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import edu.upenn.cis.qdrex.core.qre.UnaryOp;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.MTermSingleton;

public class Singleton<P extends Predicate<P, T>, T> extends UnaryOp<P, T, Double, Multiset> {

    public Singleton(QRE<P, T, Double> f) {
        super(f, Multiset.class);
    }

    @Override
    public Term<Multiset> op(Term<Double> tf) {
        return MTermSingleton.of(tf);
    }

    @Override
    public ImmutableSet<Parameter> getParametersInt() {
        return f.getParameters();
    }

    @Override
    public String toString() {
        return String.format("(Singleton %s)", f);
    }

}
