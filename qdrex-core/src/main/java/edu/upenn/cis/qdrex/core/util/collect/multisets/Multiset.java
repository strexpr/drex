package edu.upenn.cis.qdrex.core.util.collect.multisets;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import java.io.Serializable;

public class Multiset implements Serializable {

    public final BSTNode negHist; // approximate histogram (negative)
    public final int zeroCount; // number of zeros
    public final BSTNode posHist; // approximate histogram (positive)
    public final double sum;

    private Multiset(BSTNode negHist, int zeroCount, BSTNode posHist, double sum) {
        checkArgument(zeroCount >= 0);
        this.negHist = checkNotNull(negHist);
        this.zeroCount = zeroCount;
        this.posHist = checkNotNull(posHist);
        this.sum = sum;
    }

    public static final Multiset EMPTY = new Multiset(BSTNode.EMPTY, 0, BSTNode.EMPTY, 0);

    public static Multiset of(double value) {
        if (value < 0) {
            BSTNode negHist = new BSTNode(Approx.valToIndex(-value), 1);
            return new Multiset(negHist, 0, BSTNode.EMPTY, value);
        } else if (value == 0) {
            return new Multiset(BSTNode.EMPTY, 1, BSTNode.EMPTY, 0);
        } else {
            BSTNode posHist = new BSTNode(Approx.valToIndex(value), 1);
            return new Multiset(BSTNode.EMPTY, 0, posHist, value);
        }
    }

    public boolean isEmpty() {
        return size() == 0;
    }

    public Multiset insert(double value) {
        if (value < 0) {
            int index = Approx.valToIndex(-value);
            BSTNode newNegHist = negHist.insert(index);
            return new Multiset(newNegHist, zeroCount, posHist, sum + value);
        } else if (value == 0) {
            return new Multiset(negHist, zeroCount + 1, posHist, sum);
        } else {
            int index = Approx.valToIndex(value);
            BSTNode newPosHist = posHist.insert(index);
            return new Multiset(negHist, zeroCount, newPosHist, sum + value);
        }
    }

    public Multiset merge(Multiset mu) {
        if (mu.isEmpty()) {
            return this;
        } else if (this.isEmpty()) {
            return mu;
        }

        BSTNode newNegHist = negHist.merge(mu.negHist);
        BSTNode newPosHist = posHist.merge(mu.posHist);
        return new Multiset(newNegHist, zeroCount + mu.zeroCount, newPosHist, sum + mu.sum);
    }

    public int size() {
        return negHist.getTreeCount() + zeroCount + posHist.getTreeCount();
    }

    public double average() {
        int size = size();
        checkState(size > 0);
        return sum / size;
    }

    public double rank(double o) {
        int size = size();
        int index = (int)Math.floor(o * (size - 1));

        if (index < negHist.getTreeCount()) {
            int indexNeg = negHist.getTreeCount() - 1 - index;
            return -Approx.indexToVal(negHist.getElement(indexNeg));
        }
        index = index - negHist.getTreeCount();

        if (index < this.zeroCount) {
            return 0;
        }
        index = index - zeroCount;

        return Approx.indexToVal(posHist.getElement(index));
    }

    public int getBytes() {
        return 4 + 4 + 4 + 8 + negHist.getBytes() + posHist.getBytes();
    }

    @Override
    public String toString() {
        return String.format("(Multiset [sum: %f] [count: %d] negHist: %s "
                + "[zeroCount: %d] posHist: %s)", sum, posHist.getTreeCount(),
                negHist, zeroCount, posHist);
    }

}
