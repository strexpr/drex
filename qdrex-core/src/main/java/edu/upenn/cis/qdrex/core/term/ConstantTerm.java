package edu.upenn.cis.qdrex.core.term;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import static com.google.common.base.Preconditions.checkNotNull;

public class ConstantTerm<C> extends Term<C> {

    public final C value;

    public ConstantTerm(C value, Class<C> outType) {
        super(outType);
        this.value = checkNotNull(value);
    }

    public static ConstantTerm<Double> of(double v) {
        return new ConstantTerm<>(v, Double.class);
    }

    public static ConstantTerm<Multiset> of(Multiset m) {
        return new ConstantTerm<>(m, Multiset.class);
    }

    @Override
    public boolean isGround() {
        return true;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        return ImmutableSet.of();
    }

    @Override
    public C eval() throws IncompleteTermException {
        return value;
    }

    @Override
    public <PC> Term<C> subst(Parameter<PC> p, Term<PC> t) {
        return this;
    }

    @Override
    public int getBytes() {
        if (value instanceof Multiset) {
            return ((Multiset)value).getBytes();
        } else {
            return 8;
        }
    }

    @Override
    public String toString() {
        return value.toString();
    }

}
