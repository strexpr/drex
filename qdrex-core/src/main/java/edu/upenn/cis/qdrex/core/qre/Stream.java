package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.fj.Evaluator;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.fj.StreamEvaluator;
import edu.upenn.cis.qdrex.core.regex.Regex;
import edu.upenn.cis.qdrex.core.regex.Star;
import com.google.common.base.Optional;

public class Stream<Pf extends Predicate<Pf, T>, Pg extends Predicate<Pg, Cf>, T, Cf, Cg> extends QRE<Pf, T, Cg> {

    public final QRE<Pf, T, Cf> f;
    public final QRE<Pg, Cf, Cg> g;

    private /* mutable */ int size;
    private /* mutable */ AnalysisResult<Pf, T, Cg> analysisResult;

    public Stream(QRE<Pf, T, Cf> f, QRE<Pg, Cf, Cg> g) {
        super(Type.STREAM, g.outType);
        checkArgument(f.getParameters().isEmpty());
        this.f = f;
        this.g = g;
        this.size = -1;
    }

    @Override
    public int size() {
        if (size <= 0) {
            size = 1 + f.size() + g.size();
            verify(size > 0);
        }
        return size;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        return g.getParameters();
    }

    @Override
    public AnalysisResult<Pf, T, Cg> fjAnalyze() {
        if (analysisResult == null) {
            analysisResult = fjAnalyzeInt();
            verifyNotNull(analysisResult);
        }
        return analysisResult;
    }

    public AnalysisResult<Pf, T, Cg> fjAnalyzeInt() {
        AnalysisResult<Pf, T, Cf> af = f.fjAnalyze();
        if (!af.isUnambiguous()) {
            return AnalysisResult.subexprError(this, af.getAnalysisError());
        }

        Regex<Pf, T> rf = af.getDomainType();
        ImmutableSet<Pf> rfPredicates = Regex.collectPredicates(rf);
        if (rfPredicates.isEmpty()) {
            String msg = String.format("Could not find predicates in subexpression %s", f);
            return AnalysisResult.baseError(this, msg);
        }
        Pf somePf = rfPredicates.iterator().next();
        Pf truePf = somePf.or(somePf.not());

        if (rf.accepts(/* epsilon */)) {
            String msg = String.format("Subexpression %s accepts the empty data stream", f);
            return AnalysisResult.baseError(this, msg);
        }

        AnalysisResult<Pg, Cf, Cg> ag = g.fjAnalyze();
        if (!ag.isUnambiguous()) {
            return AnalysisResult.subexprError(this, ag.getAnalysisError());
        }

        Regex<Pg, Cf> rg = ag.getDomainType();
        ImmutableSet<Pg> rgPredicates = Regex.collectPredicates(rg);
        if (rgPredicates.isEmpty()) {
            String msg = String.format("Could not find predicates in subexpression %s", g);
            return AnalysisResult.baseError(this, msg);
        }
        Pg somePg = rgPredicates.iterator().next();
        Pg truePg = somePg.or(somePg.not());
        Regex<Pg, Cf> rgRef = Star.of(new edu.upenn.cis.qdrex.core.regex.Base<>(truePg));
        Optional<ImmutableList<Cf>> badGInput = Regex.getInequivalenceWitness(rg, rgRef);

        if (badGInput.isPresent()) {
            String msg = String.format("Subexpression %s undefined on input %s", g, badGInput.get());
            return AnalysisResult.baseError(this, msg);
        }

        Regex<Pf, T> pfStar = new Star<>(new edu.upenn.cis.qdrex.core.regex.Base<>(truePf));
        return AnalysisResult.unambiguous(pfStar);
    }

    @Override
    public Evaluator<Pf, T, Cg> buildEvaluator() {
        return new StreamEvaluator<>(this);
    }

    @Override
    public String toString() {
        return String.format("(Stream %s %s)", f, g);
    }

}
