package edu.upenn.cis.qdrex.core.regex;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;
import edu.upenn.cis.qdrex.core.sfa.SFA;
import edu.upenn.cis.qdrex.core.sfa.SFAUtil;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class Union<P extends Predicate<P, T>, T> extends Regex<P, T> {

    public final Regex<P, T> r1, r2;

    private /* mutable */ int size;
    private /* mutable */ SFA<P, T> sfa;

    private Union(Regex<P, T> r1, Regex<P, T> r2) {
        super(Type.UNION);
        this.r1 = checkNotNull(r1);
        this.r2 = checkNotNull(r2);
        this.size = -1;
        this.sfa = null;
    }

    @Override
    public int size() {
        if (size <= 0) {
            size = 1 + r1.size() + r2.size();
            verify(size > 0);
        }
        return size;
    }

    public static <P extends Predicate<P, T>, T> Regex<P, T> of(Regex<P, T>... rs) {
        Regex<P, T> ans = new Bottom<>();
        for (Regex<P, T> r : rs) {
            ans = ans instanceof Bottom ? r : new Union<>(ans, r);
        }
        return ans;
    }

    @Override
    public <R> R invokeVisitor(RegexVisitor<P, T, R> visitor) {
        return visitor.visitUnion(this);
    }

    @Override
    public SFA<P, T> buildSFA() {
        if (sfa == null) {
            SFA<P, T> sfa1 = r1.buildSFA();
            SFA<P, T> sfa2 = r2.buildSFA();
            sfa = SFAUtil.buildUnionSFA(sfa1, sfa2);
            verifyNotNull(sfa);
        }
        return sfa;
    }

    @Override
    public String toString() {
        return String.format("(Union %s %s)", r1, r2);
    }

}
