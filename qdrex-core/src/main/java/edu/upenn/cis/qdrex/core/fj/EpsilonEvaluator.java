package edu.upenn.cis.qdrex.core.fj;

import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.qre.Epsilon;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import java.util.HashSet;
import java.util.Set;

public class EpsilonEvaluator<P extends Predicate<P, T>, T, C> extends Evaluator<P, T, C> {

    public final Epsilon<P, T, C> epsilon;
    private final Set<Integer> th;

    public EpsilonEvaluator(Epsilon<P, T, C> epsilon) {
        super(epsilon);
        this.epsilon = epsilon;
        this.th = new HashSet<>();
    }

    @Override
    public void resetHandler() {
        th.clear();
    }

    @Override
    public ImmutableList<Response<P, T, C>> startHandler(int index) {
        th.add(index);
        return ImmutableList.of(Response.result(this, index, epsilon.t));
    }

    @Override
    public ImmutableList<Response<P, T, C>> symbolHandler(int index, T t) {
        ImmutableList.Builder<Response<P, T, C>> builder = ImmutableList.builder();
        for (int startIndex : th) {
            builder.add(Response.killUp(this, startIndex));
        }
        th.clear();
        return builder.build();
    }

}
