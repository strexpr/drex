package edu.upenn.cis.qdrex.core.term.arith;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.Term;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Nullable;

public class ITermMin extends Term<Double> {

    public final double acc; // accumulator
    public final Term<Double> left; // left child
    public final @Nullable Term<Double> right; // right child

    private /* mutable */ ImmutableSet<Parameter> allParameters;

    private ITermMin(double acc, Term<Double> left, Term<Double> right) {
        super(Double.class);
        this.acc = acc;
        this.left = checkNotNull(left);
        this.right = right;
        this.allParameters = null;
    }

    // Precondition: l & r are reduced.
    // Postcondition: created term is reduced.
    public static Term<Double> of(Term<Double> left, Term<Double> right) {
        return of(Double.POSITIVE_INFINITY, left, right);
    }

    // Precondition: r is reduced.
    // Postcondition: created term is reduced.
    public static Term<Double> of(double left, Term<Double> right) {
        return of(left, right, null);
    }

    // Precondition: r is reduced.
    // Postcondition: created term is reduced.
    public static Term<Double> of(Term<Double> left, double right) {
        return of(right, left, null);
    }

    // Precondition: l & r are reduced.
    // Postcondition: created term is reduced.
    public static Term<Double> of(double acc, Term<Double> left, Term<Double> right) {
        if (left instanceof ConstantTerm) {
            ConstantTerm<Double> c = (ConstantTerm<Double>)left;
            acc = Math.min(acc, c.eval());
            left = null;
        } else if (left instanceof ITermMin) {
            ITermMin t = (ITermMin)left;
            if (t.right == null) {
                // t = min(a | v, _)
                acc = Math.min(acc, t.acc);
                left = t.left;
            }
        }

        if (right instanceof ConstantTerm) {
            ConstantTerm<Double> c = (ConstantTerm<Double>)right;
            acc = Math.min(acc, c.eval());
            right = null;
        } else if (right instanceof ITermMin) {
            ITermMin t = (ITermMin)right;
            if (t.right == null) {
                // t = min(a | v, _)
                acc = Math.min(acc, t.acc);
                right = t.left;
            }
        }

        if (left == null) {
            left = right;
            right = null;
        }

        if (right == null) {
            // min(a | t, _)
            if (left instanceof ITermMin) {
                // min(a | min(b | t1, t2), _) -> min(min(a, b) | t1, t2)
                ITermMin t = (ITermMin)left;
                acc = Math.min(acc, t.acc);
                left = t.left;
                right = t.right;
            } else if (left instanceof ITermMax) {
                // min(a | max(b | t1, t2), _) -> max(min(a, b) | min(a, t1), min(a, t2))
                ITermMax t = (ITermMax)left;
                double newAcc = Math.min(acc,t.acc);
                Term<Double> newL = ITermMin.of(acc, t.left, null);
                Term<Double> newR = ITermMin.of(acc, t.right, null);
                return ITermMax.of(newAcc, newL, newR);
            }
        }

        if (left != null) {
            return new ITermMin(acc, left, right);
        } else if (right != null) {
            // l == null && r != null
            return new ITermMin(acc, right, null);
        } else {
            // newLeft == null && newRight == null
            return ConstantTerm.of(acc);
        }
    }

    @Override
    public boolean isGround() {
        return left.isGround() && (right == null || right.isGround());
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (allParameters == null) {
            Set<Parameter> ans = new HashSet<>();
            ans.addAll(left.getParameters());
            if (right != null) {
                ans.addAll(right.getParameters());
            }
            allParameters = ImmutableSet.copyOf(ans);
            verifyNotNull(allParameters);
        }
        return allParameters;
    }

    @Override
    public Double eval() {
        double res = Math.min(this.acc, left.eval());
        if (right != null) {
            res = Math.min(res, right.eval());
        }
        return res;
    }

    // Precondition: this & t are reduced
    // Postcondition: result is reduced
    // Only create new node if there has been a change below.
    @Override
    public <PC> Term<Double> subst(Parameter<PC> p, Term<PC> t) {
        Term<Double> newLeft = left.subst(p, t);
        Term<Double> newRight = right != null ? right.subst(p, t) : null;

        if (left != newLeft || right != newRight) {
            return of(acc, newLeft, newRight);
        } else {
            return this;
        }
    }

    @Override
    public int getBytes() {
        int leftBytes = 4 + left.getBytes();
        int rightBytes = 4 + (right != null ? right.getBytes() : 0);
        return 8 + 4 + leftBytes + rightBytes;
    }

    @Override
    public String toString() {
        String leftStr = left.toString();
        String rightStr = right == null ? "_" : right.toString();

        if (acc != Double.POSITIVE_INFINITY) {
            return String.format("(ITermMin %f | %s %s)", acc, leftStr, rightStr);
        } else {
            return String.format("(ITermMin %s %s)", leftStr, rightStr);
        }
    }

}
