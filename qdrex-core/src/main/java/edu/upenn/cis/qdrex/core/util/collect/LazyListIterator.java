package edu.upenn.cis.qdrex.core.util.collect;

import edu.upenn.cis.qdrex.core.util.Either;
import edu.upenn.cis.qdrex.core.util.collect.LazyList.ActualLazyList;
import edu.upenn.cis.qdrex.core.util.collect.LazyList.AppendLazyList;
import edu.upenn.cis.qdrex.core.util.collect.LazyList.ConsLazyList;
import edu.upenn.cis.qdrex.core.util.collect.LazyList.EmptyLazyList;
import edu.upenn.cis.qdrex.core.util.collect.LazyList.SnocLazyList;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.Stack;
import static com.google.common.base.Preconditions.checkElementIndex;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import com.google.common.base.VerifyException;

class LazyListIterator<T> implements ListIterator<T> {

    public final LazyList<T> list;

    private /* mutable */ int index;
    private /* mutable */ int currIndex;
    private final Stack<Either<LazyList<T>, T>> prevStack;
    private final Stack<Either<LazyList<T>, T>> nextStack;

    public LazyListIterator(LazyList<T> list) {
        this.list = checkNotNull(list);

        this.index = 0;
        this.currIndex = 0;

        this.prevStack = new Stack<>();
        this.nextStack = new Stack<>();
        this.nextStack.push(Either.left(list));
    }

    public void move(int n) {
        if (n > 0) {
            advance(n);
        } else if (n < 0) {
            recede(-n);
        }
    }

    public void advance(int n) {
        checkElementIndex(n, list.size - index);

        while (n > 0) {
            verify(n < list.size - index);

            Either<LazyList<T>, T> nextTop = nextStack.peek();
            if (nextTop.isRight()) {
                n--;
                prevStack.push(nextStack.pop());
                continue;
            }

            LazyList<T> nextList = nextTop.getLeft();
            if (nextList.size - currIndex < n) {
                n -= nextList.size - currIndex;
                prevStack.push(nextStack.pop());
                continue;
            }

            if (nextList instanceof ActualLazyList) {
                currIndex += n;
                n = 0;
            } else if (nextList instanceof AppendLazyList) {
                AppendLazyList<T> app = (AppendLazyList<T>)nextList;
                nextStack.pop();
                nextStack.push(Either.left(app.list2));
                nextStack.push(Either.left(app.list1));
            } else if (nextList instanceof ConsLazyList) {
                ConsLazyList<T> cons = (ConsLazyList<T>)nextList;
                nextStack.pop();
                nextStack.push(Either.left(cons.cdr));
                nextStack.push(Either.right(cons.car));
            } else if (nextList instanceof SnocLazyList) {
                SnocLazyList<T> snoc = (SnocLazyList<T>)nextList;
                nextStack.pop();
                nextStack.push(Either.right(snoc.last));
                nextStack.push(Either.left(snoc.rest));
            } else if (nextList instanceof EmptyLazyList) {
                nextStack.pop();
            } else {
                throw new VerifyException("Unreachable code!");
            }
        }
    }

    public void recede(int n) {
        checkElementIndex(n, index + 1);

        while (n > 0) {
            verify(n <= index);

            if (currIndex >= n) {
                currIndex -= n;
                return;
            }
            n -= currIndex;
            currIndex = 0;
            if (n <= 0) {
                return;
            }

            Either<LazyList<T>, T> prevTop = prevStack.peek();
            if (prevTop.isRight()) {
                n--;
                nextStack.push(prevStack.pop());
                continue;
            }

            LazyList<T> prevList = prevTop.getLeft();
            if (prevList.size <= n) {
                n -= prevList.size;
                nextStack.push(prevStack.pop());
                continue;
            }

            if (prevList instanceof ActualLazyList) {
                nextStack.push(prevStack.pop());
                currIndex = prevList.size - n;
                n = 0;
            } else if (prevList instanceof AppendLazyList) {
                AppendLazyList<T> app = (AppendLazyList<T>)prevList;
                prevStack.pop();
                prevStack.push(Either.left(app.list1));
                prevStack.push(Either.left(app.list2));
            } else if (prevList instanceof ConsLazyList) {
                ConsLazyList<T> cons = (ConsLazyList<T>)prevList;
                prevStack.pop();
                prevStack.push(Either.right(cons.car));
                prevStack.push(Either.left(cons.cdr));
            } else if (prevList instanceof SnocLazyList) {
                SnocLazyList<T> snoc = (SnocLazyList<T>)prevList;
                prevStack.pop();
                prevStack.push(Either.left(snoc.rest));
                prevStack.push(Either.right(snoc.last));
            } else if (prevList instanceof EmptyLazyList) {
                prevStack.pop();
            } else {
                throw new VerifyException("Unreachable code!");
            }
        }
    }

    @Override
    public boolean hasNext() {
        return index < list.size;
    }

    @Override
    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }

        index++;

        while (true) {
            Either<LazyList<T>, T> nextTop = nextStack.peek();
            if (nextTop.isRight()) {
                prevStack.push(nextStack.pop());
                return nextTop.getRight();
            }
            LazyList<T> nextList = nextTop.getLeft();

            if (nextList instanceof ActualLazyList) {
                ActualLazyList<T> act = (ActualLazyList<T>)nextList;
                if (currIndex < act.size) {
                    return act.list.get(currIndex++);
                }
                currIndex = 0;
                prevStack.push(nextStack.pop());
            } else if (nextList instanceof AppendLazyList) {
                AppendLazyList<T> app = (AppendLazyList<T>)nextList;
                nextStack.pop();
                nextStack.push(Either.left(app.list2));
                nextStack.push(Either.left(app.list1));
            } else if (nextList instanceof ConsLazyList) {
                ConsLazyList<T> ctop = (ConsLazyList<T>)nextList;
                nextStack.pop();
                nextStack.push(Either.left(ctop.cdr));
                nextStack.push(Either.right(ctop.car));
            } else if (nextList instanceof SnocLazyList) {
                SnocLazyList<T> stop = (SnocLazyList<T>)nextList;
                nextStack.pop();
                nextStack.push(Either.right(stop.last));
                nextStack.push(Either.left(stop.rest));
            } else if (nextList instanceof EmptyLazyList) {
                nextStack.pop();
            } else {
                throw new VerifyException("Unreachable code!");
            }
        }
    }

    @Override
    public boolean hasPrevious() {
        return index > 0;
    }

    @Override
    public T previous() {
        if (!hasPrevious()) {
            throw new NoSuchElementException();
        }

        index--;

        while (true) {
            if (currIndex > 0) {
                Either<LazyList<T>, T> nextTop = nextStack.peek();
                verify(nextTop.isLeft());
                LazyList<T> nextList = nextTop.getLeft();
                verify(nextList instanceof ActualLazyList);
                ActualLazyList<T> act = (ActualLazyList<T>)nextList;
                verify(currIndex <= act.size);
                return act.list.get(--currIndex);
            }

            Either<LazyList<T>, T> prevTop = prevStack.peek();
            if (prevTop.isRight()) {
                nextStack.push(prevStack.pop());
                return prevTop.getRight();
            }
            LazyList<T> prevList = prevTop.getLeft();

            if (prevList instanceof ActualLazyList) {
                ActualLazyList<T> act = (ActualLazyList<T>)prevList;
                nextStack.push(prevStack.pop());
                currIndex = act.size;
            } else if (prevList instanceof AppendLazyList) {
                AppendLazyList<T> app = (AppendLazyList<T>)prevList;
                prevStack.pop();
                prevStack.push(Either.left(app.list1));
                prevStack.push(Either.left(app.list2));
            } else if (prevList instanceof ConsLazyList) {
                ConsLazyList<T> cons = (ConsLazyList<T>)prevList;
                prevStack.pop();
                prevStack.push(Either.right(cons.car));
                prevStack.push(Either.left(cons.cdr));
            } else if (prevList instanceof SnocLazyList) {
                SnocLazyList<T> snoc = (SnocLazyList<T>)prevList;
                prevStack.pop();
                prevStack.push(Either.left(snoc.rest));
                prevStack.push(Either.right(snoc.last));
            } else if (prevList instanceof EmptyLazyList) {
                prevStack.pop();
            } else {
                throw new VerifyException("Unreachable code!");
            }
        }
    }

    @Override
    public int nextIndex() {
        return index;
    }

    @Override
    public int previousIndex() {
        return index - 1;
    }

    @Override @Deprecated
    public void remove() {
        throw new UnsupportedOperationException();
    }

    @Override @Deprecated
    public void set(T e) {
        throw new UnsupportedOperationException();
    }

    @Override @Deprecated
    public void add(T e) {
        throw new UnsupportedOperationException();
    }

}
