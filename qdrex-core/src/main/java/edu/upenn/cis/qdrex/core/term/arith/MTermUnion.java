package edu.upenn.cis.qdrex.core.term.arith;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.term.IncompleteTermException;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Nullable;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;

public class MTermUnion extends Term<Multiset> {

    public final Multiset acc;
    public final Term<Multiset> left;
    public final @Nullable Term<Multiset> right;

    private /* mutable */ ImmutableSet<Parameter> allParameters;

    private MTermUnion(Multiset acc, Term<Multiset> left, Term<Multiset> right) {
        super(Multiset.class);
        this.acc = checkNotNull(acc);
        this.left = checkNotNull(left);
        this.right = right;
        this.allParameters = null;
    }

    public static Term<Multiset> of(Term<Multiset> left, Term<Multiset> right) {
        return of(Multiset.EMPTY, left, right);
    }

    public static Term<Multiset> of(Multiset left, Term<Multiset> right) {
        return of(left, right, null);
    }

    public static Term<Multiset> of(Term<Multiset> left, Multiset right) {
        return of(right, left, null);
    }

    public static Term<Multiset> of(Multiset acc, Term<Multiset> left, Term<Multiset> right) {
        if (left instanceof ConstantTerm) {
            ConstantTerm<Multiset> c = (ConstantTerm<Multiset>)left;
            acc = acc.merge(c.eval());
            left = null;
        } else if (left instanceof MTermUnion) {
            MTermUnion t = (MTermUnion)left;
            if (t.right == null) {
                // t = union(a | v, _)
                acc = acc.merge(t.acc);
                left = t.left;
            }
        }

        if (right instanceof ConstantTerm) {
            ConstantTerm<Multiset> c = (ConstantTerm<Multiset>)right;
            acc = acc.merge(c.eval());
            right = null;
        } else if (right instanceof MTermUnion) {
            MTermUnion t = (MTermUnion)right;
            if (t.right == null) {
                // t = union(a | v, _)
                acc = acc.merge(t.acc);
                right = t.left;
            }
        }

        if (left == null) {
            left = right;
            right = null;
        }

        if (right == null && left instanceof MTermUnion) {
            // union(a | union(b | t1, t2), _) -> union(union(a, b) | t1, t2)
            MTermUnion t = (MTermUnion)left;
            acc = acc.merge(t.acc);
            left = t.left;
            right = t.right;
        }

        if (left != null) {
            return new MTermUnion(acc, left, right);
        } else if (right != null) {
            // l == null & r != null
            return new MTermUnion(acc, right, null);
        } else {
            // newLeft == newRight == null
            return ConstantTerm.of(acc);
        }
    }

    @Override
    public boolean isGround() {
        return left.isGround() && (right == null || right.isGround());
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (allParameters == null) {
            Set<Parameter> ans = new HashSet<>();
            ans.addAll(left.getParameters());
            if (right != null) {
                ans.addAll(right.getParameters());
            }
            allParameters = ImmutableSet.copyOf(ans);
            verify(allParameters != null);
        }
        return allParameters;
    }

    @Override
    public Multiset eval() throws IncompleteTermException {
        Multiset ans = acc;
        ans = ans.merge(left.eval());
        if (right != null) {
            ans = ans.merge(right.eval());
        }
        return ans;
    }

    @Override
    public <PC> Term<Multiset> subst(Parameter<PC> p, Term<PC> t) {
        Term<Multiset> newLeft = left.subst(p, t);
        Term<Multiset> newRight = right != null ? right.subst(p, t) : null;

        if (newLeft != left || newRight != right) {
            return of(acc, newLeft, newRight);
        } else {
            return this;
        }
    }

    @Override
    public int getBytes() {
        int leftBytes = 4 + left.getBytes();
        int rightBytes = 4 + (right != null ? right.getBytes() : 0);
        return 8 + 4 + leftBytes + rightBytes;
    }

    @Override
    public String toString() {
        String leftStr = left.toString();
        String rightStr = right == null ? "_" : right.toString();

        if (acc != Multiset.EMPTY) {
            return String.format("(MTermUnion %s | %s %s)", acc, leftStr, rightStr);
        } else {
            return String.format("(MTermUnion %s %s)", leftStr, rightStr);
        }
    }

}
