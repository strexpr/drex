package edu.upenn.cis.qdrex.core.fj.analysis;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisError.BaseAnalysisError;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public abstract class AnalysisError<P extends Predicate<P, T>, T, C> {

    public static <P extends Predicate<P, T>, T, C> BaseAnalysisError<P, T, C>
    base(QRE<P, T, C> e, String msg) {
        return new BaseAnalysisError(e, msg);
    }

    public static class BaseAnalysisError<P extends Predicate<P, T>, T, C>
    extends AnalysisError<P, T, C> {

        public final QRE<P, T, C> e;
        public final String msg;

        public BaseAnalysisError(QRE<P, T, C> e, String msg) {
            this.e = checkNotNull(e);
            this.msg = checkNotNull(msg);
        }

        @Override
        public String toString() {
            return String.format("Error while analysing %s.\n%s", e, msg);
        }

    }

    public static <P extends Predicate<P, T>, T, C> SubexprAnalysisError<P, T, C>
    subexpr(QRE<P, T, C> e, AnalysisError error) {
        return new SubexprAnalysisError(e, error);
    }

    public static class SubexprAnalysisError<P extends Predicate<P, T>, T, C>
    extends AnalysisError<P, T, C> {

        public final QRE<P, T, C> e;
        public final AnalysisError error;

        public SubexprAnalysisError(QRE<P, T, C> e, AnalysisError error) {
            this.e = checkNotNull(e);
            this.error = checkNotNull(error);
        }

        @Override
        public String toString() {
            return String.format("Error while analysing %s.\n%s", e, error);
        }

    }

}
