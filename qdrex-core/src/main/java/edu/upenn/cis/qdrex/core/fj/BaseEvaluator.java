package edu.upenn.cis.qdrex.core.fj;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.qre.Base;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import java.util.HashSet;
import java.util.Set;

public class BaseEvaluator<P extends Predicate<P, T>, T, C> extends Evaluator<P, T, C> {

    public final Base<P, T, C> base;
    public final boolean isSat;
    private final /* non-frozen */ Set<Integer> th1, th2;

    public BaseEvaluator(Base<P, T, C> base) {
        super(base);
        checkArgument(base.fjAnalyze().isUnambiguous());
        this.base = base;
        this.isSat = base.p.isSat();
        this.th1 = new HashSet<>();
        this.th2 = new HashSet<>();
    }

    @Override
    public void resetHandler() {
        th1.clear();
        th2.clear();
    }

    @Override
    public ImmutableList<Response<P, T, C>> startHandler(int index) {
        if (isSat) {
            th1.add(index);
            return ImmutableList.of();
        } else {
            return ImmutableList.of(Response.killUp(this, index));
        }
    }

    @Override
    public ImmutableList<Response<P, T, C>> symbolHandler(int index, T t) {
        checkNotNull(t);
        ImmutableList.Builder<Response<P, T, C>> builder = ImmutableList.builder();

        for (int t2 : th2) {
            builder.add(Response.killUp(this, t2));
        }
        th2.clear();

        if (base.p.test(t)) {
            for (int t1 : th1) {
                Term<C> ans = new ConstantTerm<>(base.f.apply(t), base.outType);
                builder.add(Response.result(this, t1, ans));
            }
            th2.addAll(th1);
            th1.clear();
        } else {
            for (int t1 : th1) {
                builder.add(Response.killUp(this, t1));
            }
            th1.clear();
        }

        return builder.build();
    }

}
