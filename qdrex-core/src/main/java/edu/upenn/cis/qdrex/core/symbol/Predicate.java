package edu.upenn.cis.qdrex.core.symbol;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Collection;
import java.util.HashSet;
import com.google.common.base.Optional;
import java.io.Serializable;
import java.util.Set;
import org.apache.commons.lang3.tuple.ImmutablePair;

/**
 * Type <code>P</code> of predicates over data values of type <code>T</code>.
 * @param <P> predicate type
 * @param <T> data type
 */
public abstract class Predicate<P extends Predicate<P, T>, T> implements Serializable {

    public abstract boolean test(T t);
    public abstract Optional<T> getWitness();

    public abstract P not();
    public abstract P and(P other);

    public P or(P other) {
        P notThis = this.not();
        P notThat = other.not();
        P notThisNotThat = and(notThis, notThat);
        return not(notThisNotThat);
    }

    public boolean isSat() {
        return getWitness().isPresent();
    }

    public P minus(P other) {
        return this.and(other.not());
    }

    public static <P extends Predicate<P, T>, T> P not(P pred) {
        return pred.not();
    }

    public static <P extends Predicate<P, T>, T> P and(P p1, P... ps) {
        P ans = checkNotNull(p1);
        for (P p : ps) {
            ans = ans.and(p);
        }
        return ans;
    }

    public static <P extends Predicate<P, T>, T> P and(P p1, Collection<? extends P> ps) {
        P ans = checkNotNull(p1);
        for (P p : ps) {
            ans = ans.and(p);
        }
        return ans;
    }

    public static <P extends Predicate<P, T>, T> P or(P p1, P... ps) {
        P ans = checkNotNull(p1);
        for (P p : ps) {
            ans = ans.or(p);
        }
        return ans;
    }

    public static <P extends Predicate<P, T>, T> P or(P p1, Collection<? extends P> ps) {
        P ans = checkNotNull(p1);
        for (P p : ps) {
            ans = ans.or(p);
        }
        return ans;
    }

    public static <P extends Predicate<P, T>, T> P minus(P pred1, P pred2) {
        return pred1.minus(pred2);
    }

    public static <P extends Predicate<P, T>, T> boolean areEquivalent(P pred1, P pred2) {
        boolean p12s = pred1.minus(pred2).isSat();
        boolean p21s = pred2.minus(pred1).isSat();
        return (!p12s) && (!p21s);
    }

    public static <P extends Predicate<P, T>, T> ImmutableSet<MinTerm<P, T>>
    buildMinTerms(P truePred, Collection<? extends P> ps) {
        checkNotNull(truePred);

        ImmutableSet<P> emptySet = ImmutableSet.of();
        ImmutablePair<ImmutableSet<P>, P> seed = ImmutablePair.of(emptySet, truePred);
        Set<ImmutablePair<ImmutableSet<P>, P>> terms = Sets.newHashSet(seed);

        for (P p : ps) {
            Set<ImmutablePair<ImmutableSet<P>, P>> newTerms = Sets.newHashSet();

            for (ImmutablePair<ImmutableSet<P>, P> term : terms) {
                P tp = and(term.right, p);
                P np = not(p);
                P tnp = and(term.right, np);

                if (tp.isSat()) {
                    Set<P> sp = Sets.newHashSet(p);
                    ImmutableSet<P> stp = Sets.union(term.left, sp).immutableCopy();
                    newTerms.add(ImmutablePair.of(stp, tp));
                }

                if (tnp.isSat()) {
                    Set<P> snp = Sets.newHashSet(np);
                    ImmutableSet<P> stnp = Sets.union(term.left, snp).immutableCopy();
                    newTerms.add(ImmutablePair.of(stnp, tnp));
                }
            }

            terms = newTerms;
        }

        Set<MinTerm<P, T>> ans = new HashSet<>();
        for (ImmutablePair<ImmutableSet<P>, P> term : terms) {
            P pred = term.right;
            ImmutableSet<P> posBasis = term.left;

            Set<P> negBasis = new HashSet<>();
            negBasis.addAll(ps);
            negBasis.removeAll(posBasis);

            ans.add(new MinTerm<>(pred, posBasis, ImmutableSet.copyOf(negBasis)));
        }
        return ImmutableSet.copyOf(ans);
    }

}
