package edu.upenn.cis.qdrex.core.fj;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Table;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Term;
import java.util.HashSet;
import java.util.Set;
import static com.google.common.base.Verify.verify;
import edu.upenn.cis.qdrex.core.qre.Split;

public class SplitEvaluator<P extends Predicate<P, T>, T, Cf, Cg, C> extends Evaluator<P, T, C> {

    public final Split<P, T, Cf, Cg, C> split;
    public final Evaluator<P, T, Cf> mf;
    public final Evaluator<P, T, Cg> mg;

    private final Set<Integer> thf;
    private final Table<Integer, Integer, Term<Cf>> thg;

    public SplitEvaluator(Split<P, T, Cf, Cg, C> split) {
        super(split);
        this.split = split;
        this.mf = split.f.buildEvaluator();
        this.mg = split.g.buildEvaluator();

        this.thf = new HashSet<>();
        this.thg = HashBasedTable.create();
    }

    @Override
    public void resetHandler() {
        mf.reset();
        mg.reset();
        thf.clear();
        thg.clear();
    }

    @Override
    public ImmutableList<Response<P, T, C>> startHandler(int index) {
        thf.add(index);
        ImmutableList<Response<P, T, Cf>> respf = mf.start(index);

        ImmutableList.Builder<Response<P, T, C>> builder = ImmutableList.builder();
        procf(index, respf, builder);
        return builder.build();
    }

    @Override
    public ImmutableList<Response<P, T, C>> symbolHandler(int index, T t) {
        ImmutableList<Response<P, T, Cf>> respf = mf.symbol(index, t);
        ImmutableList<Response<P, T, Cg>> respg = mg.symbol(index, t);

        ImmutableList.Builder<Response<P, T, C>> builder = ImmutableList.builder();
        procf(index, respf, builder);
        procg(respg, builder);
        return builder.build();
    }

    private void procf(int index, ImmutableList<Response<P, T, Cf>> respf,
            ImmutableList.Builder<Response<P, T, C>> builder) {
        for (Response<P, T, Cf> r : respf) {
            if (r.isResult) {
                thg.put(r.startIndex, index, r.getTerm());
                ImmutableList<Response<P, T, Cg>> respg = mg.start(index);
                procg(respg, builder);
            } else {
                verify(r.isKillUp);
                thf.remove(r.startIndex);
                if (!thg.containsRow(r.startIndex)) {
                    builder.add(Response.killUp(this, r.startIndex));
                }
            }
        }
    }

    private void procg(ImmutableList<Response<P, T, Cg>> respg,
            ImmutableList.Builder<Response<P, T, C>> builder) {
        for (Response<P, T, Cg> r : respg) {
            if (r.isResult) {
                Term<Cg> tg = r.getTerm();
                for (int i : thg.column(r.startIndex).keySet()) {
                    Term<Cf> tf = thg.get(i, r.startIndex);
                    Term<C> t = split.combine(tf, tg);
                    builder.add(Response.result(this, i, t));
                }
            } else {
                verify(r.isKillUp);
                Set<Integer> killRing = thg.column(r.startIndex).keySet();
                for (int i : ImmutableSet.copyOf(killRing)) {
                    thg.remove(i, r.startIndex);
                    if ((!thg.containsRow(i)) && (!thf.contains(i))) {
                        builder.add(Response.killUp(this, i));
                    }
                }
            }
        }
    }

}
