package edu.upenn.cis.qdrex.core.qre;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.regex.Regex;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.fj.BaseEvaluator;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;

public class Base<P extends Predicate<P, T>, T, C> extends QRE<P, T, C> {

    public final P p;
    public final transient Function<? super T, ? extends C> f;

    private /* mutable */ AnalysisResult<P, T, C> analysisResult;

    public Base(P p, Function<? super T, ? extends C> f, Class<C> outType) {
        super(QRE.Type.BASE, outType);
        this.p = checkNotNull(p);
        this.f = checkNotNull(f);
        this.analysisResult = null;
    }

    public static <P extends Predicate<P, T>, T> Base<P, T, Double>
    arith(P p, Function<? super T, ? extends Double> f) {
        return new Base(p, f, Double.class);
    }

    public static <P extends Predicate<P, T>, T> Base<P, T, Double>
    multiset(P p, Function<? super T, ? extends Multiset> f) {
        return new Base(p, f, Multiset.class);
    }

    public static <P extends Predicate<P, T>, T> Base<P, T, String>
    string(P p, Function<? super T, ? extends Object> f) {
        checkNotNull(f);
        Function<T, String> fp = t -> String.valueOf(f.apply(t));
        return new Base(p, fp, String.class);
    }

    @Override
    public int size() {
        return 1;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        return ImmutableSet.of();
    }

    @Override
    public AnalysisResult<P, T, C> fjAnalyze() {
        if (analysisResult == null) {
            Regex<P, T> r = new edu.upenn.cis.qdrex.core.regex.Base<>(p);
            analysisResult = AnalysisResult.unambiguous(r);
            verifyNotNull(analysisResult);
        }
        return analysisResult;
    }

    @Override
    public BaseEvaluator<P, T, C> buildEvaluator() {
        return new BaseEvaluator<>(this);
    }

    @Override
    public String toString() {
        return String.format("(Base %s %s)", p, f);
    }

}
