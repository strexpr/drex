package edu.upenn.cis.qdrex.core.symbol;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import java.util.Objects;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.qdrex.core.util.collect.CollectionUtil.map;

/**
 * The type of min-terms over a <code>BooleanAlgebra<P, D></code>.
 * @param <P>
 * @param <T> 
 */
public class MinTerm<P extends Predicate<P, T>, T> {

    public final P pred;
    public final ImmutableSet<P> posBasis;
    public final ImmutableSet<P> negBasis;

    public MinTerm(P pred, ImmutableSet<P> posBasis, ImmutableSet<P> negBasis) {
        this.pred = checkNotNull(pred);
        this.posBasis = checkNotNull(posBasis);
        this.negBasis = checkNotNull(negBasis);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof MinTerm)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        MinTerm minObj = (MinTerm)obj;
        return Objects.equals(pred, minObj.pred) &&
                Objects.equals(posBasis, minObj.posBasis) &&
                Objects.equals(negBasis, minObj.negBasis);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(pred)
                                    .append(posBasis)
                                    .append(negBasis)
                                    .toHashCode();
    }

    @Override
    public String toString() {
        String pos = Joiner.on(" ").join(map(posBasis, p -> p.toString()));
        pos = String.format("(posBasis %s)", pos);
        String neg = Joiner.on(" ").join(map(negBasis, p -> p.toString()));
        neg = String.format("(posBasis %s)", neg);
        return String.format("(MinTerm %s %s %s)", pred, pos, neg);
    }

}
