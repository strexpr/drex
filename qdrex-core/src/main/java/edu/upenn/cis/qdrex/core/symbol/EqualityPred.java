package edu.upenn.cis.qdrex.core.symbol;

import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import com.google.common.collect.Sets;
import java.util.Iterator;
import com.google.common.base.Optional;
import static edu.upenn.cis.qdrex.core.util.collect.CollectionUtil.ALL_POSITIVE_INTEGERS;
import static com.google.common.base.Preconditions.checkNotNull;

public class EqualityPred<T> extends Predicate<EqualityPred<T>, T> {

    public enum Type { MATCH_SOME, REJECT_ALL }

    public final Type type;
    public final ImmutableSet<T> set;
    public final Iterable<T> allTs;

    public EqualityPred(Type type, ImmutableSet<T> set, Iterable<T> allTs) {
        this.type = type;
        this.set = checkNotNull(set);
        this.allTs = checkNotNull(allTs);
    }

    public static <T> EqualityPred<T> truePred(Iterable<T> allTs) {
        return new EqualityPred<>(Type.REJECT_ALL, ImmutableSet.of(), allTs);
    }

    public static <T> EqualityPred<T> falsePred(Iterable<T> allTs) {
        return new EqualityPred<>(Type.MATCH_SOME, ImmutableSet.of(), allTs);
    }

    public static EqualityPred<Integer> equalToPositiveInt(int t) {
        checkArgument(t >= 0);
        return new EqualityPred<>(Type.MATCH_SOME, ImmutableSet.of(t), ALL_POSITIVE_INTEGERS);
    }

    @Override
    public boolean test(T t) {
        return type == Type.MATCH_SOME ? set.contains(t) : !set.contains(t);
    }

    @Override
    public Optional<T> getWitness() {
        if (type == Type.MATCH_SOME) {
            if (!set.isEmpty()) {
                T t = set.iterator().next();
                return Optional.of(t);
            } else {
                return Optional.absent();
            }
        } else {
            Iterator<T> ansIterator = Iterators.filter(allTs.iterator(),
                    t -> !set.contains(t));
            if (ansIterator.hasNext()) {
                return Optional.of(ansIterator.next());
            } else {
                return Optional.absent();
            }
        }
    }

    @Override
    public EqualityPred<T> not() {
        Type notType = type == Type.MATCH_SOME ? Type.REJECT_ALL : Type.MATCH_SOME;
        return new EqualityPred<>(notType, set, allTs);
    }

    @Override
    public EqualityPred<T> and(EqualityPred<T> other) {
        if (type == Type.MATCH_SOME) {
            if (other.type == Type.MATCH_SOME) {
                ImmutableSet<T> newSet = Sets.intersection(set, other.set).immutableCopy();
                return new EqualityPred<>(Type.MATCH_SOME, newSet, allTs);
            } else {
                ImmutableSet<T> newSet = Sets.difference(set, other.set).immutableCopy();
                return new EqualityPred<>(Type.MATCH_SOME, newSet, allTs);
            }
        } else {
            if (other.type == Type.MATCH_SOME) {
                ImmutableSet<T> newSet = Sets.difference(other.set, set).immutableCopy();
                return new EqualityPred<>(Type.MATCH_SOME, newSet, allTs);
            } else {
                ImmutableSet<T> newSet = Sets.union(set, other.set).immutableCopy();
                return new EqualityPred<>(Type.REJECT_ALL, newSet, allTs);
            }
        }
    }

    @Override
    public String toString() {
        return String.format("(EqualityPred %s %s)", type, set);
    }

}
