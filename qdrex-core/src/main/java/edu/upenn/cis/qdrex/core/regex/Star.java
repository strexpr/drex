package edu.upenn.cis.qdrex.core.regex;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;
import edu.upenn.cis.qdrex.core.sfa.SFA;
import edu.upenn.cis.qdrex.core.sfa.SFAUtil;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class Star<P extends Predicate<P, T>, T> extends Regex<P, T> {

    public final Regex<P, T> r;

    private /* mutable */ int size;
    private /* mutable */ SFA<P, T> sfa;

    public Star(Regex<P, T> r) {
        super(Type.STAR);
        this.r = checkNotNull(r);
        this.size = -1;
        this.sfa = null;
    }

    public static <P extends Predicate<P, T>, T> Star<P, T> of(Regex<P, T> r) {
        return new Star<>(r);
    }

    public static <P extends Predicate<P, T>, T> Regex<P, T> plus(Regex<P, T> r) {
        return Concat.of(r, Star.of(r));
    }

    @Override
    public int size() {
        if (size <= 0) {
            size = 1 + r.size();
            verify(size > 0);
        }
        return size;
    }

    @Override
    public <R> R invokeVisitor(RegexVisitor<P, T, R> visitor) {
        return visitor.visitStar(this);
    }

    @Override
    public SFA<P, T> buildSFA() {
        if (sfa == null) {
            SFA<P, T> rSFA = r.buildSFA();
            sfa = SFAUtil.buildStarSFA(rSFA);
            verifyNotNull(sfa);
        }
        return sfa;
    }

    @Override
    public String toString() {
        return String.format("(Star %s)", r);
    }

}
