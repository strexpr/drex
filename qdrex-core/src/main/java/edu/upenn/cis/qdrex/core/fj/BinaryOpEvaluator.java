package edu.upenn.cis.qdrex.core.fj;

import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.qre.BinaryOp;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Term;

public class BinaryOpEvaluator<P extends Predicate<P, T>, T, Cf, Cg, C> extends Evaluator<P, T, C> {

    public final BinaryOp<P, T, Cf, Cg, C> binOp;
    private final Evaluator<P, T, Cf> mf;
    private final Evaluator<P, T, Cg> mg;

    public BinaryOpEvaluator(BinaryOp<P, T, Cf, Cg, C> binOp) {
        super(binOp);
        this.binOp = binOp;
        this.mf = binOp.f.buildEvaluator();
        this.mg = binOp.g.buildEvaluator();
    }

    @Override
    public void resetHandler() {
        mf.reset();
        mg.reset();
    }

    @Override
    public ImmutableList<Response<P, T, C>> startHandler(int index) {
        ImmutableList<Response<P, T, Cf>> respf = mf.start(index);
        ImmutableList<Response<P, T, Cg>> respg = mg.start(index);
        return processSubResponses(respf, respg);
    }

    @Override
    public ImmutableList<Response<P, T, C>> symbolHandler(int index, T t) {
        ImmutableList<Response<P, T, Cf>> respf = mf.symbol(index, t);
        ImmutableList<Response<P, T, Cg>> respg = mg.symbol(index, t);
        return processSubResponses(respf, respg);
    }

    private ImmutableList<Response<P, T, C>> processSubResponses(
            Iterable<Response<P, T, Cf>> respf,
            Iterable<Response<P, T, Cg>> respg) {
        ImmutableList.Builder<Response<P, T, C>> builder = ImmutableList.builder();
        for (Response<P, T, Cf> sr1 : respf) {
            if (sr1.isResult) {
                for (Response<P, T, Cg> sr2 : respg) {
                    if (sr2.isResult && sr2.startIndex == sr1.startIndex) {
                        Term<C> result = binOp.op(sr1.getTerm(), sr2.getTerm());
                        builder.add(Response.result(this, sr1.startIndex, result));
                    }
                }
            } else {
                builder.add(Response.killUp(this, sr1.startIndex));
            }
        }
        return builder.build();
    }

}
