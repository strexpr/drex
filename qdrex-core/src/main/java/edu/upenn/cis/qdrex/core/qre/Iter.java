package edu.upenn.cis.qdrex.core.qre;

import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Term;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.fj.Evaluator;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.regex.Regex;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.fj.IterEvaluator;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.ParameterTerm;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;

public abstract class Iter<P extends Predicate<P, T>, T, Cr, Cf, C> extends QRE<P, T, C> {

    public final Term<Cr> initializer;
    public final QRE<P, T, Cf> f;
    public final boolean emptyDefined;

    private /* mutable */ int size;
    private /* mutable */ AnalysisResult<P, T, C> analysisResult;

    public Iter(Term<Cr> tr0, QRE<P, T, Cf> f, boolean emptyDefined, Class<C> outType) {
        super(Type.ITER, outType);

        this.initializer = checkNotNull(tr0);
        this.f = checkNotNull(f);
        this.emptyDefined = emptyDefined;

        this.size = -1;
        this.analysisResult = null;
    }

    public abstract Term<Cr> combine(Term<Cr> tr, Term<Cf> tf);
    public abstract Term<C> finalize(Term<Cr> tr);

    @Override
    public int size() {
        if (size <= 0) {
            size = 1 + f.size();
            verify(size > 0);
        }
        return size;
    }

    @Override
    public AnalysisResult<P, T, C> fjAnalyze() {
        if (analysisResult == null) {
            analysisResult = fjAnalyzeInt();
            verifyNotNull(analysisResult);
        }
        return analysisResult;
    }

    public AnalysisResult<P, T, C> fjAnalyzeInt() {
        AnalysisResult<P, T, Cf> a = f.fjAnalyze();
        if (!a.isUnambiguous()) {
            return AnalysisResult.subexprError(this, a.getAnalysisError());
        }

        Regex<P, T> r = a.getDomainType();
        Regex<P, T> rStar = r.star();
        Regex<P, T> rAns = emptyDefined ? rStar : rStar.concat(r);

        Optional<ImmutableList<T>> ambiguousWitness = rAns.getAmbiguousWitness();
        if (ambiguousWitness.isPresent()) {
            String msg = String.format("Ambiguous split on input %s.", ambiguousWitness.get());
            return AnalysisResult.baseError(this, msg);
        }

        return AnalysisResult.unambiguous(rAns);
    }

    @Override
    public Evaluator<P, T, C> buildEvaluator() {
        return new IterEvaluator<>(this);
    }

    public static <P extends Predicate<P, T>, T, C> Right<P, T, C>
    right(Parameter<C> p, QRE<P, T, C> f) {
        return new Right(p, f);
    }

    public static class Right<P extends Predicate<P, T>, T, C> extends Iter<P, T, C, C, C> {

        public Parameter<C> p;

        public Right(Parameter<C> p, QRE<P, T, C> f) {
            super(new ParameterTerm<>(p), f, true, f.outType);
            this.p = p;
        }

        @Override
        public Term<C> combine(Term<C> tr, Term<C> tf) {
            return tf.subst(p, tr);
        }

        @Override
        public Term<C> finalize(Term<C> tr) {
            return tr;
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            return ImmutableSet.of(p);
        }

        @Override
        public String toString() {
            return String.format("(Iter.Right %s> %s)", p, f);
        }

    }

    public static <P extends Predicate<P, T>, T, C> Left<P, T, C>
    left(Parameter<C> p, QRE<P, T, C> f) {
        return new Left(p, f);
    }

    public static class Left<P extends Predicate<P, T>, T, C> extends Iter<P, T, C, C, C> {

        public Parameter<C> p;

        public Left(Parameter<C> p, QRE<P, T, C> f) {
            super(new ParameterTerm<>(p), f, true, f.outType);
            this.p = p;
        }

        @Override
        public Term<C> combine(Term<C> tr, Term<C> tf) {
            return tr.subst(p, tf);
        }

        @Override
        public Term<C> finalize(Term<C> tr) {
            return tr;
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            return ImmutableSet.of(p);
        }

        @Override
        public String toString() {
            return String.format("(Iter.Left <%s %s)", p, f);
        }

    }

}
