package edu.upenn.cis.qdrex.core.symbol;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Optional;

public class TrivialPred<T> extends Predicate<TrivialPred<T>, T> {

    public final boolean value;
    public final T t;

    public TrivialPred(boolean value, T t) {
        this.value = value;
        this.t = checkNotNull(t);
    }

    @Override
    public boolean test(T t) {
        return value;
    }

    @Override
    public Optional<T> getWitness() {
        return value ? Optional.of(t) : Optional.absent();
    }

    @Override
    public TrivialPred<T> not() {
        return new TrivialPred(!value, t);
    }

    @Override
    public TrivialPred<T> and(TrivialPred<T> other) {
        return new TrivialPred(value && other.value, t);
    }

    @Override
    public String toString() {
        return String.format("(TrivialPred %b %s)", value, t);
    }

}
