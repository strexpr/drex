package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.fj.Evaluator;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;
import edu.upenn.cis.qdrex.core.fj.UnaryOpEvaluator;
import edu.upenn.cis.qdrex.core.term.Term;

public abstract class UnaryOp<P extends Predicate<P, T>, T, Cf, C> extends QRE<P, T, C> {

    public final QRE<P, T, Cf> f;

    private /* mutable */ int size;
    private /* mutable */ ImmutableSet<Parameter> parameters;
    private /* mutable */ AnalysisResult<P, T, C> analysisResult;

    public UnaryOp(QRE<P, T, Cf> f, Class<C> outType) {
        super(Type.OP, outType);
        this.f = checkNotNull(f);
        this.size = -1;
        this.parameters = null;
        this.analysisResult = null;
    }

    public abstract Term<C> op(Term<Cf> tf);
    public abstract ImmutableSet<Parameter> getParametersInt();

    @Override
    public int size() {
        if (size <= 0) {
            size = 1 + f.size();
            verify(size > 0);
        }
        return size;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (parameters == null) {
            parameters = getParametersInt();
            verifyNotNull(parameters);
        }
        return parameters;
    }

    @Override
    public AnalysisResult<P, T, C> fjAnalyze() {
        if (analysisResult == null) {
            AnalysisResult<P, T, Cf> af = f.fjAnalyze();
            if (af.isUnambiguous()) {
                analysisResult = AnalysisResult.unambiguous(af.getDomainType());
            } else {
                analysisResult = AnalysisResult.subexprError(this, af.getAnalysisError());
            }
            verifyNotNull(analysisResult);
        }
        return analysisResult;
    }

    @Override
    public Evaluator<P, T, C> buildEvaluator() {
        return new UnaryOpEvaluator<>(this);
    }

}
