package edu.upenn.cis.qdrex.core.util;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.VerifyException;
import java.io.Serializable;
import java.util.NoSuchElementException;
import java.util.Objects;
import javax.annotation.Nullable;
import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class Either3<L, M, R> implements Serializable, Comparable<Either3<L, M, R>> {

    public enum Direction { LEFT, MIDDLE, RIGHT }

    public final Direction direction;
    private final @Nullable L left;
    private final @Nullable M middle;
    private final @Nullable R right;

    private Either3(Direction direction, L left, M middle, R right) {
        this.direction = direction;
        this.left = direction == Direction.LEFT ? checkNotNull(left) : null;
        this.middle = direction == Direction.MIDDLE ? checkNotNull(middle) : null;
        this.right = direction == Direction.RIGHT ? checkNotNull(right) : null;
    }

    public final static <L, M, R> Either3<L, M, R> left(L left) {
        return new Either3(Direction.LEFT, left, null, null);
    }

    public final static <L, M, R> Either3<L, M, R> middle(M middle) {
        return new Either3(Direction.MIDDLE, null, middle, null);
    }

    public final static <L, M, R> Either3<L, M, R> right(R right) {
        return new Either3(Direction.RIGHT, null, null, right);
    }

    public boolean isLeft() {
        return direction == Direction.LEFT;
    }

    public L getLeft() throws NoSuchElementException {
        if (direction == Direction.LEFT) {
            return left;
        } else {
            throw new NoSuchElementException();
        }
    }

    public boolean isMiddle() {
        return direction == Direction.MIDDLE;
    }

    public M getMiddle() throws NoSuchElementException {
        if (direction == Direction.MIDDLE) {
            return middle;
        } else {
            throw new NoSuchElementException();
        }
    }

    public boolean isRight() {
        return direction == Direction.RIGHT;
    }

    public R getRight() throws NoSuchElementException {
        if (direction == Direction.RIGHT) {
            return right;
        } else {
            throw new NoSuchElementException();
        }
    }

    public <T> T map(Function<? super L, ? extends T> fleft,
            Function<? super M, ? extends T> fmiddle,
            Function<? super R, ? extends T> fright) {
        checkNotNull(fleft);
        checkNotNull(fmiddle);
        checkNotNull(fright);

        switch (direction) {
            case LEFT: return fleft.apply(left);
            case MIDDLE: return fmiddle.apply(middle);
            case RIGHT: return fright.apply(right);
            default: throw new VerifyException("Unreachable code!");
        }
    }

    /**
     * Overrides the Comparable.compareTo() method, and offers the same return
     * guarantees.
     * @param o the object to be compared
     * @return a negative number if this object is less than o, zero if they
     * are equal, and a positive number if this object is greater than o
     * @throws NullPointerException if o is null
     */
    @Override
    public int compareTo(Either3<L, M, R> o) {
        checkNotNull(o);
        return new CompareToBuilder().append(this.direction, o.direction)
                                     .append(this.left, o.left)
                                     .append(this.middle, o.middle)
                                     .append(this.right, o.right)
                                     .toComparison();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Either3)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        Either3 objPrime = (Either3)obj;
        return Objects.equals(direction, objPrime.direction) &&
                Objects.equals(left, objPrime.left) &&
                Objects.equals(middle, objPrime.middle) &&
                Objects.equals(right, objPrime.right);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(direction)
                                    .append(left)
                                    .append(middle)
                                    .append(right)
                                    .toHashCode();
    }

    @Override
    public String toString() {
        switch (direction) {
            case LEFT: return String.format("(Either3.Left %s)", left);
            case MIDDLE: return String.format("(Either3.Middle %s)", middle);
            case RIGHT: return String.format("(Either3.Right %s)", right);
            default: throw new VerifyException("Unreachable code!");
        }
    }

}
