package edu.upenn.cis.qdrex.core.qre.arith;

import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.qre.Iter;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import edu.upenn.cis.qdrex.core.qre.UnaryOp;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermAvg;
import edu.upenn.cis.qdrex.core.term.arith.MTermSingleton;
import edu.upenn.cis.qdrex.core.term.arith.MTermUnion;
import java.util.ArrayList;
import java.util.List;

public class Avg<P extends Predicate<P, T>, T> extends UnaryOp<P, T, Multiset, Double> {

    public Avg(QRE<P, T, Multiset> f) {
        super(f, Double.class);
    }

    public static <P extends Predicate<P, T>, T> Avg<P, T> of(QRE<P, T, Double>... fs) {
        checkArgument(fs.length > 0);

        List<QRE<P, T, Multiset>> fss = new ArrayList<>();
        for (QRE<P, T, Double> f : fs) {
            fss.add(new Singleton<>(f));
        }

        QRE<P, T, Multiset> fm = fss.get(0);
        for (int i = 1; i < fss.size(); i++) {
            fm = new Union<>(fm, fss.get(i));
        }
        return new Avg<>(fm);
    }

    @Override
    public Term<Double> op(Term<Multiset> tf) {
        return ITermAvg.of(tf);
    }

    @Override
    public ImmutableSet<Parameter> getParametersInt() {
        return f.getParameters();
    }

    @Override
    public String toString() {
        return String.format("(Avg %s)", f);
    }

    public static <P extends Predicate<P, T>, T> IterAvg<P, T> iter(QRE<P, T, Double> f) {
        return new IterAvg(f);
    }

    public static class IterAvg<P extends Predicate<P, T>, T> extends Iter<P, T, Multiset, Double, Double> {

        public IterAvg(QRE<P, T, Double> f) {
            super(ConstantTerm.of(Multiset.EMPTY), f, false, Double.class);
        }

        @Override
        public Term<Multiset> combine(Term<Multiset> tr, Term<Double> tf) {
            return MTermUnion.of(tr, MTermSingleton.of(tf));
        }

        @Override
        public Term<Double> finalize(Term<Multiset> tr) {
            return ITermAvg.of(tr);
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            return f.getParameters();
        }

        @Override
        public String toString() {
            return String.format("(Avg.IterAvg %s)", f);
        }

    }

}
