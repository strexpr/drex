package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.regex.Regex;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.fj.BottomEvaluator;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;

public class Bottom<P extends Predicate<P, T>, T, C> extends QRE<P, T, C> {

    private /* mutable */ AnalysisResult<P, T, C> analysisResult;

    public Bottom(Class<C> outType) {
        super(QRE.Type.BOTTOM, checkNotNull(outType));
        this.analysisResult = null;
    }

    public static <P extends Predicate<P, T>, T> Bottom<P, T, Double> arith() {
        return new Bottom(Double.class);
    }

    public static <P extends Predicate<P, T>, T> Bottom<P, T, String> string() {
        return new Bottom(String.class);
    }

    @Override
    public int size() {
        return 1;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        return ImmutableSet.of();
    }

    @Override
    public AnalysisResult<P, T, C> fjAnalyze() {
        if (analysisResult == null) {
            Regex<P, T> r = new edu.upenn.cis.qdrex.core.regex.Bottom<>();
            analysisResult = AnalysisResult.unambiguous(r);
            verifyNotNull(analysisResult);
        }
        return analysisResult;
    }

    @Override
    public BottomEvaluator<P, T, C> buildEvaluator() {
        return new BottomEvaluator<>(this);
    }

    @Override
    public String toString() {
        return "Bottom";
    }

}
