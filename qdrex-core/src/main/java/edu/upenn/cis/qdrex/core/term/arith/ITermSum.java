package edu.upenn.cis.qdrex.core.term.arith;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.Term;
import static com.google.common.base.Verify.verifyNotNull;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.Nullable;

public class ITermSum extends Term<Double> {

    public final double acc; // accumulator
    public final Term<Double> left; // left child
    public final @Nullable Term<Double> right; // right child

    private /* mutable */ ImmutableSet<Parameter> allParameters;

    private ITermSum(double acc, Term<Double> left, Term<Double> right) {
        super(Double.class);
        this.acc = acc;
        this.left = checkNotNull(left);
        this.right = right;
        this.allParameters = null;
    }

    // Precondition: l & r are reduced.
    // Postcondition: created term is reduced.
    public static Term<Double> of(Term<Double> left, Term<Double> right) {
        return of(0, left, right);
    }

    // Precondition: r is reduced.
    // Postcondition: created term is reduced.
    public static Term<Double> of(double left, Term<Double> right) {
        return of(left, right, null);
    }

    // Precondition: r is reduced.
    // Postcondition: created term is reduced.
    public static Term<Double> of(Term<Double> left, double right) {
        return of(right, left, null);
    }

    // Precondition: l & r are reduced.
    // Postcondition: created term is reduced.
    protected static Term<Double> of(double acc, Term<Double> left, Term<Double> right) {
        if (left instanceof ConstantTerm) {
            ConstantTerm<Double> c = (ConstantTerm<Double>)left;
            acc = acc + c.eval();
            left = null;
        } else if (left instanceof ITermSum) {
            ITermSum t = (ITermSum)left;
            if (t.right == null) {
                // t = sum(a | v, _)
                acc = acc + t.acc;
                left = t.left;
            }
        }

        if (right instanceof ConstantTerm) {
            ConstantTerm<Double> c = (ConstantTerm<Double>)right;
            acc = acc + c.eval();
            right = null;
        } else if (right instanceof ITermSum) {
            ITermSum t = (ITermSum)right;
            if (t.right == null) {
                // t = sum(a | v, _)
                acc = acc + t.acc;
                right = t.left;
            }
        }

        if (left == null) {
            left = right;
            right = null;
        }

        if (right == null) {
            // sum(a | t, _)
            if (left instanceof ITermSum) {
                // sum(a | sum(b | t1, t2), _) -> sum(a + b | t1, t2)
                ITermSum t = (ITermSum)left;
                acc = acc + t.acc;
                left = t.left;
                right = t.right;
            } else if (left instanceof ITermMin) {
                // sum(a | min(b | t1, t2), _) -> min(a + b | a + t1, a + t2)
                ITermMin t = (ITermMin) left;
                double newAcc = acc + t.acc;
                Term<Double> newL = ITermSum.of(acc, t.left);
                Term<Double> newR = ITermSum.of(acc, t.right);
                return ITermMin.of(newAcc, newL, newR);
            } else if (left instanceof ITermMax) {
                // sum(a | max(b | t1, t2), _) -> max(a + b | a + t1, a + t2)
                ITermMax t = (ITermMax)left;
                double newAcc = acc + t.acc;
                Term<Double> newL = ITermSum.of(acc, t.left);
                Term<Double> newR = ITermSum.of(acc, t.right);
                return ITermMax.of(newAcc, newL, newR);
            }
        }

        if (left != null) {
            return new ITermSum(acc, left, right);
        } else if (right != null) {
            // l == null & r != null
            return new ITermSum(acc, right, null);
        } else {
            // newLeft == newRight == null
            return ConstantTerm.of(acc);
        }
    }

    @Override
    public boolean isGround() {
        return left.isGround() && (right == null || right.isGround());
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (allParameters == null) {
            Set<Parameter> ans = new HashSet<>();
            ans.addAll(left.getParameters());
            if (right != null) {
                ans.addAll(right.getParameters());
            }
            allParameters = ImmutableSet.copyOf(ans);
            verifyNotNull(allParameters);
        }
        return allParameters;
    }

    @Override
    public Double eval() {
        double res = this.acc + left.eval();
        if (right != null) {
            res = res + right.eval();
        }
        return res;
    }

    // Precondition: this & t are reduced
    // Postcondition: result is reduced
    // Only create new node if there has been a change below.
    @Override
    public <PC> Term<Double> subst(Parameter<PC> p, Term<PC> t) {
        Term<Double> newLeft = left.subst(p, t);
        Term<Double> newRight = right != null ? right.subst(p, t) : null;

        if (left != newLeft || right != newRight) {
            return of(acc, newLeft, newRight);
        } else {
            return this;
        }
    }

    @Override
    public int getBytes() {
        int leftBytes = 4 + left.getBytes();
        int rightBytes = 4 + (right != null ? right.getBytes() : 0);
        return 8 + 4 + leftBytes + rightBytes;
    }

    @Override
    public String toString() {
        String leftStr = left.toString();
        String rightStr = right == null ? "_" : right.toString();

        if (acc != 0) {
            return String.format("(ITermSum %f | %s %s)", acc, leftStr, rightStr);
        } else {
            return String.format("(ITermSum %s %s)", leftStr, rightStr);
        }
    }

}
