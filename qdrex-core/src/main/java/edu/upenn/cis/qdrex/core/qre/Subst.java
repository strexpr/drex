package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import java.util.HashSet;
import java.util.Set;
import edu.upenn.cis.qdrex.core.term.Term;
import static com.google.common.base.Preconditions.checkNotNull;

public class Subst<P extends Predicate<P, T>, T, Cf, Cg> extends BinaryOp<P, T, Cf, Cg, Cg> {

    public final Parameter<Cf> p;

    public Subst(QRE<P, T, Cf> f, Parameter<Cf> p, QRE<P, T, Cg> g) {
        super(f, g, g.outType);
        this.p = checkNotNull(p);
    }

    @Override
    public Term<Cg> op(Term<Cf> tf, Term<Cg> tg) {
        return tg.subst(p, tf);
    }

    @Override
    public ImmutableSet<Parameter> getParametersInt() {
        Set<Parameter> ans = new HashSet<>();
        ans.addAll(g.getParameters());
        ans.remove(p);
        ans.addAll(f.getParameters());
        return ImmutableSet.copyOf(ans);
    }

    @Override
    public String toString() {
        return String.format("(Subst %s %s %s)", f, p, g);
    }

}
