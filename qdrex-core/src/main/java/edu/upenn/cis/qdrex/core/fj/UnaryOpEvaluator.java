package edu.upenn.cis.qdrex.core.fj;

import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.qre.UnaryOp;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Term;

public class UnaryOpEvaluator<P extends Predicate<P, T>, T, Cf, C> extends Evaluator<P, T, C> {

    public final UnaryOp<P, T, Cf, C> unaryOp;
    private final Evaluator<P, T, Cf> m;

    public UnaryOpEvaluator(UnaryOp<P, T, Cf, C> unaryOp) {
        super(unaryOp);
        this.unaryOp = unaryOp;
        this.m = unaryOp.f.buildEvaluator();
    }

    @Override
    public void resetHandler() {
        m.reset();
    }

    @Override
    public ImmutableList<Response<P, T, C>> startHandler(int index) {
        ImmutableList<Response<P, T, Cf>> resp = m.start(index);
        return processSubResponses(resp);
    }

    @Override
    public ImmutableList<Response<P, T, C>> symbolHandler(int index, T t) {
        ImmutableList<Response<P, T, Cf>> resp = m.symbol(index, t);
        return processSubResponses(resp);
    }

    private ImmutableList<Response<P, T, C>> processSubResponses(Iterable<Response<P, T, Cf>> resp) {
        ImmutableList.Builder<Response<P, T, C>> builder = ImmutableList.builder();
        for (Response<P, T, Cf> sr : resp) {
            if (sr.isResult) {
                Term<C> result = unaryOp.op(sr.getTerm());
                builder.add(Response.result(this, sr.startIndex, result));
            } else {
                builder.add(Response.killUp(this, sr.startIndex));
            }
        }
        return builder.build();
    }

}
