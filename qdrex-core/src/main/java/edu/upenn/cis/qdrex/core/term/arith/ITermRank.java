package edu.upenn.cis.qdrex.core.term.arith;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.Term;
import static com.google.common.base.Verify.verifyNotNull;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;

public class ITermRank extends Term<Double> {

    public final Term<Multiset> M;
    public final double o; // in interval [0,1]

    private /* mutable */ ImmutableSet<Parameter> allParameters;

    private ITermRank(Term<Multiset> M, double o) {
        super(Double.class);
        checkArgument(0 <= o && o <= 1);
        this.M = checkNotNull(M);
        this.o = o;
        this.allParameters = null;
    }

    public static Term<Double> of(Term<Multiset> m, double o) {
        if (m instanceof ConstantTerm) {
            Multiset mu = m.eval();
            return ConstantTerm.of(mu.rank(o));
        } else {
            return new ITermRank(m, o);
        }
    }

    @Override
    public boolean isGround() {
        return M.isGround();
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (allParameters == null) {
            allParameters = M.getParameters();
            verifyNotNull(allParameters);
        }
        return allParameters;
    }

    @Override
    public Double eval() {
        Multiset mu = M.eval();
        double ans = mu.rank(this.o);
        return ans;
    }

    @Override
    public <PC> Term<Double> subst(Parameter<PC> p, Term<PC> t) {
        Term<Multiset> newM = M.subst(p, t);
        return newM != M ? of(newM, this.o) : this;
    }

    @Override
    public int getBytes() {
        return 4 + M.getBytes() + 8 + 4;
    }

    @Override
    public String toString() {
        return String.format("(ITermRank %f %s)", o, M);
    }

}
