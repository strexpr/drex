package edu.upenn.cis.qdrex.core.regex;

import static com.google.common.base.Verify.verifyNotNull;
import edu.upenn.cis.qdrex.core.sfa.SFA;
import edu.upenn.cis.qdrex.core.sfa.SFAUtil;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class Bottom<P extends Predicate<P, T>, T> extends Regex<P, T> {

    private /* mutable */ SFA<P, T> sfa;

    public Bottom() {
        super(Type.BOTTOM);
        this.sfa = null;
    }

    @Override
    public int size() {
        return 1;
    }

    @Override
    public <R> R invokeVisitor(RegexVisitor<P, T, R> visitor) {
        return visitor.visitBottom(this);
    }

    @Override
    public SFA<P, T> buildSFA() {
        if (sfa == null) {
            sfa = SFAUtil.buildBottomSFA();
            verifyNotNull(sfa);
        }
        return sfa;
    }

    @Override
    public String toString() {
        return "Bottom";
    }

}
