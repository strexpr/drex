package edu.upenn.cis.qdrex.core.util.collect;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import java.util.Collection;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import com.google.common.collect.AbstractIterator;

public class CollectionUtil {

    public static <T> ImmutableList<T> cons(T car, ImmutableList<T> cdr) {
        ImmutableList.Builder<T> builder = ImmutableList.builder();
        builder.add(checkNotNull(car));
        builder.addAll(checkNotNull(cdr));
        return builder.build();
    }

    public static <T> ImmutableList<T> snoc(ImmutableList<T> rest, T last) {
        ImmutableList.Builder<T> builder = ImmutableList.builder();
        builder.addAll(checkNotNull(rest));
        builder.add(checkNotNull(last));
        return builder.build();
    }

    public static <T> ImmutableList<T> append(ImmutableList<T> list1, ImmutableList<T> list2) {
        ImmutableList.Builder<T> builder = ImmutableList.builder();
        builder.addAll(checkNotNull(list1));
        builder.addAll(checkNotNull(list2));
        return builder.build();
    }

    public static <T, U> Collection<U> map(Iterable<T> input,
            Function<? super T, ? extends U> f, Collection<U> output) {
        checkNotNull(f);
        checkNotNull(output);
        for (T t : input) {
            output.add(f.apply(t));
        }
        return output;
    }

    public static <T, U> ImmutableList<U> map(Iterable<T> input, Function<? super T, U> f) {
        checkNotNull(f);

        ImmutableList.Builder<U> builder = ImmutableList.builder();
        for (T t : input) {
            builder.add(f.apply(t));
        }
        return builder.build();
    }

    public static <T, U> Collection<U> flatMap(Iterable<T> input,
            Function<? super T, Collection<? extends U>> f, Collection<U> output) {
        checkNotNull(f);
        checkNotNull(output);
        for (T t : input) {
            output.addAll(f.apply(t));
        }
        return output;
    }

    public static <T, U> ImmutableList<U> flatMap(Iterable<T> input,
            Function<? super T, Collection<U>> f) {
        checkNotNull(f);
        ImmutableList.Builder<U> builder = ImmutableList.builder();
        for (T t : input) {
            builder.addAll(f.apply(t));
        }
        return builder.build();
    }

    public static final Iterable<Integer> ALL_POSITIVE_INTEGERS =
            () -> new AbstractIterator<Integer>() {
                private int i = 0;
                @Override
                public Integer computeNext() {
                    int ans = i++;
                    verify(ans >= 0);
                    return ans;
                }
            };

}
