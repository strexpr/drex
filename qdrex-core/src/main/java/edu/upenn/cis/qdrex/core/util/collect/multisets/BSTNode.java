package edu.upenn.cis.qdrex.core.util.collect.multisets;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkElementIndex;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import java.io.Serializable;
import javax.annotation.Nullable;

public class BSTNode implements Serializable {

    public final int value;
    public final int nodeCount;
    public final @Nullable BSTNode left;
    public final @Nullable BSTNode right;

    private /* mutable */ int height;
    private /* mutable */ int size;
    private /* mutable */ int treeCount;
    public final boolean isBalanced;

    public BSTNode(int value, int nodeCount) {
        this(value, nodeCount, null, null);
    }

    private BSTNode(int value, int nodeCount, BSTNode left, BSTNode right) {
        checkArgument(nodeCount >= 0);

        this.value = value;
        this.nodeCount = nodeCount;
        this.left = left;
        this.right = right;

        this.height = -1;
        this.size = -1;
        this.treeCount = -1;

        boolean leftBal = left != null ? left.isBalanced : true;
        boolean rightBal = right != null ? right.isBalanced : true;
        int lHeight = left != null ? left.getHeight() : 0;
        int rHeight = right != null ? right.getHeight() : 0;
        this.isBalanced = leftBal && rightBal && Math.abs(lHeight - rHeight) < 2;
    }

    public static final BSTNode EMPTY = new BSTNode(0, 0);

    public int getHeight() {
        if (height <= 0) {
            int hLeft = left != null ? left.getHeight() : 0;
            int hRight = right != null ? right.getHeight() : 0;
            height = 1 + Math.max(hLeft, hRight);
            verify(height > 0);
        }
        return height;
    }

    /**
     * @return number of BST nodes starting from the present root
     */
    public int getSize() {
        if (size <= 0) {
            int sLeft = left != null ? left.getSize() : 0;
            int sRight = right != null ? right.getSize() : 0;
            size = 1 + sLeft + sRight;
            verify(size > 0);
        }
        return size;
    }

    public int getRank(double o) {
        checkArgument(o >= 0 && o <= 1);
        int n = this.getTreeCount();
        int index = (int)Math.floor(o * (n - 1));
        return this.getElement(index);
    }

    public int getElement(int index) {
        int totalCount = getTreeCount();
        checkElementIndex(index, totalCount);

        int leftCount = left != null ? left.getTreeCount() : 0;
        if (index < leftCount) {
            return left.getElement(index);
        } else if (index < leftCount + nodeCount) {
            return value;
        } else {
            verify(index >= leftCount);
            return right.getElement(index - leftCount - nodeCount);
        }
    }

    /**
     * @return total count of the multiset starting from this root
     */
    public int getTreeCount() {
        if (treeCount < 0) {
            int leftCount = left != null ? left.getTreeCount() : 0;
            int rightCount = right != null ? right.getTreeCount() : 0;
            treeCount = nodeCount + leftCount + rightCount;
            verify(treeCount >= 0);
        }
        return treeCount;
    }

    public BSTNode merge(BSTNode t) {
        checkNotNull(t);
        BSTNode ans = this.insert(t.value, t.nodeCount);
        ans = t.left != null ? ans.merge(t.left) : ans;
        ans = t.right != null ? ans.merge(t.right) : ans;
        return ans;
    }

    public BSTNode insert(int value) {
        return insert(value, 1);
    }

    public BSTNode insert(int value, int count) {
        checkArgument(count >= 0);

        if (count == 0) {
            return this;
        } else if (value == this.value) {
            return new BSTNode(value, this.nodeCount + count, left, right);
        } else if (value < this.value) {
            BSTNode newLeft = left != null ? left.insert(value, count) : new BSTNode(value, count);
            verify(left != newLeft);
            BSTNode ans = new BSTNode(this.value, this.nodeCount, newLeft, right);
            ans = ans.rebalance();
            return ans;
        } else { // value > this.value
            BSTNode newRight = right != null ? right.insert(value, count) : new BSTNode(value, count);
            verify(right != newRight);
            BSTNode ans = new BSTNode(this.value, this.nodeCount, left, newRight);
            ans = ans.rebalance();
            return ans;
        }
    }

    public BSTNode rebalance() {
        /*
        We assume that all descendants of this node are themselves balanced. We
        guarantee this by restricting all constructors to private.
        */
        verify(left == null || left.isBalanced);
        verify(right == null || right.isBalanced);

        int leftHeight = left != null ? left.getHeight() : 0;
        int rightHeight = right != null ? right.getHeight() : 0;

        if (leftHeight > rightHeight + 1) {
            verify(leftHeight >= 2 && left != null);
            BSTNode l = left;

            int llHeight = l.left != null ? l.left.getHeight() : 0;
            int lrHeight = l.right != null ? l.right.getHeight() : 0;
            verify(llHeight >= 1 || lrHeight >= 1);
            verify(l.left != null || l.right != null);

            if (llHeight >= lrHeight) {
                verify(l.left != null);
                BSTNode newRight = new BSTNode(this.value, this.nodeCount, l.right, right);
                BSTNode ans = new BSTNode(l.value, l.nodeCount, l.left, newRight);
                verify(ans.isBalanced);
                return ans;
            } else {
                verify(lrHeight > llHeight && l.right != null);
                BSTNode lr = l.right;

                BSTNode newLeft = new BSTNode(l.value, l.nodeCount, l.left, lr.left);
                BSTNode newRight = new BSTNode(this.value, this.nodeCount, lr.right, this.right);
                BSTNode ans = new BSTNode(lr.value, lr.nodeCount, newLeft, newRight);

                verify(ans.isBalanced);
                return ans;
            }
        } else if (rightHeight > leftHeight + 1) {
            verify(rightHeight >= 2 && right != null);
            BSTNode r = right;

            int rlHeight = r.left != null ? r.left.getHeight() : 0;
            int rrHeight = r.right != null ? r.right.getHeight() : 0;
            verify(rlHeight >= 1 || rrHeight >= 1);
            verify(r.left != null || r.right != null);

            if (rlHeight > rrHeight) {
                verify(r.left != null);
                BSTNode rl = r.left;

                BSTNode newLeft = new BSTNode(this.value, this.nodeCount, this.left, rl.left);
                BSTNode newRight = new BSTNode(r.value, r.nodeCount, rl.right, r.right);
                BSTNode ans = new BSTNode(rl.value, rl.nodeCount, newLeft, newRight);

                verify(ans.isBalanced);
                return ans;
            } else {
                verify(rrHeight >= rlHeight && r.right != null);
                BSTNode newLeft = new BSTNode(this.value, this.nodeCount, this.left, r.left);
                BSTNode ans = new BSTNode(r.value, r.nodeCount, newLeft, r.right);
                verify(ans.isBalanced);
                return ans;
            }
        } else {
            verify(this.isBalanced);
            return this;
        }
    }

    /**
     * @return estimate of the total bytes occupied by the BST
     */
    public int getBytes() {
        int leftBytes = 4 + (left != null ? left.getBytes() : 0);
        int rightBytes = 4 + (right != null ? right.getBytes() : 0);
        return 6 * 4 + leftBytes + rightBytes;
    }

    @Override
    public String toString() {
        double decodedValue = Approx.indexToVal(value);
        return String.format("(Histogram %s [%f : %d] %s)", left, decodedValue,
                nodeCount, right);
    }

}
