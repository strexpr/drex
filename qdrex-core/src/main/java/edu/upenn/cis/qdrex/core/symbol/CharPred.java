package edu.upenn.cis.qdrex.core.symbol;

import com.google.common.collect.ImmutableRangeSet;
import com.google.common.collect.Range;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import org.apache.commons.lang3.StringEscapeUtils;

public class CharPred extends Predicate<CharPred, Character> {

    public final ImmutableRangeSet<Character> ranges;

    public CharPred(ImmutableRangeSet<Character> ranges) {
        this.ranges = checkNotNull(ranges);
    }

    public static CharPred of(char c) {
        return new CharPred(ImmutableRangeSet.of(Range.singleton(c)));
    }

    public static CharPred span(char... cs) {
        checkArgument(cs.length % 2 == 0);

        ImmutableRangeSet.Builder<Character> ans = ImmutableRangeSet.builder();
        for (int i = 0; i < cs.length; i += 2) {
            checkArgument(cs[i] <= cs[i + 1]);
            ans.add(Range.closed(cs[i], cs[i + 1]));
        }
        return new CharPred(ans.build());
    }

    public static CharPred discrete(char... cs) {
        ImmutableRangeSet.Builder<Character> ans = ImmutableRangeSet.builder();
        for (char c : cs) {
            ans.add(Range.singleton(c));
        }
        return new CharPred(ans.build());
    }

    @Override
    public boolean test(Character t) {
        return ranges.contains(checkNotNull(t));
    }

    @Override
    public Optional<Character> getWitness() {
        for (Range<Character> range : ranges.asRanges()) {
            char lo = range.hasLowerBound() ? range.lowerEndpoint() : Character.MIN_VALUE;
            if (range.contains(lo)) {
                return Optional.of(lo);
            } else if (ranges.contains((char)(lo + 1))) {
                return Optional.of((char)(lo + 1));
            }
        }
        return Optional.absent();
    }

    @Override
    public CharPred not() {
        return new CharPred(ranges.complement());
    }

    @Override
    public CharPred and(CharPred other) {
        checkNotNull(other);

        ImmutableRangeSet.Builder<Character> ans = ImmutableRangeSet.builder();
        for (Range<Character> range : ranges.asRanges()) {
            ans.addAll(other.ranges.subRangeSet(range));
        }
        return new CharPred(ans.build());
    }

    @Override
    public String toString() {
        String ans = ranges.toString();
        ans = ans.replaceAll("\u2025", "..");
        ans = StringEscapeUtils.escapeJava(ans);
        return String.format("(CharPred %s)", ans);
    }

    public final static CharPred TRUE = new CharPred(ImmutableRangeSet.of(Range.all()));
    public final static CharPred FALSE = new CharPred(ImmutableRangeSet.of());

    public final static CharPred ALPHA = CharPred.span('A', 'Z', 'a', 'z');
    public final static CharPred UPPER_ALPHA = CharPred.span('A', 'Z');
    public final static CharPred LOWER_ALPHA = CharPred.span('a', 'z');

    public final static CharPred NUM = CharPred.span('0', '9');

    public final static CharPred ALPHA_NUM = ALPHA.or(NUM);
    public final static CharPred WORD = CharPred.of('_').or(ALPHA_NUM);

    public final static CharPred BLANK = CharPred.span(' ', ' ', '\t', '\t');
    public final static CharPred SPACES = CharPred.span('\t', '\r', ' ', ' ');

    public final static CharPred CNTRL = CharPred.span((char)0, (char)31,
            (char)127, (char)127);
    public final static CharPred GRAPH = CharPred.span((char)0x21, (char)0x7e);
    public final static CharPred PRINT = CharPred.span((char)0x20, (char)0x7e);
    public final static CharPred PUNCT = CharPred.discrete('[', ']', '!', '\"',
            '#', '$', '%', '&', '\'', '(', ')', '*', '+', ',', '.', '/', ':',
            ';', '<', '=', '>', '?', '@', '\\', '^', '_', '`', '{', '|', '}',
            '~', '-');

    public final static CharPred XDIGIT = CharPred.span('0', '9', 'A', 'F', 'a', 'f');

}
