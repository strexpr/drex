package edu.upenn.cis.qdrex.core.util;

import com.google.common.base.Function;
import java.util.NoSuchElementException;
import java.util.Objects;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import static com.google.common.base.Preconditions.checkNotNull;
import java.io.Serializable;
import javax.annotation.Nullable;
import org.apache.commons.lang3.builder.CompareToBuilder;

public final class Either<L, R> implements Serializable, Comparable<Either<L, R>> {

    public enum Direction { LEFT, RIGHT }

    public final Direction direction;
    private final @Nullable L left;
    private final @Nullable R right;

    private Either(Direction direction, L left, R right) {
        this.direction = direction;
        this.left = direction == Direction.LEFT ? checkNotNull(left) : null;
        this.right = direction == Direction.RIGHT ? checkNotNull(right) : null;
    }

    public final static <L, R> Either<L, R> left(L left) {
        return new Either(Direction.LEFT, left, null);
    }

    public final static <L, R> Either<L, R> right(R right) {
        return new Either(Direction.RIGHT, null, right);
    }

    public boolean isLeft() {
        return direction == Direction.LEFT;
    }

    public L getLeft() throws NoSuchElementException {
        if (direction == Direction.LEFT) {
            return left;
        } else {
            throw new NoSuchElementException();
        }
    }

    public boolean isRight() {
        return direction == Direction.RIGHT;
    }

    public R getRight() throws NoSuchElementException {
        if (direction == Direction.RIGHT) {
            return right;
        } else {
            throw new NoSuchElementException();
        }
    }

    public <T> T map(Function<? super L, ? extends T> fleft, Function<? super R, ? extends T> fright) {
        checkNotNull(fleft);
        checkNotNull(fright);

        if (direction == Direction.LEFT) {
            return fleft.apply(left);
        } else {
            return fright.apply(right);
        }
    }

    /**
     * Overrides the Comparable.compareTo() method, and offers the same return
     * guarantees.
     * @param o the object to be compared
     * @return a negative number if this object is less than o, zero if they
     * are equal, and a positive number if this object is greater than o
     * @throws NullPointerException if o is null
     */
    @Override
    public int compareTo(Either<L, R> o) {
        checkNotNull(o);
        return new CompareToBuilder().append(this.direction, o.direction)
                                     .append(this.left, o.left)
                                     .append(this.right, o.right)
                                     .toComparison();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Either)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        Either objPrime = (Either)obj;
        return Objects.equals(direction, objPrime.direction) &&
                Objects.equals(left, objPrime.left) &&
                Objects.equals(right, objPrime.right);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(direction)
                                    .append(left)
                                    .append(right)
                                    .toHashCode();
    }

    @Override
    public String toString() {
        if (direction == Direction.LEFT) {
            return String.format("(Either.Left %s)", left);
        } else {
            return String.format("(Either.Right %s)", right);
        }
    }

}
