package edu.upenn.cis.qdrex.core.util;

import static com.google.common.base.Preconditions.checkElementIndex;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.util.collect.LazyList;
import java.util.List;
import static org.apache.commons.lang3.ArrayUtils.toObject;

public final class LazyString implements CharSequence {

    public final LazyList<Character> charList;

    private LazyString(LazyList<Character> charList) {
        this.charList = checkNotNull(charList);
    }

    public static LazyString of(ImmutableList<Character> charList) {
        return of(LazyList.of(charList));
    }

    public static LazyString of(LazyList<Character> charList) {
        return new LazyString(charList);
    }

    public static LazyString of(String str) {
        char[] charArray = str.toCharArray();
        LazyList<Character> charList = LazyList.of(toObject(charArray));
        return new LazyString(charList);
    }

    public LazyString concat(LazyString ls) {
        return new LazyString(charList.append(ls.charList));
    }

    public static LazyString concat(LazyString str1, LazyString str2) {
        return str1.concat(str2);
    }

    @Override
    public char charAt(int index) {
        checkElementIndex(index, charList.size);
        return charList.get(index);
    }

    @Override
    public int length() {
        return charList.size;
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        List<Character> sublist = charList.subList(start, end);
        return of(ImmutableList.copyOf(sublist));
    }

    @Override
    public String toString() {
        StringBuilder ans = new StringBuilder();
        for (char c : charList) {
            ans.append(c);
        }
        return ans.toString();
    }

}
