package edu.upenn.cis.qdrex.core.term;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableSet;
import java.io.Serializable;

public abstract class Term<C> implements Serializable {

    public final Class<C> outType;

    public Term(Class<C> outType) {
        this.outType = checkNotNull(outType);
    }

    public abstract boolean isGround();
    public abstract ImmutableSet<Parameter> getParameters();
    public abstract C eval() throws IncompleteTermException;
    public abstract <PC> Term<C> subst(Parameter<PC> p, Term<PC> t);
    public abstract int getBytes();

    @Override
    public abstract String toString();

}
