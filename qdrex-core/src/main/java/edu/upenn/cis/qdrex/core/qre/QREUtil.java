package edu.upenn.cis.qdrex.core.qre;

import com.google.common.base.Function;
import edu.upenn.cis.qdrex.core.regex.Concat;
import edu.upenn.cis.qdrex.core.regex.Regex;
import edu.upenn.cis.qdrex.core.regex.RegexVisitor;
import edu.upenn.cis.qdrex.core.regex.Star;
import edu.upenn.cis.qdrex.core.regex.Union;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.util.function.ConstantFunction;
import static com.google.common.base.Preconditions.checkNotNull;

public class QREUtil {

    public static <P extends Predicate<P, T>, T, C> QRE<P, T, C>
    regexToConst(Regex<P, T> r, C c, Class<C> outType) {
        checkNotNull(r);
        checkNotNull(c);
        checkNotNull(outType);
        RegexVisitor<P, T, QRE<P, T, C>> visitor = new RegexToConstVisitor<>(c, outType);
        return visitor.visit(r);
    }

    public static <P extends Predicate<P, T>, T, C> QRE<P, T, C>
    regexToTerm(Regex<P, T> r, Term<C> t, C c, Class<C> outType) {
        QRE<P, T, C> rc = regexToConst(checkNotNull(r), checkNotNull(c), checkNotNull(outType));
        Parameter<C> p = newParam(outType, checkNotNull(t));
        Epsilon<P, T, C> et = new Epsilon<>(t);
        return new ToSplit<>(rc, p, et);
    }

    public static class RegexToConstVisitor<P extends Predicate<P, T>, T, C> extends RegexVisitor<P, T, QRE<P, T, C>> {

        public final Term<C> t;
        public final Function<T, C> f;
        public final Class<C> outType;

        public RegexToConstVisitor(C c, Class<C> outType) {
            this.t = new ConstantTerm<>(checkNotNull(c), checkNotNull(outType));
            this.f = ConstantFunction.of(c);
            this.outType = outType;
        }

        @Override
        public QRE<P, T, C> visitBase(edu.upenn.cis.qdrex.core.regex.Base<P, T> base) {
            return new Base<>(base.pred, f, outType);
        }

        @Override
        public QRE<P, T, C> visitEpsilon(edu.upenn.cis.qdrex.core.regex.Epsilon<P, T> epsilon) {
            return new Epsilon<>(t);
        }

        @Override
        public QRE<P, T, C> visitBottom(edu.upenn.cis.qdrex.core.regex.Bottom<P, T> bottom) {
            return new Bottom<>(outType);
        }

        @Override
        public QRE<P, T, C> visitUnion(Union<P, T> union) {
            QRE<P, T, C> e1 = visit(union.r1);
            QRE<P, T, C> e2 = visit(union.r2);
            return new TryElse<>(e1, e2);
        }

        @Override
        public QRE<P, T, C> visitConcat(Concat<P, T> concat) {
            QRE<P, T, C> e1 = visit(concat.r1);
            QRE<P, T, C> e2 = visit(concat.r2);
            Parameter<C> p = newParam(outType, e1, e2);
            return new ToSplit<>(e1, p, e2);
        }

        @Override
        public QRE<P, T, C> visitStar(Star<P, T> star) {
            QRE<P, T, C> ec = new Epsilon<>(t);
            QRE<P, T, C> fr = visit(star.r);
            Parameter<C> p = newParam(outType, ec, fr);
            Iter.Right<P, T, C> iter = Iter.right(p, fr);
            return new ToSplit<>(ec, p, iter);
        }

    }

    public static <P extends Predicate<P, T>, T, C, CP> Parameter<CP>
    newParam(Class<CP> paramType, QRE... es) {
        checkNotNull(es);
        for (int i = 0; true; i++) {
            Parameter<CP> pi = Parameter.of(paramType, i);

            boolean isNew = true;

            for (QRE e : es) {
                if (e.getParameters().contains(pi)) {
                    isNew = false;
                    break;
                }
            }

            if (isNew) {
                return pi;
            }
        }
    }

    public static <P extends Predicate<P, T>, T, C, CP> Parameter<CP>
    newParam(Class<CP> paramType, Term... ts) {
        checkNotNull(ts);
        for (int i = 0; true; i++) {
            Parameter<CP> pi = Parameter.of(paramType, i);

            boolean isNew = true;

            for (Term t : ts) {
                if (t.getParameters().contains(pi)) {
                    isNew = false;
                    break;
                }
            }

            if (isNew) {
                return pi;
            }
        }
    }

}
