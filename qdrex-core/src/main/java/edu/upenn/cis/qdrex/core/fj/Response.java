package edu.upenn.cis.qdrex.core.fj;

import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Term;
import java.util.NoSuchElementException;
import static com.google.common.base.Preconditions.checkNotNull;

public abstract class Response<P extends Predicate<P, T>, T, C> {

    public final Evaluator<P, T, C> evaluator;
    public final int startIndex;
    public final boolean isResult, isKillUp;

    private Response(Evaluator<P, T, C> evaluator, int startIndex, boolean isResult) {
        this.evaluator = checkNotNull(evaluator);
        this.startIndex = startIndex;
        this.isResult = isResult;
        this.isKillUp = !isResult;
    }

    public abstract Term<C> getTerm() throws NoSuchElementException;

    public static <P extends Predicate<P, T>, T, C> Result<P, T, C>
    result(Evaluator<P, T, C> evaluator, int startIndex, Term<C> result) {
        return new Result<>(evaluator, startIndex, result);
    }

    public static <P extends Predicate<P, T>, T, C> KillUp<P, T, C>
    killUp(Evaluator<P, T, C> evaluator, int startIndex) {
        return new KillUp<>(evaluator, startIndex);
    }

    public static final class Result<P extends Predicate<P, T>, T, C> extends Response<P, T, C> {

        public final Term<C> term;

        public Result(Evaluator<P, T, C> evaluator, int startIndex, Term<C> term) {
            super(evaluator, startIndex, true);
            this.term = checkNotNull(term);
        }

        @Override
        public Term<C> getTerm() {
            return term;
        }

        @Override
        public String toString() {
            return String.format("(Result %d %s %s)", startIndex, term, evaluator);
        }

    }

    public static final class KillUp<P extends Predicate<P, T>, T, C> extends Response<P, T, C> {

        public KillUp(Evaluator<P, T, C> evaluator, int startIndex) {
            super(evaluator, startIndex, false);
        }

        @Override
        public Term<C> getTerm() throws NoSuchElementException {
            throw new NoSuchElementException();
        }

        @Override
        public String toString() {
            return String.format("(KillUp %d %s)", startIndex, evaluator);
        }

    }

}
