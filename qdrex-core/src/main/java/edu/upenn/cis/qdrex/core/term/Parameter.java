package edu.upenn.cis.qdrex.core.term;

import java.util.Objects;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import static com.google.common.base.Preconditions.checkNotNull;

public abstract class Parameter<C> {

    public final Class<C> parType;

    public Parameter(Class<C> parType) {
        this.parType = checkNotNull(parType);
    }

    @Override
    public abstract boolean equals(Object obj);

    @Override
    public abstract int hashCode();

    public static <C, T> WrapperParameter<C, T> of(Class parType, T t) {
        return new WrapperParameter<>(parType, t);
    }

    public static class WrapperParameter<C, T> extends Parameter<C> {

        public final T t;

        public WrapperParameter(Class parType, T t) {
            super(parType);
            this.t =  checkNotNull(t);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof WrapperParameter)) {
                return false;
            } else if (obj == this) {
                return true;
            }

            WrapperParameter<?, ?> wo = (WrapperParameter<?, ?>)obj;
            return Objects.equals(parType, wo.parType) && Objects.equals(t, wo.t);
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(parType)
                                        .append(t)
                                        .build();
        }

        @Override
        public String toString() {
            return String.format("(WrapperParameter %s)", t);
        }

    }

}
