package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.regex.Regex;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import java.util.HashSet;
import com.google.common.base.Optional;
import java.util.Set;
import edu.upenn.cis.qdrex.core.fj.TryElseEvaluator;
import java.util.Objects;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Verify.verifyNotNull;

public class TryElse<P extends Predicate<P, T>, T, C> extends QRE<P, T, C> {

    public final QRE<P, T, C> f, g;

    private /* mutable */ int size;
    private /* mutable */ ImmutableSet<Parameter> parameters;
    private /* mutable */ AnalysisResult<P, T, C> analysisResult;

    public TryElse(QRE<P, T, C> f, QRE<P, T, C> g) {
        super(Type.TRYELSE, f.outType);
        checkArgument(Objects.equals(f.outType, g.outType));
        this.f = f;
        this.g = g;

        this.size = -1;
        this.parameters = null;
        this.analysisResult = null;
    }

    @Override
    public int size() {
        if (size < 0) {
            size = 1 + f.size() + g.size();
        }
        return size;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (parameters == null) {
            Set<Parameter> ans = new HashSet<>();
            ans.addAll(f.getParameters());
            ans.addAll(g.getParameters());
            parameters = ImmutableSet.copyOf(ans);
        }
        return parameters;
    }

    @Override
    public AnalysisResult<P, T, C> fjAnalyze() {
        if (analysisResult == null) {
            analysisResult = fjAnalyzeInt();
            verifyNotNull(analysisResult);
        }
        return analysisResult;
    }

    public AnalysisResult<P, T, C> fjAnalyzeInt() {
        AnalysisResult<P, T, C> a1 = f.fjAnalyze();
        if (!a1.isUnambiguous()) {
            return AnalysisResult.subexprError(this, a1.getAnalysisError());
        }

        AnalysisResult<P, T, C> a2 = g.fjAnalyze();
        if (!a2.isUnambiguous()) {
            return AnalysisResult.subexprError(this, a2.getAnalysisError());
        }

        Regex<P, T> r1 = a1.getDomainType();
        Regex<P, T> r2 = a2.getDomainType();
        Regex<P, T> r1r2 = r1.union(r2);

        Optional<ImmutableList<T>> ambiguousWitness = r1r2.getAmbiguousWitness();
        if (ambiguousWitness.isPresent()) {
            String msg = String.format("Both subexpressions are defined on "
                    + "input %s.", ambiguousWitness.get());
            return AnalysisResult.baseError(this, msg);
        }

        return AnalysisResult.unambiguous(r1r2);
    }

    @Override
    public TryElseEvaluator<P, T, C> buildEvaluator() {
        return new TryElseEvaluator<>(this);
    }

    @Override
    public String toString() {
        return String.format("(TryElse %s %s)", f, g);
    }

}
