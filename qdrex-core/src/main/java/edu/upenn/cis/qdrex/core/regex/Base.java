package edu.upenn.cis.qdrex.core.regex;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verifyNotNull;
import edu.upenn.cis.qdrex.core.sfa.SFA;
import edu.upenn.cis.qdrex.core.sfa.SFAUtil;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class Base<P extends Predicate<P, T>, T> extends Regex<P, T> {

    public final P pred;

    private /* mutable */ SFA<P, T> sfa;

    public Base(P pred) {
        super(Type.BASE);
        this.pred = checkNotNull(pred);
        this.sfa = null;
    }

    @Override
    public int size() {
        return 1;
    }

    @Override
    public <R> R invokeVisitor(RegexVisitor<P, T, R> visitor) {
        return visitor.visitBase(this);
    }

    @Override
    public SFA<P, T> buildSFA() {
        if (sfa == null) {
            sfa = SFAUtil.buildBaseSFA(pred);
            verifyNotNull(sfa);
        }
        return sfa;
    }

    @Override
    public String toString() {
        return String.format("(Base %s)", pred);
    }

}
