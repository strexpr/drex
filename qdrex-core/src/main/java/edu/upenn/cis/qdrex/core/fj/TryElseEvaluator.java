package edu.upenn.cis.qdrex.core.fj;

import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.qre.TryElse;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import java.util.HashSet;
import java.util.Set;
import static com.google.common.base.Verify.verify;

public class TryElseEvaluator<P extends Predicate<P, T>, T, C> extends Evaluator<P, T, C> {

    public final TryElse<P, T, C> tryElse;
    private final Evaluator<P, T, C> mf, mg;
    private final Set<Integer> thf, thg;

    public TryElseEvaluator(TryElse<P, T, C> tryElse) {
        super(tryElse);
        this.tryElse = tryElse;
        this.mf = tryElse.f.buildEvaluator();
        this.mg = tryElse.g.buildEvaluator();
        this.thf = new HashSet<>();
        this.thg = new HashSet<>();
    }

    @Override
    public void resetHandler() {
        thf.clear();
        thg.clear();
        mf.reset();
        mg.reset();
    }

    @Override
    public ImmutableList<Response<P, T, C>> startHandler(int index) {
        thf.add(index);
        thg.add(index);

        ImmutableList<Response<P, T, C>> respf = mf.start(index);
        ImmutableList<Response<P, T, C>> respg = mg.start(index);

        ImmutableList.Builder<Response<P, T, C>> builder = ImmutableList.builder();
        processSubResponses(respf, thf, thg, builder);
        processSubResponses(respg, thg, thf, builder);
        return builder.build();
    }

    @Override
    public ImmutableList<Response<P, T, C>> symbolHandler(int index, T t) {
        ImmutableList<Response<P, T, C>> respf = mf.symbol(index, t);
        ImmutableList<Response<P, T, C>> respg = mg.symbol(index, t);

        ImmutableList.Builder<Response<P, T, C>> builder = ImmutableList.builder();
        processSubResponses(respf, thf, thg, builder);
        processSubResponses(respg, thg, thf, builder);
        return builder.build();
    }

    private void processSubResponses(ImmutableList<Response<P, T, C>> subResponses,
            Set<Integer> thisTh, Set<Integer> thatTh,
            ImmutableList.Builder<Response<P, T, C>> builder) {
        for (Response<P, T, C> subResponse : subResponses) {
            processSubResponse(subResponse, thisTh, thatTh, builder);
        }
    }

    private void processSubResponse(Response<P, T, C> subResponse,
            Set<Integer> thisTh, Set<Integer> thatTh,
            ImmutableList.Builder<Response<P, T, C>> builder) {
        if (subResponse.isResult) {
            builder.add(Response.result(this, subResponse.startIndex, subResponse.getTerm()));
        } else {
            verify(subResponse.isKillUp);
            thisTh.remove(subResponse.startIndex);
            if (!thatTh.contains(subResponse.startIndex)) {
                builder.add(Response.killUp(this, subResponse.startIndex));
            }
        }
    }

}
