package edu.upenn.cis.qdrex.core.sfa;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableTable;
import com.google.common.collect.Sets;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import java.util.Set;
import java.util.Collection;
import java.util.Stack;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Table;
import edu.upenn.cis.qdrex.core.symbol.MinTerm;
import edu.upenn.cis.qdrex.core.util.collect.CollectionUtil;
import static edu.upenn.cis.qdrex.core.util.collect.CollectionUtil.snoc;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import com.google.common.base.Optional;
import org.apache.commons.lang3.tuple.ImmutablePair;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import java.io.Serializable;

public class SFA<P extends Predicate<P, T>, T> implements Serializable {

    public final ImmutableSet<State> states;
    public final transient ImmutableTable<State, P, ImmutableSet<State>> transitions;
    public final ImmutableMap<State, ImmutableSet<State>> epsilonTransitions;
    public final ImmutableSet<State> initialStates;
    public final ImmutableSet<State> finalStates;

    private final /* non-frozen */ Map<State, ImmutableSet<State>> epsilonClosureMemo;
    private /* mutable */ ImmutableMap<State, ImmutableList<T>> reachableStates;

    public SFA(ImmutableSet<State> states,
            ImmutableTable<State, P, ImmutableSet<State>> transitions,
            ImmutableMap<State, ImmutableSet<State>> epsilonTransitions)
    throws IllegalArgumentException {
        this.states = checkNotNull(states);
        this.transitions = checkNotNull(transitions);
        this.epsilonTransitions = checkNotNull(epsilonTransitions);

        ImmutableSet.Builder<State> initialStatesBuilder = ImmutableSet.builder();
        ImmutableSet.Builder<State> finalStatesBuilder = ImmutableSet.builder();
        for (State q : states) {
            if (q.isInitial) {
                initialStatesBuilder.add(q);
            }

            if (q.isFinal) {
                finalStatesBuilder.add(q);
            }
        }
        this.initialStates = initialStatesBuilder.build();
        this.finalStates = finalStatesBuilder.build();

        this.epsilonClosureMemo = new HashMap<>();
        this.reachableStates = null;
    }

    public ImmutableMap<P, ImmutableSet<State>> getTransitions(State q) {
        if (transitions.containsRow(checkNotNull(q))) {
            return transitions.row(q);
        } else {
            return ImmutableMap.of();
        }
    }

    public ImmutableSet<State> getEpsilonTransitions(State q) {
        if (epsilonTransitions.containsKey(checkNotNull(q))) {
            return epsilonTransitions.get(q);
        } else {
            return ImmutableSet.of();
        }
    }

    /**
     * @param truePred predicate which is true of every input
     * @return all mutually distinguishable symbol classes considered by this
     * automaton.
     */
    public ImmutableSet<P> getMinTerms(P truePred) {
        checkNotNull(truePred);

        ImmutableSet<P> predicates = transitions.columnKeySet();
        ImmutableSet<MinTerm<P, T>> minTerms = Predicate.buildMinTerms(truePred, predicates);

        ImmutableSet.Builder<P> ans = ImmutableSet.builder();
        for (MinTerm<P, T> minTerm : minTerms) {
            ans.add(minTerm.pred);
        }
        return ans.build();
    }

    /**
     * Given a source state <code>src</code>, and a symbol predicate
     * <code>pred</code>, returns all potential successor states <code>q</code>.
     * @param src source state
     * @param pred predicate
     * @return
     */
    public ImmutableSet<State> getSuccessors(State src, P pred) {
        checkNotNull(src);
        checkNotNull(pred);

        ImmutableSet<State> srce = epsilonClosure(src);
        ImmutableSet.Builder<State> ans = ImmutableSet.builder();
        for (State q1 : srce) {
            ImmutableMap<P, ImmutableSet<State>> tq = transitions.row(q1);
            for (P p : tq.keySet()) {
                Optional<T> predPW = Predicate.and(pred, p).getWitness();
                if (predPW.isPresent()) {
                    ans.addAll(epsilonClosure(tq.get(p)));
                }
            }
        }
        return ans.build();
    }

    public boolean isAccepting(Collection<? extends State> states) {
        for (State q : states) {
            if (finalStates.contains(q)) {
                return true;
            }
        }
        return false;
    }

    public boolean accepts(T... ts) {
        return accepts(ImmutableList.copyOf(ts));
    }

    public boolean accepts(Iterable<? extends T> w) {
        ImmutableSet<State> curr = epsilonClosure(initialStates);
        for (T t : w) {
            ImmutableSet.Builder<State> currPrime = ImmutableSet.builder();
            for (State q : curr) {
                ImmutableMap<P, ImmutableSet<State>> tq = getTransitions(q);
                for (P p : tq.keySet()) {
                    if (p.test(t)) {
                        ImmutableSet<State> qpSucc = tq.get(p);
                        qpSucc = epsilonClosure(qpSucc);
                        currPrime.addAll(qpSucc);
                    }
                }
            }
            curr = currPrime.build();
        }
        return isAccepting(curr);
    }

    public ImmutableSet<State> epsilonClosure(Collection<? extends State> curr) {
        ImmutableSet.Builder<State> ans = ImmutableSet.builder();
        for (State q : curr) {
            ans.addAll(epsilonClosure(q));
        }
        return ans.build();
    }

    public ImmutableSet<State> epsilonClosure(State seedState) {
        checkNotNull(seedState);

        if (epsilonClosureMemo.containsKey(seedState)) {
            return epsilonClosureMemo.get(seedState);
        }

        Set<State> ans = Sets.newHashSet(seedState);
        Stack<State> unexplored = new Stack<>();
        unexplored.push(seedState);

        while (!unexplored.isEmpty()) {
            State q = unexplored.pop();
            ImmutableSet<State> qe = getEpsilonTransitions(q);
            for (State qPrime : qe) {
                if (!ans.contains(qPrime)) {
                    ans.add(qPrime);
                    unexplored.push(qPrime);
                }
            }
        }

        ImmutableSet<State> immutableAns = ImmutableSet.copyOf(ans);
        epsilonClosureMemo.put(seedState, immutableAns);
        return immutableAns;
    }

    public ImmutableMap<State, ImmutableList<T>> getReachableStates() {
        if (reachableStates != null) {
            return reachableStates;
        }

        Map<State, ImmutableList<T>> ans = new HashMap<>();
        for (State q : initialStates) {
            ans.put(q, ImmutableList.of());
        }
        Stack<State> unexplored = new Stack<>();
        unexplored.addAll(initialStates);

        while (!unexplored.isEmpty()) {
            State q = unexplored.pop();
            ImmutableList<T> qw = ans.get(q);

            ImmutableSet<State> qe = epsilonClosure(q);
            for (State qPrime : qe) {
                if (!ans.containsKey(qPrime)) {
                    ans.put(qPrime, qw);
                    unexplored.push(qPrime);
                }
            }

            ImmutableMap<P, ImmutableSet<State>> tq = getTransitions(q);
            for (P p : tq.keySet()) {
                Optional<T> pw = p.getWitness();
                if (pw.isPresent()) {
                    for (State qPrime : tq.get(p)) {
                        if (!ans.containsKey(qPrime)) {
                            ans.put(qPrime, CollectionUtil.snoc(qw, pw.get()));
                            unexplored.push(qPrime);
                        }
                    }
                }
            }
        }

        reachableStates = ImmutableMap.copyOf(ans);
        return reachableStates;
    }

    public Optional<ImmutableList<T>> getAmbiguousWitness() {
        Table<State, State, ImmutableList<T>> witnesses = HashBasedTable.create();

        // Every pair of unequal initial states is ambiguously reachble
        for (State q1 : epsilonClosure(initialStates)) {
            for (State q2 : epsilonClosure(initialStates)) {
                if (!q1.equals(q2)) {
                    witnesses.put(q1, q2, ImmutableList.of());
                }
            }
        }

        // For every reachable state, ...
        for (Entry<State, ImmutableList<T>> qw : getReachableStates().entrySet()) {
            State q = qw.getKey();
            ImmutableList<T> w = qw.getValue();
            ImmutableMap<P, ImmutableSet<State>> tq = getTransitions(q);

            // ... every pair of unequal states coming out of a satisfiable
            // transition is ambiguously reachable.
            for (P p : tq.keySet()) {
                Optional<T> pw = p.getWitness();
                if (pw.isPresent()) {
                    ImmutableList<T> wpw = snoc(w, pw.get());
                    for (State qp1 : epsilonClosure(tq.get(p))) {
                        for (State qp2 : epsilonClosure(tq.get(p))) {
                            if (!qp1.equals(qp2)) {
                                witnesses.put(qp1, qp2, wpw);
                            }
                        }
                    }
                }
            }

            // ... every pair of (possibly equal) states coming out of two
            // simultaneously satisfiable transitions is ambiguously reachable.
            for (P p1 : tq.keySet()) {
                for (P p2 : tq.keySet()) {
                    if (!p1.equals(p2)) {
                        P p1p2 = Predicate.and(p1, p2);
                        Optional<T> p1p2w = p1p2.getWitness();
                        if (p1p2w.isPresent()) {
                            ImmutableList<T> wp1p2w = snoc(w, p1p2w.get());
                            for (State q1 : epsilonClosure(tq.get(p1))) {
                                for (State q2 : epsilonClosure(tq.get(p2))) {
                                    witnesses.put(q1, q2, wp1p2w);
                                }
                            }
                        }
                    }
                }
            }
        }

        Stack<ImmutablePair<State, State>> unexplored = new Stack<>();
        for (State q1 : witnesses.rowKeySet()) {
            for (State q2 : witnesses.row(q1).keySet()) {
                unexplored.push(ImmutablePair.of(q1, q2));
            }
        }

        // For every pair of ambiguously reachable states q1 and q2, ...
        while (!unexplored.isEmpty()) {
            ImmutablePair<State, State> q1q2 = unexplored.pop();

            State q1 = q1q2.left;
            ImmutableMap<P, ImmutableSet<State>> tq1 = getTransitions(q1);

            State q2 = q1q2.right;
            ImmutableMap<P, ImmutableSet<State>> tq2 = getTransitions(q2);

            ImmutableList<T> w = witnesses.get(q1, q2);

            // ... and every pair of simultaneously satisfiable outgoing
            // transitions p1 and p2 from q1 and q2 respectively, ...
            for (P p1 : tq1.keySet()) {
                for (P p2 : tq2.keySet()) {
                    P p1p2 = Predicate.and(p1, p2);
                    Optional<T> p1p2w = p1p2.getWitness();
                    if (p1p2w.isPresent()) {
                        ImmutableList<T> wp1p2w = snoc(w, p1p2w.get());
                        // ... every pair of subsequent states is ambiguously
                        // reachable.
                        for (State q1n : epsilonClosure(tq1.get(p1))) {
                            for (State q2n : epsilonClosure(tq2.get(p2))) {
                                if (!witnesses.contains(q1n, q2n)) {
                                    witnesses.put(q1n, q2n, wp1p2w);
                                    unexplored.push(ImmutablePair.of(q1n, q2n));
                                }
                            }
                        }
                    }
                }
            }
        }

        // The automaton is ambiguous iff there is a pair of final states which
        // are ambiguously reachable.
        for (State q1 : finalStates) {
            for (State q2 : finalStates) {
                if (witnesses.contains(q1, q2)) {
                    return Optional.of(witnesses.get(q1, q2));
                }
            }
        }
        return Optional.absent();
    }

    public static <P extends Predicate<P, T>, T> Optional<ImmutableList<T>>
    getInequivalenceWitness(SFA<P, T> sfa1, SFA<P, T> sfa2) {
        checkNotNull(sfa1);
        checkNotNull(sfa2);

        if (sfa1.transitions.isEmpty() && sfa2.transitions.isEmpty()) {
            ImmutableList<T> epsilon = ImmutableList.of();
            if (sfa1.accepts(epsilon) == sfa2.accepts(epsilon)) {
                return Optional.absent();
            } else {
                return Optional.of(epsilon);
            }
        }

        P somePred = Sets.union(sfa1.transitions.columnKeySet(), sfa2.transitions.columnKeySet()).iterator().next();
        P truePred = somePred.or(somePred.not());
        ImmutableSet<P> min1 = sfa1.getMinTerms(truePred);
        ImmutableSet<P> min2 = sfa2.getMinTerms(truePred);
        ImmutableSet<MinTerm<P, T>> minTerms = Predicate.buildMinTerms(truePred, Sets.union(min1, min2));

        ImmutableSet<State> initial1 = sfa1.epsilonClosure(sfa1.initialStates);
        ImmutableSet<State> initial2 = sfa2.epsilonClosure(sfa2.initialStates);

        if (sfa1.isAccepting(initial1) != sfa2.isAccepting(initial2)) {
            return Optional.of(ImmutableList.of());
        }

        Table<ImmutableSet<State>, ImmutableSet<State>, ImmutableList<T>>
                witnesses = HashBasedTable.create();
        witnesses.put(initial1, initial2, ImmutableList.of());

        Stack<ImmutablePair<ImmutableSet<State>, ImmutableSet<State>>>
                unexplored = new Stack<>();
        unexplored.push(ImmutablePair.of(initial1, initial2));

        while (!unexplored.isEmpty()) {
            ImmutableSet<State> sq1 = unexplored.peek().left;
            ImmutableSet<State> sq2 = unexplored.peek().right;
            unexplored.pop();
            ImmutableList<T> w = witnesses.get(sq1, sq2);

            for (MinTerm<P, T> mt : minTerms) {
                P p = mt.pred;
                Optional<T> pw = p.getWitness();
                verify(pw.isPresent());
                T t = pw.get();
                ImmutableList<T> wt = snoc(w, t);

                ImmutableSet.Builder<State> sq1SuccBuilder = ImmutableSet.builder();
                for (State q1 : sq1) {
                    sq1SuccBuilder.addAll(sfa1.getSuccessors(q1, p));
                }
                ImmutableSet<State> sq1Succ = sq1SuccBuilder.build();

                ImmutableSet.Builder<State> sq2SuccBuilder = ImmutableSet.builder();
                for (State q2 : sq2) {
                    sq2SuccBuilder.addAll(sfa2.getSuccessors(q2, p));
                }
                ImmutableSet<State> sq2Succ = sq2SuccBuilder.build();

                if (sfa1.isAccepting(sq1Succ) != sfa2.isAccepting(sq2Succ)) {
                    return Optional.of(wt);
                }

                if (!witnesses.contains(sq1Succ, sq2Succ)) {
                    witnesses.put(sq1Succ, sq2Succ, wt);
                    unexplored.push(ImmutablePair.of(sq1Succ, sq2Succ));
                }
            }
        }

        return Optional.absent();
    }

}
