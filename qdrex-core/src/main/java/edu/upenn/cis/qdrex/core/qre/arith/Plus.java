package edu.upenn.cis.qdrex.core.qre.arith;

import static com.google.common.base.Verify.verifyNotNull;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import com.google.common.collect.Sets;
import edu.upenn.cis.qdrex.core.qre.BinaryOp;
import edu.upenn.cis.qdrex.core.qre.Epsilon;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermSum;
import edu.upenn.cis.qdrex.core.qre.Iter;
import edu.upenn.cis.qdrex.core.qre.Split;

public class Plus<P extends Predicate<P, T>, T> extends BinaryOp<P, T, Double, Double, Double> {

    public Plus(QRE<P, T, Double> f, QRE<P, T, Double> g) {
        super(f, g, g.outType);
    }

    @Override
    public Term<Double> op(Term<Double> tf, Term<Double> tg) {
        return ITermSum.of(tf, tg);
    }

    @Override
    public ImmutableSet<Parameter> getParametersInt() {
        return Sets.union(f.getParameters(), g.getParameters()).immutableCopy();
    }

    @Override
    public String toString() {
        return String.format("(Plus %s %s)", f, g);
    }

    public static <P extends Predicate<P, T>, T> IterPlus<P, T>
    iterPlus(QRE<P, T, Double> f, boolean emptyDefined) {
        return new IterPlus(f, emptyDefined);
    }

    public static class IterPlus<P extends Predicate<P, T>, T> extends Iter<P, T, Double, Double, Double> {

        public IterPlus(QRE<P, T, Double> f, boolean emptyDefined) {
            super(ConstantTerm.of(0), f, emptyDefined, Double.class);
        }

        @Override
        public Term<Double> combine(Term<Double> tr, Term<Double> tf) {
            return ITermSum.of(tr, tf);
        }

        @Override
        public Term<Double> finalize(Term<Double> tr) {
            return tr;
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            return f.getParameters();
        }

        @Override
        public String toString() {
            return String.format("(Plus.IterPlus %s)", f);
        }

    }

    public static <P extends Predicate<P, T>, T> QRE<P, T, Double> splitPlus(QRE<P, T, Double>... es) {
        boolean first = true;
        QRE<P, T, Double> ans = new Epsilon<>(ConstantTerm.of(0));
        for (QRE<P, T, Double> e : es) {
            ans = first ? e : new SplitPlus(ans, e);
            first = false;
        }
        return ans;
    }

    public static class SplitPlus<P extends Predicate<P, T>, T>
    extends Split<P, T, Double, Double, Double> {

        private /* mutable */ ImmutableSet<Parameter> parameters;

        public SplitPlus(QRE<P, T, Double> f, QRE<P, T, Double> g) {
            super(f, g, g.outType);
            this.parameters = null;
        }

        @Override
        public Term<Double> combine(Term<Double> tf, Term<Double> tg) {
            return ITermSum.of(tf, tg);
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            if (parameters == null) {
                parameters = Sets.union(f.getParameters(), g.getParameters()).immutableCopy();
                verifyNotNull(parameters);
            }
            return parameters;
        }

        @Override
        public String toString() {
            return String.format("(Plus.SplitPlus %s %s)", f, g);
        }

    }

}
