package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import java.util.HashSet;
import java.util.Set;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.qdrex.core.term.Term;

public class FromSplit<P extends Predicate<P, T>, T, Cf, Cg> extends Split<P, T, Cf, Cg, Cf> {

    public final Parameter<Cg> p;

    private /* mutable */ ImmutableSet<Parameter> parameters;

    public FromSplit(QRE<P, T, Cf> f, Parameter<Cg> p, QRE<P, T, Cg> g) {
        super(f, g, f.outType);
        this.p = checkNotNull(p);
        this.parameters = null;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (parameters == null) {
            Set<Parameter> ans = new HashSet<>();
            ans.addAll(f.getParameters());
            ans.remove(p);
            ans.addAll(g.getParameters());
            parameters = ImmutableSet.copyOf(ans);
        }
        return parameters;
    }

    @Override
    public Term<Cf> combine(Term<Cf> tf, Term<Cg> tg) {
        return tf.subst(p, tg);
    }

    @Override
    public String toString() {
        return String.format("(FromSplit %s <%s %s)", f, p, g);
    }

}
