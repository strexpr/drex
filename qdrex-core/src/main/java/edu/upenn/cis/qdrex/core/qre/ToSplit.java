package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import java.util.HashSet;
import java.util.Set;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.qdrex.core.term.Term;

public class ToSplit<P extends Predicate<P, T>, T, Cf, Cg> extends Split<P, T, Cf, Cg, Cg> {

    public final Parameter<Cf> p;

    private /* mutable */ ImmutableSet<Parameter> parameters;

    public ToSplit(QRE<P, T, Cf> f, Parameter<Cf> p, QRE<P, T, Cg> g) {
        super(f, g, g.outType);
        this.p = checkNotNull(p);
        this.parameters = null;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (parameters == null) {
            Set<Parameter> ans = new HashSet<>();
            ans.addAll(g.getParameters());
            ans.remove(p);
            ans.addAll(f.getParameters());
            parameters = ImmutableSet.copyOf(ans);
        }
        return parameters;
    }

    @Override
    public Term<Cg> combine(Term<Cf> tf, Term<Cg> tg) {
        return tg.subst(p, tf);
    }

    @Override
    public String toString() {
        return String.format("(ToSplit %s %s> %s)", f, p, g);
    }

}
