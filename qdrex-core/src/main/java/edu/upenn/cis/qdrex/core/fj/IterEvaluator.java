package edu.upenn.cis.qdrex.core.fj;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Table;
import edu.upenn.cis.qdrex.core.qre.Iter;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Term;
import java.util.Set;
import static com.google.common.base.Verify.verify;

public class IterEvaluator<P extends Predicate<P, T>, T, Cr, Cf, C> extends Evaluator<P, T, C> {

    public final Iter<P, T, Cr, Cf, C> iter2;
    public final Evaluator<P, T, Cf> mf;

    private final Table<Integer, Integer, Term<Cr>> th;

    public IterEvaluator(Iter<P, T, Cr, Cf, C> iter) {
        super(iter);
        this.iter2 = iter;
        this.mf = iter.f.buildEvaluator();
        this.th = HashBasedTable.create();
    }

    @Override
    public void resetHandler() {
        mf.reset();
        th.clear();
    }

    @Override
    public ImmutableList<Response<P, T, C>> startHandler(int index) {
        th.put(index, index, iter2.initializer);
        ImmutableList<Response<P, T, Cf>> subResp = mf.start(index);
        verify(subResp.isEmpty());

        if (iter2.emptyDefined) {
            Term<Cr> tr0 = iter2.initializer;
            Term<C> result0 = iter2.finalize(tr0);
            return ImmutableList.of(Response.result(this, index, result0));
        } else {
            return ImmutableList.of();
        }
    }

    @Override
    public ImmutableList<Response<P, T, C>> symbolHandler(int index, T t) {
        ImmutableList<Response<P, T, Cf>> subResp = mf.symbol(index, t);

        ImmutableList.Builder<Response<P, T, C>> builder = ImmutableList.builder();
        for (Response<P, T, Cf> r : subResp) {
            if (r.isResult) {
                Term<Cf> newRes = r.getTerm();
                for (int i : th.column(r.startIndex).keySet()) {
                    Term<Cr> oldRes = th.get(i, r.startIndex);
                    Term<Cr> res = iter2.combine(oldRes, newRes);
                    th.put(i, index, res);
                    Term<C> resC = iter2.finalize(res);
                    builder.add(Response.result(this, i, resC));
                }
                ImmutableList<Response<P, T, Cf>> respPrime = mf.start(index);
                verify(respPrime.isEmpty());
            } else {
                verify(r.isKillUp);
                Set<Integer> killRing = th.column(r.startIndex).keySet();
                for (int i : ImmutableSet.copyOf(killRing)) {
                    th.remove(i, r.startIndex);
                    if (!th.containsRow(i)) {
                        builder.add(Response.killUp(this, i));
                    }
                }
            }
        }
        return builder.build();
    }

}
