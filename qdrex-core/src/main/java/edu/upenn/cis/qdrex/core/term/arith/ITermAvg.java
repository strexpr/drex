package edu.upenn.cis.qdrex.core.term.arith;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.Term;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;

public class ITermAvg extends Term<Double> {

    public final Term<Multiset> multiset;

    private ITermAvg(Term<Multiset> m) {
        super(Double.class);
        this.multiset = checkNotNull(m);
    }

    public static Term<Double> of(Term<Multiset> m) {
        checkNotNull(m);
        if (m instanceof ConstantTerm) {
            Multiset mu = m.eval();
            return ConstantTerm.of(mu.average());
        } else {
            return new ITermAvg(m);
        }
    }

    @Override
    public boolean isGround() {
        return multiset.isGround();
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        return multiset.getParameters();
    }

    @Override
    public Double eval() {
        Multiset mu = multiset.eval();
        return mu.average();
    }

    @Override
    public <PC> Term<Double> subst(Parameter<PC> p, Term<PC> t) {
        Term<Multiset> newM = multiset.subst(p, t);
        return newM != multiset ? ITermAvg.of(newM) : this;
    }

    @Override
    public int getBytes() {
        return 4 + multiset.getBytes();
    }

    @Override
    public String toString() {
        return String.format("(ITermAvg %s)", multiset.toString());
    }

}
