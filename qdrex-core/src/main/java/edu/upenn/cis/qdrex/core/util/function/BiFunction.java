package edu.upenn.cis.qdrex.core.util.function;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkNotNull;

@FunctionalInterface
public interface BiFunction<T, U, R> extends java.util.function.BiFunction<T, U, R> {

    @Override
    public abstract R apply(T t, U u);

    public default Function<U, R> partialApply(T t) {
        return u -> apply(t, u);
    }

    public default Function<T, R> partialApplyRight(U u) {
        return t -> apply(t, u);
    }

    public default <V> BiFunction<T, U, V> andThen(Function<? super R, ? extends V> after) {
        checkNotNull(after);
        return (t, u) -> {
            R r = apply(t, u);
            V ans = after.apply(r);
            return ans;
        };
    }

    public static <T, U, R> Function<U, R> partialApply(BiFunction<T, U, R> f, T t) {
        return f.partialApply(t);
    }

    public static <T, U, R> Function<T, R> partialApplyRight(BiFunction<T, U, R> f, U u) {
        return f.partialApplyRight(u);
    }

}
