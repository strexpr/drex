package edu.upenn.cis.qdrex.core.qre.arith;

import static com.google.common.base.Verify.verifyNotNull;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import com.google.common.collect.Sets;
import edu.upenn.cis.qdrex.core.qre.BinaryOp;
import edu.upenn.cis.qdrex.core.qre.Iter;
import edu.upenn.cis.qdrex.core.qre.Split;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermMin;

public class Min<P extends Predicate<P, T>, T> extends BinaryOp<P, T, Double, Double, Double> {

    public Min(QRE<P, T, Double> f, QRE<P, T, Double> g) {
        super(f, g, g.outType);
    }

    @Override
    public Term<Double> op(Term<Double> tf, Term<Double> tg) {
        return ITermMin.of(tf, tg);
    }

    @Override
    public ImmutableSet<Parameter> getParametersInt() {
        return Sets.union(f.getParameters(), g.getParameters()).immutableCopy();
    }

    @Override
    public String toString() {
        return String.format("(Min %s %s)", f, g);
    }

    public static <P extends Predicate<P, T>, T> IterMin<P, T> iterMin(QRE<P, T, Double> f) {
        return new IterMin(f);
    }

    public static class IterMin<P extends Predicate<P, T>, T> extends Iter<P, T, Double, Double, Double> {

        public IterMin(QRE<P, T, Double> f) {
            super(ConstantTerm.of(Double.MAX_VALUE), f, false, Double.class);
        }

        @Override
        public Term<Double> combine(Term<Double> tr, Term<Double> tf) {
            return ITermMin.of(tr, tf);
        }

        @Override
        public Term<Double> finalize(Term<Double> tr) {
            return tr;
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            return f.getParameters();
        }

        @Override
        public String toString() {
            return String.format("(Min.IterMin %s)", f);
        }

    }

    public static class SplitMin<P extends Predicate<P, T>, T>
    extends Split<P, T, Double, Double, Double> {

        private /* mutable */ ImmutableSet<Parameter> parameters;

        public SplitMin(QRE<P, T, Double> f, QRE<P, T, Double> g) {
            super(f, g, g.outType);
            this.parameters = null;
        }

        @Override
        public Term<Double> combine(Term<Double> tf, Term<Double> tg) {
            return ITermMin.of(tf, tg);
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            if (parameters == null) {
                parameters = Sets.union(f.getParameters(), g.getParameters()).immutableCopy();
                verifyNotNull(parameters);
            }
            return parameters;
        }

        @Override
        public String toString() {
            return String.format("(Min.SplitMin %s %s)", f, g);
        }

    }

}
