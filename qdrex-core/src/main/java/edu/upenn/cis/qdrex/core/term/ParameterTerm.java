package edu.upenn.cis.qdrex.core.term;

import com.google.common.collect.ImmutableSet;

public class ParameterTerm<C> extends Term<C> {

    public final Parameter<C> parameter;
    private /* mutable */ ImmutableSet<Parameter> allParameters;

    public ParameterTerm(Parameter<C> parameter) {
        super(parameter.parType);
        this.parameter = parameter;
        this.allParameters = null;
    }

    @Override
    public boolean isGround() {
        return false;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (allParameters == null) {
            allParameters = ImmutableSet.of(parameter);
        }
        return allParameters;
    }

    @Override
    public C eval() throws IncompleteTermException {
        String msg = String.format("Evaluating incomplete term %s", this);
        throw new IncompleteTermException(msg);
    }

    @Override
    public <PC> Term<C> subst(Parameter<PC> p, Term<PC> t) {
        return parameter.equals(p) ? (Term<C>)t : this;
    }

    @Override
    public int getBytes() {
        return 4 + 4;
    }

    @Override
    public String toString() {
        return parameter.toString();
    }

}
