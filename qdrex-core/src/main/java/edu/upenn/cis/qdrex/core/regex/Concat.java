package edu.upenn.cis.qdrex.core.regex;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;
import edu.upenn.cis.qdrex.core.sfa.SFA;
import edu.upenn.cis.qdrex.core.sfa.SFAUtil;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class Concat<P extends Predicate<P, T>, T> extends Regex<P, T> {

    public final Regex<P, T> r1, r2;

    private /* mutable */ int size;
    private /* mutable */ SFA<P, T> sfa;

    private Concat(Regex<P, T> r1, Regex<P, T> r2) {
        super(Type.CONCAT);
        this.r1 = checkNotNull(r1);
        this.r2 = checkNotNull(r2);
        this.size = -1;
        this.sfa = null;
    }

    public static <P extends Predicate<P, T>, T> Regex<P, T> of(Regex<P, T>... rs) {
        Regex<P, T> ans = new Epsilon<>();
        for (Regex<P, T> r : rs) {
            ans = ans instanceof Epsilon ? r : new Concat<>(ans, r);
        }
        return ans;
    }

    @Override
    public int size() {
        if (size <= 0) {
            size = 1 + r1.size() + r2.size();
            verify(size > 0);
        }
        return size;
    }

    @Override
    public <R> R invokeVisitor(RegexVisitor<P, T, R> visitor) {
        return visitor.visitConcat(this);
    }

    @Override
    public SFA<P, T> buildSFA() {
        if (sfa == null) {
            SFA<P, T> sfa1 = r1.buildSFA();
            SFA<P, T> sfa2 = r2.buildSFA();
            sfa = SFAUtil.buildConcatSFA(sfa1, sfa2);
            verifyNotNull(sfa);
        }
        return sfa;
    }

    @Override
    public String toString() {
        return String.format("(Concat %s %s)", r1, r2);
    }

}
