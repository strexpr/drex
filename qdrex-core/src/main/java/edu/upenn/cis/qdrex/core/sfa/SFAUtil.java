package edu.upenn.cis.qdrex.core.sfa;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableTable;
import edu.upenn.cis.qdrex.core.sfa.State.WrapperState;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.util.Either;
import edu.upenn.cis.qdrex.core.util.function.BiFunction;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class SFAUtil {

    /**
     * Trims the input automaton to only the reachable states.
     * @param <P> type of predicates
     * @param <T> type of data values
     * @param sfa input SFA
     * @return trimmed SFA
     */
    public static <P extends Predicate<P, T>, T> SFA<P, T> trim(SFA<P, T> sfa) {
        ImmutableSet<State> states = sfa.getReachableStates().keySet();

        ImmutableTable.Builder<State, P, ImmutableSet<State>> transitionsBuilder = ImmutableTable.builder();
        for (State q : states) {
            for (P p : sfa.getTransitions(q).keySet()) {
                if (p.isSat()) {
                    transitionsBuilder.put(q, p, sfa.transitions.get(q, p));
                }
            }
        }
        ImmutableTable<State, P, ImmutableSet<State>> transitions = transitionsBuilder.build();

        ImmutableMap.Builder<State, ImmutableSet<State>> epsilonTransitionsBuilder = ImmutableMap.builder();
        for (State q : states) {
            epsilonTransitionsBuilder.put(q, sfa.getEpsilonTransitions(q));
        }
        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = epsilonTransitionsBuilder.build();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    public static <P extends Predicate<P, T>, T> SFA<P, T> buildBaseSFA(P pred) {
        checkNotNull(pred);

        State q0 = State.of(0, true, false);
        State q1 = State.of(1, false, true);
        ImmutableSet<State> states = ImmutableSet.of(q0, q1);

        ImmutableTable<State, P, ImmutableSet<State>> transitions =
                ImmutableTable.of(q0, pred, ImmutableSet.of(q1));
        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions =
                ImmutableMap.of();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    public static <P extends Predicate<P, T>, T> SFA<P, T> buildEpsilonSFA() {
        State q0 = State.of(0, true, true);
        ImmutableSet<State> states = ImmutableSet.of(q0);

        ImmutableTable<State, P, ImmutableSet<State>> transitions = ImmutableTable.of();
        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = ImmutableMap.of();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    public static <P extends Predicate<P, T>, T> SFA<P, T> buildBottomSFA() {
        State q0 = State.of(0, true, false);
        ImmutableSet<State> states = ImmutableSet.of(q0);

        ImmutableTable<State, P, ImmutableSet<State>> transitions = ImmutableTable.of();
        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = ImmutableMap.of();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    public static <P extends Predicate<P, T>, T> SFA<P, T> buildUnionSFA(SFA<P, T> sfa1, SFA<P, T> sfa2) {
        checkNotNull(sfa1);
        checkNotNull(sfa2);

        ImmutableSet.Builder<State> statesBuilder = ImmutableSet.builder();
        ImmutableTable.Builder<State, P, ImmutableSet<State>> transitionsBuilder = ImmutableTable.builder();
        ImmutableMap.Builder<State, ImmutableSet<State>> epsilonTransitionsBuilder = ImmutableMap.builder();

        Function<State, WrapperState<Either<State, State>>> proj1 =
                q -> State.of(Either.left(q), q.isInitial, q.isFinal);
        Function<State, WrapperState<Either<State, State>>> proj2 =
                q -> State.of(Either.right(q), q.isInitial, q.isFinal);

        // Build sub-machine 1
        for (State q : sfa1.states) {
            State q1 = proj1.apply(q);
            statesBuilder.add(q1);

            for (P p : sfa1.getTransitions(q).keySet()) {
                ImmutableSet.Builder<State> q1Out = ImmutableSet.builder();
                for (State qPrime : sfa1.transitions.get(q, p)) {
                    q1Out.add(proj1.apply(qPrime));
                }
                transitionsBuilder.put(q1, p, q1Out.build());
            }

            ImmutableSet.Builder<State> q1E = ImmutableSet.builder();
            for (State qPrime : sfa1.getEpsilonTransitions(q)) {
                q1E.add(proj1.apply(qPrime));
            }
            epsilonTransitionsBuilder.put(q1, q1E.build());
        }

        // Build sub-machine 2
        for (State q : sfa2.states) {
            State q2 = proj2.apply(q);
            statesBuilder.add(q2);

            for (P p : sfa2.getTransitions(q).keySet()) {
                ImmutableSet.Builder<State> q2Out = ImmutableSet.builder();
                for (State qPrime : sfa2.transitions.get(q, p)) {
                    q2Out.add(proj2.apply(qPrime));
                }
                transitionsBuilder.put(q2, p, q2Out.build());
            }

            ImmutableSet.Builder<State> q2E = ImmutableSet.builder();
            for (State qPrime : sfa2.getEpsilonTransitions(q)) {
                q2E.add(proj2.apply(qPrime));
            }
            epsilonTransitionsBuilder.put(q2, q2E.build());
        }

        ImmutableSet<State> states = statesBuilder.build();
        ImmutableTable<State, P, ImmutableSet<State>> transitions = transitionsBuilder.build();
        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = epsilonTransitionsBuilder.build();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    public static <P extends Predicate<P, T>, T> SFA<P, T> buildIntersectionSFA(SFA<P, T> sfa1, SFA<P, T> sfa2) {
        checkNotNull(sfa1);
        checkNotNull(sfa2);

        ImmutableSet.Builder<State> statesBuilder = ImmutableSet.builder();
        ImmutableTable.Builder<State, P, ImmutableSet<State>> transitionsBuilder = ImmutableTable.builder();
        ImmutableMap.Builder<State, ImmutableSet<State>> epsilonTransitionsBuilder = ImmutableMap.builder();

        BiFunction<State, State, WrapperState<ImmutablePair<State, State>>> prod =
                (State q1, State q2) -> State.of(q1, q2, q1.isInitial && q2.isInitial,
                        q1.isFinal && q2.isFinal);

        // Product construction
        // For every state (q1, q2) of the product, ...
        for (State q1 : sfa1.states) {
            for (State q2 : sfa2.states) {
                State q1q2 = prod.apply(q1, q2);
                statesBuilder.add(q1q2);

                // ... and for every pair of simultaneously satisfiable outgoing
                // transitions, construct the relevant transition.
                for (P p1 : sfa1.getTransitions(q1).keySet()) {
                    for (P p2 : sfa2.getTransitions(q2).keySet()) {
                        P p1AndP2 = p1.and(p2);
                        if (p1AndP2.isSat()) {
                            ImmutableSet.Builder<State> targets = ImmutableSet.builder();
                            for (State q1Tgt : sfa1.transitions.get(q1, p1)) {
                                for (State q2Tgt : sfa2.transitions.get(q2, p2)) {
                                    targets.add(prod.apply(q1Tgt, q2Tgt));
                                }
                            }
                            transitionsBuilder.put(q1q2, p1AndP2, targets.build());
                        }
                    }
                }

                ImmutableSet.Builder<State> epsilonTargets = ImmutableSet.builder();
                // ... for every epsilon transition from q1 to q1Prime, emit an
                // epsilon transition from (q1, q2) to (q1Prime, q2).
                for (State q1Prime : sfa1.getEpsilonTransitions(q1)) {
                    epsilonTargets.add(prod.apply(q1Prime, q2));
                }
                // ... and for every epsilon transition from q2 to q2Prime, emit
                // an epsilon transition from (q1, q2) to (q1, q2Prime).
                for (State q2Prime : sfa1.getEpsilonTransitions(q2)) {
                    epsilonTargets.add(prod.apply(q1, q2Prime));
                }
                epsilonTransitionsBuilder.put(q1q2, epsilonTargets.build());
            }
        }

        ImmutableSet<State> states = statesBuilder.build();
        ImmutableTable<State, P, ImmutableSet<State>> transitions = transitionsBuilder.build();
        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = epsilonTransitionsBuilder.build();

        SFA<P, T> ans = new SFA<>(states, transitions, epsilonTransitions);
        ans = trim(ans);
        return ans;
    }

    public static <P extends Predicate<P, T>, T> SFA<P, T> buildConcatSFA(SFA<P, T> sfa1, SFA<P, T> sfa2) {
        checkNotNull(sfa1);
        checkNotNull(sfa2);

        ImmutableSet.Builder<State> statesBuilder = ImmutableSet.builder();
        ImmutableTable.Builder<State, P, ImmutableSet<State>> transitionsBuilder = ImmutableTable.builder();
        ImmutableMap.Builder<State, ImmutableSet<State>> epsilonTransitionsBuilder = ImmutableMap.builder();

        Function<State, WrapperState<Either<State, State>>> proj1 = (State q) ->
                State.of(Either.left(q), q.isInitial, false);
        Function<State, WrapperState<Either<State, State>>> proj2 = (State q) ->
                State.of(Either.right(q), false, q.isFinal);

        // Build sub-machine 1
        for (State q : sfa1.states) {
            State q1 = proj1.apply(q);
            statesBuilder.add(q1);

            for (P p : sfa1.getTransitions(q).keySet()) {
                ImmutableSet.Builder<State> q1Out = ImmutableSet.builder();
                for (State qPrime : sfa1.transitions.get(q, p)) {
                    q1Out.add(proj1.apply(qPrime));
                }
                transitionsBuilder.put(q1, p, q1Out.build());
            }

            ImmutableSet.Builder<State> q1E = ImmutableSet.builder();
            for (State qPrime : sfa1.getEpsilonTransitions(q)) {
                q1E.add(proj1.apply(qPrime));
            }
            // Connect sub-machine 1 to sub-machine 2
            if (q.isFinal) {
                for (State q2 : sfa2.initialStates) {
                    q1E.add(proj2.apply(q2));
                }
            }
            epsilonTransitionsBuilder.put(q1, q1E.build());
        }

        // Build sub-machine 2
        for (State q : sfa2.states) {
            State q2 = proj2.apply(q);
            statesBuilder.add(q2);

            for (P p : sfa2.getTransitions(q).keySet()) {
                ImmutableSet.Builder<State> q2Out = ImmutableSet.builder();
                for (State qPrime : sfa2.transitions.get(q, p)) {
                    q2Out.add(proj2.apply(qPrime));
                }
                transitionsBuilder.put(q2, p, q2Out.build());
            }

            ImmutableSet.Builder<State> q2E = ImmutableSet.builder();
            for (State qPrime : sfa2.getEpsilonTransitions(q)) {
                q2E.add(proj2.apply(qPrime));
            }
            epsilonTransitionsBuilder.put(q2, q2E.build());
        }

        ImmutableSet<State> states = statesBuilder.build();
        ImmutableTable<State, P, ImmutableSet<State>> transitions = transitionsBuilder.build();
        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = epsilonTransitionsBuilder.build();

        return new SFA<>(states, transitions, epsilonTransitions);
    }

    public static <P extends Predicate<P, T>, T> SFA<P, T> buildStarSFA(SFA<P, T> sfa) {
        checkNotNull(sfa);

        Function<State, WrapperState<State>> proj = (State q) -> State.of(q, false, false);

        ImmutableSet.Builder<State> statesBuilder = ImmutableSet.builder();
        ImmutableTable.Builder<State, P, ImmutableSet<State>> transitionsBuilder = ImmutableTable.builder();
        ImmutableMap.Builder<State, ImmutableSet<State>> epsilonTransitionsBuilder = ImmutableMap.builder();

        State q0 = State.of(0, true, true);
        statesBuilder.add(q0);
        ImmutableSet.Builder<State> q0E = ImmutableSet.builder();
        for (State q : sfa.initialStates) {
            q0E.add(proj.apply(q));
        }
        epsilonTransitionsBuilder.put(q0, q0E.build());

        for (State q : sfa.states) {
            statesBuilder.add(proj.apply(q));
            for (P p : sfa.getTransitions(q).keySet()) {
                ImmutableSet.Builder<State> targets = ImmutableSet.builder();
                for (State qPrime : sfa.transitions.get(q, p)) {
                    targets.add(proj.apply(qPrime));
                }
                transitionsBuilder.put(proj.apply(q), p, targets.build());
            }

            ImmutableSet.Builder<State> epsilonTargets = ImmutableSet.builder();
            for (State qPrime : sfa.getEpsilonTransitions(q)) {
                epsilonTargets.add(proj.apply(qPrime));
            }
            if (q.isFinal) {
                epsilonTargets.add(q0);
            }
            epsilonTransitionsBuilder.put(proj.apply(q), epsilonTargets.build());
        }

        ImmutableSet<State> states = statesBuilder.build();
        ImmutableTable<State, P, ImmutableSet<State>> transitions = transitionsBuilder.build();
        ImmutableMap<State, ImmutableSet<State>> epsilonTransitions = epsilonTransitionsBuilder.build();
        return new SFA<>(states, transitions, epsilonTransitions);
    }

}
