package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.fj.Evaluator;
import edu.upenn.cis.qdrex.core.term.Term;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import java.io.Serializable;
import java.util.Iterator;

/**
 * Type of quantitative regular expressions, mapping data streams over T* to
 * cost values in C. For example, string-to-string DReX expressions would be of
 * the form QRE<Character, String>.
 * @param <P> type of predicates
 * @param <T> data domain
 * @param <C> cost domain
 */
public abstract class QRE<P extends Predicate<P, T>, T, C> implements Serializable {

    public enum Type {
        BASE, EPSILON, BOTTOM, OP, BINOP, TRYELSE, SPLIT, ITER, STREAM
    }

    public final Type type;
    public final Class<C> outType;

    public QRE(Type type, Class<C> outType) {
        this.type = type;
        this.outType = checkNotNull(outType);
    }

    public abstract int size();
    public abstract ImmutableSet<Parameter> getParameters();
    public abstract AnalysisResult<P, T, C> fjAnalyze();
    public abstract Evaluator<P, T, C> buildEvaluator();

    @Override
    public abstract String toString();

    public Optional<Term<C>> eval(Iterator<? extends T> iterator) {
        AnalysisResult<P, T, C> analysisResult = fjAnalyze();
        checkState(analysisResult.isUnambiguous());
        Evaluator<P, T, C> m = buildEvaluator();
        return m.eval(iterator);
    }

    public Optional<Term<C>> eval(Iterable<? extends T> w) {
        return eval(w.iterator());
    }

    public Optional<Term<C>> eval(T... ts) {
        checkState(fjAnalyze().isUnambiguous());
        Evaluator<P, T, C> m = buildEvaluator();
        return m.eval(ts);
    }

}
