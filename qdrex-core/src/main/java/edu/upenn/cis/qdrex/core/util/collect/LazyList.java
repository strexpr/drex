package edu.upenn.cis.qdrex.core.util.collect;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkElementIndex;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import java.util.AbstractList;
import java.util.Iterator;
import java.util.ListIterator;

public abstract class LazyList<T> extends AbstractList<T> {

    public final int size;

    private LazyList(int size) {
        checkArgument(size >= 0);
        this.size = size;
    }

    public static <T> EmptyLazyList<T> of() {
        return EMPTY;
    }

    public static final class EmptyLazyList<T> extends LazyList<T> {
        public EmptyLazyList() {
            super(0);
        }
    }

    public static final EmptyLazyList EMPTY = new EmptyLazyList();

    public static <T> LazyList<T> of(T... ts) {
        if (ts.length == 0) {
            return of();
        } else {
            ImmutableList<T> tsl = ImmutableList.copyOf(ts);
            return of(tsl);
        }
    }

    public static <T> LazyList<T> of(ImmutableList<T> list) {
        if (list.isEmpty()) {
            return of();
        } else {
            return new ActualLazyList<>(list);
        }
    }

    public static final class ActualLazyList<T> extends LazyList<T> {
        public final ImmutableList<T> list;
        public ActualLazyList(ImmutableList<T> list) {
            super(list.size());
            checkArgument(!list.isEmpty());
            this.list = list;
        }
    }

    public static <T> LazyList<T> cons(T car, LazyList<T> cdr) {
        return cdr.isEmpty() ? of(car) : new ConsLazyList<>(car, cdr);
    }

    public static final class ConsLazyList<T> extends LazyList<T> {
        public final T car;
        public final LazyList<T> cdr;
        public ConsLazyList(T car, LazyList<T> cdr) {
            super(1 + cdr.size);
            checkArgument(!cdr.isEmpty());
            this.car = checkNotNull(car);
            this.cdr = cdr;
        }
    }

    public static <T> LazyList<T> snoc(LazyList<T> rest, T tail) {
        return rest.isEmpty() ? of(tail) : new SnocLazyList<>(rest, tail);
    }

    public static final class SnocLazyList<T> extends LazyList<T> {
        public final LazyList<T> rest;
        public final T last;
        public SnocLazyList(LazyList<T> rest, T last) {
            super(rest.size + 1);
            checkArgument(rest.size > 0);
            this.rest = rest;
            this.last = checkNotNull(last);
        }
    }

    public static <T> LazyList<T> append(LazyList<T> list1, LazyList<T> list2) {
        return list1.append(list2);
    }

    public LazyList<T> append(LazyList<T> l) {
        checkNotNull(l);

        if (this.isEmpty()) {
            return l;
        } else if (l.isEmpty()) {
            return this;
        } else {
            return new AppendLazyList<>(this, l);
        }
    }

    public static final class AppendLazyList<T> extends LazyList<T> {
        public final LazyList<T> list1, list2;
        public AppendLazyList(LazyList<T> list1, LazyList<T> list2) {
            super(list1.size + list2.size);
            checkArgument(!list1.isEmpty());
            checkArgument(!list2.isEmpty());
            this.list1 = list1;
            this.list2 = list2;
        }
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public Iterator<T> iterator() {
        return new LazyListIterator<>(this);
    }

    @Override
    public T get(int index) {
        checkElementIndex(index, size);
        ListIterator<T> iterator = listIterator(index);
        return iterator.next();
    }

    @Override
    public ListIterator<T> listIterator() {
        return new LazyListIterator<>(this);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        checkElementIndex(index, size);
        LazyListIterator<T> ans = new LazyListIterator<>(this);
        ans.move(index);
        return ans;
    }

}
