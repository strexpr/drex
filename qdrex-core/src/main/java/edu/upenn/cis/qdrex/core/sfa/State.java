package edu.upenn.cis.qdrex.core.sfa;

import static com.google.common.base.Preconditions.checkNotNull;
import java.io.Serializable;
import java.util.Objects;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.tuple.ImmutablePair;

public abstract class State implements Serializable {

    public final boolean isInitial;
    public final boolean isFinal;

    public State(boolean isInitial, boolean isFinal) {
        this.isInitial = isInitial;
        this.isFinal = isFinal;
    }

    @Override
    public abstract boolean equals(Object obj);

    @Override
    public abstract int hashCode();

    public static <T> WrapperState<T> of(T t, boolean isInitial, boolean isFinal) {
        return new WrapperState<>(t, isInitial, isFinal);
    }

    public static <T, U> WrapperState<ImmutablePair<T, U>>
    of(T t, U u, boolean isInitial, boolean isFinal) {
        ImmutablePair<T, U> tu = ImmutablePair.of(t, u);
        return new WrapperState<>(tu, isInitial, isFinal);
    }

    public static class WrapperState<T> extends State {

        public final T t;

        public WrapperState(T t, boolean isInitial, boolean isFinal) {
            super(isInitial, isFinal);
            this.t =  checkNotNull(t);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof WrapperState)) {
                return false;
            } else if (obj == this) {
                return true;
            }

            WrapperState<?> wo = (WrapperState<?>)obj;
            return Objects.equals(t, wo.t) && this.isInitial == wo.isInitial &&
                    this.isFinal == wo.isFinal;
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(t)
                                        .append(isInitial)
                                        .append(isFinal)
                                        .build();
        }

        @Override
        public String toString() {
            return String.format("(WrapperState [initial: %b, final: %b] %s)",
                    isInitial, isFinal, t);
        }

    }

}
