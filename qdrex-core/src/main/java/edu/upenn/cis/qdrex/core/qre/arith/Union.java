package edu.upenn.cis.qdrex.core.qre.arith;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import com.google.common.collect.Sets;
import edu.upenn.cis.qdrex.core.qre.BinaryOp;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.qre.Iter;
import edu.upenn.cis.qdrex.core.qre.Split;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.arith.MTermUnion;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import static com.google.common.base.Verify.verifyNotNull;

public class Union<P extends Predicate<P, T>, T> extends BinaryOp<P, T, Multiset, Multiset, Multiset> {

    public Union(QRE<P, T, Multiset> f, QRE<P, T, Multiset> g) {
        super(f, g, g.outType);
    }

    @Override
    public Term<Multiset> op(Term<Multiset> tf, Term<Multiset> tg) {
        return MTermUnion.of(tf, tg);
    }

    @Override
    public ImmutableSet<Parameter> getParametersInt() {
        return Sets.union(f.getParameters(), g.getParameters()).immutableCopy();
    }

    @Override
    public String toString() {
        return String.format("(Union %s %s)", f, g);
    }

    public static <P extends Predicate<P, T>, T> IterUnion<P, T> iterUnion(QRE<P, T, Multiset> f,
            boolean emptyDefined) {
        return new IterUnion(f, emptyDefined);
    }

    public static class IterUnion<P extends Predicate<P, T>, T> extends Iter<P, T, Multiset, Multiset, Multiset> {

        public IterUnion(QRE<P, T, Multiset> f, boolean emptyDefined) {
            super(ConstantTerm.of(Multiset.EMPTY), f, emptyDefined, Multiset.class);
        }

        @Override
        public Term<Multiset> combine(Term<Multiset> tr, Term<Multiset> tf) {
            return MTermUnion.of(tr, tf);
        }

        @Override
        public Term<Multiset> finalize(Term<Multiset> tr) {
            return tr;
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            return f.getParameters();
        }

        @Override
        public String toString() {
            return String.format("(Union.IterUnion %s)", f);
        }

    }

    public static class SplitUnion<P extends Predicate<P, T>, T>
    extends Split<P, T, Multiset, Multiset, Multiset> {

        private /* mutable */ ImmutableSet<Parameter> parameters;

        public SplitUnion(QRE<P, T, Multiset> f, QRE<P, T, Multiset> g) {
            super(f, g, g.outType);
            this.parameters = null;
        }

        @Override
        public Term<Multiset> combine(Term<Multiset> tf, Term<Multiset> tg) {
            return MTermUnion.of(tf, tg);
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            if (parameters == null) {
                parameters = Sets.union(f.getParameters(), g.getParameters()).immutableCopy();
                verifyNotNull(parameters);
            }
            return parameters;
        }

        @Override
        public String toString() {
            return String.format("(Union.SplitUnion %s %s)", f, g);
        }

    }

}
