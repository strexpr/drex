package edu.upenn.cis.qdrex.core.fj.analysis;

import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.regex.Regex;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.util.Either;
import java.io.Serializable;

/**
 * Result of analyzing a <code>QRE<P, T, C></code> for ambiguity.
 * @param <P>
 * @param <T>
 * @param <C>
 */
public class AnalysisResult<P extends Predicate<P, T>, T, C> implements Serializable {

    private final Either<Regex<P, T>, AnalysisError<P, T, C>> result;

    private AnalysisResult(Regex<P, T> domainType) {
        this.result = Either.left(domainType);
    }

    private AnalysisResult(AnalysisError<P, T, C> error) {
        this.result = Either.right(error);
    }

    public static <P extends Predicate<P, T>, T, C> AnalysisResult<P, T, C>
    unambiguous(Regex<P, T> domainType) {
        return new AnalysisResult<>(domainType);
    }

    public static <P extends Predicate<P, T>, T, C> AnalysisResult<P, T, C>
    baseError(QRE<P, T, C> e, String msg) {
        AnalysisError<P, T, C> error = AnalysisError.base(e, msg);
        return new AnalysisResult<>(error);
    }

    public static <P extends Predicate<P, T>, T, C> AnalysisResult<P, T, C>
    subexprError(QRE<P, T, C> e, AnalysisError subError) {
        AnalysisError<P, T, C> error = AnalysisError.subexpr(e, subError);
        return new AnalysisResult<>(error);
    }

    public Regex<P, T> getDomainType() {
        return result.getLeft();
    }

    public AnalysisError<P, T, C> getAnalysisError() {
        return result.getRight();
    }

    public boolean isUnambiguous() {
        return result.isLeft();
    }

}
