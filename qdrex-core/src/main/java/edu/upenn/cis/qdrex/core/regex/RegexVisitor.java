package edu.upenn.cis.qdrex.core.regex;

import edu.upenn.cis.qdrex.core.symbol.Predicate;

public abstract class RegexVisitor<P extends Predicate<P, T>, T, R> {

    public R visit(Regex<P, T> r) {
        return r.invokeVisitor(this);
    }

    public abstract R visitBase(Base<P, T> base);
    public abstract R visitEpsilon(Epsilon<P, T> epsilon);
    public abstract R visitBottom(Bottom<P, T> bottom);
    public abstract R visitUnion(Union<P, T> union);
    public abstract R visitConcat(Concat<P, T> concat);
    public abstract R visitStar(Star<P, T> star);

}
