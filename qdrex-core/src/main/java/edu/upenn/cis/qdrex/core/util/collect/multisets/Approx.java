package edu.upenn.cis.qdrex.core.util.collect.multisets;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Verify.verify;
import java.io.Serializable;

public class Approx implements Serializable {

    private static double epsilon = -1;
    private static double lnOneEpsilon = -1;

    static {
        setEpsilon(0.01);
    }

    public static void setEpsilon(double epsilon) {
        checkArgument(epsilon > 0);
        Approx.epsilon = epsilon;
        Approx.lnOneEpsilon = Math.log(1 + epsilon);
    }

    public static double getEpsilon() {
        verify(epsilon > 0);
        return epsilon;
    }

    public static double getLnOneEpsilon() {
        verify(epsilon > 0);
        return lnOneEpsilon;
    }

    /**
     * Maps the input <code>val</code> to the corresponding bucket index.
     * Requires <code>val > 0</code>.
     * @param val input value
     * @return bucket index
     */
    public static int valToIndex(double val) {
        checkArgument(val > 0);

        if (val >= 1.0) {
            // log_{1 + eps}(val) = ln(val) / ln(1 + eps)
            double indexDbl = Math.log(val) / getLnOneEpsilon();
            int index = (int)Math.floor(indexDbl);
            return index;
        } else {
            // log_{1 + eps}(1 / val) = -log_{1 + eps}(val) = -ln(val) / ln(1 + eps)
            double indexDbl = -Math.log(val) / getLnOneEpsilon();
            int index = -((int)Math.floor(indexDbl)) - 1;
            return index;
        }
    }

    public static double indexToVal(int index) {
        return Math.exp(getLnOneEpsilon() * index);
    }

    public static double round(double val) {
        return indexToVal(valToIndex(val));
    }

}
