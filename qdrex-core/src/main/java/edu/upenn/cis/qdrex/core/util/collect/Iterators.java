package edu.upenn.cis.qdrex.core.util.collect;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Iterator;

public class Iterators {

    public static <T> Iterator<T> keep(Iterator<T> iterator, int n) {
        checkNotNull(iterator);
        checkArgument(n >= 0);

        return new Iterator<T>() {

            private /* mutable */ int i = 0;

            @Override
            public boolean hasNext() {
                return iterator.hasNext() && i < n;
            }

            @Override
            public T next() {
                i++;
                return iterator.next();
            }

        };
    }

    public static <T> Iterable<T> keep(Iterable<T> iterable, int n) {
        checkNotNull(iterable);
        checkArgument(n >= 0);
        return () -> keep(iterable.iterator(), n);
    }

    public static <T> int length(Iterator<T> iterator) {
        int ans = 0;
        while (iterator.hasNext()) {
            iterator.next();
            ans++;
        }
        return ans;
    }

    public static <T> int length(Iterable<T> iterable) {
        return length(iterable.iterator());
    }

}
