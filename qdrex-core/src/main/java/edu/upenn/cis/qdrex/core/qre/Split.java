package edu.upenn.cis.qdrex.core.qre;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.fj.Evaluator;
import edu.upenn.cis.qdrex.core.fj.SplitEvaluator;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.regex.Regex;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Term;
import com.google.common.base.Optional;

public abstract class Split<P extends Predicate<P, T>, T, Cf, Cg, C> extends QRE<P, T, C> {

    public final QRE<P, T, Cf> f;
    public final QRE<P, T, Cg> g;

    private /* mutable */ int size;
    private /* mutable */ AnalysisResult<P, T, C> analysisResult;

    public Split(QRE<P, T, Cf> f, QRE<P, T, Cg> g, Class<C> outType) {
        super(Type.SPLIT, outType);
        this.f = checkNotNull(f);
        this.g = checkNotNull(g);
        this.size = -1;
        this.analysisResult = null;
    }

    public abstract Term<C> combine(Term<Cf> tf, Term<Cg> tg);

    @Override
    public int size() {
        if (size <= 0) {
            size = 1 + f.size() + g.size();
            verify(size > 0);
        }
        return size;
    }

    @Override
    public AnalysisResult<P, T, C> fjAnalyze() {
        if (analysisResult == null) {
            analysisResult = fjAnalyzeInt();
            verifyNotNull(analysisResult);
        }
        return analysisResult;
    }

    public AnalysisResult<P, T, C> fjAnalyzeInt() {
        AnalysisResult<P, T, Cf> af = f.fjAnalyze();
        if (!af.isUnambiguous()) {
            return AnalysisResult.subexprError(this, af.getAnalysisError());
        }

        AnalysisResult<P, T, Cg> ag = g.fjAnalyze();
        if (!ag.isUnambiguous()) {
            return AnalysisResult.subexprError(this, ag.getAnalysisError());
        }

        Regex<P, T> rf = af.getDomainType();
        Regex<P, T> rg = ag.getDomainType();
        Regex<P, T> rfrg = rf.concat(rg);

        Optional<ImmutableList<T>> ambiguousWitness = rfrg.getAmbiguousWitness();
        if (ambiguousWitness.isPresent()) {
            ImmutableList<T> aw = ambiguousWitness.get();
            String msg = String.format("Ambiguous split on input %s.", aw);
            return AnalysisResult.baseError(this, msg);
        }

        return AnalysisResult.unambiguous(rfrg);
    }

    @Override
    public Evaluator<P, T, C> buildEvaluator() {
        return new SplitEvaluator<>(this);
    }

}
