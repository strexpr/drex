package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.regex.Regex;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import com.google.common.base.Optional;
import edu.upenn.cis.qdrex.core.fj.BinaryOpEvaluator;
import edu.upenn.cis.qdrex.core.term.Term;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;

public abstract class BinaryOp<P extends Predicate<P, T>, T, Cf, Cg, C> extends QRE<P, T, C> {

    public final QRE<P, T, Cf> f;
    public final QRE<P, T, Cg> g;

    private /* mutable */ int size;
    private /* mutable */ ImmutableSet<Parameter> parameters;
    private /* mutable */ AnalysisResult<P, T, C> analysisResult;

    public BinaryOp(QRE<P, T, Cf> f, QRE<P, T, Cg> g, Class<C> outType) {
        super(Type.BINOP, outType);
        this.f = checkNotNull(f);
        this.g = checkNotNull(g);

        this.size = -1;
        this.parameters = null;
        this.analysisResult = null;
    }

    public abstract Term<C> op(Term<Cf> tf, Term<Cg> tg);
    public abstract ImmutableSet<Parameter> getParametersInt();

    @Override
    public int size() {
        if (size <= 0) {
            size = 1 + f.size() + g.size();
            verify(size > 0);
        }
        return size;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        if (parameters == null) {
            parameters = getParametersInt();
            verifyNotNull(parameters);
        }
        return parameters;
    }

    @Override
    public AnalysisResult<P, T, C> fjAnalyze() {
        if (analysisResult == null) {
            analysisResult = fjAnalyzeInt();
            verifyNotNull(analysisResult);
        }
        return analysisResult;
    }

    public AnalysisResult<P, T, C> fjAnalyzeInt() {
        AnalysisResult<P, T, Cf> a1 = f.fjAnalyze();
        if (!a1.isUnambiguous()) {
            return AnalysisResult.subexprError(this, a1.getAnalysisError());
        }

        AnalysisResult<P, T, Cg> a2 = g.fjAnalyze();
        if (!a2.isUnambiguous()) {
            return AnalysisResult.subexprError(this, a2.getAnalysisError());
        }

        Regex<P, T> r1 = a1.getDomainType();
        Regex<P, T> r2 = a2.getDomainType();

        Optional<ImmutableList<T>> inequivalenceWitness = Regex.getInequivalenceWitness(r1, r2);
        if (inequivalenceWitness.isPresent()) {
            String msg = String.format("Only one subexpression is defined on "
                    + "input %s.", inequivalenceWitness.get());
            return AnalysisResult.baseError(this, msg);
        }

        return r1.size() < r2.size() ?
                AnalysisResult.unambiguous(r1) :
                AnalysisResult.unambiguous(r2);
    }

    @Override
    public BinaryOpEvaluator<P, T, Cf, Cg, C> buildEvaluator() {
        return new BinaryOpEvaluator<>(this);
    }

}
