package edu.upenn.cis.qdrex.core.fj;

import static com.google.common.base.Preconditions.checkState;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Term;
import static com.google.common.base.Verify.verify;
import edu.upenn.cis.qdrex.core.qre.Stream;

public class StreamEvaluator<Pf extends Predicate<Pf, T>, Pg extends Predicate<Pg, Cf>, T, Cf, Cg> extends Evaluator<Pf, T, Cg> {

    public final Stream<Pf, Pg, T, Cf, Cg> stream;
    public final Evaluator<Pf, T, Cf> mf;
    public final Evaluator<Pg, Cf, Cg> mg;

    private boolean isStarted;
    private int startIndex;
    private Term<Cg> tg;

    public StreamEvaluator(Stream<Pf, Pg, T, Cf, Cg> stream) {
        super(stream);
        this.stream = stream;
        this.mf = stream.f.buildEvaluator();
        this.mg = stream.g.buildEvaluator();

        this.isStarted = false;
        this.startIndex = 0;
        this.tg = null;
    }

    @Override
    public void resetHandler() {
        mf.reset();
        mg.reset();
        isStarted = false;
        startIndex = 0;
        tg = null;
    }

    @Override
    public ImmutableList<Response<Pf, T, Cg>> startHandler(int index) {
        checkState(!isStarted);
        isStarted = true;

        ImmutableList<Response<Pf, T, Cf>> respf = mf.start(index);
        verify(respf.isEmpty() || (respf.size() == 1 && respf.get(0).isKillUp));

        ImmutableList<Response<Pg, Cf, Cg>> respg = mg.start(index);
        verify(respg.size() == 1 && respg.get(0).isResult);
        startIndex = index;
        tg = respg.get(0).getTerm();
        return ImmutableList.of(Response.result(this, index, tg));
    }

    @Override
    public ImmutableList<Response<Pf, T, Cg>> symbolHandler(int index, T t) {
        ImmutableList<Response<Pf, T, Cf>> respf = mf.symbol(index, t);
        verify(respf.size() <= 1);

        if (respf.size() == 1 && respf.get(0).isResult) {
            Term<Cf> tf = respf.get(0).getTerm();
            ImmutableList<Response<Pg, Cf, Cg>> respg = mg.symbol(index, tf.eval());
            verify(respg.size() == 1 && respg.get(0).isResult);
            tg = respg.get(0).getTerm();
        }

        if (isStarted) {
            return ImmutableList.of(Response.result(this, startIndex, tg));
        } else {
            return ImmutableList.of();
        }
    }

}
