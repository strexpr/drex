package edu.upenn.cis.qdrex.core.fj;

import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Term;
import java.util.Iterator;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Verify.verify;
import static com.google.common.base.Verify.verifyNotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public abstract class Evaluator<P extends Predicate<P, T>, T, C> implements Serializable {

    public final QRE<P, T, C> e;

    public final int serialNumber;
    private static /* mutable */ int instanceCount = 0;
    private static final boolean DEBUG = false;

    private static boolean memLoggingEnabled = false;
    private static long peakMemUsage = 0, numLoggingCalls = 0;
    public static final long LOGGING_FREQUENCY = 1000;

    private static int streamLen = 0;
    private static long fullTime = 0, secondHalfTime = 0;

    public Evaluator(QRE<P, T, C> e) {
        this.e = checkNotNull(e);
        checkArgument(e.fjAnalyze().isUnambiguous());
        this.serialNumber = instanceCount++;
    }

    public abstract void resetHandler();
    public abstract ImmutableList<Response<P, T, C>> startHandler(int index);
    public abstract ImmutableList<Response<P, T, C>> symbolHandler(int index, T t);

    public void reset() {
        if (DEBUG) {
            System.out.printf("(reset %s\n", this);
        }
        resetHandler();
        if (DEBUG) {
            System.out.printf(")\n");
        }
    }

    public ImmutableList<Response<P, T, C>> start(int index) {
        if (DEBUG) {
            System.out.printf("(start %s %d\n", this, index);
        }
        ImmutableList<Response<P, T, C>> ans = startHandler(index);
        if (DEBUG) {
            System.out.printf("(start-return\n");
            for (Response<P, T, C> resp : ans) {
                System.out.printf("%s\n", resp);
            }
            System.out.printf("))\n");
        }
        return ans;
    }

    public ImmutableList<Response<P, T, C>> symbol(int index, T t) {
        checkNotNull(t);

        if (DEBUG) {
            System.out.printf("(symbol %s %d %s\n", this, index, t);
        }
        ImmutableList<Response<P, T, C>> ans = symbolHandler(index, t);
        if (DEBUG) {
            System.out.printf("(start-return\n");
            for (Response<P, T, C> resp : ans) {
                System.out.printf("%s\n", resp);
            }
            System.out.printf("))\n");
        }
        return ans;
    }

    public Optional<Term<C>> eval(Iterator<? extends T> iterator) {
        int i = 0;

        this.reset();
        logMemUsage();

        long startTime = System.nanoTime();
        long halfTime = 0;
        boolean halfStarted = false;

        ImmutableList<Response<P, T, C>> responses = this.start(-1);
        verifyNotNull(responses);
        verify(responses.size() <= 1);
        while (iterator.hasNext()) {
            T t = iterator.next();
            responses = this.symbol(i++, t);
            verifyNotNull(responses);
            verify(responses.size() <= 1);

            logMemUsage();
            if (!halfStarted && i + i > streamLen) {
                halfStarted = true;
                halfTime = System.nanoTime();
            }
        }

        long endTime = System.nanoTime();
        fullTime = endTime - startTime;
        secondHalfTime = endTime - halfTime;

        if (responses.size() == 1 && responses.get(0).isResult) {
            return Optional.of(responses.get(0).getTerm());
        } else {
            return Optional.absent();
        }
    }

    public Optional<Term<C>> eval(Iterable<? extends T> w) {
        return eval(w.iterator());
    }

    public Optional<Term<C>> eval(T... ts) {
        return eval(ImmutableList.copyOf(ts));
    }

    @Override
    public String toString() {
        return String.format("(Evaluator [%d] %s)", serialNumber, e);
    }

    public static void setMemLogging(boolean enabled) {
        Evaluator.memLoggingEnabled = enabled;
    }

    public static void resetPeakMemUsage() {
        peakMemUsage = 0;
    }

    public static long getPeakMemUsage() {
        return peakMemUsage;
    }

    public void logMemUsage() {
        if (memLoggingEnabled && (numLoggingCalls++ % LOGGING_FREQUENCY == 0)) {
            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                ObjectOutputStream out = new ObjectOutputStream(bos);
                out.writeObject(this);
                out.flush();
                long memUsage = bos.toByteArray().length;
                peakMemUsage = Math.max(peakMemUsage, memUsage);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }
    }

    public static void setTimeLogging(int streamLen) {
        Evaluator.streamLen = streamLen;
    }

    public static long getFullNanoTime() {
        return Evaluator.fullTime;
    }

    public static long getSecondHalfNanoTime() {
        return Evaluator.secondHalfTime;
    }

}
