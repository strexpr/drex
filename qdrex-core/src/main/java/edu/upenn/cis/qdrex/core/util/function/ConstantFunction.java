package edu.upenn.cis.qdrex.core.util.function;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkNotNull;
import java.io.Serializable;

public class ConstantFunction<T, R> implements Function<T, R>, Serializable {

    public final R r;

    public ConstantFunction(R r) {
        this.r = checkNotNull(r);
    }

    public static <T, R> ConstantFunction<T, R> of(R r) {
        return new ConstantFunction<>(r);
    }

    @Override
    public R apply(T input) {
        return r;
    }

    @Override
    public String toString() {
        return String.format("(\\x -> %s)", r);
    }

}
