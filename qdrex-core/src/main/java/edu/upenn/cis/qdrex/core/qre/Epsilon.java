package edu.upenn.cis.qdrex.core.qre;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.fj.EpsilonEvaluator;
import edu.upenn.cis.qdrex.core.fj.analysis.AnalysisResult;
import edu.upenn.cis.qdrex.core.regex.Regex;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import edu.upenn.cis.qdrex.core.term.Term;
import static com.google.common.base.Verify.verifyNotNull;

public class Epsilon<P extends Predicate<P, T>, T, C> extends QRE<P, T, C> {

    public final Term<C> t;

    private /* mutable */ AnalysisResult<P, T, C> analysisResult;

    public Epsilon(Term<C> t) {
        super(QRE.Type.EPSILON, t.outType);
        this.t = t;
        this.analysisResult = null;
    }

    @Override
    public int size() {
        return 1;
    }

    @Override
    public ImmutableSet<Parameter> getParameters() {
        return t.getParameters();
    }

    @Override
    public AnalysisResult<P, T, C> fjAnalyze() {
        if (analysisResult == null) {
            Regex<P, T> ans = new edu.upenn.cis.qdrex.core.regex.Epsilon<>();
            analysisResult = AnalysisResult.unambiguous(ans);
            verifyNotNull(analysisResult);
        }
        return analysisResult;
    }

    @Override
    public EpsilonEvaluator<P, T, C> buildEvaluator() {
        return new EpsilonEvaluator<>(this);
    }

    @Override
    public String toString() {
        return String.format("(Epsilon %s)", t);
    }

}
