package edu.upenn.cis.qdrex.core.qre.arith;

import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import edu.upenn.cis.qdrex.core.term.Parameter;
import com.google.common.collect.Sets;
import edu.upenn.cis.qdrex.core.qre.BinaryOp;
import edu.upenn.cis.qdrex.core.qre.Iter;
import edu.upenn.cis.qdrex.core.qre.Split;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermMult;
import static com.google.common.base.Verify.verifyNotNull;

public class Mult<P extends Predicate<P, T>, T> extends BinaryOp<P, T, Double, Double, Double> {

    public Mult(QRE<P, T, Double> f, QRE<P, T, Double> g) {
        super(f, g, g.outType);
    }

    @Override
    public Term<Double> op(Term<Double> tf, Term<Double> tg) {
        return ITermMult.of(tf, tg);
    }

    @Override
    public ImmutableSet<Parameter> getParametersInt() {
        return Sets.union(f.getParameters(), g.getParameters()).immutableCopy();
    }

    @Override
    public String toString() {
        return String.format("(Mult %s %s)", f, g);
    }

    public static <P extends Predicate<P, T>, T> IterMult<P, T> iterMult(QRE<P, T, Double> f,
            boolean emptyDefined) {
        return new IterMult(f, emptyDefined);
    }

    public static class IterMult<P extends Predicate<P, T>, T> extends Iter<P, T, Double, Double, Double> {

        public IterMult(QRE<P, T, Double> f, boolean emptyDefined) {
            super(ConstantTerm.of(1), f, emptyDefined, Double.class);
        }

        @Override
        public Term<Double> combine(Term<Double> tr, Term<Double> tf) {
            return ITermMult.of(tr, tf);
        }

        @Override
        public Term<Double> finalize(Term<Double> tr) {
            return tr;
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            return f.getParameters();
        }

        @Override
        public String toString() {
            return String.format("(Mult.IterMult %s)", f);
        }

    }

    public static class SplitMult<P extends Predicate<P, T>, T>
    extends Split<P, T, Double, Double, Double> {

        private /* mutable */ ImmutableSet<Parameter> parameters;

        public SplitMult(QRE<P, T, Double> f, QRE<P, T, Double> g) {
            super(f, g, g.outType);
            this.parameters = null;
        }

        @Override
        public Term<Double> combine(Term<Double> tf, Term<Double> tg) {
            return ITermMult.of(tf, tg);
        }

        @Override
        public ImmutableSet<Parameter> getParameters() {
            if (parameters == null) {
                parameters = Sets.union(f.getParameters(), g.getParameters()).immutableCopy();
                verifyNotNull(parameters);
            }
            return parameters;
        }

        @Override
        public String toString() {
            return String.format("(Mult.SplitMult %s %s)", f, g);
        }

    }

}
