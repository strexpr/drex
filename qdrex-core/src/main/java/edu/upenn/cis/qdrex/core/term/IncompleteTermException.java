package edu.upenn.cis.qdrex.core.term;

public class IncompleteTermException extends RuntimeException {

    public IncompleteTermException() {
    }

    public IncompleteTermException(Object message) {
        super(String.valueOf(message));
    }

    public IncompleteTermException(Object message, Throwable cause) {
        super(String.valueOf(message), cause);
    }

}
