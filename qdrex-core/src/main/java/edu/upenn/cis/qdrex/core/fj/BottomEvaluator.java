package edu.upenn.cis.qdrex.core.fj;

import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.core.fj.Response.KillUp;
import edu.upenn.cis.qdrex.core.qre.Bottom;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class BottomEvaluator<P extends Predicate<P, T>, T, C> extends Evaluator<P, T, C> {

    public BottomEvaluator(Bottom<P, T, C> bottom) {
        super(bottom);
    }

    @Override
    public void resetHandler() {
        // skip
    }

    @Override
    public ImmutableList<Response<P, T, C>> startHandler(int index) {
        KillUp<P, T, C> response = Response.killUp(this, index);
        return ImmutableList.of(response);
    }

    @Override
    public ImmutableList<Response<P, T, C>> symbolHandler(int index, T t) {
        return ImmutableList.of();
    }

}
