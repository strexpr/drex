package edu.upenn.cis.drex.sa.sed;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.util.assertions.UnreachableCodeException;
import java.io.IOException;
import java.io.PrintStream;
import org.apache.commons.io.IOUtils;

public class SedUtil {

    public static String getSedLocation() {
        for (String sedLoc : SED_LOCATIONS) {
            try {
                Process sed = new ProcessBuilder(sedLoc).start();
                sed.getOutputStream().close();
                return sedLoc;
            } catch (Exception ex) {
            }
        }

        throw new UnreachableCodeException();
    }

    public static String getOutput(String command, String input)
    throws IOException {
        checkNotNull(command);
        checkNotNull(input);

        String sedLocation = getSedLocation();
        Process sed = new ProcessBuilder(sedLocation, command).start();
        try (PrintStream sedIn = new PrintStream(sed.getOutputStream())) {
            byte[] sedInput = input.getBytes("UTF-8");
            sedIn.write(sedInput);
        }

        String ans = IOUtils.toString(sed.getInputStream(), "UTF-8");

        return ans;
    }

    public static final String[] SED_LOCATIONS = new String[] { "sed",
        "C:\\cygwin64\\bin\\sed.exe" };

}
