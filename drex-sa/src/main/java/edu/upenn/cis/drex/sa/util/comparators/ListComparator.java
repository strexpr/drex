package edu.upenn.cis.drex.sa.util.comparators;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Comparator;
import java.util.List;

public class ListComparator<T> implements Comparator<List<T>> {

    public ListComparator(Comparator<T> comparator) {
        this.comparator = checkNotNull(comparator);
    }

    public static <T extends Comparable<T>> ListComparator<T> of() {
        Comparator<T> Tcomparator = new DefaultComparator<>();
        return new ListComparator<>(Tcomparator);
    }

    @Override
    public int compare(List<T> l1, List<T> l2) {
        checkNotNull(l1);
        checkNotNull(l2);

        for (int i = 0; i < l1.size() && i < l2.size(); i++) {
            int ci = comparator.compare(l1.get(i), l2.get(i));
            if (ci != 0) {
                return ci;
            }
        }

        return l1.size() - l2.size();
    }

    public final Comparator<T> comparator;

}
