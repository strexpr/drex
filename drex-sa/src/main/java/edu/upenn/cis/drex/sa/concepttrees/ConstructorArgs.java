package edu.upenn.cis.drex.sa.concepttrees;

import static com.google.common.base.Preconditions.checkArgument;

public class ConstructorArgs {

    public ConstructorArgs(long count, long longestPathLength, long sumOfNodeDepths) {
        checkArgument(count >= 0);
        checkArgument(longestPathLength >= 0);
        checkArgument(sumOfNodeDepths >= 0);

        this.count = count;
        this.longestPathLength = longestPathLength;
        this.sumOfNodeDepths = sumOfNodeDepths;
    }

    public final long count, longestPathLength, sumOfNodeDepths;

}
