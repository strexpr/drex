package edu.upenn.cis.drex.sa.util.collections;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;
import java.util.Comparator;
import java.util.Iterator;

public class IntersectionIterator<T> implements Iterator<T> {

    public IntersectionIterator(Iterator<T> iterator1, Iterator<T> iterator2,
            Comparator<T> comparator) {
        this.iterator1 = Iterators.peekingIterator(checkNotNull(iterator1));
        this.iterator2 = Iterators.peekingIterator(checkNotNull(iterator2));
        this.comparator = checkNotNull(comparator);
    }

    @Override
    public boolean hasNext() {
        advance();
        return iterator1.hasNext() && iterator2.hasNext();
    }

    @Override
    public T next() {
        advance();
        iterator1.next();
        return iterator2.next();
    }

    private void advance() {
        while (iterator1.hasNext() && iterator2.hasNext()) {
            int comparison = comparator.compare(iterator1.peek(), iterator2.peek());
            if (comparison < 0) {
                iterator1.next();
            } else if (comparison == 0) {
                break;
            } else {
                iterator2.next();
            }
        }
    }

    private final PeekingIterator<T> iterator1, iterator2;
    public final Comparator<T> comparator;

}
