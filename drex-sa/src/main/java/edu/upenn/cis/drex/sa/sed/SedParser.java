package edu.upenn.cis.drex.sa.sed;

import edu.upenn.cis.drex.core.util.assertions.UnreachableCodeException;

import edu.upenn.cis.drex.core.expr.*;
import edu.upenn.cis.drex.core.regex.*;
import theory.*;

import org.apache.commons.lang3.tuple.ImmutablePair;
import com.google.common.collect.ImmutableList;
import java.io.IOException;
import java.io.InputStream;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.IOUtils;

public class SedParser {

    public boolean line_by_line = true;
    public boolean id_on_line = false;
    public boolean programming = false;
    public boolean pretty_print = false;
    public boolean format = false;
    public boolean delete_line = false;
    public boolean delete_multi_lines = false;
    public boolean delete_upto_newline = false;

    private int line_number = -1;
    private String[] lines = null;

    public final List<String> epsilon_constants = new ArrayList<String>();
    public final List<CharFunc> char_funcs = new ArrayList<CharFunc>();
    public final List<Expression> base_exprs = new ArrayList<Expression>();
    public final List<CharPred> char_preds = new ArrayList<CharPred>();
    public final Map<String, Regex> primary_regexes = new java.util.HashMap<String, Regex>();
    public final Map<String, List<Regex>> regex_backrefs = 
        new java.util.HashMap<String, List<Regex>>();

    private String currentLine()
    {
        return lines[line_number];
    }
    private String previousLine()
    {
        return lines[line_number-1];
    }

    private String chompLine()
    {
        line_number = line_number + 1; 
        return lines[line_number - 1];
    }

    private boolean chompLine(String line)
    {
        if (lines[line_number].equals(line)) {
            line_number = line_number + 1;
            return true;
        }
        return false;
    }

    private String chompLineWithPrefix(String prefix)
    {
        String line = lines[line_number];
        if (line.startsWith(prefix)) {
            line_number = line_number + 1;
            return line.substring(prefix.length());
        }
        return null;
    }

    private void unexpectedLine()
        throws Exception
    {
        throw new ParseException(
                "Unexpected line: " + chompLine(),
                line_number-1);
    }

    private void chompExpectedEOF()
        throws Exception
    {
        if (line_number < lines.length)
            unexpectedLine();
    }

    private void chompExpectedLine(String s)
        throws Exception
    {
        if (chompLine(s))
            return;
        unexpectedLine();
    }

    private String chompExpectedPrefix(String prefix)
        throws Exception
    {
        String rest = chompLineWithPrefix(prefix);
        if (rest == null)
            unexpectedLine();
        return rest;
    }

    public SedParser(String command)
        throws Exception
    {
        parseSedProgram(command);
    }

    public Regex parseRegexParseTreeNode(String primary_regex_text)
        throws Exception
    {
        Regex ret, left, right;
        CharPred c;
        int value;
        switch (chompExpectedPrefix("REGEX TREE NODE BEGIN: ")) {
            case "CONCAT":
                left = parseRegexParseTreeNode(primary_regex_text);
                right = parseRegexParseTreeNode(primary_regex_text);
                ret = Concat.of(left, right);
                break;
            case "CHARACTER":
                value = Integer.parseInt(chompLine());
                c = new CharPred((char)value);
                char_preds.add(c);
                ret = new edu.upenn.cis.drex.core.regex.Symbol(c);
                break;
            case "END_OF_RE":
                ret = new edu.upenn.cis.drex.core.regex.Epsilon();
                break;
            case "SUBEXP":
                ret = parseRegexParseTreeNode(primary_regex_text);
                regex_backrefs.get(primary_regex_text).add(ret);
                break;
            case "OP_DUP_ASTERISK":
                Regex child = parseRegexParseTreeNode(primary_regex_text);
                ret = new edu.upenn.cis.drex.core.regex.Star(child);
                break;
            case "OP_ALT":
                left = parseRegexParseTreeNode(primary_regex_text);
                right = parseRegexParseTreeNode(primary_regex_text);
                ret = new Union(left, right);
                break;
            case "SIMPLE_BRACKET":
                List<Character> chars = new ArrayList<Character>();
                while (!chompLine("END CHARACTERS")) {
                    value = Integer.parseInt(chompExpectedPrefix("CHARACTER: "));
                    chars.add((char) value);
                }
                c = CharPred.of(ImmutableList.copyOf(chars));
                char_preds.add(c);
                ret = new edu.upenn.cis.drex.core.regex.Symbol(c);
                break;
            case "COMPLEX_BRACKET":
                List<ImmutablePair<Character, Character>> char_ranges 
                    = new ArrayList<ImmutablePair<Character, Character>>();
                int nmb_chars = Integer.parseInt(chompExpectedPrefix("NMBCHARS "));
                for (int i = 0; i < nmb_chars; i++) {
                    value = Integer.parseInt(chompExpectedPrefix("MBCHAR "));
                    char_ranges.add(ImmutablePair.of((char) value, (char) value));
                }
                chompExpectedLine("END MBCHARS");

                int collated_symbols = Integer.parseInt(chompExpectedPrefix("NCOLLSYMS "));
                int equivalence_classes = Integer.parseInt(chompExpectedPrefix("NEQUIV "));
                if (collated_symbols != 0 || equivalence_classes != 0)
                    throw new Exception("U WOT M8?");

                int nranges = Integer.parseInt(chompExpectedPrefix("NRANGES "));
                for (int i = 0; i < nranges; i++) {
                    int start = Integer.parseInt(chompExpectedPrefix("RANGE START: "));
                    int end = Integer.parseInt(chompExpectedPrefix("RANGE END: "));
                    char_ranges.add(ImmutablePair.of((char) start, (char) end));
                }

                int ncharclasses = Integer.parseInt(chompExpectedPrefix("NCHARCLASSES "));
                for (int i = 0; i < ncharclasses; i++) {
                    String cclass = chompLine();
                    switch(cclass) {
                        case "alnum":
                            char_ranges.addAll(StdCharPred.ALPHA_NUM.intervals);
                            break;
                        case "alpha":
                            char_ranges.addAll(StdCharPred.ALPHA.intervals);
                            break;
                        case "blank":
                            char_ranges.addAll(StdCharPred.BLANK.intervals);
                            break;
                        case "cntrl":
                            char_ranges.addAll(StdCharPred.CNTRL.intervals);
                            break;
                        case "digit":
                            char_ranges.addAll(StdCharPred.NUM.intervals);
                            break;
                        case "graph":
                            char_ranges.addAll(StdCharPred.GRAPH.intervals);
                            break;
                        case "lower":
                            char_ranges.addAll(StdCharPred.LOWER_ALPHA.intervals);
                            break;
                        case "punct":
                            char_ranges.addAll(StdCharPred.PUNCT.intervals);
                            break;
                        case "space":
                            char_ranges.addAll(StdCharPred.SPACES.intervals);
                            break;
                        case "upper":
                            char_ranges.addAll(StdCharPred.UPPER_ALPHA.intervals);
                            break;
                        case "xdigit":
                            throw new UnreachableCodeException("ASDSA");
                    }
                }

                int non_match = Integer.parseInt(chompExpectedPrefix("NONMATCH "));
                if (non_match == 1)
                    c = new CharPred(
                            CharPred.invertIntervals(ImmutableList.copyOf(char_ranges))
                            );
                else
                    c = new CharPred(ImmutableList.copyOf(char_ranges));
                char_preds.add(c);
                ret = new edu.upenn.cis.drex.core.regex.Symbol(c);

                break;
            default:
                throw new ParseException(
                        "Unknown regex operator: " + previousLine(),
                        line_number);
        }
        chompExpectedLine("REGEX TREE NODE END: ");
        return ret;
    }

    public Regex parseRegex(String regex_text)
        throws Exception
    {
        regex_backrefs.put(regex_text, new ArrayList<Regex>());
        Regex regex = parseRegexParseTreeNode(regex_text);
        String regex_end_text = chompExpectedPrefix("REGEX PARSE END: ");
        if (!regex_end_text.equals(regex_text))
            throw new ParseException(
                    "Regex start does not match end (" 
                    + regex_text 
                    + ") (" 
                    + regex_end_text + ")",
                    line_number);
        return regex;
    }

    public String parseAIC()
        throws Exception
    {
        String size_prefix = "SIZE IN BYTES ";
        String text = "";
        String byte_string;
        int bytes = -1;
        boolean in_text = false;
        while (true) {
            if ((byte_string = chompLineWithPrefix(size_prefix)) != null) {
                bytes = Integer.parseInt(byte_string);
            } else if (chompLine("TEXT START:")) {
                in_text = true;
            } else if (chompLine("TEXT END:")) {
                return text;
            } else if (in_text) {
                text = text + chompLine() + "\n";
            } else {
                unexpectedLine();
            }
        }
    }

    public ImmutablePair<String, Integer> parseReplacementComponent()
        throws Exception
    {
        chompExpectedLine("REPL_TYPE:");
        String repl_type = chompLine();
        if (!repl_type.equals("ASIS"))
            throw new Exception("\\[lLuU] not handled yet!");

        String prefix = null;
        Integer register = null;
        while (true) {
            if (chompLine("REPLACEMENT COMPONENT END")) {
                return ImmutablePair.of(prefix, register);
            } else if (chompLine("STRING BEGIN")) {
                int bytes = Integer.parseInt(chompLineWithPrefix("BYTES "));
                while (!chompLine("STRING END")) {
                    if (prefix == null)
                        prefix = chompLine();
                    else
                        prefix = prefix + "\n" + chompLine();
                }
            } else if (chompLine("REGISTER")) {
                register = Integer.parseInt(chompLine());
            }
        }
    }

    public List<ImmutablePair<String, Integer>> parseReplacement()
        throws Exception
    {
        List<ImmutablePair<String, Integer>> replacement_components
            = new ArrayList<ImmutablePair<String, Integer>>();
        while (chompLine("REPLACEMENT COMPONENT BEGIN"))
            replacement_components.add(parseReplacementComponent());

        chompExpectedLine("REPLACEMENT END");
        return replacement_components;
    }

    public Expression makeSubstitution(
            String regex_text, 
            List<ImmutablePair<String, Integer>> replacement) 
    {
        return null;
    }

    public Expression parseS()
        throws Exception
    {
        String regex_text = chompLineWithPrefix("REGEX: ");
        if (!primary_regexes.containsKey(regex_text))
            throw new ParseException(
                    "Unable to find regex for substitution: " 
                    + regex_text, line_number);

        chompExpectedLine("REPLACEMENT BEGIN");
        List<ImmutablePair<String, Integer>> replacement
            = parseReplacement();

        return makeSubstitution(regex_text, replacement);
    }

    public Expression parseY()
        throws Exception
    {
        List<ImmutablePair<Character, Character>> table =
            new ArrayList<ImmutablePair<Character, Character>>();
        while (!chompLine("END")) {
            String line = chompLine();
            String[] t = line.split(" -> ");
            if (t.length != 2 || t[0].length() != 1 || t[1].length() != 1)
                throw new ParseException(
                        "Unable to parse translate: " + line,
                        line_number);
            table.add(ImmutablePair.of(t[0].charAt(0), t[1].charAt(0)));
        }
        return ExpressionUtil.sedY(ImmutableList.copyOf(table));
    }

    public void parseCommand()
        throws Exception
    {
        String line = chompLine();
        switch(line.charAt(0)) {
            case 'y':
                base_exprs.add(parseY());
                break;
            case '{':
            case '}':
            case ':':
            case 'T':
            case 'b':
            case 't':
                line_by_line = false;
                programming = true;
                break;
            case 'p':
            case 'P':
                id_on_line = true;
                break;
            case 'l':
            case 'L':
                pretty_print = true;
                format = true;
                break;
            case 's':
                parseS();
                break;
            case 'a':
            case 'i':
            case 'c':
                epsilon_constants.add(parseAIC());
                break;
            case 'd':
            case 'q':
            case 'Q':
            case 'z':
                delete_multi_lines = true;
            case 'D':
                delete_line = true;
                break;
            case 'e':
            case 'W':
            case 'w':
            case 'r':
            case 'R':
            case '=':
            case 'F':
                throw new Exception("Cannot express in DReX");
            case 'n':
            case 'N':
            case 'x':
            case 'g':
            case 'G':
            case 'h':
            case 'H':
                line_by_line = false;
                break;
            default:
                throw new ParseException(
                        "Unexpected command character in line: " + line,
                        line_number);
        }
        chompExpectedLine("COMMAND END:");
    }

    public void parseProgram()
        throws Exception
    {
        while (chompLine("COMMAND BEGIN:"))
            parseCommand();
        chompExpectedLine("PROGRAM END:");
    }

    public void parseSedProgram()
        throws Exception
    {
        String text;
        while ((text = chompLineWithPrefix("REGEX PARSE BEGIN: ")) != null) {
            primary_regexes.put(text, parseRegex(text));
        }
        chompExpectedLine("PROGRAM BEGIN:");
        parseProgram();
        chompExpectedEOF();
    }

    public void parseSedOutput(String program)
        throws Exception
    {
        lines = program.split(System.getProperty("line.separator"));
        line_number = 0;
        parseSedProgram();
    }

    public void extract()
    {
        for (CharPred pred : char_preds)
            System.out.println("PRED: " + pred);
        for (CharFunc func : char_funcs)
            System.out.println("FUNC: " + func);
        for (String cons : epsilon_constants)
            System.out.println("EPSILON CONSTANT: " + cons);
        for (String rtext : primary_regexes.keySet()) {
            System.out.println(
                    "REGEX: " 
                    + rtext 
                    + " -> " 
                    + primary_regexes.get(rtext));
            for (Regex r : regex_backrefs.get(rtext))
                System.out.println("BACK REF: " + r);
        }
        System.out.println("DONE");
    }

    public void parseSedProgram(String command)
        throws Exception
    {
        Process sed = new ProcessBuilder(getMSedLocation(), command).start();
        sed.getOutputStream().close();
        String output = IOUtils.toString(sed.getInputStream());
        System.out.println(output);
        parseSedOutput(output);
    }

    public String getMSedLocation() throws IOException {
        ClassLoader loader = SedParser.class.getClassLoader();
        InputStream stream = loader.getResourceAsStream("msed");
        String ans = IOUtils.toString(stream);
        return ans.trim();
    }

}
