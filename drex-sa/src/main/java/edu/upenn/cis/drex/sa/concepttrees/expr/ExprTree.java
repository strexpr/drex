package edu.upenn.cis.drex.sa.concepttrees.expr;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Lists;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.sa.concepttrees.ConstructorArgs;
import edu.upenn.cis.drex.sa.concepttrees.regex.RegexTree.SplitRegexTree;
import java.util.List;
import theory.CharSolver;

public abstract class ExprTree {

    private ExprTree(ConstructorArgs args) {
        this.args = checkNotNull(args);
    }

    public Optional<ExprTree> sieve(Expression expr) {
        return ExprTreeUtil.sieve(this, checkNotNull(expr));
    }

    public final ConstructorArgs args;

    public static final ExprTree EMPTY = new EmptyExprTree();

    public static final class EmptyExprTree extends ExprTree {

        public EmptyExprTree() {
            super(new ConstructorArgs(0, 0,0));
        }

    }

    public static final class ExprExprTree extends ExprTree {

        public ExprExprTree(Expression expr) {
            super(new ConstructorArgs(1, 0, 0));
            checkNotNull(expr);
            checkNotNull(expr.getDomainType());
            this.expr = expr;
        }

        public final Expression expr;

    }

    public static final class SplitExprTree extends ExprTree {

        public SplitExprTree(String splitStr, ExprTree posTree, ExprTree negTree) {
            super(SplitRegexTree.getArgs(checkNotNull(posTree).args, checkNotNull(negTree).args));

            this.splitStr = checkNotNull(splitStr);
            this.posTree = posTree;
            this.negTree = negTree;
            this.splitStrArray = Lists.charactersOf(splitStr);
        }

        public static Optional<SplitExprTree> of(Expression e1, Expression e2) {
            checkNotNull(e1);
            checkNotNull(e2);

            Regex e1Domain = checkNotNull(e1.getDomainType());
            Regex e2Domain = checkNotNull(e2.getDomainType());

            ExprTree e1Tree = new ExprExprTree(e1);
            ExprTree e2Tree = new ExprExprTree(e2);

            Optional<SplitRegexTree> regexTree = SplitRegexTree.of(e1Domain, e2Domain);
            if (regexTree.isPresent()) {
                String splitStr = regexTree.get().splitStr;
                CharSolver solver = new CharSolver();
                boolean e1Pos = e1Domain.contains(splitStr, solver);
                boolean e2Pos = e2Domain.contains(splitStr, solver);

                if (e1Pos) {
                    checkAssertion(!e2Pos);
                    SplitExprTree ans = new SplitExprTree(splitStr, e1Tree, e2Tree);
                    return Optional.of(ans);
                } else {
                    checkAssertion(e2Pos);
                    SplitExprTree ans = new SplitExprTree(splitStr, e2Tree, e1Tree);
                    return Optional.of(ans);
                }
            } else {
                return Optional.absent();
            }
        }

        public final String splitStr;
        public final ExprTree posTree, negTree;
        public final List<Character> splitStrArray;

    }

}
