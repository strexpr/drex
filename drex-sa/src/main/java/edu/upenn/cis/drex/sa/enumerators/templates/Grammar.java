package edu.upenn.cis.drex.sa.enumerators.templates;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.Symbol;
import edu.upenn.cis.drex.sa.concepttrees.expr.ExprBank;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds.BasePreds;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import static edu.upenn.cis.drex.sa.enumerators.templates.Production.TemplateType.EXPR_TEMPLATE;
import static edu.upenn.cis.drex.sa.enumerators.templates.Production.TemplateType.REGEX_TEMPLATE;
import edu.upenn.cis.drex.sa.enumerators.templates.std.ConcreteExpressionTemplate;
import edu.upenn.cis.drex.sa.enumerators.templates.std.HoleTemplate;
import edu.upenn.cis.drex.sa.enumerators.templates.std.IfElseTemplate;
import edu.upenn.cis.drex.sa.enumerators.templates.std.IterSumTemplate;
import static edu.upenn.cis.drex.sa.enumerators.templates.std.PosGrammar.IFELSE_BUILD_PREDICATE;
import static edu.upenn.cis.drex.sa.enumerators.templates.std.PosGrammar.ITERSUM_BUILD_PREDICATE;
import static edu.upenn.cis.drex.sa.enumerators.templates.std.PosGrammar.SPLIT_BUILD_PREDICATE;
import static edu.upenn.cis.drex.sa.enumerators.templates.std.PosGrammar.SUM_BUILD_PREDICATE;
import edu.upenn.cis.drex.sa.enumerators.templates.std.RestrictTemplate;
import edu.upenn.cis.drex.sa.enumerators.templates.std.SplitTemplate;
import edu.upenn.cis.drex.sa.enumerators.templates.std.SumTemplate;
import edu.upenn.cis.drex.sa.util.StringUtil;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import theory.CharOffset;
import theory.CharPred;

public abstract class Grammar {

    public Grammar(NonTerminal start, BasePreds basePredStrategy) {
        checkNotNull(start);

        this.start = start;

        this.productions = new HashMap<>();
        this.examples = new HashSet<>();
        this.exampleInputStrings = new HashSet<>();
        this.basePredStartegy = basePredStrategy;
        this.basePreds = new HashSet<>();
        this.constOutputSubstrings = new HashSet<>();
        this.offsets = new HashSet<>();
        this.baseExprs = new HashSet<>();
    }

    @Override
    public String toString() {
        StringBuilder ans = new StringBuilder();
        ans.append(String.format("Start: %s\n", start));
        for (NonTerminal nt : productions.keySet()) {
            for (Production prod : productions.get(nt)) {
                ans.append(String.format("%s ::= %s\n", nt, prod));
            }
        }
        return ans.toString();
    }

    public void populateExamples(Collection<IOExample> examples) {
        this.examples.clear();
        this.examples.addAll(checkNotNull(examples));

        for (IOExample example : examples) {
            this.exampleInputStrings.add(example.input);
        }
    }

    public void populateBasePreds() {
        checkState(examples != null);

        Set<String> baseStrings = new HashSet<>();
        for (IOExample ex : examples) {
            String input = ex.input;
            baseStrings.add(input);
        }

        ImmutableSet<String> immutableBaseStrings = ImmutableSet.copyOf(baseStrings);
        this.basePreds.addAll(CharPreds.makeBaseCharPredsFromStrings(immutableBaseStrings, basePredStartegy));
        System.out.printf("Choosing predicates:\n");
        for (CharPred pred : this.basePreds) {
            System.out.printf("%s\n", pred);
        }
    }

    public void populateConstStrings() {
        checkState(examples != null);
        this.constOutputSubstrings.clear();
        this.constOutputSubstrings.add("");

        for (IOExample example : checkNotNull(examples)) {
            checkNotNull(example);
            if (example.outputKnown()) {
                this.constOutputSubstrings.addAll(StringUtil.getSubstrings(example.getOutput()));
            }
        }
    }

    public void populateOffsets() {
        checkState(examples != null);
        this.offsets.clear();

        for (IOExample ex : checkNotNull(examples)) {
            String input = checkNotNull(ex).input;
            if (ex.outputKnown()) {
                String output = ex.getOutput();
                for (int i = 0; i < input.length(); i++) {
                    for (int j = 0; j < ex.getOutput().length(); j++) {
                        this.offsets.add((long)output.charAt(j) - (long)input.charAt(i));
                    }
                }
            }
        }
    }

    public void populateBaseExprs(Collection<Expression> baseExprs) {
        this.baseExprs.clear();
        this.baseExprs.addAll(baseExprs);
    }

    public List<Expression> getFilteredConcreteExpressions() {
        List<Expression> concreteExpressions = Lists.<Expression>newArrayList(new Bottom());
        concreteExpressions.addAll(baseExprs);

        for (String constant : constOutputSubstrings) {
            if (constant.length() <= 1) {
                concreteExpressions.add(new Epsilon(constant));
            }
        }

        for (CharPred pred : basePreds) {
            concreteExpressions.add(new Symbol(pred, ""));
            for (String constant : constOutputSubstrings) {
                if (constant.length() == 1) {
                    concreteExpressions.add(new Symbol(pred, constant));
                }
            }
        }

        for (CharPred pred : basePreds) {
            for (long offset : offsets) {
                concreteExpressions.add(new Symbol(pred, new CharOffset(offset)));
            }
        }

        ExprBank bank = new ExprBank(StringUtil.getSubstrings(ImmutableSet.copyOf(exampleInputStrings)));
        List<Expression> filteredConcreteExpressions = new ArrayList<>();
        for (Expression e : concreteExpressions) {
            if (bank.inPlaceSieve(e)) {
                filteredConcreteExpressions.add(e);
            }
        }

        return filteredConcreteExpressions;
    }

    public void populateProductions(boolean addIfElse, boolean addRestrict) {
        List<Expression> filteredConcreteExpressions = getFilteredConcreteExpressions();
        List<Production> prodList = new ArrayList<>();
        for (Expression e : filteredConcreteExpressions) {
            prodList.add(new ConcreteExpressionTemplate(e));
        }

        Hole h1 = new Hole(EXPR_TEMPLATE, start, "H1");
        Hole h2 = new Hole(EXPR_TEMPLATE, start, "H2");
        Hole h3 = new Hole(REGEX_TEMPLATE, start, "H3");

        HoleTemplate ht1 = new HoleTemplate(h1);
        HoleTemplate ht2 = new HoleTemplate(h2);
        HoleTemplate ht3 = new HoleTemplate(h3);

        prodList.add(new SplitTemplate(ht1, ht2, false, SPLIT_BUILD_PREDICATE));
        prodList.add(new SplitTemplate(ht1, ht2, true, SPLIT_BUILD_PREDICATE));
        prodList.add(new SumTemplate(ht1, ht2, SUM_BUILD_PREDICATE));
        prodList.add(new IterSumTemplate(ht1, false, ITERSUM_BUILD_PREDICATE));
        prodList.add(new IterSumTemplate(ht1, true, ITERSUM_BUILD_PREDICATE));

        if (addIfElse) {
            prodList.add(new IfElseTemplate(ht1, ht2, IFELSE_BUILD_PREDICATE));
        }

        if (addRestrict) {
            prodList.add(new RestrictTemplate(ht1, ht3));
        }

        productions.clear();
        productions.put(start, prodList);
    }

    public final NonTerminal start;

    public final Map<NonTerminal, Collection<Production>> productions;
    public final Set<IOExample> examples;
    public final Set<String> exampleInputStrings;
    public final BasePreds basePredStartegy;
    public final Set<CharPred> basePreds;
    public final Set<String> constOutputSubstrings;
    public final Set<Long> offsets;
    public final Set<Expression> baseExprs;

}
