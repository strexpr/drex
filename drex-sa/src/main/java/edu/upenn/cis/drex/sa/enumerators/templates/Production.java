package edu.upenn.cis.drex.sa.enumerators.templates;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import java.util.Set;

public abstract class Production {

    public Production(Set<Hole> holes, TemplateType type, Predicate<Expression> buildPredicate) {
        checkNotNull(holes);
        checkNotNull(buildPredicate);

        this.holes = holes;
        this.type = type;
        this.buildPredicate = buildPredicate;
    }

    public abstract ImmutableCollection<IOConstraints> holeEnumeratorsFor(IOExample example);
    public abstract Expression build(ImmutableMap<Hole, Expression> holes);
    public abstract Production substitute(Hole h, Expression e);
    @Override
    public abstract String toString();

    public Expression build() {
        checkState(this.holes.isEmpty());
        return this.build(ImmutableMap.<Hole, Expression>of());
    }

    public enum TemplateType {
        REGEX_TEMPLATE,
        EXPR_TEMPLATE
    }

    public final Set<Hole> holes;
    public final TemplateType type;
    public final Predicate<Expression> buildPredicate;

}
