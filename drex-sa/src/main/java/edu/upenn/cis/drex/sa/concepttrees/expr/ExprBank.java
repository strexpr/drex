package edu.upenn.cis.drex.sa.concepttrees.expr;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.dp.DP;
import edu.upenn.cis.drex.core.expr.Expression;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ExprBank {

    public ExprBank(ImmutableCollection<String> discriminants) {
        checkNotNull(discriminants);
        for (String discriminant : discriminants) {
            checkNotNull(discriminant);
        }

        this.analysedExprSet = new HashSet<>();
        this.discriminants = ImmutableList.copyOf(discriminants);
        this.bank = new HashMap<>();
    }

    public boolean inPlaceSieve(Expression expr) {
        checkNotNull(expr);
        checkNotNull(expr.getDomainType());

        if (analysedExprSet.contains(expr)) {
            return false;
        }
        analysedExprSet.add(expr);

        ImmutableList.Builder<Optional<String>> signatureBuilder = ImmutableList.builder();
        for (String discriminant : discriminants) {
            String result = DP.eval(expr, discriminant);
            signatureBuilder.add(Optional.fromNullable(result));
        }
        ImmutableList<Optional<String>> signature = signatureBuilder.build();

        if (!bank.containsKey(signature)) {
            bank.put(signature, ExprTree.EMPTY);
        }

        ExprTree oldExprTree = bank.get(signature);
        checkAssertion(oldExprTree != null);
        Optional<ExprTree> newExprTree = oldExprTree.sieve(expr);
        if (newExprTree.isPresent()) {
            bank.put(signature, newExprTree.get());
            return true;
        } else {
            return false;
        }
    }

    private final Set<Expression> analysedExprSet;
    public final ImmutableList<String> discriminants;
    private final Map<ImmutableList<Optional<String>>, ExprTree> bank;

}
