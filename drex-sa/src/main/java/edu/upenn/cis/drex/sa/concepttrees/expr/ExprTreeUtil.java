package edu.upenn.cis.drex.sa.concepttrees.expr;

import automata.sfa.SFA;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.assertNotNull;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.assertions.UnreachableCodeException;
import edu.upenn.cis.drex.sa.concepttrees.expr.ExprTree.EmptyExprTree;
import edu.upenn.cis.drex.sa.concepttrees.expr.ExprTree.ExprExprTree;
import edu.upenn.cis.drex.sa.concepttrees.expr.ExprTree.SplitExprTree;
import java.util.Stack;
import javax.annotation.Nullable;
import theory.CharPred;
import theory.CharSolver;

public class ExprTreeUtil {

    public static Optional<ExprTree> sieve(ExprTree tree, Expression expr) {
        checkNotNull(tree);
        checkNotNull(expr);

        Regex domainType = checkNotNull(expr.getDomainType());
        SFA<CharPred, Character> sfa = assertNotNull(domainType.getSFA());
        Stack<SieveStackElement> sieveStack = new Stack();
        CharSolver solver = new CharSolver();

        while (tree instanceof SplitExprTree) {
            SplitExprTree splitTree = (SplitExprTree)tree;
            if (sfa.accepts(splitTree.splitStrArray, solver)) {
                sieveStack.push(new SieveStackElement(splitTree.splitStr, null,
                        splitTree.negTree));
                tree = splitTree.posTree;
            } else {
                sieveStack.push(new SieveStackElement(splitTree.splitStr,
                        splitTree.posTree, null));
                tree = splitTree.negTree;
            }
        }

        ExprTree ans = null;
        if (tree instanceof EmptyExprTree) {
            ans = new ExprExprTree(expr);
        } else if (tree instanceof ExprExprTree) {
            Expression treeExpr = ((ExprExprTree)tree).expr;
            Optional<SplitExprTree> optAns = SplitExprTree.of(treeExpr, expr);
            if (!optAns.isPresent()) {
                return Optional.absent();
            } else {
                ans = optAns.get();
            }
        } else {
            throw new UnreachableCodeException();
        }
        checkAssertion(ans != null);

        while (!sieveStack.empty()) {
            ans = sieveStack.pop().build(ans);
        }
        return Optional.of(ans);
    }

}

class SieveStackElement {

    public SieveStackElement(String splitStr, @Nullable ExprTree posTree,
            @Nullable ExprTree negTree) {
        this.splitStr = checkNotNull(splitStr);
        checkArgument(posTree != null || negTree != null);
        checkArgument(posTree == null || negTree == null);
        this.posTree = posTree;
        this.negTree = negTree;
    }

    public SplitExprTree build(ExprTree tree) {
        checkNotNull(tree);
        if (posTree == null) {
            return new SplitExprTree(splitStr, tree, negTree);
        } else {
            return new SplitExprTree(splitStr, posTree, tree);
        }
    }

    public final String splitStr;
    public final @Nullable ExprTree posTree;
    public final @Nullable ExprTree negTree;

}
