package edu.upenn.cis.drex.sa.util.collections;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import java.util.Iterator;

public class Product<T, U, V> implements Iterator<V> {

    public Product(Iterator<T> leftIterator, Iterable<U> rightBacking,
            BiFunction<T, U, V> transformer) {
        this.leftIterator = Iterators.peekingIterator(checkNotNull(leftIterator));
        this.rightBacking = checkNotNull(rightBacking);
        this.transformer = checkNotNull(transformer);
        this.rightView = rightBacking.iterator();
    }

    public static <T, U, V> Product<T, U, V> of(Iterable<T> left,
            Iterable<U> right, BiFunction<T, U, V> transformer) {
        checkNotNull(left);
        checkNotNull(right);
        checkNotNull(transformer);

        return new Product(left.iterator(), right, transformer);
    }

    @Override
    public boolean hasNext() {
        if (leftIterator.hasNext()) {
            tryResetRight();
            return leftIterator.hasNext() && rightView.hasNext();
        } else {
            return false;
        }
    }

    @Override
    public V next() {
        tryResetRight();
        return transformer.apply(leftIterator.peek(), rightView.next());
    }

    private void tryResetRight() {
        if (!rightView.hasNext()) {
            leftIterator.next();
            rightView = rightBacking.iterator();
        }
    }

    private final PeekingIterator<T> leftIterator;
    private final Iterable<U> rightBacking;
    private final BiFunction<T, U, V> transformer;
    private /* mutable */ Iterator<U> rightView;

}