package edu.upenn.cis.drex.sa.enumerators.templates.std;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.Hole;
import edu.upenn.cis.drex.sa.enumerators.templates.IOConstraints;
import edu.upenn.cis.drex.sa.enumerators.templates.Production;
import edu.upenn.cis.drex.sa.enumerators.templates.ProductionUtil;

public class IfElseTemplate extends Production {

    public IfElseTemplate(Production t1, Production t2, Predicate<Expression> buildPredicate) {
        super(Sets.union(checkNotNull(t1).holes, checkNotNull(t2).holes).immutableCopy(),
                t1.type, checkNotNull(buildPredicate));

        checkArgument(t1.type == t2.type);

        this.t1 = t1;
        this.t2 = t2;
    }

    @Override
    public ImmutableCollection<IOConstraints> holeEnumeratorsFor(IOExample example) {
        checkNotNull(example);

        ImmutableCollection<IOConstraints> t1ExConstraints = t1.holeEnumeratorsFor(example);
        ImmutableCollection<IOConstraints> t2ExConstraints = t2.holeEnumeratorsFor(example);

        if (example.outputKnown() || example.defined) {
            IOExample nex = IOExample.undefinedOn(example.input);
            ImmutableCollection<IOConstraints> t1NexConstraints = t1.holeEnumeratorsFor(nex);
            ImmutableCollection<IOConstraints> t2NexConstraints = t2.holeEnumeratorsFor(nex);

            ImmutableList.Builder<IOConstraints> ansBuilder = ImmutableList.builder();
            ansBuilder.addAll(ProductionUtil.combineIOConstraints(t1ExConstraints, t2NexConstraints));
            ansBuilder.addAll(ProductionUtil.combineIOConstraints(t1NexConstraints, t2ExConstraints));
            return ansBuilder.build();
        } else {
            return ProductionUtil.combineIOConstraints(t1ExConstraints, t2ExConstraints);
        }
    }

    @Override
    public Expression build(ImmutableMap<Hole, Expression> holes) {
        checkNotNull(holes);

        Expression e1 = t1.build(holes);
        Expression e2 = t2.build(holes);
        return new IfElse(e1, e2);
    }

    @Override
    public Production substitute(Hole h, Expression e) {
        checkNotNull(h);
        checkNotNull(e);

        Production t1Prime = t1.substitute(h, e);
        Production t2Prime = t2.substitute(h, e);
        return new IfElseTemplate(t1Prime, t2Prime, buildPredicate);
    }

    @Override
    public String toString() {
        return String.format("(ifelse-template %s %s)", t1, t2);
    }

    public final Production t1, t2;

}
