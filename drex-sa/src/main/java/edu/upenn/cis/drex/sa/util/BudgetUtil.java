package edu.upenn.cis.drex.sa.util;

import com.google.common.base.Function;
import com.google.common.base.Supplier;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import edu.upenn.cis.drex.sa.util.collections.CollectionUtil;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BudgetUtil {
    
    public static Iterator<Expression> withBudget(
            final int budget,
            final Function<Integer, Iterator<Expression>> firstBit,
            final BiFunction<Integer, Expression, Iterator<Expression>> secondBit) {
        
        List<Supplier<Iterator<Expression>>> suppliers = new ArrayList<>();
        for (int i = 0; i <= budget; i++) {
            final int firstBitBudget = i;
            final int secondBitBudget = budget - i;
            suppliers.add(new Supplier<Iterator<Expression>>() {
                final Iterator<Expression> mids = firstBit.apply(firstBitBudget);

                @Override
                public Iterator<Expression> get() {
                    return CollectionUtil.lazyFlatMap(mids, secondBit.partialApply(secondBitBudget));
                }
            });
        }
        return CollectionUtil.lazyConcat(suppliers);
    }

}
