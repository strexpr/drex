package edu.upenn.cis.drex.sa.enumerators.expr;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import edu.upenn.cis.drex.sa.enumerators.base.AbstractPoolGroup;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.NonTerminal;
import edu.upenn.cis.drex.sa.enumerators.templates.std.MultiGrammar;
import edu.upenn.cis.drex.sa.util.BudgetUtil;
import edu.upenn.cis.drex.sa.util.StringUtil;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class MultiEnumeratorPoolGroup extends AbstractPoolGroup {

    public MultiEnumeratorPoolGroup(MultiGrammar grammar, Collection<IOExample> examples) {
        super(grammar, examples);

        // Initialize pools
        this.pools = new HashMap<>();
        this.buildPools(this.examples);
        this.matchExtender = new ExtendExpressionUtil(this);
    }

    private void buildPools(Collection<IOExample> examples)
    {
        for (IOExample example : examples) if (example.outputKnown()) {
            for (String input : StringUtil.getSubstrings(example.input)) {
                for (String output : StringUtil.getSubstrings(example.getOutput())) {
                    IOExample subExample = IOExample.io(input, output);
                    if (!this.pools.containsKey(subExample)) {
                        MultiEnumeratorPool pool = new MultiEnumeratorPool(this, subExample);
                        this.pools.put(subExample, pool);
                    }
                }
            }
        }
    }

    @Override
    public AbstractPoolGroup addExample(IOExample ex)
    {
        // TODO! Throw exception if example added multiply.
        checkNotNull(ex);
        checkNotNull(this.examples);

        this.examples.add(ex);
        this.buildPools(this.examples);
        this.grammar.populateExamples(examples);
        this.grammar.populateConstStrings();
        this.grammar.populateOffsets();
        this.grammar.populateBasePreds();
        this.grammar.populateBaseExprs(this.grammar.baseExprs);
        this.grammar.populateProductions(false, true);

        return this;
    }

    @Override
    public Iterator<Expression> fromAllPools(NonTerminal holeType, int size) {
        return Iterators.<Expression>singletonIterator(new Bottom());
    }

    @Override
    public Iterator<Expression> enumerate(
            final NonTerminal holeType,
            final int size,
            ImmutableCollection<IOExample> localExamples) {

        IOExample possibleGoodExample = null;
        for (IOExample ioExample : localExamples) {
            if (ioExample.outputKnown() && pools.containsKey(ioExample)) {
                if (possibleGoodExample == null || possibleGoodExample.input.length() < ioExample.input.length())
                    possibleGoodExample = ioExample;
            }
        }
        if (possibleGoodExample == null) {
            return ExprEnumeratorUtil.whichMatch(fromAllPools(holeType, size), localExamples);
        }
        final IOExample goodExample = possibleGoodExample;

        Iterator<Expression> ans = Collections.emptyIterator();
        final ImmutableList<IOExample> restExamples = ImmutableList.copyOf(
                Iterators.filter(
                        localExamples.iterator(),
                        Predicates.not(Predicates.equalTo(goodExample))
                ));

        final Function<Integer, Iterator<Expression>> matchFirst =
        new Function<Integer, Iterator<Expression>>() {
            @Override
            public Iterator<Expression> apply(Integer initialBudget) {
                return pools.get(goodExample).ofSize(initialBudget, holeType).iterator();
            }
        };
        final BiFunction<Integer, Expression, Iterator<Expression>> matchRest =
                new BiFunction<Integer, Expression, Iterator<Expression>>() {
            @Override
            public Iterator<Expression> apply(Integer remainingBudget, Expression e) {
                return matchExtender.match(e, remainingBudget, restExamples);
            }
        };
        return BudgetUtil.withBudget(size, matchFirst, matchRest);
    }

    private final Map<IOExample, MultiEnumeratorPool> pools;
    private final ExtendExpressionUtil matchExtender;

}
