package edu.upenn.cis.drex.sa.enumerators.base;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import edu.upenn.cis.drex.sa.enumerators.expr.ExprEnumeratorUtil;
import static edu.upenn.cis.drex.sa.enumerators.expr.BlindExprPool.CONSISTENT_FILTER;
import edu.upenn.cis.drex.sa.enumerators.templates.NonTerminal;
import edu.upenn.cis.drex.sa.enumerators.templates.IOConstraints;
import edu.upenn.cis.drex.sa.enumerators.templates.Production;
import edu.upenn.cis.drex.sa.enumerators.templates.Hole;
import edu.upenn.cis.drex.sa.util.BudgetUtil;
import edu.upenn.cis.drex.sa.util.collections.Buffer;
import edu.upenn.cis.drex.sa.util.collections.CollectionUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractPool {

    public AbstractPool(AbstractPoolGroup poolGroup, IOExample example) {
        this.poolGroup = checkNotNull(poolGroup);
        this.example = checkNotNull(example);
    }

    public abstract Buffer<Expression> ofSize(int size, NonTerminal nt);

    public Iterator<Expression> build(int size, NonTerminal nt) {
        List<Supplier<Iterator<Expression>>> productions =  new ArrayList<>();
        for (Production prodn : poolGroup.grammar.productions.get(nt)) {
            if (size >= 1 + prodn.holes.size()) {
                productions.add(lazyBuildProdn(size, prodn));
            }
        }
        Iterator<Expression> ans = CollectionUtil.lazyConcat(productions);
        ans = Iterators.filter(ans, CONSISTENT_FILTER);
        ans = ExprEnumeratorUtil.whichMatch(ans, example);
        return ans;
    }

    public Supplier<Iterator<Expression>> lazyBuildProdn(final int size,
            final Production prodn) {
        checkArgument(size >= 0);
        checkNotNull(prodn);

        return new Supplier<Iterator<Expression>>() {
            @Override
            public Iterator<Expression> get() {
                return buildProdn(size, prodn);
            }
        };
    }

    public Iterator<Expression> buildProdn(int size, Production prodn) {
        checkArgument(size >= 1 + prodn.holes.size());
        checkNotNull(prodn);

        List<Supplier<Iterator<Expression>>> ansList = new ArrayList<>();
        if (size > prodn.holes.size()) {
            for (IOConstraints disjunct : prodn.holeEnumeratorsFor(example)) {
                ansList.add(lazyBuildProdnConstraints(size - 1, prodn, disjunct));
            }
        }
        Iterator<Expression> ans = CollectionUtil.lazyConcat(ansList);
        ans = Iterators.filter(ans, prodn.buildPredicate);
        /*
        ans = CollectionUtil.signalingIterator(
                ans,
                "Iterator for " + prodn + " (" + size + ")", 
                System.out);
        */
        return ans;
    }

    public Supplier<Iterator<Expression>> lazyBuildProdnConstraints(final int size,
            final Production prodn, final IOConstraints constraints) {
        checkArgument(size >= 0);
        checkNotNull(prodn);
        checkNotNull(constraints);

        return new Supplier<Iterator<Expression>>() {
            @Override
            public Iterator<Expression> get() {
                return buildProdnConstraints(size, prodn, constraints);
            }
        };
    }

    public Iterator<Expression> buildProdnConstraints(final int size,
            final Production prodn, final IOConstraints constraints) {
        checkArgument(size >= 0);
        checkNotNull(prodn);
        checkNotNull(constraints);

        if (prodn.holes.isEmpty()) {
            Expression expr = prodn.build();
            if (expr.getDomainType() != null && example.matches(expr)) {
                return Iterators.singletonIterator(expr);
            } else {
                return Collections.emptyIterator();
            }
        }

        final Hole h = prodn.holes.iterator().next();
        final ImmutableSet<IOExample> hConstraints = constraints.getHoleConstraints(h);
        
        // System.out.printf("For size %d and production %s for pool %s, hConstraints size %d\n", size, prodn, this.example, hConstraints.size());
        
        Function<Integer, Iterator<Expression>> firstHoleExprs = 
        new Function<Integer, Iterator<Expression>>() {
            @Override
            public Iterator<Expression> apply(Integer initialBudget) {
                return poolGroup.enumerate(h.holeType, initialBudget, hConstraints);
            }
        };
        BiFunction<Integer, Expression, Iterator<Expression>> generateRestHoles =
        new BiFunction<Integer, Expression, Iterator<Expression>>() {
            @Override
            public Iterator<Expression> apply(Integer remainingBudget, Expression he) {
                Production prodnPrime = prodn.substitute(h, he);
                return buildProdnConstraints(remainingBudget, prodnPrime, constraints);
            }
        };
        return BudgetUtil.withBudget(size, firstHoleExprs, generateRestHoles);
    }

    public static Function<AbstractPool, Iterator<Expression>> OF_SIZE(final int n, final NonTerminal nt) {
        return new Function<AbstractPool, Iterator<Expression>>() {
            @Override
            public Iterator<Expression> apply(AbstractPool f) {
                return f.ofSize(n, nt).iterator();
            }
        };
    }

    public final AbstractPoolGroup poolGroup;
    public final IOExample example;

}
