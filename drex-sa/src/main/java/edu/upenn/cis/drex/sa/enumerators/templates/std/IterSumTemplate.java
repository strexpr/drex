package edu.upenn.cis.drex.sa.enumerators.templates.std;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.Hole;
import edu.upenn.cis.drex.sa.enumerators.templates.IOConstraints;
import edu.upenn.cis.drex.sa.enumerators.templates.Production;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class IterSumTemplate extends Production {

    public IterSumTemplate(Production t, boolean left, Predicate<Expression> buildPredicate) {
        super(checkNotNull(t).holes, t.type, checkNotNull(buildPredicate));
        this.t = t;
        this.left = left;
    }

    @Override
    public ImmutableCollection<IOConstraints> holeEnumeratorsFor(IOExample example) {
        checkNotNull(example);

        String input = example.input;
        ImmutableList.Builder<IOConstraints> ansBuilder = ImmutableList.builder();

        if (input.isEmpty()) {
            if (!example.outputKnown() || example.getOutput().isEmpty()) {
                return IOConstraints.TRUE;
            } else {
                return IOConstraints.FALSE;
            }
        }
        
        ImmutableList<ImmutablePair<IOExample, IOExample>> exampleSplits;
        if (example.outputKnown()) {
            exampleSplits = example.leftNonEmptySplits();
        } else if (example.defined) {
            exampleSplits = example.leftNonEmptyInputOnlySplits();
        } else {
            /*
                On not defined on, we give up and pass very weak constraints. We
                some how have to say that it is not defined on every split. It
                can be done by replacing and and or for IOConstraints.
                Currently, and is combine and or is putting the two
                IOConstraints in a list.
            */
            exampleSplits = ImmutableList.of();
        }
        for (ImmutablePair<IOExample, IOExample> exampleSplit : exampleSplits)
            ansBuilder.addAll(t.holeEnumeratorsFor(exampleSplit.left));

        ImmutableList<IOConstraints> ans = ansBuilder.build();
        return ans;
    }

    @Override
    public Expression build(ImmutableMap<Hole, Expression> holes) {
        checkNotNull(holes);
        return new IteratedSum(t.build(holes), left);
    }

    @Override
    public Production substitute(Hole h, Expression e) {
        checkNotNull(h);
        checkNotNull(e);
        return new IterSumTemplate(t.substitute(h, e), left, buildPredicate);
    }

    @Override
    public String toString() {
        return String.format("(iter-template %s [left = %b])", t, left);
    }

    public final Production t;
    public final boolean left;

}
