package edu.upenn.cis.drex.sa.enumerators.templates.std;

import com.google.common.base.Predicate;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds.BasePreds;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.Grammar;
import edu.upenn.cis.drex.sa.enumerators.templates.NonTerminal;
import java.util.Collection;

public class PosGrammar extends Grammar {

    private PosGrammar(BasePreds basePredStrategy) {
        super(new NonTerminal("Start"), basePredStrategy);
    }

    public static PosGrammar build(BasePreds basePredStrategy, Collection<IOExample> examples,
            Collection<Expression> baseExprs) {
        System.out.printf("Calling PosGrammar.build()\n");
        System.out.printf("basePredStrategy: %s\n", basePredStrategy);
        System.out.printf("baseExprs:\n");
        for (Expression ex : baseExprs) {
            System.out.printf("%s\n", ex);
        }

        PosGrammar ans = new PosGrammar(basePredStrategy);
        ans.populateExamples(examples);
        ans.populateConstStrings();
        ans.populateOffsets();
        ans.populateBasePreds();
        ans.populateBaseExprs(baseExprs);
        ans.populateProductions(true, true);
        return ans;
    }

    public static final Predicate<Expression> IFELSE_BUILD_PREDICATE = new Predicate<Expression>(){
        @Override
        public boolean apply(Expression t) {
            return t instanceof IfElse && !(((IfElse)t).e1 instanceof IfElse);
        }
    };

    public static final Predicate<Expression> ITERSUM_BUILD_PREDICATE = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return t instanceof IteratedSum && !(((IteratedSum)t).e instanceof IteratedSum);
        }
    };

    public static final Predicate<Expression> SPLIT_BUILD_PREDICATE = new Predicate<Expression>(){
        @Override
        public boolean apply(Expression t) {
            return t instanceof SplitSum && !(((SplitSum)t).e1 instanceof SplitSum);
        }
    };

    public static final Predicate<Expression> SUM_BUILD_PREDICATE = new Predicate<Expression>(){
        @Override
        public boolean apply(Expression t) {
            return t instanceof Sum && !(((Sum)t).e1 instanceof Sum);
        }
    };

}
