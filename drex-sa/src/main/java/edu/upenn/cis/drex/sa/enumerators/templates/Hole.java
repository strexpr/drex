package edu.upenn.cis.drex.sa.enumerators.templates;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.sa.enumerators.templates.Production.TemplateType;

public class Hole {

    public Hole(TemplateType type, NonTerminal holeType, String name) {
        this.type = type;
        this.holeType = checkNotNull(holeType);
        this.name = checkNotNull(name);
    }

    @Override
    public String toString() {
        return name;
    }

    public final TemplateType type;
    public final NonTerminal holeType;
    public final String name;

}
