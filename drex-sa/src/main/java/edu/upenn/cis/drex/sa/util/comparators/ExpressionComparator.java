package edu.upenn.cis.drex.sa.util.comparators;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import edu.upenn.cis.drex.core.expr.Composition;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IVisitor;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.Restrict;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.expr.Symbol;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.assertions.UnreachableCodeException;
import java.util.Comparator;
import org.apache.commons.lang3.builder.CompareToBuilder;
import theory.CharFunc;

public class ExpressionComparator implements Comparator<Expression> {

    @Override
    public int compare(Expression e1, Expression e2) {
        if (checkNotNull(e1).size != checkNotNull(e2).size) {
            return e1.size - e2.size;
        }

        return new ExpressionComparatorVisitor(e2).visit(e1);
    }

}

class ExpressionComparatorVisitor extends IVisitor<Integer> {

    public ExpressionComparatorVisitor(Expression e2) {
        this.e2 = checkNotNull(e2);
    }

    @Override
    public Integer visitBottom(Bottom e) {
        checkNotNull(e);
        if (e2 instanceof Bottom) {
            return 0;
        } else {
            checkAssertion(e2 instanceof Symbol || e2 instanceof Epsilon);
            return -1;
        }
    }

    @Override
    public Integer visitSymbol(Symbol e) {
        checkNotNull(e);
        if (e2 instanceof Bottom) {
            return 1;
        } else if (e2 instanceof Symbol) {
            Symbol e2s = (Symbol)e2;
            Comparator<CharFunc> charFuncComparator = new CharFuncComparator();
            ListComparator<CharFunc> charFuncListComparator = new ListComparator<>(charFuncComparator);
            return new CompareToBuilder()
                    .append(e.a, e2s.a, new CharPredComparator())
                    .append(e.d, e2s.d, charFuncListComparator)
                    .toComparison();
        } else {
            checkAssertion(e2 instanceof Epsilon);
            return -1;
        }
    }

    @Override
    public Integer visitEpsilon(Epsilon e) {
        checkNotNull(e);
        if (e2 instanceof Bottom || e2 instanceof Symbol) {
            return 1;
        } else if (e2 instanceof Epsilon) {
            Epsilon e2e = (Epsilon)e2;
            return e.d.compareTo(e2e.d);
        } else {
            throw new UnreachableCodeException();
        }
    }

    @Override
    public Integer visitIfElse(IfElse e) {
        checkNotNull(e);
        if (e2 instanceof Bottom || e2 instanceof Symbol
                || e2 instanceof Epsilon) {
            return 1;
        } else if (e2 instanceof IfElse) {
            IfElse e2ie = (IfElse)e2;
            return new CompareToBuilder()
                    .append(e.e1, e2ie.e1, new ExpressionComparator())
                    .append(e.e2, e2ie.e2, new ExpressionComparator())
                    .toComparison();
        } else {
            return -1;
        }
    }

    @Override
    public Integer visitSplitSum(SplitSum e) {
        checkNotNull(e);
        if (e2 instanceof Bottom || e2 instanceof Symbol
                || e2 instanceof Epsilon || e2 instanceof IfElse) {
            return 1;
        } else if (e2 instanceof SplitSum) {
            SplitSum e2ss = (SplitSum)e2;
            return new CompareToBuilder()
                    .append(e.e1, e2ss.e1, new ExpressionComparator())
                    .append(e.e2, e2ss.e2, new ExpressionComparator())
                    .append(e.left, e2ss.left)
                    .toComparison();
        } else {
            return -1;
        }
    }

    @Override
    public Integer visitSum(Sum e) {
        checkNotNull(e);
        if (e2 instanceof Bottom || e2 instanceof Symbol
                || e2 instanceof Epsilon || e2 instanceof IfElse
                || e2 instanceof SplitSum) {
            return 1;
        } else if (e2 instanceof Sum) {
            Sum e2s = (Sum)e2;
            return new CompareToBuilder()
                    .append(e.e1, e2s.e1, new ExpressionComparator())
                    .append(e.e2, e2s.e2, new ExpressionComparator())
                    .toComparison();
        } else {
            return -1;
        }
    }

    @Override
    public Integer visitIteratedSum(IteratedSum e) {
        checkNotNull(e);
        if (e2 instanceof Bottom || e2 instanceof Symbol
                || e2 instanceof Epsilon || e2 instanceof IfElse
                || e2 instanceof SplitSum || e2 instanceof Sum) {
            return 1;
        } else if (e2 instanceof IteratedSum) {
            IteratedSum e2i = (IteratedSum)e2;
            return new CompareToBuilder()
                    .append(e.e, e2i.e, new ExpressionComparator())
                    .append(e.left, e2i.left)
                    .toComparison();
        } else {
            return -1;
        }
    }

    @Override
    public Integer visitChainedSum(ChainedSum e) {
        checkNotNull(e);
        if (e2 instanceof Bottom || e2 instanceof Symbol
                || e2 instanceof Epsilon || e2 instanceof IfElse
                || e2 instanceof SplitSum || e2 instanceof Sum
                || e2 instanceof IteratedSum) {
            return 1;
        } else if (e2 instanceof ChainedSum) {
            ChainedSum e2cs = (ChainedSum)e2;
            return new CompareToBuilder()
                    .append(e.e, e2cs.e, new ExpressionComparator())
                    .append(e.r, e2cs.r, new RegexComparator())
                    .append(e.left, e2cs.left)
                    .toComparison();
        } else {
            return -1;
        }
    }

    @Override
    public Integer visitComposition(Composition e) {
        checkNotNull(e);
        if (e2 instanceof Bottom || e2 instanceof Symbol
                || e2 instanceof Epsilon || e2 instanceof IfElse
                || e2 instanceof SplitSum || e2 instanceof Sum
                || e2 instanceof IteratedSum || e2 instanceof ChainedSum) {
            return 1;
        } else if (e2 instanceof Composition) {
            Composition e2c = (Composition)e2;
            return new CompareToBuilder()
                    .append(e.first, e2c.first, new ExpressionComparator())
                    .append(e.second, e2c.second, new ExpressionComparator())
                    .toComparison();
        } else {
            return -1;
        }
    }

    @Override
    public Integer visitRestrict(Restrict e) {
        checkNotNull(e);
        if (e2 instanceof Bottom || e2 instanceof Symbol
                || e2 instanceof Epsilon || e2 instanceof IfElse
                || e2 instanceof SplitSum || e2 instanceof Sum
                || e2 instanceof IteratedSum || e2 instanceof ChainedSum
                || e2 instanceof Composition) {
            return 1;
        } else if (e2 instanceof Restrict) {
            Restrict e2r = (Restrict)e2;
            return new CompareToBuilder()
                    .append(e.e, e2r.e, new ExpressionComparator())
                    .append(e.r, e2r.r, new RegexComparator())
                    .toComparison();
        } else {
            throw new UnreachableCodeException();
        }
    }

    public final Expression e2;

}
