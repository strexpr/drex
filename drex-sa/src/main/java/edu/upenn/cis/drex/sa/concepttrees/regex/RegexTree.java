package edu.upenn.cis.drex.sa.concepttrees.regex;

import automata.sfa.SFA;
import edu.upenn.cis.drex.sa.concepttrees.ConstructorArgs;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Lists;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import theory.CharPred;
import theory.CharSolver;

public abstract class RegexTree {

    public RegexTree(ConstructorArgs args) {
        this.args = checkNotNull(args);
    }

    public Optional<RegexTree> sieve(Regex regex) {
        return RegexTreeUtil.sieve(this, checkNotNull(regex));
    }

    public final ConstructorArgs args;

    public static final RegexTree EMPTY = new EmptyRegexTree();

    public static final class EmptyRegexTree extends RegexTree {

        public EmptyRegexTree() {
            super(new ConstructorArgs(0, 0, 0));
        }

    }

    public static final class RegexRegexTree extends RegexTree {

        public RegexRegexTree(Regex regex) {
            super(new ConstructorArgs(1, 0, 0));
            checkNotNull(regex);
            checkArgument(regex.getSFA() != null);
            this.regex = regex;
        }

        public final Regex regex;

    }

    public static final class SplitRegexTree extends RegexTree {

        public SplitRegexTree(String splitStr, RegexTree posTree, RegexTree negTree) {
            super(getArgs(checkNotNull(posTree).args, checkNotNull(negTree).args));

            this.splitStr = checkNotNull(splitStr);
            this.posTree = posTree;
            this.negTree = negTree;
            this.splitStrArray = Lists.charactersOf(splitStr);
        }

        public static ConstructorArgs getArgs(ConstructorArgs posTreeArgs,
                ConstructorArgs negTreeArgs) {
            long regexCount = posTreeArgs.count + negTreeArgs.count;
            long longestPathLength = 1 + posTreeArgs.longestPathLength +
                    negTreeArgs.longestPathLength;
            long sumOfNodeDepths = posTreeArgs.sumOfNodeDepths +
                    posTreeArgs.count + negTreeArgs.sumOfNodeDepths +
                    negTreeArgs.count;

            return new ConstructorArgs(regexCount, longestPathLength,
                    sumOfNodeDepths);
        }

        public static Optional<SplitRegexTree> of(Regex r1, Regex r2) {
            checkNotNull(r1);
            checkNotNull(r2);
            checkNotNull(r1.getSFA());
            checkNotNull(r2.getSFA());

            CharSolver solver = new CharSolver();
            @SuppressWarnings("null")
            SFA<CharPred, Character> sfa1 = r1.getSFA().determinize(solver);
            @SuppressWarnings("null")
            SFA<CharPred, Character> sfa2 = r2.getSFA().determinize(solver);

            @SuppressWarnings("null")
            SFA<CharPred, Character> sfa1M2 = sfa1.minus(sfa2, solver);
            if (!sfa1M2.isEmpty()) {
                List<Character> witness = sfa1M2.getWitness(solver);
                checkAssertion(witness != null);
                String splitStr = StringUtils.join(witness, "");
                RegexTree posTree = new RegexRegexTree(r1);
                RegexTree negTree = new RegexRegexTree(r2);
                return Optional.of(new SplitRegexTree(splitStr, posTree, negTree));
            }

            @SuppressWarnings("null")
            SFA<CharPred, Character> sfa2M1 = sfa2.minus(sfa1, solver);
            if (!sfa2M1.isEmpty()) {
                List<Character> witness = sfa2M1.getWitness(solver);
                checkAssertion(witness != null);
                String splitStr = StringUtils.join(witness, "");
                RegexTree posTree = new RegexRegexTree(r2);
                RegexTree negTree = new RegexRegexTree(r1);
                return Optional.of(new SplitRegexTree(splitStr, posTree, negTree));
            }

            return Optional.absent();
        }

        public final String splitStr;
        public final RegexTree posTree, negTree;
        public final List<Character> splitStrArray;

    }

}
