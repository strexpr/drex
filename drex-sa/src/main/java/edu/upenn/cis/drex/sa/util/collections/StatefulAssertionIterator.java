package edu.upenn.cis.drex.sa.util.collections;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.util.function.Consumer;
import java.util.Iterator;

public class StatefulAssertionIterator<T> implements Iterator<T> {

    public StatefulAssertionIterator(Iterator<T> iterator, Consumer<T> consumer) {
        this.iterator = checkNotNull(iterator);
        this.consumer = checkNotNull(consumer);
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public T next() {
        T ans = iterator.next();
        consumer.accept(ans);
        return ans;
    }

    @Override
    public void remove() {
        iterator.remove();
    }

    private final Iterator<T> iterator;
    private final Consumer<T> consumer;

}
