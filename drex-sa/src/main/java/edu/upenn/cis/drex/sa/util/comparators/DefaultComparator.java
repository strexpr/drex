package edu.upenn.cis.drex.sa.util.comparators;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Comparator;

public class DefaultComparator<T extends Comparable<T>> implements Comparator<T> {

    @Override
    public int compare(T t1, T t2) {
        return checkNotNull(t1).compareTo(checkNotNull(t2));
    }

}
