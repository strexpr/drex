package edu.upenn.cis.drex.sa.enumerators.expr;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.sa.enumerators.base.AbstractPoolGroup;
import edu.upenn.cis.drex.core.expr.Expression;
import java.util.Iterator;

public class ExprEnumerator implements Iterator<Expression> {

    public ExprEnumerator(AbstractPoolGroup poolGroup)
    {
        checkNotNull(poolGroup);

        this.poolGroup = poolGroup;
        this.size = 0;
        this.sizeSpecificIterator = poolGroup.enumerate(size);
    }

    @Override
    public boolean hasNext() {
        // Getting this right is difficult
        // So, just return true
        return true;
    }

    @Override
    public Expression next() {
        while (!sizeSpecificIterator.hasNext()) {
            size = size + 1;
            System.out.println("Increasing size to " + size);
            sizeSpecificIterator = poolGroup.enumerate(size);
        }
        return sizeSpecificIterator.next();
    }

    public final AbstractPoolGroup poolGroup;
    private /* mutable */ int size;
    private /* mutable */ Iterator<Expression> sizeSpecificIterator;

}
