package edu.upenn.cis.drex.sa.enumerators.templates.std;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.ExpressionUtil;
import edu.upenn.cis.drex.core.expr.Restrict;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.Hole;
import edu.upenn.cis.drex.sa.enumerators.templates.IOConstraints;
import edu.upenn.cis.drex.sa.enumerators.templates.Production;
import edu.upenn.cis.drex.sa.enumerators.templates.ProductionUtil;

public class RestrictTemplate extends Production {

    public RestrictTemplate(Production te, Production tr) {
        this(checkNotNull(te), checkNotNull(tr), DEFAULT_BUILD_PREDICATE);
    }

    public static final Predicate<Expression> DEFAULT_BUILD_PREDICATE = new Predicate<Expression>(){
        @Override
        public boolean apply(Expression t) {
            return t instanceof Restrict && !(((Restrict)t).e instanceof Restrict);
        }
    };

    public RestrictTemplate(Production te, Production tr, Predicate<Expression> buildPredicate) {
        super(Sets.union(checkNotNull(te).holes, checkNotNull(tr).holes).immutableCopy(),
                TemplateType.EXPR_TEMPLATE, checkNotNull(buildPredicate));

        checkArgument(te.type == TemplateType.EXPR_TEMPLATE);
        // checkArgument(tr.type == TemplateType.REGEX_TEMPLATE);

        this.te = te;
        this.tr = tr;
    }

    @Override
    public ImmutableCollection<IOConstraints> holeEnumeratorsFor(IOExample example) {
        checkNotNull(example);

        ImmutableCollection<IOConstraints> eConstraints = example.defined ?
                te.holeEnumeratorsFor(example) : IOConstraints.TRUE;
        ImmutableCollection<IOConstraints> rConstraints = example.defined ?
                tr.holeEnumeratorsFor(IOExample.io(example.input, "")) :
                tr.holeEnumeratorsFor(example);

        return ProductionUtil.combineIOConstraints(eConstraints, rConstraints);
    }

    @Override
    public Expression build(ImmutableMap<Hole, Expression> holes) {
        checkNotNull(holes);
        Expression ee = te.build(holes);
        Expression re = tr.build(holes);
        return new Restrict(ee, ExpressionUtil.expressionToRegex(re));
    }

    @Override
    public Production substitute(Hole h, Expression e) {
        checkNotNull(h);
        checkNotNull(e);
        return new RestrictTemplate(te.substitute(h, e), tr.substitute(h, e));
    }

    @Override
    public String toString() {
        return String.format("(restrict-template %s %s)", te, tr);
    }

    public final Production te, tr;

}
