package edu.upenn.cis.drex.sa.util.collections;

import java.util.NoSuchElementException;

public interface RawIterator<T> {

    public T next() throws NoSuchElementException;

}
