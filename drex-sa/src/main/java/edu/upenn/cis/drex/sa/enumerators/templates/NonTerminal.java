package edu.upenn.cis.drex.sa.enumerators.templates;

public class NonTerminal {

    public NonTerminal(String name) {
        this.id = NT_ID++;
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public final int id;
    public final String name;
    private static int NT_ID = 0;

}
