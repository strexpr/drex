package edu.upenn.cis.drex.sa.concepttrees.expr;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.sa.util.collections.StatefulFilter;
import edu.upenn.cis.drex.sa.util.collections.StatefulFilter.State;
import java.util.Iterator;

public class IndistinguishableExprFilter {

    public IndistinguishableExprFilter(ImmutableCollection<String> discriminants) {
        checkNotNull(discriminants);
        checkArgument(!discriminants.contains(null));
        bank = new ExprBank(discriminants);
    }

    public Iterator<Expression> filter(Iterator<Expression> iterator) {
        State<Expression> state = new FilterState(this);
        return new StatefulFilter(checkNotNull(iterator), state);
    }

    private final ExprBank bank;

    private class FilterState implements State<Expression> {

        public FilterState(IndistinguishableExprFilter filter) {
            this.filter = checkNotNull(filter);
        }

        @Override
        public boolean update(Expression e) {
            return filter.bank.inPlaceSieve(e);
        }

        public final IndistinguishableExprFilter filter;

    }

}
