package edu.upenn.cis.drex.sa.enumerators.expr;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.Restrict;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.expr.Symbol;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.sa.concepttrees.expr.IndistinguishableExprFilter;
import edu.upenn.cis.drex.sa.enumerators.regex.RegexPool;
import edu.upenn.cis.drex.sa.util.collections.Buffer;
import edu.upenn.cis.drex.sa.util.collections.Product;
import edu.upenn.cis.drex.sa.util.collections.CollectionUtil;
import edu.upenn.cis.drex.sa.util.collections.StatelessAssertionIterator;
import edu.upenn.cis.drex.sa.util.comparators.ExpressionComparator;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import theory.CharOffset;
import theory.CharPred;

public class BlindExprPool {

    public BlindExprPool(ImmutableSet<CharPred> charPreds,
            ImmutableSet<Character> constants,
            ImmutableSet<CharOffset> offsets,
            ImmutableList<String> discriminants) {
        for (CharPred charPred : checkNotNull(charPreds)) {
            checkNotNull(charPred);
        }
        this.charPreds = charPreds;

        for (char constant : checkNotNull(constants)) {
            checkNotNull(constant);
        }
        this.constants = constants;

        for (CharOffset offset : checkNotNull(offsets)) {
            checkNotNull(offset);
        }
        this.offsets = offsets;

        this.expressions = new ArrayList<>();

        for (String discriminant : checkNotNull(discriminants)) {
            checkNotNull(discriminant);
        }
        this.indistinguishableExprFilter = new IndistinguishableExprFilter(discriminants);

        this.regexPool = new RegexPool(charPreds);
    }

    public Buffer<Expression> ofSize(int n) {
        checkArgument(n >= 0);
        while (n >= expressions.size()) {
            populateExpressions();
        }
        return expressions.get(n);
    }

    private void populateExpressions() {
        if (expressions.isEmpty()) {
            expressions.add(new Buffer(Collections.emptyIterator()));
        } else if (expressions.size() == 1) {
            populateExpressions1();
        } else {
            populateExpressions2();
        }
    }

    private void populateExpressions1() {
        List<Expression> allExprs = new ArrayList<>();

        allExprs.add(new Bottom());
        allExprs.add(new Epsilon(""));
        for (char constant : constants) {
            allExprs.add(new Epsilon(Character.toString(constant)));
        }
        for (CharPred charPred : charPreds) {
            for (CharOffset offset : offsets) {
                allExprs.add(new Symbol(charPred, offset));
            }
        }

        ExpressionComparator comparator = new ExpressionComparator();
        Collections.sort(allExprs, comparator);
        Iterator<Expression> ansIterator = new StatelessAssertionIterator(allExprs.iterator(),
                new Predicate<Expression>() {
                    @Override
                    public boolean apply(Expression t) {
                        return t.size == 1;
                    }
                });
        ansIterator = Iterators.filter(ansIterator, CONSISTENT_FILTER);
        ansIterator = indistinguishableExprFilter.filter(ansIterator);
        ansIterator = CollectionUtil.assertIncreasing(ansIterator, comparator);

        Buffer ansBuffer = new Buffer(ansIterator);
        expressions.add(ansBuffer);
    }

    private void populateExpressions2() {
        checkArgument(expressions.size() >= 2);
        final int newSize = expressions.size();

        ExpressionComparator comparator = new ExpressionComparator();
        Iterator<Expression> condIterator = buildConds(newSize);
        Iterator<Expression> concatIterator = buildSplits(newSize);
        Iterator<Expression> sumIterator = buildSums(newSize);
        Iterator<Expression> iterSumIterator = buildStars(newSize);
        Iterator<Expression> chainSumIterator = buildChain(newSize);
        Iterator<Expression> restrictIterator = buildRestrict(newSize);

        Iterator<Expression> ansIterator = CollectionUtil.mergeSorted(new ExpressionComparator(),
                condIterator, concatIterator, sumIterator, iterSumIterator,
                chainSumIterator, restrictIterator);
        ansIterator = new StatelessAssertionIterator(ansIterator, new Predicate<Expression>() {
            @Override
            public boolean apply(Expression expr) {
                return expr.size == newSize;
            }
        });
        ansIterator = Iterators.filter(ansIterator, CONSISTENT_FILTER);
        ansIterator = indistinguishableExprFilter.filter(ansIterator);
        ansIterator = CollectionUtil.assertIncreasing(ansIterator, comparator);

        Buffer ansBuffer = new Buffer(ansIterator);
        expressions.add(ansBuffer);
    }

    public static final Predicate<Expression> NOT_BOT = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return !(checkNotNull(t) instanceof Bottom);
        }
    };

    private Iterator<Expression> buildConds(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        if (expressions.size() == 2) {
            return Collections.emptyIterator();
        }

        Iterator<Expression> ans = Collections.emptyIterator();
        for (int leftSize = 1; leftSize < expressions.size() - 1; leftSize++) {
            int rightSize = newSize - leftSize - 1;
            checkAssertion(leftSize > 0 && rightSize > 0);
            Buffer<Expression> leftBuffer = expressions.get(leftSize);
            Buffer<Expression> rightBuffer = expressions.get(rightSize);
            Iterator<Expression> newExprs = Product.of(leftBuffer, rightBuffer, BUILD_COND);
            ans = CollectionUtil.mergeSorted(new ExpressionComparator(), ans, newExprs);
        }
        return ans;
    }

    public static final Predicate<Expression> NOT_COND = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return !(checkNotNull(t) instanceof IfElse);
        }
    };

    public static final BiFunction<Expression, Expression, Expression> BUILD_COND = new BiFunction<Expression, Expression, Expression>() {
        @Override
        public Expression apply(Expression left, Expression right) {
            return new IfElse(checkNotNull(left), checkNotNull(right));
        }
    };

    private Iterator<Expression> buildSplits(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        if (expressions.size() == 2) {
            return Collections.emptyIterator();
        }

        Iterator<Expression> ans = Collections.emptyIterator();
        for (int leftSize = 1; leftSize < expressions.size() - 1; leftSize++) {
            int rightSize = newSize - leftSize - 1;
            checkAssertion(leftSize > 0 && rightSize > 0);

            Buffer<Expression> leftBuffer = expressions.get(leftSize);
            Buffer<Expression> rightBuffer = expressions.get(rightSize);
            Iterator<Expression> newExprs = Product.of(leftBuffer, rightBuffer, BUILD_SPLITS);
            Iterator<Expression> newExprsLeft = Product.of(leftBuffer, rightBuffer, BUILD_LEFT_SPLITS);

            ans = CollectionUtil.mergeSorted(new ExpressionComparator(), ans, newExprs, newExprsLeft);
        }
        return ans;
    }

    public static final Predicate<Expression> NOT_SPLIT = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            if (checkNotNull(t) instanceof SplitSum) {
                return ((SplitSum)t).left;
            } else {
                return true;
            }
        }
    };

    public static final BiFunction<Expression, Expression, Expression> BUILD_SPLITS = new BiFunction<Expression, Expression, Expression>() {
        @Override
        public Expression apply(Expression left, Expression right) {
            return new SplitSum(checkNotNull(left), checkNotNull(right));
        }
    };

    public static final Predicate<Expression> NOT_LEFT_SPLIT = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            if (checkNotNull(t) instanceof SplitSum) {
                return !((SplitSum)t).left;
            } else {
                return true;
            }
        }
    };

    public static final BiFunction<Expression, Expression, Expression> BUILD_LEFT_SPLITS = new BiFunction<Expression, Expression, Expression>() {
        @Override
        public Expression apply(Expression left, Expression right) {
            return new SplitSum(checkNotNull(left), checkNotNull(right), true);
        }
    };

    private Iterator<Expression> buildSums(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        if (expressions.size() == 2) {
            return Collections.emptyIterator();
        }

        Iterator<Expression> ans = Collections.emptyIterator();
        for (int leftSize = 1; leftSize < expressions.size() - 1; leftSize++) {
            int rightSize = newSize - leftSize - 1;
            checkAssertion(leftSize > 0 && rightSize > 0);

            Buffer<Expression> leftBuffer = expressions.get(leftSize);
            Buffer<Expression> rightBuffer = expressions.get(rightSize);
            Iterator<Expression> newExprs = Product.of(leftBuffer, rightBuffer, BUILD_SUMS);

            ans = CollectionUtil.mergeSorted(new ExpressionComparator(), ans,
                    newExprs);
        }
        return ans;
    }

    public static final Predicate<Expression> NOT_SUM = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return !(checkNotNull(t) instanceof Sum);
        }
    };

    public static final BiFunction<Expression, Expression, Expression> BUILD_SUMS = new BiFunction<Expression, Expression, Expression>() {
        @Override
        public Expression apply(Expression left, Expression right) {
            return new Sum(checkNotNull(left), checkNotNull(right));
        }
    };

    private Iterator<Expression> buildStars(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        Buffer<Expression> inputBuffer = expressions.get(newSize - 1);

        Iterator<Expression> stars = Iterators.transform(inputBuffer.iterator(), BUILD_STAR);
        Iterator<Expression> leftStars = Iterators.transform(inputBuffer.iterator(), BUILD_LEFT_STAR);
        return CollectionUtil.mergeSorted(new ExpressionComparator(), stars, leftStars);
    }

    public static final Predicate<Expression> NOT_STAR = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return !(checkNotNull(t) instanceof IteratedSum);
        }
    };

    public static final Function<Expression, Expression> BUILD_STAR = new Function<Expression, Expression>() {
        @Override
        public Expression apply(Expression expr) {
            return new IteratedSum(checkNotNull(expr));
        }
    };

    public static final Function<Expression, Expression> BUILD_LEFT_STAR = new Function<Expression, Expression>() {
        @Override
        public Expression apply(Expression expr) {
            return new IteratedSum(checkNotNull(expr), true);
        }
    };

    private Iterator<Expression> buildChain(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        if (expressions.size() == 2) {
            return Collections.emptyIterator();
        }

        Iterator<Expression> ans = Collections.emptyIterator();
        for (int leftSize = 1; leftSize < expressions.size() - 1; leftSize++) {
            int rightSize = newSize - leftSize - 1;
            checkAssertion(leftSize > 0 && rightSize > 0);
            Buffer<Expression> exprBuffer = expressions.get(leftSize);
            Buffer<Regex> regexBuffer = regexPool.ofSize(rightSize);
            Iterator<Expression> newExprs = Product.of(exprBuffer, regexBuffer, BUILD_CHAIN);
            ans = CollectionUtil.mergeSorted(new ExpressionComparator(), ans, newExprs);
        }
        return ans;
    }

    public static final Predicate<Expression> NOT_CHAIN = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return !(checkNotNull(t) instanceof ChainedSum);
        }
    };

    public static final BiFunction<Expression, Regex, Expression> BUILD_CHAIN = new BiFunction<Expression, Regex, Expression>() {
        @Override
        public Expression apply(Expression expr, Regex regex) {
            return new ChainedSum(checkNotNull(expr), checkNotNull(regex));
        }
    };

    private Iterator<Expression> buildRestrict(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        if (expressions.size() == 2) {
            return Collections.emptyIterator();
        }

        Iterator<Expression> ans = Collections.emptyIterator();
        for (int leftSize = 1; leftSize < expressions.size() - 1; leftSize++) {
            int rightSize = newSize - leftSize - 1;
            checkAssertion(leftSize > 0 && rightSize > 0);

            Buffer<Expression> exprIterator = expressions.get(leftSize);
            Buffer<Regex> regexBuffer = regexPool.ofSize(rightSize);
            Iterator<Expression> newExprs = Product.of(exprIterator, regexBuffer, BUILD_RESTRICT);
            ans = CollectionUtil.mergeSorted(new ExpressionComparator(), ans, newExprs);
        }
        return ans;
    }

    public static final Predicate<Expression> NOT_RESTRICT = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return !(checkNotNull(t) instanceof Restrict);
        }
    };

    public static final BiFunction<Expression, Regex, Expression> BUILD_RESTRICT = new BiFunction<Expression, Regex, Expression>() {
        @Override
        public Expression apply(Expression expr, Regex regex) {
            return new Restrict(checkNotNull(expr), checkNotNull(regex));
        }
    };

    public final ImmutableSet<CharPred> charPreds;
    public final ImmutableSet<Character> constants;
    public final ImmutableSet<CharOffset> offsets;
    private final List<Buffer<Expression>> expressions;
    private final IndistinguishableExprFilter indistinguishableExprFilter;
    public final RegexPool regexPool;

    public static final Predicate<Expression> CONSISTENT_FILTER = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression input) {
            return checkNotNull(input).getDomainType() != null;
        }
    };

}
