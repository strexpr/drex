package edu.upenn.cis.drex.sa.enumerators.expr;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.expr.*;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.RegexUtil;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import java.util.Iterator;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import static edu.upenn.cis.drex.sa.enumerators.expr.BlindExprPool.CONSISTENT_FILTER;
import edu.upenn.cis.drex.sa.util.BudgetUtil;
import edu.upenn.cis.drex.sa.util.collections.CollectionUtil;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.tuple.ImmutablePair;
import theory.CharSolver;

public class ExtendExpressionUtil {

    public final MultiEnumeratorPoolGroup poolGroup;
    public final CharSolver charSolver;

    public ExtendExpressionUtil(MultiEnumeratorPoolGroup poolGroup)
    {
        this.poolGroup = poolGroup;
        this.charSolver = new CharSolver();
    }

    Iterator<Expression> match(Expression expression, final int budget, ImmutableList<IOExample> examples) {
        if (examples.isEmpty())
            return Iterators.singletonIterator(expression);

        IOExample firstExample = examples.get(0);
        final ImmutableList<IOExample> restExamples = examples.subList(1, examples.size());

        if (firstExample.matches(expression))
            return match(expression, budget, restExamples);

        return BudgetUtil.withBudget(budget,
                this.matchWithBudget(expression, firstExample),
                this.matchWithBudget(restExamples)
                );
    }

    private Iterator<Expression> match(Expression expression, int budget, IOExample example) {
        if (example.matches(expression)) {
            if (budget == 0)
                return Iterators.singletonIterator(expression);
            else
                return Collections.emptyIterator();
        }

        List<Iterator<Expression>> cases = new ArrayList<>();

        // Try modifying the sub-expressions
        switch (expression.type) {
            case SYMBOL:
                cases.add(this.matchSymbol((Symbol) expression, budget, example));
                break;
            case SPLIT:
            case LSPLIT:
                cases.add(this.matchSplit((SplitSum) expression, budget, example));
                break;
            case ITER:
            case LITER:
                cases.add(this.matchIter((IteratedSum) expression, budget, example));
                break;
            case SUM:
                cases.add(this.matchSum((Sum) expression, budget, example));
                break;
            case ELSE:
                cases.add(this.matchElse((IfElse) expression, budget, example));
                break;
            case RESTRICT:
                cases.add(this.matchRestrict((Restrict) expression, budget, example));
                break;
            case BOTTOM:
                cases.add(this.matchBottom((Bottom) expression, budget, example));
                break;
            case EPSILON:
                cases.add(this.matchEpsilon((Epsilon) expression, budget, example));
                break;
            case COMPOSE:
            case CHAINSUM:
            case LCHAINSUM:
            default:
                throw new UnsupportedOperationException("Not supported yet.");
        }

        // Try adding a top-level else
        Iterator<Expression> topLevelElses = this.poolGroup.enumerate(
            this.poolGroup.grammar.start,
            budget,
            ImmutableList.of(example));
        Iterator<Expression> topLevel = Iterators.transform(topLevelElses, IfElse.IfElse.partialApply(expression));
        cases.add(topLevel);
        Iterator ans = Iterators.filter(Iterators.concat(cases.iterator()), CONSISTENT_FILTER);

        return ans;
    }

    private Iterator<Expression> matchSymbol(Symbol symbol, int budget, IOExample example) {
        return Collections.emptyIterator();
    }

    private Iterator<Expression> matchSplit(final SplitSum splitSum, final int budget, IOExample example) {
        List<Supplier<Iterator<Expression>>> cases = new ArrayList<>();
        ImmutableList<ImmutablePair<IOExample, IOExample>> exampleSplits = !splitSum.left ?
                example.splits() : example.reverseSplits();
        for (final ImmutablePair<IOExample, IOExample> exampleSplit : exampleSplits) {
            cases.add(new Supplier<Iterator<Expression>>() {
                @Override
                public Iterator<Expression> get() {
                    Function<Integer, Iterator<Expression>> rightGenerator =
                        matchWithBudget(splitSum.e2, exampleSplit.right);
                    BiFunction<Integer, Expression, Iterator<Expression>> combiner = !splitSum.left ?
                        combineWith(rightGenerator, SplitSum.RSplit) :
                        combineWith(rightGenerator, SplitSum.LSplit);
                    return BudgetUtil.withBudget(budget,
                        matchWithBudget(splitSum.e1, exampleSplit.left),
                        combiner);
                }
            });
        }
        return CollectionUtil.lazyConcat(cases);
    }

    private Iterator<Expression> matchIter(final IteratedSum iteratedSum, final int budget, IOExample example) {
        if (example.input.length() == 0 && example.getOutput().length() != 0)
                return Collections.emptyIterator();

        List<Supplier<Iterator<Expression>>> cases = new ArrayList<>();
        ImmutableList<ImmutablePair<IOExample, IOExample>> exampleSplits = !iteratedSum.left ?
            example.leftNonEmptySplits() : example.leftNonEmptyReverseSplits();

        for (final ImmutablePair<IOExample, IOExample> exampleSplit : exampleSplits) {
            cases.add(new Supplier<Iterator<Expression>>() {
                @Override
                public Iterator<Expression> get() {
                    Function<Integer, Iterator<Expression>> leftMatches =
                        matchWithBudget(iteratedSum.e, exampleSplit.left);
                    BiFunction<Integer, Expression, Iterator<Expression>> curr =
                        combineIter(exampleSplit.right, iteratedSum.left);
                    return BudgetUtil.withBudget(budget, leftMatches, curr);
                }
            });
        }
        return CollectionUtil.lazyConcat(cases);
    }

    private Iterator<Expression> matchSum(final Sum sum, final int budget, IOExample example) {
        List<Supplier<Iterator<Expression>>> cases = new ArrayList<>();
        for (final ImmutablePair<IOExample, IOExample> exampleSplit : example.outputSplits()) {
            cases.add(new Supplier<Iterator<Expression>>() {
                @Override
                public Iterator<Expression> get() {
                    Function<Integer, Iterator<Expression>> rGenerator =
                        matchWithBudget(sum.e2, exampleSplit.right);
                    return BudgetUtil.withBudget(budget,
                        matchWithBudget(sum.e1, exampleSplit.left),
                        combineWith(rGenerator, Sum.Sum));
                }
            });
        }
        return CollectionUtil.lazyConcat(cases);
    }

    private Iterator<Expression> matchElse(IfElse ifElse, int budget, IOExample example) {
        final Expression e1 = ifElse.e1;
        final Expression e2 = ifElse.e2;
        Iterator<Expression> modifyFirst = Iterators.transform(
                this.match(e1, budget, example), IfElse.IfElse.partialApplyRight(e2));
        Iterator<Expression> modifySecond = Iterators.transform(
                this.match(e2, budget, example), IfElse.IfElse.partialApply(e1));
        return Iterators.concat(modifyFirst, modifySecond);
    }

    // The expression domain already contains this input,
    //    If expression is correct, on input extend regex
    //    Else return none
    // Otherwise, extend expression and then extend regex
    private Iterator<Expression> matchRestrict(Restrict restrict, int budget, IOExample example) {
        String input = example.input;
        boolean inRegex = restrict.r.contains(input, charSolver);
        boolean inExpression = restrict.e.getDomainType().contains(input, charSolver);
        if (inRegex && inExpression)
            Collections.emptyIterator();

        BiFunction<Integer, Expression, Iterator<Expression>> matchRegex =
            this.combineWith(this.matchWithBudgetRegex(restrict.r, example), Restrict.Restrict);
        if (inExpression && !example.matches(restrict.e))
            return Collections.emptyIterator();
        else if (inExpression && !inRegex) {
            return matchRegex.apply(budget, restrict.e);
        } else {
            return BudgetUtil.withBudget(budget,
                this.matchWithBudget(restrict.e, example),
                matchRegex);
        }
    }

    private Iterator<Expression> matchBottom(Bottom bottom, int budget, IOExample example) {
        return this.poolGroup.enumerate(
            this.poolGroup.grammar.start,
            budget,
            ImmutableList.of(example));
    }

    private Iterator<Expression> matchEpsilon(Epsilon epsilon, int budget, IOExample example) {
        return Collections.emptyIterator();
    }

    private Iterator<Regex> matchRegex(Regex regex, Integer budget, IOExample example) {
        Preconditions.checkNotNull(example);
        // System.out.println(regex + " " + example);
        // checkAssertion (!example.outputKnown() || example.getOutput().isEmpty());

        IOExample normalizedExample = example.defined ?
                IOExample.io(example.input, "") : IOExample.undefinedOn(example.input);
        Expression regexExpr = RegexUtil.regexToExpression(regex);
        Iterator<Expression> modRegexExprs = this.match(regexExpr, budget, example);
        return Iterators.transform(modRegexExprs, new Function<Expression, Regex>() {
            @Override
            public Regex apply(Expression f) {
                return ExpressionUtil.expressionToRegex(f);
            }
        });
    }

    Function<Integer, Iterator<Regex>> matchWithBudgetRegex(
            final Regex regex,
            final IOExample example) {
        return new Function<Integer, Iterator<Regex>>() {
            @Override
            public Iterator<Regex> apply(Integer budget) {
                return matchRegex(regex, budget, example);
            }
        };
    }

    Function<Integer, Iterator<Expression>> matchWithBudget(
            final Expression expression, final IOExample example) {
        return new Function<Integer, Iterator<Expression>>() {
            @Override
            public Iterator<Expression> apply(Integer budget) {
                return match(expression, budget, example);
            }
        };
    }

    BiFunction<Integer, Expression, Iterator<Expression>> matchWithBudget(
            final ImmutableList<IOExample> examples) {
        return new BiFunction<Integer, Expression, Iterator<Expression>>() {
            @Override
            public Iterator<Expression> apply(Integer budget, Expression expression) {
                return match(expression, budget, examples);
            }
        };
    }

    <U> BiFunction<Integer, Expression, Iterator<Expression>> combineWith(
        final Function<Integer, Iterator<U>> generator,
        final BiFunction<Expression, U, Expression> combiner
    ) {
        return new BiFunction<Integer, Expression, Iterator<Expression>>() {
            @Override
            public Iterator<Expression> apply(Integer t, final Expression e) {
                Iterator<U> us = generator.apply(t);
                return Iterators.transform(us, combiner.partialApply(e));
            }
        };
    }

    private BiFunction<Integer, Expression, Iterator<Expression>> combineIter(
            final IOExample io,
            final boolean left) {
        return new BiFunction<Integer, Expression, Iterator<Expression>>() {
            @Override
            public Iterator<Expression> apply(Integer remainingBudget, Expression leftSubExpr) {
                return match(new IteratedSum(leftSubExpr, left), remainingBudget, io);
            }
        };
    }

}
