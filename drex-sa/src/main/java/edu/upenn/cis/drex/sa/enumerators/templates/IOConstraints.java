package edu.upenn.cis.drex.sa.enumerators.templates;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class IOConstraints {

    public IOConstraints(ImmutableMap<Hole, ImmutableSet<IOExample>> constraints) {
        checkNotNull(constraints);
        this.constraints = constraints;
    }

    public ImmutableSet<IOExample> getHoleConstraints(Hole h) {
        checkNotNull(h);
        return constraints.getOrDefault(h, ImmutableSet.<IOExample>of());
    }

    @Override
    public String toString() {
        StringBuilder ans = new StringBuilder();
        for (Hole h : constraints.keySet()) {
            for (IOExample ioExample : constraints.get(h)) {
                ans.append(String.format("%s -> %s\n", h, ioExample));
            }
        }
        if (constraints.keySet().isEmpty())
            ans.append("True\n");
        return ans.toString();
    }

    public static final ImmutableSet<IOConstraints> TRUE = ImmutableSet.of(new IOConstraints(ImmutableMap.<Hole, ImmutableSet<IOExample>>of()));
    public static final ImmutableSet<IOConstraints> FALSE = ImmutableSet.of();
 
    private final ImmutableMap<Hole, ImmutableSet<IOExample>> constraints;

    public static IOConstraints combine(IOConstraints c1, IOConstraints c2) {
        Map<Hole, ImmutableSet<IOExample>> constraints = new HashMap<>();
        Set<Hole> allHoles = Sets.union(c1.constraints.keySet(), c2.constraints.keySet());
        for (Hole h : allHoles) {
            ImmutableSet<IOExample> hConstraints = ImmutableSet.copyOf(Sets.union(c1.constraints.getOrDefault(h, ImmutableSet.<IOExample>of()),
                        c2.constraints.getOrDefault(h, ImmutableSet.<IOExample>of())
                    ));
            constraints.put(h, hConstraints);
        }
        return new IOConstraints(ImmutableMap.copyOf(constraints));
    }

}
