package edu.upenn.cis.drex.sa.enumerators.regex;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Empty;
import edu.upenn.cis.drex.core.regex.Epsilon;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import edu.upenn.cis.drex.core.regex.Union;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.sa.concepttrees.regex.InequivalentRegexFilter;
import edu.upenn.cis.drex.sa.util.comparators.RegexComparator;
import edu.upenn.cis.drex.sa.util.collections.StatelessAssertionIterator;
import edu.upenn.cis.drex.sa.util.collections.Product;
import edu.upenn.cis.drex.sa.util.collections.Buffer;
import edu.upenn.cis.drex.sa.util.collections.CollectionUtil;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import theory.CharPred;

public class RegexPool {

    public RegexPool(ImmutableSet<CharPred> charPreds) {
        for (CharPred pred : checkNotNull(charPreds)) {
            checkNotNull(pred);
        }
        this.charPreds = charPreds;
        this.expressions = new ArrayList<>();
        this.inequivalentRegexFilter = new InequivalentRegexFilter();
    }

    public Buffer<Regex> ofSize(int n) {
        checkArgument(n >= 0);
        while (n >= expressions.size()) {
            populateExpressions();
        }
        return expressions.get(n);
    }

    private void populateExpressions() {
        if (expressions.isEmpty()) {
            expressions.add(new Buffer(Collections.emptyIterator()));
        } else if (expressions.size() == 1) {
            populateExpressions1();
        } else {
            populateExpressions2();
        }
    }

    private void populateExpressions1() {
        List<Regex> allSymbols = new ArrayList<>();
        allSymbols.add(new Empty());
        allSymbols.add(new Epsilon());

        for (CharPred c : charPreds) {
            allSymbols.add(new Symbol(c));
        }

        RegexComparator comparator = new RegexComparator();
        Collections.sort(allSymbols, comparator);
        Iterator<Regex> ansIterator = new StatelessAssertionIterator(allSymbols.iterator(),
                new Predicate<Regex>() {
                    @Override
                    public boolean apply(Regex t) {
                        return t.size == 1;
                    }
                });
        ansIterator = Iterators.filter(ansIterator, UNAMBIGUOUS_FILTER);
        ansIterator = inequivalentRegexFilter.filter(ansIterator);
        ansIterator = CollectionUtil.assertIncreasing(ansIterator, comparator);

        Buffer ansBuffer = new Buffer(ansIterator);
        expressions.add(ansBuffer);
    }

    private void populateExpressions2() {
        checkArgument(expressions.size() >= 2);
        final int newSize = expressions.size();

        Iterator<Regex> starIterator = buildStars(newSize);
        Iterator<Regex> concatIterator = buildConcats(newSize);
        Iterator<Regex> unionIterator = buildUnions(newSize);

        RegexComparator comparator = new RegexComparator();
        Iterator<Regex> ansIterator = CollectionUtil.mergeSorted(comparator,
                starIterator, concatIterator, unionIterator);
        ansIterator = new StatelessAssertionIterator(ansIterator, new Predicate<Regex>() {
            @Override
            public boolean apply(Regex t) {
                return t.size == newSize;
            }
        });
        ansIterator = Iterators.filter(ansIterator, UNAMBIGUOUS_FILTER);
        ansIterator = inequivalentRegexFilter.filter(ansIterator);
        ansIterator = CollectionUtil.assertIncreasing(ansIterator, comparator);

        Buffer ansBuffer = new Buffer(ansIterator);
        expressions.add(ansBuffer);
    }

    private Iterator<Regex> buildStars(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        Iterator<Regex> inputIterator = expressions.get(newSize - 1).iterator();
        return Iterators.transform(inputIterator, BUILD_STAR);
    }

    private Iterator<Regex> buildConcats(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        if (expressions.size() == 2) {
            return Collections.emptyIterator();
        }

        Iterator<Regex> ans = Collections.emptyIterator();
        for (int leftSize = 1; leftSize < expressions.size() - 1; leftSize++) {
            int rightSize = newSize - leftSize - 1;
            checkAssertion(leftSize > 0 && rightSize > 0);
            Buffer<Regex> leftBuffer = expressions.get(leftSize);
            Buffer<Regex> rightBuffer = expressions.get(rightSize);
            Iterator<Regex> newRegexes = Product.of(leftBuffer, rightBuffer, BUILD_CONCAT);
            ans = CollectionUtil.mergeSorted(new RegexComparator(), ans, newRegexes);
        }
        return ans;
    }

    private Iterator<Regex> buildUnions(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        if (expressions.size() == 2) {
            return Collections.emptyIterator();
        }

        Iterator<Regex> ans = Collections.emptyIterator();
        for (int leftSize = 1; leftSize < expressions.size() - 1; leftSize++) {
            int rightSize = newSize - leftSize - 1;
            checkAssertion(leftSize > 0 && rightSize > 0);
            Buffer<Regex> leftBuffer = expressions.get(leftSize);
            Buffer<Regex> rightBuffer = expressions.get(rightSize);
            Iterator<Regex> newRegexes = Product.of(leftBuffer, rightBuffer, BUILD_UNION);
            ans = CollectionUtil.mergeSorted(new RegexComparator(), ans, newRegexes);
        }
        return ans;
    }

    public final ImmutableSet<CharPred> charPreds;
    private final List<Buffer<Regex>> expressions;
    private final InequivalentRegexFilter inequivalentRegexFilter;

    public static final Predicate<Regex> UNAMBIGUOUS_FILTER = new Predicate<Regex>() {
        @Override
        public boolean apply(Regex input) {
            return checkNotNull(input).getAmbiguity() == null;
        }
    };

    public static final Predicate<Regex> NOT_STAR = new Predicate<Regex>() {
        @Override
        public boolean apply(Regex t) {
            return !(checkNotNull(t) instanceof Star);
        }
    };

    public static final Function<Regex, Regex> BUILD_STAR = new Function<Regex, Regex>() {
        @Override
        public Regex apply(Regex regex) {
            return new Star(checkNotNull(regex));
        }
    };

    public static final Predicate<Regex> NOT_CONCAT = new Predicate<Regex>() {
        @Override
        public boolean apply(Regex t) {
            return !(checkNotNull(t) instanceof Concat);
        }
    };

    public static final BiFunction<Regex, Regex, Regex> BUILD_CONCAT = new BiFunction<Regex, Regex, Regex>() {
        @Override
        public Regex apply(Regex left, Regex right) {
            return new Concat(checkNotNull(left), checkNotNull(right));
        }
    };

    public static final Predicate<Regex> NOT_UNION = new Predicate<Regex>() {
        @Override
        public boolean apply(Regex t) {
            return !(checkNotNull(t) instanceof Union);
        }
    };

    public static final BiFunction<Regex, Regex, Regex> BUILD_UNION = new BiFunction<Regex, Regex, Regex>() {
        @Override
        public Regex apply(Regex left, Regex right) {
            return new Union(checkNotNull(left), checkNotNull(right));
        }
    };

}
