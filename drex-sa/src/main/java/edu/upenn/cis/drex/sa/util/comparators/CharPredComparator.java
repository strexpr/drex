package edu.upenn.cis.drex.sa.util.comparators;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Comparator;
import theory.CharPred;
import theory.CharSolver;

public class CharPredComparator implements Comparator<CharPred> {

    @Override
    public int compare(CharPred pred1, CharPred pred2) {
        CharSolver charSolver = new CharSolver();
        CharPred notPred1 = charSolver.MkNot(checkNotNull(pred1));
        CharPred notPred2 = charSolver.MkNot(checkNotNull(pred2));
        CharPred p1NotP2 = charSolver.MkAnd(pred1, notPred2);
        CharPred p2NotP1 = charSolver.MkAnd(pred2, notPred1);
        char w1 = charSolver.IsSatisfiable(p1NotP2) ?
                charSolver.generateWitness(p1NotP2) : CharPred.MAX_CHAR;
        char w2 = charSolver.IsSatisfiable(p2NotP1) ?
                charSolver.generateWitness(p2NotP1) : CharPred.MAX_CHAR;
        return w1 - w2;
    }

}
