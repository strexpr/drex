package edu.upenn.cis.drex.sa.enumerators.regex;

import edu.upenn.cis.drex.sa.enumerators.base.CharPreds;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.regex.Regex;
import java.util.Iterator;
import java.util.NoSuchElementException;
import theory.CharPred;

public class RegexEnumerator implements Iterator<Regex> {

    public RegexEnumerator(RegexPool pool) {
        this.pool = checkNotNull(pool);
        this.size = 0;
        this.sizeSpecificIterator = pool.ofSize(size).iterator();
        this.emptyRun = 0;
    }

    public RegexEnumerator(ImmutableSet<CharPred> charPreds) {
        this(new RegexPool(checkNotNull(charPreds)));
    }

    @Override
    public boolean hasNext() {
        while (!sizeSpecificIterator.hasNext()) {
            size++;
            sizeSpecificIterator = pool.ofSize(size).iterator();
            emptyRun++;

            if (size > 2 && size < 2 * emptyRun) {
                return false;
            }
        }

        emptyRun = 0;
        return true;
    }

    @Override
    public Regex next() {
        if (hasNext()) {
            return sizeSpecificIterator.next();
        } else {
            throw new NoSuchElementException();
        }
    }

    public final RegexPool pool;
    private /* mutable */ int size;
    private /* mutable */ Iterator<Regex> sizeSpecificIterator;
    private /* mutable */ int emptyRun;

    public static Iterator<Regex> ofExamples(ImmutableSet<String> positive,
            ImmutableSet<String> negative, CharPreds.BasePreds basePreds) {
        ImmutableSet.Builder<String> baseStrings = ImmutableSet.builder();
        baseStrings.addAll(checkNotNull(positive));
        baseStrings.addAll(checkNotNull(negative));
        ImmutableSet<CharPred> preds =
                CharPreds.makeBaseCharPredsFromStrings(baseStrings.build(),
                        basePreds);

        Iterator<Regex> enumerator = new RegexEnumerator(preds);
        enumerator = RegexEnumeratorUtil.whichMatch(enumerator, positive);
        enumerator = RegexEnumeratorUtil.whichDontMatch(enumerator, negative);
        return enumerator;
    }

}
