package edu.upenn.cis.drex.sa.enumerators.regex;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.Iterator;
import theory.CharSolver;

public class RegexEnumeratorUtil {

    public static Iterator<Regex> whichMatch(Iterator<Regex> lazyList, ImmutableSet<String> strSet) {
        checkNotNull(lazyList);
        for (String str : checkNotNull(strSet)) {
            lazyList = whichMatch(lazyList, str);
        }
        return lazyList;
    }

    public static Iterator<Regex> whichMatch(Iterator<Regex> lazyList, final String str) {
        checkNotNull(str);
        final CharSolver solver = new CharSolver();

        return Iterators.filter(checkNotNull(lazyList), new Predicate<Regex>() {
            @Override
            public boolean apply(Regex input) {
                return checkNotNull(input).contains(str, solver);
            }
        });
    }

    public static Iterator<Regex> whichDontMatch(Iterator<Regex> lazyList, ImmutableSet<String> strSet) {
        checkNotNull(lazyList);
        for (String str : checkNotNull(strSet)) {
            lazyList = whichDontMatch(lazyList, str);
        }
        return lazyList;
    }

    public static Iterator<Regex> whichDontMatch(Iterator<Regex> lazyList, final String str) {
        checkNotNull(str);
        final CharSolver solver = new CharSolver();

        return Iterators.filter(checkNotNull(lazyList), new Predicate() {
            @Override
            public boolean apply(Object input) {
                checkAssertion(input instanceof Regex);
                return !((Regex)input).contains(str, solver);
            }
        });
    }

}
