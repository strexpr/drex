package edu.upenn.cis.drex.sa.enumerators.expr;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import com.google.common.collect.Sets;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.Expression;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.sa.enumerators.base.AbstractPool;
import edu.upenn.cis.drex.sa.enumerators.base.AbstractPoolGroup;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.NonTerminal;
import edu.upenn.cis.drex.sa.enumerators.templates.std.PosGrammar;
import edu.upenn.cis.drex.sa.util.StringUtil;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class PosExprPoolGroup extends AbstractPoolGroup {

    public PosExprPoolGroup(ImmutableCollection<IOExample> examples,
            PosGrammar grammar) {
        super(checkNotNull(grammar), checkNotNull(examples));

        ImmutableSet.Builder<String> positiveInputs = ImmutableSet.builder();
        for (IOExample ex : examples) {
            positiveInputs.add(ex.input);
        }
        this.discriminants = StringUtil.getSubstrings(positiveInputs.build());

        Map<IOExample, AbstractPool> tempPools = new HashMap<>();
        for (IOExample example : examples) {
            if (example.outputKnown()) {
                for (String input : StringUtil.getSubstrings(example.input)) {
                    for (String output : StringUtil.getSubstrings(example.getOutput())) {
                        IOExample ioExample = IOExample.io(input, output);
                        if (!tempPools.containsKey(ioExample)) {
                            PosExprPool pool = new PosExprPool(this, ioExample);
                            tempPools.put(ioExample, pool);
                        }
                    }
                }
            }
        }
        this.pools = ImmutableMap.copyOf(tempPools);
    }

    public final ImmutableSet<String> discriminants;
    public final ImmutableMap<IOExample, AbstractPool> pools;

    @Override
    public AbstractPoolGroup addExample(IOExample ex) {
        checkNotNull(ex);
        checkArgument(!examples.contains(ex));

        Set<IOExample> exPrime = Sets.newHashSet();
        exPrime.addAll(examples);
        exPrime.add(ex);
        PosGrammar pg = PosGrammar.build(grammar.basePredStartegy, exPrime, grammar.baseExprs);
        return new PosExprPoolGroup(ImmutableSet.copyOf(exPrime), pg);
    }

    @Override
    public Iterator<Expression> fromAllPools(final NonTerminal holeType, final int size) {
        if (pools.isEmpty()) {
            System.out.printf("No positive examples to generate from!\n");
            return Iterators.<Expression>singletonIterator(new Bottom());
        }
        Iterator<Expression> ans = Iterators.concat(Iterators.transform(
            pools.values().iterator(), AbstractPool.OF_SIZE(size, holeType)));
        return ans;
    }


    public Iterator<Expression> enumerateOnNegativeExamples(NonTerminal holeType,
            int size,
            final ImmutableCollection<IOExample> cons) {
        if (pools.isEmpty()) {
            System.out.printf("No positive examples to generate from!\n");
            return Iterators.<Expression>singletonIterator(new Bottom());
        }

        for (IOExample ex : cons)
            checkAssertion(!ex.outputKnown() && !ex.defined);

        Iterator<AbstractPool> goodPools = Iterators.filter(pools.values().iterator(),
            new Predicate<AbstractPool>() {
                @Override
                public boolean apply(AbstractPool t) {
                    return !cons.contains(IOExample.undefinedOn(t.example.input));
                }
            }
        );
        Iterator<Expression> ans = Iterators.concat(
                Iterators.transform(goodPools, AbstractPool.OF_SIZE(size, holeType)));
        return ans;
    }

    @Override
    public Iterator<Expression> enumerate(NonTerminal holeType, int size, ImmutableCollection<IOExample> localExamples) {
        checkNotNull(holeType);
        checkNotNull(localExamples);

        IOExample possibleGoodExample = null;
        for (IOExample ioExample : localExamples) {
            if (ioExample.outputKnown() && pools.containsKey(ioExample)) {
                if (possibleGoodExample == null || possibleGoodExample.input.length() < ioExample.input.length())
                    possibleGoodExample = ioExample;
            }
        }
        if (possibleGoodExample == null) {
            return ExprEnumeratorUtil.whichMatch(
                    enumerateOnNegativeExamples(holeType, size, localExamples), 
                    localExamples);
        }
        final IOExample goodExample = possibleGoodExample;

        Iterator<Expression> ans = pools.get(goodExample).ofSize(size, holeType).iterator();
        ans = ExprEnumeratorUtil.whichMatch(ans, localExamples);
        return ans;
    }

}
