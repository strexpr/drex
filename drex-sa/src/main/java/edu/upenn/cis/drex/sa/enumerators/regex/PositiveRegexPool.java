package edu.upenn.cis.drex.sa.enumerators.regex;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.regex.Empty;
import edu.upenn.cis.drex.core.regex.Epsilon;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Symbol;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.sa.concepttrees.regex.InequivalentRegexFilter;
import static edu.upenn.cis.drex.sa.enumerators.regex.RegexPool.BUILD_CONCAT;
import static edu.upenn.cis.drex.sa.enumerators.regex.RegexPool.BUILD_STAR;
import static edu.upenn.cis.drex.sa.enumerators.regex.RegexPool.BUILD_UNION;
import static edu.upenn.cis.drex.sa.enumerators.regex.RegexPool.NOT_CONCAT;
import static edu.upenn.cis.drex.sa.enumerators.regex.RegexPool.NOT_STAR;
import static edu.upenn.cis.drex.sa.enumerators.regex.RegexPool.NOT_UNION;
import static edu.upenn.cis.drex.sa.enumerators.regex.RegexPool.UNAMBIGUOUS_FILTER;
import edu.upenn.cis.drex.sa.util.comparators.RegexComparator;
import edu.upenn.cis.drex.sa.util.collections.Buffer;
import edu.upenn.cis.drex.sa.util.collections.Product;
import edu.upenn.cis.drex.sa.util.collections.CollectionUtil;
import edu.upenn.cis.drex.sa.util.collections.StatelessAssertionIterator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import theory.CharPred;

public class PositiveRegexPool {

    public PositiveRegexPool(String string, PositiveRegexPoolGroup poolGroup) {
        this.string = checkNotNull(string);
        this.poolGroup = checkNotNull(poolGroup);
        this.expressions = new ArrayList<>();
        this.inequivalentRegexFilter = new InequivalentRegexFilter();
    }

    public Buffer<Regex> ofSize(int n) {
        checkArgument(n >= 0);
        while (n >= expressions.size()) {
            populateExpressions();
        }
        return expressions.get(n);
    }

    private void populateExpressions() {
        if (expressions.isEmpty()) {
            expressions.add(new Buffer(Collections.emptyIterator()));
        } else if (expressions.size() == 1) {
            populateExpressions1();
        } else {
            populateExpressions2();
        }
    }

    private void populateExpressions1() {
        List<Regex> allSymbols = new ArrayList<>();
        allSymbols.add(new Empty());
        allSymbols.add(new Epsilon());

        for (CharPred c : poolGroup.charPreds) {
            allSymbols.add(new Symbol(c));
        }
        allSymbols.addAll(poolGroup.baseRegexes);

        if (poolGroup.sorted) {
            RegexComparator comparator = new RegexComparator();
            Collections.sort(allSymbols, comparator);
        }

        Iterator<Regex> ansIterator = allSymbols.iterator();
        if (poolGroup.baseRegexes.isEmpty()) {
            ansIterator = new StatelessAssertionIterator(ansIterator,
                    new Predicate<Regex>() {
                        @Override
                        public boolean apply(Regex t) {
                            return t.size == 1;
                        }
                    });
        }
        ansIterator = Iterators.filter(ansIterator, UNAMBIGUOUS_FILTER);
        ansIterator = RegexEnumeratorUtil.whichMatch(ansIterator, string);
        ansIterator = inequivalentRegexFilter.filter(ansIterator);

        if (poolGroup.sorted) {
            RegexComparator comparator = new RegexComparator();
            ansIterator = CollectionUtil.assertIncreasing(ansIterator, comparator);
        }

        Buffer ansBuffer = new Buffer(ansIterator);
        expressions.add(ansBuffer);
    }

    private void populateExpressions2() {
        checkArgument(expressions.size() >= 2);
        final int newSize = expressions.size();

        Supplier<Iterator<Regex>> starSupplier = lazyBuildStars(newSize);
        Supplier<Iterator<Regex>> concatSupplier = lazyBuildConcats(newSize);
        Supplier<Iterator<Regex>> unionSupplier = lazyBuildUnions(newSize);

        Iterator<Regex> ansIterator = CollectionUtil.lazyConcat(starSupplier,
                concatSupplier, unionSupplier);
        if (poolGroup.baseRegexes.isEmpty()) {
            ansIterator = new StatelessAssertionIterator(ansIterator, new Predicate<Regex>() {
                @Override
                public boolean apply(Regex t) {
                    return t.size == newSize;
                }
            });
        }
        ansIterator = Iterators.filter(ansIterator, UNAMBIGUOUS_FILTER);
        ansIterator = inequivalentRegexFilter.filter(ansIterator);
        if (poolGroup.sorted) {
            RegexComparator comparator = new RegexComparator();
            ansIterator = CollectionUtil.assertIncreasing(ansIterator, comparator);
        }

        Buffer ansBuffer = new Buffer(ansIterator);
        expressions.add(ansBuffer);
    }

    private Supplier<Iterator<Regex>> lazyBuildStars(final int newSize) {
        return new Supplier<Iterator<Regex>>() {
            @Override
            public Iterator<Regex> get() {
                return buildStars(newSize);
            }
        };
    }

    private Iterator<Regex> buildStars(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());

        Iterator<Regex> ans = Collections.emptyIterator();
        for (int i = 0; i <= string.length(); i++) {
            String left = string.substring(0, i);
            String right = string.substring(i);
            checkAssertion(string.equals(left + right));

            PositiveRegexPool pool = poolGroup.getPool(left);
            Iterator<Regex> newRegexes = pool.ofSize(newSize - 1).iterator();
            newRegexes = Iterators.filter(newRegexes, NOT_STAR);
            newRegexes = Iterators.transform(newRegexes, BUILD_STAR);
            newRegexes = Iterators.filter(newRegexes, UNAMBIGUOUS_FILTER);
            newRegexes = RegexEnumeratorUtil.whichMatch(newRegexes, right);

            if (poolGroup.sorted) {
                ans = CollectionUtil.mergeSorted(new RegexComparator(), ans, newRegexes);
            } else {
                ans = Iterators.concat(ans, newRegexes);
            }
        }

        return ans;
    }

    private Supplier<Iterator<Regex>> lazyBuildConcats(final int newSize) {
        return new Supplier<Iterator<Regex>>() {
            @Override
            public Iterator<Regex> get() {
                return buildConcats(newSize);
            }
        };
    }

    private Iterator<Regex> buildConcats(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        if (newSize == 2) {
            return Collections.emptyIterator();
        }

        Iterator<Regex> ans = Collections.emptyIterator();
        for (int leftSize = 1; leftSize < newSize - 1; leftSize++) {
            int rightSize = newSize - leftSize - 1;
            checkAssertion(leftSize > 0 && rightSize > 0);

            Iterator<Regex> newRegexes = Collections.emptyIterator();

            for (int i = 0; i <= string.length(); i++) {
                String left = string.substring(0, i);
                String right = string.substring(i);

                Iterator<Regex> leftIterator = poolGroup.getPool(left).ofSize(leftSize).iterator();
                leftIterator = Iterators.filter(leftIterator, NOT_CONCAT);

                Buffer<Regex> rightBuffer = poolGroup.getPool(right).ofSize(rightSize);

                Iterator<Regex> splits = new Product(leftIterator, rightBuffer, BUILD_CONCAT);
                if (poolGroup.sorted) {
                    RegexComparator comparator = new RegexComparator();
                    newRegexes = CollectionUtil.mergeSorted(comparator, newRegexes, splits);
                } else {
                    newRegexes = Iterators.concat(newRegexes, splits);
                }
            }

            ans = Iterators.concat(ans, newRegexes);
        }

        return ans;
    }

    private Supplier<Iterator<Regex>> lazyBuildUnions(final int newSize) {
        return new Supplier<Iterator<Regex>>() {
            @Override
            public Iterator<Regex> get() {
                return buildUnions(newSize);
            }
        };
    }

    private Iterator<Regex> buildUnions(int newSize) {
        checkArgument(newSize >= 2);
        checkState(newSize <= expressions.size());
        if (newSize == 2) {
            return Collections.emptyIterator();
        }

        Iterator<Regex> ans = buildUnionsNonlocal(newSize);
        return ans;
    }

    private Iterator<Regex> buildUnionsNonlocal(int newSize) {
        checkArgument(newSize >= 3);
        checkState(newSize <= expressions.size());

        Iterator<Regex> ans = Collections.emptyIterator();
        for (int leftSize = 1; leftSize < newSize - 1; leftSize++) {
            int rightSize = newSize - leftSize - 1;
            checkAssertion(leftSize > 0 && rightSize > 0);

            if (rightSize < leftSize) {
                continue;
            }

            Iterator<Regex> newRegexes = Collections.emptyIterator();

            for (String stringPrime : poolGroup.posSubstrSet) {
                if (string.equals(stringPrime)) {
                    continue;
                }
                // TODO. Match all strings to check viability.

                Iterator<Regex> leftIterator = expressions.get(leftSize).iterator();
                leftIterator = Iterators.filter(leftIterator, NOT_UNION);

                PositiveRegexPool rightPool = poolGroup.getPool(stringPrime);
                Buffer<Regex> rightBuffer = rightPool.ofSize(rightSize);

                Iterator<Regex> unions = new Product(leftIterator, rightBuffer, BUILD_UNION);
                if (poolGroup.sorted) {
                    RegexComparator comparator = new RegexComparator();
                    newRegexes = CollectionUtil.mergeSorted(comparator, newRegexes, unions);
                } else {
                    newRegexes = Iterators.concat(newRegexes, unions);
                }
            }

            for (String stringPrime : poolGroup.posSubstrSet) {
                if (string.equals(stringPrime)) {
                    continue;
                }
                // TODO. Match all strings to check viability.

                Iterator<Regex> leftIterator = poolGroup.getPool(stringPrime).ofSize(leftSize).iterator();
                leftIterator = Iterators.filter(leftIterator, NOT_UNION);

                Buffer<Regex> rightBuffer = expressions.get(rightSize);

                Iterator<Regex> unions = new Product(leftIterator, rightBuffer, BUILD_UNION);
                if (poolGroup.sorted) {
                    RegexComparator comparator = new RegexComparator();
                    newRegexes = CollectionUtil.mergeSorted(comparator, newRegexes, unions);
                } else {
                    newRegexes = Iterators.concat(newRegexes, unions);
                }
            }

            ans = Iterators.concat(ans, newRegexes);
        }

        return ans;
    }

    public final String string;
    public final PositiveRegexPoolGroup poolGroup;
    private final List<Buffer<Regex>> expressions;
    private final InequivalentRegexFilter inequivalentRegexFilter;

}
