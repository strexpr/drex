package edu.upenn.cis.drex.sa.util.collections;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Comparator;
import java.util.Iterator;

public class DistinctIterator<T> implements Iterator<T> {

    public DistinctIterator(Iterator<T> iterator, Comparator<T> comparator) {
        this.iterator = checkNotNull(iterator);
        this.comparator = checkNotNull(comparator);
        this.prev = Optional.absent();
        advance();
    }

    private void advance() {
        while (iterator.hasNext()) {
            T potentialNext = iterator.next();

            if (!prev.isPresent()) {
                prev = Optional.of(potentialNext);
                return;
            }

            if (comparator.compare(prev.get(), potentialNext) < 0) {
                prev = Optional.of(potentialNext);
                return;
            }
        }
        prev = Optional.absent();
    }

    @Override
    public boolean hasNext() {
        return prev.isPresent();
    }

    @Override
    public T next() {
        T ans = prev.get();
        advance();
        return ans;
    }

    public final Iterator<T> iterator;
    public final Comparator<T> comparator;
    private /* mutable */ Optional<T> prev;

}
