package edu.upenn.cis.drex.sa.util.collections;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;
import java.util.Iterator;

public class StatefulFilter<T> implements Iterator<T> {

    public StatefulFilter(Iterator<T> iterator, State<T> state) {
        checkNotNull(iterator);
        this.iterator = Iterators.peekingIterator(iterator);
        this.advanced = false;
        this.state = checkNotNull(state);
    }

    @Override
    public boolean hasNext() {
        tryAdvance();
        return iterator.hasNext();
    }

    @Override
    public T next() {
        tryAdvance();
        T ans = iterator.next();
        advanced = false;
        return ans;
    }

    private void tryAdvance() {
        if (!advanced) {
            advance();
        }
        advanced = true;
    }

    private void advance() {
        while (iterator.hasNext() && !state.update(iterator.peek())) {
            iterator.next();
        }
    }

    private final PeekingIterator<T> iterator;
    private /* mutable */ boolean advanced;
    private final State<T> state;

    public interface State<T> {

        /**
         * Attempts to transition with t as the next element. Leaves the state
         * unchanged if the transition failed.
         * @param t
         * @return true if the transition succeeded, and false otherwise.
         */
        public boolean update(T t);

    }

}
