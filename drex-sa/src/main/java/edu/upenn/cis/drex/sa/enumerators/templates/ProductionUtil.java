package edu.upenn.cis.drex.sa.enumerators.templates;

import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;

public class ProductionUtil {
    
    public static ImmutableCollection<IOConstraints> combineIOConstraints(
            ImmutableCollection<IOConstraints> a,
            ImmutableCollection<IOConstraints> b)
    {
        ImmutableCollection.Builder<IOConstraints> ret = ImmutableList.builder();
        for (IOConstraints aH : a) {
            for (IOConstraints bH : b) {
                ret.add(IOConstraints.combine(aH, bH));
            }
        }
        return ret.build();
    }

}