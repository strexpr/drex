package edu.upenn.cis.drex.sa.enumerators.expr;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import edu.upenn.cis.drex.core.expr.Expression;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.sa.concepttrees.expr.IndistinguishableExprFilter;
import edu.upenn.cis.drex.sa.enumerators.base.AbstractPool;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.NonTerminal;
import edu.upenn.cis.drex.sa.util.collections.Buffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PosExprPool extends AbstractPool {

    public PosExprPool(PosExprPoolGroup poolGroup, IOExample example) {
        super(checkNotNull(poolGroup), checkNotNull(example));
        checkAssertion(example.outputKnown());
        
        this.memo = HashBasedTable.create();
        this.indistinguishableExprFilter = new HashMap<>();
        this.posPoolGroup = poolGroup;
    }

    private IndistinguishableExprFilter indistinguishabilityFilterFor(NonTerminal nt)
    {
        if (!this.indistinguishableExprFilter.containsKey(nt))
            this.indistinguishableExprFilter.put(
                nt,
                new IndistinguishableExprFilter(posPoolGroup.discriminants));
        return this.indistinguishableExprFilter.get(nt);
    }
    
    @Override
    public Buffer<Expression> ofSize(int size, NonTerminal nt) {
        checkArgument(size >= 0);
        checkNotNull(nt);

        if (!memo.contains(size, nt)) {
            Iterator<Expression> ans = build(size, nt);
            ans = indistinguishabilityFilterFor(nt).filter(ans);
            String iteratorName = String.format("(%s, %s, %d)", nt, example, size);
            // ans = printingIterator(ans, iteratorName, System.out);
            // ans = signalingIterator(ans, iteratorName, System.out);
            memo.put(size, nt, new Buffer<>(ans));
        }

        return memo.get(size, nt);
    }

    public String dumpState() {
        StringBuilder ans = new StringBuilder();
        for (int size : memo.rowKeySet()) {
            for (NonTerminal nt : memo.row(size).keySet()) {
                ans.append(String.format("Size: %s, non-terminal: %s\n", size, nt));
                for (Expression e : memo.get(size, nt)) {
                    ans.append(String.format("%s\n", e));
                }
            }
        }
        return ans.toString();
    }

    private final Table<Integer, NonTerminal, Buffer<Expression>> memo;
    private final Map<NonTerminal, IndistinguishableExprFilter> indistinguishableExprFilter;
    private final PosExprPoolGroup posPoolGroup;
}
