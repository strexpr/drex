package edu.upenn.cis.drex.sa.enumerators.base;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.sa.enumerators.templates.Grammar;
import edu.upenn.cis.drex.sa.enumerators.templates.NonTerminal;
import java.util.Collection;
import java.util.Iterator;

public abstract class AbstractPoolGroup {

    public AbstractPoolGroup(Grammar grammar, Collection<IOExample> examples) {
        checkNotNull(grammar);
        checkNotNull(examples);

        this.grammar = grammar;
        this.examples = examples;
    }

    public abstract AbstractPoolGroup addExample(IOExample ex);

    // public abstract AbstractPool getPool(InputOutputExample ex);

    public abstract Iterator<Expression> fromAllPools(NonTerminal holeType, int size);

    /**
     * Given a hole type, and production size, produces all expressions which
     * match the given input-output examples.
     * @param holeType
     * @param size
     * @param examples
     * @return
     */
    public abstract Iterator<Expression> enumerate(NonTerminal holeType, int size,
            ImmutableCollection<IOExample> examples);

    public Iterator<Expression> enumerate(int size) {
        return this.enumerate(this.grammar.start, size, ImmutableSet.copyOf(this.examples));
    }

    public final Grammar grammar;
    public final Collection<IOExample> examples;

}
