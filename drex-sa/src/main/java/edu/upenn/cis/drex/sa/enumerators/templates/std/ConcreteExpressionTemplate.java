package edu.upenn.cis.drex.sa.enumerators.templates.std;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.Production;
import edu.upenn.cis.drex.sa.enumerators.templates.Hole;
import edu.upenn.cis.drex.sa.enumerators.templates.IOConstraints;

public class ConcreteExpressionTemplate extends Production {

    public ConcreteExpressionTemplate(Expression expr) {
        super(ImmutableSet.<Hole>of(), TemplateType.EXPR_TEMPLATE,
                Predicates.<Expression>alwaysTrue());
        this.expr = checkNotNull(expr);
    }

    @Override
    public ImmutableSet<IOConstraints> holeEnumeratorsFor(IOExample example) {
        checkNotNull(example);

        if (example.matches(expr)) {
            return IOConstraints.TRUE;
        } else {
            return IOConstraints.FALSE;
        }
    }

    @Override
    public Expression build(ImmutableMap<Hole, Expression> holes) {
        checkNotNull(holes);
        return expr;
    }

    @Override
    public Production substitute(Hole h, Expression e) {
        checkNotNull(h);
        checkNotNull(e);
        return this;
    }

    @Override
    public String toString() {
        return String.format("(concrete-expr-template %s)", expr);
    }

    public final Expression expr;

}
