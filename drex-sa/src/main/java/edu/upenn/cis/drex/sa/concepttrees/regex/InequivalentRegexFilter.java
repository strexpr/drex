package edu.upenn.cis.drex.sa.concepttrees.regex;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.sa.util.collections.StatefulFilter;
import edu.upenn.cis.drex.sa.util.collections.StatefulFilter.State;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class InequivalentRegexFilter {

    public InequivalentRegexFilter() {
        this.analysedRegexes = new HashSet<>();
        this.regexTree = RegexTree.EMPTY;
    }

    public Iterator<Regex> filter(Iterator<Regex> iterator) {
        State<Regex> state = new FilterState(this);
        return new StatefulFilter(checkNotNull(iterator), state);
    }

    private final Set<Regex> analysedRegexes;
    private /* mutable */ RegexTree regexTree;

    private class FilterState implements State<Regex> {

        public FilterState(InequivalentRegexFilter filter) {
            this.filter = checkNotNull(filter);
        }

        @Override
        public boolean update(Regex t) {
            if (analysedRegexes.contains(checkNotNull(t))) {
                return false;
            }
            analysedRegexes.add(checkNotNull(t));

            Optional<RegexTree> treePrime = regexTree.sieve(t);
            if (treePrime.isPresent()) {
                filter.regexTree = treePrime.get();
                return true;
            } else {
                return false;
            }
        }

        public final InequivalentRegexFilter filter;

    }

}
