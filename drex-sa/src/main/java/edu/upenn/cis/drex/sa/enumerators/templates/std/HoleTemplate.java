package edu.upenn.cis.drex.sa.enumerators.templates.std;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.Production;
import edu.upenn.cis.drex.sa.enumerators.templates.Hole;
import edu.upenn.cis.drex.sa.enumerators.templates.IOConstraints;

public class HoleTemplate extends Production {

    public HoleTemplate(Hole hole) {
        this(checkNotNull(hole), Predicates.<Expression>alwaysTrue());
    }

    public HoleTemplate(Hole hole, Predicate<Expression> buildPredicate) {
        super(ImmutableSet.of(checkNotNull(hole)), hole.type,
                checkNotNull(buildPredicate));
        this.hole = hole;
    }

    @Override
    public ImmutableList<IOConstraints> holeEnumeratorsFor(IOExample example) {
        IOConstraints cons = new IOConstraints(ImmutableMap.of(this.hole, ImmutableSet.of(example)));
        return ImmutableList.of(cons);
    }

    @Override
    public Expression build(ImmutableMap<Hole, Expression> holes) {
        checkNotNull(holes);
        checkArgument(holes.containsKey(hole));
        return holes.get(hole);
    }

    @Override
    public Production substitute(Hole h, Expression e) {
        checkNotNull(h);
        checkNotNull(e);

        if (hole.equals(h)) {
            return new ConcreteExpressionTemplate(e);
        } else {
            return this;
        }
    }

    @Override
    public String toString() {
        return String.format("(hole-template %s %s)", hole, hole.holeType);
    }

    public final Hole hole;

}
