package edu.upenn.cis.drex.sa.util.comparators;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.Comparator;
import theory.CharConstant;
import theory.CharFunc;
import theory.CharOffset;

public class CharFuncComparator implements Comparator<CharFunc> {

    @Override
    public int compare(CharFunc f1, CharFunc f2) {
        checkNotNull(f1);
        checkArgument(f1 instanceof CharConstant || f1 instanceof CharOffset);
        checkNotNull(f2);
        checkArgument(f2 instanceof CharConstant || f2 instanceof CharOffset);

        if (f1 instanceof CharConstant && f2 instanceof CharConstant) {
            CharConstant f1c = (CharConstant)f1;
            CharConstant f2c = (CharConstant)f2;
            return f1c.c - f2c.c;
        } else if (f1 instanceof CharConstant && f2 instanceof CharOffset) {
            return -1;
        } else if (f1 instanceof CharOffset && f2 instanceof CharConstant) {
            return 1;
        } else {
            checkAssertion(f1 instanceof CharOffset && f2 instanceof CharOffset);
            CharOffset f1o = (CharOffset)f1;
            CharOffset f2o = (CharOffset)f2;
            return Long.compare(f1o.increment, f2o.increment);
        }
    }

}
