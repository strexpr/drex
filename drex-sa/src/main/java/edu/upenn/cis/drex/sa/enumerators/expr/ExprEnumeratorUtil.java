package edu.upenn.cis.drex.sa.enumerators.expr;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import java.util.Iterator;

public class ExprEnumeratorUtil {

    public static Iterator<Expression> whichMatch(Iterator<Expression> iterator,
            String input, String output) {
        return whichMatch(iterator, IOExample.io(input, output));
    }

    public static Iterator<Expression> whichMatch(Iterator<Expression> iterator,
            final IOExample example) {
        checkNotNull(iterator);
        checkNotNull(example);

        return Iterators.filter(checkNotNull(iterator), new Predicate<Expression>() {
            @Override
            public boolean apply(Expression expr) {
                checkNotNull(expr);
                checkNotNull(expr.getDomainType());
                return example.matches(expr);
            }
        });
    }

    public static Iterator<Expression> whichMatch(Iterator<Expression> iterator,
            ImmutableCollection<IOExample> examples) {
        checkNotNull(iterator);
        checkNotNull(examples);
        checkArgument(!examples.contains(null));

        for (IOExample example : examples) {
            iterator = whichMatch(iterator, example);
        }

        return iterator;
    }
    
    public static Predicate<Expression> IS_ANY_ITER = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return (t instanceof IteratedSum);
        }
    };

    public static Predicate<Expression> IS_ITER = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return (t instanceof IteratedSum) && !((IteratedSum)t).left;
        }
    };

    public static Predicate<Expression> IS_LITER = new Predicate<Expression>() {
        @Override
        public boolean apply(Expression t) {
            return (t instanceof IteratedSum) && ((IteratedSum)t).left;
        }
    };

}
