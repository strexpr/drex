package edu.upenn.cis.drex.sa.util;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.primitives.Chars;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class StringUtil {

    public static ImmutableSet<String> getSubstrings(String string) {
        ImmutableSet.Builder<String> ans = ImmutableSet.builder();
        ans.add("");
        for (int i = 0; i < checkNotNull(string).length(); i++) {
            for (int j = i; j <= string.length(); j++) {
                ans.add(string.substring(i, j));
            }
        }
        return ans.build();
    }

    public static ImmutableSet<String> getSubstrings(ImmutableCollection<String> strings) {
        ImmutableSet.Builder<String> ans = ImmutableSet.builder();
        for (String string : checkNotNull(strings)) {
            ans.addAll(getSubstrings(checkNotNull(string)));
        }
        return ans.build();
    }
    
    private static ImmutableList<ImmutablePair<String, String>> getSplitsInternal(String s, boolean swap) {
        ImmutableList.Builder<ImmutablePair<String, String>> ans = ImmutableList.builder();
        for (int i = 0; i <= s.length(); i++) {
            if (!swap) {
                ans.add(ImmutablePair.of(s.substring(0, i), s.substring(i)));
            } else {
                ans.add(ImmutablePair.of(s.substring(i), s.substring(0, i)));
            }
        }
        return ans.build();
    }
    
    public static ImmutableList<ImmutablePair<String, String>> getSplits(String s) {
        return getSplitsInternal(s, false);
    }
    
    public static ImmutableList<ImmutablePair<String, String>> getReverseSplits(String s) {
        return getSplitsInternal(s, true);
    }

    public static ImmutableList<ImmutablePair<String, String>> getLeftNonEmptySplits(String s) {
        ImmutableList.Builder<ImmutablePair<String, String>> ans = ImmutableList.builder();
        for (int i = 1; i <= s.length(); i++) {
            ans.add(ImmutablePair.of(s.substring(0, i), s.substring(i)));
        }
        return ans.build();
    }

    public static String printHex(String str) {
        checkNotNull(str);
        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            ans.append("[");
            for (byte b : Chars.toByteArray(str.charAt(i))) {
                ans.append(String.format("%d, ", b));
            }
            ans.append("] ");
        }
        return ans.toString();
    }

}
