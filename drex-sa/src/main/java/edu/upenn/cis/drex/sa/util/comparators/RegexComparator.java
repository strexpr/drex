package edu.upenn.cis.drex.sa.util.comparators;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.regex.Complement;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Empty;
import edu.upenn.cis.drex.core.regex.Epsilon;
import edu.upenn.cis.drex.core.regex.IVisitor;
import edu.upenn.cis.drex.core.regex.Intersection;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import edu.upenn.cis.drex.core.regex.Union;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.Comparator;
import org.apache.commons.lang3.builder.CompareToBuilder;

public class RegexComparator implements Comparator<Regex> {

    @Override
    public int compare(Regex r1, Regex r2) {
        if (checkNotNull(r1).size != checkNotNull(r2).size) {
            return r1.size - r2.size;
        }

        return new RegexComparatorVisitor(r2).visit(r1);
    }

}

class RegexComparatorVisitor extends IVisitor<Integer> {

    public RegexComparatorVisitor(Regex r2) {
        this.r2 = checkNotNull(r2);
    }

    @Override
    public Integer visitEmpty(Empty r) {
        if (r2 instanceof Empty) {
            return 0;
        } else {
            return -1;
        }
    }

    @Override
    public Integer visitEpsilon(Epsilon r) {
        if (r2 instanceof Empty) {
            return 1;
        } else if (r2 instanceof Epsilon) {
            return 0;
        } else {
            return -1;
        }
    }

    @Override
    public Integer visitSymbol(Symbol r) {
        if (r2 instanceof Empty || r2 instanceof Epsilon) {
            return 1;
        } else {
            checkAssertion(r2 instanceof Symbol);
            return new CharPredComparator().compare(r.a, ((Symbol)r2).a);
        }
    }

    @Override
    public Integer visitUnion(Union r) {
        if (r2 instanceof Star || r2 instanceof Concat) {
            return 1;
        } else if (r2 instanceof Union) {
            Union r2Union = (Union)r2;
            return new CompareToBuilder()
                    .append(r.r1, r2Union.r1, new RegexComparator())
                    .append(r.r2, r2Union.r2, new RegexComparator())
                    .toComparison();
        } else {
            checkAssertion(r2 instanceof Complement || r2 instanceof Intersection);
            return -1;
        }
    }

    @Override
    public Integer visitConcat(Concat r) {
        if (r2 instanceof Star) {
            return 1;
        } else if (r2 instanceof Concat) {
            Concat r2Concat = (Concat)r2;
            return new CompareToBuilder()
                    .append(r.r1, r2Concat.r1, new RegexComparator())
                    .append(r.r2, r2Concat.r2, new RegexComparator())
                    .toComparison();
        } else {
            checkAssertion(r2 instanceof Union || r2 instanceof Complement ||
                    r2 instanceof Intersection);
            return -1;
        }
    }

    @Override
    public Integer visitStar(Star r) {
        if (r2 instanceof Star) {
            return new RegexComparator().compare(r.r, ((Star)r2).r);
        } else {
            checkAssertion(r2 instanceof Concat || r2 instanceof Union ||
                    r2 instanceof Complement || r2 instanceof Intersection);
            return -1;
        }
    }

    @Override
    public Integer visitComplement(Complement r) {
        if (r2 instanceof Star || r2 instanceof Concat || r2 instanceof Union) {
            return 1;
        } else if (r2 instanceof Complement) {
            return new RegexComparator().compare(r.r, ((Complement)r2).r);
        } else {
            checkAssertion(r2 instanceof Intersection);
            return -1;
        }
    }

    @Override
    public Integer visitIntersection(Intersection r) {
        if (r2 instanceof Star || r2 instanceof Concat || r2 instanceof Union ||
                r2 instanceof Complement) {
            return 1;
        } else {
            checkAssertion(r2 instanceof Intersection);
            Intersection r2Intersection = (Intersection)r2;
            return new CompareToBuilder()
                    .append(r.r1, r2Intersection.r1, new RegexComparator())
                    .append(r.r2, r2Intersection.r2, new RegexComparator())
                    .toComparison();
        }
    }

    public final Regex r2;

}
