package edu.upenn.cis.drex.sa.enumerators.regex;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.sa.util.StringUtil;
import java.util.HashMap;
import java.util.Map;
import theory.CharPred;

public class PositiveRegexPoolGroup {

    public PositiveRegexPoolGroup(ImmutableCollection<String> posStrSet,
            ImmutableCollection<CharPred> charPreds, boolean sorted,
            ImmutableCollection<Regex> baseRegexes) {
        checkNotNull(posStrSet);
        checkArgument(!posStrSet.contains(null));
        checkNotNull(charPreds);
        checkArgument(!charPreds.contains(null));
        checkNotNull(baseRegexes);
        checkArgument(!baseRegexes.contains(null));

        this.posStrSet = posStrSet;
        this.charPreds = charPreds;
        this.sorted = sorted && baseRegexes.isEmpty();
        this.baseRegexes = baseRegexes;

        ImmutableSet.Builder<String> posSubstrBuilder = ImmutableSet.builder();
        for (String str : posStrSet) {
            posSubstrBuilder.addAll(StringUtil.getSubstrings(str));
        }
        this.posSubstrSet = posSubstrBuilder.build();

        this.pools = new HashMap<>();
    }

    public PositiveRegexPool getPool(String string) {
        checkNotNull(string);
        checkArgument(posSubstrSet.contains(string));

        if (pools.containsKey(string)) {
            return pools.get(string);
        } else {
            PositiveRegexPool ans = new PositiveRegexPool(string, this);
            pools.put(string, ans);
            return ans;
        }
    }

    public final ImmutableCollection<String> posStrSet;
    public final ImmutableCollection<CharPred> charPreds;
    public final boolean sorted;
    public final ImmutableCollection<Regex> baseRegexes;
    public final ImmutableCollection<String> posSubstrSet;
    private final Map<String, PositiveRegexPool> pools;

}
