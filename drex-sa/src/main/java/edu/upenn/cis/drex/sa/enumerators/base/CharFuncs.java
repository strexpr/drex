package edu.upenn.cis.drex.sa.enumerators.base;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import theory.CharOffset;

public class CharFuncs {

    public static ImmutableSet<Character> makeConstants(ImmutableCollection<IOExample> examples) {
        checkNotNull(examples);
        checkArgument(!examples.contains(null));

        ImmutableSet.Builder<Character> ans = ImmutableSet.builder();

        for (IOExample example : examples) {
            if (example.outputKnown()) {
                String output = example.getOutput();
                for (int i = 0; i < output.length(); i++) {
                    ans.add(output.charAt(i));
                }
            }
        }

        return ans.build();
    }

    public static ImmutableSet<CharOffset> makeOffsets(ImmutableCollection<IOExample> examples) {
        checkNotNull(examples);
        checkArgument(!examples.contains(null));

        ImmutableSet.Builder<CharOffset> ans = ImmutableSet.builder();

        for (IOExample example : examples) {
            if (example.outputKnown()) {
                for (int i = 0; i < example.input.length(); i++) {
                    char li = example.input.charAt(i);
                    String output = example.getOutput();
                    for (int j = 0; j < output.length(); j++) {
                        char rj = output.charAt(j);
                        ans.add(new CharOffset(rj - li));
                    }
                }
            }
        }

        return ans.build();
    }

    public static final ImmutableSet<CharOffset> STD_OFFSETS =
            ImmutableSet.of(new CharOffset(0));

}
