package edu.upenn.cis.drex.sa.util.collections;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Buffer<T> implements Iterable<T> {

    public Buffer(Iterator<T> iterator) {
        this.back = new ArrayList<>();
        this.iterator = checkNotNull(iterator);
    }

    public int exhaustIterator() {
        Iterator<T> view = iterator();
        while (view.hasNext()) {
            view.next();
        }
        return back.size();
    }
    
    @Override
    public Iterator<T> iterator() {
        return new BackedView(this);
    }

    private class BackedView implements Iterator<T> {

        public BackedView(Buffer backing) {
            this.backing = checkNotNull(backing);
        }

        @Override
        public boolean hasNext() {
            return i < back.size() || iterator.hasNext();
        }

        @Override
        public T next() {
            if (i < back.size()) {
                T ans = back.get(i);
                i++;
                return ans;
            } else {
                T ans = iterator.next();
                back.add(ans);
                i++;
                return ans;
            }
        }

        public final Buffer backing;
        private /* mutable */ int i = 0;

    }

    private final List<T> back;
    private final Iterator<T> iterator;

}
