package edu.upenn.cis.drex.sa.enumerators.expr;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.sa.enumerators.base.CharFuncs;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.util.StringUtil;
import java.util.Iterator;
import java.util.NoSuchElementException;
import theory.CharOffset;
import theory.CharPred;

public class BlindExprEnumerator implements Iterator<Expression> {

    public BlindExprEnumerator(BlindExprPool pool) {
        this.pool = checkNotNull(pool);
        this.size = 0;
        this.sizeSpecificIterator = pool.ofSize(size).iterator();
        this.emptyRun = 0;
    }

    public BlindExprEnumerator(ImmutableSet<CharPred> charPreds,
            ImmutableSet<Character> constants,
            ImmutableSet<CharOffset> offsets,
            ImmutableList<String> discriminants) {
        this(new BlindExprPool(checkNotNull(charPreds),
                checkNotNull(constants), checkNotNull(offsets),
                checkNotNull(discriminants)));
    }

    @Override
    public boolean hasNext() {
        while (!sizeSpecificIterator.hasNext()) {
            size++;
            sizeSpecificIterator = pool.ofSize(size).iterator();
            emptyRun++;

            if (size > 10 && size < 10 * emptyRun) {
                // TODO! Tighten bounds!
                return false;
            }
        }

        emptyRun = 0;
        return true;
    }

    @Override
    public Expression next() {
        if (hasNext()) {
            return sizeSpecificIterator.next();
        } else {
            throw new NoSuchElementException();
        }
    }

    public final BlindExprPool pool;
    private /* mutable */ int size;
    private /* mutable */ Iterator<Expression> sizeSpecificIterator;
    private /* mutable */ int emptyRun;

    public static Iterator<Expression> ofExamples(ImmutableCollection<IOExample> examples,
            CharPreds.BasePreds basePreds) {
        checkNotNull(examples);
        checkArgument(!examples.contains(null));

        ImmutableSet.Builder<String> baseStringsBuilder = ImmutableSet.builder();
        for (IOExample example : examples) {
            baseStringsBuilder.add(checkNotNull(example.input));
        }
        ImmutableSet<String> baseStrings = baseStringsBuilder.build();
        ImmutableSet<CharPred> charPreds = CharPreds.makeBaseCharPredsFromStrings(baseStrings, basePreds);
        ImmutableSet<Character> constants = CharFuncs.makeConstants(examples);
        ImmutableSet<CharOffset> offsets = CharFuncs.makeOffsets(examples);

        return ofExamples(examples, charPreds, constants, offsets);
    }
    
    public static Iterator<Expression> ofExamples(ImmutableCollection<IOExample> examples,
        ImmutableSet<CharPred> charPreds, ImmutableSet<Character> constants,
        ImmutableSet<CharOffset> offsets) {
        
        checkNotNull(examples);
        checkArgument(!examples.contains(null));

        ImmutableSet.Builder<String> baseStringsBuilder = ImmutableSet.builder();
        for (IOExample example : examples) {
            baseStringsBuilder.add(checkNotNull(example.input));
        }
        ImmutableSet<String> baseStrings = baseStringsBuilder.build();
        ImmutableList<String> discriminants = ImmutableList.copyOf(StringUtil.getSubstrings(baseStrings));

        BlindExprPool pool = new BlindExprPool(charPreds, constants, offsets, discriminants);
        Iterator<Expression> ans = new BlindExprEnumerator(pool);
        ans = ExprEnumeratorUtil.whichMatch(ans, examples);
        return ans;       
    }

}
