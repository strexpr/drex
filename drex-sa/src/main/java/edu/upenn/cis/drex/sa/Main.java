package edu.upenn.cis.drex.sa;

import automata.sfa.SFA;
import com.google.common.base.CharMatcher;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;
import com.google.common.primitives.Chars;
import edu.upenn.cis.drex.core.dp.DP;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.forkjoin.ExpressionEvaluatorBuilder;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import static edu.upenn.cis.drex.sa.Main.getRandomCharacter;
import edu.upenn.cis.drex.sa.enumerators.base.AbstractPoolGroup;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds.BasePreds;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.expr.ExprEnumerator;
import edu.upenn.cis.drex.sa.enumerators.expr.MultiEnumeratorPoolGroup;
import edu.upenn.cis.drex.sa.enumerators.expr.PosExprPoolGroup;
import edu.upenn.cis.drex.sa.enumerators.templates.Grammar;
import edu.upenn.cis.drex.sa.enumerators.templates.std.MultiGrammar;
import edu.upenn.cis.drex.sa.enumerators.templates.std.PosGrammar;
import edu.upenn.cis.drex.sa.sed.SedUtil;
import edu.upenn.cis.drex.sa.util.StringUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import org.apache.commons.lang3.tuple.ImmutablePair;
import theory.CharFunc;
import theory.CharOffset;
import theory.CharPred;
import theory.CharSolver;
import transducers.sst.SST;

public class Main {

    public static ImmutableSet<IOExample> getExamples(String cmd,
            ImmutableCollection<String> inputs) throws IOException {
        checkNotNull(cmd);
        checkNotNull(inputs);

        ImmutableSet.Builder<IOExample> ans = ImmutableSet.builder();
        for (String input : inputs) {
            checkNotNull(input);
            String output = SedUtil.getOutput(cmd, input);
            checkAssertion(output != null);
            IOExample example = IOExample.io(input, output);
            ans.add(example);
        }
        return ans.build();
    }

    public static void loop(
            String cmd,
            ImmutableCollection<String> inputs,
            ImmutableCollection<ImmutablePair<Regex, Regex>> specs,
            Grammar grammar,
            boolean useMultiEnumerator)
            throws IOException
    {
        AbstractPoolGroup poolGroup = null;
        if (useMultiEnumerator) {
            checkArgument(grammar instanceof MultiGrammar);
            ImmutableCollection<IOExample> ex = getExamples(cmd, inputs);
            List<IOExample> mut_ex = Lists.newArrayList(ex);
            poolGroup = new MultiEnumeratorPoolGroup((MultiGrammar)grammar, mut_ex);
        } else {
            checkArgument(grammar instanceof PosGrammar);
            poolGroup = new PosExprPoolGroup(getExamples(cmd, inputs), (PosGrammar)grammar);
        }

        CharSolver solver = new CharSolver();
        while (true) {
            System.out.printf("--------------------------------------------\n");
            System.out.printf("Main loop iteration\n");

            ExprEnumerator enumerator = new ExprEnumerator(poolGroup); // TODO! Fix!

            System.out.printf("Grammar:\n%s\n", poolGroup.grammar);
            System.out.printf("Examples:\n");
            for (IOExample ex : poolGroup.examples) {
                System.out.printf("%s\n", ex);
            }
            System.out.println("Beginning expression enumeration.");
            if (!enumerator.hasNext()) {
                // Unsynthesizable!
                System.out.printf("Unsynthesizable!\n");
                break;
            }

            Expression e = enumerator.next();
            System.out.printf("Testing candidate expression: %s\n", e);
            System.out.printf("Candidate expression size: %d\n", e.size);

            for (IOExample io : poolGroup.examples) {
                checkAssertion(io.matches(e));
            }

            SST<CharPred, CharFunc, Character> sst = e.getSST(solver);
            SFA<CharPred, Character> eDomain = e.getDomainType().getSFA();
            checkAssertion(eDomain != null);
            SFA<CharPred, Character> notEDomain = eDomain.complement(solver);

            boolean terminateMainLoop = true;

            for (ImmutablePair<Regex, Regex> spec : specs) {
                Regex pre = spec.left;
                Regex post = spec.right;

                SFA<CharPred, Character> notPost = post.getSFA().complement(solver);
                System.out.println("POST: " + post);
                System.out.println("NOT POST AUT: " + notPost);
                System.out.println("SST: " + e);
                SFA<CharPred, Character> notPostPreim = SST.preImage(sst, notPost, solver);
                SFA<CharPred, Character> badInputs = notPostPreim.intersectionWith(pre.getSFA(), solver);

                @SuppressWarnings("null")
                SFA<CharPred, Character> undefInputs = pre.getSFA().intersectionWith(notEDomain, solver);
                if (!undefInputs.isEmpty()) {
                    List<Character> inputWitnessList = undefInputs.getWitness(solver);
                    String inputWitness = new String(Chars.toArray(inputWitnessList));
                    String expectedOutput = SedUtil.getOutput(cmd, inputWitness);

                    System.out.printf("Expression undefined on input: \"%s\"\n", inputWitness);
                    System.out.printf("Added input-output example: \"%s\" -> \"%s\"\n",
                            inputWitness, expectedOutput);
                    System.out.printf("In hex: %s -> %s", StringUtil.printHex(inputWitness), StringUtil.printHex(expectedOutput));
                    System.out.printf("Input length: %d\n", inputWitness.length());
                    System.out.printf("Output length: %d\n", expectedOutput.length());

                    IOExample newExample = IOExample.io(inputWitness, expectedOutput);
                    poolGroup = poolGroup.addExample(newExample);

                    terminateMainLoop = false;
                    break;
                } else if (badInputs.isEmpty()) {
                    // skip
                } else {
                    List<Character> inputWitnessList = badInputs.getWitness(solver);
                    String inputWitness = new String(Chars.toArray(inputWitnessList));
                    String actualOutput = DP.eval(e, inputWitness);
                    String expectedOutput = SedUtil.getOutput(cmd, inputWitness);

                    if (actualOutput.equals(expectedOutput)) {
                        // Real counter-example!
                        System.out.printf("Reference function fails specification\n");
                        System.out.printf("Counter-example input: \"%s\"\n", inputWitness);
                        System.out.printf("Counter-example output: \"%s\"\n", actualOutput);
                        System.out.printf("Pre: %s\n", pre);
                        System.out.printf("Post: %s\n", post);
                        terminateMainLoop = true;
                        break;
                    } else {
                        System.out.printf("Candidate expression disagrees with "
                                + "reference function on input: \"%s\"\n", inputWitness);
                        System.out.printf("Added input-output example: \"%s\" -> \"%s\"\n",
                                inputWitness, expectedOutput);
                        System.out.printf("In hex, input: %s\n", StringUtil.printHex(inputWitness));
                        System.out.printf("In hex, actual output %s\n", StringUtil.printHex(actualOutput));
                        System.out.printf("In hex, expected output %s\n", StringUtil.printHex(expectedOutput));
                        System.out.printf("Input length: %d\n", inputWitness.length());
                        System.out.printf("Output length: %d\n", expectedOutput.length());

                        poolGroup = poolGroup.addExample(IOExample.io(inputWitness, expectedOutput));

                        terminateMainLoop = false;
                        break;
                    }
                }
            }

            if (terminateMainLoop) {
                System.out.printf("--------------------------------------------\n");
                System.out.printf("Mutation testing\n");
                List<String> testInputs = new ArrayList<>();
                for (IOExample ex : poolGroup.examples)
                    testInputs.add(ex.input);
                Optional<String> counterExample = mutationTest(cmd, e, ImmutableSet.copyOf(testInputs), poolGroup.grammar.basePreds, specs);
                if (counterExample.isPresent()) {
                    String cexInput = counterExample.get();
                    String expOutput = SedUtil.getOutput(cmd, cexInput);
                    String actOutput = DP.eval(e, cexInput);
                    checkAssertion(!actOutput.equals(expOutput));
                    System.out.printf("Mutation testing failed on input: \"%s\" (%d)\n",
                            cexInput, cexInput.length());
                    System.out.printf("Expected output: \"%s\" (%d)\n",
                            expOutput, expOutput.length());
                    System.out.printf("Actual output: \"%s\" (%d)\n",
                            actOutput, actOutput.length());
                    System.out.printf("In hex, input: %s\n", StringUtil.printHex(cexInput));
                    System.out.printf("In hex, actual output %s\n", StringUtil.printHex(actOutput));
                    System.out.printf("In hex, expected output %s\n", StringUtil.printHex(expOutput));

                    poolGroup = poolGroup.addExample(IOExample.io(cexInput, expOutput));

                    terminateMainLoop = false;
                }
            }

            if (terminateMainLoop) {
                System.out.printf("--------------------------------------------\n");
                System.out.printf("Main loop termination\n");
                System.out.printf("Synthesized expression: %s\n", e);
                break;
            }
        }
    }

    public static void loop(String cmd, ImmutableCollection<String> inputs,
            ImmutableCollection<ImmutablePair<Regex, Regex>> specs,
            ImmutableSet<CharPred> charPreds, ImmutableSet<Character> constants,
            ImmutableSet<CharOffset> offsets,
            ImmutableCollection<Expression> baseExprs,
            ImmutableCollection<Function<Expression, Expression>> exprTemplates)
            throws IOException, Exception {
        checkNotNull(cmd);
        checkNotNull(inputs);
        checkArgument(!inputs.contains(null));
        checkNotNull(specs);
        checkArgument(!specs.contains(null));
        for (ImmutablePair<Regex, Regex> spec : specs) {
            checkNotNull(spec.left);
            checkNotNull(spec.right);
        }
        checkNotNull(charPreds);
        checkArgument(!charPreds.contains(null));
        checkNotNull(constants);
        checkArgument(!constants.contains(null));
        checkNotNull(baseExprs);
        checkArgument(!baseExprs.contains(null));
        checkNotNull(exprTemplates);
        checkArgument(!exprTemplates.contains(null));

        boolean useMultiEnumerator = true;

        CharPreds.BasePreds basePreds = CharPreds.BasePreds.CHARS_INCLUDING_NOT;
        boolean sorted = true;
        boolean unions = true;
        boolean restrict = false;
        System.out.printf("basePreds: %s\n", basePreds);
        System.out.printf("sorted: %s\n", sorted);
        System.out.printf("unions: %s\n", unions);
        System.out.printf("restrict: %s\n", restrict);

        Iterator<Expression> enumerator = null;
        List<IOExample> newExamples = new ArrayList<>();
        while (true) {
            System.out.printf("--------------------------------------------\n");
            System.out.printf("Main loop iteration\n");

            ImmutableSet<IOExample> examples = getExamples(cmd, inputs);

            if (!useMultiEnumerator) {
                throw new Exception("Positive enumerator presently broken!");
                /*
                enumerator = PosExprEnumerator.ofExamples(
                    examples, charPreds, constants, offsets, sorted, unions,
                    restrict, baseExprs, exprTemplates);
                */
            } else {
                throw new Exception("Multi enumerator presently broken!");
                // enumerator = MultiEnumerator.ofExamples(
                //     examples, charPreds, constants, offsets, sorted, unions,
                //     restrict, baseExprs, exprTemplates);
                /*
                if (enumerator == null || !(enumerator instanceof MultiEnumerator))
                    enumerator = MultiEnumerator.ofExamples(
                        examples, charPreds, constants, offsets, sorted, unions,
                        true, baseExprs, exprTemplates);
                else {
                    // We need to add extension to pool groups first
                    for (InputOutputExample io : newExamples)
                        ((MultiEnumerator)enumerator).addExample(io);
                    newExamples.clear();
                }
                */
            }

            /*
            System.out.println("Beginning expression enumeration.");
            if (!enumerator.hasNext()) {
                // Unsynthesizable!
                System.out.printf("Unsynthesizable!\n");
                break;
            }

            // TODO! Correctness vs partial correctness
            Expression e = enumerator.next();
            System.out.printf("Testing candidate expression: %s\n", e);
            System.out.printf("Candidate expression size: %d\n", e.size);

            for (InputOutputExample io : examples) {
                checkAssertion(io.matches(e));
            }

            CharSolver solver = new CharSolver();
            SST<CharPred, CharFunc, Character> sst = e.getSST(solver);
            @SuppressWarnings("null")
            SFA<CharPred, Character> eDomain = e.getDomainType().getSFA();
            checkAssertion(eDomain != null);
            @SuppressWarnings("null")
            SFA<CharPred, Character> notEDomain = eDomain.complement(solver);

            boolean terminateMainLoop = true;

            for (ImmutablePair<Regex, Regex> spec : specs) {
                Regex pre = spec.left;
                Regex post = spec.right;

                @SuppressWarnings("null")

                SFA<CharPred, Character> notPost = post.getSFA().complement(solver);
                System.out.println("POST: " + post);
                System.out.println("NOT POST AUT: " + notPost);
                System.out.println("SST: " + e);
                SFA<CharPred, Character> notPostPreim = SST.preImage(sst, notPost, solver);
                SFA<CharPred, Character> badInputs = notPostPreim.intersectionWith(pre.getSFA(), solver);

                @SuppressWarnings("null")
                SFA<CharPred, Character> undefInputs = pre.getSFA().intersectionWith(notEDomain, solver);
                if (!undefInputs.isEmpty()) {
                    List<Character> inputWitnessList = undefInputs.getWitness(solver);
                    String inputWitness = new String(Chars.toArray(inputWitnessList));
                    String expectedOutput = SedUtil.getOutput(cmd, inputWitness);

                    newExamples.add(InputOutputExample.io(inputWitness, expectedOutput));
                    System.out.printf("Expression undefined on input: \"%s\"\n", inputWitness);
                    System.out.printf("Added input-output example: \"%s\" -> \"%s\"\n",
                            inputWitness, expectedOutput);
                    System.out.printf("Input length: %d\n", inputWitness.length());
                    System.out.printf("Output length: %d\n", expectedOutput.length());
                    if (inputs.contains(inputWitness))
                        throw new Exception("Trying to add input multiple times: " + inputWitness);
                    ImmutableSet.Builder<String> inputsPrime = ImmutableSet.builder();
                    inputsPrime.addAll(inputs);
                    inputsPrime.add(inputWitness);
                    inputs = inputsPrime.build();

                    terminateMainLoop = false;
                    break;
                } else if (badInputs.isEmpty()) {
                    // skip
                } else {
                    List<Character> inputWitnessList = badInputs.getWitness(solver);
                    String inputWitness = new String(Chars.toArray(inputWitnessList));
                    String actualOutput = DP.eval(e, inputWitness);
                    String expectedOutput = SedUtil.getOutput(cmd, inputWitness);

                    if (actualOutput.equals(expectedOutput)) {
                        // Real counter-example!
                        System.out.printf("Reference function fails specification\n");
                        System.out.printf("Counter-example input: \"%s\"\n", inputWitness);
                        System.out.printf("Counter-example output: \"%s\"\n", actualOutput);
                        System.out.printf("Pre: %s\n", pre);
                        System.out.printf("Post: %s\n", post);
                        terminateMainLoop = true;
                        break;
                    } else {
                        newExamples.add(InputOutputExample.io(inputWitness, expectedOutput));
                        System.out.printf("Candidate expression disagrees with "
                                + "reference function on input: \"%s\"\n", inputWitness);
                        System.out.printf("Added input-output example: \"%s\" -> \"%s\"\n",
                                inputWitness, expectedOutput);
                        System.out.printf("Input length: %d\n", inputWitness.length());
                        System.out.printf("Output length: %d\n", expectedOutput.length());
                        ImmutableSet.Builder<String> inputsPrime = ImmutableSet.builder();
                        inputsPrime.addAll(inputs);
                        inputsPrime.add(inputWitness);
                        inputs = inputsPrime.build();
                        terminateMainLoop = false;
                        break;
                    }
                }
            }

            if (terminateMainLoop) {
                System.out.printf("--------------------------------------------\n");
                System.out.printf("Mutation testing\n");
                Optional<String> counterExample = mutationTest(cmd, e, inputs, charPreds, specs);
                if (counterExample.isPresent()) {
                    String cexInput = counterExample.get();
                    String expOutput = SedUtil.getOutput(cmd, cexInput);
                    String actOutput = DP.eval(e, cexInput);
                    System.out.printf("Mutation testing failed on input: \"%s\" (%d)\n",
                            cexInput, cexInput.length());
                    System.out.printf("Expected output: \"%s\" (%d)\n",
                            expOutput, expOutput.length());
                    System.out.printf("Actual output: \"%s\" (%d)\n",
                            actOutput, actOutput.length());
                    ImmutableSet.Builder<String> inputsPrime = ImmutableSet.builder();

                    newExamples.add(InputOutputExample.io(cexInput, expOutput));
                    System.out.printf("Candidate expression disagrees with "
                                + "reference function on input: \"%s\"\n", cexInput);
                    System.out.printf("Added input-output example: \"%s\" -> \"%s\"\n",
                                cexInput, expOutput);
                    System.out.printf("Input length: %d\n", cexInput.length());
                    System.out.printf("Output length: %d\n", expOutput.length());
                    inputsPrime.addAll(inputs);
                    inputsPrime.add(cexInput);
                    inputs = inputsPrime.build();
                    terminateMainLoop = false;
                }
            }

            if (terminateMainLoop) {
                System.out.printf("--------------------------------------------\n");
                System.out.printf("Main loop termination\n");
                System.out.printf("Synthesized expression: %s\n", e);
                break;
            }

            */
        }
    }

    public static Optional<String> mutationTest(String cmd, Expression e,
            ImmutableCollection<String> inputs,
            Collection<CharPred> charPreds,
            ImmutableCollection<ImmutablePair<Regex, Regex>> specs) throws IOException {
        checkNotNull(cmd);
        checkNotNull(e);
        checkArgument(e.getDomainType() != null);
        checkNotNull(inputs);
        checkArgument(!inputs.contains(null));
        checkNotNull(charPreds);
        checkArgument(!charPreds.contains(null));

        int mutationTestingLimits = 10;
        for (String input : inputs) {
            for (int i = 0; i < mutationTestingLimits; i++) {
                String newInput = input;
                for (int j = 0; j < mutationTestingLimits; j++) {
                    newInput = mutate(newInput, charPreds, specs);
                    // System.out.println("Testing: " + newInput);
                    String expectedOutput = SedUtil.getOutput(cmd, newInput);
                    String actualOutput = DP.eval(e, newInput);
                    if (!expectedOutput.equals(actualOutput)) {
                        return Optional.of(newInput);
                    }
                }
            }
        }

        return Optional.absent();
    }

    public static String mutate(String input, Collection<CharPred> charPreds,
            ImmutableCollection<ImmutablePair<Regex, Regex>> specs) {
        checkNotNull(input);

        while (true) {
            String output = null;
            int choice = RANDOM_SOURCE.nextInt(3);
            if (choice == 0) {
                // 0. Double the length of some substring
                int pos1 = RANDOM_SOURCE.nextInt(input.length() + 1);
                int pos2 = RANDOM_SOURCE.nextInt(input.length() + 1);

                int posL = Math.min(pos1, pos2);
                int posR = Math.max(pos1, pos2);
                checkAssertion(posL <= posR && posR <= input.length());

                if (posL == posR) {
                    output = input;
                } else {
                    String left = input.substring(0, posL);
                    String mid = input.substring(posL, posR);
                    String right = input.substring(posR);
                    checkAssertion(input.equals(left + mid + right));
                    output = left + mid + mid + right;
                }
            } else if (choice == 1) {
                // 1. Delete some substring
                int pos1 = RANDOM_SOURCE.nextInt(input.length() + 1);
                int pos2 = RANDOM_SOURCE.nextInt(input.length() + 1);

                int posL = Math.min(pos1, pos2);
                int posR = Math.max(pos1, pos2);
                checkAssertion(posL <= posR);

                if (posL == posR) {
                    output = input;
                } else {
                    String left = input.substring(0, posL);
                    String mid = input.substring(posL, posR);
                    String right = input.substring(posR);
                    checkAssertion(input.equals(left + mid + right));
                    output = left + right;
                }
            } else {
                // 2. Introduce foreign characters at some location
                int pos = RANDOM_SOURCE.nextInt(input.length() + 1);
                checkAssertion(pos <= input.length());
                String left = input.substring(0, pos);
                String right = input.substring(pos);
                checkAssertion(input.equals(left + right));
                char c = getRandomCharacter(charPreds);
                if (CharMatcher.ASCII.matches(c)) {
                    output = left + c + right;
                }
            }

            for (ImmutablePair<Regex, Regex> spec : specs) {
                if (output != null && output.length() < 20 && spec.left.contains(output, new CharSolver())) {
                    return output;
                }
            }
        }
    }

    public static char getRandomCharacter(Collection<CharPred> charPreds) {
        checkNotNull(charPreds);
        checkArgument(!charPreds.contains(null));

        List<CharPred> satisfiablePreds = new ArrayList<>();
        for (CharPred pred : charPreds) {
            if (!pred.intervals.isEmpty()) {
                satisfiablePreds.add(pred);
            }
        }
        checkArgument(!satisfiablePreds.isEmpty());

        int predIndex = RANDOM_SOURCE.nextInt(satisfiablePreds.size());
        CharPred pred = satisfiablePreds.get(predIndex);
        return getRandomCharacter(pred);
    }

    public static char getRandomCharacter(CharPred pred) {
        checkNotNull(pred);
        checkArgument(!pred.intervals.isEmpty(), "The input predicate is unsatisfiable.");

        int intervalIndex = RANDOM_SOURCE.nextInt(pred.intervals.size());
        ImmutablePair<Character, Character> interval = pred.intervals.get(intervalIndex);
        char lo = interval.left;
        char hi = interval.right;
        checkAssertion(lo <= hi);
        long ans = lo + RANDOM_SOURCE.nextInt(hi - lo + 1);
        return (char)ans;
    }


    public static void main(String[] args)
            throws IOException
    {
        String cmd = "/^a*b*$/ y/ab/ba/";
        ImmutableSet<String> inputs = ImmutableSet.of("a");
        Set<IOExample> examples = new HashSet<>();
        for (String input : inputs) {
            examples.add(IOExample.io(input, SedUtil.getOutput(cmd, input)));
        }

        CharPred predA = new CharPred('a');
        CharPred predB = new CharPred('b');
        CharPred predAOrB = new CharPred('a', 'b');
        ImmutableCollection<CharPred> basePreds = ImmutableSet.of(predA, predB, predAOrB);
        PosGrammar G = PosGrammar.build(BasePreds.CHARS, examples, ImmutableSet.<Expression>of());

        Regex a = new edu.upenn.cis.drex.core.regex.Symbol(predA);
        Regex b = new edu.upenn.cis.drex.core.regex.Symbol(predB);
        Regex aStarbStar = new edu.upenn.cis.drex.core.regex.Concat(Star.plus(a), new Star(b));
        Regex bStaraStar = new edu.upenn.cis.drex.core.regex.Concat(Star.plus(b), new Star(a));
        ImmutableList<ImmutablePair<Regex, Regex>> specs = ImmutableList.of(ImmutablePair.of(aStarbStar, bStaraStar));

        Main.loop(cmd, inputs, specs, G, false);
    }

    public static final Random RANDOM_SOURCE = new Random(0);
    public static final boolean FORKJOIN_FAIL_FLAG = setForkjoinFailFlag();

    public static boolean setForkjoinFailFlag() {
        ExpressionEvaluatorBuilder.FAIL_FLAG = true;
        return true;
    }

}
