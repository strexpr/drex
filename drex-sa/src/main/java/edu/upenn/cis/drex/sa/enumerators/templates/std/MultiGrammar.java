package edu.upenn.cis.drex.sa.enumerators.templates.std;

import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds.BasePreds;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.Grammar;
import edu.upenn.cis.drex.sa.enumerators.templates.NonTerminal;
import java.util.Collection;

public class MultiGrammar extends Grammar {

    private MultiGrammar(BasePreds basePredStrategy) {
        super(new NonTerminal("Start"), basePredStrategy);
    }

    public static MultiGrammar build(BasePreds basePredStrategy, Collection<IOExample> examples,
            Collection<Expression> baseExprs) {
        System.out.printf("Calling MultiGrammar.build()\n");
        System.out.printf("basePredStrategy: %s\n", basePredStrategy);
        System.out.printf("baseExprs:\n");
        for (Expression ex : baseExprs) {
            System.out.printf("%s\n", ex);
        }

        MultiGrammar ans = new MultiGrammar(basePredStrategy);
        ans.populateExamples(examples);
        ans.populateConstStrings();
        ans.populateOffsets();
        ans.populateBasePreds();
        ans.populateBaseExprs(baseExprs);
        ans.populateProductions(false, true);
        return ans;
    }

}
