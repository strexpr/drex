package edu.upenn.cis.drex.sa.enumerators.base;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.primitives.Chars;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import theory.CharPred;
import theory.CharSolver;
import theory.StdCharPred;

public class CharPreds {

    public enum BasePreds { NONE, STD, CHARS, CHARS_INCLUDING_NOT };

    public static ImmutableSet<CharPred> makeBaseCharPredsFromChars(
            ImmutableCollection<Character> baseChars, BasePreds basePreds) {
        if (basePreds == BasePreds.NONE) {
            return ImmutableSet.of();
        }

        List<CharPred> preds = new ArrayList<>(STD_CHARPREDS);
        if (basePreds == BasePreds.CHARS || basePreds == BasePreds.CHARS_INCLUDING_NOT) {
            for (char c : checkNotNull(baseChars)) {
                CharPred pred = new CharPred(c);
                preds.add(pred);
                if (basePreds == BasePreds.CHARS_INCLUDING_NOT) {
                    preds.add((new CharSolver()).MkNot(pred));
                }
            }
        }

        Set<Set<CharPred>> eqClasses = new HashSet<>();
        for (CharPred pred : preds) {
            boolean isDistinctFromAll = true;
            for (Set<CharPred> eqClass : eqClasses) {
                checkAssertion(!eqClass.isEmpty());
                CharPred predPrime = eqClass.iterator().next();

                boolean isDistinctFromPredPrime = false;
                for (char c : baseChars) {
                    if (pred.isSatisfiedBy(c) != predPrime.isSatisfiedBy(c)) {
                        isDistinctFromPredPrime = true;
                        break;
                    }
                }

                if (!isDistinctFromPredPrime) {
                    eqClass.add(pred);
                    isDistinctFromAll = false;
                    break;
                }
            }

            if (isDistinctFromAll) {
                eqClasses.add(Sets.newHashSet(pred));
            }
        }

        CharSolver solver = new CharSolver();
        Set<CharPred> ans = new HashSet<>();
        for (Set<CharPred> eqClass : eqClasses) {
            CharPred rep = StdCharPred.TRUE;
            for (CharPred predPrime : eqClass) {
                rep = solver.MkAnd(rep, predPrime);
            }
            if (!rep.intervals.isEmpty()) {
                ans.add(rep);
            }
        }

        return ImmutableSet.copyOf(ans);
    }

    public static ImmutableSet<CharPred> makeBaseCharPredsFromStrings(
            ImmutableCollection<String> baseStrings, BasePreds basePreds) {
        ImmutableSet.Builder<Character> baseChars = ImmutableSet.builder();
        for (String str : checkNotNull(baseStrings)) {
            checkNotNull(str);
            baseChars.addAll(Chars.asList(str.toCharArray()));
        }
        return makeBaseCharPredsFromChars(baseChars.build(), basePreds);
    }

    public static final ImmutableSet<CharPred> STD_CHARPREDS = ImmutableSet.of(StdCharPred.TRUE,
            StdCharPred.ALPHA, StdCharPred.ALPHA_NUM, StdCharPred.BLANK,
            StdCharPred.CNTRL, StdCharPred.GRAPH, StdCharPred.LOWER_ALPHA,
            StdCharPred.NUM, StdCharPred.PRINT, StdCharPred.PUNCT,
            StdCharPred.SPACES, StdCharPred.UPPER_ALPHA, StdCharPred.WORD,
            StdCharPred.XDIGIT);

}
