package edu.upenn.cis.drex.sa.util.collections;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.Iterator;

public class StatelessAssertionIterator<T> implements Iterator<T> {

    public StatelessAssertionIterator(Iterator<T> iterator, Predicate<T> predicate) {
        this.iterator = checkNotNull(iterator);
        this.predicate = checkNotNull(predicate);
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public T next() {
        T ans = iterator.next();
        checkAssertion(predicate.apply(ans));
        return ans;
    }

    @Override
    public void remove() {
        iterator.remove();
    }

    private final Iterator<T> iterator;
    public final Predicate<T> predicate;

}
