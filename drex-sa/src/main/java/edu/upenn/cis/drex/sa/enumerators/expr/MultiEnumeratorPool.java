package edu.upenn.cis.drex.sa.enumerators.expr;

import static com.google.common.base.Preconditions.checkArgument;
import edu.upenn.cis.drex.sa.enumerators.base.AbstractPool;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Iterators;
import com.google.common.collect.Table;
import edu.upenn.cis.drex.core.expr.Expression;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import static edu.upenn.cis.drex.sa.enumerators.expr.BlindExprPool.CONSISTENT_FILTER;
import edu.upenn.cis.drex.sa.enumerators.templates.NonTerminal;
import edu.upenn.cis.drex.sa.util.collections.Buffer;
import java.util.Iterator;

/**
 * This class is supposed to technically work exactly like the
 * PosExprEnumeratorPool.
 * However, we PosExprEnumeratorPool builds * else's and other things without
 * any reference example.
 * @author aradha
 */
public class MultiEnumeratorPool extends AbstractPool {
    public MultiEnumeratorPool(MultiEnumeratorPoolGroup poolGroup,
            IOExample example) {
        super(checkNotNull(poolGroup), checkNotNull(example));
        checkAssertion(example.outputKnown());

        this.memo = HashBasedTable.create();
        this.multiPoolGroup = poolGroup;
    }

    @Override
    public Buffer<Expression> ofSize(int size, NonTerminal nt) {
        checkArgument(size >= 0);
        checkNotNull(nt);

        if (!memo.contains(size, nt)) {
            Iterator<Expression> ans = build(size, nt);
            String iteratorName = String.format("(%s, %s, %d)", nt, example, size);
            // ans = printingIterator(ans, iteratorName, System.out);
            // ans = signalingIterator(ans, iteratorName, System.out);
            ans = Iterators.filter(ans, CONSISTENT_FILTER);
            memo.put(size, nt, new Buffer<>(ans));
        }

        // memo.get(size, nt).exhaustIterator();

        return memo.get(size, nt);
    }

    private final Table<Integer, NonTerminal, Buffer<Expression>> memo;
    private final MultiEnumeratorPoolGroup multiPoolGroup;

}
