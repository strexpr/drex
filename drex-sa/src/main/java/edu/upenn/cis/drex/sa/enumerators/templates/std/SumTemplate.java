package edu.upenn.cis.drex.sa.enumerators.templates.std;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.Hole;
import edu.upenn.cis.drex.sa.enumerators.templates.IOConstraints;
import edu.upenn.cis.drex.sa.enumerators.templates.Production;
import edu.upenn.cis.drex.sa.enumerators.templates.ProductionUtil;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class SumTemplate extends Production {

    public SumTemplate(Production t1, Production t2, Predicate<Expression> buildPredicate) {
        super(Sets.union(checkNotNull(t1).holes, checkNotNull(t2).holes).immutableCopy(),
                t1.type, checkNotNull(buildPredicate));

        checkArgument(t1.type == t2.type);

        this.t1 = t1;
        this.t2 = t2;
    }

    @Override
    public ImmutableList<IOConstraints> holeEnumeratorsFor(IOExample example) {
        checkNotNull(example);

        String input = example.input;
        ImmutableList.Builder<IOConstraints> ansBuilder = ImmutableList.builder();

        if (example.outputKnown()) {
            for (ImmutablePair<IOExample, IOExample> exampleSplit : example.outputSplits()) {
                ImmutableCollection<IOConstraints> e1Holes = t1.holeEnumeratorsFor(exampleSplit.left);
                ImmutableCollection<IOConstraints> e2Holes = t2.holeEnumeratorsFor(exampleSplit.right);
                ansBuilder.addAll(ProductionUtil.combineIOConstraints(e1Holes, e2Holes));
            }
        } else if (example.defined) {
            ImmutableCollection<IOConstraints> e1Holes = 
                t1.holeEnumeratorsFor(IOExample.definedOn(input));
            ImmutableCollection<IOConstraints> e2Holes = 
                t2.holeEnumeratorsFor(IOExample.definedOn(input));
            ansBuilder.addAll(ProductionUtil.combineIOConstraints(e1Holes, e2Holes));
        } else {
            /*
            On not defined on, we give up and pass very weak constraints. We
            some how have to say that it is not defined on every split. It
            can be done by replacing and and or for IOConstraints.
            Currently, and is combine and or is putting the two
            IOConstraints in a list.
            */
        }

        ImmutableList<IOConstraints> ans = ansBuilder.build();
        return ans;
    }

    @Override
    public Expression build(ImmutableMap<Hole, Expression> holes) {
        checkNotNull(holes);

        Expression e1 = t1.build(holes);
        Expression e2 = t2.build(holes);
        return new Sum(e1, e2);
    }

    @Override
    public Production substitute(Hole h, Expression e) {
        checkNotNull(h);
        checkNotNull(e);

        Production t1Prime = t1.substitute(h, e);
        Production t2Prime = t2.substitute(h, e);
        return new SumTemplate(t1Prime, t2Prime, buildPredicate);
    }

    @Override
    public String toString() {
        return String.format("(sum-template %s %s)", t1, t2);
    }

    public Production t1, t2;

}
