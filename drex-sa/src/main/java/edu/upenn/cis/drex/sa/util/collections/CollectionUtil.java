package edu.upenn.cis.drex.sa.util.collections;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Predicate;
import com.google.common.base.Supplier;
import com.google.common.collect.Iterators;
import com.google.common.collect.Lists;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.function.Consumer;
import java.io.PrintStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import javax.annotation.Nullable;
import org.apache.commons.lang3.mutable.MutableLong;

public class CollectionUtil {

    public static <T> Iterator<T> mergeSorted(Comparator<T> comparator, Iterator<T>... iterators) {
        checkNotNull(comparator);
        List<Iterator<T>> iteratorList = Lists.newArrayList(iterators);
        return Iterators.mergeSorted(iteratorList, comparator);
    }

    public static <T> Iterator<T> assertIncreasing(Iterator<T> iterator, final Comparator<T> comparator) {
        checkNotNull(iterator);
        checkNotNull(comparator);

        return new StatefulAssertionIterator<>(iterator, new Consumer<T>() {

            @Override
            public void accept(T r) {
                checkNotNull(r);
                if (prev.isPresent()) {
                    T prevGet = prev.get();
                    boolean incr = comparator.compare(prevGet, r) < 0;
                    checkAssertion(incr);
                }
                prev = Optional.of(r);
            }

            public /* mutable */ Optional<T> prev = Optional.absent();

        });
    }

    public static <T> Iterator<T> lazyConcat(final List<Supplier<Iterator<T>>> suppliers) {
        checkNotNull(suppliers);

        return new Iterator<T> () {

            @Override
            public boolean hasNext() {
                while (!iterator.hasNext() && index < suppliers.size()) {
                    iterator = checkNotNull(suppliers.get(index).get());
                    index++;
                }

                return iterator.hasNext();
            }

            @Override
            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                return iterator.next();
            }

            private /* mutable */ Iterator<T> iterator = Collections.emptyIterator();
            private /* mutable */ int index = 0;

        };
    }

    public static <T> Iterator<T> lazyConcat(final Supplier<Iterator<T>>... suppliers) {
        checkNotNull(suppliers);

        return new Iterator<T> () {

            @Override
            public boolean hasNext() {
                while (!iterator.hasNext() && index < suppliers.length) {
                    iterator = checkNotNull(suppliers[index].get());
                    index++;
                }

                return iterator.hasNext();
            }

            @Override
            public T next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                return iterator.next();
            }

            private /* mutable */ Iterator<T> iterator = Collections.emptyIterator();
            private /* mutable */ int index = 0;

        };
    }

    public static <T> Iterator<T> flatten(final Iterator<Iterator<T>> iterator) {
        checkNotNull(iterator);
        return new Iterator<T>(){
            @Override
            public boolean hasNext() {
                while (curr == null) {
                    if (iterator.hasNext()) {
                        curr = iterator.next();
                    } else {
                        return false;
                    }
                }
                return curr.hasNext();
            }

            @Override
            public T next() {
                if (hasNext()) {
                    return curr.next();
                } else {
                    throw new NoSuchElementException();
                }
            }

            Iterator<T> curr = null;
        };
    }

    public static <T, U> Iterator<U> lazyFlatMap(final Iterator<T> iterator, final Function<T, Iterator<U>> f) {
        return new Iterator<U>() {

            @Override
            public boolean hasNext() {
                while(!currentIterator.hasNext() && iterator.hasNext())
                    currentIterator =  f.apply(iterator.next());
                return currentIterator.hasNext();
            }

            @Override
            public U next() {
                if (!hasNext()) {
                    throw new NoSuchElementException();
                }

                return currentIterator.next();
            }
            
            private Iterator<U> currentIterator = Collections.emptyIterator();
        };
    }

    public static <T> Iterator<T> buildConcrete(final RawIterator<T> rawIterator) {
        checkNotNull(rawIterator);
        return new Iterator<T>() {

            @Override
            public boolean hasNext() {
                try {
                    peekOrThrow();
                    checkAssertion(hasPeeked);
                    return hasPeeked;
                } catch (NoSuchElementException e) {
                    return false;
                }
            }

            @Override
            public T next() {
                peekOrThrow();
                T ans = t;
                hasPeeked = false;
                t = null;
                return ans;
            }

            public void peekOrThrow() throws NoSuchElementException {
                if (!hasPeeked) {
                    t = rawIterator.next();
                    hasPeeked = true;
                }
            }

            private /* mutable */ boolean hasPeeked;
            private /* mutable */ @Nullable T t;

        };
    }

    public static int signalingIteratorCount = 0;
    public static <T> Iterator<T> signalingIterator(final Iterator<T> iterator,
            final String iteratorName, final PrintStream printStream) {
        checkNotNull(iterator);
        checkNotNull(iteratorName);
        checkNotNull(printStream);
        final int signalingIteratorId = signalingIteratorCount++;
        final MutableLong totalTime = new MutableLong(0);
        final MutableLong maxTime = new MutableLong(0);

        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                if (!printedBegin) {
                    printStream.printf("Starting iterator [%d] %s.\n", signalingIteratorId, iteratorName);
                    printedBegin = true;
                    startTime = System.nanoTime();
                }

                long localStartTime = System.nanoTime();
                boolean ans = iterator.hasNext();
                long localEndTime = System.nanoTime();
                long localTime = localEndTime - localStartTime;
                totalTime.setValue(totalTime.getValue() + localTime);
                maxTime.setValue(Math.max(maxTime.getValue(), localTime));

                if (!ans && !printedEnd) {
                    long endTime = System.nanoTime();
                    printStream.printf("[%d] Iterator [%d] %s exhausted. %d elements discovered.\n", endTime - startTime, signalingIteratorId, iteratorName, nElem);
                    // printStream.printf("Iterator %d statistics. Total time: %d, max time: %d\n", signalingIteratorId, totalTime.getValue(), maxTime.getValue());
                    printedEnd = true;
                }
                return ans;
            }

            @Override
            public T next() {
                if (!printedBegin) {
                    printStream.printf("Starting iterator %s.\n", iteratorName);
                    printedBegin = true;
                    startTime = System.nanoTime();
                }
                nElem++;

                long localStartTime = System.nanoTime();
                T ans = iterator.next();
                long localEndTime = System.nanoTime();
                long localTime = localEndTime - localStartTime;
                totalTime.setValue(totalTime.getValue() + localTime);
                maxTime.setValue(Math.max(maxTime.getValue(), localTime));

                return ans;
            }

            private boolean printedEnd = false;
            private boolean printedBegin = false;
            private int nElem = 0;
            private long startTime;
        };
    }

    public static <T> Iterator<T> printingIterator(final Iterator<T> iterator,
            final String iteratorName, final PrintStream printStream) {
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public T next() {
                T ans = iterator.next();
                System.out.printf("Iterator %s returns %s\n", iteratorName, ans);
                return ans;
            }
        };
    }

    public static <T> Iterator<T> idIterator(final Iterator<T> in, final Predicate<T> predicate) {
        return new Iterator<T>() {
            @Override
            public boolean hasNext() {
                return in.hasNext();
            }

            @Override
            public T next() {
                T t = in.next();
                predicate.apply(t);
                return t;
            }
        };
    }

}
