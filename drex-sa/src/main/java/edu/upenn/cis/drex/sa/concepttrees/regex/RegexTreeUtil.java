package edu.upenn.cis.drex.sa.concepttrees.regex;

import automata.sfa.SFA;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.assertions.UnreachableCodeException;
import edu.upenn.cis.drex.sa.concepttrees.regex.RegexTree.EmptyRegexTree;
import edu.upenn.cis.drex.sa.concepttrees.regex.RegexTree.RegexRegexTree;
import edu.upenn.cis.drex.sa.concepttrees.regex.RegexTree.SplitRegexTree;
import java.util.Stack;
import javax.annotation.Nullable;
import theory.CharPred;
import theory.CharSolver;

public class RegexTreeUtil {

    public static Optional<RegexTree> sieve(RegexTree tree, Regex regex) {
        checkNotNull(tree);
        checkNotNull(regex);

        SFA<CharPred, Character> sfa = checkNotNull(regex.getSFA());
        Stack<SieveStackElement> sieveStack = new Stack();
        CharSolver solver = new CharSolver();

        while (tree instanceof SplitRegexTree) {
            SplitRegexTree splitTree = (SplitRegexTree)tree;
            if (sfa.accepts(splitTree.splitStrArray, solver)) {
                sieveStack.push(new SieveStackElement(splitTree.splitStr, null,
                        splitTree.negTree));
                tree = splitTree.posTree;
            } else {
                sieveStack.push(new SieveStackElement(splitTree.splitStr,
                        splitTree.posTree, null));
                tree = splitTree.negTree;
            }
        }

        RegexTree ans = null;
        if (tree instanceof EmptyRegexTree) {
            ans = new RegexRegexTree(regex);
        } else if (tree instanceof RegexRegexTree) {
            Regex treeRegex = ((RegexRegexTree)tree).regex;
            Optional<SplitRegexTree> optAns = SplitRegexTree.of(treeRegex, regex);
            if (!optAns.isPresent()) {
                return Optional.absent();
            } else {
                ans = optAns.get();
            }
        } else {
            throw new UnreachableCodeException();
        }
        checkAssertion(ans != null);

        while (!sieveStack.empty()) {
            ans = sieveStack.pop().build(ans);
        }
        return Optional.of(ans);
    }

}

class SieveStackElement {

    public SieveStackElement(String splitStr, @Nullable RegexTree posTree,
            @Nullable RegexTree negTree) {
        this.splitStr = checkNotNull(splitStr);
        checkArgument(posTree != null || negTree != null);
        checkArgument(posTree == null || negTree == null);
        this.posTree = posTree;
        this.negTree = negTree;
    }

    public SplitRegexTree build(RegexTree tree) {
        checkNotNull(tree);
        if (posTree == null) {
            return new SplitRegexTree(splitStr, tree, negTree);
        } else {
            return new SplitRegexTree(splitStr, posTree, tree);
        }
    }

    public final String splitStr;
    public final @Nullable RegexTree posTree;
    public final @Nullable RegexTree negTree;

}
