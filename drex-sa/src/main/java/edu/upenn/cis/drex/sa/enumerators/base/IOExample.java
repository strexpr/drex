package edu.upenn.cis.drex.sa.enumerators.base;

import com.google.common.base.Objects;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.dp.DP;
import edu.upenn.cis.drex.core.expr.Expression;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.sa.util.StringUtil;
import javax.annotation.Nullable;
import org.apache.commons.lang3.tuple.ImmutablePair;

public class IOExample {

    private IOExample(String input, @Nullable String output, boolean defined) {
        checkNotNull(input);

        this.input = input;
        this.output = output;
        this.defined = defined;
    }

    @Override
    public String toString() {
        String inputStr = String.format("\"%s\" (%d)", input, input.length());
        if (outputKnown()) {
            String outputStr = String.format("\"%s\" (%d)", output, output.length());
            return String.format("[%s -> %s]", inputStr, outputStr);
        } else if (defined) {
            return String.format("[%s -> defined]", inputStr);
        } else {
            return String.format("[%s -> undefined]", inputStr);
        }
    }

    public boolean outputKnown() {
        return output != null;
    }

    public String getOutput() {
        checkState(this.output != null);
        return output;
    }

    public static IOExample io(String input, String output) {
        return new IOExample(checkNotNull(input), checkNotNull(output), true);
    }

    public static IOExample definedOn(String input) {
        return defined(input, true);
    }

    public static IOExample undefinedOn(String input) {
        return defined(input, false);
    }

    public static IOExample defined(String input, boolean defined) {
        return new IOExample(checkNotNull(input), null, defined);
    }

    public boolean matches(Expression expr) {
        String actualOutput = DP.eval(checkNotNull(expr), this.input);

        if (defined) {
            String expectedOutput = output != null ? output : actualOutput;
            return Objects.equal(actualOutput, expectedOutput);
        } else {
            return actualOutput == null;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof IOExample) {
            IOExample ioObj = (IOExample)obj;
            return Objects.equal(this.input, ioObj.input) &&
                    Objects.equal(this.output, ioObj.output) &&
                    Objects.equal(this.defined, ioObj.defined);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(IOExample.class, defined, input, output);
    }
    
    private ImmutableList<ImmutablePair<IOExample, IOExample>> splitInternal(
            boolean invert,
            boolean skipLeftInputEpsilons,
            boolean skipOutputs
    ) {
        checkAssertion(skipOutputs || this.outputKnown());
        ImmutableList.Builder<ImmutablePair<IOExample, IOExample>> splits = ImmutableList.builder();

        ImmutableList<ImmutablePair<String, String>> inputSplits = !skipLeftInputEpsilons ?
                StringUtil.getSplits(input) : StringUtil.getLeftNonEmptySplits(input);

        if (skipOutputs) {
            for (ImmutablePair<String, String> inputSplit : inputSplits) {
                splits.add(ImmutablePair.of(
                    IOExample.defined(inputSplit.left, this.defined),
                    IOExample.defined(inputSplit.right, this.defined)
                ));
            }
            return splits.build();
        }
        
        ImmutableList<ImmutablePair<String, String>> outputSplits = !invert ? 
                StringUtil.getSplits(output) : StringUtil.getReverseSplits(output);
        for (ImmutablePair<String, String> inputSplit : inputSplits) {
            for (ImmutablePair<String, String> outputSplit : outputSplits) {
                splits.add(ImmutablePair.of(
                        IOExample.io(inputSplit.left, outputSplit.left), 
                        IOExample.io(inputSplit.right, outputSplit.right)
                        ));
            }
        }
        return splits.build();
    }

    public ImmutableList<ImmutablePair<IOExample, IOExample>> outputSplits() {
        checkAssertion(this.outputKnown());
        ImmutableList.Builder<ImmutablePair<IOExample, IOExample>> splits = ImmutableList.builder();
        
        ImmutableList<ImmutablePair<String, String>> outputSplits =
                StringUtil.getSplits(output);
        
        for (ImmutablePair<String, String> outputSplit : outputSplits) {
            splits.add(ImmutablePair.of(
                    IOExample.io(input, outputSplit.left), 
                    IOExample.io(input, outputSplit.right)
                    ));
        }
        return splits.build();
    }

    
    public ImmutableList<ImmutablePair<IOExample, IOExample>> splits() {
        return this.splitInternal(false, false, false);
    }

    public ImmutableList<ImmutablePair<IOExample, IOExample>> reverseSplits() {
        return this.splitInternal(true, false, false);
    }

    public ImmutableList<ImmutablePair<IOExample, IOExample>> leftNonEmptySplits() {
        return this.splitInternal(false, true, false);
    }

    public ImmutableList<ImmutablePair<IOExample, IOExample>> leftNonEmptyReverseSplits() {
        return this.splitInternal(true, true, false);
    }

    public ImmutableList<ImmutablePair<IOExample, IOExample>> leftNonEmptyInputOnlySplits() {
        return this.splitInternal(false, true, true);
    }

    public ImmutableList<ImmutablePair<IOExample, IOExample>> inputOnlySplits() {
        return this.splitInternal(false, false, true);
    }

    public ImmutableList<ImmutablePair<IOExample, IOExample>> reverseInputOnlySplits() {
        return this.splitInternal(true, false, true);
    }

    public final String input;
    private final @Nullable String output;
    public final boolean defined;

}
