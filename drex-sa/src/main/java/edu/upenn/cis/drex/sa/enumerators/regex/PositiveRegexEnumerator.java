package edu.upenn.cis.drex.sa.enumerators.regex;

import static com.google.common.base.Preconditions.checkArgument;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import java.util.Iterator;
import java.util.NoSuchElementException;
import theory.CharPred;

public class PositiveRegexEnumerator implements Iterator<Regex> {

    public PositiveRegexEnumerator(PositiveRegexPool pool) {
        this.pool = checkNotNull(pool);
        this.size = 0;
        this.sizeSpecificIterator = pool.ofSize(size).iterator();
        this.emptyRun = 0;
    }

    @Override
    public boolean hasNext() {
        // TODO! This is flaky!
        while (!sizeSpecificIterator.hasNext()) {
            size++;
            sizeSpecificIterator = pool.ofSize(size).iterator();
            emptyRun++;

            int sumSizes = 1;
            for (Regex regex : pool.poolGroup.baseRegexes) {
                sumSizes += regex.size;
            }

            if (2 * sumSizes < size && size < 2 * emptyRun) {
                return false;
            }
        }

        emptyRun = 0;
        return true;
    }

    @Override
    public Regex next() {
        if (hasNext()) {
            return sizeSpecificIterator.next();
        } else {
            throw new NoSuchElementException();
        }
    }

    public final PositiveRegexPool pool;
    private /* mutable */ int size;
    private /* mutable */ Iterator<Regex> sizeSpecificIterator;
    private /* mutable */ int emptyRun;

    public static Iterator<Regex> ofExamples(ImmutableSet<String> positive,
            ImmutableSet<String> negative, CharPreds.BasePreds basePreds,
            boolean sorted, ImmutableCollection<Regex> baseRegexes) {
        checkNotNull(positive);
        checkArgument(!positive.contains(null));
        checkNotNull(negative);
        checkArgument(!negative.contains(null));
        checkNotNull(baseRegexes);
        checkArgument(!baseRegexes.contains(null));

        if (positive.size() == 0) {
            return RegexEnumerator.ofExamples(positive, negative, basePreds);
        }

        ImmutableSet.Builder<String> baseStrings = ImmutableSet.builder();
        baseStrings.addAll(positive);
        baseStrings.addAll(negative);
        ImmutableSet<CharPred> preds =
                CharPreds.makeBaseCharPredsFromStrings(baseStrings.build(),
                        basePreds);

        PositiveRegexPoolGroup poolGroup = new PositiveRegexPoolGroup(positive, preds, sorted, baseRegexes);
        String firstPositive = positive.iterator().next();
        PositiveRegexPool firstPool = poolGroup.getPool(firstPositive);
        Iterator<Regex> iterator = new PositiveRegexEnumerator(firstPool);

        iterator = RegexEnumeratorUtil.whichMatch(iterator, positive);
        iterator = RegexEnumeratorUtil.whichDontMatch(iterator, negative);

        PeekingIterator<Regex> ans = Iterators.peekingIterator(iterator);
        return ans;
    }

    public static Iterator<Regex> ofExamples2(ImmutableSet<String> positive,
            ImmutableSet<String> negative, CharPreds.BasePreds basePreds,
            boolean sorted) {
        checkNotNull(positive);
        checkArgument(!positive.contains(null));
        checkNotNull(negative);
        checkArgument(!negative.contains(null));

        if (positive.size() == 0) {
            return RegexEnumerator.ofExamples(positive, negative, basePreds);
        }

        ImmutableSet.Builder<String> baseStrings = ImmutableSet.builder();
        baseStrings.addAll(positive);
        baseStrings.addAll(negative);
        ImmutableSet<CharPred> preds =
                CharPreds.makeBaseCharPredsFromStrings(baseStrings.build(),
                        basePreds);

        ImmutableSet.Builder<Regex> baseRegexesBuilder = ImmutableSet.builder();
        for (CharPred charPred : preds) {
            Star charPredStar = new Star(new Symbol(charPred));
            if (charPredStar.getAmbiguity() == null) {
                baseRegexesBuilder.add(charPredStar);
            }
        }
        ImmutableSet baseRegexes = baseRegexesBuilder.build();

        PositiveRegexPoolGroup poolGroup = new PositiveRegexPoolGroup(positive, preds, sorted, baseRegexes);
        String firstPositive = positive.iterator().next();
        PositiveRegexPool firstPool = poolGroup.getPool(firstPositive);
        Iterator<Regex> iterator = new PositiveRegexEnumerator(firstPool);

        iterator = RegexEnumeratorUtil.whichMatch(iterator, positive);
        iterator = RegexEnumeratorUtil.whichDontMatch(iterator, negative);

        PeekingIterator<Regex> ans = Iterators.peekingIterator(iterator);
        return ans;
    }

}
