package edu.upenn.cis.drex.sa.util.comparators;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import java.util.List;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ListComparatorTest {

    @Test
    public void test1() {
        ListComparator<Integer> comparator = ListComparator.of();

        List<Integer> l0 = ImmutableList.of();
        List<Integer> l1 = ImmutableList.of(0, 1, 2, 3, 4);
        List<Integer> l2 = Lists.newArrayList(0, 1, 2, 3, 4, 5);
        List<Integer> l3 = ImmutableList.of(0, 1, 2, 3, 4, 5);
        List<Integer> l4 = ImmutableList.of(0, 1, 2, 3, 5);

        List<List<Integer>> ll = ImmutableList.of(l0, l1, l2, l3, l4);

        List<Integer> values = ImmutableList.of(0, 1, 2, 2, 4);

        for (int i = 0; i < ll.size(); i++) {
            for (int j = 0; j < ll.size(); j++) {
                int expected = values.get(i) - values.get(j);
                int result = comparator.compare(ll.get(i), ll.get(j));

                assertEquals(expected < 0, result < 0);
                assertEquals(expected == 0, result == 0);
                assertEquals(expected > 0, result > 0);
            }
        }
    }

}
