package edu.upenn.cis.drex.sa;

import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.ExpressionUtil;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.RegexUtil;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import edu.upenn.cis.drex.core.regex.Union;
import java.io.IOException;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import theory.CharOffset;
import theory.CharPred;
import theory.CharSolver;
import theory.StdCharPred;

public class MainTest {

    @Test
    public void test2() throws IOException, Exception {
        String cmd = "s/ab/cd/";
        ImmutableSet<String> inputs = ImmutableSet.of();

        CharSolver solver = new CharSolver();
        Regex pre = new Star(new Symbol(solver.True()));

        CharPred a = new CharPred('a');
        CharPred notA = solver.MkNot(a);
        CharPred b = new CharPred('b');
        CharPred notB = solver.MkNot(b);
        CharPred notANotB = solver.MkAnd(notA, notB);

        Regex as = new Symbol(a);
        Regex nas = new Symbol(notA);
        Regex notANotBS = new Symbol(notANotB);
        Regex asPlus = Star.plus(as);

        Regex l = new Star(new Union(nas, new Concat(asPlus, notANotBS)));
        Regex r = new Concat(l, asPlus);
        Regex post = new Union(l, r);
        ImmutableSet<ImmutablePair<Regex, Regex>> specs =
                ImmutableSet.of(ImmutablePair.of(pre, post));
        
        ImmutableSet<CharPred> charPreds = ImmutableSet.of(a, notA, b, notB,
                notANotB);
        ImmutableSet<Character> charConsts = ImmutableSet.of('a', 'b', 'c', 'd');
        ImmutableSet<CharOffset> charOffsets = ImmutableSet.of(new CharOffset(0));
        ImmutableSet<Expression> baseExprs = ImmutableSet.of(
                ExpressionUtil.stringToEpsilon("ab"),
                new edu.upenn.cis.drex.core.expr.Epsilon("cd"));

        ImmutableSet<Function<Expression, Expression>> exprTemplates = ImmutableSet.of();

        Main.loop(cmd, inputs, specs, charPreds, charConsts, charOffsets, baseExprs,
                exprTemplates);
    }

    @Test
    public void test4() throws IOException, Exception {
        String cmd = "s/log$//";
        ImmutableSet<String> inputs = ImmutableSet.of(// "8327999",
                "abclog"
        );

        CharPred truePred = StdCharPred.TRUE;
        CharPred lPred = new CharPred('l');
        CharPred oPred = new CharPred('o');
        CharPred gPred = new CharPred('g');

        Regex sigma = new Symbol(truePred);
        Regex sigmaStar = new Star(sigma);
        Regex sigmaPlus = Star.plus(sigma);
        Regex log = RegexUtil.stringToRegex("log");
        Regex sigmaPlusLog = Concat.of(sigmaPlus, log);

        ImmutableSet<ImmutablePair<Regex, Regex>> specs =
                ImmutableSet.of(ImmutablePair.of(sigmaPlus, sigmaPlus));

        Expression id = new IteratedSum(new edu.upenn.cis.drex.core.expr.Symbol(truePred, new CharOffset(0)));
        /* Expression wrap = new IteratedSum(new edu.upenn.cis.drex.core.expr.Symbol(p9, new CharConstant('0')));
        Expression plus1 = new edu.upenn.cis.drex.core.expr.Symbol(p08, new CharOffset(1));
        Expression leading = new IfElse(new SplitSum(leadingId, plus1), new Epsilon("1"));
        Expression full = new SplitSum(leading, wrap);
        System.out.printf("Target expression size: %d\n", full.size); */

        ImmutableSet<CharPred> charPreds = ImmutableSet.of(truePred, lPred, oPred, gPred);
        ImmutableSet<Character> charConsts = ImmutableSet.of();
        ImmutableSet<CharOffset> charOffsets = ImmutableSet.of(new CharOffset(0));
        ImmutableSet<Expression> baseExprs = ImmutableSet.of(id, ExpressionUtil.stringToEpsilon("log"));

        ImmutableSet<Function<Expression, Expression>> exprTemplates = ImmutableSet.of();

        Main.loop(cmd, inputs, specs, charPreds, charConsts, charOffsets, baseExprs,
                exprTemplates);
    }

}
