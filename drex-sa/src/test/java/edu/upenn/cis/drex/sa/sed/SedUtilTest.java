package edu.upenn.cis.drex.sa.sed;

import java.io.IOException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SedUtilTest {

    @Test
    public void test1() throws IOException {
        String cmd = "s/s/g/";
        String input = "ssb\nmsb";
        assertEquals("gsb\nmgb", SedUtil.getOutput(cmd, input));
    }

}
