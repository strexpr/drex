package edu.upenn.cis.drex.sa.enumerators.regex;

import edu.upenn.cis.drex.sa.enumerators.base.CharPreds;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.regex.Regex;
import java.util.Iterator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

public class PositiveRegexEnumeratorTest {

    @Test
    public void test3() {
        int n = 1;

        ImmutableSet.Builder<String> posSetBuilder = ImmutableSet.builder();
        ImmutableSet.Builder<String> negSetBuilder = ImmutableSet.builder();
        posSetBuilder.add("abi@ex.com");
        negSetBuilder.add("", "a", "b", "g", "m", "aa", "ab", "am", "om", "!", ".", "i");

        Iterator<Regex> enumerator = PositiveRegexEnumerator.ofExamples2(
                posSetBuilder.build(), negSetBuilder.build(),
                CharPreds.BasePreds.CHARS, true);
        enumerator = Iterators.limit(enumerator, n);
        ImmutableList<Regex> regexList = ImmutableList.copyOf(enumerator);

        assertEquals(regexList.size(), n);
        for (Regex r : regexList) {
            assertNotNull(r);
            System.out.println(r);
        }
    }

    @Test
    public void test4() {
        int n = 1;

        ImmutableSet.Builder<String> posSetBuilder = ImmutableSet.builder();
        ImmutableSet.Builder<String> negSetBuilder = ImmutableSet.builder();
        posSetBuilder.add("abi", "ABI");
        negSetBuilder.add("Abi", "aZ");

        Iterator<Regex> enumerator = PositiveRegexEnumerator.ofExamples2(
                posSetBuilder.build(), negSetBuilder.build(),
                CharPreds.BasePreds.STD, true);
        enumerator = Iterators.limit(enumerator, n);
        ImmutableList<Regex> regexList = ImmutableList.copyOf(enumerator);

        assertEquals(regexList.size(), n);
        for (Regex r : regexList) {
            assertNotNull(r);
            System.out.println(r);
        }
    }

}
