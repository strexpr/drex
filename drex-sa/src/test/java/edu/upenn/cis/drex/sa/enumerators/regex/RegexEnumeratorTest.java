package edu.upenn.cis.drex.sa.enumerators.regex;

import edu.upenn.cis.drex.sa.enumerators.base.CharPreds;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.sa.util.comparators.RegexComparator;
import java.util.Iterator;
import org.junit.Test;
import static org.junit.Assert.*;

public class RegexEnumeratorTest {

    @Test
    public void test1() {
        int n = 100;
        Iterator<Regex> enumerator = new RegexEnumerator(CharPreds.STD_CHARPREDS);
        enumerator = Iterators.limit(enumerator, n);
        ImmutableList<Regex> regexList = ImmutableList.copyOf(enumerator);

        assertEquals(regexList.size(), n);

        RegexComparator comparator = new RegexComparator();
        Regex prev = null;
        for (Regex r : regexList) {
            assertNotNull(r);
            if (prev != null) {
                assertTrue(comparator.compare(prev, r) < 0);
            }
            prev = r;
        }
    }

    @Test
    public void test2() {
        int n = 50;

        ImmutableSet<String> positiveExamples = ImmutableSet.of("aab@gmail.com");
        ImmutableSet<String> negativeExamples = ImmutableSet.of();

        ImmutableSet<CharPreds.BasePreds> basePredSet = ImmutableSet.of(CharPreds.BasePreds.NONE,
                CharPreds.BasePreds.STD, CharPreds.BasePreds.CHARS, CharPreds.BasePreds.CHARS_INCLUDING_NOT);
        for (CharPreds.BasePreds basePreds : basePredSet) {
            Iterator<Regex> enumerator = RegexEnumerator.ofExamples(
                    positiveExamples, negativeExamples, basePreds);
            enumerator = Iterators.limit(enumerator, n);
            ImmutableList<Regex> regexList = ImmutableList.copyOf(enumerator);

            if (basePreds != CharPreds.BasePreds.NONE) {
                assertEquals(regexList.size(), n);
            }

            for (Regex r : regexList) {
                assertNotNull(r);
            }
        }
    }

    @Test
    public void test3() {
        int n = 1;

        ImmutableSet.Builder<String> posSetBuilder = ImmutableSet.builder();
        ImmutableSet.Builder<String> negSetBuilder = ImmutableSet.builder();
        posSetBuilder.add("abc@ex.com");
        negSetBuilder.add("", "a", "b", "g", "m", "aa", "ab", "am", "om");

        Iterator<Regex> enumerator = RegexEnumerator.ofExamples(
                posSetBuilder.build(), negSetBuilder.build(),
                CharPreds.BasePreds.STD);
        enumerator = Iterators.limit(enumerator, n);
        ImmutableList<Regex> regexList = ImmutableList.copyOf(enumerator);

        assertEquals(regexList.size(), n);
        for (Regex r : regexList) {
            assertNotNull(r);
        }
    }

}
