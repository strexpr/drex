package edu.upenn.cis.drex.sa.enumerators.regex;

import edu.upenn.cis.drex.sa.enumerators.base.CharPreds;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.regex.Regex;
import org.junit.Test;

public class PositiveRegexPoolGroupTest {

    @Test
    public void test1() {
        ImmutableSet<String> posStrSet = ImmutableSet.of("abc@gmail.com",
                "ghi@upenn.edu", "def@ghi.com");
        PositiveRegexPoolGroup poolGroup = new PositiveRegexPoolGroup(posStrSet,
                CharPreds.STD_CHARPREDS, true, ImmutableSet.<Regex>of());
    }

}
