package edu.upenn.cis.drex.sa.concepttrees.regex;

import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import java.util.Iterator;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import theory.CharPred;

public class InequivalentRegexFilterTest {

    @Test
    public void InequivalentRegexFilterTest1() {
        InequivalentRegexFilter filter = new InequivalentRegexFilter();

        Regex r1 = new Symbol(CharPred.of(ImmutableList.of('a'))); // "a"
        Regex r2 = new Star(r1); // "a"*
        Regex r3 = new Concat(r1, r2); // "a"+
        Regex r4 = new Concat(r1, r3); // "a"2+
        Regex r5 = new Concat(r2, r1); // "a"+

        List<Regex> list1 = ImmutableList.of(r1, r2, r3, r4, r5);
        List<Regex> list2 = ImmutableList.of(r1, r2, r3, r4, r5);

        Iterator<Regex> f1 = filter.filter(list1.iterator());
        Iterator<Regex> f2 = filter.filter(list2.iterator());

        assertEquals(r1, f1.next());
        assertEquals(r2, f2.next());
        assertEquals(r3, f1.next());
        assertEquals(r4, f2.next());
        assertFalse(f1.hasNext());
        assertFalse(f2.hasNext());
    }

}
