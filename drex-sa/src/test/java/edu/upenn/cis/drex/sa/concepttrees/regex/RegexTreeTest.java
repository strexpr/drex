package edu.upenn.cis.drex.sa.concepttrees.regex;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import static org.junit.Assert.*;
import org.junit.Test;
import theory.CharPred;

public class RegexTreeTest {

    @Test
    public void regexTreeTest1() {
        RegexTree tree = RegexTree.EMPTY;

        Regex ra = new Symbol(CharPred.of(ImmutableList.of('a')));
        Optional<RegexTree> treePrime = tree.sieve(ra);
        assertTrue(treePrime.isPresent());
        tree = treePrime.get();

        Regex raStar = new Star(ra);
        treePrime = tree.sieve(raStar);
        assertTrue(treePrime.isPresent());
        tree = treePrime.get();

        Regex raPlus = new Concat(ra, raStar);
        treePrime = tree.sieve(raPlus);
        assertTrue(treePrime.isPresent());
        tree = treePrime.get();

        Regex raP2 = new Concat(ra, raPlus);
        treePrime = tree.sieve(raP2);
        assertTrue(treePrime.isPresent());
        tree = treePrime.get();

        Regex raPPrime = new Concat(raStar, ra);
        treePrime = tree.sieve(raPPrime);
        assertFalse(treePrime.isPresent());
    }

}
