package edu.upenn.cis.drex.sa.enumerators.expr;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import edu.upenn.cis.drex.sa.Main;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds.BasePreds;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.enumerators.templates.std.MultiGrammar;
import edu.upenn.cis.drex.sa.enumerators.templates.std.PosGrammar;
import edu.upenn.cis.drex.sa.sed.SedUtil;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import theory.CharConstant;
import theory.CharOffset;
import theory.CharPred;
import theory.CharSolver;
import theory.StdCharPred;

public class MultiEnumeratorTest {

    public static final boolean USE_MULTI_ENUMERATOR = true;

    @Test
    public void testPositiveStd() throws Exception {
        String cmd = "/^a*b*$/ y/ab/ba/";
        ImmutableSet<String> inputs = ImmutableSet.of("a");
        Set<IOExample> examples = new HashSet<>();
        for (String input : inputs) {
            examples.add(IOExample.io(input, SedUtil.getOutput(cmd, input)));
        }

        CharPred predA = new CharPred('a');
        CharPred predB = new CharPred('b');
        MultiGrammar G = MultiGrammar.build(BasePreds.CHARS_INCLUDING_NOT, examples, ImmutableSet.<Expression>of());

        Regex a = new edu.upenn.cis.drex.core.regex.Symbol(predA);
        Regex b = new edu.upenn.cis.drex.core.regex.Symbol(predB);
        Regex aStarbStar = new edu.upenn.cis.drex.core.regex.Concat(Star.plus(a), new Star(b));
        Regex bStaraStar = new edu.upenn.cis.drex.core.regex.Concat(Star.plus(b), new Star(a));
        ImmutableList<ImmutablePair<Regex, Regex>> specs = ImmutableList.of(ImmutablePair.of(aStarbStar, bStaraStar));

        Main.loop(cmd, inputs, specs, G, USE_MULTI_ENUMERATOR);
    }

    @Test
    public void testPositiveStd2() throws Exception {
        String cmd = "s/s/g/g";
        ImmutableSet<String> inputs = ImmutableSet.of();
        Set<IOExample> examples = new HashSet<>();
        for (String input : inputs) {
            examples.add(IOExample.io(input, SedUtil.getOutput(cmd, input)));
        }

        MultiGrammar G = MultiGrammar.build(BasePreds.CHARS_INCLUDING_NOT, examples, ImmutableSet.<Expression>of());

        CharPred predTrue = StdCharPred.TRUE;
        CharPred predS = new CharPred('s');
        CharPred predNotS = new CharSolver().MkNot(predS);
        Regex pre = new Star(new edu.upenn.cis.drex.core.regex.Symbol(predTrue));
        Regex post = new Star(new edu.upenn.cis.drex.core.regex.Symbol(predNotS));
        ImmutableList<ImmutablePair<Regex, Regex>> specs = ImmutableList.of(ImmutablePair.of(pre, post));

        Main.loop(cmd, inputs, specs, G, USE_MULTI_ENUMERATOR);
    }

    @Test
    public void testPositiveStd3() throws Exception {
        String cmd = "/[^0-9]/ d; :d s/9\\(_*\\)$/_\\1/; td; s/^\\(_*\\)$/1\\1/; tn\n" +
            "     s/8\\(_*\\)$/9\\1/; tn\n" +
            "     s/7\\(_*\\)$/8\\1/; tn\n" +
            "     s/6\\(_*\\)$/7\\1/; tn\n" +
            "     s/5\\(_*\\)$/6\\1/; tn\n" +
            "     s/4\\(_*\\)$/5\\1/; tn\n" +
            "     s/3\\(_*\\)$/4\\1/; tn\n" +
            "     s/2\\(_*\\)$/3\\1/; tn\n" +
            "     s/1\\(_*\\)$/2\\1/; tn\n" +
            "     s/0\\(_*\\)$/1\\1/; tn\n" +
            ":n\n" +
            "y/_/0/";
        ImmutableSet<String> inputs = ImmutableSet.of();
        Set<IOExample> examples = new HashSet<>();
        for (String input : inputs) {
            examples.add(IOExample.io(input, SedUtil.getOutput(cmd, input)));
        }

        CharPred p09 = new CharPred('0', '9');
        CharPred p0 = new CharPred('0');
        CharPred p19 = new CharPred('1', '9');
        CharPred p08 = new CharPred('0', '8');
        CharPred p9 = new CharPred('9');

        Expression wrap = new IteratedSum(new edu.upenn.cis.drex.core.expr.Symbol(p9, new CharConstant('0')));
        Expression leadingId = new IteratedSum(new edu.upenn.cis.drex.core.expr.Symbol(p09, new CharOffset(0)));

        MultiGrammar G = MultiGrammar.build(BasePreds.CHARS_INCLUDING_NOT, examples, ImmutableSet.<Expression>of(wrap, leadingId));

        Regex p0s = new Symbol(p0);
        Regex starP0s = new Star(p0s);

        Regex pre = Star.plus(new Symbol(p09));
        Regex post = Concat.of(starP0s, new Symbol(p19), new Star(new Symbol(p09)));
        ImmutableSet<ImmutablePair<Regex, Regex>> specs = ImmutableSet.of(ImmutablePair.of(pre, post));

        Main.loop(cmd, inputs, specs, G, USE_MULTI_ENUMERATOR);
    }

    /*
    @Test
    public void test2() {
        int n = 5;

        ImmutableSet<InputOutputExample> examples = ImmutableSet.of(InputOutputExample.io("a", "a"),
                InputOutputExample.io("A", "a"),
                InputOutputExample.io("aa", "aa"),
                InputOutputExample.io("AA", "aa"),
                InputOutputExample.io("Aa", "aa"),
                InputOutputExample.io("A.A", "a.a"));

        ImmutableSet<CharPreds.BasePreds> basePredSet = ImmutableSet.of(
                CharPreds.BasePreds.NONE,
                CharPreds.BasePreds.STD,
                CharPreds.BasePreds.CHARS,
                CharPreds.BasePreds.CHARS_INCLUDING_NOT);
        for (CharPreds.BasePreds basePreds : basePredSet) {
            System.out.println(basePreds);

            Iterator<Expression> enumerator = PosExprEnumerator.ofExamples(examples,
                    basePreds, true, true, false);
            enumerator = Iterators.limit(enumerator, n);
            ImmutableList<Expression> exprList = ImmutableList.copyOf(enumerator);

            if (basePreds != CharPreds.BasePreds.NONE) {
                assertEquals(n, exprList.size());
            }

            if (!exprList.isEmpty()) {
                System.out.printf("%s: %d\n", exprList.get(0), exprList.get(0).size);
            }

            for (Expression expr : exprList) {
                assertNotNull(expr);
            }
        }
    }

    @Test
    public void test3() {
        int n = 1;

        ImmutableSet<InputOutputExample> examples =
                ImmutableSet.of(InputOutputExample.io("abc@ex.com", "ex.com"),
                        InputOutputExample.io("abi@ex.com", "ex.com"),
                        InputOutputExample.io("ab09@ex.com", "ex.com"));

        boolean sorted = true;
        boolean unions = false;
        boolean restrict = true;

        ImmutableSet<CharPreds.BasePreds> basePredSet = ImmutableSet.of(
                // CharPreds.BasePreds.NONE,
                CharPreds.BasePreds.STD);
                // CharPreds.BasePreds.CHARS,
                // CharPreds.BasePreds.CHARS_INCLUDING_NOT);
        for (CharPreds.BasePreds basePreds : basePredSet) {
            System.out.printf("basePreds: %s\n", basePreds);
            System.out.printf("sorted: %s\n", sorted);
            System.out.printf("unions: %s\n", unions);
            System.out.printf("restrict: %s\n", restrict);

            Iterator<Expression> enumerator = PosExprEnumerator.ofExamples(examples,
                    basePreds, sorted, unions, restrict);
            enumerator = Iterators.limit(enumerator, n);
            ImmutableList<Expression> exprList = ImmutableList.copyOf(enumerator);

            if (basePreds != CharPreds.BasePreds.NONE) {
                assertEquals(n, exprList.size());
            }

            if (!exprList.isEmpty()) {
                System.out.printf("%s: %d\n", exprList.get(0), exprList.get(0).size);
            }

            for (Expression expr : exprList) {
                assertNotNull(expr);
            }
        }
    }

    @Test
    public void test4() {
        int n = 1;

        ImmutableSet<InputOutputExample> examples = ImmutableSet.of(InputOutputExample.io("abc@ex.com", "ex.com"));

        boolean sorted = true;
        boolean unions = true;
        boolean restrict = true;

        System.out.printf("sorted: %s\n", sorted);
        System.out.printf("unions: %s\n", unions);
        System.out.printf("restrict: %s\n", restrict);

        CharSolver solver = new CharSolver();
        CharPred charPredAt = new CharPred('@');
        CharPred charPredNotAt = solver.MkNot(charPredAt);
        ImmutableSet<CharPred> charPreds = ImmutableSet.of(solver.True(), charPredAt, charPredNotAt);

        ImmutableSet<Character> constants = ImmutableSet.of();

        ImmutableSet<CharOffset> offsets = ImmutableSet.of(new CharOffset(0));

        Iterator<Expression> enumerator = PosExprEnumerator.ofExamples(examples,
                charPreds, constants, offsets, sorted, unions, restrict,
                ImmutableSet.<Expression>of(),
                ImmutableSet.<Function<Expression, Expression>>of());
        enumerator = Iterators.limit(enumerator, n);
        ImmutableList<Expression> exprList = ImmutableList.copyOf(enumerator);

        assertEquals(n, exprList.size());

        if (!exprList.isEmpty()) {
            System.out.printf("%s: %d\n", exprList.get(0), exprList.get(0).size);
        }

        for (Expression expr : exprList) {
            assertNotNull(expr);
        }
    }
    */
}
