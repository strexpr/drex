package edu.upenn.cis.drex.sa.sed;

import java.io.IOException;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class SedParserTest {

    public void testSedProgram(String program)
        throws Exception
    {
        SedParser parser = new SedParser(program);
        parser.extract();
    }

    @Test
    public void testProgram1()
        throws Exception
    {
        String program = "y/abc/def/";
        testSedProgram(program);
    }

    @Test
    public void testProgram2()
        throws Exception
    {
        String program = "# Comment comment";
        testSedProgram(program);
    }

    @Test
    public void testProgram3()
        throws Exception
    {
        String program = "a Random text";
        testSedProgram(program);
    }

    @Test
    public void testProgram4()
        throws Exception
    {
        String program = "s/a\\(b\\)c*/def/g";
        testSedProgram(program);
    }

    @Test
    public void testProgram5()
        throws Exception
    {
        String program = "s/a\\|b/def/g";
        testSedProgram(program);
    }

    @Test
    public void testProgram6()
        throws Exception
    {
        String program = "s/[abc]/def/g";
        testSedProgram(program);
    }

    @Test
    public void testProgram7()
        throws Exception
    {
        String program = "s/[a-c]/def/g";
        testSedProgram(program);
    }

    @Test
    public void testProgram8()
        throws Exception
    {
        String program = "s/[^a-z0-93-4]/def/g";
        testSedProgram(program);
    }

    @Test
    public void testProgram9()
        throws Exception
    {
        String program = "s/[^[:alnum:]]/def/g";
        testSedProgram(program);
    }
}
