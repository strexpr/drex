package edu.upenn.cis.drex.sa.enumerators.expr;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Iterators;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.sa.enumerators.base.CharFuncs;
import edu.upenn.cis.drex.sa.enumerators.base.CharPreds;
import edu.upenn.cis.drex.sa.enumerators.base.IOExample;
import edu.upenn.cis.drex.sa.util.comparators.ExpressionComparator;
import java.util.Iterator;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ExprEnumeratorTest {

    @Test
    public void test1() {
        int n = 1000;
        Iterator<Expression> enumerator = new BlindExprEnumerator(CharPreds.STD_CHARPREDS,
                ImmutableSet.<Character>of(),
                CharFuncs.STD_OFFSETS,
                ImmutableList.<String>of());
        enumerator = Iterators.limit(enumerator, n);
        ImmutableList<Expression> exprList = ImmutableList.copyOf(enumerator);

        assertEquals(exprList.size(), n);

        ExpressionComparator comparator = new ExpressionComparator();
        Expression prev = null;
        for (Expression expr : exprList) {
            assertNotNull(expr);
            if (prev != null) {
                assertTrue(comparator.compare(prev, expr) < 0);
            }
            prev = expr;
        }
    }

    @Test
    public void test2() {
        int n = 5;

        ImmutableSet<IOExample> examples = ImmutableSet.of(IOExample.io("a", "a"),
                IOExample.io("b", "b"));

        ImmutableSet<CharPreds.BasePreds> basePredSet = ImmutableSet.of(CharPreds.BasePreds.NONE,
                CharPreds.BasePreds.STD, CharPreds.BasePreds.CHARS, CharPreds.BasePreds.CHARS_INCLUDING_NOT);
        for (CharPreds.BasePreds basePreds : basePredSet) {
            Iterator<Expression> enumerator = BlindExprEnumerator.ofExamples(examples, basePreds);
            enumerator = Iterators.limit(enumerator, n);
            ImmutableList<Expression> exprList = ImmutableList.copyOf(enumerator);

            if (basePreds != CharPreds.BasePreds.NONE) {
                assertEquals(exprList.size(), n);
            }

            for (Expression expr : exprList) {
                assertNotNull(expr);
            }
        }
    }

}
