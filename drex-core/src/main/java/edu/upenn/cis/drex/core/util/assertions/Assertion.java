package edu.upenn.cis.drex.core.util.assertions;

public class Assertion {

    public static void checkAssertion(boolean condition) throws AssertionViolation {
        if (!condition) {
            throw new AssertionViolation();
        }
    }

    public static void checkAssertion(boolean condition, Object msg) throws AssertionViolation {
        if (!condition) {
            throw new AssertionViolation(msg);
        }
    }

    public static <T> T assertNotNull(T t) throws AssertionViolation {
        checkAssertion(t != null);
        return t;
    }

    public static <T> T assertNotNull(T t, Object msg) throws AssertionViolation {
        checkAssertion(t != null, msg);
        return t;
    }

}
