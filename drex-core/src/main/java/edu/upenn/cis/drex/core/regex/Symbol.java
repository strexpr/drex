package edu.upenn.cis.drex.core.regex;

import edu.upenn.cis.drex.core.domaintype.RegexAmbiguity;
import automata.sfa.SFAInputMove;
import automata.sfa.SFA;
import automata.sfa.SFAMove;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import edu.upenn.cis.drex.core.domaintype.RegexDomain;
import static edu.upenn.cis.drex.core.regex.Regex.RegexType.SYMBOL;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import theory.CharPred;
import theory.CharSolver;

public class Symbol extends Regex {

    public Symbol(CharPred a) {
        super(getSuperArgs(checkNotNull(a)));
        this.a = checkNotNull(a);
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> rv) {
        return checkNotNull(rv).visitSymbol(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Symbol) {
            return Objects.equals(a, ((Symbol)obj).a);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Symbol.class, a);
    }

    public final CharPred a;

    public static ConstructorArgs getSuperArgs(final CharPred a) {
        checkNotNull(a);
        Supplier<RegexDomain> domainSupplier = new Supplier<RegexDomain>() {
            @Override
            public RegexDomain get() {
                return new RegexDomain(getSFA(a), getAmbiguity(a));
            }
        };
        return new ConstructorArgs(domainSupplier, 1, SYMBOL);
    }

    public static SFA<CharPred, Character> getSFA(CharPred a) {
        SFAMove<CharPred, Character> transition = new SFAInputMove<>(0, 1, checkNotNull(a));
        List<SFAMove<CharPred, Character>> transitions = Arrays.asList(transition);
        Integer intState = 0;
        List<Integer> finStates = Arrays.asList(1);
            return SFA.MkSFA(transitions, intState, finStates, new CharSolver());

    }

    public static RegexAmbiguity getAmbiguity(CharPred a) {
        CharSolver solver = new CharSolver();
        if (!solver.IsSatisfiable(checkNotNull(a))) {
            String errorMsg = String.format("Unsatisfiable predicate %s.", a);
            return RegexAmbiguity.simpleErrorOf(errorMsg);
        } else {
            return null;
        }
    }

}
