package edu.upenn.cis.drex.core.domaintype;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.regex.Regex;
import javax.annotation.Nullable;

public class DomainTypeAnalysisResult {

    public DomainTypeAnalysisResult(Regex domainType, @Nullable ImmutableSet<Integer> domainStringLengths) {
        checkArgument(domainType != null && domainType.getSFA() != null && domainType.getAmbiguity() == null);
        this.domainType = domainType;
        this.domainTypeError = null;
        this.domainStringLengths = domainStringLengths;
    }

    public DomainTypeAnalysisResult(DomainTypeError domainTypeError,
            @Nullable ImmutableSet<Integer> domainStringLengths) {
        this.domainType = null;
        this.domainTypeError = checkNotNull(domainTypeError);
        this.domainStringLengths = domainStringLengths;
    }

    public final @Nullable Regex domainType;
    public final @Nullable DomainTypeError domainTypeError;
    public final @Nullable ImmutableSet<Integer> domainStringLengths;

}
