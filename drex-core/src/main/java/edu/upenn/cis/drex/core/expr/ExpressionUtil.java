package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableCollection;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Empty;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.RegexUtil;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Union;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.assertions.UnreachableCodeException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.tuple.ImmutablePair;
import theory.CharConstant;
import theory.CharFunc;
import theory.CharOffset;
import theory.CharPred;
import theory.CharSolver;
import theory.StdCharPred;

public abstract class ExpressionUtil {

    /**
     * Returns a constant function f with the same domain as e, such that
     * f(s) = "", for all s such that e(s) is defined.
     * @param e
     * @return
     */
    public static Expression silence(Expression e) {
        SilenceVisitor visitor = new SilenceVisitor();
        return visitor.visit(checkNotNull(e));
    }

    public static Regex expressionToRegex(Expression e) {
        checkArgument(e != null);
        ExprRegexTranslator visitor = new ExprRegexTranslator();
        Regex ans = visitor.visit(e);
        return ans;
    }


    /**
     * Returns the function f which maps individual characters satisfying the
     * predicate pred to themselves, and is undefined for all other strings.
     * @param pred
     * @return
     */
    public static Expression predToSelf(CharPred pred, CharSolver solver) {
        return new Symbol(checkNotNull(pred), CharOffset.IDENTITY);
    }

    /**
     * Returns the function f which maps the input string str to itself, and is
     * undefined for all other strings.
     * @param str
     * @return
     */
    public static Expression stringToSelf(String str, CharSolver solver) {
        Expression[] eList = new Expression[checkNotNull(str).length()];
        for (int i = 0; i < str.length(); i++) {
            char str_i = str.charAt(i);
            eList[i] = predToSelf(new CharPred(str_i), solver);
        }
        return SplitSum.of(eList);
    }

    /**
     * Returns the function f which maps each string str in strList to itself,
     * and is undefined for all other strings.
     * @param strList
     * @return
     */
    public static Expression allStringsToSelves(ImmutableList<String> strList, CharSolver solver) {
        checkNotNull(strList);
        Expression[] eList = new Expression[strList.size()];
        for (int i = 0; i < strList.size(); i++) {
            eList[i] = stringToSelf(strList.get(i), solver);
        }
        return IfElse.of(eList);
    }

    /**
     * Returns the identity function f over strings of characters, where each
     * character satisfies the predicate pred. The function f is undefined for
     * all other strings.
     * @param pred
     * @return
     */
    public static Expression id(CharPred pred, CharSolver solver) {
        return new IteratedSum(predToSelf(checkNotNull(pred), solver));
    }

    /**
     * Returns the identity function f over non-empty strings of characters,
     * where each character satisfies the predicate pred. The function f is
     * undefined for all other strings.
     * @param pred
     * @return
     */
    public static Expression idPlus(CharPred pred, CharSolver solver) {
        return IteratedSum.plus(new Symbol(checkNotNull(pred), CharOffset.IDENTITY));
    }

    /**
     * Returns the function f which maps non-empty strings, all of whose
     * characters satisfy the predicate pred, to the empty string. The function
     * f is undefined for all other strings.
     * @param pred
     * @return
     */
    public static Expression predToEpsilonPlus(CharPred pred) {
        return IteratedSum.plus(predToEpsilon(checkNotNull(pred)));
    }

    /**
     * Returns the function f which maps individual characters satisfying the
     * predicate pred to the empty string, and undefined for all other strings.
     * @param pred
     * @return
     */
    public static Expression predToEpsilon(CharPred pred) {
        ImmutableList<CharFunc> emptyList = ImmutableList.of();
        return new Symbol(checkNotNull(pred), emptyList);
    }

    /**
     * Returns the function f which maps the string str to the empty string
     * epsilon, and is undefined for all other strings.
     * @param str
     * @return
     */
    public static Expression stringToEpsilon(String str) {
        Expression[] eList = new Expression[checkNotNull(str).length()];
        for (int i = 0; i < str.length(); i++) {
            char str_i = str.charAt(i);
            eList[i] = predToEpsilon(new CharPred(str_i));
        }
        return SplitSum.of(eList);
    }

    /**
     * Returns the function f which maps each string str in the list strList
     * to the empty string epsilon, and is undefined for all other strings.
     * @param strList
     * @return
     */
    public static Expression allStringsToEpsilon(ImmutableList<String> strList) {
        checkNotNull(strList);
        Expression[] eList = new Expression[strList.size()];
        for (int i = 0; i < strList.size(); i++) {
            eList[i] = stringToEpsilon(strList.get(i));
        }
        return IfElse.of(eList);
    }

    /**
     * Given an expression e, which is a sum of split sums, returns the list
     * of component split sums. For example, on input ((e1.e2) + (e3.e4)) + (e5.e6),
     * this returns { (e1.e2), (e3.e4), (e5.e6) }.
     * @param e
     * @return List of the top-level split sums in e
     * @throws NullPointerException if e is null
     * @throws IllegalArgumentException if e is not a sum of split sums
     */
    public static ImmutableList<SplitSum> getTopLevelSplitSums(Expression e) {
        TopLevelSplitSumVisitor visitor = new TopLevelSplitSumVisitor();
        return ImmutableList.copyOf(visitor.visit(checkNotNull(e)));
    }

    /**
     * Returns the size of the DReX expression e.
     * @param e
     * @return 
     */
    public static int getSizeOf(Expression e) {
        SizeVisitor visitor = new SizeVisitor();
        return visitor.visit(checkNotNull(e));
    }

    /**
     * Returns the expression e which for each pair (p, q) in substs, replaces
     * every occurrence of the character p with q, and echoes the input
     * character everywhere else.
     * @param substs
     * @return
     */
    public static Expression sedY(ImmutableCollection<ImmutablePair<Character, Character>> substs) {
        checkNotNull(substs);
        checkArgument(!substs.contains(null));

        CharSolver solver = new CharSolver();
        CharPred rest = StdCharPred.TRUE;
        Expression ans = null;
        for (ImmutablePair<Character, Character> subst : substs) {
            char p = checkNotNull(subst.left);
            char q = checkNotNull(subst.right);

            CharPred pPred = new CharPred(p);
            Expression pq = new Symbol(pPred, new CharConstant(q));
            rest = solver.MkAnd(rest, solver.MkNot(pPred));
            ans = ans == null ? pq : new IfElse(ans, pq);
        }
        Expression eRest = new Symbol(rest, new CharOffset(0));
        ans = ans == null ? eRest : new IfElse(ans, eRest);
        return new IteratedSum(ans);
    }

    /**
     * Returns the expression e which for each pair (p, q) in substs, replaces
     * every occurrence of the character p with q, and echoes the input
     * character everywhere else.
     * @param substs
     * @return
     */
    public static Expression sedY(ImmutablePair<Character, Character>... substs) {
        ImmutableSet.Builder<ImmutablePair<Character, Character>> substsSetBuilder = ImmutableSet.builder();
        for (ImmutablePair<Character, Character> subst : substs) {
            substsSetBuilder.add(checkNotNull(subst));
        }
        return sedY(substsSetBuilder.build());
    }

}

class SilenceVisitor extends IVisitor<Expression> {

    @Override
    public Expression visitBottom(Bottom e) {
        return checkNotNull(e);
    }

    @Override
    public Expression visitSymbol(Symbol e) {
        ImmutableList<CharFunc> emptyList = ImmutableList.of();
        return new Symbol(checkNotNull(e).a, emptyList);
    }

    @Override
    public Expression visitEpsilon(Epsilon e) {
        return new Epsilon("");
    }

    @Override
    public Expression visitIfElse(IfElse e) {
        Expression se1 = ExpressionUtil.silence(checkNotNull(e).e1);
        Expression se2 = ExpressionUtil.silence(e.e2);
        checkAssertion(se1 != null && se2 != null);
        return new IfElse(se1, se2);
    }

    @Override
    public Expression visitSplitSum(SplitSum e) {
        Expression se1 = ExpressionUtil.silence(checkNotNull(e).e1);
        Expression se2 = ExpressionUtil.silence(e.e2);
        checkAssertion(se1 != null && se2 != null);
        return new SplitSum(se1, se2);
    }

    @Override
    public Expression visitSum(Sum e) {
        Expression se1 = ExpressionUtil.silence(checkNotNull(e).e1);
        Expression se2 = ExpressionUtil.silence(e.e2);
        checkAssertion(se1 != null && se2 != null);
        return new Sum(se1, se2);
    }

    @Override
    public Expression visitIteratedSum(IteratedSum e) {
        Expression se = ExpressionUtil.silence(e.e);
        checkAssertion(se != null);
        return new IteratedSum(se, e.left);
    }

    @Override
    public Expression visitChainedSum(ChainedSum e) {
        Expression se = ExpressionUtil.silence(checkNotNull(e).e);
        checkAssertion(se != null);
        return new ChainedSum(se, e.r, e.left);
    }

    @Override
    public Expression visitComposition(Composition e) {
        return new Composition(checkNotNull(e).first, ExpressionUtil.silence(e.second));
    }

    @Override
    public Expression visitRestrict(Restrict e) {
        return new Restrict(ExpressionUtil.silence(checkNotNull(e).e), e.r);
    }

}

class TopLevelSplitSumVisitor extends IVisitor<List<SplitSum>> {

    @Override
    public List<SplitSum> visitBottom(Bottom e) {
        throw new UnreachableCodeException();
    }

    @Override
    public List<SplitSum> visitSymbol(Symbol e) {
        throw new UnreachableCodeException();
    }

    @Override
    public List<SplitSum> visitEpsilon(Epsilon e) {
        throw new UnreachableCodeException();
    }

    @Override
    public List<SplitSum> visitIfElse(IfElse e) {
        throw new UnreachableCodeException();
    }

    @Override
    public List<SplitSum> visitSplitSum(SplitSum e) {
        return Arrays.asList(checkNotNull(e));
    }

    @Override
    public List<SplitSum> visitSum(Sum e) {
        List<SplitSum> ans = new ArrayList<>();
        ans.addAll(visit(checkNotNull(e).e1));
        ans.addAll(visit(e.e2));
        return ans;
    }

    @Override
    public List<SplitSum> visitIteratedSum(IteratedSum e) {
        throw new UnreachableCodeException();
    }

    @Override
    public List<SplitSum> visitChainedSum(ChainedSum e) {
        throw new UnreachableCodeException();
    }

    @Override
    public List<SplitSum> visitComposition(Composition e) {
        throw new UnreachableCodeException();
    }

    @Override
    public List<SplitSum> visitRestrict(Restrict e) {
        throw new UnreachableCodeException();
    }

}

class SizeVisitor extends IVisitor<Integer> {

    @Override
    public Integer visitBottom(Bottom e) {
        return 1;
    }

    @Override
    public Integer visitSymbol(Symbol e) {
        return 1;
    }

    @Override
    public Integer visitEpsilon(Epsilon e) {
        return 1;
    }

    @Override
    public Integer visitIfElse(IfElse e) {
        return 1 + ExpressionUtil.getSizeOf(e.e1) + ExpressionUtil.getSizeOf(e.e2);
    }

    @Override
    public Integer visitSplitSum(SplitSum e) {
        return 1 + ExpressionUtil.getSizeOf(e.e1) + ExpressionUtil.getSizeOf(e.e2);
    }

    @Override
    public Integer visitSum(Sum e) {
        return 1 + ExpressionUtil.getSizeOf(e.e1) + ExpressionUtil.getSizeOf(e.e2);
    }

    @Override
    public Integer visitIteratedSum(IteratedSum e) {
        return 1 + ExpressionUtil.getSizeOf(e.e);
    }

    @Override
    public Integer visitChainedSum(ChainedSum e) {
        return 1 + ExpressionUtil.getSizeOf(e.e) + RegexUtil.getSizeOf(e.r);
    }

    @Override
    public Integer visitComposition(Composition e) {
        return 1 + ExpressionUtil.getSizeOf(e.first) + ExpressionUtil.getSizeOf(e.second);
    }

    @Override
    public Integer visitRestrict(Restrict e) {
        return 1 + ExpressionUtil.getSizeOf(e.e) + RegexUtil.getSizeOf(e.r);
    }

}

class ExprRegexTranslator extends IVisitor<Regex> {

    @Override
    public Regex visitBottom(Bottom e) {
        return new Empty();
    }

    @Override
    public Regex visitSymbol(Symbol e) {
        return new edu.upenn.cis.drex.core.regex.Symbol(e.a);
    }

    @Override
    public Regex visitEpsilon(Epsilon e) {
        return new edu.upenn.cis.drex.core.regex.Epsilon();
    }

    @Override
    public Regex visitIfElse(IfElse e) {
        return new Union(visit(e.e1), visit(e.e2));
    }

    @Override
    public Regex visitSplitSum(SplitSum e) {
        return new Concat(visit(e.e1), visit(e.e2));
    }

    @Override
    public Regex visitSum(Sum e) {
        // Assuming that the sum is unambiguous
        return visit(e.e1);
    }

    @Override
    public Regex visitIteratedSum(IteratedSum e) {
        return new Star(visit(e.e));
    }

    @Override
    public Regex visitChainedSum(ChainedSum e) {
        return new Concat(visit(e.e), new Star(e.r));
    }

    @Override
    public Regex visitComposition(Composition e) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Regex visitRestrict(Restrict e) {
        return e.r;
    }

}
