package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import edu.upenn.cis.drex.core.regex.Regex;
import java.util.Objects;
import java.util.function.BiFunction;

public class Composition extends Expression {

    public Composition(Expression first, Expression second) {
        super(DomainTypeAnalyser.composition(checkNotNull(first), checkNotNull(second)),
                first.size + second.size + 1, ExpressionType.COMPOSE);
        this.first = first;
        this.second = second;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return checkNotNull(ev).visitComposition(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Composition) {
            Composition compObj = (Composition)obj;
            return Objects.equals(first, compObj.first) &&
                    Objects.equals(second, compObj.second);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Composition.class, first, second);
    }

    public final Expression first, second;
    
    public final static BiFunction<Expression, Expression, Expression> Composition
        = new BiFunction<Expression, Expression, Expression>() {

        @Override
        public Composition apply(Expression t, Expression u) {
            return new Composition(t, u);
        }
    };

}
