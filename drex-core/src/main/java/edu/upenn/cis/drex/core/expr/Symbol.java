package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import theory.CharPred;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import java.util.Objects;
import theory.CharConstant;
import theory.CharFunc;

public class Symbol extends Expression {

    public Symbol(CharPred a, ImmutableList<CharFunc> d) {
        super(DomainTypeAnalyser.symbol(checkNotNull(a)), 1, ExpressionType.SYMBOL);
        this.a = a;
        this.d = checkNotNull(d);
    }
    
    public Symbol(CharPred a, CharFunc d) {
        this(checkNotNull(a), ImmutableList.of(checkNotNull(d)));
    }
    
    public Symbol(CharPred a, String s) {
        super(DomainTypeAnalyser.symbol(checkNotNull(a)), 1, ExpressionType.SYMBOL);
        this.a = a;
        ImmutableList.Builder<CharFunc> l = ImmutableList.builder();
        for (char c : checkNotNull(s).toCharArray()) {
            l.add(new CharConstant(c));
        }
        this.d = l.build();
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return ev.visitSymbol(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Symbol) {
            Symbol symbObj = (Symbol)obj;
            return Objects.equals(a, symbObj.a) &&
                    Objects.equals(d, symbObj.d);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Symbol.class, a, d);
    }

    public final CharPred a;
    public final ImmutableList<CharFunc> d;

}
