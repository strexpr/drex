package edu.upenn.cis.drex.core.util;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;

public class Memo<T> implements Supplier<T> {

    public Memo(Supplier<T> supplier) {
        this.supplier = checkNotNull(supplier);
        this.invoked = false;
    }

    @Override
    public synchronized T get() {
        if (!invoked) {
            cache = supplier.get();
            invoked = true;
        }
        return cache;
    }

    public boolean isInvoked() {
        return invoked;
    }

    private final Supplier<T> supplier;
    private /* nonfinal */ boolean invoked;
    private /* nonfinal */ T cache;

}
