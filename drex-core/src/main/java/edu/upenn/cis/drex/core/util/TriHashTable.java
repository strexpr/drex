package edu.upenn.cis.drex.core.util;

import java.util.HashMap;
import org.apache.commons.lang3.tuple.ImmutableTriple;

public class TriHashTable<T1, T2, T3, Tval>
extends HashMap<ImmutableTriple<T1, T2, T3>, Tval> {

    public boolean containsKey(T1 t1, T2 t2, T3 t3) {
        ImmutableTriple<T1, T2, T3> key = ImmutableTriple.of(t1, t2, t3);
        return this.containsKey(key);
    }

    public Tval get(T1 t1, T2 t2, T3 t3) {
        ImmutableTriple<T1, T2, T3> key = ImmutableTriple.of(t1, t2, t3);
        return this.get(key);
    }

    public Tval put(T1 t1, T2 t2, T3 t3, Tval val) {
        ImmutableTriple<T1, T2, T3> key = ImmutableTriple.of(t1, t2, t3);
        Tval oldVal = this.containsKey(key) ? this.get(key) : null;
        this.put(key, val);
        return oldVal;
    }

}
