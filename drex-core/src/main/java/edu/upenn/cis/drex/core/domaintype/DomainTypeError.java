package edu.upenn.cis.drex.core.domaintype;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.expr.Expression;

public abstract class DomainTypeError {

    public DomainTypeError(String errorMsg) {
        this.errorMsg = checkNotNull(errorMsg);
    }

    public static DomainTypeError simpleErrorOf(String errorMsg) {
        return new DomainTypeErrorSimple(checkNotNull(errorMsg));
    }

    public static DomainTypeError untypedSubexpression(Expression expr) {
        return new DomainTypeErrorSubexpression(checkNotNull(expr));
    }

    public final String errorMsg;

}

class DomainTypeErrorSimple extends DomainTypeError {

    public DomainTypeErrorSimple(String errorMsg) {
        super(checkNotNull(errorMsg));
    }

}

class DomainTypeErrorSubexpression extends DomainTypeError {

    public DomainTypeErrorSubexpression(Expression expr) {
        super(buildErrorMsg(checkNotNull(expr)));
        this.expr = expr;
    }

    private static String buildErrorMsg(Expression expr) {
        checkNotNull(expr);
        checkArgument(expr.getDomainTypeError() != null);
        String ans = String.format("Ill-typed subexpression %s.\n%s", expr,
                expr.getDomainTypeError().errorMsg);
        return ans;
    }

    public final Expression expr;

}
