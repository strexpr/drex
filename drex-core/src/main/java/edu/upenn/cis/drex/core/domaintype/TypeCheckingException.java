package edu.upenn.cis.drex.core.domaintype;

public class TypeCheckingException extends Exception {

    public TypeCheckingException(DomainTypeError domainTypeError) {
        super(domainTypeError.errorMsg);
        this.domainTypeError = domainTypeError;
    }

    public TypeCheckingException(DomainTypeError domainTypeError, String errorMsg) {
        super(errorMsg);
        this.domainTypeError = domainTypeError;
    }

    public final DomainTypeError domainTypeError;

}
