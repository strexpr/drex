package edu.upenn.cis.drex.core.domaintype;

import edu.upenn.cis.drex.core.regex.Regex;
import static com.google.common.base.Preconditions.checkNotNull;

public class RegexAmbiguity {

    private RegexAmbiguity(String errorMsg) {
        this.errorMsg = checkNotNull(errorMsg);
    }

    public final String errorMsg;

    public static RegexAmbiguity simpleErrorOf(String errorMsg) {
        return new RegexAmbiguity(checkNotNull(errorMsg));
    }

    public static RegexAmbiguity ofAmbiguousSubexpression(Regex r) {
        return new SubexpressionAmbiguity(checkNotNull(r));
    }

    public static class SubexpressionAmbiguity extends RegexAmbiguity {

        public SubexpressionAmbiguity(Regex r) {
            super(buildErrorMsg(checkNotNull(r)));
            this.r = r;
        }

        private static String buildErrorMsg(Regex r) {
            checkNotNull(r);
            checkNotNull(r.getAmbiguity());
            String ans = String.format("Ill-typed subexpression %s.\n%s", r, r.getAmbiguity().errorMsg);
            return ans;
        }

        public final Regex r;

    }

}
