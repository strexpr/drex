package edu.upenn.cis.drex.core.forkjoin.response;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.forkjoin.Evaluator;

public abstract class Response {

    public Response(Evaluator origin, int startIndex) {
        this.origin = checkNotNull(origin);
        checkArgument(0 <= startIndex);
        this.startIndex = startIndex;
    }

    @Override
    public abstract String toString();

    public abstract <T> T invokeVisitor(IVisitor<T> visitor);

    public final Evaluator origin;
    public final int startIndex;

}
