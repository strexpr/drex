package edu.upenn.cis.drex.core.util.function;

import com.google.common.base.Function;

public abstract class BiFunction<T, U, R> {

    public abstract R apply(T t, U u);

    public Function<U, R> partialApply(final T t) {
        return new Function<U, R> () {
            @Override
            public R apply(U u) {
                return BiFunction.this.apply(t,u);
            }
        };
    }
    
    public Function<T, R> partialApplyRight(final U u) {
        return new Function<T, R> () {
            @Override
            public R apply(T t) {
                return BiFunction.this.apply(t,u);
            }
        };
    }
}
