package edu.upenn.cis.drex.core.expr;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import java.util.Objects;

public class Epsilon extends Expression {

    public Epsilon(final String d) {
        super(DomainTypeAnalyser.epsilon(), 1, ExpressionType.EPSILON);
        this.d = d;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return checkNotNull(ev).visitEpsilon(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Epsilon) {
            Epsilon epsObj = (Epsilon)obj;
            return Objects.equals(d, epsObj.d);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Epsilon.class, d);
    }

    public final String d;

    public static Function<String, Expression> Epsilon =
        new Function<String, Expression> () {

        @Override
        public Epsilon apply(String f) {
            return new Epsilon(f);
        }
    };

}
