package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class IVisitor<T> {

    public T visit(Expression e) {
        return checkNotNull(e).invokeVisitor(this);
    }

    public abstract T visitBottom(Bottom e);
    public abstract T visitSymbol(Symbol e);
    public abstract T visitEpsilon(Epsilon e);
    public abstract T visitIfElse(IfElse e);
    public abstract T visitSplitSum(SplitSum e);
    public abstract T visitSum(Sum e);
    public abstract T visitIteratedSum(IteratedSum e);
    public abstract T visitChainedSum(ChainedSum e);
    public abstract T visitComposition(Composition e);
    public abstract T visitRestrict(Restrict e);

}
