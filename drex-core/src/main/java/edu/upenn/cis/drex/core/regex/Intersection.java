package edu.upenn.cis.drex.core.regex;

import automata.sfa.SFA;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import edu.upenn.cis.drex.core.domaintype.RegexAmbiguity;
import edu.upenn.cis.drex.core.domaintype.RegexDomain;
import static edu.upenn.cis.drex.core.regex.Regex.RegexType.INTERSECTION;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.Objects;
import theory.CharPred;
import theory.CharSolver;

public class Intersection extends Regex {

    public Intersection(Regex r1, Regex r2) {
        super(getSuperArgs(checkNotNull(r1), checkNotNull(r2)));
        this.r1 = r1;
        this.r2 = r2;
    }

    public Regex makeSimple() {
        checkArgument(getSFA() != null);
        return RegexUtil.of(getSFA());
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> rv) {
        return checkNotNull(rv).visitIntersection(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Intersection) {
            Intersection objUnion = (Intersection)obj;
            return Objects.equals(r1, objUnion.r1) &&
                    Objects.equals(r2, objUnion.r2);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Intersection.class, r1, r2);
    }

    public final Regex r1, r2;

    public static ConstructorArgs getSuperArgs(final Regex r1, final Regex r2) {
        checkNotNull(r1);
        checkNotNull(r2);

        int size = r1.size + r2.size + 1;

        Supplier<RegexDomain> domainSupplier = new Supplier<RegexDomain>() {
            @Override
            public RegexDomain get() {
                if (r1.getSFA() == null) {
                    checkAssertion(r1.getAmbiguity() != null);
                    return new RegexDomain(null, RegexAmbiguity.ofAmbiguousSubexpression(r1));
                } else if (r2.getSFA() == null) {
                    checkAssertion(r2.getAmbiguity() != null);
                    return new RegexDomain(null, RegexAmbiguity.ofAmbiguousSubexpression(r2));
                }
                SFA<CharPred, Character> sfa = r1.getSFA().intersectionWith(r2.getSFA(), new CharSolver());
                checkAssertion(sfa != null);
                return new RegexDomain(sfa, null);
            }
        };
        return new ConstructorArgs(domainSupplier, size, INTERSECTION);
    }

}
