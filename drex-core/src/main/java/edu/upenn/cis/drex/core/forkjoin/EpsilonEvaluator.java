package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;

public class EpsilonEvaluator extends Evaluator {

    EpsilonEvaluator(edu.upenn.cis.drex.core.expr.Epsilon expr) {
        super(checkNotNull(expr));
        this.d = LazyString.of(expr.d);
        this.th = null;
    }

    EpsilonEvaluator(edu.upenn.cis.drex.core.regex.Epsilon regex) {
        super(checkNotNull(regex));
        this.d = LazyString.of("");
        this.th = null;
    }

    @Override
    void actualReset() {
        th = null;
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        // checkAssertion(th == null);
        th = index;
        ImmutableList<Response> ans = ImmutableList.of((Response)new Result(this, index, 0, d));
        return ans;
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        checkArgument(0 <= index);
        if (th != null) {
            ImmutableList<Response> ans = ImmutableList.of((Response)new KillUp(this, th));
            th = null;
            return ans;
        } else {
            return ImmutableList.of();
        }
    }

    @Override
    void actualKillDown(int index) {
        // checkAssertion(th != null && th.equals(index));
        if (th.intValue() == index) {
            th = null;
        }
    }

    public final LazyString d;
    private /* nonfinal */ Integer th;

}
