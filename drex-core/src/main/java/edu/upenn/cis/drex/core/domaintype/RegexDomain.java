package edu.upenn.cis.drex.core.domaintype;

import automata.sfa.SFA;
import static com.google.common.base.Preconditions.checkArgument;
import javax.annotation.Nullable;
import theory.CharPred;

public class RegexDomain {

    public RegexDomain(SFA<CharPred, Character> sfa, RegexAmbiguity ambiguity) {
        checkArgument(sfa != null || ambiguity != null);
        this.sfa = sfa;
        this.ambiguity = ambiguity;
    }

    public final @Nullable SFA<CharPred, Character> sfa;
    public final @Nullable RegexAmbiguity ambiguity;

}
