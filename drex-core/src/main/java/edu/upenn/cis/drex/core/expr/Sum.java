package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import java.util.Objects;

public class Sum extends Expression {

    public Sum(Expression e1, Expression e2) {
        super(DomainTypeAnalyser.sum(checkNotNull(e1), checkNotNull(e2)),
                e1.size + e2.size + 1, ExpressionType.SUM);
        this.e1 = e1;
        this.e2 = e2;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return checkNotNull(ev).visitSum(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Sum) {
            Sum sumObj = (Sum)obj;
            return Objects.equals(e1, sumObj.e1) &&
                    Objects.equals(e2, sumObj.e2);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Sum.class, e1, e2);
    }

    public final Expression e1, e2;

    public static Expression ofList(ImmutableList<Expression> exprList) {
        checkNotNull(exprList);
        checkArgument(!exprList.isEmpty());

        Expression ans = exprList.get(0);
        for (int i = 1; i < exprList.size(); i++) {
            ans = new Sum(ans, checkNotNull(exprList.get(i)));
        }
        return ans;
    }
    
        
    public static final BiFunction<Expression, Expression, Expression> Sum =
        new BiFunction<Expression, Expression, Expression>() {

        @Override
        public Expression apply(Expression t, Expression u) {
            return new Sum(t, u);
        }
    };

}
