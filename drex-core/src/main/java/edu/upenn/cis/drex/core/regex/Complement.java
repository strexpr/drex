package edu.upenn.cis.drex.core.regex;

import automata.sfa.SFA;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import com.google.common.base.Supplier;
import edu.upenn.cis.drex.core.domaintype.RegexAmbiguity;
import edu.upenn.cis.drex.core.domaintype.RegexDomain;
import static edu.upenn.cis.drex.core.regex.Regex.RegexType.NOT;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.Objects;
import theory.CharPred;
import theory.CharSolver;

public class Complement extends Regex {

    public Complement(Regex r) {
        super(getSuperArgs(checkNotNull(r)));
        this.r = r;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> rv) {
        return checkNotNull(rv).visitComplement(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Complement) {
            return Objects.equals(r, ((Complement)obj).r);
        } else {
            return false;
        }
    }

    public Regex makeSimple() {
        SFA<CharPred, Character> sfa = getSFA();
        checkState(sfa != null);
        return RegexUtil.of(sfa);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Complement.class, r);
    }

    public final Regex r;

    public static ConstructorArgs getSuperArgs(final Regex r) {
        Supplier<RegexDomain> domainSupplier = new Supplier<RegexDomain>() {
            @Override
            public RegexDomain get() {
                if (r.getSFA() == null) {
                    checkAssertion(r.getAmbiguity() != null);
                    return new RegexDomain(null, RegexAmbiguity.ofAmbiguousSubexpression(r));
                }

                SFA<CharPred, Character> sfa = SFA.complementOf(r.getSFA(), new CharSolver());
                checkAssertion(sfa != null);
                return new RegexDomain(sfa, null);
            }
        };
        return new ConstructorArgs(domainSupplier, r.size + 1, NOT);
    }

}
