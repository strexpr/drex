package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import edu.upenn.cis.drex.core.regex.Star;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class IteratedSumEvaluator extends Evaluator {

    IteratedSumEvaluator(IteratedSum expr) {
        super(checkNotNull(expr));
        this.f = ExpressionEvaluatorBuilder.build(expr.e);
        this.left = expr.left;
        this.thf = new HashMap<>();
        this.postponedKills = new HashSet<>();
    }

    IteratedSumEvaluator(Star regex) {
        super(checkNotNull(regex));
        this.f = RegexEvaluatorBuilder.build(regex.r);
        this.left = false;
        this.thf = new HashMap<>();
        this.postponedKills = new HashSet<>();
    }

    @Override
    void actualReset() {
        thf.clear();
        f.reset();
        postponedKills.clear();
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        thf.put(index, new HashMap<Integer, LazyString>());
        thf.get(index).put(index, LazyString.of(""));

        ImmutableList<Response> fr = f.start(index);
        if (!fr.isEmpty()) {
            checkAssertion(fr.size() == 1);
            checkAssertion(fr.get(0) instanceof KillUp);
            checkAssertion(fr.get(0).startIndex == index);
            postponedKills.add(index);
        }

        ImmutableList<Response> ans = ImmutableList.of((Response)new Result(this, index, 0, LazyString.of("")));
        return ans;
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        ImmutableList<Response> fResp = f.symbol(index, symbol);
        ImmutableList<Response> ans = process(fResp);
        return ans;
    }

    @Override
    void actualKillDown(int index) {
        // checkAssertion(thf.containsKey(index));
        Set<Integer> potentialKills = new HashSet<>();

        if (thf.containsKey(index)) {
            potentialKills.addAll(thf.get(index).keySet());
            thf.remove(index);
        }

        for (int globalStart : thf.keySet()) {
            potentialKills.removeAll(thf.get(globalStart).keySet());
        }

        for (int killIndex : potentialKills) {
            f.killDown(killIndex);
        }
    }

    private ImmutableList<Response> process(ImmutableList<Response> children) {
        final ImmutableList.Builder<Response> respBuilder = new ImmutableList.Builder<>();
        for (Response resp : children) {
            if (resp instanceof Result) {
                processResult((Result)resp, respBuilder);
            } else {
                checkAssertion(resp instanceof KillUp);
                processKill((KillUp)resp, respBuilder);
            }
        }
        for (int i : postponedKills) {
            respBuilder.add(new KillUp(this, i));
        }
        postponedKills.clear();
        return respBuilder.build();
    }

    private void processResult(Result result, ImmutableList.Builder<Response> respBuilder) {
        checkNotNull(result);
        int fStartIndex = result.startIndex;
        int fLength = result.length;
        LazyString fValue = result.value;

        for (int startIndex : thf.keySet()) {
            Map<Integer, LazyString> thfi = thf.get(startIndex);
            if (thfi.containsKey(fStartIndex)) {
                int length = (fStartIndex - startIndex) + fLength;
                LazyString fsValue = thfi.get(fStartIndex);
                checkAssertion(fsValue != null);
                LazyString value = !left ? fsValue.concat(fValue) : fValue.concat(fsValue);
                thfi.put(fStartIndex + fLength, value);
                f.start(fStartIndex + fLength);
                respBuilder.add(new Result(this, startIndex, length, value));
            }
        }
    }

    private void processKill(KillUp kill, ImmutableList.Builder<Response> respBuilder) {
        checkNotNull(kill);
        int fStartIndex = kill.startIndex;
        Set<Integer> killRing = new TreeSet<>();

        for (int startIndex : thf.keySet()) {
            Map<Integer, LazyString> thfi = thf.get(startIndex);
            thfi.remove(fStartIndex);
            if (thfi.isEmpty()) {
                killRing.add(startIndex);
            }
        }

        for (int startIndex : killRing) {
            thf.remove(startIndex);
            respBuilder.add(new KillUp(this, startIndex));
        }
    }

    public final Evaluator f;
    public final boolean left;
    private final Map<Integer, Map<Integer, LazyString>> thf;
    private final Set<Integer> postponedKills;
}
