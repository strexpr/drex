package edu.upenn.cis.drex.core.util.function;

public interface Consumer<T> {

    public void accept(T t);

}
