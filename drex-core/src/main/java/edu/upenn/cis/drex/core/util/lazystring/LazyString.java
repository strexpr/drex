package edu.upenn.cis.drex.core.util.lazystring;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkElementIndex;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import java.util.Deque;
import java.util.LinkedList;
import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableInt;
import org.apache.commons.lang3.mutable.MutableObject;
import edu.upenn.cis.drex.core.util.Memo;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;

public abstract class LazyString {

    LazyString(int length) {
        checkArgument(0 <= length);
        this.length = length;
        this.str = new Memo<>(new Supplier<String>() {
            @Override
            public String get() {
                return buildStr();
            }
        });
    }

    public static LazyString of(String d) {
        return new ActualString(checkNotNull(d));
    }

    public LazyString concat(LazyString ls) {
        return new ConcatString(this, checkNotNull(ls));
    }

    public char charAt(int index) {
        checkElementIndex(index, length);

        if (str.isInvoked()) {
            return str.get().charAt(index);
        } else {
            final Mutable<LazyString> curr = new MutableObject<>(this);
            final MutableInt currIndex = new MutableInt(index);

            IVisitor<Character> visitor = new IVisitor<Character>() {

                @Override
                public Character visitActualString(ActualString str) {
                    return str.value.charAt(currIndex.intValue());
                }

                @Override
                public Character visitConcatString(ConcatString str) {
                    if (currIndex.intValue() < str.str1.length) {
                        curr.setValue(str.str1);
                    } else {
                        currIndex.subtract(str.str1.length);
                        curr.setValue(str.str2);
                    }
                    return null;
                }

            };

            Character ans = visitor.visit(curr.getValue());
            while (ans == null) {
                ans = visitor.visit(curr.getValue());
            }

            return ans;
        }
    }

    public String build() {
        return str.get();
    }

    private String buildStr() {
        final StringBuilder sb = new StringBuilder();
        final Deque<LazyString> d = new LinkedList<>();
        d.addFirst(this);

        IVisitor<Void> visitor = new IVisitor<Void>() {

            @Override
            public Void visitActualString(ActualString str) {
                sb.append(checkNotNull(str).value);
                return null;
            }

            @Override
            public Void visitConcatString(ConcatString str) {
                d.addFirst(checkNotNull(str).str2);
                d.addFirst(str.str1);
                return null;
            }

        };

        while (!d.isEmpty()) {
            visitor.visit(d.removeFirst());
        }

        String ans = sb.toString();
        checkAssertion(ans.length() == length);
        return ans;
    }

    abstract <T> T invokeVisitor(IVisitor<T> visitor);

    public final int length;
    private final Memo<String> str;

}
