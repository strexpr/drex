package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import java.util.Objects;

public class Restrict extends Expression {

    public Restrict(Expression e, Regex r) {
        super(DomainTypeAnalyser.restrict(checkNotNull(e), checkNotNull(r)),
                e.size + r.size + 1, ExpressionType.RESTRICT);
        this.e = e;
        this.r = r;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return checkNotNull(ev).visitRestrict(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Restrict) {
            Restrict restrictObj = (Restrict)obj;
            return Objects.equals(e, restrictObj.e) &&
                    Objects.equals(r, restrictObj.r);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Restrict.class, e, r);
    }

    public final Expression e;
    public final Regex r;

    public static final BiFunction<Expression, Regex, Expression> Restrict =
        new BiFunction<Expression, Regex, Expression>() {

        @Override
        public Expression apply(Expression t, Regex u) {
            return new Restrict(t, u);
        }
    };

}
