package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.regex.Empty;

public class BottomEvaluator extends Evaluator {

    public BottomEvaluator(Bottom expr) {
        super(checkNotNull(expr));
    }

    public BottomEvaluator(Empty regex) {
        super(checkNotNull(regex));
    }

    @Override
    void actualReset() {
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        return ImmutableList.of((Response)new KillUp(this, index));
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        return ImmutableList.of();
    }

    @Override
    void actualKillDown(int index) {
        // throw new UnreachableCodeException();
    }

}
