package edu.upenn.cis.drex.core.regex;

import automata.sfa.SFA;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import edu.upenn.cis.drex.core.domaintype.RegexDomain;
import static edu.upenn.cis.drex.core.regex.Regex.RegexType.EMPTY;
import java.util.Objects;
import theory.CharSolver;

public class Empty extends Regex {

    public Empty() {
        super(getSuperArgs());
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> rv) {
        return checkNotNull(rv).visitEmpty(this);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Empty;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Empty.class);
    }

    public static ConstructorArgs getSuperArgs() {
        Supplier<RegexDomain> domainSupplier = new Supplier<RegexDomain>() {
            @Override
            public RegexDomain get() {
                return new RegexDomain(SFA.getEmptySFA(new CharSolver()), null);
            }
        };
        return new ConstructorArgs(domainSupplier, 1, EMPTY);
    }

}
