package edu.upenn.cis.drex.core.util.lazystring;

import static com.google.common.base.Preconditions.checkNotNull;

abstract class IVisitor<T> {

    public T visit(LazyString str) {
        return checkNotNull(str).invokeVisitor(this);
    }

    public abstract T visitActualString(ActualString str);
    public abstract T visitConcatString(ConcatString str);

}
