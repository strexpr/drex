package edu.upenn.cis.drex.core.regex;

import automata.sfa.SFA;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.primitives.Chars;
import edu.upenn.cis.drex.core.domaintype.RegexAmbiguity;
import edu.upenn.cis.drex.core.domaintype.RegexDomain;
import edu.upenn.cis.drex.core.util.Memo;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;
import java.util.List;
import javax.annotation.Nullable;
import theory.CharPred;
import theory.CharSolver;

public abstract class Regex {

    public Regex(ConstructorArgs constructorArgs) {
        this.domainSupplier = new Memo<>(checkNotNull(constructorArgs).domainSupplier);
        this.size = constructorArgs.size;
        this.type = constructorArgs.type;
    }

    public abstract <T> T invokeVisitor(IVisitor<T> rv);

    @Override
    public abstract boolean equals(Object obj);

    @Override
    public abstract int hashCode();

    public SFA<CharPred, Character> getSFA() {
        return domainSupplier.get().sfa;
    }

    public RegexAmbiguity getAmbiguity() {
        return domainSupplier.get().ambiguity;
    }

    public enum RegexType {
        CONCAT,
        EMPTY,
        EPSILON,
        STAR,
        SYMBOL,
        UNION,
        UNIQUE_STAR,
        NOT,
        INTERSECTION
    }

    public final Memo<RegexDomain> domainSupplier;
    public final int size;
    public final RegexType type;

    @Override
    public String toString() {
        IVisitor<String> visitor = new IVisitor<String>() {

            @Override
            public String visitEmpty(Empty r) {
                return "empty";
            }

            @Override
            public String visitEpsilon(Epsilon r) {
                return "epsilon";
            }

            @Override
            public String visitSymbol(Symbol r) {
                return String.format("%s", r.a.toString());
            }

            @Override
            public String visitUnion(Union r) {
                String sr1 = r.r1.toString();
                String sr2 = r.r2.toString();
                checkAssertion(sr1 != null && sr2 != null);
                return String.format("(union %s %s)", sr1, sr2);
            }

            @Override
            public String visitConcat(Concat r) {
                String sr1 = r.r1.toString();
                String sr2 = r.r2.toString();
                checkAssertion(sr1 != null && sr2 != null);
                return String.format("(concat %s %s)", sr1, sr2);
            }

            @Override
            public String visitStar(Star r) {
                String sr = r.r.toString();
                checkAssertion(sr != null);
                return String.format("(star %s)", sr);
            }

            @Override
            public String visitComplement(Complement r) {
                String sr = r.r.toString();
                checkAssertion(sr != null);
                return String.format("(complement %s)", sr);
            }

            @Override
            public String visitIntersection(Intersection r) {
                String sr1 = r.r1.toString();
                String sr2 = r.r2.toString();
                checkAssertion(sr1 != null && sr2 != null);
                return String.format("(intersection %s %s)", sr1, sr2);
            }

        };

        return visitor.visit(this);
    }

    public boolean contains(String string, CharSolver solver) {
        LazyString lazyString = LazyString.of(checkNotNull(string));
        return RegexEvaluator.belongsTo(lazyString, this, solver);
    }

    public String minusWitness(Regex r) {
        checkNotNull(r);

        SFA<CharPred, Character> thisSFA = this.getSFA();
        SFA<CharPred, Character> rSFA = r.getSFA();
        SFA<CharPred, Character> difference = checkNotNull(thisSFA).minus(checkNotNull(rSFA), new CharSolver());
        checkAssertion(difference != null);
        List<Character> witness = difference.getWitness(new CharSolver());
        if (witness != null) {
            return new String(Chars.toArray(witness));
        } else {
            return null;
        }
    }

    public static class ConstructorArgs {

        public ConstructorArgs(Supplier<RegexDomain> domainSupplier, int size, RegexType type) {
            this.domainSupplier = checkNotNull(domainSupplier);
            checkArgument(size > 0);
            this.size = size;
            this.type = type;
        }

        public final Supplier<RegexDomain> domainSupplier;
        public final int size;
        public final RegexType type;

    }

}
