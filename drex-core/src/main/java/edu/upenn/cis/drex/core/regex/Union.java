package edu.upenn.cis.drex.core.regex;

import edu.upenn.cis.drex.core.domaintype.RegexAmbiguity;
import automata.sfa.SFA;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.primitives.Chars;
import edu.upenn.cis.drex.core.domaintype.RegexDomain;
import static edu.upenn.cis.drex.core.regex.Regex.RegexType.UNION;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.List;
import java.util.Objects;
import theory.CharPred;
import theory.CharSolver;

public class Union extends Regex {

    public Union(Regex r1, Regex r2) {
        super(getSuperArgs(checkNotNull(r1), checkNotNull(r2)));
        this.r1 = r1;
        this.r2 = r2;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> rv) {
        return checkNotNull(rv).visitUnion(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Union) {
            Union objUnion = (Union)obj;
            return Objects.equals(r1, objUnion.r1) &&
                    Objects.equals(r2, objUnion.r2);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Union.class, r1, r2);
    }

    public final Regex r1, r2;

    public static ConstructorArgs getSuperArgs(final Regex r1, final Regex r2) {
        checkNotNull(r1);
        checkNotNull(r2);

        int size = r1.size + r2.size + 1;

        Supplier<RegexDomain> domainSupplier = new Supplier<RegexDomain>() {
            @Override
            public RegexDomain get() {
                if (r1.getSFA() == null) {
                    checkAssertion(r1.getAmbiguity() != null);
                    return new RegexDomain(null, RegexAmbiguity.ofAmbiguousSubexpression(r1));
                } else if (r2.getSFA() == null) {
                    checkAssertion(r2.getAmbiguity() != null);
                    return new RegexDomain(null, RegexAmbiguity.ofAmbiguousSubexpression(r2));
                }
                SFA<CharPred, Character> sfa = r1.getSFA().unionWith(r2.getSFA(), new CharSolver());
                checkAssertion(sfa != null);

                if (r1.getAmbiguity() != null) {
                    return new RegexDomain(sfa, RegexAmbiguity.ofAmbiguousSubexpression(r1));
                } else if (r2.getAmbiguity() != null) {
                    return new RegexDomain(sfa, RegexAmbiguity.ofAmbiguousSubexpression(r2));
                } else if (r1 instanceof Empty) {
                    String errorMsg = String.format("Empty subexpression %s.", r1);
                    return new RegexDomain(sfa, RegexAmbiguity.simpleErrorOf(errorMsg));
                } else if (r2 instanceof Empty) {
                    String errorMsg = String.format("Empty subexpression %s.", r2);
                    return new RegexDomain(sfa, RegexAmbiguity.simpleErrorOf(errorMsg));
                }

                SFA<CharPred, Character> intersection = r1.getSFA().intersectionWith(r2.getSFA(), new CharSolver());
                if (!intersection.isEmpty()) {
                    List<Character> inp = intersection.getWitness(new CharSolver());
                    String counterexample = new String(Chars.toArray(inp));
                    String errorMsg = String.format("Non-disjoint subexpressions\n"
                            + "%s\n"
                            + "and\n"
                            + "%s\n"
                            + "on input string\n"
                            + "\"%s\"", r1, r2, counterexample);
                    return new RegexDomain(sfa, RegexAmbiguity.simpleErrorOf(errorMsg));
                }

                return new RegexDomain(sfa, null);
            }
        };

        return new ConstructorArgs(domainSupplier, size, UNION);
    }

}
