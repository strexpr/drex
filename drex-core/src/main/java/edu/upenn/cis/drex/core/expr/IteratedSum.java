package edu.upenn.cis.drex.core.expr;

import com.google.common.base.Function;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import java.util.Objects;

public class IteratedSum extends Expression {

    public IteratedSum(Expression e, boolean left) {
        super(DomainTypeAnalyser.iteratedSum(checkNotNull(e)), e.size + 1,
                left ? ExpressionType.LITER : ExpressionType.ITER);
        this.e = e;
        this.left = left;
    }

    public IteratedSum(Expression e) {
        this(checkNotNull(e), false);
    }

    public static Expression plus(Expression e) {
        return new SplitSum(checkNotNull(e), new IteratedSum(e));
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return ev.visitIteratedSum(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof IteratedSum) {
            IteratedSum iterObj = (IteratedSum)obj;
            return Objects.equals(e, iterObj.e) &&
                    Objects.equals(left, iterObj.left);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(IteratedSum.class, e, left);
    }

    public final Expression e;
    public final boolean left;

        
    Function<Expression, Expression> RIteratedSum =
        new Function<Expression, Expression>() {

        @Override
        public Expression apply(Expression t) {
            return new IteratedSum(t, false);
        }
    };
    
    Function<Expression, Expression> LIteratedSum =
        new Function<Expression, Expression>() {

        @Override
        public Expression apply(Expression t) {
            return new IteratedSum(t, true);
        }
    };

}
