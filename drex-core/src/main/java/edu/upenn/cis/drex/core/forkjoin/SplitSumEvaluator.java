package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import edu.upenn.cis.drex.core.regex.Concat;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SplitSumEvaluator extends Evaluator {

    SplitSumEvaluator(SplitSum expr) {
        super(checkNotNull(expr));
        this.f = ExpressionEvaluatorBuilder.build(expr.e1);
        this.g = ExpressionEvaluatorBuilder.build(expr.e2);
        this.left = expr.left;
        thf = new HashSet<>();
        thg = HashBasedTable.create();
    }

    SplitSumEvaluator(Concat regex) {
        super(checkNotNull(regex));
        this.f = RegexEvaluatorBuilder.build(regex.r1);
        this.g = RegexEvaluatorBuilder.build(regex.r2);
        this.left = false;
        thf = new HashSet<>();
        thg = HashBasedTable.create();
    }

    @Override
    void actualReset() {
        thf.clear();
        thg.clear();
        f.reset();
        g.reset();
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        thf.add(index);
        return process(f.start(index));
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        List<Response> childResponses = new ArrayList<>();
        childResponses.addAll(f.symbol(index, symbol));
        childResponses.addAll(g.symbol(index, symbol));
        ImmutableList<Response> ans = process(ImmutableList.copyOf(childResponses));
        return ans;
    }

    @Override
    void actualKillDown(int index) {
        int fStartIndex = index;

        if (thf.contains(fStartIndex)) {
            thf.remove(fStartIndex);
            f.killDown(fStartIndex);
        }

        Set<Integer> potentialGKills = ImmutableSet.copyOf(thg.row(fStartIndex).keySet());
        for (int gStartIndex : potentialGKills) {
            thg.remove(fStartIndex, gStartIndex);
        }
        potentialGKills = Sets.difference(potentialGKills, thg.columnKeySet());
        for (int gStartIndex : potentialGKills) {
            g.killDown(gStartIndex);
        }
    }

    private ImmutableList<Response> process(ImmutableList<Response> children) {
        List<Response> responses = new ArrayList<>();
        for (Response resp : checkNotNull(children)) {
            if (resp instanceof Result) {
                processResult((Result)resp, responses);
            } else {
                checkAssertion(resp instanceof KillUp);
                processKill((KillUp)resp, responses);
            }
        }
        return ImmutableList.copyOf(responses);
    }

    private void processResult(Result result, List<Response> responses) {
        checkArgument(result.origin == f || result.origin == g);

        if (result.origin == f) {
            int currIndex = result.startIndex + result.length;
            thg.put(result.startIndex, currIndex, result.value);
            ImmutableList<Response> gResponses = g.start(currIndex);
            ImmutableList<Response> processedResponses = process(gResponses);
            responses.addAll(processedResponses);
        } else {
            checkAssertion(result.origin == g);
            int gStartIndex = result.startIndex;
            int gLength = result.length;
            LazyString gValue = result.value;

            for (int fStartIndex : thg.rowKeySet()) {
                if (thg.contains(fStartIndex, gStartIndex)) {
                    LazyString fValue = thg.get(fStartIndex, gStartIndex);
                    int length = gStartIndex + gLength - fStartIndex;
                    LazyString value = !left ? fValue.concat(gValue) : gValue.concat(fValue);
                    responses.add(new Result(this, fStartIndex, length, value));
                }
            }
        }
    }

    private void processKill(KillUp killUp, List<Response> responses) {
        checkArgument(killUp.origin == f || killUp.origin == g);

        if (killUp.origin == f) {
            thf.remove(killUp.startIndex);
            if (!thg.containsRow(killUp.startIndex)) {
                responses.add(new KillUp(this, killUp.startIndex));
            }
        } else {
            Set<Integer> delRec = new HashSet<>();
            for (int fStartIndex : thg.rowKeySet()) {
                delRec.add(fStartIndex);
            }

            Set<Integer> killRing = new HashSet<>();
            for (int fStartIndex : delRec) {
                thg.remove(fStartIndex, killUp.startIndex);
                if (!thf.contains(fStartIndex) && !thg.containsRow(fStartIndex)) {
                    killRing.add(fStartIndex);
                }
            }

            for (int fStartIndex : killRing) {
                responses.add(new KillUp(this, fStartIndex));
            }
        }
    }

    public final Evaluator f, g;
    public final boolean left;
    private final Set<Integer> thf;
    private final Table<Integer, Integer, LazyString> thg;

}
