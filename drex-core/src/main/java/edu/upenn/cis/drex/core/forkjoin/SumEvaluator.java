package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.forkjoin.response.IVisitor;
import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;
import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableObject;

public class SumEvaluator extends Evaluator {

    SumEvaluator(Sum e) {
        super(checkNotNull(e));
        this.f = ExpressionEvaluatorBuilder.build(e.e1);
        this.g = ExpressionEvaluatorBuilder.build(e.e2);
    }

    @Override
    void actualReset() {
        f.reset();
        g.reset();
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        ImmutableList.Builder<Response> childRespBuilder = new ImmutableList.Builder<>();
        childRespBuilder.addAll(checkNotNull(f.start(index)));
        childRespBuilder.addAll(checkNotNull(g.start(index)));
        return process(childRespBuilder.build());
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        ImmutableList.Builder<Response> childRespBuilder = new ImmutableList.Builder<>();
        childRespBuilder.addAll(checkNotNull(f.symbol(index, symbol)));
        childRespBuilder.addAll(checkNotNull(g.symbol(index, symbol)));
        return process(childRespBuilder.build());
    }

    @Override
    void actualKillDown(int index) {
        f.killDown(index);
        g.killDown(index);
    }

    private ImmutableList<Response> process(ImmutableList<Response> children) {
        final ImmutableList.Builder<Response> respBuilder = new ImmutableList.Builder<>();
        final Mutable<LazyString> fans = new MutableObject<>();
        final Mutable<LazyString> gans = new MutableObject<>();

        for (Response resp : children) {
            final SumEvaluator thisExpr = this;

            IVisitor<Void> visitor = new IVisitor<Void>() {

                @Override
                public Void visitResult(Result result) {
                    checkArgument(result.origin == f || result.origin == g);

                    if (result.origin == f) {
                        fans.setValue(checkNotNull(result.value));
                    } else {
                        gans.setValue(checkNotNull(result.value));
                    }

                    if (fans.getValue() != null && gans.getValue() != null) {
                        LazyString ans = fans.getValue().concat(gans.getValue());
                        respBuilder.add(new Result(thisExpr, result.startIndex, result.length, ans));
                    }

                    return null;
                }

                @Override
                public Void visitKillUp(KillUp killUp) {
                    if (killUp.origin == f) {
                        respBuilder.add(new KillUp(thisExpr, killUp.startIndex));
                    }
                    return null;
                }

            };

            visitor.visit(checkNotNull(resp));
        }

        return respBuilder.build();
    }

    public final Evaluator f, g;

}
