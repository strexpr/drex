package edu.upenn.cis.drex.core.util.assertions;

public class AssertionViolation extends RuntimeException {

    public AssertionViolation() {
    }

    public AssertionViolation(Object message) {
        super(String.valueOf(message));
    }

    public AssertionViolation(Object message, Throwable cause) {
        super(String.valueOf(message), cause);
    }

}
