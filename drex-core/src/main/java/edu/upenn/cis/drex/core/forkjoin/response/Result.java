package edu.upenn.cis.drex.core.forkjoin.response;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.forkjoin.Evaluator;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;

public class Result extends Response {

    public Result(Evaluator origin, int startIndex, int length, LazyString value) {
        super(checkNotNull(origin), startIndex);
        checkArgument(0 <= length);
        this.length = length;
        this.value = checkNotNull(value);
        this.id = count++;
    }

    @Override
    public String toString() {
        if (origin.expr != null) {
            return String.format("(Result %d %d %d %s %s)", id, startIndex, length, value.build(), origin.expr.toString());
        } else {
            return String.format("(Result %d %d %d %s %s)", id, startIndex, length, value.build(), origin.regex.toString());
        }
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> visitor) {
        return checkNotNull(visitor).visitResult(this);
    }

    public final int length;
    public final LazyString value;
    public final int id;
    private static int count = 0;

}
