package edu.upenn.cis.drex.core.util.lazystring;

import static com.google.common.base.Preconditions.checkNotNull;

class ConcatString extends LazyString {

    public ConcatString(LazyString str1, LazyString str2) {
        super(checkNotNull(str1).length + checkNotNull(str2).length);
        this.str1 = str1;
        this.str2 = str2;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> visitor) {
        return checkNotNull(visitor).visitConcatString(this);
    }

    public final LazyString str1, str2;

}
