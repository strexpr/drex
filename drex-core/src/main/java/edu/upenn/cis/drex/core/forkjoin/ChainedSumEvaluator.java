package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import edu.upenn.cis.drex.core.forkjoin.response.IVisitor;
import java.util.Map;
import java.util.Set;
import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import edu.upenn.cis.drex.core.util.ImmutableQuad;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;

public class ChainedSumEvaluator extends Evaluator {

    ChainedSumEvaluator(ChainedSum e) {
        super(checkNotNull(e));

        this.e = ExpressionEvaluatorBuilder.build(e.e);
        this.r = RegexEvaluatorBuilder.build(e.r);
        this.left = e.left;

        this.b = new HashSet<>();
        this.eth = new HashSet<>();
        this.rth = new HashMap<>();
    }

    @Override
    void actualReset() {
        b.clear();
        eth.clear();
        rth.clear();

        e.reset();
        r.reset();
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        // checkAssertion(!b.contains(index));
        b.add(index);

        // checkAssertion(!rth.containsKey(index));
        rth.put(index, new HashSet<>(Arrays.asList(index)));

        ImmutableList<Response> eResponses = e.start(index);
        ImmutableList<Response> rResponses = r.start(index);
        checkAssertion(eResponses.isEmpty() && rResponses.isEmpty());
        return ImmutableList.of();
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        ImmutableList.Builder<Response> combRespBuilder = new ImmutableList.Builder<>();
        combRespBuilder.addAll(e.symbol(index, symbol));
        combRespBuilder.addAll(r.symbol(index, symbol));
        return process(combRespBuilder.build());
    }

    @Override
    void actualKillDown(int index) {
        b.remove(index);

        Set<Integer> potentialKills = new HashSet<>();

        if (rth.containsKey(index)) {
            for (int j : rth.get(index)) {
                potentialKills.add(j);
            }
            rth.remove(index);
        }

        Set<ImmutableQuad<Integer, Integer, Integer, LazyString>> delRec = new HashSet<>();
        for (ImmutableQuad<Integer, Integer, Integer, LazyString> rec : eth) {
            if (rec.t1.equals(index)) {
                delRec.add(rec);
            }
        }
        eth.removeAll(delRec);

        for (int globalStart : rth.keySet()) {
            potentialKills.removeAll(rth.get(globalStart));
        }

        for (int killIndex : potentialKills) {
            e.killDown(killIndex);
            r.killDown(killIndex);
        }
    }

    private ImmutableList<Response> process(final ImmutableList<Response> allResponses) {
        final ImmutableList.Builder<Response> respBuilder = new ImmutableList.Builder<>();

        for (Response resp : checkNotNull(allResponses)) {
            IVisitor<Void> visitor = new IVisitor<Void>() {

                @Override
                public Void visitResult(Result result) {
                    processResult(result, respBuilder);
                    return null;
                }

                @Override
                public Void visitKillUp(KillUp killUp) {
                    processKill(killUp, respBuilder);
                    return null;
                }

            };

            visitor.visit(checkNotNull(resp));
        }

        return respBuilder.build();
    }

    private void processResult(Result result, ImmutableList.Builder<Response> respBuilder) {
        if (result.origin == e) {
            processExpressionResult(result, respBuilder);
        } else {
            processRegexResult(result);
        }
    }

    private void processExpressionResult(Result result,
            ImmutableList.Builder<Response> respBuilder) {
        Set<ImmutableQuad<Integer, Integer, Integer, LazyString>> newRecords = new HashSet<>();

        for (ImmutableQuad<Integer, Integer, Integer, LazyString> rec : eth) {
            if (rec.t2 == result.startIndex) {
                int startIndex = rec.t1;
                int currIndex = result.startIndex + result.length;
                int length = currIndex - startIndex;

                LazyString value = !left ? rec.t4.concat(result.value) : result.value.concat(rec.t4);
                respBuilder.add(new Result(this, startIndex, length, value));

                newRecords.add(ImmutableQuad.of(startIndex, rec.t3, currIndex, value));
                e.start(currIndex);
            }
        }

        ethAdd(newRecords);
    }

    private void processRegexResult(Result result) {
        int startIndex = result.startIndex;
        int currIndex = result.startIndex + result.length;

        if (b.contains(result.startIndex)) {
            ethAdd(startIndex, startIndex, currIndex, LazyString.of(""));
            ImmutableList<Response> eResponses = e.start(currIndex);
            checkAssertion(eResponses.isEmpty());
        }

        boolean added = false;
        for (Entry<Integer, Set<Integer>> rec : rth.entrySet()) {
            if (rec.getValue().contains(startIndex)) {
                rec.getValue().add(currIndex);
                added = true;
            }
        }
        checkAssertion(added);
        ImmutableList<Response> rResponses = r.start(currIndex);
        checkAssertion(rResponses.isEmpty());
    }

    private void processKill(KillUp kill, ImmutableList.Builder<Response> respBuilder) {
        if (kill.origin == e) {
            processExpressionKill(kill);
        } else {
            processRegexKill(kill, respBuilder);
        }
    }

    private void processExpressionKill(KillUp kill) {
        Set<ImmutableQuad<Integer, Integer, Integer, LazyString>> delRec = new HashSet<>();
        for (ImmutableQuad<Integer, Integer, Integer, LazyString> rec : eth) {
            if (rec.t2.equals(kill.startIndex)) {
                delRec.add(rec);
            }
        }
        eth.removeAll(delRec);
    }

    private void processRegexKill(KillUp kill, ImmutableList.Builder<Response> respBuilder) {

        Set<Integer> killRing = new HashSet<>();

        for (Entry<Integer, Set<Integer>> startEntry : rth.entrySet()) {
            startEntry.getValue().remove(kill.startIndex);
            if (startEntry.getValue().isEmpty()) {
                killRing.add(startEntry.getKey());
            }
        }

        for (int startIndex : killRing) {
            respBuilder.add(new KillUp(this, startIndex));
            rth.remove(startIndex);
        }

    }

    private void ethAdd(int i1, int i2, int i3, LazyString l) {
        checkNotNull(l);
        for (ImmutableQuad<Integer, Integer, Integer, LazyString> quad : eth) {
            if (quad.t1.intValue() == i1 && quad.t2.intValue() == i2 && quad.t3.intValue() == i3) {
                eth.remove(quad);
                eth.add(ImmutableQuad.of(i1, i2, i3, l));
                return;
            }
        }
        eth.add(ImmutableQuad.of(i1, i2, i3, l));
    }

    private void ethAdd(Collection<ImmutableQuad<Integer, Integer, Integer, LazyString>> c) {
        for (ImmutableQuad<Integer, Integer, Integer, LazyString> quad : checkNotNull(c)) {
            checkNotNull(quad);
            ethAdd(quad.t1, quad.t2, quad.t3, quad.t4);
        }
    }

    public final Evaluator e, r;
    public final boolean left;

    private final Set<Integer> b;
    private final Set<ImmutableQuad<Integer, Integer, Integer, LazyString>> eth;
    private final Map<Integer, Set<Integer>> rth;

}
