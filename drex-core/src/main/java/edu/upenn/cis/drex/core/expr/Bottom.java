package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import java.util.Objects;

public class Bottom extends Expression {

    public Bottom() {
        super(DomainTypeAnalyser.bottom(), 1, ExpressionType.BOTTOM);
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return checkNotNull(ev).visitBottom(this);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Bottom;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Bottom.class);
    }

}
