package edu.upenn.cis.drex.core.util;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.Serializable;

import org.apache.commons.lang3.builder.CompareToBuilder;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ImmutableQuad<T1, T2, T3, T4>
implements Serializable, Comparable<ImmutableQuad<T1, T2, T3, T4>> {

    public ImmutableQuad(T1 t1, T2 t2, T3 t3, T4 t4) {
        this.t1 = t1;
        this.t2 = t2;
        this.t3 = t3;
        this.t4 = t4;
    }

    public static <T1, T2, T3, T4> ImmutableQuad<T1, T2, T3, T4> of(T1 t1, T2 t2, T3 t3, T4 t4) {
        return new ImmutableQuad<>(t1, t2, t3, t4);
    }

    /**
     * Overrides the Comparable.compareTo() method, and offers the same return
     * guarantees.
     * @param o the object to be compared
     * @return a negative number if this object is less than o, zero if they
     * are equal, and a positive number if this object is greater than o
     * @throws NullPointerException if o is null
     */
    @Override
    public int compareTo(ImmutableQuad<T1, T2, T3, T4> o) {
        return new CompareToBuilder().append(this.t1, checkNotNull(o).t1)
                                     .append(this.t2, o.t2)
                                     .append(this.t3, o.t3)
                                     .append(this.t4, o.t4)
                                     .toComparison();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof ImmutableQuad)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        @SuppressWarnings("unchecked")
        ImmutableQuad<T1, T2, T3, T4> rhs = (ImmutableQuad<T1, T2, T3, T4>)obj;
        return new EqualsBuilder().append(t1, rhs.t1)
                                  .append(t2, rhs.t2)
                                  .append(t3, rhs.t3)
                                  .append(t4, rhs.t4)
                                  .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(t1)
                                    .append(t2)
                                    .append(t3)
                                    .append(t4)
                                    .toHashCode();
    }

    public final T1 t1;
    public final T2 t2;
    public final T3 t3;
    public final T4 t4;

}
