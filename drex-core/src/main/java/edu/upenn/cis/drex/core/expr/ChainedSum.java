package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import edu.upenn.cis.drex.core.domaintype.TypeCheckingException;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.Objects;
import java.util.function.BiFunction;

public class ChainedSum extends Expression {

    public ChainedSum(Expression e, Regex r, boolean left) {
        super(DomainTypeAnalyser.chainedSum(e, r), e.size + r.size + 1,
                left ? ExpressionType.LCHAINSUM : ExpressionType.CHAINSUM);
        this.e = e;
        this.r = r;
        this.left = left;
    }

    public ChainedSum(Expression e, Regex r) {
        this(e, r, false);
    }

    public ChainedSum(Expression e, boolean left)
    throws TypeCheckingException {
        super(DomainTypeAnalyser.chainedSum(checkNotNull(e), null), e.size + 1,
                left ? ExpressionType.LCHAINSUM : ExpressionType.CHAINSUM);
        if (getDomainType() == null) {
            throw new TypeCheckingException(checkNotNull(this.getDomainTypeError()));
        }

        this.e = e;
        Regex er = e.getDomainType();
        checkAssertion(er instanceof Concat);
        this.r = ((Concat)er).r1;
        this.left = left;
    }

    public ChainedSum(Expression e) throws TypeCheckingException {
        this(checkNotNull(e), false);
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return ev.visitChainedSum(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof ChainedSum) {
            ChainedSum csObj = (ChainedSum)obj;
            return Objects.equals(e, csObj.e) &&
                    Objects.equals(r, csObj.r) &&
                    Objects.equals(left, csObj.left);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(ChainedSum.class, e, r, left);
    }

    public final Expression e;
    public final Regex r;

    public final boolean left;
    
    public final static BiFunction<Expression, Regex, Expression> RChainedSum
        = new BiFunction<Expression, Regex, Expression>() {

        @Override
        public ChainedSum apply(Expression t, Regex u) {
            return new ChainedSum(t, u, false);
        }
    };
    
    public final static BiFunction<Expression, Regex, Expression> LChainedSum
        = new BiFunction<Expression, Regex, Expression>() {

        @Override
        public ChainedSum apply(Expression t, Regex u) {
            return new ChainedSum(t, u, true);
        }
    };

    
}
