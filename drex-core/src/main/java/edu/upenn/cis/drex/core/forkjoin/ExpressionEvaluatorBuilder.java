package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import edu.upenn.cis.drex.core.expr.Composition;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IVisitor;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.Restrict;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.expr.Symbol;
import edu.upenn.cis.drex.core.util.assertions.UnreachableCodeException;

public class ExpressionEvaluatorBuilder extends IVisitor<Evaluator> {

    public static boolean FAIL_FLAG = false;

    @Deprecated
    public static Evaluator build(Expression e) {
        if (FAIL_FLAG) {
            throw new UnsupportedOperationException("Expression evaluators are broken in the presence of restrict!");
        } else {
            ExpressionEvaluatorBuilder builder = new ExpressionEvaluatorBuilder();
            checkNotNull(e);
            checkArgument(e.getDomainType() != null);
            return builder.visit(e);
        }
    }

    @Override
    public Evaluator visitBottom(Bottom e) {
        return new BottomEvaluator(e);
    }

    @Override
    public Evaluator visitSymbol(Symbol e) {
        return new SymbolEvaluator(e);
    }

    @Override
    public Evaluator visitEpsilon(Epsilon e) {
        return new EpsilonEvaluator(e);
    }

    @Override
    public Evaluator visitIfElse(IfElse e) {
        return new IfElseEvaluator(e);
    }

    @Override
    public Evaluator visitSplitSum(SplitSum e) {
        return new SplitSumEvaluator(e);
    }

    @Override
    public Evaluator visitSum(Sum e) {
        return new SumEvaluator(e);
    }

    @Override
    public Evaluator visitIteratedSum(IteratedSum e) {
        return new IteratedSumEvaluator(e);
    }

    @Override
    public Evaluator visitChainedSum(ChainedSum e) {
        return new ChainedSumEvaluator(e);
    }

    @Override
    public Evaluator visitComposition(Composition e) {
        throw new UnreachableCodeException();
    }

    @Override
    public Evaluator visitRestrict(Restrict e) {
        return new RestrictEvaluator(e);
    }

}
