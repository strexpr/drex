package edu.upenn.cis.drex.core.regex;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkElementIndex;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.util.TriHashTable;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ArrayUtils;
import theory.CharSolver;

public class RegexEvaluator {

    public RegexEvaluator(LazyString string) {
        this.string = checkNotNull(string);
        this.substringMemo = new TriHashTable<>();
    }

    public static boolean belongsTo(LazyString string, Regex r, CharSolver solver) {
        RegexEvaluator evaluator = new RegexEvaluator(checkNotNull(string));
        return evaluator.belongsTo(0, string.length, checkNotNull(r), solver);
    }

    public boolean belongsTo(final int i, final int j, final Regex r, final CharSolver solver) {
        checkElementIndex(i, string.length + 1);
        checkElementIndex(j, string.length + 1);
        checkArgument(i <= j);
        checkNotNull(r);

        if (substringMemo.containsKey(i, j, r)) {
            return substringMemo.get(i, j, r);
        }

        if (r.getSFA() != null) {
            String sij = "";
            if (i < j) {
                sij = string.build().substring(i, j);
            }
            List<Character> sijl = Arrays.asList(ArrayUtils.toObject(sij.toCharArray()));
            boolean result = r.getSFA().accepts(sijl, solver);
            substringMemo.put(i, j, r, result);
            return result;
        }

        IVisitor<Boolean> visitor = new IVisitor<Boolean>() {

            @Override
            public Boolean visitEmpty(Empty r) {
                return false;
            }

            @Override
            public Boolean visitEpsilon(Epsilon r) {
                return i == j;
            }

            @Override
            public Boolean visitSymbol(Symbol r) {                
                return i + 1 == j && solver.HasModel(r.a, string.charAt(i));
            }

            @Override
            public Boolean visitUnion(Union r) {
                return belongsTo(i, j, r.r1, solver) || belongsTo(i, j, r.r2, solver);
            }

            @Override
            public Boolean visitConcat(Concat r) {
                for (int k = i; k <= j; k++) {
                    boolean validSplit = belongsTo(i, k, r.r1, solver) && belongsTo(k, j, r.r2, solver);
                    if (validSplit) {
                        return true;
                    }
                }
                return false;
            }

            @Override
            public Boolean visitStar(Star r) {
                PathCount parseTreeCount = countStarRParseTrees(i, j, r.r, solver);
                return parseTreeCount != PathCount.NONE;
            }

            @Override
            public Boolean visitComplement(Complement r) {
                return !belongsTo(i, j, r.r, solver);
            }

            @Override
            public Boolean visitIntersection(Intersection r) {
                return belongsTo(i, j, r.r1, solver) && belongsTo(i, j, r.r2, solver);
            }

        };

        boolean result = visitor.visit(r);
        substringMemo.put(i, j, r, result);
        return result;
    }

    public PathCount countStarRParseTrees(int i, int j, Regex r, CharSolver solver) {
        checkElementIndex(i, string.length + 1);
        checkElementIndex(j, string.length + 1);
        checkArgument(i <= j);
        checkNotNull(r);

        PathCount[] positions = new PathCount[j - i + 1];
        Arrays.fill(positions, PathCount.NONE);
        positions[0] = PathCount.ONE;

        for (int ip = 0; ip < positions.length; ip++) {
            if (positions[ip] != PathCount.NONE) {
                for (int jp = ip; jp < positions.length; jp++) {
                    if (belongsTo(i + ip, i + jp, r, solver)) {
                        positions[jp] = addPathCounts(positions[ip], positions[jp]);
                    }
                }
            }
        }

        return positions[positions.length - 1];
    }

    public static PathCount addPathCounts(PathCount p1, PathCount p2) {
        if (p1 == PathCount.NONE) {
            return p2;
        } else if (p2 == PathCount.NONE) {
            return p1;
        } else {
            return PathCount.MANY;
        }
    }

    public final LazyString string;
    public enum PathCount { NONE, ONE, MANY };
    private final /* non-frozen */ TriHashTable<Integer, Integer, Regex, Boolean> substringMemo;

}
