package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkArgument;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import edu.upenn.cis.drex.core.expr.Expression;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import javax.annotation.Nullable;

public abstract class Evaluator {

    public Evaluator(Expression expr) {
        this.expr = checkNotNull(expr);
        checkNotNull(expr.getDomainType());
        this.regex = null;
        synchronized (Evaluator.class) {
            this.serialNumber = transducerCount;
            transducerCount++;
        }
    }

    public Evaluator(Regex regex) {
        this.expr = null;
        this.regex = checkNotNull(regex);
        checkArgument(regex.getAmbiguity() == null);
        synchronized (Evaluator.class) {
            this.serialNumber = transducerCount;
            transducerCount++;
        }
    }

    void reset() {
        if (DEBUG) {
            System.out.printf("(reset %s)\n", expr);
        }
        actualReset();
        if (DEBUG) {
            System.out.printf("(resetReturn %s)\n", expr);
        }
    }

    abstract void actualReset();

    ImmutableList<Response> start(int index) {
        if (DEBUG) {
            System.out.printf("(start %d %s)\n", index, expr);
        }
        ImmutableList<Response> ans = actualStart(index);
        if (DEBUG) {
            System.out.printf("(startReturn %d %s\n", index, expr);
            for (Response resp : ans) {
                System.out.printf("\t%s\n", resp);
            }
            System.out.printf(")\n");
        }
        return ans;
    }

    abstract ImmutableList<Response> actualStart(int index);

    ImmutableList<Response> symbol(int index, char symbol) {
        if (DEBUG) {
            System.out.printf("(symbol %d %c %s)\n", index, symbol, expr);
        }
        ImmutableList<Response> ans = actualSymbol(index, symbol);
        if (DEBUG) {
            System.out.printf("(symbolReturn %d %c %s\n", index, symbol, expr);
            for (Response resp : ans) {
                System.out.printf("\t%s\n", resp);
            }
            System.out.printf(")\n");
        }
        return ans;
    }

    abstract ImmutableList<Response> actualSymbol(int index, char symbol);

    void killDown(int index) {
        if (DEBUG) {
            System.out.printf("(killDown %d %s)\n", index, expr);
        }
        actualKillDown(index);
        if (DEBUG) {
            System.out.printf("(killDownReturn %d %s)\n", index, expr);
        }
    }

    abstract void actualKillDown(int index);

    public final @Nullable Expression expr;
    public final @Nullable Regex regex;
    public final int serialNumber;
    private static int transducerCount = 0;
    public static final boolean DEBUG = false;

    public synchronized String eval(String input) {
        checkNotNull(input);

        reset();
        ImmutableList<Response> responseList = start(0);
        for (int i = 0; i < input.length(); i++) {
            responseList = symbol(i, input.charAt(i));
        }
        checkAssertion(responseList != null);

        if (responseList.size() > 1) {
            System.out.println("EXPR: " + this.expr);
            System.out.println("REGEX: " + this.regex);
            System.out.println("INPUT: " + input);
        }
        checkAssertion(responseList.size() <= 1);
        if (responseList.size() == 1 && responseList.get(0) instanceof Result) {
            Result answer = (Result)responseList.get(0);
            return answer.value.build();
        } else {
            return null;
        }
    }

    @Override
    public String toString() {
        if (expr != null) {
            return String.format("ExpressionTransducer(%s)", expr);
        } else {
            checkAssertion(regex != null);
            return String.format("RegexTransducer(%s)", regex);
        }
    }

}
