package edu.upenn.cis.drex.core.forkjoin.response;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class IVisitor<T> {

    public T visit(Response response) {
        return checkNotNull(response).invokeVisitor(this);
    }

    public abstract T visitResult(Result result);
    public abstract T visitKillUp(KillUp killUp);

}
