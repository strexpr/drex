package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;

public class RestrictEvaluator extends Evaluator {

    RestrictEvaluator(edu.upenn.cis.drex.core.expr.Restrict r) {
        super(checkNotNull(r));
        this.et = ExpressionEvaluatorBuilder.build(r.e);
        this.rt = RegexEvaluatorBuilder.build(r.r);
    }

    @Override
    void actualReset() {
        et.reset();
        rt.reset();
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        return process(et.start(index), rt.start(index));
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        return process(et.symbol(index, symbol), rt.symbol(index, symbol));
    }

    @Override
    void actualKillDown(int index) {
        et.killDown(index);
        rt.killDown(index);
    }

    private ImmutableList<Response> process(ImmutableList<Response> eResponses,
            ImmutableList<Response> rResponses) {
        ImmutableList.Builder<Response> respBuilder = ImmutableList.builder();
        processExpressionResponses(eResponses, rResponses, respBuilder);
        processRegexResponses(eResponses, rResponses, respBuilder);
        return respBuilder.build();
    }

    private void processExpressionResponses(ImmutableList<Response> eResponses,
            ImmutableList<Response> rResponses,
            ImmutableList.Builder<Response> respBuilder) {
        for (Response eResp : eResponses) {
            if (eResp instanceof Result) {
                int length = ((Result)eResp).length;
                LazyString value = ((Result)eResp).value;
                for (Response rResp : rResponses) {
                    if (rResp instanceof Result && rResp.startIndex == eResp.startIndex) {
                        respBuilder.add(new Result(this, eResp.startIndex, length, value));
                        break;
                    }
                }
            }
        }
    }

    private void processRegexResponses(ImmutableList<Response> eResponses,
            ImmutableList<Response> rResponses,
            ImmutableList.Builder<Response> respBuilder) {
        for (Response rResp : rResponses) {
            if (rResp instanceof KillUp) {
                boolean eDead = false;
                for (Response eResp : eResponses) {
                    if (eResp instanceof KillUp && eResp.startIndex == rResp.startIndex) {
                        eDead = true;
                        break;
                    }
                }

                if (!eDead) {
                    et.killDown(rResp.startIndex);
                }

                respBuilder.add(new KillUp(this, rResp.startIndex));
            }
        }
    }

    public final Evaluator et;
    public final Evaluator rt;

}
