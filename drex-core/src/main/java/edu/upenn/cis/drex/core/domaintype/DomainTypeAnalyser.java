package edu.upenn.cis.drex.core.domaintype;

import automata.sfa.SFA;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import com.google.common.primitives.Chars;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Empty;
import edu.upenn.cis.drex.core.regex.Epsilon;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import edu.upenn.cis.drex.core.regex.Union;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Nullable;
import theory.CharPred;
import theory.CharSolver;

public class DomainTypeAnalyser {

    public static Supplier<DomainTypeAnalysisResult> bottom() {
        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                return new DomainTypeAnalysisResult(new Empty(), ImmutableSet.<Integer>of());
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> epsilon() {
        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                return new DomainTypeAnalysisResult(new Epsilon(), ImmutableSet.of(0));
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> symbol(final CharPred a) {
        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                ImmutableSet<Integer> domainStringLengths = ImmutableSet.of(1);

                Regex domainType = new Symbol(checkNotNull(a));
                if (domainType.getAmbiguity() != null) {
                    CharSolver solver = new CharSolver();
                    checkAssertion(!solver.IsSatisfiable(a));
                    String errorMsg = "Non-bottom expressions cannot have empty domain types.";
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }

                return new DomainTypeAnalysisResult(domainType, domainStringLengths);
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> splitSum(final Expression e1, final Expression e2) {
        checkNotNull(e1);
        checkNotNull(e2);

        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                ImmutableSet<Integer> domainStringLengths = null;
                if (e1.getDomainStringLengths() != null &&
                        e2.getDomainStringLengths() != null) {
                    ImmutableSet.Builder<Integer> builder = ImmutableSet.builder();
                    for (int l1 : e1.getDomainStringLengths()) {
                        for (int l2 : e2.getDomainStringLengths()) {
                            builder.add(l1 + l2);
                        }
                    }
                    domainStringLengths = builder.build();
                }

                Regex t1 = e1.getDomainType();
                Regex t2 = e2.getDomainType();

                if (t1 == null) {
                    DomainTypeError domainTypeError = DomainTypeError.untypedSubexpression(e1);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                } else if (t2 == null) {
                    DomainTypeError domainTypeError = DomainTypeError.untypedSubexpression(e2);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }

                Regex domainType = new Concat(t1, t2);
                if (domainType.getAmbiguity() != null) {
                    String errorMsg = String.format("Ambiguous concatenation of expressions\n"
                            + "%s\n"
                            + "and\n"
                            + "%s.\n"
                            + "%s", e1, e2, domainType.getAmbiguity().errorMsg);
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }

                return new DomainTypeAnalysisResult(domainType, domainStringLengths);
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> iteratedSum(final Expression e) {
        checkNotNull(e);
        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                ImmutableSet<Integer> domainStringLengths = null;

                Regex t = e.getDomainType();
                if (t == null) {
                    DomainTypeError domainTypeError = DomainTypeError.untypedSubexpression(e);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }

                Regex domainType = new Star(t);
                if (domainType.getAmbiguity() != null) {
                    String errorMsg = String.format("Ambiguous iteration of expression\n"
                            + "%s.\n"
                            + "%s", e, domainType.getAmbiguity().errorMsg);
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }

                return new DomainTypeAnalysisResult(domainType, domainStringLengths);
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> ifElse(final Expression e1, final Expression e2) {
        checkNotNull(e1);
        checkNotNull(e2);

        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                /* mutable */ ImmutableSet<Integer> domainStringLengths = null;
                if (e1.getDomainStringLengths() != null &&
                        e2.getDomainStringLengths() != null) {
                    ImmutableSet.Builder<Integer> builder = new ImmutableSet.Builder<>();
                    builder.addAll(e1.getDomainStringLengths());
                    builder.addAll(e2.getDomainStringLengths());
                    domainStringLengths = builder.build();
                }

                Regex t1 = e1.getDomainType();
                Regex t2 = e2.getDomainType();

                if (t1 == null) {
                    DomainTypeError domainTypeError = DomainTypeError.untypedSubexpression(e1);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                } else if (t2 == null) {
                    DomainTypeError domainTypeError = DomainTypeError.untypedSubexpression(e2);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }

                Regex domainType = new Union(t1, t2);
                if (domainType.getAmbiguity() != null) {
                    String errorMsg = String.format("Ambiguous conditional of expressions\n"
                            + "%s\n"
                            + "and\n"
                            + "%s.\n"
                            + "%s", e1, e2, domainType.getAmbiguity().errorMsg);
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }

                return new DomainTypeAnalysisResult(domainType, domainStringLengths);
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> chainedSum(final Expression e, final @Nullable Regex r) {
        checkNotNull(e);

        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                Regex rl = r;

                Regex edt = e.getDomainType();
                if (edt == null) {
                    DomainTypeError domainTypeError = DomainTypeError.untypedSubexpression(e);
                    return new DomainTypeAnalysisResult(domainTypeError, null);
                }
                checkAssertion(edt.getAmbiguity() == null && edt.getSFA() != null);

                if (rl == null && !(edt instanceof Concat)) {
                    String errorMsg = String.format("Could not infer splitting regex in "
                            + "expression\n"
                            + "%s\n"
                            + "with domain type\n"
                            + "%s.", e, edt);
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, null);
                } else if (rl == null) {
                    rl = ((Concat)edt).r1;
                }

                Regex r2 = new Concat(rl, rl);
                if (r2.getAmbiguity() != null) {
                    String errorMsg = String.format("Splitting regular expression\n"
                            + "%s\n"
                            + "is not unambiguously concatenable with itself.\n"
                            + "%s", rl, r2.getAmbiguity().errorMsg);
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, null);
                }
                checkAssertion(r2.getSFA() != null);

                if (!edt.getSFA().isEquivalentTo(r2.getSFA(), new CharSolver())) {
                    String errorMsg = String.format("Expression\n"
                            + "%s\n"
                            + "has domain type\n"
                            + "%s.\n"
                            + "This is not equivalent to the expected domain type\n"
                            + "%s", e, edt, r2);
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, null);
                }

                Regex rStar = new Star(rl);
                if (rStar.getAmbiguity() != null) {
                    String errorMsg = String.format("Regex\n"
                            + "%s\n"
                            + "is not unambiguously iterable.\n"
                            + "%s", e, rStar.getAmbiguity().errorMsg);
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, null);
                }

                Regex domainType = new Concat(r2, rStar);
                checkAssertion(domainType.getAmbiguity() == null && domainType.getSFA() != null);
                return new DomainTypeAnalysisResult(domainType, null);
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> sum(final Expression e1, final Expression e2) {
        checkNotNull(e1);
        checkNotNull(e2);

        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                ImmutableSet<Integer> domainStringLengths = null;
                if (e1.getDomainStringLengths() != null &&
                        e2.getDomainStringLengths() != null) {
                    Set<Integer> builder = new HashSet<>(e1.getDomainStringLengths());
                    builder.retainAll(e2.getDomainStringLengths());
                    domainStringLengths = ImmutableSet.copyOf(builder);
                }

                Regex t1 = e1.getDomainType();
                Regex t2 = e2.getDomainType();

                if (t1 == null) {
                    DomainTypeError domainTypeError = DomainTypeError.untypedSubexpression(e1);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                } else if (t2 == null) {
                    DomainTypeError domainTypeError = DomainTypeError.untypedSubexpression(e2);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }
                checkAssertion(t1.getAmbiguity() == null && t2.getAmbiguity() == null);
                checkAssertion(t1.getSFA() != null && t2.getSFA() != null);

                if (!t1.getSFA().isEquivalentTo(t2.getSFA(), new CharSolver())) {
                    SFA<CharPred, Character> lrDiffSFA = t1.getSFA().minus(t2.getSFA(), new CharSolver());
                    SFA<CharPred, Character> rlDiffSFA = t2.getSFA().minus(t1.getSFA(), new CharSolver());
                    checkAssertion(!lrDiffSFA.isEmpty() || !rlDiffSFA.isEmpty());

                    /* mutable */ String errorMsg;
                    if (!lrDiffSFA.isEmpty()) {
                        List<Character> s = lrDiffSFA.getWitness(new CharSolver());
                        checkAssertion(s != null);
                        String counterexample = new String(Chars.toArray(s));
                        errorMsg = String.format("On input\n" +
                                "\"%s\",\n" +
                                "expression\n" +
                                "%s\n" +
                                "is defined, but not expression\n" +
                                "%s.", counterexample, e1, e2);
                    } else {
                        checkAssertion(!rlDiffSFA.isEmpty());
                        List<Character> s = rlDiffSFA.getWitness(new CharSolver());
                        checkAssertion(s != null);
                        String counterexample = new String(Chars.toArray(s));
                        errorMsg = String.format("On input\n" +
                                "\"%s\",\n" +
                                "expression\n" +
                                "%s\n" +
                                "is defined, but not expression\n" +
                                "%s.", counterexample, e2, e1);
                    }

                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }

                return new DomainTypeAnalysisResult(t1, domainStringLengths);
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> composition(final Expression first, Expression second) {
        checkNotNull(first);
        checkNotNull(second);

        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf("Compositions are always ill-typed.");
                return new DomainTypeAnalysisResult(domainTypeError, first.getDomainStringLengths());
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> restrict(final Expression e, final Regex r) {
        checkNotNull(e);
        checkNotNull(r);

        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                ImmutableSet<Integer> domainStringLengths = e.getDomainStringLengths();

                Regex et = e.getDomainType();
                if (et == null) {
                    DomainTypeError domainTypeError = DomainTypeError.untypedSubexpression(e);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }
                checkAssertion(et.getSFA() != null);

                if (r.getAmbiguity() != null) {
                    String errorMsg = String.format("Ambiguous regular expression %s.\n"
                            + "%s", r, r.getAmbiguity().errorMsg);
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }
                checkAssertion(r.getSFA() != null);

                SFA<CharPred, Character> rMinusET = r.getSFA().minus(et.getSFA(), new CharSolver());
                if (!rMinusET.isEmpty()) {
                    List<Character> witness = rMinusET.getWitness(new CharSolver());
                    String witnessStr = new String(Chars.toArray(witness));
                    String errorMsg = String.format("Expression\n"
                            + "%s\n"
                            + "undefined on string\n"
                            + "\"%s\".\n"
                            + "This string is accepted by the regular expression\n"
                            + "%s.", e, witnessStr, r);
                    DomainTypeError domainTypeError = DomainTypeError.simpleErrorOf(errorMsg);
                    return new DomainTypeAnalysisResult(domainTypeError, domainStringLengths);
                }

                return new DomainTypeAnalysisResult(r, domainStringLengths);
            }
        };
    }

    public static Supplier<DomainTypeAnalysisResult> hole() {
        return new Supplier<DomainTypeAnalysisResult>() {
            @Override
            public DomainTypeAnalysisResult get() {
                String errorMsg = "The domain of a hole is undefined.";
                DomainTypeError error = DomainTypeError.simpleErrorOf(errorMsg);
                return new DomainTypeAnalysisResult(error, null);
            }
        };
    }

}
