package edu.upenn.cis.drex.core.dp;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import edu.upenn.cis.drex.core.expr.Composition;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IVisitor;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.Restrict;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.expr.Symbol;
import edu.upenn.cis.drex.core.regex.RegexEvaluator;
import edu.upenn.cis.drex.core.regex.RegexEvaluator.PathCount;
import static edu.upenn.cis.drex.core.regex.RegexEvaluator.addPathCounts;
import edu.upenn.cis.drex.core.util.TriHashTable;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.core.util.assertions.AssertionViolation;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import theory.CharFunc;
import theory.CharSolver;

public class DP {

    private DP(LazyString sigma) {
        this.sigma = checkNotNull(sigma);
        this.out = new TriHashTable<>();
        this.bel = new RegexEvaluator(sigma);
    }

    public static LazyString eval(Expression e, LazyString sigma) {
        DP dp = new DP(checkNotNull(sigma));
        return dp.eval(checkNotNull(e), 0, sigma.length, new CharSolver());
    }

    public static LazyString eval(final Expression e, final LazyString sigma,
            long timeout, TimeUnit unit)
            throws InterruptedException, TimeoutException {
        checkArgument(0 <= timeout);
        try {
            FutureTask<LazyString> task = new FutureTask<>(new Callable<LazyString>() {
                @Override
                public LazyString call() throws Exception {
                    return eval(e, sigma);
                }
            });
            // FutureTask<LazyString> task = new FutureTask<>(() -> eval(e, sigma));
            ExecutorService executor = Executors.newSingleThreadExecutor();
            executor.execute(task);
            return task.get(timeout, unit);
        } catch (ExecutionException ex) {
            throw new AssertionViolation(ex);
        }
    }

    public static String eval(Expression e, String sigma) {
        LazyString ans = eval(checkNotNull(e), LazyString.of(checkNotNull(sigma)));
        return ans != null ? ans.build() : null;
    }

    public static String eval(Expression e, String sigma, long timeout, TimeUnit unit)
    throws InterruptedException, TimeoutException {
        checkArgument(0 <= timeout);
        LazyString ans = eval(checkNotNull(e), LazyString.of(checkNotNull(sigma)), timeout, unit);
        return ans != null ? ans.build() : null;
    }

    public LazyString eval(Expression e, final int i, final int j, final CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);

        if (out.containsKey(e, i, j)) {
            return out.get(e, i, j);
        }

        if (e.getDomainStringLengths() != null &&
                !e.getDomainStringLengths().contains(j - i)) {
            out.put(e, i, j, null);
            return null;
        }

        final DP dp = this;
        IVisitor<LazyString> visitor = new IVisitor<LazyString>() {

            @Override
            public LazyString visitBottom(Bottom e) {
                return dp.evalBottom(e, i, j, solver);
            }

            @Override
            public LazyString visitSymbol(Symbol e) {
                return dp.evalSymbol(e, i, j, solver);
            }

            @Override
            public LazyString visitEpsilon(Epsilon e) {
                return dp.evalEpsilon(e, i, j, solver);
            }

            @Override
            public LazyString visitIfElse(IfElse e) {
                return dp.evalIfElse(e, i, j, solver);
            }

            @Override
            public LazyString visitSplitSum(SplitSum e) {
                return dp.evalSplitSum(e, i, j, solver);
            }

            @Override
            public LazyString visitSum(Sum e) {
                return dp.evalSum(e, i, j, solver);
            }

            @Override
            public LazyString visitIteratedSum(IteratedSum e) {
                return dp.evalIteratedSum(e, i, j, solver);
            }

            @Override
            public LazyString visitChainedSum(ChainedSum e) {
                return dp.evalChainedSum(e, i, j, solver);
            }

            @Override
            public LazyString visitComposition(Composition e) {
                return dp.evalComposition(e, i, j, solver);
            }

            @Override
            public LazyString visitRestrict(Restrict e) {
                return dp.evalRestrict(e, i, j, solver);
            }

        };

        out.put(e, i, j, visitor.visit(e));
        return out.get(e, i, j);
    }

    private LazyString evalBottom(Bottom e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);
        return null;
    }

    private LazyString evalEpsilon(Epsilon e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);
        return i == j ? LazyString.of(e.d) : null;
    }

    private LazyString evalSymbol(Symbol e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);
        char c = sigma.charAt(i);
        if (i + 1 == j && solver.HasModel(e.a, c)) {
            StringBuilder sb = new StringBuilder();
            for(CharFunc f : e.d) {
                sb.append(solver.MkSubstFuncConst(f, c));
            }
            return LazyString.of(sb.toString());
        } else {
            return null;
        }
    }

    private LazyString evalSplitSum(SplitSum e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);

        List<LazyString> ansList = new ArrayList<>();

        for (int k = i; k <= j; k++) {
            if (e.e1.getDomainStringLengths() != null &&
                    !e.e1.getDomainStringLengths().contains(k - i)) {
                continue;
            } else if (e.e2.getDomainStringLengths() != null &&
                    !e.e2.getDomainStringLengths().contains(j - k)) {
                continue;
            }

            LazyString tau1 = eval(e.e1, i, k, solver );
            if (tau1 != null) {
                LazyString tau2 = eval(e.e2, k, j, solver);
                if (tau2 != null) {
                    ansList.add(!e.left ? tau1.concat(tau2) : tau2.concat(tau1));
                }
            }
        }

        if (ansList.size() == 1) {
            return ansList.get(0);
        } else {
            return null;
        }
    }

    private LazyString evalIteratedSum(IteratedSum e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);

        PathCount[] positions = new PathCount[j - i + 1];
        Arrays.fill(positions, PathCount.NONE);
        positions[0] = PathCount.ONE;

        LazyString[] ans = new LazyString[j - i + 1];
        ans[0] = LazyString.of("");

        for (int ip = 0; ip < positions.length; ip++) {
            if (positions[ip] != PathCount.NONE) {
                for (int jp = ip; jp < positions.length; jp++) {
                    LazyString intRes = eval(e.e, i + ip, i + jp, solver);
                    if (intRes != null) {
                        positions[jp] = addPathCounts(positions[ip], positions[jp]);
                        ans[jp] = !e.left ? ans[ip].concat(intRes) : intRes.concat(ans[ip]);
                    }
                }
            }
        }

        if (positions[positions.length - 1] == PathCount.ONE) {
            checkAssertion(ans[positions.length - 1] != null);
            return ans[positions.length - 1];
        } else {
            return null;
        }
    }

    private LazyString evalSum(Sum e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);
        LazyString tau1 = eval(e.e1, i, j, solver);
        if (tau1 != null) {
            LazyString tau2 = eval(e.e2, i, j, solver);
            if (tau2 != null) {
                LazyString tau = tau1.concat(tau2);
                return tau;
            }
        }
        return null;
    }

    private LazyString evalIfElse(IfElse e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);
        LazyString tau1 = eval(e.e1, i, j, solver);
        if (tau1 != null) {
            return tau1;
        } else {
            return eval(e.e2, i, j, solver);
        }
    }

    private LazyString evalChainedSum(ChainedSum e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);

        PathCount[] positions = new PathCount[j - i + 1];
        Arrays.fill(positions, PathCount.NONE);
        positions[0] = PathCount.ONE;

        int[] prevSplit = new int[j - i + 1];
        Arrays.fill(prevSplit, -1);
        prevSplit[0] = 0;

        for (int ip = 0; ip < positions.length; ip++) {
            if (positions[ip] != PathCount.NONE) {
                for (int jp = ip; jp < positions.length; jp++) {
                    if (bel.belongsTo(i + ip, i + jp, e.r, solver)) {
                        positions[jp] = addPathCounts(positions[ip], positions[jp]);
                        if (positions[jp] == PathCount.ONE) {
                            prevSplit[jp] = ip;
                        } else {
                            prevSplit[jp] = -1;
                        }
                    }
                }
            }
        }

        if (positions[positions.length - 1] == PathCount.ONE) {
            int currSplit = positions.length - 1;
            int nextSplit = -1;
            int nextNextSplit = -1;
            LazyString ans = null;

            while (currSplit > 0) {
                if (nextSplit >= 0) { // Don't do the check in the first iteration
                    checkAssertion(prevSplit[nextSplit] == currSplit);
                }
                if (nextNextSplit >= 0) { // Don't do the check in the first two iterations
                    checkAssertion(prevSplit[nextNextSplit] == nextSplit);
                }

                checkAssertion(prevSplit[currSplit] < currSplit);
                nextNextSplit = nextSplit;
                nextSplit = currSplit;
                currSplit = prevSplit[currSplit];

                if (nextNextSplit >= 0) {
                    LazyString newAnsPatch = eval(e.e, i + currSplit, i + nextNextSplit, solver);
                    if (newAnsPatch == null) {
                        return null;
                    } else {
                        if (ans == null) { // Initialize ans in second iteration
                            ans = newAnsPatch;
                        } else { // Add to answer on remaining iterations
                            if (!e.left) { // Add on left or right
                                ans = newAnsPatch.concat(ans);
                            } else {
                                ans = ans.concat(newAnsPatch);
                            }
                        }
                    }
                }
            }
            return ans;
        } else {
            return null;
        }
    }

    private LazyString evalComposition(Composition e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);
        LazyString gamma = eval(e.first, i, j, solver);
        return gamma != null ? DP.eval(e.second, gamma) : null;
    }

    private LazyString evalRestrict(Restrict e, int i, int j, CharSolver solver) {
        checkNotNull(e);
        checkArgument(0 <= i && i <= j && j <= sigma.length);
        return bel.belongsTo(i, j, e.r, solver) ? eval(e.e, i, j, solver) : null;
    }

    public final LazyString sigma;
    private final /* nonfrozen */ TriHashTable<Expression, Integer, Integer, LazyString> out;
    public final RegexEvaluator bel;

}
