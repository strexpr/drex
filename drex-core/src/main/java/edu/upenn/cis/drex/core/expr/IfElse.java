package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import java.util.Objects;

public class IfElse extends Expression {

    public IfElse(Expression e1, Expression e2) {
        super(DomainTypeAnalyser.ifElse(checkNotNull(e1), checkNotNull(e2)),
                e1.size + e2.size + 1, ExpressionType.ELSE);
        this.e1 = e1;
        this.e2 = e2;
    }

    public static Expression of(Expression... ev) {
        if (checkNotNull(ev).length == 0) {
            return null;
        } else {
            Expression ans = ev[0];
            for (int i = 1; i < ev.length; i++) {
                ans = new IfElse(ans, checkNotNull(ev[i]));
            }
            return ans;
        }
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return ev.visitIfElse(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof IfElse) {
            IfElse ieObj = (IfElse)obj;
            return Objects.equals(e1, ieObj.e1) &&
                    Objects.equals(e2, ieObj.e2);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(IfElse.class, e1, e2);
    }

    public final Expression e1, e2;

    public static final BiFunction<Expression, Expression, Expression> IfElse =
        new BiFunction<Expression, Expression, Expression>() {

        @Override
        public Expression apply(Expression t, Expression u) {
            return new IfElse(t, u);
        }
    };

}
