package edu.upenn.cis.drex.core.regex;

import automata.sfa.SFA;
import automata.sfa.SFAMove;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import edu.upenn.cis.drex.core.domaintype.RegexDomain;
import static edu.upenn.cis.drex.core.regex.Regex.RegexType.EPSILON;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import theory.CharPred;
import theory.CharSolver;

public class Epsilon extends Regex {

    public Epsilon() {
        super(getSuperArgs());
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> rv) {
        return checkNotNull(rv).visitEpsilon(this);
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof Epsilon;
    }

    @Override
    public int hashCode() {
        return Objects.hash(Epsilon.class);
    }

    public static ConstructorArgs getSuperArgs() {
        Supplier<RegexDomain> domainSupplier = new Supplier<RegexDomain>(){
            @Override
            public RegexDomain get() {
                List<Integer> states = Arrays.asList(0);
                List<SFAMove<CharPred, Character>> transitions = Arrays.asList();
                return new RegexDomain(SFA.MkSFA(transitions, 0, states, new CharSolver()), null);
            }
        };
        return new ConstructorArgs(domainSupplier, 1, EPSILON);
    }

}
