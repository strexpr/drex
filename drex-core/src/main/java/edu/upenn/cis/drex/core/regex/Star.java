package edu.upenn.cis.drex.core.regex;

import edu.upenn.cis.drex.core.domaintype.RegexAmbiguity;
import automata.sfa.SFA;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.primitives.Chars;
import edu.upenn.cis.drex.core.domaintype.RegexDomain;
import static edu.upenn.cis.drex.core.regex.Regex.RegexType.STAR;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import theory.CharPred;
import theory.CharSolver;

public class Star extends Regex {

    public Star(Regex r) {
        super(getSuperArgs(checkNotNull(r)));
        this.r = r;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> rv) {
        return checkNotNull(rv).visitStar(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Star) {
            return Objects.equals(r, ((Star)obj).r);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Star.class, r);
    }

    public final Regex r;

    public static ConstructorArgs getSuperArgs(final Regex r) {
        int size = checkNotNull(r).size + 1;

        Supplier<RegexDomain> domainSupplier = new Supplier<RegexDomain>() {
            @Override
            public RegexDomain get() {
                if (r.getSFA() == null) {
                    checkAssertion(r.getAmbiguity() != null);
                    return new RegexDomain(null, RegexAmbiguity.ofAmbiguousSubexpression(r));
                }
                SFA<CharPred, Character> sfa = SFA.star(r.getSFA(), new CharSolver());
                checkAssertion(sfa != null);

                if (r.getAmbiguity() != null) {
                    return new RegexDomain(sfa, RegexAmbiguity.ofAmbiguousSubexpression(r));
                } 
                /*
                else if (r instanceof Empty) {
                    String errorMsg = String.format("Empty subexpression %s.", r);
                    return new RegexDomain(sfa, RegexAmbiguity.simpleErrorOf(errorMsg));
                }
                */

                if (r.getSFA().accepts(new ArrayList<Character>(), new CharSolver())) {
                    String errorMsg = String.format("Ambiguous iteration of expression\n" +
                            "%s\n" +
                            "on string\n" +
                            "\"\".", r);
                    return new RegexDomain(sfa, RegexAmbiguity.simpleErrorOf(errorMsg));
                }

                List<Character> ambig = sfa.getAmbiguousInput(new CharSolver());
                if (ambig != null) {
                    String counterexample = new String(Chars.toArray(ambig));
                    String errorMsg = String.format("Ambiguous iteration of expression\n" +
                            "%s\n" +
                            "on string\n" +
                            "\"%s\".", r, counterexample);
                    return new RegexDomain(sfa, RegexAmbiguity.simpleErrorOf(errorMsg));
                }

                return new RegexDomain(sfa, null);
            }
        };

        return new ConstructorArgs(domainSupplier, size, STAR);
    }

    public static Regex plus(Regex r) {
        checkNotNull(r);
        return new Concat(r, new Star(r));
    }

}
