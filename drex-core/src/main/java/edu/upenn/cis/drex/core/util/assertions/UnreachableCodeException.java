package edu.upenn.cis.drex.core.util.assertions;

public class UnreachableCodeException extends AssertionViolation {

    public UnreachableCodeException() {
    }

    public UnreachableCodeException(Object message) {
        super(String.valueOf(message));
    }

    public UnreachableCodeException(Object message, Throwable cause) {
        super(String.valueOf(message), cause);
    }

}
