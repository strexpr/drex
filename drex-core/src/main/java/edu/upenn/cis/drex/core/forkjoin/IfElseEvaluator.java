package edu.upenn.cis.drex.core.forkjoin;

import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.forkjoin.response.IVisitor;
import edu.upenn.cis.drex.core.regex.Union;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class IfElseEvaluator extends Evaluator {

    IfElseEvaluator(IfElse expr) {
        super(checkNotNull(expr));

        this.f = ExpressionEvaluatorBuilder.build(expr.e1);
        this.g = ExpressionEvaluatorBuilder.build(expr.e2);
        this.thf = new HashSet<>();
        this.thg = new HashSet<>();
    }

    IfElseEvaluator(Union regex) {
        super(checkNotNull(regex));

        this.f = RegexEvaluatorBuilder.build(regex.r1);
        this.g = RegexEvaluatorBuilder.build(regex.r2);
        this.thf = new HashSet<>();
        this.thg = new HashSet<>();
    }

    @Override
    void actualReset() {
        thf.clear();
        thg.clear();
        f.reset();
        g.reset();
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        checkArgument(0 <= index);

        thf.add(index);
        thg.add(index);

        List<Response> responses = new ArrayList<>();
        responses.addAll(f.start(index));
        responses.addAll(g.start(index));
        return process(ImmutableList.copyOf(responses));
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        checkArgument(0 <= index);

        List<Response> responses = new ArrayList<>();
        responses.addAll(f.symbol(index, symbol));
        responses.addAll(g.symbol(index, symbol));
        return process(ImmutableList.copyOf(responses));
    }

    @Override
    void actualKillDown(int index) {
        if (thf.contains(index)) {
            thf.remove(index);
            f.killDown(index);
        }
        if (thg.contains(index)) {
            thg.remove(index);
            g.killDown(index);
        }
    }

    private ImmutableList<Response> process(ImmutableList<Response> children) {
        final ImmutableList.Builder<Response> respBuilder = new ImmutableList.Builder<>();
        for (Response resp : children) {
            IVisitor<Void> visitor = new IVisitor<Void>() {

                @Override
                public Void visitResult(Result result) {
                    processResult(result, respBuilder);
                    return null;
                }

                @Override
                public Void visitKillUp(KillUp killUp) {
                    processKill(killUp, respBuilder);
                    return null;
                }

            };

            visitor.visit(checkNotNull(resp));
        }
        return respBuilder.build();
    }

    private void processResult(Result result, ImmutableList.Builder<Response> respBuilder) {
        checkNotNull(result);
        respBuilder.add(new Result(this, result.startIndex, result.length, result.value));
    }

    private void processKill(KillUp kill, ImmutableList.Builder<Response> respBuilder) {
        checkNotNull(kill);

        if (kill.origin == f) {
            thf.remove(kill.startIndex);
        } else {
            checkAssertion(kill.origin == g);
            thg.remove(kill.startIndex);
        }

        if (!thf.contains(kill.startIndex) && !thg.contains(kill.startIndex)) {
            respBuilder.add(new KillUp(this, kill.startIndex));
        }
    }

    public final Evaluator f, g;
    private final Set<Integer> thf, thg;

}
