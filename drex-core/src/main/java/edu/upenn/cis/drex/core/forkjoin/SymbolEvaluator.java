package     edu.upenn.cis.drex.core.forkjoin;

import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;
import theory.CharFunc;
import theory.CharPred;
import theory.CharSolver;

public class SymbolEvaluator extends Evaluator {

    SymbolEvaluator(edu.upenn.cis.drex.core.expr.Symbol e) {
        super(checkNotNull(e));
        this.a = e.a;
        this.d = e.d;
        this.th = null;
        this.thp = null;
    }

    SymbolEvaluator(edu.upenn.cis.drex.core.regex.Symbol regex) {
        super(checkNotNull(regex));
        this.a = regex.a;
        this.d = ImmutableList.of();
        this.th = null;
        this.thp = null;
    }

    @Override
    void actualReset() {
        th = null;
        thp = null;
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        // checkAssertion(th == null);
        th = index;
        return ImmutableList.of();
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        ImmutableList.Builder<Response> respBuilder = new ImmutableList.Builder<>();
        CharSolver solver = new CharSolver();
        
        if (thp != null) {
            respBuilder.add(new KillUp(this, thp));
            thp = null;
        }

        if (th != null && solver.HasModel(a, symbol)) {
            StringBuilder sb = new StringBuilder();
            for(CharFunc f: d)
                sb.append(solver.MkSubstFuncConst(f, symbol));
            
            LazyString ds = LazyString.of(sb.toString());
            respBuilder.add(new Result(this, th, 1, ds));
            thp = th;
            th = null;
        } else if (th != null) {
            respBuilder.add(new KillUp(this, th));
            th = null;
        }

        return respBuilder.build();
    }

    @Override
    void actualKillDown(int index) {
        if (th != null && th.intValue() == index) {
            th = null;
        } else if (thp != null && thp.intValue() == index) {
            thp = null;
        }
    }

    public final CharPred a;
    public final ImmutableList<CharFunc> d;
    private /* nonfinal */ Integer th, thp;

}
