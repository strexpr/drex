package edu.upenn.cis.drex.core.forkjoin.response;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.forkjoin.Evaluator;

public class KillUp extends Response {

    public KillUp(Evaluator origin, int startIndex) {
        super(checkNotNull(origin), startIndex);
    }

    @Override
    public String toString() {
        if (origin.expr != null) {
            return String.format("(KillUp %d %s)", startIndex, origin.expr.toString());
        } else {
            return String.format("(KillUp %d %s)", startIndex, origin.regex.toString());
        }
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> visitor) {
        return checkNotNull(visitor).visitKillUp(this);
    }

}
