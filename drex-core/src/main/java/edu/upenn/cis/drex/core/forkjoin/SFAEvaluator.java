package edu.upenn.cis.drex.core.forkjoin;

import automata.Move;
import automata.sfa.SFA;
import automata.sfa.SFAEpsilon;
import automata.sfa.SFAInputMove;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import edu.upenn.cis.drex.core.forkjoin.response.KillUp;
import edu.upenn.cis.drex.core.forkjoin.response.Response;
import edu.upenn.cis.drex.core.forkjoin.response.Result;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.util.lazystring.LazyString;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import theory.CharPred;

public class SFAEvaluator extends Evaluator {

    public SFAEvaluator(Regex r) {
        super(checkNotNull(r));
        this.currentStates = new HashMap<>();
        this.deadStates = getDeadStates(r.getSFA());
        this.epsilonClosure = getEpsilonClosure(r.getSFA());
    }

    @Override
    void actualReset() {
        currentStates.clear();
    }

    @Override
    ImmutableList<Response> actualStart(int index) {
        checkArgument(0 <= index);
        checkArgument(!currentStates.containsKey(index));

        int q0 = regex.getSFA().getInitialState();
        for (int otherStartIndex : currentStates.keySet()) {
            checkArgument(!currentStates.get(otherStartIndex).contains(q0));
        }

        if (deadStates.contains(q0)) {
            return ImmutableList.of((Response) new KillUp(this, index));
        } else {
            Set<Integer> initialStates = epsilonClosure.get(q0);
            currentStates.put(index, initialStates);

            for (int q : initialStates) {
                if (regex.getSFA().getFinalStates().contains(q)) {
                    return ImmutableList.of((Response) new Result(this, index, 0, LazyString.of("")));
                }
            }
            return ImmutableList.of();
        }
    }

    @Override
    ImmutableList<Response> actualSymbol(int index, char symbol) {
        ImmutableList.Builder<Response> responseBuilder = ImmutableList.builder();

        for (int startIndex : currentStates.keySet()) {
            Set<Integer> newStates = new HashSet<>();
            Collection<SFAInputMove<CharPred, Character>> transitions =
                    regex.getSFA().getInputMovesFrom(currentStates.get(startIndex));
            for (SFAInputMove<CharPred, Character> transition : transitions) {
                if (transition.guard.isSatisfiedBy(symbol)) {
                    for (int nextState : epsilonClosure.get(transition.to)) {
                        if (regex.getSFA().getFinalStates().contains(nextState)) {
                            int length = index - startIndex;
                            responseBuilder.add(new Result(this, startIndex, length, LazyString.of("")));
                        }

                        if (!deadStates.contains(nextState)) {
                            newStates.add(nextState);
                        }
                    }
                }
            }

            if (!newStates.isEmpty()) {
                currentStates.put(startIndex, ImmutableSet.copyOf(newStates));
            } else {
                currentStates.remove(startIndex);
                responseBuilder.add(new KillUp(this, startIndex));
            }
        }

        return responseBuilder.build();
    }

    @Override
    void actualKillDown(int index) {
        checkArgument(0 <= index);
        checkArgument(currentStates.containsKey(index));
        currentStates.remove(index);
    }

    private final Map<Integer, Set<Integer>> currentStates;
    private final Set<Integer> deadStates;
    private final Map<Integer, Set<Integer>> epsilonClosure;

    public static Set<Integer> getDeadStates(SFA<CharPred, Character> sfa) {
        checkNotNull(sfa);

        Set<Integer> aliveStates = Sets.newHashSet(sfa.getFinalStates());
        boolean fixpoint = false;
        while (!fixpoint) {
            fixpoint = true;

            for (Move<CharPred, Character> move : sfa.getMovesTo(aliveStates)) {
                if (!aliveStates.contains(move.from)) {
                    aliveStates.add(move.from);
                    fixpoint = false;
                }
            }
        }

        Set<Integer> ans = new HashSet<>();
        for (int q : sfa.getStates()) {
            if (!aliveStates.contains(q)) {
                ans.add(q);
            }
        }
        return ans;
    }

    public static Map<Integer, Set<Integer>> getEpsilonClosure(SFA<CharPred, Character> sfa) {
        checkNotNull(sfa);

        Collection<Integer> states = sfa.getStates();
        Map<Integer, Set<Integer>> ans = new HashMap<>();
        for (int q : states) {
            ans.put(q, Sets.newHashSet(q));
        }

        boolean fixpoint = false;
        while (!fixpoint) {
            fixpoint = true;

            for (int q : states) {
                for (SFAEpsilon<CharPred, Character> epsilonMove : sfa.getEpsilonFrom(ans.get(q))) {
                    int qPrime = epsilonMove.to;
                    if (!ans.get(q).contains(qPrime)) {
                        ans.get(q).add(qPrime);
                        fixpoint = false;
                    }
                }
            }
        }

        return ans;
    }

}
