package edu.upenn.cis.drex.core.util.lazystring;

import static com.google.common.base.Preconditions.checkNotNull;

class ActualString extends LazyString {

    public ActualString(String value) {
        super(checkNotNull(value).length());
        this.value = value;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> visitor) {
        return checkNotNull(visitor).visitActualString(this);
    }

    public final String value;

}
