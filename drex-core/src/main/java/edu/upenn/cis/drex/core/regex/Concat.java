package edu.upenn.cis.drex.core.regex;

import automata.sfa.SFA;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.primitives.Chars;
import edu.upenn.cis.drex.core.domaintype.RegexAmbiguity;
import edu.upenn.cis.drex.core.domaintype.RegexDomain;
import static edu.upenn.cis.drex.core.regex.Regex.RegexType.CONCAT;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.List;
import java.util.Objects;
import theory.CharPred;
import theory.CharSolver;

public class Concat extends Regex {

    public Concat(Regex r1, Regex r2) {
        super(getSuperArgs(checkNotNull(r1), checkNotNull(r2)));
        this.r1 = r1;
        this.r2 = r2;
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> rv) {
        return checkNotNull(rv).visitConcat(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Concat) {
            Concat objConcat = (Concat)obj;
            return Objects.equals(r1, objConcat.r1) &&
                    Objects.equals(r2, objConcat.r2);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(Concat.class, r1, r2);
    }

    public final Regex r1, r2;

    public static ConstructorArgs getSuperArgs(final Regex r1, final Regex r2) {
        checkNotNull(r1);
        checkNotNull(r2);

        int size = r1.size + r2.size + 1;

        Supplier<RegexDomain> domainSupplier = new Supplier<RegexDomain>() {
            @Override
            public RegexDomain get() {
                if (r1.getSFA() == null) {
                    checkAssertion(r1.getAmbiguity() != null);
                    return new RegexDomain(null, RegexAmbiguity.ofAmbiguousSubexpression(r1));
                } else if (r2.getSFA() == null) {
                    checkAssertion(r2.getAmbiguity() != null);
                    return new RegexDomain(null, RegexAmbiguity.ofAmbiguousSubexpression(r2));
                }
                SFA<CharPred, Character> sfa = r1.getSFA().concatenateWith(r2.getSFA(), new CharSolver());
                checkAssertion(sfa != null);

                if (r1.getAmbiguity() != null) {
                    return new RegexDomain(sfa, RegexAmbiguity.ofAmbiguousSubexpression(r1));
                } else if (r2.getAmbiguity() != null) {
                    return new RegexDomain(sfa, RegexAmbiguity.ofAmbiguousSubexpression(r2));
                } else if (r1 instanceof Empty) {
                    String errorMsg = String.format("Empty subexpression %s.", r1);
                    return new RegexDomain(sfa, RegexAmbiguity.simpleErrorOf(errorMsg));
                } else if (r2 instanceof Empty) {
                    String errorMsg = String.format("Empty subexpression %s.", r2);
                    return new RegexDomain(sfa, RegexAmbiguity.simpleErrorOf(errorMsg));
                }

                List<Character> ambig = sfa.getAmbiguousInput(new CharSolver());
                if (ambig != null) {
                    String counterexample = new String(Chars.toArray(ambig));
                    String errorMsg = String.format("Ambiguous concatenation of expressions\n" +
                            "%s\n" +
                            "and\n" +
                            "%s\n" +
                            "on string\n" +
                            "\"%s\".", r1, r2, counterexample);
                    return new RegexDomain(sfa, RegexAmbiguity.simpleErrorOf(errorMsg));
                }

                return new RegexDomain(sfa, null);
            }
        };

        return new ConstructorArgs(domainSupplier, size, CONCAT);
    }

    public static Regex of(Regex... rList) {
        Regex ans = new Epsilon();
        for (Regex r : rList) {
            checkArgument(r != null);
            ans = new Concat(ans, r);
        }
        return ans;
    }

}
