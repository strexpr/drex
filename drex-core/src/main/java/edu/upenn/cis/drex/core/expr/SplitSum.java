package edu.upenn.cis.drex.core.expr;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalyser;
import edu.upenn.cis.drex.core.util.function.BiFunction;
import java.util.Objects;

public class SplitSum extends Expression {

    public SplitSum(Expression e1, Expression e2, boolean left) {
        super(DomainTypeAnalyser.splitSum(checkNotNull(e1), checkNotNull(e2)),
                e1.size + e2.size + 1, 
                left ? ExpressionType.LSPLIT : ExpressionType.SPLIT);
        this.e1 = e1;
        this.e2 = e2;
        this.left = left;
    }

    public SplitSum(Expression e1, Expression e2) {
        this(checkNotNull(e1), checkNotNull(e2), false);
    }

    public static Expression of(Expression... ev) {
        /* Mutable */ Expression ans = new Epsilon("");
        for (int i = 0; i < checkNotNull(ev).length; i++) {
            ans = new SplitSum(ans, checkNotNull(ev[i]));
        }
        return ans;
    }

    public static Expression ofList(ImmutableList<Expression> ev, boolean left) {
        /* Mutable */ Expression ans = new Epsilon("");
        for (int i = 0; i < checkNotNull(ev).size(); i++) {
            ans = new SplitSum(ans, checkNotNull(ev.get(i)), left);
        }
        return ans;
    }

    public static Expression ofList(ImmutableList<Expression> ev) {
        return ofList(checkNotNull(ev), false);
    }

    @Override
    public <T> T invokeVisitor(IVisitor<T> ev) {
        return ev.visitSplitSum(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SplitSum) {
            SplitSum splitObj = (SplitSum)obj;
            return Objects.equals(e1, splitObj.e1) &&
                    Objects.equals(e2, splitObj.e2) &&
                    Objects.equals(left, splitObj.left);
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(SplitSum.class, e1, e2, left);
    }

    public final Expression e1, e2;
    public final boolean left;

    
    public final static BiFunction<Expression, Expression, Expression> RSplit =
        new BiFunction<Expression, Expression, Expression>() {

        @Override
        public Expression apply(Expression t, Expression u) {
            return new SplitSum(t, u, false);
        }
    };
    
    public static BiFunction<Expression, Expression, Expression> LSplit =
        new BiFunction<Expression, Expression, Expression>() {

        @Override
        public Expression apply(Expression t, Expression u) {
            return new SplitSum(t, u, true);
        }
    };

}
