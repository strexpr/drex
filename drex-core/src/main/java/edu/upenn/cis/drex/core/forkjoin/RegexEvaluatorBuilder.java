package edu.upenn.cis.drex.core.forkjoin;

import static com.google.common.base.Preconditions.checkArgument;
import edu.upenn.cis.drex.core.regex.Complement;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Empty;
import edu.upenn.cis.drex.core.regex.Epsilon;
import edu.upenn.cis.drex.core.regex.IVisitor;
import edu.upenn.cis.drex.core.regex.Intersection;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import edu.upenn.cis.drex.core.regex.Union;

public class RegexEvaluatorBuilder extends IVisitor<Evaluator> {

    @Deprecated
    public static Evaluator build(Regex regex) {
        if (ExpressionEvaluatorBuilder.FAIL_FLAG) {
            throw new UnsupportedOperationException("Regex evaluators are broken in the presence of restrict!");
        } else {
            RegexEvaluatorBuilder builder = new RegexEvaluatorBuilder();
            checkArgument(regex != null && regex.getAmbiguity() == null);
            return builder.visit(regex);
        }
    }

    @Override
    public Evaluator visitEmpty(Empty regex) {
        checkArgument(regex != null && regex.getAmbiguity() == null);
        return new BottomEvaluator(regex);
    }

    @Override
    public Evaluator visitEpsilon(Epsilon regex) {
        checkArgument(regex != null && regex.getAmbiguity() == null);
        return new EpsilonEvaluator(regex);
    }

    @Override
    public Evaluator visitSymbol(Symbol regex) {
        checkArgument(regex != null && regex.getAmbiguity() == null);
        return new SymbolEvaluator(regex);
    }

    @Override
    public Evaluator visitUnion(Union regex) {
        checkArgument(regex != null && regex.getAmbiguity() == null);
        return new IfElseEvaluator(regex);
    }

    @Override
    public Evaluator visitConcat(Concat regex) {
        checkArgument(regex != null && regex.getAmbiguity() == null);
        return new SplitSumEvaluator(regex);
    }

    @Override
    public Evaluator visitStar(Star regex) {
        checkArgument(regex != null && regex.getAmbiguity() == null);
        return new IteratedSumEvaluator(regex);
    }

    @Override
    public Evaluator visitComplement(Complement regex) {
        checkArgument(regex != null && regex.getAmbiguity() == null);
        return new SFAEvaluator(regex);
    }

    @Override
    public Evaluator visitIntersection(Intersection regex) {
        checkArgument(regex != null && regex.getAmbiguity() == null);
        return new SFAEvaluator(regex);
    }

}
