package edu.upenn.cis.drex.core.regex;

import automata.sfa.SFA;
import automata.sfa.SFAInputMove;
import automata.sfa.SFAMove;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.SplitSum;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import theory.CharFunc;
import theory.CharPred;
import theory.CharSolver;
import theory.StdCharPred;

public class RegexUtil {

    public static int getSizeOf(Regex r) {
        SizeVisitor visitor = new SizeVisitor();
        return visitor.visit(checkNotNull(r));
    }

    public static Regex stringToRegex(String s) {
        checkNotNull(s);
        Regex ans = new Epsilon();
        for (int i = 0; i < s.length(); i++) {
            Regex si = new Symbol(new CharPred(s.charAt(i)));
            ans = new Concat(ans, si);
        }
        return ans;
    }

    public static Expression regexToExpression(Regex r) {
        checkArgument(r != null);
        RegexExprTranslator visitor = new RegexExprTranslator();
        Expression ans = visitor.visit(r);
        // checkAssertion(ans.getDomainType() != null);
        return ans;
    }

    public static ImmutableSet<Regex> subRegexes(Regex r) {
        return new SubRegexVisitor().visit(checkNotNull(r));
    }

    public static Regex of(SFA<CharPred, Character> sfa) {
        CharSolver solver = new CharSolver();
        sfa = checkNotNull(sfa).determinize(solver).minimize(solver);

        Collection<Integer> states = sfa.getStates();
        Table<Integer, Integer, Regex> table = HashBasedTable.create();
        for (int i : states) {
            for (int j : states) {
                CharPred pij = StdCharPred.FALSE;
                Set<SFAMove<CharPred, Character>> fromMoves = new HashSet<>(sfa.getTransitionsFrom(i));
                Set<SFAMove<CharPred, Character>> toMoves = new HashSet<>(sfa.getTransitionsTo(j));
                ImmutableSet<SFAMove<CharPred, Character>> moves = Sets.intersection(fromMoves, toMoves).immutableCopy();
                for (SFAMove<CharPred, Character> move : moves) {
                    SFAInputMove<CharPred, Character> typedMove = (SFAInputMove<CharPred, Character>)move;
                    pij = solver.MkOr(pij, typedMove.guard);
                }

                Regex pijr = new Symbol(pij);
                checkAssertion(pijr.getSFA() != null);
                table.put(i, j, pijr);
            }
        }

        for (int iteration = 0; iteration < states.size(); iteration++) {
            Table<Integer, Integer, Regex> tablePrime = HashBasedTable.create();

            for (int i : states) {
                for (int j : states) {
                    Regex r = table.get(i, j);
                    for (int k : states) {
                        Regex rik = table.get(i, k);
                        Regex rkk = table.get(k, k);
                        Regex rkj = table.get(k, j);
                        checkAssertion(rik != null && rkk != null && rkj != null);

                        if (rik instanceof Empty || rkk instanceof Empty || rkj instanceof Empty) {
                            continue;
                        }

                        Regex rkkStar = new Star(rkk);
                        Regex newPath = rik;
                        newPath = newPath instanceof Epsilon ? rkkStar : new Concat(newPath, rkkStar);
                        newPath = rkj instanceof Epsilon ? newPath : new Concat(newPath, rkj);
                        r = r instanceof Empty ? newPath : new Union(r, newPath);
                        checkAssertion(r.getSFA() != null);
                    }
                    tablePrime.put(i, j, r);
                }
            }

            table = tablePrime;
        }

        int initialState = sfa.getInitialState();
        Regex ans = sfa.getFinalStates().contains(initialState) ? new Epsilon() : new Empty();
        for (int finalState : sfa.getFinalStates()) {
            if (ans instanceof Empty) {
                ans = table.get(initialState, finalState);
            } else {
                ans = new Union(ans, table.get(initialState, finalState));
            }
        }
        return ans;
    }

}

class SizeVisitor extends IVisitor<Integer> {

    @Override
    public Integer visitEmpty(Empty r) {
        return 1;
    }

    @Override
    public Integer visitEpsilon(Epsilon r) {
        return 1;
    }

    @Override
    public Integer visitSymbol(Symbol r) {
        return 1;
    }

    @Override
    public Integer visitUnion(Union r) {
        return 1 + RegexUtil.getSizeOf(r.r1) + RegexUtil.getSizeOf(r.r2);
    }

    @Override
    public Integer visitConcat(Concat r) {
        return 1 + RegexUtil.getSizeOf(r.r1) + RegexUtil.getSizeOf(r.r2);
    }

    @Override
    public Integer visitStar(Star r) {
        return 1 + RegexUtil.getSizeOf(r.r);
    }

    @Override
    public Integer visitComplement(Complement r) {
        return 1 + RegexUtil.getSizeOf(r.r);
    }

    @Override
    public Integer visitIntersection(Intersection r) {
        return 1 + RegexUtil.getSizeOf(r.r1) + RegexUtil.getSizeOf(r.r2);
    }

}

class RegexExprTranslator extends IVisitor<Expression> {

    @Override
    public Expression visitEmpty(Empty r) {
        return new Bottom();
    }

    @Override
    public Expression visitEpsilon(Epsilon r) {
        return new edu.upenn.cis.drex.core.expr.Epsilon("");
    }

    @Override
    public Expression visitSymbol(Symbol r) {
        checkNotNull(r);
        ImmutableList<CharFunc> fEpsilon = ImmutableList.of();
        return new edu.upenn.cis.drex.core.expr.Symbol(r.a, fEpsilon);
    }

    @Override
    public Expression visitUnion(Union r) {
        return new IfElse(visit(checkNotNull(r).r1), visit(r.r2));
    }

    @Override
    public Expression visitConcat(Concat r) {
        return new SplitSum(visit(checkNotNull(r).r1), visit(r.r2));
    }

    @Override
    public Expression visitStar(Star r) {
        return new IteratedSum(visit(checkNotNull(r).r));
    }

    @Override
    public Expression visitComplement(Complement r) {
        return visit(checkNotNull(r).makeSimple());
    }

    @Override
    public Expression visitIntersection(Intersection r) {
        return visit(checkNotNull(r).makeSimple());
    }

}

class SubRegexVisitor extends IVisitor<ImmutableSet<Regex>> {

    @Override
    public ImmutableSet<Regex> visitEmpty(Empty r) {
        return ImmutableSet.of((Regex)checkNotNull(r));
    }

    @Override
    public ImmutableSet<Regex> visitEpsilon(Epsilon r) {
        return ImmutableSet.of((Regex)checkNotNull(r));
    }

    @Override
    public ImmutableSet<Regex> visitSymbol(Symbol r) {
        return ImmutableSet.of((Regex)checkNotNull(r));
    }

    @Override
    public ImmutableSet<Regex> visitUnion(Union r) {
        ImmutableSet.Builder<Regex> ans = ImmutableSet.builder();
        ans.add(checkNotNull(r));
        ans.addAll(RegexUtil.subRegexes(r.r1));
        ans.addAll(RegexUtil.subRegexes(r.r2));
        return ans.build();
    }

    @Override
    public ImmutableSet<Regex> visitConcat(Concat r) {
        ImmutableSet.Builder<Regex> ans = ImmutableSet.builder();
        ans.add(checkNotNull(r));
        ans.addAll(RegexUtil.subRegexes(r.r1));
        ans.addAll(RegexUtil.subRegexes(r.r2));
        return ans.build();
    }

    @Override
    public ImmutableSet<Regex> visitStar(Star r) {
        ImmutableSet.Builder<Regex> ans = ImmutableSet.builder();
        ans.add(checkNotNull(r));
        ans.addAll(RegexUtil.subRegexes(r.r));
        return ans.build();
    }

    @Override
    public ImmutableSet<Regex> visitComplement(Complement r) {
        ImmutableSet.Builder<Regex> ans = ImmutableSet.builder();
        ans.add(checkNotNull(r));
        ans.addAll(RegexUtil.subRegexes(r.r));
        return ans.build();
    }

    @Override
    public ImmutableSet<Regex> visitIntersection(Intersection r) {
        ImmutableSet.Builder<Regex> ans = ImmutableSet.builder();
        ans.add(checkNotNull(r));
        ans.addAll(RegexUtil.subRegexes(r.r1));
        ans.addAll(RegexUtil.subRegexes(r.r2));
        return ans.build();
    }

}