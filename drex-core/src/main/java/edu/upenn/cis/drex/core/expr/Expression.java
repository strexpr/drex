package edu.upenn.cis.drex.core.expr;

import automata.sfa.SFA;
import com.google.common.base.Joiner;
import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.collect.ImmutableSet;
import theory.CharPred;
import theory.CharSolver;
import theory.CharFunc;
import edu.upenn.cis.drex.core.domaintype.DomainTypeAnalysisResult;
import edu.upenn.cis.drex.core.domaintype.DomainTypeError;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.util.Memo;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Nullable;
import transducers.sst.CharConstant;
import transducers.sst.CharFunction;
import transducers.sst.ConstantToken;
import transducers.sst.SST;
import transducers.sst.Token;

public abstract class Expression {

    public Expression(Supplier<DomainTypeAnalysisResult> domainTypeSupplier, int size, ExpressionType type) {
        checkArgument(size > 0);
        this.domainTypeSupplier = new Memo<>(checkNotNull(domainTypeSupplier));
        this.size = size;
        this.type = type;
    }

    public abstract <T> T invokeVisitor(IVisitor<T> ev);

    @Override
    public abstract boolean equals(Object obj);

    @Override
    public abstract int hashCode();

    public SST<CharPred, CharFunc, Character> getSST(final CharSolver ba) {
        IVisitor<SST<CharPred, CharFunc, Character>> visitor = new IVisitor<SST<CharPred, CharFunc, Character>>() {

            @Override
            public SST<CharPred, CharFunc, Character> visitBottom(Bottom e) {
                return SST.getEmptySST(ba);
            }

            @Override
            public SST<CharPred, CharFunc, Character> visitSymbol(Symbol e) {
                List<Token<CharPred, CharFunc, Character>> tokens = new ArrayList<>();
                for (CharFunc cf : e.d) {
                    tokens.add(new CharFunction<CharPred, CharFunc, Character>(cf));
                }
                return SST.getBaseSST(e.a, tokens,ba);
            }

            @Override
            public SST<CharPred, CharFunc, Character> visitEpsilon(Epsilon e) {
                List<ConstantToken<CharPred, CharFunc, Character>> tokens = new ArrayList<>();
                for(Character c : e.d.toCharArray()) {
                    tokens.add(new CharConstant<CharPred, CharFunc, Character>(c));
                }
                return SST.getEpsilonSST(tokens, ba);
            }

            @Override
            public SST<CharPred, CharFunc, Character> visitIfElse(IfElse e) {
                SST<CharPred, CharFunc, Character> sst1 = e.e1.getSST(ba);
                SST<CharPred, CharFunc, Character> sst2 = e.e2.getSST(ba);
                checkAssertion(sst1 != null && sst2 != null);
                return sst1.unionWith(sst2, ba);
            }

            @Override
            public SST<CharPred, CharFunc, Character> visitSplitSum(SplitSum e) {
                SST<CharPred, CharFunc, Character> sst1 = e.e1.getSST(ba);
                SST<CharPred, CharFunc, Character> sst2 = e.e2.getSST(ba);
                checkAssertion(sst1 != null && sst2 != null);
                return sst1.concatenateWith(sst2, ba);
            }

            @Override
            public SST<CharPred, CharFunc, Character> visitSum(Sum e) {
                SST<CharPred, CharFunc, Character> sst1 = e.e1.getSST(ba);
                SST<CharPred, CharFunc, Character> sst2 = e.e2.getSST(ba);
                checkAssertion(sst1 != null && sst2 != null);
                return sst1.combineWith(sst2, ba);
            }

            @Override
            public SST<CharPred, CharFunc, Character> visitIteratedSum(IteratedSum e) {
                SST<CharPred,CharFunc,Character> sst = e.e.getSST(ba);
                checkAssertion(sst != null);
                return !e.left ? sst.star(ba) : sst.leftStar(ba);
            }

            @Override
            public SST<CharPred, CharFunc, Character> visitChainedSum(ChainedSum e) {
                SST<CharPred, CharFunc, Character> sst = e.e.getSST(ba);
                SFA<CharPred, Character> sfa = e.r.getSFA();
                checkAssertion(sst != null && sfa != null);
                return SST.computeShuffle(sst, sfa, ba, e.left);
            }

            @Override
            public SST<CharPred, CharFunc, Character> visitComposition(Composition e) {
                throw new UnsupportedOperationException("Not supported yet."); 
            }

            @Override
            public SST<CharPred, CharFunc, Character> visitRestrict(Restrict e) {
                SST<CharPred, CharFunc, Character> sst = e.e.getSST(ba);
                SFA<CharPred, Character> sfa = e.r.getSFA();
                checkAssertion(sst != null && sfa != null);
                return sst.restrictInput(sfa, ba);
            }

        };

        return visitor.visit(this);
    }

    @Override
    public String toString() {
        IVisitor<String> visitor = new IVisitor<String>() {

            @Override
            public String visitBottom(Bottom e) {
                return "bot";
            }

            @Override
            public String visitSymbol(Symbol e) {
                List<String> fns = new ArrayList<>();
                for (CharFunc f : e.d) {
                    fns.add(f.toString());
                }
                String fn = "[" + Joiner.on(", ").join(fns) + "]";
                return String.format("(symbol %s %s)", e.a, fn);
            }

            @Override
            public String visitEpsilon(Epsilon e) {
                return String.format("(epsilon \"%s\")", e.d);
            }

            @Override
            public String visitIfElse(IfElse e) {
                String se1 = e.e1.toString();
                String se2 = e.e2.toString();
                checkAssertion(se1 != null && se2 != null);
                return String.format("(ifelse %s %s)", se1, se2);
            }

            @Override
            public String visitSplitSum(SplitSum e) {
                String se1 = e.e1.toString();
                String se2 = e.e2.toString();
                checkAssertion(se1 != null && se2 != null);
                if (!e.left) {
                    return String.format("(split %s %s)", se1, se2);
                } else {
                    return String.format("(left-split %s %s)", se1, se2);
                }
            }

            @Override
            public String visitSum(Sum e) {
                String se1 = e.e1.toString();
                String se2 = e.e2.toString();
                checkAssertion(se1 != null && se2 != null);
                return String.format("(combine %s %s)", se1, se2);
            }

            @Override
            public String visitIteratedSum(IteratedSum e) {
                if (!e.left) {
                    return String.format("(iter %s)", e.e);
                } else {
                    return String.format("(left-iter %s)", e.e);
                }
            }

            @Override
            public String visitChainedSum(ChainedSum e) {
                String opname = (e.left ? "left-" : "") + "chained-sum";
                String se = e.e.toString();
                String sr = e.r.toString();
                checkAssertion(opname != null && se != null && sr != null);
                return String.format("(%s %s %s)", opname, se, sr);
            }

            @Override
            public String visitComposition(Composition e) {
                String sf = e.first.toString();
                String ss = e.second.toString();
                checkAssertion(sf != null && ss != null);
                return String.format("(andthen %s %s)", sf, ss);
            }

            @Override
            public String visitRestrict(Restrict e) {
                String se = e.e.toString();
                String sr = e.r.toString();
                checkAssertion(se != null && sr != null);
                return String.format("(restrict %s %s)", se, sr);
            }

        };

        return visitor.visit(this);
    }

    public @Nullable Regex getDomainType() {
        return domainTypeSupplier.get().domainType;
    }

    public @Nullable DomainTypeError getDomainTypeError() {
        return domainTypeSupplier.get().domainTypeError;
    }

    public @Nullable ImmutableSet<Integer> getDomainStringLengths() {
        return domainTypeSupplier.get().domainStringLengths;
    }

    public enum ExpressionType {
        SYMBOL,
        SPLIT,
        LSPLIT,
        ITER,
        LITER,
        SUM,
        ELSE,
        RESTRICT,
        CHAINSUM,
        LCHAINSUM,
        BOTTOM,
        EPSILON,
        COMPOSE
    }

    public final Memo<DomainTypeAnalysisResult> domainTypeSupplier;
    public final int size;
    public final ExpressionType type;
}
