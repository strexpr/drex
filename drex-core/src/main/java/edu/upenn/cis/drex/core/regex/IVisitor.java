package edu.upenn.cis.drex.core.regex;

import static com.google.common.base.Preconditions.checkNotNull;

public abstract class IVisitor<T> {

    public T visit(Regex r) {
        return checkNotNull(r).invokeVisitor(this);
    }

    public abstract T visitEmpty(Empty r);
    public abstract T visitEpsilon(Epsilon r);
    public abstract T visitSymbol(Symbol r);
    public abstract T visitUnion(Union r);
    public abstract T visitConcat(Concat r);
    public abstract T visitStar(Star r);
    public abstract T visitComplement(Complement r);
    public abstract T visitIntersection(Intersection r);

}
