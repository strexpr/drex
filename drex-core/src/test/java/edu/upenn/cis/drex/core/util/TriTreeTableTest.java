package edu.upenn.cis.drex.core.util;

import org.junit.Test;
import static org.junit.Assert.*;

public class TriTreeTableTest {

    @Test
    public void test1() {
        final TriHashTable<Integer, String, Boolean, Character> table = new TriHashTable<>();
        final Character val1 = 'Z', val2 = 'Y', val3 = 'X';

        assertFalse(table.containsKey(0, "a", true));
        assertNull(table.put(0, "a", true, val1));
        assertTrue(table.containsKey(0, "a", true));
        assertFalse(table.containsKey(0, "b", true));

        assertEquals(val1, table.get(0, "a", true));
        assertEquals(val1, table.put(0, "a", true, val2));
        assertEquals(val2, table.get(0, "a", true));
        assertFalse(table.containsKey(0, "b", true));

        assertNull(table.put(0, "b", true, val3));
        assertTrue(table.containsKey(0, "a", true));
        assertTrue(table.containsKey(0, "b", true));
        assertEquals(val2, table.get(0, "a", true));
        assertEquals(val3, table.get(0, "b", true));
    }

}
