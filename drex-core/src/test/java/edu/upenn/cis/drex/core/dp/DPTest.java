package edu.upenn.cis.drex.core.dp;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.expr.Symbol;
import edu.upenn.cis.drex.core.domaintype.TypeCheckingException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import static org.junit.Assert.assertNotNull;
import theory.CharOffset;
import theory.CharPred;
import theory.CharSolver;

public class DPTest {

    final theory.CharSolver solver = new theory.CharSolver();
    final Expression ae = new Symbol(new CharPred('0', '9'), "");
    final Expression aa = new Symbol(new CharPred('0', '9'), CharOffset.IDENTITY);
    final Expression be = new Symbol(new CharPred('b'), "");
    final Expression bb = new Symbol(new CharPred('b'), "b");
    final Symbol ab = new Symbol(new CharPred('a'), "b");
    final Symbol ba = new Symbol(new CharPred('b'), "a");

    @Test
    public void testBasicBottom() {
        Expression e = new Bottom();
        assertNull(DP.eval(e, ""));
        assertNull(DP.eval(e, "a"));
        assertNull(DP.eval(e, "b"));
        assertNull(DP.eval(e, "ab"));
        assertNull(DP.eval(e, "aaaaabbbbb"));
    }

    @Test
    public void testBasicSymbol() {
        Symbol tab = new Symbol(new CharPred('a'), "b");
        assertNull(DP.eval(tab, ""));
        assertEquals(DP.eval(tab, "a"), "b");
        assertNull(DP.eval(tab, "aa"));
        assertNull(DP.eval(tab, "b"));
        assertNull(DP.eval(tab, "ab"));
        assertNull(DP.eval(tab, "ba"));
        assertNull(DP.eval(tab, "bb"));

        Symbol tba = new Symbol(new CharPred('b'), "a");
        assertNull(DP.eval(tba, ""));
        assertEquals(DP.eval(tba, "b"), "a");
        assertNull(DP.eval(tba, "aa"));
        assertNull(DP.eval(tba, "a"));
        assertNull(DP.eval(tba, "ab"));
        assertNull(DP.eval(tba, "ba"));
        assertNull(DP.eval(tba, "bb"));
    }

    @Test
    public void testBasicDisjointUnion() {
        IfElse tabba = new IfElse(ab, ba);
        assertNull(DP.eval(tabba, ""));
        assertEquals(DP.eval(tabba, "b"), "a");
        assertNull(DP.eval(tabba, "aa"));
        assertEquals(DP.eval(tabba, "a"), "b");
        assertNull(DP.eval(tabba, "ab"));
        assertNull(DP.eval(tabba, "ba"));
        assertNull(DP.eval(tabba, "bb"));
    }

    @Test
    public void testBasicEpsilon() {
        Epsilon tef = new Epsilon("f");
        assertEquals(DP.eval(tef, ""), "f");
        assertNull(DP.eval(tef, "a"));
        assertNull(DP.eval(tef, "b"));
        assertNull(DP.eval(tef, "aa"));
        assertNull(DP.eval(tef, "ab"));
        assertNull(DP.eval(tef, "ba"));
        assertNull(DP.eval(tef, "bb"));
    }

    @Test
    public void testBasicIteratedSum() {
        Symbol ab = new Symbol(new CharPred('a'), "b");
        Symbol ba = new Symbol(new CharPred('b'), "a");
        IfElse abba = new IfElse(ab, ba);

        IteratedSum tabs = new IteratedSum(ab);
        assertEquals(DP.eval(tabs, ""), "");
        assertEquals(DP.eval(tabs, "a"), "b");
        assertNull(DP.eval(tabs, "b"));
        assertEquals(DP.eval(tabs, "aa"), "bb");
        assertNull(DP.eval(tabs, "ab"));
        assertNull(DP.eval(tabs, "ba"));
        assertNull(DP.eval(tabs, "bb"));
        assertNull(DP.eval(tabs, "aaaaababa"));
        assertEquals(DP.eval(tabs, "aaaaa"), "bbbbb");

        IteratedSum tabbas = new IteratedSum(abba);
        assertEquals(DP.eval(tabbas, ""), "");
        assertEquals(DP.eval(tabbas, "a"), "b");
        assertEquals(DP.eval(tabbas, "b"), "a");
        assertEquals(DP.eval(tabbas, "aa"), "bb");
        assertEquals(DP.eval(tabbas, "ab"), "ba");
        assertEquals(DP.eval(tabbas, "ba"), "ab");
        assertEquals(DP.eval(tabbas, "bb"), "aa");
        assertEquals(DP.eval(tabbas, "aaaababa"), "bbbbabab");
    }

    @Test
    public void testBasicSplitSum() {
        Symbol ab = new Symbol(new CharPred('a'), "b");
        Symbol ba = new Symbol(new CharPred('b'), "a");
        IfElse abba = new IfElse(ab, ba);
        IteratedSum abbas = new IteratedSum(abba);

        Symbol aa = new Symbol(new CharPred('a'), "a");
        Symbol bb = new Symbol(new CharPred('b'), "b");
        IfElse aabb = new IfElse(aa, bb);
        IteratedSum aabbs = new IteratedSum(aabb);

        Symbol de = new Symbol(new CharPred('d'), "");
        SplitSum deaabbs = new SplitSum(de, aabbs);
        SplitSum t = new SplitSum(abbas, deaabbs);

        assertNull(DP.eval(t, ""));
        assertNull(DP.eval(t, "a"));
        assertNull(DP.eval(t, "b"));
        assertEquals(DP.eval(t, "d"), "");
        assertNull(DP.eval(t, "aa"));
        assertNull(DP.eval(t, "ab"));
        assertNull(DP.eval(t, "ba"));
        assertNull(DP.eval(t, "bb"));
        assertEquals(DP.eval(t, "aabbadabba"), "bbaababba");
    }

    @Test
    public void testBasicSum() {
        CharSolver solver = new CharSolver();
        IteratedSum id = new IteratedSum(new Symbol(solver.True(), CharOffset.IDENTITY));
        Sum t = new Sum(id, id);

        assertEquals(DP.eval(t, ""), "");
        assertEquals(DP.eval(t, "a"), "aa");
        assertEquals(DP.eval(t, "b"), "bb");
        assertEquals(DP.eval(t, "aa"), "aaaa");
        assertEquals(DP.eval(t, "ab"), "abab");
        assertEquals(DP.eval(t, "ba"), "baba");
        assertEquals(DP.eval(t, "bb"), "bbbb");
    }

    @Test
    public void testShuffle() throws TypeCheckingException {
        final Expression aes = new IteratedSum(ae);
        final Expression aas = new IteratedSum(aa);

        final Expression aesbe = new SplitSum(aes, be);
        final Expression aasbb = new SplitSum(aas, bb);

        final SplitSum echoR = new SplitSum(aesbe, aasbb);
        final SplitSum echoL = new SplitSum(aasbb, aesbe);

        final Sum subexprList = new Sum(echoR, echoL);
        final ChainedSum shuffle = new ChainedSum(subexprList);

        assertNull(DP.eval(shuffle, ""));
        String s = DP.eval(shuffle, "1b2b3b4b5b6b7b8b9b10b11b12b13b");
        assertEquals("2b1b3b2b4b3b5b4b6b5b7b6b8b7b9b8b10b9b11b10b12b11b13b12b", s);
    }

    @Test
    public void testShuffleTimeout1()
            throws InterruptedException, TimeoutException, TypeCheckingException {

        final Expression aes = new IteratedSum(ae);
        final Expression aas = new IteratedSum(aa);

        final Expression aesbe = new SplitSum(aes, be);
        final Expression aasbb = new SplitSum(aas, bb);

        final SplitSum echoR = new SplitSum(aesbe, aasbb);
        final SplitSum echoL = new SplitSum(aasbb, aesbe);

        final Sum subexprList = new Sum(echoR, echoL);
        final ChainedSum shuffle = new ChainedSum(subexprList);

        String shortInput = "1b2b3b4b5b6b7b8b9b10b11b12b13b";
        assertNotNull(DP.eval(shuffle, shortInput, 30, TimeUnit.SECONDS));
    }

    @Test(expected = TimeoutException.class)
    public void testShuffleTimeout2()
            throws InterruptedException, TimeoutException, TypeCheckingException {

        final Expression aes = new IteratedSum(ae);
        final Expression aas = new IteratedSum(aa);

        final Expression aesbe = new SplitSum(aes, be);
        final Expression aasbb = new SplitSum(aas, bb);

        final SplitSum echoR = new SplitSum(aesbe, aasbb);
        final SplitSum echoL = new SplitSum(aasbb, aesbe);

        final Sum subexprList = new Sum(echoR, echoL);
        final ChainedSum shuffle = new ChainedSum(subexprList);

        String longInput = "1b2b3b4b5b6b7b8b9b10b11b12b13b";
        for (int i = 0; i < 10; i++) {
            longInput = longInput + longInput;
        }
        assertNotNull(DP.eval(shuffle, longInput, 1, TimeUnit.SECONDS));
    }

}
