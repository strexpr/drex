package edu.upenn.cis.drex.core.util;

import com.google.common.base.Supplier;
import org.apache.commons.lang3.mutable.MutableInt;
import static org.junit.Assert.*;
import org.junit.Test;

public class MemoTest {

    @Test
    public void testBasic() {
        // Memo<Integer> m = new Memo<>(() -> 42);
        Memo<Integer> m = new Memo<>(new Supplier<Integer>() {
            @Override
            public Integer get() {
                return 42;
            }
        });
        assertFalse(m.isInvoked());
        assertEquals(42, m.get().intValue());
        assertTrue(m.isInvoked());
        assertEquals(42, m.get().intValue());
    }

    @Test
    public void testCaching() {
        final MutableInt x = new MutableInt(41);
        Memo<Integer> m = new Memo<>(new Supplier<Integer>() {
            @Override
            public Integer get() {
                return x.intValue();
            }
        });
        x.increment();
        assertFalse(m.isInvoked());
        assertEquals(42, m.get().intValue());
        x.increment();
        assertTrue(m.isInvoked());
        assertEquals(42, m.get().intValue());
    }

    @Test(expected = NullPointerException.class)
    public void testNull() {
        new Memo<>(null);
    }

}
