package edu.upenn.cis.drex.core.util.lazystring;

import static org.junit.Assert.*;
import org.junit.Test;

public class LazyStringTest {

    @Test
    public void testBasic() {
        final LazyString a = LazyString.of("abc");
        final LazyString b = LazyString.of("");
        final LazyString c = LazyString.of("def");
        final LazyString abc = a.concat(b).concat(c);

        for (int i = 0; i < 6; i++) {
            assertEquals("abcdef".charAt(i), abc.charAt(i));
        }

        assertEquals("abcdef", abc.build());

        for (int i = 0; i < 6; i++) {
            assertEquals("abcdef".charAt(i), abc.charAt(i));
        }
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testIndexOutOfBounds1() {
        final LazyString a = LazyString.of("abc");
        final LazyString b = LazyString.of("");
        final LazyString c = LazyString.of("def");
        final LazyString abc = a.concat(b).concat(c);
        abc.charAt(-1);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void testIndexOutOfBounds2() {
        final LazyString a = LazyString.of("abc");
        final LazyString b = LazyString.of("");
        final LazyString c = LazyString.of("def");
        final LazyString abc = a.concat(b).concat(c);
        abc.charAt(6);
    }

    @Test
    public void testLargeString1() {
        final String base = "ab";
        final int repeat = 24;

        LazyString ab = LazyString.of(base);
        for (int i = 0; i < repeat; i++) {
            ab = ab.concat(ab);
        }
        final String sab = ab.build();

        assertEquals(base.length() * (2 << (repeat - 1)), sab.length());
        for (int i = 0; i < sab.length(); i++) {
            assertEquals(i % 2 == 0 ? 'a' : 'b', sab.charAt(i));
        }
    }

    @Test
    public void testLargeString2() {
        final String base = "ab";
        final int repeat = 1000000;

        LazyString ab = LazyString.of("");
        for (int i = 0; i < repeat; i++) {
            ab = ab.concat(LazyString.of(base));
        }
        final String sab = ab.build();

        assertEquals(base.length() * repeat, sab.length());
        for (int i = 0; i < sab.length(); i++) {
            assertEquals(i % 2 == 0 ? 'a' : 'b', sab.charAt(i));
        }
    }

}
