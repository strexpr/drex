package edu.upenn.cis.drex.core.domaintype;

import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Empty;
import edu.upenn.cis.drex.core.regex.Epsilon;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import edu.upenn.cis.drex.core.regex.Union;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Test;
import theory.CharSolver;
import theory.StdCharPred;

public class RegexAmbiguityTest {

    final CharSolver solver = new CharSolver();

    @Test
    public void testEmpty() {
        Regex empty = new Empty();
        assertNull(empty.getAmbiguity());
    }

    @Test
    public void testEpsilon() {
        Regex epsilon = new Epsilon();
        assertNull(epsilon.getAmbiguity());
    }

    @Test
    public void testSymbol() {
        Regex symbol1 = new Symbol(StdCharPred.ALPHA);
        assertNull(symbol1.getAmbiguity());
        Regex symbol2 = new Symbol(StdCharPred.TRUE);
        assertNull(symbol2.getAmbiguity());
        Regex symbol3 = new Symbol(StdCharPred.FALSE);
        assertNotNull(symbol3.getAmbiguity());
    }

    @Test
    public void testConcat() {
        Regex alpha = new Symbol(StdCharPred.ALPHA);
        Regex num = new Symbol(StdCharPred.NUM);
        Regex empty = new Symbol(solver.False());

        Regex aa = new Concat(alpha, alpha);
        assertNull(aa.getAmbiguity());
        Regex an = new Concat(alpha, num);
        assertNull(an.getAmbiguity());

        Regex as = new Star(alpha);
        assertNull(as.getAmbiguity());
        Regex asas = new Concat(as, as);
        assertNotNull(asas.getAmbiguity());

        Regex af = new Concat(alpha, empty);
        assertNotNull(af.getAmbiguity());
    }

    @Test
    public void testStar() {
        Regex alpha = new Symbol(StdCharPred.ALPHA);
        Regex as = new Star(alpha);
        assertNull(as.getAmbiguity());
        Regex ass = new Star(as);
        assertNotNull(ass.getAmbiguity());
    }

    @Test
    public void testUnion() {
        Regex alpha = new Symbol(StdCharPred.ALPHA);
        Regex num = new Symbol(StdCharPred.NUM);
        Regex an = new Union(alpha, num);
        assertNull(an.getAmbiguity());
        Regex anan = new Union(an, an);
        assertNotNull(anan.getAmbiguity());
    }

}
