package edu.upenn.cis.drex.core.regex;

import automata.sfa.SFA;
import com.google.common.collect.ImmutableList;
import static org.junit.Assert.*;
import org.junit.Test;
import theory.CharPred;
import theory.CharSolver;
import theory.StdCharPred;

public class RegexTest {

    final CharSolver solver = new CharSolver();

    @Test
    public void regexTestBasic() {

        Regex empty = new Empty();
        Regex epsilon = new Epsilon();
        Regex lowercase = new Symbol(new CharPred('a', 'z'));
        Regex digit = new Symbol(new CharPred('0', '9'));
        Regex ld = new Union(lowercase, digit);
        Regex dld = new Concat(digit, ld);
        Regex digitS = new Star(digit);
        Regex sigma = new Symbol(solver.True());
        Regex sigmaS = new Star(sigma);

        assertFalse(empty.contains("", solver));
        assertFalse(empty.contains("abc", solver));

        assertTrue(epsilon.contains("", solver));
        assertFalse(epsilon.contains("a", solver));
        assertFalse(epsilon.contains("ab", solver));

        assertFalse(lowercase.contains("", solver));
        assertTrue(lowercase.contains("a", solver));
        assertTrue(lowercase.contains("b", solver));
        assertFalse(lowercase.contains("A", solver));
        assertFalse(lowercase.contains("ab", solver));

        assertTrue(digit.contains("9", solver));
        assertTrue(digit.contains("0", solver));
        assertFalse(digit.contains("09", solver));
        assertFalse(digit.contains("a", solver));
        assertFalse(digit.contains("ab", solver));
        assertFalse(digit.contains("", solver));

        assertTrue(ld.contains("a", solver));
        assertTrue(ld.contains("6", solver));
        assertFalse(ld.contains("A", solver));
        assertFalse(ld.contains("05", solver));

        assertTrue(dld.contains("00", solver));
        assertTrue(dld.contains("0a", solver));
        assertFalse(dld.contains("aa", solver));

        assertTrue(digitS.contains("9", solver));
        assertTrue(digitS.contains("0", solver));
        assertTrue(digitS.contains("09", solver));
        assertFalse(digitS.contains("a", solver));
        assertFalse(digitS.contains("ab", solver));
        assertTrue(digitS.contains("", solver));

        assertTrue(sigma.contains("9", solver));
        assertTrue(sigmaS.contains("0", solver));
        assertTrue(sigmaS.contains("09", solver));
        assertTrue(sigmaS.contains("09999", solver));
    }

    @Test
    public void regexEquivalenceTest() {
        Regex ra = new Symbol(CharPred.of(ImmutableList.of('a')));
        Regex raStar = new Star(ra);
        Regex raPlus = new Concat(ra, raStar);

        @SuppressWarnings("null")
        SFA<CharPred, Character> sfaA = ra.getSFA().determinize(solver);
        @SuppressWarnings("null")
        SFA<CharPred, Character> sfaAPlus = raPlus.getSFA().determinize(solver);
        assertNotNull(sfaA != null);
        assertNotNull(sfaAPlus != null);

        SFA<CharPred, Character> sfaAMPlus = sfaA.minus(sfaAPlus, solver);
        assertTrue(sfaAMPlus.isEmpty());

        SFA<CharPred, Character> sfaPlusMA = sfaAPlus.minus(sfaA, solver);
        assertFalse(sfaPlusMA.isEmpty());
    }

    @Test
    public void regexEquivalenceTest2() {
        Regex rEmpty = new Empty();
        Regex rTrue = new Symbol(StdCharPred.TRUE);

        @SuppressWarnings("null")
        SFA<CharPred, Character> sfaEmpty = rEmpty.getSFA().determinize(solver);
        @SuppressWarnings("null")
        SFA<CharPred, Character> sfaTrue = rTrue.getSFA().determinize(solver);
        assertNotNull(sfaEmpty != null);
        assertNotNull(sfaTrue != null);

        SFA<CharPred, Character> sfaEmptyMTrue = sfaEmpty.minus(sfaTrue, solver);
        assertTrue(sfaEmptyMTrue.isEmpty());

        SFA<CharPred, Character> sfaTrueMEmpty = sfaTrue.minus(sfaEmpty, solver);
        assertFalse(sfaTrueMEmpty.isEmpty());
    }

}
