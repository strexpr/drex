package edu.upenn.cis.drex.core.forkjoin;

public class RegexEvaluatorTest {

    /* @Test
    public void testBasicEmpty() {
        Evaluator tBot = RegexEvaluatorBuilder.build(new Empty());
        assertNull(tBot.eval(""));
        assertNull(tBot.eval("a"));
        assertNull(tBot.eval("b"));
        assertNull(tBot.eval("ab"));
        assertNull(tBot.eval("aaaaabbbbb"));
    }

    @Test
    public void testBasicEpsilon() {
        Evaluator tEpsilon = RegexEvaluatorBuilder.build(new Epsilon());
        assertNotNull(tEpsilon.eval(""));
        assertNull(tEpsilon.eval("a"));
        assertNull(tEpsilon.eval("b"));
        assertNull(tEpsilon.eval("ab"));
        assertNull(tEpsilon.eval("aaaaabbbbb"));
    }

    @Test
    public void testBasicSymbol() {
        Symbol ab = new Symbol(new CharPred('a'));
        Evaluator tab = RegexEvaluatorBuilder.build(ab);
        assertNull(tab.eval(""));
        assertNotNull(tab.eval("a"));
        assertNull(tab.eval("aa"));
        assertNull(tab.eval("b"));
        assertNull(tab.eval("ab"));
        assertNull(tab.eval("ba"));
        assertNull(tab.eval("bb"));

        Symbol ba = new Symbol(new CharPred('b'));
        Evaluator tba = RegexEvaluatorBuilder.build(ba);
        assertNull(tba.eval(""));
        assertNotNull(tba.eval("b"));
        assertNull(tba.eval("aa"));
        assertNull(tba.eval("a"));
        assertNull(tba.eval("ab"));
        assertNull(tba.eval("ba"));
        assertNull(tba.eval("bb"));
    }

    @Test
    public void testBasicUnion() {
        Symbol ab = new Symbol(new CharPred('a'));
        Symbol ba = new Symbol(new CharPred('b'));
        Union abba = new Union(ab, ba);
        Evaluator tabba = RegexEvaluatorBuilder.build(abba);
        assertNull(tabba.eval(""));
        assertNotNull(tabba.eval("b"));
        assertNull(tabba.eval("aa"));
        assertNotNull(tabba.eval("a"));
        assertNull(tabba.eval("ab"));
        assertNull(tabba.eval("ba"));
        assertNull(tabba.eval("bb"));
    }

    @Test
    public void testBasicStar() {
        Symbol ab = new Symbol(new CharPred('a'));
        Symbol ba = new Symbol(new CharPred('b'));
        Union abba = new Union(ab, ba);

        Star abs = new Star(ab);
        Evaluator tabs = RegexEvaluatorBuilder.build(abs);
        assertNotNull(tabs.eval(""));
        assertNotNull(tabs.eval("a"));
        assertNull(tabs.eval("b"));
        assertNotNull(tabs.eval("aa"));
        assertNull(tabs.eval("ab"));
        assertNull(tabs.eval("ba"));
        assertNull(tabs.eval("bb"));
        assertNull(tabs.eval("aaaaababa"));
        assertNotNull(tabs.eval("aaaaa"));

        Star abbas = new Star(abba);
        Evaluator tabbas = RegexEvaluatorBuilder.build(abbas);
        assertNotNull(tabbas.eval(""));
        assertNotNull(tabbas.eval("a"));
        assertNotNull(tabbas.eval("b"));
        assertNotNull(tabbas.eval("aa"));
        assertNotNull(tabbas.eval("ab"));
        assertNotNull(tabbas.eval("ba"));
        assertNotNull(tabbas.eval("bb"));
        assertNotNull(tabbas.eval("aaaababa"));
    }

    @Test
    public void testComplement() {
        Symbol a = new Symbol(new CharPred('a'));
        Star as = new Star(a);
        Complement asc = new Complement(as);
        Evaluator tasc = RegexEvaluatorBuilder.build(asc);
        assertNotNull(tasc.eval("b"));
        assertNotNull(tasc.eval("ab"));
        assertNull(tasc.eval(""));
        assertNull(tasc.eval("a"));
        assertNull(tasc.eval("aaa"));

        Complement ascc = new Complement(asc);
        Evaluator tascc = RegexEvaluatorBuilder.build(ascc);
        assertNull(tascc.eval("b"));
        assertNull(tascc.eval("ab"));
        assertNotNull(tascc.eval(""));
        assertNotNull(tascc.eval("a"));
        assertNotNull(tascc.eval("aaa"));

        Symbol b = new Symbol(new CharPred('b'));
        Concat ab = new Concat(a, b);
        Star abs = new Star(ab);
        Complement absc = new Complement(abs);
        Evaluator tabsc = RegexEvaluatorBuilder.build(absc);
        assertNull(tabsc.eval(""));
        assertNotNull(tabsc.eval("a"));
        assertNull(tabsc.eval("ab"));
        assertNotNull(tabsc.eval("aba"));
        assertNull(tabsc.eval("abab"));
        assertNotNull(tabsc.eval("ababs"));
        assertNotNull(tabsc.eval("ababsc"));
    } */

}
