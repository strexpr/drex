package edu.upenn.cis.drex.core.forkjoin;

public class ExpressionEvaluatorTest {

    /* CharSolver solver = new CharSolver();
    final Symbol ab = new Symbol(new CharPred('a'), "b");
    final Symbol ba = new Symbol(new CharPred('b'), "a");
    final Symbol aa = new Symbol(new CharPred('a'), "a");
    final Symbol bb = new Symbol(new CharPred('b'), "b");
    final Expression ae = new Symbol(new CharPred('a'), "");
    final Expression be = new Symbol(new CharPred('b'), "");

    @Test
    public void testBasicBottom() {
        Evaluator tBot = ExpressionEvaluatorBuilder.build(new Bottom());
        assertNull(tBot.eval(""));
        assertNull(tBot.eval("a"));
        assertNull(tBot.eval("b"));
        assertNull(tBot.eval("ab"));
        assertNull(tBot.eval("aaaaabbbbb"));
    }

    @Test
    public void testBasicSymbol() {
        Evaluator tab = ExpressionEvaluatorBuilder.build(ab);
        assertNull(tab.eval(""));
        assertEquals(tab.eval("a"), "b");
        assertNull(tab.eval("aa"));
        assertNull(tab.eval("b"));
        assertNull(tab.eval("ab"));
        assertNull(tab.eval("ba"));
        assertNull(tab.eval("bb"));

        Evaluator tba = ExpressionEvaluatorBuilder.build(ba);
        assertNull(tba.eval(""));
        assertEquals(tba.eval("b"), "a");
        assertNull(tba.eval("aa"));
        assertNull(tba.eval("a"));
        assertNull(tba.eval("ab"));
        assertNull(tba.eval("ba"));
        assertNull(tba.eval("bb"));
    }

    @Test
    public void testBasicDisjointUnion() {
        IfElse abba = new IfElse(ab, ba);
        Evaluator tabba = ExpressionEvaluatorBuilder.build(abba);
        assertNull(tabba.eval(""));
        assertEquals(tabba.eval("b"), "a");
        assertNull(tabba.eval("aa"));
        assertEquals(tabba.eval("a"), "b");
        assertNull(tabba.eval("ab"));
        assertNull(tabba.eval("ba"));
        assertNull(tabba.eval("bb"));
    }

    @Test
    public void testBasicEpsilon() {
        Epsilon ef = new Epsilon("f");
        Evaluator tef = ExpressionEvaluatorBuilder.build(ef);
        assertEquals(tef.eval(""), "f");
        assertNull(tef.eval("a"));
        assertNull(tef.eval("b"));
        assertNull(tef.eval("aa"));
        assertNull(tef.eval("ab"));
        assertNull(tef.eval("ba"));
        assertNull(tef.eval("bb"));
    }

    @Test
    public void testBasicIteratedSum() {
        IfElse abba = new IfElse(ab, ba);

        IteratedSum abs = new IteratedSum(ab);
        Evaluator tabs = ExpressionEvaluatorBuilder.build(abs);
        assertEquals(tabs.eval(""), "");
        assertEquals(tabs.eval("a"), "b");
        assertNull(tabs.eval("b"));
        assertEquals(tabs.eval("aa"), "bb");
        assertNull(tabs.eval("ab"));
        assertNull(tabs.eval("ba"));
        assertNull(tabs.eval("bb"));
        assertNull(tabs.eval("aaaaababa"));
        assertEquals(tabs.eval("aaaaa"), "bbbbb");

        IteratedSum abbas = new IteratedSum(abba);
        Evaluator tabbas = ExpressionEvaluatorBuilder.build(abbas);
        assertEquals(tabbas.eval(""), "");
        assertEquals(tabbas.eval("a"), "b");
        assertEquals(tabbas.eval("b"), "a");
        assertEquals(tabbas.eval("aa"), "bb");
        assertEquals(tabbas.eval("ab"), "ba");
        assertEquals(tabbas.eval("ba"), "ab");
        assertEquals(tabbas.eval("bb"), "aa");
        assertEquals(tabbas.eval("aaaababa"), "bbbbabab");
    }

    @Test
    public void testBasicSplitSum() {
        IfElse abba = new IfElse(ab, ba);
        IteratedSum abbas = new IteratedSum(abba);

        IfElse aabb = new IfElse(aa, bb);
        IteratedSum aabbs = new IteratedSum(aabb);

        Symbol de = new Symbol(new CharPred('d'), "");
        SplitSum deaabbs = new SplitSum(de, aabbs);
        SplitSum abbasdeaabbs = new SplitSum(abbas, deaabbs);

        Evaluator t = ExpressionEvaluatorBuilder.build(abbasdeaabbs);
        assertNull(t.eval(""));
        assertNull(t.eval("a"));
        assertNull(t.eval("b"));
        assertEquals(t.eval("d"), "");
        assertNull(t.eval("aa"));
        assertNull(t.eval("ab"));
        assertNull(t.eval("ba"));
        assertNull(t.eval("bb"));
        assertEquals(t.eval("aabbadabba"), "bbaababba");
    }

    @Test
    public void testBasicSum() {
        CharSolver solver = new CharSolver();
        IteratedSum id = new IteratedSum(new Symbol(solver.True(), CharOffset.IDENTITY));
        Sum copy = new Sum(id, id);
        Evaluator t = ExpressionEvaluatorBuilder.build(copy);

        assertEquals(t.eval(""), "");
        assertEquals(t.eval("a"), "aa");
        assertEquals(t.eval("b"), "bb");
        assertEquals(t.eval("aa"), "aaaa");
        assertEquals(t.eval("ab"), "abab");
        assertEquals(t.eval("ba"), "baba");
        assertEquals(t.eval("bb"), "bbbb");
    }

    @Test
    public void testShuffle() throws TypeCheckingException {
        final Expression ae = new Symbol(new CharPred('0', '9'), "");
        final Expression aa = new Symbol(new CharPred('0', '9'), CharOffset.IDENTITY);
        final Expression be = new Symbol(new CharPred('b'), "");
        final Expression bb = new Symbol(new CharPred('b'), "b");
        
        final Expression aes = new IteratedSum(ae);
        final Expression aas = new IteratedSum(aa);

        final Expression aesbe = new SplitSum(aes, be);
        final Expression aasbb = new SplitSum(aas, bb);

        final SplitSum echoR = new SplitSum(aesbe, aasbb);
        final SplitSum echoL = new SplitSum(aasbb, aesbe);

        final Sum subexprList = new Sum(echoR, echoL);
        final ChainedSum shuffle = new ChainedSum(subexprList);

        Evaluator t = ExpressionEvaluatorBuilder.build(shuffle);
        assertNull(t.eval(""));
        assertEquals("2b1b3b2b4b3b5b4b6b5b7b6b8b7b9b8b10b9b11b10b12b11b13b12b",
                t.eval("1b2b3b4b5b6b7b8b9b10b11b12b13b"));
    }

    @Test
    public void testRestrict() {
        CharPred pred08 = new CharPred('0', '8');
        CharPred pred9 = new CharPred('9');

        Symbol s1 = new Symbol(pred08, new CharOffset(1));
        Symbol s2 = new Symbol(pred9, (CharFunc) new CharOffset(-9));
        Expression rot = new IfElse(s1, s2);
        
        Expression caesar = new IteratedSum(rot);

        Regex regex08s = new Star(new edu.upenn.cis.drex.core.regex.Symbol(pred08));
        Regex regex9 = new edu.upenn.cis.drex.core.regex.Symbol(pred9);
        Regex regex08s9 = new Concat(regex08s, regex9);
        Expression caesar08s9 = new Restrict(caesar, regex08s9);

        Evaluator tCaesar08s9 = ExpressionEvaluatorBuilder.build(caesar08s9);
        assertNull(tCaesar08s9.eval("06010154"));
        assertEquals("171212650", tCaesar08s9.eval("060101549"));

        Expression id = new IteratedSum(new Symbol(solver.True(), CharOffset.IDENTITY));
        Expression id08s9 = new Restrict(id, regex08s9);

        Evaluator tId08s9 = ExpressionEvaluatorBuilder.build(id08s9);
        assertNull(tId08s9.eval("06010154"));
        assertEquals("060101549", tId08s9.eval("060101549"));

        Expression alternate = new IteratedSum(new SplitSum(id08s9, caesar08s9));
        Evaluator tAlternate = ExpressionEvaluatorBuilder.build(alternate);
        assertNull(tAlternate.eval("06010154"));
        assertNull(tAlternate.eval("060101549"));
        assertEquals("060101549171212650", tAlternate.eval("060101549060101549"));
        assertEquals("060101549171212650060101549171212650", tAlternate.eval("060101549060101549060101549060101549"));
    }

    @Test
    public void testWeirdFalse() {
        CharPred pA = new CharPred('a');
        CharPred pN = new CharPred('n');

        Symbol a = new Symbol(pA, new CharOffset(0));
        IteratedSum aStar = new IteratedSum(a);
        Symbol n = new Symbol(pN, new CharOffset(0));
        Expression test = SplitSum.of(a, aStar, n);
        Expression iterTest = new IteratedSum(test);

        Evaluator iterTestEvaluator = ExpressionEvaluatorBuilder.build(iterTest);
        assertEquals("anan", iterTestEvaluator.eval("anan"));
    }

    @Test
    public void testWeirdTrue() {
        CharPred pA = new CharPred('a');
        CharPred pN = new CharPred('n');

        Symbol idChar = new Symbol(StdCharPred.TRUE, new CharOffset(0));
        IteratedSum id = new IteratedSum(idChar);

        edu.upenn.cis.drex.core.regex.Symbol a = new edu.upenn.cis.drex.core.regex.Symbol(pA);
        edu.upenn.cis.drex.core.regex.Symbol n = new edu.upenn.cis.drex.core.regex.Symbol(pN);
        Regex an = Concat.of(a, n);

        Expression test = new Restrict(id, an);
        Expression iterTest = new IteratedSum(test);

        Evaluator iterTestEvaluator = ExpressionEvaluatorBuilder.build(iterTest);
        assertEquals("anan", iterTestEvaluator.eval("anan"));
    }

    @Test
    public void testWeirdTrue2() {
        CharPred pA = new CharPred('a');
        CharPred pB = new CharPred('b');

        Symbol copyA = new Symbol(pA, new CharOffset(0));
        Expression epsilonOrCopyA = new IfElse(new Epsilon(""), copyA);

        Expression aStar = new IteratedSum(copyA);
        Expression bStar = new IteratedSum(new Symbol(pB, new CharOffset(0)));
        Expression aStarBStar = new SplitSum(aStar, bStar);

        Regex a = new edu.upenn.cis.drex.core.regex.Symbol(pA);
        Regex ab = new edu.upenn.cis.drex.core.regex.Symbol((new CharSolver()).MkOr(pA, pB));
        Regex b = new edu.upenn.cis.drex.core.regex.Symbol(pB);
        Regex l3 = Concat.of(a, ab, b);
        Expression right = new Restrict(aStarBStar, l3);

        Expression test = new SplitSum(epsilonOrCopyA, right);

        Evaluator evaluator = ExpressionEvaluatorBuilder.build(test);
        assertEquals("aabb", evaluator.eval("aabb"));
    }

    @Test
    public void testWeirdTrue3() {
        CharPred pA = new CharPred('a');
        CharPred pB = new CharPred('b');
        CharPred pC = new CharPred('c');

        Symbol copyA = new Symbol(pA, new CharOffset(0));
        Symbol copyB = new Symbol(pB, new CharOffset(0));
        Symbol copyC = new Symbol(pC, new CharOffset(0));
        Expression copyBOrC = new IfElse(copyB, copyC);
        Expression epsilonOrCopyA = new IfElse(new Epsilon(""), copyA);

        Regex a = new edu.upenn.cis.drex.core.regex.Symbol(pA);
        Regex b = new edu.upenn.cis.drex.core.regex.Symbol(pB);
        Regex c = new edu.upenn.cis.drex.core.regex.Symbol(pC);
        Regex babb = Concat.of(b, a, b, b);
        Regex abac = Concat.of(a, b, a, c);
        Regex regex = new Union(babb, abac);
        Regex aStar = new Star(a);
        Regex bOrC = new Union(b, c);
        Regex aStarBC = new Concat(aStar, bOrC);

        Expression idAStar = new IteratedSum(copyA);
        Expression idAStarBOrC = new SplitSum(idAStar, copyBOrC);
        Expression expr = new SplitSum(idAStarBOrC, idAStarBOrC);
        Expression cs = new ChainedSum(expr, aStarBC);

        Expression right = new Restrict(cs, regex);

        Expression test = new SplitSum(epsilonOrCopyA, right);

        Evaluator evaluator = ExpressionEvaluatorBuilder.build(test);
        assertEquals("abababb", evaluator.eval("ababb"));
    }

    @Test
    public void testWeirdTrue4() {
        // Test fails if ChainedSumEvaluator.processExpressionResult uses 
        // ordered immutable set and break is enabled in the loop
        // To make it an ordered set, use TreeSet
                
        CharPred pA = new CharPred('a');
        CharPred pB = new CharPred('b');
        CharPred pC = new CharPred('c');

        Symbol copyA = new Symbol(pA, new CharOffset(0));
        Symbol copyB = new Symbol(pB, new CharOffset(0));
        Symbol copyC = new Symbol(pC, new CharOffset(0));
        Expression copyBOrC = new IfElse(copyB, copyC);
        Expression epsilonOrCopyA = new IfElse(new Epsilon(""), copyA);

        Regex a = new edu.upenn.cis.drex.core.regex.Symbol(pA);
        Regex b = new edu.upenn.cis.drex.core.regex.Symbol(pB);
        Regex c = new edu.upenn.cis.drex.core.regex.Symbol(pC);
        Regex babab = Concat.of(b, a, b, a, b);
        Regex ababac = Concat.of(a, b, a, b, a, c);
        Regex regex = new Union(babab, ababac);
        Regex aStar = new Star(a);
        Regex bOrC = new Union(b, c);
        Regex aStarBC = new Concat(aStar, bOrC);

        Expression idAStar = new IteratedSum(copyA);
        Expression idAStarBOrC = new SplitSum(idAStar, copyBOrC);
        Expression expr = new SplitSum(idAStarBOrC, idAStarBOrC);
        Expression cs = new ChainedSum(expr, aStarBC);

        Expression right = new Restrict(cs, regex);

        Expression test = new SplitSum(epsilonOrCopyA, right);

        Evaluator evaluator = ExpressionEvaluatorBuilder.build(test);
        assertEquals("abababab", evaluator.eval("ababab"));
    }

    @Test
    public void testIterBottom() {
        Expression bot = new Bottom();
        Expression iterBot = new IteratedSum(bot);

        assertNotNull(iterBot.getDomainType());

        Evaluator eval = ExpressionEvaluatorBuilder.build(iterBot);
        assertNotNull(eval.eval(""));
        assertNull(eval.eval("sldlkasd"));
    }

    @Test
    public void testIterBottom2() {
        // (split (iter (symbol [0-9] [x + 0]))
        //        (ifelse (restrict (split (iter (symbol [9] [x -> 0]))
        //                                 (split (iter bot)
        //                                        (epsilon "1")))
        //                          [9])
        //                (symbol [0-8] [x + 1])))

        CharPred p09 = new CharPred('0', '9');
        CharOffset o0 = new CharOffset(0);
        Expression p09IdStar = new IteratedSum(new Symbol(p09, o0));

        CharPred p9 = new CharPred('9');
        Expression p9IdStar = new IteratedSum(new Symbol(p9, o0));
        boolean iter = true;
        Expression iterBotE1;
        if (iter) {
            iterBotE1 = new SplitSum(new IteratedSum(new Bottom()), new Epsilon("1"));
        } else {
            iterBotE1 = new SplitSum(new Epsilon(""), new Epsilon("1"));
        }
        Expression e1 = new SplitSum(p9IdStar, iterBotE1);
        Expression e2 = new Restrict(e1, new edu.upenn.cis.drex.core.regex.Symbol(p9));

        Expression e = new SplitSum(p09IdStar, e2);
        Evaluator evaluator = ExpressionEvaluatorBuilder.build(e);
        assertEquals("9999999999999991", evaluator.eval("999999999999999"));
    }

    @Test
    public void testIterRestrict3() {
        CharPred all = StdCharPred.TRUE;
        Expression swallowChar = new Symbol(all, "");
        Expression echoChar = new Symbol(all, new CharOffset(0));

        // Both (iter bot) and (epsilon "") display the bug
        // Expression iterBot = new IteratedSum(new Bottom());
        Expression iterBot = new Epsilon("");

        Expression id = new IteratedSum(echoChar);
        Expression idTail = new SplitSum(swallowChar, id);
        Expression idTailPrime = new SplitSum(idTail, iterBot);

        // This does not display the bug
        // Expression idTailPrime = idTail;

        Expression restrict = new Restrict(
                idTailPrime,
                new edu.upenn.cis.drex.core.regex.Symbol(all)
        );

        Expression f = new IteratedSum(restrict);

        Evaluator evaluator = ExpressionEvaluatorBuilder.build(f);
        assertEquals("", evaluator.eval("bc"));
    } */

}
