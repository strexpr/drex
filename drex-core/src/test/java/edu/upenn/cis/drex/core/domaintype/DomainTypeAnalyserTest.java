package edu.upenn.cis.drex.core.domaintype;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.junit.Assert.*;
import org.junit.Test;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.Restrict;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.expr.Symbol;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import theory.CharOffset;
import theory.CharPred;
import theory.CharSolver;
import theory.StdCharPred;

public class DomainTypeAnalyserTest {

    final CharSolver solver = new CharSolver();
    
    public static void assertTyped(Expression e) {
        checkNotNull(e);
        assertNotNull(e.getDomainType());
        assertNull(e.getDomainTypeError());
    }

    public static void assertUntyped(Expression e) {
        checkNotNull(e);
        assertNull(e.getDomainType());
        assertNotNull(e.getDomainTypeError());
    }

    @Test
    public void tcTestBasic() throws TypeCheckingException {
        IteratedSum id = new IteratedSum(new Epsilon("a"));
        Sum copy = new Sum(id, id);
        assertUntyped(copy);
    }

    @Test
    public void tcTestSymbol() throws TypeCheckingException {
        Symbol s = new Symbol(solver.True(), "a");
        assertTyped(s);
    }

    @Test
    public void tcTestIter() throws TypeCheckingException {
        IteratedSum id1 = new IteratedSum(new Symbol(solver.True(), "a"));
        assertTyped(id1);
    }

    @Test
    public void tcTestSumEquiv() throws TypeCheckingException {
        IteratedSum id1 = new IteratedSum(new Symbol(solver.True(), "a"));
        IteratedSum id2 = new IteratedSum(new Symbol(new CharPred('a', 'z'), "a"));
        Sum copy = new Sum(id1, id2);
        assertUntyped(copy);
    }

    @Test
    public void tcTestSumEquiv2() throws TypeCheckingException {
        IteratedSum id2 = new IteratedSum(new Symbol(new CharPred('a', 'z'), "a"));
        Sum copy = new Sum(id2, id2);
        assertTyped(copy);
    }

    @Test
    public void tcTestSplitSumAmbig() throws TypeCheckingException {
        IteratedSum id2 = new IteratedSum(new Symbol(new CharPred('a', 'z'), "a"));
        SplitSum copy = new SplitSum(id2, id2);
        assertUntyped(copy);
    }

    @Test
    public void tcTestSplitSumAmbigEps() throws TypeCheckingException {
        IteratedSum id2 = new IteratedSum(new Symbol(new CharPred('a', 'z'), "a"));
        SplitSum copy = new SplitSum(id2, new Epsilon("a"));
        assertTyped(copy);
    }

    @Test
    public void tcTestSplitSumIter() throws TypeCheckingException {
        IteratedSum id1 = new IteratedSum(new Symbol(new CharPred('a', 'z'), "a"));
        IteratedSum id2 = new IteratedSum(new Symbol(new CharPred('1', '4'), "a"));
        SplitSum copy = new SplitSum(id1, id2);
        assertTyped(copy);
    }

    @Test
    public void tcTestChainedSum() throws TypeCheckingException {
        CharPred pred = new CharPred('a', 'z');
        Symbol id = new Symbol(pred, "a");
        SplitSum split = new SplitSum(id, id);
        ChainedSum cs = new ChainedSum(split, new edu.upenn.cis.drex.core.regex.Symbol(pred));
        assertTyped(cs);
    }

    @Test
    public void tcTestChainedSum2() throws TypeCheckingException {
        CharPred pred = new CharPred('a', 'z');
        Symbol id = new Symbol(pred, "a");
        SplitSum split = new SplitSum(id, id);
        ChainedSum cs = new ChainedSum(split, new edu.upenn.cis.drex.core.regex.Symbol(pred));
        assertTyped(cs);
    }

    @Test
    public void tcTestChainedSum3() throws TypeCheckingException {
        CharPred pred = new CharPred('a', 'z');
        CharPred pred2 = new CharPred('a', 'b');
        Symbol id = new Symbol(pred, "a");
        Symbol id2 = new Symbol(pred2, "a");
        SplitSum split = new SplitSum(id, id2);
        ChainedSum cs = new ChainedSum(split, new edu.upenn.cis.drex.core.regex.Symbol(pred));
        assertUntyped(cs);
    }

    @Test
    public void testShuffle() {
        final Expression ae = new Symbol(new CharPred('0', '9'), "");
        final Expression aa = new Symbol(new CharPred('0', '9'), CharOffset.IDENTITY);
        final Expression be = new Symbol(new CharPred('b'), "");
        final Expression bb = new Symbol(new CharPred('b'), "b");

        final Expression aes = new IteratedSum(ae);
        final Expression aas = new IteratedSum(aa);

        final Expression aesbe = new SplitSum(aes, be);
        final Expression aasbb = new SplitSum(aas, bb);

        final SplitSum echoR = new SplitSum(aesbe, aasbb);
        final SplitSum echoL = new SplitSum(aasbb, aesbe);

        final Regex ra = new edu.upenn.cis.drex.core.regex.Symbol(new CharPred('0', '8'));
        final Regex rb = new edu.upenn.cis.drex.core.regex.Symbol(new CharPred('b'));
        final Regex ras = new edu.upenn.cis.drex.core.regex.Star(ra);
        final Regex rasb = new edu.upenn.cis.drex.core.regex.Concat(ras, rb);

        final Sum subexprList = new Sum(echoR, echoL);

        Expression shuffle = new ChainedSum(subexprList, rasb);
        assertUntyped(shuffle);
    }

    @Test
    public void testRestrict() {
        Expression id = new IteratedSum(new Symbol(solver.True(), CharOffset.IDENTITY));
        assertTyped(id);

        Expression idId = new SplitSum(id, id);
        assertUntyped(idId);

        Regex rAlpha = new edu.upenn.cis.drex.core.regex.Symbol(StdCharPred.ALPHA);
        Regex rNum = new edu.upenn.cis.drex.core.regex.Symbol(StdCharPred.NUM);
        Regex regex = new Concat(new Star(rAlpha), rNum);
        Expression idRest = new Restrict(id, regex);
        assertTyped(idRest);

        Expression idRest2 = new SplitSum(idRest, idRest);
        assertTyped(idRest2);
    }

}
