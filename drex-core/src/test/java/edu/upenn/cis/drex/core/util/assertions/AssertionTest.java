package edu.upenn.cis.drex.core.util.assertions;

import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import org.junit.Test;

public class AssertionTest {

    @Test
    public void testTrue() {
        checkAssertion(true);
        checkAssertion(true, "This assertion must not be violated");
    }

    @Test(expected = AssertionViolation.class)
    public void testFalse1() {
        checkAssertion(false);
    }

    @Test(expected = AssertionViolation.class)
    public void testFalse2() {
        checkAssertion(false, "This assertion must be violated");
    }

}
