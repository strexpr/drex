package edu.upenn.cis.drex.bench;

import com.google.common.collect.ImmutableMap;
import edu.upenn.cis.drex.core.expr.Expression;
import java.util.Map.Entry;
import org.junit.Test;
import static org.junit.Assert.*;

public class BenchExprsTest {

    @Test
    public void testBenchExprsTyped() {
        ImmutableMap<String, Expression> benchExprs = BenchExprs.buildBenchmarkPrograms();
        for (Entry<String, Expression> es : benchExprs.entrySet()) {
            assertNotNull(es.getValue().getDomainType());
            assertNull(es.getValue().getDomainTypeError());
        }
    }

}
