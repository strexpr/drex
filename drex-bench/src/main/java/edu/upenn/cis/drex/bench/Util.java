package edu.upenn.cis.drex.bench;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import edu.upenn.cis.drex.core.expr.Composition;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.Restrict;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.expr.Symbol;
import edu.upenn.cis.drex.core.regex.Complement;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Empty;
import edu.upenn.cis.drex.core.regex.Intersection;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Union;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.apache.commons.io.IOUtils;

public class Util {

    public static Expression deepCopy(Expression e) {
        return DeepCopyExpr.copy(checkNotNull(e));
    }

    public static Regex deepCopy(Regex r) {
        return DeepCopyRegex.copy(checkNotNull(r));
    }

    public static ImmutableList<String> getBibtexStrings() throws IOException {
        int len = 0;
        String nextBenchmarkString = "";

        ClassLoader classLoader = Main.class.getClassLoader();
        String directoryName = "bibtex-files";
        InputStream directoryStream = classLoader.getResourceAsStream(directoryName);
        List<String> fileNames = IOUtils.readLines(directoryStream);

        List<String> files = new ArrayList<>();
        for (String fileName : fileNames) {
            String fullFileName = directoryName + "/" + fileName;
            InputStream fileStream = classLoader.getResourceAsStream(fullFileName);
            files.add(IOUtils.toString(fileStream));
        }

        List<String> benchmarkStrings = new ArrayList<>();
        while (nextBenchmarkString.length() <= 1e5) {
            nextBenchmarkString = getBibtexString(files, len++);
            benchmarkStrings.add(nextBenchmarkString);
        }

        return ImmutableList.copyOf(benchmarkStrings);
    }

    public static String getBibtexString(List<String> files, int len) throws IOException {
        checkNotNull(files);
        checkArgument(0 <= len);

        StringBuilder ans = new StringBuilder();
        for (int i = 0; i < len; i++) {
            ans.append(files.get(i % files.size()));
        }
        return ans.toString();
    }

    public static ImmutableList<String> getStrings() {
        int len = 1;
        double growthRate = 1.021276;

        Random r = new Random(0);
        ImmutableList.Builder<String> builder = ImmutableList.builder();
        while (len <= 1e5) {
            builder.add(getString(len, r));
            len = (int)Math.ceil(len * growthRate);
        }

        return builder.build();
    }

    public static final int ASCII_PRINTABLE_LO = 32;
    public static final int ASCII_PRINTABLE_HI = 126;
    public static String getString(int len, Random r) {
        checkArgument(0 <= len);
        checkNotNull(r);
        char[] buf = new char[len];
        for (int i = 0; i + 1 < len; i++) {
            buf[i] = (char)getNextRandomInt(r, ASCII_PRINTABLE_LO, ASCII_PRINTABLE_HI);
        }
        buf[buf.length - 1] = ';';
        return new String(buf);
    }

    public static int getNextRandomInt(Random r, int lo, int hi) {
        checkNotNull(r);
        checkArgument(lo <= hi);
        return lo + r.nextInt(hi - lo + 1);
    }

    public static PrintWriter getBenchmarkOutputWriter(String prefix)
            throws FileNotFoundException {
        String fname = String.format("%s-%d.txt", checkNotNull(prefix),
                System.currentTimeMillis());
        return new PrintWriter(fname);
    }

}

class DeepCopyExpr extends edu.upenn.cis.drex.core.expr.IVisitor<Expression> {

    public static Expression copy(Expression e) {
        DeepCopyExpr copier = new DeepCopyExpr();
        return copier.visit(checkNotNull(e));
    }

    @Override
    public Expression visitBottom(Bottom bottom) {
        checkNotNull(bottom);
        return new Bottom();
    }

    @Override
    public Expression visitSymbol(Symbol symbol) {
        return new Symbol(checkNotNull(symbol).a, symbol.d);
    }

    @Override
    public Expression visitEpsilon(Epsilon epsln) {
        return new Epsilon(checkNotNull(epsln).d);
    }

    @Override
    public Expression visitIfElse(IfElse ifelse) {
        checkNotNull(ifelse);
        // ans.setValue(new IfElse(ifelse.e1, ifelse.e2));
        Expression e1c = copy(ifelse.e1);
        Expression e2c = copy(ifelse.e2);
        return new IfElse(e1c, e2c);
    }

    @Override
    public Expression visitSplitSum(SplitSum ss) {
        checkNotNull(ss);
        // ans.setValue(new SplitSum(ss.e1, ss.e2));
        Expression e1c = copy(ss.e1);
        Expression e2c = copy(ss.e2);
        return new SplitSum(e1c, e2c);
    }

    @Override
    public Expression visitSum(Sum sum) {
        checkNotNull(sum);
        // ans.setValue(new Sum(sum.e1, sum.e2));
        Expression e1c = copy(sum.e1);
        Expression e2c = copy(sum.e2);
        return new Sum(e1c, e2c);
    }

    @Override
    public Expression visitIteratedSum(IteratedSum is) {
        checkNotNull(is);
        // ans.setValue(new IteratedSum(is.e, is.left));
        Expression ec = copy(is.e);
        return new IteratedSum(ec, is.left);
    }

    @Override
    public Expression visitChainedSum(ChainedSum cs) {
        checkNotNull(cs);
        // ans.setValue(new ChainedSum(cs.e, cs.r, cs.left));
        Expression ec = copy(cs.e);
        Regex rc = Util.deepCopy(cs.r);
        return new ChainedSum(ec, rc, cs.left);
    }

    @Override
    public Expression visitComposition(Composition cmpstn) {
        checkNotNull(cmpstn);
        // ans.setValue(new Composition(cmpstn.first, cmpstn.second));
        Expression fc = copy(cmpstn.first);
        Expression sc = copy(cmpstn.second);
        return new Composition(fc, sc);
    }

    @Override
    public Expression visitRestrict(Restrict restrict) {
        checkNotNull(restrict);
        // ans.setValue(new Restrict(restrict.e, restrict.r));
        Expression ec = copy(restrict.e);
        Regex rc = Util.deepCopy(restrict.r);
        return new Restrict(ec, rc);
    }

}

class DeepCopyRegex extends edu.upenn.cis.drex.core.regex.IVisitor<Regex> {

    public static Regex copy(Regex regex) {
        DeepCopyRegex copier = new DeepCopyRegex();
        return copier.visit(checkNotNull(regex));
    }

    @Override
    public Regex visitEmpty(Empty r) {
        checkNotNull(r);
        return new Empty();
    }

    @Override
    public Regex visitEpsilon(edu.upenn.cis.drex.core.regex.Epsilon r) {
        checkNotNull(r);
        return new edu.upenn.cis.drex.core.regex.Epsilon();
    }

    @Override
    public Regex visitSymbol(edu.upenn.cis.drex.core.regex.Symbol r) {
        checkNotNull(r);
        return new edu.upenn.cis.drex.core.regex.Symbol(r.a);
    }

    @Override
    public Regex visitUnion(Union r) {
        checkNotNull(r);
        Regex r1c = copy(r.r1);
        Regex r2c = copy(r.r2);
        return new Union(r1c, r2c);
    }

    @Override
    public Regex visitConcat(Concat r) {
        checkNotNull(r);
        Regex r1c = copy(r.r1);
        Regex r2c = copy(r.r2);
        return new Concat(r1c, r2c);
    }

    @Override
    public Regex visitStar(Star r) {
        checkNotNull(r);
        Regex ric = copy(r.r);
        return new Star(ric);
    }

    @Override
    public Regex visitComplement(Complement r) {
        checkNotNull(r);
        Regex ric = copy(r.r);
        return new Complement(ric);
    }

    @Override
    public Regex visitIntersection(Intersection r) {
        checkNotNull(r);
        Regex r1c = copy(r.r1);
        Regex r2c = copy(r.r2);
        return new Intersection(r1c, r2c);
    }

}
