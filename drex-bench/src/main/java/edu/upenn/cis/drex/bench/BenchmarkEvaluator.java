package edu.upenn.cis.drex.bench;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.dp.DP;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.forkjoin.ExpressionEvaluatorBuilder;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.time.StopWatch;

public class BenchmarkEvaluator {

    public static void doSimpleBenchmarks(BenchmarkEvaluatorParams params)
            throws FileNotFoundException {
        checkNotNull(params);

        Map<String, Expression> benchmarks = BenchExprs.buildBenchmarkPrograms();
        ImmutableList<String> inputStrings = Util.getStrings();
        for (Map.Entry<String, Expression> se : benchmarks.entrySet()) {
            if (!se.getKey().startsWith("bibtex")) {
                String fjFName = String.format("FJ-%s", se.getKey());
                try (PrintWriter fjOut = Util.getBenchmarkOutputWriter(fjFName)) {
                    Evaluator fjEvaluator = getForkjoinEvaluator(se.getKey(), se.getValue());
                    doBenchmark(fjOut, fjEvaluator, inputStrings, params);
                }

                String dpFName = String.format("DP-%s", se.getKey());
                try (PrintWriter dpOut = Util.getBenchmarkOutputWriter(dpFName)) {
                    Evaluator dpEvaluator = getDPEvaluator(se.getKey(), se.getValue(),
                            params.localTimeoutNano, TimeUnit.NANOSECONDS);
                    doBenchmark(dpOut, dpEvaluator, inputStrings, params);
                }
            }
        }
    }

    public static void doBibtexBenchmarks(BenchmarkEvaluatorParams params)
            throws FileNotFoundException, IOException {
        checkNotNull(params);

        Map<String, Expression> benchmarks = BenchExprs.buildBenchmarkPrograms();
        ImmutableList<String> inputStrings = Util.getBibtexStrings();
        for (Map.Entry<String, Expression> se : benchmarks.entrySet()) {
            if (se.getKey().startsWith("bibtex")) {
                String fjFName = String.format("FJ-%s", se.getKey());
                try (PrintWriter fjOut = Util.getBenchmarkOutputWriter(fjFName)) {
                    Evaluator fjEvaluator = getForkjoinEvaluator(se.getKey(), se.getValue());
                    doBenchmark(fjOut, fjEvaluator, inputStrings, params);
                }

                String dpFName = String.format("DP-%s", se.getKey());
                try (PrintWriter dpOut = Util.getBenchmarkOutputWriter(dpFName)) {
                    Evaluator dpEvaluator = getDPEvaluator(se.getKey(), se.getValue(),
                            params.localTimeoutNano, TimeUnit.NANOSECONDS);
                    doBenchmark(dpOut, dpEvaluator, inputStrings, params);
                }
            }
        }
    }

    public static void doBenchmark(PrintWriter out, Evaluator evaluator,
            ImmutableList<String> inputs, BenchmarkEvaluatorParams params) {
        checkNotNull(out);
        checkNotNull(evaluator);
        checkNotNull(inputs);
        checkNotNull(params);

        out.printf("BenchmarkEvaluator.doBenchmark(%s, %s, %s, %s).\n",
                out, evaluator, "String[]", params);
        int emptyCount = 0;
        for (String input : inputs) {
            long[] evalTimes = doBenchmark(evaluator, checkNotNull(input), params);
            out.printf("%d: %s\n", input.length(), Arrays.toString(evalTimes));
            out.flush();

            if (evalTimes.length == 0) {
                emptyCount++;
                if (emptyCount >= params.abandonCount) {
                    break;
                }
            }
        }
    }

    public static long[] doBenchmark(Evaluator evaluator, String input,
            BenchmarkEvaluatorParams params) {
        checkNotNull(evaluator);
        checkNotNull(input);
        checkNotNull(params);
        System.out.printf("Evaluating %s on strings of length %d.\n", evaluator, input.length());

        List<Long> evalTimes = new ArrayList<>();
        String referenceOutput = null;
        StopWatch localStopwatch = new StopWatch();
        StopWatch globalStopwatch = new StopWatch();
        globalStopwatch.reset();
        globalStopwatch.start();
        for (int i = 0; i < params.iterationCount; i++) {
            try {
                localStopwatch.reset();
                localStopwatch.start();
                String output = evaluator.eval(input);
                localStopwatch.stop();
                evalTimes.add(localStopwatch.getNanoTime());

                if (i > 0) {
                    String errorMsg = String.format("Iteration: %d.\n" +
                            "Expected output: \"%s\".\n" +
                            "Actual output: \"%s\".\n" +
                            "Evaluator: %s\n", i, referenceOutput, output, evaluator);
                    checkAssertion(Objects.equals(referenceOutput, output), errorMsg);
                } else {
                    referenceOutput = output;
                }
            } catch (InterruptedException | TimeoutException ex) {
                Logger logger = Logger.getLogger(BenchmarkEvaluator.class.getName());
                String msg = String.format("Timeout while evaluating expression %s on string of length %s.\n" +
                        "\"%s\".", evaluator, input.length(), input);
                logger.log(Level.ALL, msg, ex);
            }

            if (globalStopwatch.getNanoTime() >= params.globalTimeoutNano) {
                break;
            }
        }
        globalStopwatch.stop();

        return ArrayUtils.toPrimitive(evalTimes.toArray(new Long[0]));
    }

    public static Evaluator getDPEvaluator(final String eName, final Expression e,
            final long timeout, final TimeUnit timeUnit) {
        checkNotNull(eName);
        checkNotNull(e);
        checkArgument(timeout >= 0);
        Evaluator evaluator = new Evaluator() {
            @Override
            public String eval(String input) throws InterruptedException, TimeoutException {
                return DP.eval(e, checkNotNull(input), timeout, timeUnit);
            }

            @Override
            public String toString() {
                return String.format("DPEvaluator(Expression: %s, Timeout: %d %s)",
                        eName, timeout, timeUnit);
            }
        };
        return evaluator;
    }

    public static Evaluator getForkjoinEvaluator(final String eName, Expression e) {
        checkNotNull(eName);
        checkArgument(e != null && e.getDomainType() != null);
        final edu.upenn.cis.drex.core.forkjoin.Evaluator transducer = ExpressionEvaluatorBuilder.build(e);
        Evaluator evaluator = new Evaluator() {
            @Override
            public String eval(String input) {
                return transducer.eval(checkNotNull(input));
            }

            @Override
            public String toString() {
                return String.format("ForkjoinEvaluator(Expression: %s)", eName);
            }
        };
        return evaluator;
    }

}
