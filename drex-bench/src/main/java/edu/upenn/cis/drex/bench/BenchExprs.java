package edu.upenn.cis.drex.bench;

import edu.upenn.cis.drex.core.expr.Symbol;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import static edu.upenn.cis.drex.core.expr.ExpressionUtil.*;
import edu.upenn.cis.drex.core.domaintype.TypeCheckingException;
import edu.upenn.cis.drex.core.util.assertions.AssertionViolation;
import theory.CharOffset;
import theory.CharPred;
import theory.CharSolver;
import theory.StdCharPred;

public class BenchExprs {

    static final CharSolver solver = new CharSolver();
    
    /**
     * Returns a collection of named DReX benchmark expressions.
     * @return
     */
    public static ImmutableMap<String, Expression> buildBenchmarkPrograms() {
        ImmutableMap.Builder<String, Expression> builder = ImmutableMap.builder();
        for (Method method : BenchExprs.class.getMethods()) {
            if (method.isAnnotationPresent(BenchExpr.class)) {
                try {
                    BenchExpr benchmark = method.getAnnotation(BenchExpr.class);
                    Expression expr = (Expression)method.invoke(null);
                    builder.put(benchmark.name(), expr);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    throw new RuntimeException(e);
                }
            }
        }
        return builder.build();
    }

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ElementType.METHOD})
    private @interface BenchExpr {
        String name();
    }

    /**
     * Returns a DReX expression which deletes all C++-style single-line
     * comments from a text-file.
     * @return
     */
    @BenchExpr(name = "deleteComments")
    public static Expression buildDeleteComments() {
        final CharPred slash = new CharPred('/');
        final CharPred nonSlash = solver.MkNot(slash);
        final CharPred newline = new CharPred('\n');
        final CharPred nonNewline = solver.MkNot(newline);

        final Expression deleteS = new Symbol(slash, "");
        final Expression deleteSS = new SplitSum(deleteS, deleteS);

        final Expression nonNewlineCharToEpsilon = predToEpsilon(nonNewline);
        final Expression nonNewlineStringToEpsilon = new IteratedSum(nonNewlineCharToEpsilon);
        final Expression deleteLine = new SplitSum(nonNewlineStringToEpsilon, predToEpsilon(newline));
        final Expression deleteComment = new SplitSum(deleteSS, deleteLine);

        final Expression nonNewlineCharToSelf = predToSelf( nonNewline, solver);
        final Expression nonNewlineStringToSelf = new IteratedSum(nonNewlineCharToSelf);
        final Expression lineToSelf = new SplitSum(nonNewlineStringToSelf, predToSelf(newline, solver));
        final Expression nonCommentLineToSelf = new SplitSum(predToSelf(nonSlash, solver), lineToSelf);

        final Expression deleteComments = new IteratedSum(new IfElse(nonCommentLineToSelf, deleteComment));
        final Expression ending = new IfElse(new SplitSum(predToSelf(nonSlash, solver), nonNewlineStringToSelf), new SplitSum(deleteSS, nonNewlineStringToEpsilon));
        return new SplitSum(deleteComments, ending);
    }

    /**
     * Returns a DReX expression which inserts double-quotation marks around
     * each alphabetic word in the input string. For example, "ab b.b" is mapped
     * to "\"ab\" \"b\".\"b\"".
     * @return
     */
    @BenchExpr(name = "insertQuotes")
    public static Expression buildInsertQuotes() {
        Expression alphaToSelf = predToSelf(StdCharPred.ALPHA, solver);
        Expression nonAlphaToSelf = predToSelf(solver.MkNot(StdCharPred.ALPHA), solver);

        Expression naPlus = IteratedSum.plus(nonAlphaToSelf);
        Expression naStar = new IteratedSum(nonAlphaToSelf);

        Expression aplus = IteratedSum.plus(alphaToSelf);

        Expression epsilonToQuote = new Epsilon("\"");

        Expression alphaWord = SplitSum.of(epsilonToQuote, aplus, epsilonToQuote);
        Expression allWords = new IteratedSum(new SplitSum(alphaWord, naPlus));
        Expression lastWord = new IfElse(alphaWord, new Epsilon(""));

        return new SplitSum(new SplitSum(naStar, allWords), lastWord);
    }

    @BenchExpr(name = "getTags")
    public static Expression buildGetTags() {
        CharPred restrict = solver.True();
        restrict = solver.MkOr(restrict, new CharPred('>'));
        restrict = solver.MkOr(restrict, new CharPred('\''));
        restrict = solver.MkOr(restrict, new CharPred('\"'));

        //get tags
        CharPred opentag = new CharPred('<');
        CharPred closetag = new CharPred('>');
        CharPred notopen = solver.MkAnd(restrict, solver.MkNot(opentag));
        CharPred notclosed = solver.MkAnd(restrict, solver.MkNot(closetag));
        CharPred nottag = solver.MkAnd(restrict, solver.MkNot(solver.MkOr(opentag, closetag)));

        Expression copyOpen = predToSelf(opentag, solver);
        Expression openalphaclosed = new SplitSum(new SplitSum(
                copyOpen,
                new SplitSum(
                        new IteratedSum(predToSelf(nottag, solver)),
                        predToSelf(nottag, solver)
                )
        ),
                predToSelf(closetag, solver));

        //case <>
        Expression ocpat = new SplitSum(predToEpsilon(opentag), predToEpsilon(closetag));

        Expression deletenotclose = predToEpsilon(notclosed);
        Expression deletenotopen = predToEpsilon(notopen);
        Expression rdeletenotclose = new SplitSum(predToEpsilon(opentag),new IteratedSum(deletenotclose));
        Expression rdeletenotopen = new IteratedSum(deletenotopen);

        Expression notpat =
                new IfElse(
                new SplitSum(rdeletenotopen, rdeletenotclose),
                        rdeletenotopen);

        Expression repeatgettags = new IteratedSum(
                new SplitSum(notpat, openalphaclosed));

        return new SplitSum(repeatgettags, notpat);
    }

    @BenchExpr(name = "shuffle")
    public static Expression buildShuffle() {
        CharPred b = new CharPred(';');
        CharPred notb = solver.MkNot(b);

        final Expression ae = new Symbol(notb, "");
        final Expression aa = new Symbol(notb, CharOffset.IDENTITY);
        final Expression be = new Symbol(b, "");
        final Expression bb = new Symbol(b, ";");

        final Expression aes = new IteratedSum(ae);
        final Expression aas = new IteratedSum(aa);

        final Expression aesbe = new SplitSum(aes, be);
        final Expression aasbb = new SplitSum(aas, bb);

        final SplitSum echoR = new SplitSum(aesbe, aasbb);
        final SplitSum echoL = new SplitSum(aasbb, aesbe);

        final Sum subexprList = new Sum(echoR, echoL);

        try {
            return new ChainedSum(subexprList);
        } catch (TypeCheckingException e) {
            throw new AssertionViolation(e);
        }
    }

    @BenchExpr(name = "reverse")
    public static Expression buildReverse() {
        final CharPred b = new CharPred(';');
        final CharPred notb = solver.MkNot(b);
        final Expression aa = new Symbol(notb, CharOffset.IDENTITY);
        final Expression bb = new Symbol(b, ";");
        final Expression copynotb = new IteratedSum(aa);
        return new IteratedSum(new SplitSum(copynotb, bb), true);
    }

    @BenchExpr(name = "bibtexSwapper")
    public static Expression buildBibtexSwapper() {
        ImmutableList<String> headers = ImmutableList.of("incollection",
                "inproceedings", "InProceedings", "INPROCEEDINGS", "book",
                "article");

        ImmutableList<String> attributes = ImmutableList.of("year", "isbn",
                "series", "editor", "doi", "booktitle", "url", "publisher",
                "author", "pages", "volume", "acmid", "address", "organization");

        CharPred openPar = new CharPred('{');
        CharPred closePar = new CharPred('}');
        CharPred comma = new CharPred(',');
        CharPred eq = new CharPred('=');
        CharPred notPar = solver.MkNot(solver.MkOr(openPar, closePar));

        CharPred notComma = solver.MkNot(comma);
        CharPred notCommanotSpaces = solver.MkAnd(notComma, solver.MkNot(StdCharPred.SPACES));

        Expression copySpaces = id(StdCharPred.SPACES, solver);
        Expression copyOpenPar = predToSelf(openPar, solver);
        Expression copyClosedPar = predToSelf(closePar, solver);
        Expression copyValues = new SplitSum(copyOpenPar, new SplitSum(id(notPar, solver), copyClosedPar));
        Expression copyAt = predToSelf(new CharPred('@'), solver);
        Expression copyHeader = allStringsToSelves(headers, solver);
        Expression copyAttribute = allStringsToSelves(attributes, solver);
        Expression copyalphanum = idPlus(StdCharPred.ALPHA_NUM, solver);
        Expression copyNotComma = idPlus(notCommanotSpaces, solver);
        Expression copyComma = predToSelf(comma, solver);
        Expression copyEqual = new SplitSum(copySpaces, new SplitSum(predToSelf(eq, solver), copySpaces));

        Expression deleteOpenPar = predToEpsilon(openPar);
        Expression deleteClosedPar = predToEpsilon(closePar);
        Expression deleteValues = new SplitSum(deleteOpenPar, new SplitSum(new IteratedSum(predToEpsilon(notPar)), deleteClosedPar));
        Expression deleteAttribute = allStringsToEpsilon(attributes);
        Expression deleteSpaces = new IteratedSum(predToEpsilon(StdCharPred.SPACES));
        Expression deleteEqual = new SplitSum(deleteSpaces, new SplitSum(predToEpsilon(eq), deleteSpaces));
        Expression deleteComma = predToEpsilon(comma);

        Expression copyNontitleAttr = new SplitSum(copyAttribute, new SplitSum(copyEqual, copyValues));
        Expression copyTitleAttr = new SplitSum(stringToSelf("title", solver), new SplitSum(copyEqual, copyValues));
        Expression copyNontitleAttrComma = new SplitSum(copyNontitleAttr, copyComma);
        Expression copyTitleAttrComma = new SplitSum(copyTitleAttr, copyComma);

        Expression deleteNontitleAttr = new SplitSum(deleteAttribute, new SplitSum(deleteEqual, deleteValues));
        Expression deleteTitleAttr = new SplitSum(stringToEpsilon("title"), new SplitSum(deleteEqual, deleteValues));
        Expression deleteNontitleAttrComma = new SplitSum(deleteNontitleAttr, deleteComma);
        Expression deleteTitleAttrComma = new SplitSum(deleteTitleAttr, deleteComma);

        Expression copyNonTitle
                = new SplitSum(
                        new IteratedSum(
                                new IfElse(
                                        new SplitSum(copyNontitleAttrComma, copySpaces),
                                        new SplitSum(deleteTitleAttrComma, deleteSpaces)
                                )
                        ),
                        new IfElse(
                                new SplitSum(
                                        new IfElse(copyNontitleAttrComma,
                                                new SplitSum(copyNontitleAttr, new Epsilon(","))
                                        ),
                                        copySpaces
                                ),
                                new SplitSum(
                                        new IfElse(
                                                deleteTitleAttrComma,
                                                deleteTitleAttr
                                        ),
                                        deleteSpaces
                                )
                        )
                );

        Expression copyTitle
                = new SplitSum(
                        new IteratedSum(
                                new IfElse(
                                        new SplitSum(copyTitleAttrComma, copySpaces),
                                        new SplitSum(deleteNontitleAttrComma, deleteSpaces)
                                )
                        ),
                        new IfElse(
                                new SplitSum(new IfElse(copyTitleAttrComma, new SplitSum(copyTitleAttr, new Epsilon(","))), copySpaces),
                                new SplitSum(
                                        new IfElse(deleteNontitleAttrComma, deleteNontitleAttr),
                                        deleteSpaces
                                )
                        )
                );

        Expression[] splitters = new Expression[] { copyAt,
                copyHeader, copySpaces, copyOpenPar, copySpaces, copyNotComma,
                copyComma, copySpaces, new Sum(copyTitle ,copyNonTitle),
                copyClosedPar, copySpaces };

        Expression editEntry = SplitSum.of(splitters);
        Expression program = new IteratedSum(editEntry);
        return program;
    }

    @BenchExpr(name = "bibtexShuffle")
    public static Expression buildBibtexShuffle() {
        ImmutableList<String> headers = ImmutableList.of("incollection",
                "inproceedings", "InProceedings", "INPROCEEDINGS", "book",
                "article");

        ImmutableList<String> attributes = ImmutableList.of("year", "isbn",
                "series", "editor", "doi", "booktitle", "url", "publisher",
                "author", "pages", "volume", "acmid", "address", "organization");

        CharPred openPar = new CharPred('{');
        CharPred closePar = new CharPred('}');
        CharPred comma = new CharPred(',');
        CharPred eq = new CharPred('=');
        CharPred notPar = solver.MkNot(solver.MkOr(openPar, closePar));

        CharPred notComma = solver.MkNot(comma);
        CharPred notCommanotSpaces = solver.MkAnd(notComma, solver.MkNot(StdCharPred.SPACES));

        Expression copySpaces = id(StdCharPred.SPACES, solver);
        Expression copyOpenPar = predToSelf(openPar, solver);
        Expression copyClosedPar = predToSelf(closePar, solver);
        Expression copyValues = SplitSum.of(copyOpenPar, id(notPar, solver), copyClosedPar);
        Expression copyAt = predToSelf(new CharPred('@'), solver);
        Expression copyHeader = allStringsToSelves(headers, solver);
        Expression copyAttribute = allStringsToSelves(attributes, solver);
        Expression copyNotComma = idPlus(notCommanotSpaces, solver);
        Expression copyComma = predToSelf(comma, solver);
        Expression copyEqual = SplitSum.of(copySpaces, predToSelf(eq, solver), copySpaces);

        Expression deleteOpenPar = predToEpsilon(openPar);
        Expression deleteClosedPar = predToEpsilon(closePar);
        Expression deleteValues = SplitSum.of(deleteOpenPar, new IteratedSum(predToEpsilon(notPar)), deleteClosedPar);
        Expression deleteAt = predToEpsilon(new CharPred('@'));
        Expression deleteHeader = allStringsToEpsilon(headers);
        Expression deleteAttribute = allStringsToEpsilon(attributes);
        Expression deleteNotComma = predToEpsilonPlus(notCommanotSpaces);
        Expression deleteSpaces = new IteratedSum(predToEpsilon(StdCharPred.SPACES));
        Expression deleteEqual = SplitSum.of(deleteSpaces, predToEpsilon(eq), deleteSpaces);
        Expression deleteComma = predToEpsilon(comma);

        Expression copyNontitleAttr = SplitSum.of(copyAttribute, copyEqual, copyValues);
        Expression copyTitleAttr = SplitSum.of(stringToSelf("title", solver), copyEqual, copyValues);
        Expression copyNontitleAttrComma = SplitSum.of(copyNontitleAttr, copyComma);
        Expression copyTitleAttrComma = SplitSum.of(copyTitleAttr, copyComma);

        Expression deleteNontitleAttr = SplitSum.of(deleteAttribute, deleteEqual, deleteValues);
        Expression deleteTitleAttr = SplitSum.of(stringToEpsilon("title"), deleteEqual, deleteValues);
        Expression deleteNontitleAttrComma = SplitSum.of(deleteNontitleAttr, deleteComma);
        Expression deleteTitleAttrComma = SplitSum.of(deleteTitleAttr, deleteComma);

        Expression deleteAttrs = SplitSum.of(new IteratedSum(new IfElse(SplitSum.of(deleteNontitleAttrComma, deleteSpaces),
                                                                        SplitSum.of(deleteTitleAttrComma, deleteSpaces))),
                                             new IfElse(SplitSum.of(new IfElse(deleteNontitleAttrComma, deleteNontitleAttr),
                                                                    deleteSpaces),
                                                        SplitSum.of(new IfElse(deleteTitleAttrComma, deleteTitleAttr),
                                                                    deleteSpaces)));

        Expression copyNonTitle = SplitSum.of(new IteratedSum(new IfElse(SplitSum.of(copyNontitleAttrComma, copySpaces),
                                                                         SplitSum.of(deleteTitleAttrComma, deleteSpaces))),
                                              new IfElse(SplitSum.of(new IfElse(copyNontitleAttrComma,
                                                                                SplitSum.of(copyNontitleAttr, new Epsilon(","))),
                                                                     copySpaces),
                                                         SplitSum.of(new IfElse(deleteTitleAttrComma, deleteTitleAttr),
                                                                     deleteSpaces)));

        Expression copyTitle = SplitSum.of(new IteratedSum(new IfElse(SplitSum.of(copyTitleAttrComma, copySpaces),
                                                                      SplitSum.of(deleteNontitleAttrComma, deleteSpaces))),
                                           new IfElse(SplitSum.of(new IfElse(copyTitleAttrComma,
                                                                             SplitSum.of(copyTitleAttr,
                                                                                         new Epsilon(","))),
                                                                  copySpaces),
                                                      SplitSum.of(new IfElse(deleteNontitleAttrComma, deleteNontitleAttr),
                                                                  deleteSpaces)));

        Expression[] firstHeader = new Expression[] {
                copyAt,
                copyHeader, copySpaces, copyOpenPar, copySpaces,
                copyNotComma,
                copyComma, copySpaces,
                deleteAttrs,
                deleteClosedPar, deleteSpaces };

        Expression[] sndTitle = new Expression[] {
                deleteAt,
                deleteHeader, deleteSpaces, deleteOpenPar, deleteSpaces, deleteNotComma,
                deleteComma, deleteSpaces,
                copyTitle,
                deleteClosedPar, deleteSpaces };

        Expression[] firstAllButTitle = new Expression[] {
                deleteAt,
                deleteHeader, deleteSpaces, deleteOpenPar, deleteSpaces, deleteNotComma,
                deleteComma, deleteSpaces,
                copyNonTitle,
                copyClosedPar, copySpaces };

        Expression[] sndClose = new Expression[] {
                deleteAt,
                deleteHeader, deleteSpaces, deleteOpenPar, deleteSpaces, deleteNotComma,
                deleteComma, deleteSpaces,
                deleteAttrs,
                deleteClosedPar, deleteSpaces };

        Expression editTwoEntries = new Sum(SplitSum.of(SplitSum.of(firstHeader), SplitSum.of(sndTitle)),
                                            SplitSum.of(SplitSum.of(firstAllButTitle), SplitSum.of(sndClose)));

        try {
            return new ChainedSum(editTwoEntries);
        } catch (TypeCheckingException e) {
            throw new AssertionViolation(e);
        }
    }


}
