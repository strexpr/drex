package edu.upenn.cis.drex.bench;

import static com.google.common.base.Preconditions.checkArgument;

public class BenchmarkEvaluatorParams {

    public BenchmarkEvaluatorParams(int iterationCount, long localTimeoutNano,
            long globalTimeoutNano, int abandonCount) {
        checkArgument(iterationCount >= 0);
        checkArgument(localTimeoutNano >= 0);
        checkArgument(globalTimeoutNano >= 0);
        checkArgument(abandonCount >= 0);

        this.iterationCount = iterationCount;
        this.localTimeoutNano = localTimeoutNano;
        this.globalTimeoutNano = globalTimeoutNano;
        this.abandonCount = abandonCount;
    }

    @Override
    public String toString() {
        return String.format("BenchmarkEvaluatorParams(%d, %d, %d, %d)",
                iterationCount, localTimeoutNano, globalTimeoutNano, abandonCount);
    }

    public final int iterationCount;
    public final long localTimeoutNano;
    public final long globalTimeoutNano;
    public final int abandonCount;

}
