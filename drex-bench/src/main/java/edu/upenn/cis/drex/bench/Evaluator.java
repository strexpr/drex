package edu.upenn.cis.drex.bench;

import java.util.concurrent.TimeoutException;
import javax.annotation.Nullable;

public interface Evaluator {

    public @Nullable String eval(String input) throws InterruptedException, TimeoutException;

}
