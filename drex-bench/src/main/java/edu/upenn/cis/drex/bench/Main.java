package edu.upenn.cis.drex.bench;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

public class Main {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        try (PrintWriter tcOut = Util.getBenchmarkOutputWriter("TC")) {
            BenchmarkTypechecker.doBenchmark(tcOut, ITERATION_COUNT);
        }

        BenchmarkEvaluatorParams params = new BenchmarkEvaluatorParams(ITERATION_COUNT,
                ITERATION_TIMEOUT_NANO, GLOBAL_TIMEOUT_NANO, ABANDON_COUNT);
        BenchmarkEvaluator.doSimpleBenchmarks(params);
        BenchmarkEvaluator.doBibtexBenchmarks(params);
    }

    public static final int ITERATION_COUNT = 128;
    public static final long ITERATION_TIMEOUT_NANO = 60000000000l;
    public static final long GLOBAL_TIMEOUT_NANO = 300000000000l;
    public static final int ABANDON_COUNT = 5;

}
