package edu.upenn.cis.drex.bench;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.drex.bench.Util.deepCopy;
import edu.upenn.cis.drex.core.expr.Expression;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang3.time.StopWatch;

public class BenchmarkTypechecker {

    public static void doBenchmark(PrintWriter out, int iterationCount) {
        checkNotNull(out);
        checkArgument(iterationCount >= 0);

        out.printf("BenchmarkTypechecker.doBenchmark(%s, %d).\n", out, iterationCount);
        for (Entry<String, Expression> se : BenchExprs.buildBenchmarkPrograms().entrySet()) {
            Expression benchmark = se.getValue();
            long[] copyTimes = new long[iterationCount];
            StopWatch stopwatch = new StopWatch();

            for (int i = 0; i < iterationCount; i++) {
                if (i % 20 == 0) {
                    Logger logger = Logger.getLogger(BenchmarkTypechecker.class.getName());
                    String msg = String.format("[LOG] Finished %d iterations of %d for benchmark %s.",
                            i, iterationCount, se.getKey());
                    logger.log(Level.ALL, msg);
                }

                stopwatch.reset();
                stopwatch.start();
                Expression copy = deepCopy(benchmark);
                stopwatch.stop();
                copyTimes[i] = stopwatch.getNanoTime();

                checkAssertion(copy != null);
                checkAssertion(copy.getDomainType() != null);
            }

            out.println(String.format("%s: %s", se.getKey(), Arrays.toString(copyTimes)));
        }
    }

}
