package edu.upenn.cis.drex.front.visitors;

import edu.upenn.cis.drex.front.FrontLexer;
import edu.upenn.cis.drex.front.FrontParser;
import edu.upenn.cis.drex.front.errors.FrontErrorListener;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.PredicateEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.util.Util;
import java.io.IOException;
import java.io.InputStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import theory.CharPred;

public class PredicateDefinitionTest {

    @Test
    public void testPredicateDefinition1() throws IOException {
        InputStream inputStream = Util.getTestScript("predicateDefinitionTest1.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        SymbolTable symbolTable = CommandVisitor.eval(parser.commandStar(), null, false);

        CharPred alpha1 = ((PredicateEntry)symbolTable.get("alpha1")).value;
        CharPred alpha2 = ((PredicateEntry)symbolTable.get("alpha2")).value;
        CharPred alpha3 = ((PredicateEntry)symbolTable.get("alpha3")).value;
        CharPred myTrue = ((PredicateEntry)symbolTable.get("myTrue")).value;
        CharPred myFalse = ((PredicateEntry)symbolTable.get("myFalse")).value;
        CharPred myOtherFalse = ((PredicateEntry)symbolTable.get("myOtherFalse")).value;
        CharPred myThirdFalse = ((PredicateEntry)symbolTable.get("myThirdFalse")).value;
        CharPred myOtherTrue = ((PredicateEntry)symbolTable.get("myOtherTrue")).value;
        assertTrue(true);
    }

    @Test(expected = ScriptError.class)
    public void testPredicateDefinition2() throws IOException {
        InputStream inputStream = Util.getTestScript("predicateDefinitionTest2.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        CommandVisitor.eval(parser.commandStar(), null, false);
    }

    @Test(expected = ScriptError.class)
    public void testPredicateDefinition3() throws IOException {
        InputStream inputStream = Util.getTestScript("predicateDefinitionTest3.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        CommandVisitor.eval(parser.commandStar(), null, false);
    }

}
