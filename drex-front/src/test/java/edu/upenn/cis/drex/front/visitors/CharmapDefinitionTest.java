package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.front.FrontLexer;
import edu.upenn.cis.drex.front.FrontParser;
import edu.upenn.cis.drex.front.errors.FrontErrorListener;
import edu.upenn.cis.drex.front.symboltable.CharmapEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.util.Util;
import java.io.IOException;
import java.io.InputStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import theory.CharFunc;

public class CharmapDefinitionTest {

    public void testCharmap(CharFunc fn, ImmutableList<ImmutablePair<Character, Character>> spec) {
        checkNotNull(fn);
        checkNotNull(spec);
        for (ImmutablePair<Character, Character> inOut : spec) {
            char expected = inOut.right;
            char actual = fn.instantiateWith(inOut.left);
            assertEquals(expected, actual);
        }
    }

    @Test
    public void charmapDefinitionTest1() throws IOException {
        InputStream inputStream = Util.getTestScript("charmapDefinitionTest1.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        SymbolTable symbolTable = CommandVisitor.eval(parser.commandStar(), null, false);

        ImmutableList<ImmutablePair<Character, Character>> p0Spec =
                ImmutableList.<ImmutablePair<Character, Character>>builder()
                             .add(ImmutablePair.of('a', 'a'))
                             .add(ImmutablePair.of('b', 'b'))
                             .add(ImmutablePair.of('4', '4'))
                             .add(ImmutablePair.of('Z', 'Z'))
                             .build();
        CharFunc p0 = ((CharmapEntry)symbolTable.get("p0")).value;
        testCharmap(p0, p0Spec);

        ImmutableList<ImmutablePair<Character, Character>> p1Spec =
                ImmutableList.<ImmutablePair<Character, Character>>builder()
                             .add(ImmutablePair.of('a', 'b'))
                             .add(ImmutablePair.of('b', 'c'))
                             .add(ImmutablePair.of('4', '5'))
                             .add(ImmutablePair.of('Y', 'Z'))
                             .build();
        CharFunc p1 = ((CharmapEntry)symbolTable.get("p1")).value;
        testCharmap(p1, p1Spec);

        ImmutableList<ImmutablePair<Character, Character>> toLowerSpec =
                ImmutableList.<ImmutablePair<Character, Character>>builder()
                             .add(ImmutablePair.of('A', 'a'))
                             .add(ImmutablePair.of('Z', 'z'))
                             .build();
        CharFunc toLower = ((CharmapEntry)symbolTable.get("toLower")).value;
        testCharmap(toLower, toLowerSpec);

        ImmutableList<ImmutablePair<Character, Character>> toUpperSpec =
                ImmutableList.<ImmutablePair<Character, Character>>builder()
                             .add(ImmutablePair.of('a', 'A'))
                             .add(ImmutablePair.of('b', 'B'))
                             .build();
        CharFunc toUpper = ((CharmapEntry)symbolTable.get("toUpper")).value;
        testCharmap(toUpper, toUpperSpec);
    }

}
