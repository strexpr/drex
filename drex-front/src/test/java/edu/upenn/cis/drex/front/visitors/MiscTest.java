package edu.upenn.cis.drex.front.visitors;

import edu.upenn.cis.drex.front.FrontLexer;
import edu.upenn.cis.drex.front.FrontParser;
import edu.upenn.cis.drex.front.errors.FrontErrorListener;
import edu.upenn.cis.drex.front.symboltable.StringEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.util.Util;
import java.io.IOException;
import java.io.InputStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class MiscTest {

    @Test
    public void restrictBugTrigger() throws IOException {
        InputStream inputStream = Util.getTestScript("RestrictBugTrigger.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        FrontParser.CommandStarContext context = parser.commandStar();
        SymbolTable objects = CommandVisitor.eval(context, null, false);
        assertEquals("ba", objects.stdout.toString());
    }

    @Test
    public void bigFunBigStr() throws IOException {
        InputStream inputStream = Util.getTestScript("BigFunBigStr.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        FrontParser.CommandStarContext context = parser.commandStar();
        SymbolTable objects = CommandVisitor.eval(context, null, false);
    }

    @Test
    public void prefixDecode() throws IOException {
        InputStream inputStream = Util.getTestScript("PrefixDecode.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        FrontParser.CommandStarContext context = parser.commandStar();
        SymbolTable objects = CommandVisitor.eval(context, null, false);
    }

    @Test
    public void escapeString() throws IOException {
        InputStream inputStream = Util.getTestScript("EscapeString.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        FrontParser.CommandStarContext context = parser.commandStar();
        SymbolTable objects = CommandVisitor.eval(context, "", false);
    }

    @Test
    public void swapNames() throws IOException {
        InputStream inputStream = Util.getTestScript("SwapNames.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        FrontParser.CommandStarContext context = parser.commandStar();
        SymbolTable objects = CommandVisitor.eval(context, null, false);
    }

    @Test
    public void flipLookback1() throws IOException {
        InputStream inputStream = Util.getTestScript("FlipLookback1.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        FrontParser.CommandStarContext context = parser.commandStar();
        SymbolTable objects = CommandVisitor.eval(context, null, false);
    }

    @Test
    public void weirdIter() throws IOException {
        InputStream inputStream = Util.getTestScript("WeirdIter.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        FrontParser.CommandStarContext context = parser.commandStar();
        SymbolTable objects = CommandVisitor.eval(context, null, false);

        String it1 = ((StringEntry)objects.get("it1")).value;
        assertEquals("anan", it1);
    }

}
