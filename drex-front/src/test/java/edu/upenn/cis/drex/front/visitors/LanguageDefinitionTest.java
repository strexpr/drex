package edu.upenn.cis.drex.front.visitors;

import edu.upenn.cis.drex.front.FrontLexer;
import edu.upenn.cis.drex.front.FrontParser;
import edu.upenn.cis.drex.front.errors.FrontErrorListener;
import edu.upenn.cis.drex.front.util.Util;
import java.io.IOException;
import java.io.InputStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.junit.Test;

public class LanguageDefinitionTest {

    @Test
    public void testLanguageDefinition1() throws IOException {
        InputStream inputStream = Util.getTestScript("languageDefinitionTest1.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        CommandVisitor.eval(parser.commandStar(), null, false);
    }

}
