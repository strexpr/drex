package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.forkjoin.Evaluator;
import edu.upenn.cis.drex.core.forkjoin.ExpressionEvaluatorBuilder;
import edu.upenn.cis.drex.front.FrontLexer;
import edu.upenn.cis.drex.front.FrontParser;
import edu.upenn.cis.drex.front.FrontParser.CommandStarContext;
import edu.upenn.cis.drex.front.errors.FrontErrorListener;
import edu.upenn.cis.drex.front.symboltable.RewriteEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.util.Util;
import java.io.IOException;
import java.io.InputStream;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.lang3.tuple.ImmutablePair;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class RewriteDefinitionTest {

    public void testExpression(Expression expr, ImmutableList<ImmutablePair<String, String>> tests) {
        checkNotNull(expr);
        Evaluator evaluator = ExpressionEvaluatorBuilder.build(expr);
        for (ImmutablePair<String, String> test : tests) {
            assertEquals(test.right, evaluator.eval(test.left));
        }
    }

    @Test
    public void rewriteDefinitionTest1() throws IOException {
        InputStream inputStream = Util.getTestScript("rewriteDefinitionTest1.drex");
        ANTLRInputStream antlrInputStream = new ANTLRInputStream(inputStream);
        FrontLexer lexer = new FrontLexer(antlrInputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        CommandStarContext context = parser.commandStar();
        SymbolTable objects = CommandVisitor.eval(context, null, false);

        ImmutableList<ImmutablePair<String, String>> constHelloSpec =
                ImmutableList.<ImmutablePair<String, String>>builder()
                             .add(ImmutablePair.of("", "hello"))
                             .add(ImmutablePair.of("a", "hello"))
                             .add(ImmutablePair.of("06010154", "hello"))
                             .build();
        Expression constHello = ((RewriteEntry)objects.get("constHello")).value;
        testExpression(constHello, constHelloSpec);

        ImmutableList<ImmutablePair<String, String>> alphaConstHelloSpec =
                ImmutableList.<ImmutablePair<String, String>>builder()
                             .add(ImmutablePair.of("", "hello"))
                             .add(ImmutablePair.of("a", "hello"))
                             .add(ImmutablePair.of("06010154", (String)null))
                             .build();
        Expression alphaConstHello1 = ((RewriteEntry)objects.get("alphaConstHello1")).value;
        testExpression(alphaConstHello1, alphaConstHelloSpec);
        Expression alphaConstHello2 = ((RewriteEntry)objects.get("alphaConstHello2")).value;
        testExpression(alphaConstHello2, alphaConstHelloSpec);
        Expression alphaConstHello3 = ((RewriteEntry)objects.get("alphaConstHello3")).value;
        testExpression(alphaConstHello3, alphaConstHelloSpec);

        ImmutableList<ImmutablePair<String, String>> swapSpec =
                ImmutableList.<ImmutablePair<String, String>>builder()
                             .add(ImmutablePair.of("", (String)null))
                             .add(ImmutablePair.of("a", (String)null))
                             .add(ImmutablePair.of("a b", "b a"))
                             .add(ImmutablePair.of("Mukund    Raghothaman", "Raghothaman Mukund"))
                             .build();
        Expression swap1 = ((RewriteEntry)objects.get("swap1")).value;
        testExpression(swap1, swapSpec);
        Expression swap2 = ((RewriteEntry)objects.get("swap2")).value;
        testExpression(swap2, swapSpec);
    }

}
