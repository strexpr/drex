package edu.upenn.cis.drex.front.visitors;

import org.junit.Test;
import static org.junit.Assert.*;

public class LiteralVisitorTest {

    @Test
    public void testProcessLiteral() {
        assertEquals('c', LiteralVisitor.processCharacterLiteral("\"c\""));
        assertEquals('\uAb06', LiteralVisitor.processCharacterLiteral("\"\\uaB06\""));
    }

}
