package edu.upenn.cis.drex.front.util;

import java.io.IOException;
import java.io.InputStream;

public class Util {

    public static InputStream getTestScript(String testName) throws IOException {
        ClassLoader classLoader = Util.class.getClassLoader();
        String fileName = String.format("test-scripts/%s", testName);
        return classLoader.getResourceAsStream(fileName);
    }

}
