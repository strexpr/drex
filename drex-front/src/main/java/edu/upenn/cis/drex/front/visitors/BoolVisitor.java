package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.BoolAndContext;
import edu.upenn.cis.drex.front.FrontParser.BoolContext;
import edu.upenn.cis.drex.front.FrontParser.BoolNotContext;
import edu.upenn.cis.drex.front.FrontParser.BoolOrContext;
import edu.upenn.cis.drex.front.FrontParser.BoolParenContext;
import edu.upenn.cis.drex.front.FrontParser.BoolRegexEqContext;
import edu.upenn.cis.drex.front.FrontParser.BoolRegexLessEqualsContext;
import edu.upenn.cis.drex.front.FrontParser.BoolStrEqContext;
import edu.upenn.cis.drex.front.FrontParser.BoolTripleContext;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import java.util.Objects;
import theory.CharFunc;
import theory.CharPred;
import theory.CharSolver;
import transducers.sst.SST;

public class BoolVisitor extends FrontBaseVisitor<Boolean> {

    BoolVisitor(SymbolTable symbolTable) {
        this.symbolTable = checkNotNull(symbolTable);
    }

    public static boolean interpret(BoolContext ctx,
            SymbolTable symbolTable) {
        BoolVisitor visitor = new BoolVisitor(checkNotNull(symbolTable));
        return visitor.visit(checkNotNull(ctx));
    }

    @Override
    public Boolean visitBoolStrEq(BoolStrEqContext ctx) {
        String str0 = StringVisitor.interpret(checkNotNull(ctx).string(0), symbolTable);
        String str1 = StringVisitor.interpret(ctx.string(1), symbolTable);
        return Objects.equals(str0, str1);
    }

    @Override
    @SuppressWarnings("null")
    public Boolean visitBoolRegexEq(BoolRegexEqContext ctx) {
        Regex r0 = LanguageVisitor.interpret(checkNotNull(ctx).regex(0), symbolTable);
        Regex r1 = LanguageVisitor.interpret(ctx.regex(1), symbolTable);
        checkAssertion(r0.getSFA() != null && r1.getSFA() != null);
        return r0.getSFA().isEquivalentTo(r1.getSFA(), new CharSolver());
    }

    @Override
    @SuppressWarnings("null")
    public Boolean visitBoolRegexLessEquals(BoolRegexLessEqualsContext ctx) {
        Regex r0 = LanguageVisitor.interpret(checkNotNull(ctx).regex(0), symbolTable);
        Regex r1 = LanguageVisitor.interpret(ctx.regex(1), symbolTable);
        checkAssertion(r0.getSFA() != null && r1.getSFA() != null);
        return r0.getSFA().minus(r1.getSFA(), new CharSolver()).isEmpty();
    }

    @Override
    @SuppressWarnings("null")
    public Boolean visitBoolTriple(BoolTripleContext ctx) {
        checkNotNull(ctx);
        Regex pre = LanguageVisitor.interpret(ctx.regex(0), symbolTable);
        Expression rewrite = RewriteVisitor.interpret(ctx.rewrite(), symbolTable);
        Regex post = LanguageVisitor.interpret(ctx.regex(1), symbolTable);
        checkAssertion(pre.getSFA() != null && post.getSFA() != null);

        CharSolver solver = new CharSolver();
        SST<CharPred, CharFunc, Character> sst = rewrite.getSST(solver);
        
        return sst.typeCheck(pre.getSFA(), post.getSFA(), solver);
    }

    @Override
    public Boolean visitBoolNot(BoolNotContext ctx) {
        return !visit(checkNotNull(ctx).bool());
    }

    @Override
    public Boolean visitBoolAnd(BoolAndContext ctx) {
        checkNotNull(ctx);
        return visit(ctx.bool(0)) && visit(ctx.bool(1));
    }

    @Override
    public Boolean visitBoolOr(BoolOrContext ctx) {
        checkNotNull(ctx);
        return visit(ctx.bool(0)) || visit(ctx.bool(1));
    }

    @Override
    public Boolean visitBoolParen(BoolParenContext ctx) {
        return visit(checkNotNull(ctx).bool());
    }

    public final SymbolTable symbolTable;

}
