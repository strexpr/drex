package edu.upenn.cis.drex.front.errors;

public class ScriptError extends RuntimeException {

    public ScriptError(Object message) {
        super(String.valueOf(message));
    }

    public ScriptError(Object message, Throwable cause) {
        super(String.valueOf(message), cause);
    }

}
