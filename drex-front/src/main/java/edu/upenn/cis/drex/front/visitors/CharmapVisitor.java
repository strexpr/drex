package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.CharmapContext;
import edu.upenn.cis.drex.front.FrontParser.CharmapIdContext;
import edu.upenn.cis.drex.front.FrontParser.CharmapPlusContext;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.CharmapEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.symboltable.SymbolTableEntry;
import edu.upenn.cis.drex.front.symboltable.Util;
import theory.CharFunc;
import theory.CharOffset;
import theory.CharPred;

public class CharmapVisitor extends FrontBaseVisitor<CharFunc> {

    CharmapVisitor(SymbolTable symbolTable) {
        this.symbolTable = checkNotNull(symbolTable);
    }

    public static CharFunc interpret(CharmapContext context,
            SymbolTable symbolTable) {
        CharmapVisitor visitor = new CharmapVisitor(checkNotNull(symbolTable));
        return visitor.visit(checkNotNull(context));
    }

    @Override
    public CharFunc visitCharmapId(CharmapIdContext ctx) {
        String id = checkNotNull(ctx).getText();
        SymbolTableEntry entry = symbolTable.get(id);
        checkAssertion(entry instanceof CharmapEntry);
        return ((CharmapEntry)entry).value;
    }

    @Override
    public CharFunc visitCharmapPlus(CharmapPlusContext ctx) {
        long offset = OffsetVisitor.interpret(checkNotNull(ctx).offset(), symbolTable);
        if (offset > (CharPred.MAX_CHAR - CharPred.MIN_CHAR) || offset < (CharPred.MIN_CHAR - CharPred.MAX_CHAR)) {
            String location = Util.getLocation(ctx);
            String errorMsg = String.format("%s. Offset is too large or too small\n"
                    + "\"%l\",\n"
                    + ". Expected offset in range (%d, %d).",
                    location, 
                    offset, 
                    CharPred.MIN_CHAR - CharPred.MAX_CHAR,
                    CharPred.MAX_CHAR - CharPred.MIN_CHAR);
            throw new ScriptError(errorMsg);
        }
        return new CharOffset(offset);
    }

    public final SymbolTable symbolTable;

}
