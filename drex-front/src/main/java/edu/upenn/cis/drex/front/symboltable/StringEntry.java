package edu.upenn.cis.drex.front.symboltable;

import javax.annotation.Nullable;
import org.antlr.v4.runtime.Token;

public class StringEntry extends SymbolTableEntry {

    public StringEntry(@Nullable String value, @Nullable Token token) {
        super(token);
        this.value = value;
    }

    public final String value;

}
