package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.RegexConcatContext;
import edu.upenn.cis.drex.front.FrontParser.RegexContext;
import edu.upenn.cis.drex.front.FrontParser.RegexDomainContext;
import edu.upenn.cis.drex.front.FrontParser.RegexIdContext;
import edu.upenn.cis.drex.front.FrontParser.RegexLabelledContext;
import edu.upenn.cis.drex.front.FrontParser.RegexOptContext;
import edu.upenn.cis.drex.front.FrontParser.RegexParenContext;
import edu.upenn.cis.drex.front.FrontParser.RegexPlusContext;
import edu.upenn.cis.drex.front.FrontParser.RegexPredicateContext;
import edu.upenn.cis.drex.front.FrontParser.RegexStarContext;
import edu.upenn.cis.drex.front.FrontParser.RegexStringContext;
import edu.upenn.cis.drex.front.FrontParser.RegexUnionContext;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.Util;

public class RegexSplitVisitor extends FrontBaseVisitor<ImmutableSet<String>> {

    public static ImmutableSet<String> getTopLevelSplit(RegexContext ctx) {
        RegexSplitVisitor visitor = new RegexSplitVisitor();
        return visitor.visit(checkNotNull(ctx));
    }

    @Override
    public ImmutableSet<String> visitRegexPredicate(RegexPredicateContext ctx) {
        return ImmutableSet.of();
    }

    @Override
    public ImmutableSet<String> visitRegexString(RegexStringContext ctx) {
        return ImmutableSet.of();
    }

    @Override
    public ImmutableSet<String> visitRegexId(RegexIdContext ctx) {
        return ImmutableSet.of();
    }

    @Override
    public ImmutableSet<String> visitRegexDomain(RegexDomainContext ctx) {
        return ImmutableSet.of();
    }

    @Override
    public ImmutableSet<String> visitRegexStar(RegexStarContext ctx) {
        return ImmutableSet.of();
    }

    @Override
    public ImmutableSet<String> visitRegexPlus(RegexPlusContext ctx) {
        return ImmutableSet.of();
    }

    @Override
    public ImmutableSet<String> visitRegexOpt(RegexOptContext ctx) {
        return ImmutableSet.of();
    }

    @Override
    public ImmutableSet<String> visitRegexLabelled(RegexLabelledContext ctx) {
        return ImmutableSet.of(checkNotNull(ctx).ID().getText());
    }

    @Override
    public ImmutableSet<String> visitRegexConcat(RegexConcatContext ctx) {
        ImmutableSet<String> r0t = visit(checkNotNull(ctx).regex(0));
        ImmutableSet<String> r1t = visit(ctx.regex(1));
        ImmutableSet<String> commonIdentifiers = Sets.intersection(r0t, r1t)
                .immutableCopy();
        if (!commonIdentifiers.isEmpty()) {
            String location = Util.getLocation(ctx);
            String errorMsg = String.format("%s. Identifier %s common to both "
                    + "regular expressions in concatenation expression.",
                    location, commonIdentifiers.iterator().next());
            throw new ScriptError(errorMsg);
        }
        return Sets.union(r0t, r1t).immutableCopy();
    }

    @Override
    public ImmutableSet<String> visitRegexUnion(RegexUnionContext ctx) {
        return ImmutableSet.of();
    }

    @Override
    public ImmutableSet<String> visitRegexParen(RegexParenContext ctx) {
        return visit(checkNotNull(ctx).regex());
    }

}
