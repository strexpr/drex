package edu.upenn.cis.drex.front.symboltable;

import static com.google.common.base.Preconditions.checkNotNull;
import javax.annotation.Nullable;
import org.antlr.v4.runtime.Token;
import theory.CharFunc;

public class CharmapEntry extends SymbolTableEntry {

    public CharmapEntry(CharFunc value, @Nullable Token token) {
        super(token);
        this.value = checkNotNull(value);
    }

    public final CharFunc value;

}
