package edu.upenn.cis.drex.front.symboltable;

import static com.google.common.base.Preconditions.checkNotNull;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

public class Util {

    public static String getLocation(int line, int col) {
        return String.format("[Line %d, column %d]", line, col);
    }

    public static String getLocation(Token token) {
        int line = checkNotNull(token).getLine();
        int col = 1 + token.getCharPositionInLine();
        return getLocation(line, col);
    }

    public static String getLocation(ParserRuleContext ctx) {
        return getLocation(checkNotNull(ctx).start);
    }

}
