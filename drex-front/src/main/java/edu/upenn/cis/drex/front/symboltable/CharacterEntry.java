package edu.upenn.cis.drex.front.symboltable;

import javax.annotation.Nullable;
import org.antlr.v4.runtime.Token;

public class CharacterEntry extends SymbolTableEntry {

    public CharacterEntry(char value, @Nullable Token token) {
        super(token);
        this.value = value;
    }

    public final char value;

}
