package edu.upenn.cis.drex.front.symboltable;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.regex.Regex;
import javax.annotation.Nullable;
import org.antlr.v4.runtime.Token;

public class RegexEntry extends SymbolTableEntry {

    public RegexEntry(Regex value, @Nullable Token token) {
        super(token);
        this.value = checkNotNull(value);
    }

    public final Regex value;

}
