package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.collect.ImmutableMap;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import java.util.Map.Entry;

public class LiteralVisitor {

    public static final ImmutableMap<String, Character> escapeSequences =
            ImmutableMap.<String, Character>builder()
                        .put("\\b", '\b')
                        .put("\\t", '\t')
                        .put("\\n", '\n')
                        .put("\\f", '\f')
                        .put("\\r", '\r')
                        .put("\\\"", '\"')
                        .put("\\\'", '\'')
                        .put("\\\\", '\\')
                        .build();

    public static char processUnquotedCharacterLiteral(String charLitStr) {
        checkArgument(charLitStr != null && !charLitStr.isEmpty());
        if (charLitStr.startsWith("\\u")) {
            checkArgument(charLitStr.length() > 2);
            return (char)Integer.parseInt(charLitStr.substring(2), 16);
        } else if (escapeSequences.containsKey(charLitStr)) {
            return escapeSequences.get(charLitStr);
        } else {
            checkAssertion(charLitStr.length() == 1);
            return charLitStr.charAt(0);
        }
    }

    public static char processCharacterLiteral(String charLitStr) {
        checkArgument(charLitStr != null && charLitStr.length() > 2);
        charLitStr = charLitStr.substring(1, charLitStr.length() - 1);
        return processUnquotedCharacterLiteral(charLitStr);
    }

    public static String processStringLiteral(String strLitStr) {
        checkArgument(strLitStr != null && strLitStr.length() >= 2);
        strLitStr = strLitStr.substring(1, strLitStr.length() - 1);
        StringBuilder builder = new StringBuilder();

        while (!strLitStr.isEmpty()) {
            if (strLitStr.startsWith("\\u")) {
                builder.append(processUnquotedCharacterLiteral(strLitStr.substring(0, 6)));
                strLitStr = strLitStr.substring(6);
            } else if (strLitStr.startsWith("\\")) {
                boolean validEscapeSequence = false;
                for (Entry<String, Character> escapeSequence : escapeSequences.entrySet()) {
                    if (strLitStr.startsWith(escapeSequence.getKey())) {
                        builder.append(escapeSequence.getValue());
                        strLitStr = strLitStr.substring(escapeSequence.getKey().length());
                        validEscapeSequence = true;
                        break;
                    }
                }
                checkAssertion(validEscapeSequence);
            } else {
                builder.append(strLitStr.charAt(0));
                strLitStr = strLitStr.substring(1);
            }
        }

        return builder.toString();
    }

    public static int processIntegerLiteral(String intLitStr) {
        return Integer.valueOf(intLitStr);
    }

}
