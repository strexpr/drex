package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.drex.core.expr.Epsilon;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.Restrict;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.expr.Symbol;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.RegexUtil;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.BaseRewriteContext;
import edu.upenn.cis.drex.front.FrontParser.StrCharmapAppContext;
import edu.upenn.cis.drex.front.FrontParser.StrConcatContext;
import edu.upenn.cis.drex.front.FrontParser.StrIdContext;
import edu.upenn.cis.drex.front.FrontParser.StrIntLitContext;
import edu.upenn.cis.drex.front.FrontParser.StrParenContext;
import edu.upenn.cis.drex.front.FrontParser.StrReadFileContext;
import edu.upenn.cis.drex.front.FrontParser.StrRewriteAppContext;
import edu.upenn.cis.drex.front.FrontParser.StrStdinContext;
import edu.upenn.cis.drex.front.FrontParser.StrStrLitContext;
import edu.upenn.cis.drex.front.FrontParser.StringContext;
import edu.upenn.cis.drex.front.FrontParser.StrSubstrContext;
import edu.upenn.cis.drex.front.symboltable.StringEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.symboltable.SymbolTableEntry;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.TerminalNode;
import org.apache.commons.lang3.tuple.ImmutablePair;
import theory.CharFunc;
import theory.CharOffset;
import theory.CharSolver;

public class BaseRewriteVisitor extends FrontBaseVisitor<Expression> {

    BaseRewriteVisitor(SymbolTable symbolTable, ImmutableList<ImmutablePair<String, Regex>> topLevelSplit,
            ImmutableSet<String> topLevelIdentifiers) {
        this.symbolTable = checkNotNull(symbolTable);
        this.topLevelSplit = checkNotNull(topLevelSplit);
        this.topLevelIdentifiers = checkNotNull(topLevelIdentifiers);
    }

    public static Expression interpret(StringContext context,
            ImmutableList<ImmutablePair<String, Regex>> topLevelSplit,
            ImmutableSet<String> topLevelIdentifiers,
            SymbolTable symbolTable) {
        BaseRewriteVisitor visitor = new BaseRewriteVisitor(checkNotNull(symbolTable),
                checkNotNull(topLevelSplit), checkNotNull(topLevelIdentifiers));
        return visitor.visit(checkNotNull(context));
    }

    public static Expression intepret(BaseRewriteContext context, SymbolTable symbolTable) {
        checkNotNull(context);
        checkNotNull(symbolTable);

        LanguageVisitor langVisitor = new LanguageVisitor(symbolTable);
        langVisitor.visit(context.regex());
        ImmutableList<ImmutablePair<String, Regex>> topLevelSplit = langVisitor.getTopLevelSplit();
        ImmutableSet<String> topLevelIdentifiers = langVisitor.getTopLevelIdentifiers();
        StringContext baseOutput = context.string();

        Expression result = interpret(baseOutput, topLevelSplit, topLevelIdentifiers, symbolTable);
        return result;
    }

    public Expression ofString(String string) {
        Expression expr = new Epsilon(checkNotNull(string));
        checkAssertion(expr.getDomainType() != null);
        for (ImmutablePair<String, Regex> sr : topLevelSplit) {
            Expression e = RegexUtil.regexToExpression(sr.right);
            checkAssertion(e.getDomainType() != null);
            expr = new SplitSum(expr, e);
            checkAssertion(expr.getDomainType() != null);
        }
        return expr;
    }

    public Expression buildLookbackExpression(String id, Expression rewrite,
            ParserRuleContext context) {
        checkNotNull(id);
        checkArgument(rewrite != null && rewrite.getDomainType() != null);
        checkNotNull(context);

        Expression expr = new Epsilon("");
        checkAssertion(expr.getDomainType() != null);
        boolean matched = false;
        for (ImmutablePair<String, Regex> sr : topLevelSplit) {
            if (id.equals(sr.left)) {
                Expression ep = new Restrict(rewrite, sr.right);
                RewriteVisitor.checkExpressionTyped(ep, context);
                expr = new SplitSum(expr, ep);
                checkAssertion(!matched);
                matched = true;
            } else {
                Expression e = RegexUtil.regexToExpression(sr.right);
                checkAssertion(e.getDomainType() != null);
                expr = new SplitSum(expr, e);
            }
            checkAssertion(expr.getDomainType() != null);
        }
        checkAssertion(matched);

        return expr;
    }

    @Override
    public Expression visitStrId(StrIdContext context) {
        String id = checkNotNull(context).getText();
        if (symbolTable.containsKey(id)) {
            SymbolTableEntry entry = symbolTable.get(id);
            checkAssertion(entry instanceof StringEntry);
            return ofString(((StringEntry) entry).value);
        } else {
            CharSolver solver = new CharSolver();
            Expression idStr = new IteratedSum(new Symbol(solver.True(), new CharOffset(0)));
            return buildLookbackExpression(id, idStr, context);
        }
    }

    @Override
    public Expression visitStrStrLit(StrStrLitContext context) {
        return ofString(StringVisitor.interpret(checkNotNull(context), symbolTable));
    }

    @Override
    public Expression visitStrIntLit(StrIntLitContext context) {
        TerminalNode intLit = checkNotNull(context).IntegerLiteral();
        int i = LiteralVisitor.processIntegerLiteral(intLit.getText());
        return ofString(Integer.toString(i));
    }

    @Override
    public Expression visitStrStdin(StrStdinContext context) {
        return ofString(StringVisitor.interpret(checkNotNull(context), symbolTable));
    }

    @Override
    public Expression visitStrReadFile(StrReadFileContext context) {
        return ofString(StringVisitor.interpret(checkNotNull(context), symbolTable));
    }

    @Override
    public Expression visitStrConcat(StrConcatContext context) {
        Expression expr0 = visit(checkNotNull(context).string(0));
        Expression expr1 = visit(context.string(1));
        checkAssertion(expr0.getDomainType() != null &&
                expr1.getDomainType() != null);
        Expression expr = new Sum(expr0, expr1);
        checkAssertion(expr.getDomainType() != null);
        return expr;
    }

    @Override
    public Expression visitStrSubstr(StrSubstrContext context) {
        return ofString(StringVisitor.interpret(checkNotNull(context), symbolTable));
    }

    @Override
    public Expression visitStrRewriteApp(StrRewriteAppContext context) {
        checkNotNull(context);
        if (context.string() instanceof StrIdContext &&
                !symbolTable.containsKey(context.string().getText())) {
            Expression rewrite = RewriteVisitor.interpret(context.rewrite(), symbolTable);
            checkAssertion(rewrite.getDomainType() != null);
            String id = context.string().getText();
            return buildLookbackExpression(id, rewrite, context);
        } else {
            return ofString(StringVisitor.interpret(context, symbolTable));
        }
    }

    @Override
    public Expression visitStrCharmapApp(StrCharmapAppContext context) {
        checkNotNull(context);
        if (context.string() instanceof StrIdContext &&
                !symbolTable.containsKey(context.string().getText())) {
            CharFunc func = CharmapVisitor.interpret(context.charmap(), symbolTable);
            Expression rewrite = new Symbol(new CharSolver().True(), func);
            String id = context.string().getText();
            return buildLookbackExpression(id, rewrite, context);
        } else {
            return ofString(StringVisitor.interpret(context, symbolTable));
        }
    }

    @Override
    public Expression visitStrParen(StrParenContext context) {
        return visit(checkNotNull(context).string());
    }

    public final SymbolTable symbolTable;
    public final ImmutableList<ImmutablePair<String, Regex>> topLevelSplit;
    public final ImmutableSet<String> topLevelIdentifiers;

}
