package edu.upenn.cis.drex.front.symboltable;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.base.Supplier;
import com.google.common.base.Suppliers;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.regex.Regex;
import java.util.HashMap;
import org.antlr.v4.runtime.Token;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.front.errors.ScriptError;
import javax.annotation.Nullable;
import theory.CharFunc;
import theory.CharPred;

public class SymbolTable extends HashMap<String, SymbolTableEntry> {

    public SymbolTable(@Nullable String stdin, boolean onWebServer) {
        this(Suppliers.ofInstance(stdin), onWebServer);
    }

    public SymbolTable(Supplier<String> stdin, boolean onWebServer) {
        this.stdin = Suppliers.memoize(checkNotNull(stdin));
        this.stdout = new StringBuilder();
        this.onWebServer = onWebServer;
    }

    public void checkReassignment(String id, Token token) {
        SymbolTableEntry entry = this.get(id);
        if (entry != null) {
            String location = Util.getLocation(token);
            checkAssertion(entry.token != null);
            String errorMsg = String.format("%s. Attempting to redefine "
                    + "identifier %s. Previously defined at location %s.",
                    location, id, Util.getLocation(entry.token));
            throw new ScriptError(errorMsg);
        }
    }

    public void assign(String id, char value, Token token) {
        checkReassignment(checkNotNull(id), checkNotNull(token));
        this.put(id, new CharacterEntry(value, token));
    }

    public void assign(String id, String value, Token token) {
        checkReassignment(checkNotNull(id), checkNotNull(token));
        this.put(id, new StringEntry(value, token));
    }

    public void assign(String id, CharPred value, Token token) {
        checkReassignment(checkNotNull(id), checkNotNull(token));
        this.put(id, new PredicateEntry(checkNotNull(value), token));
    }

    public void assign(String id, CharFunc value, Token token) {
        checkReassignment(checkNotNull(id), checkNotNull(token));
        this.put(id, new CharmapEntry(checkNotNull(value), token));
    }

    public void assign(String id, Regex value, Token token) {
        checkReassignment(checkNotNull(id), checkNotNull(token));
        super.put(id, new RegexEntry(checkNotNull(value), token));
    }

    public void assign(String id, Expression value, Token token) {
        checkReassignment(checkNotNull(id), checkNotNull(token));
        this.put(id, new RewriteEntry(checkNotNull(value), token));
    }

    public final Supplier<String> stdin;
    public final StringBuilder stdout;
    public final boolean onWebServer;

    public static final int ONLINE_LIMIT_SCRIPT_LEN = 32768;
    public static final int ONLINE_LIMIT_EXPR_SIZE = 2048;
    public static final int ONLINE_LIMIT_STR_LEN = 32768;

}
