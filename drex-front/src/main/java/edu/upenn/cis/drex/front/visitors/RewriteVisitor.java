package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.drex.core.domaintype.TypeCheckingException;
import edu.upenn.cis.drex.core.expr.Bottom;
import edu.upenn.cis.drex.core.expr.ChainedSum;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.expr.ExpressionUtil;
import edu.upenn.cis.drex.core.expr.IfElse;
import edu.upenn.cis.drex.core.expr.IteratedSum;
import edu.upenn.cis.drex.core.expr.SplitSum;
import edu.upenn.cis.drex.core.expr.Sum;
import edu.upenn.cis.drex.core.regex.Regex;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.RewriteBaseContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteBottomContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteChainExplicitContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteChainInferredContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteCombineContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteElseContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteIdContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteIterContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteLeftChainExplicitContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteLeftChainInferredContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteLeftIterContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteLeftSplitContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteParenContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteSplitContext;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.RewriteEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.symboltable.SymbolTableEntry;
import edu.upenn.cis.drex.front.symboltable.Util;
import org.antlr.v4.runtime.ParserRuleContext;

public class RewriteVisitor extends FrontBaseVisitor<Expression> {

    RewriteVisitor(SymbolTable symbolTable) {
        this.symbolTable = checkNotNull(symbolTable);
    }

    public static Expression interpret(RewriteContext context,
            SymbolTable symbolTable) {
        RewriteVisitor visitor = new RewriteVisitor(checkNotNull(symbolTable));
        Expression rewrite = visitor.visit(checkNotNull(context));
        if (symbolTable.onWebServer) {
            int rewriteSize = ExpressionUtil.getSizeOf(rewrite);
            if (rewriteSize > SymbolTable.ONLINE_LIMIT_EXPR_SIZE) {
                String errorMsg = String.format("%s. Expression of size %d. "
                        + "Only submit expressions of size <= %d please.",
                        Util.getLocation(context), rewriteSize,
                        SymbolTable.ONLINE_LIMIT_EXPR_SIZE);
                throw new ScriptError(errorMsg);
            }
        }
        return rewrite;
    }

    public static void checkExpressionTyped(Expression rewrite, ParserRuleContext ctx) {
        if (checkNotNull(rewrite).getDomainType() == null) {
            String location = Util.getLocation(checkNotNull(ctx));
            String errorMsg = String.format("%s. Ill-typed expression %s.\n%s",
                    location, rewrite,
                    rewrite.getDomainTypeError().errorMsg);
            throw new ScriptError(errorMsg);
        }
    }

    @Override
    public Expression visitRewriteBottom(RewriteBottomContext ctx) {
        Expression rewrite = new Bottom();
        checkAssertion(rewrite.getDomainType() != null);
        return rewrite;
    }

    @Override
    public Expression visitRewriteId(RewriteIdContext ctx) {
        String id = checkNotNull(ctx).getText();
        SymbolTableEntry entry = symbolTable.get(id);
        checkAssertion(entry instanceof RewriteEntry);
        Expression rewrite = ((RewriteEntry)entry).value;
        checkAssertion(rewrite.getDomainType() != null);
        return rewrite;
    }

    @Override
    public Expression visitRewriteBase(RewriteBaseContext ctx) {
        Expression rewrite = BaseRewriteVisitor.intepret(checkNotNull(ctx).baseRewrite(), symbolTable);
        checkExpressionTyped(rewrite, ctx);
        return rewrite;
    }

    @Override
    public Expression visitRewriteElse(RewriteElseContext ctx) {
        Expression e0 = visit(checkNotNull(ctx).rewrite(0));
        Expression e1 = visit(checkNotNull(ctx).rewrite(1));
        Expression rewrite = new IfElse(e0, e1);
        checkExpressionTyped(rewrite, ctx);
        return rewrite;
    }

    @Override
    public Expression visitRewriteSplit(RewriteSplitContext ctx) {
        ImmutableList.Builder<Expression> splits = ImmutableList.builder();
        for (RewriteContext rewriteContext : checkNotNull(ctx).rewrite()) {
            splits.add(visit(rewriteContext));
        }
        Expression rewrite = SplitSum.ofList(splits.build(), false);
        checkExpressionTyped(rewrite, ctx);
        return rewrite;
    }

    @Override
    public Expression visitRewriteLeftSplit(RewriteLeftSplitContext ctx) {
        ImmutableList.Builder<Expression> splits = ImmutableList.builder();
        for (RewriteContext rewriteContext : checkNotNull(ctx).rewrite()) {
            splits.add(visit(rewriteContext));
        }
        Expression rewrite = SplitSum.ofList(splits.build(), true);
        checkExpressionTyped(rewrite, ctx);
        return rewrite;
    }

    @Override
    public Expression visitRewriteCombine(RewriteCombineContext ctx) {
        ImmutableList.Builder<Expression> splits = ImmutableList.builder();
        for (RewriteContext rewriteContext : checkNotNull(ctx).rewrite()) {
            splits.add(visit(rewriteContext));
        }
        Expression rewrite = Sum.ofList(splits.build());
        checkExpressionTyped(rewrite, ctx);
        return rewrite;
    }

    @Override
    public Expression visitRewriteIter(RewriteIterContext ctx) {
        Expression e = visit(checkNotNull(ctx).rewrite());
        Expression rewrite = new IteratedSum(e, false);
        checkExpressionTyped(rewrite, ctx);
        return rewrite;
    }

    @Override
    public Expression visitRewriteLeftIter(RewriteLeftIterContext ctx) {
        Expression e = visit(checkNotNull(ctx).rewrite());
        Expression rewrite = new IteratedSum(e, true);
        checkExpressionTyped(rewrite, ctx);
        return rewrite;
    }

    @Override
    public Expression visitRewriteChainInferred(RewriteChainInferredContext ctx) {
        Expression e = visit(checkNotNull(ctx).rewrite());
        try {
            Expression rewrite = new ChainedSum(e, false);
            checkAssertion(rewrite.getDomainType() != null);
            return rewrite;
        } catch (TypeCheckingException ex) {
            String location = Util.getLocation(ctx);
            String errorMsg = String.format("%s. Ill-typed expression "
                    + "chain(%s).\n%s", location, ctx.rewrite().getText(),
                    ex.domainTypeError.errorMsg);
            throw new ScriptError(errorMsg);
        }
    }

    @Override
    public Expression visitRewriteChainExplicit(RewriteChainExplicitContext ctx) {
        Expression e = visit(checkNotNull(ctx).rewrite());
        Regex r = LanguageVisitor.interpret(ctx.regex(), symbolTable);
        Expression rewrite = new ChainedSum(e, r, false);
        checkExpressionTyped(rewrite, ctx);
        return rewrite;
    }

    @Override
    public Expression visitRewriteLeftChainInferred(RewriteLeftChainInferredContext ctx) {
        Expression e = visit(checkNotNull(ctx).rewrite());
        try {
            Expression rewrite = new ChainedSum(e, true);
            checkAssertion(rewrite.getDomainType() != null);
            return rewrite;
        } catch (TypeCheckingException ex) {
            String location = Util.getLocation(ctx);
            String errorMsg = String.format("%s. Ill-typed expression "
                    + "left-chain(%s).\n%s", location, ctx.rewrite().getText(),
                    ex.domainTypeError.errorMsg);
            throw new ScriptError(errorMsg);
        }
    }

    @Override
    public Expression visitRewriteLeftChainExplicit(RewriteLeftChainExplicitContext ctx) {
        Expression e = visit(checkNotNull(ctx).rewrite());
        Regex r = LanguageVisitor.interpret(ctx.regex(), symbolTable);
        Expression rewrite = new ChainedSum(e, r, true);
        checkExpressionTyped(rewrite, ctx);
        return rewrite;
    }

    @Override
    public Expression visitRewriteParen(RewriteParenContext ctx) {
        Expression rewrite = visit(checkNotNull(ctx).rewrite());
        checkAssertion(rewrite.getDomainType() != null);
        return rewrite;
    }

    public final SymbolTable symbolTable;

}
