package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.PrintCommandContext;
import edu.upenn.cis.drex.front.FrontParser.PrintFileContext;
import edu.upenn.cis.drex.front.FrontParser.PrintStdoutContext;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.symboltable.Util;
import java.io.FileOutputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;

public class PrintVisitor extends FrontBaseVisitor<Void> {

    PrintVisitor(SymbolTable symbolTable) {
        this.symbolTable = checkNotNull(symbolTable);
    }

    public static void interpret(PrintCommandContext ctx,
            SymbolTable symbolTable) {
        PrintVisitor visitor = new PrintVisitor(checkNotNull(symbolTable));
        visitor.visit(checkNotNull(ctx));
    }

    @Override
    public Void visitPrintStdout(PrintStdoutContext ctx) {
        String string = StringVisitor.interpret(checkNotNull(ctx).string(), symbolTable);
        string = string != null ? string : "null";
        symbolTable.stdout.append(string);
        return null;
    }

    @Override
    public Void visitPrintFile(PrintFileContext ctx) {
        String location = Util.getLocation(ctx);
        if (symbolTable.onWebServer) {
            String errorMsg = String.format("%s. Writing to local files forbidden "
                    + "on demo server.", location);
            throw new ScriptError(errorMsg);
        }

        String string = StringVisitor.interpret(checkNotNull(ctx).string(0), symbolTable);
        string = string != null ? string : "null";
        String fileName = StringVisitor.interpret(ctx.string(1), symbolTable);
        try {
            IOUtils.write(string, new FileOutputStream(fileName));
        } catch (IOException ex) {
            String errorMsg = String.format("%s. Unable to write string %s to "
                    + "file %s.", location, string, fileName);
            throw new ScriptError(errorMsg, ex);
        }

        return null;
    }

    public final SymbolTable symbolTable;

}
