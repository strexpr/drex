package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;

import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.AssertCommandContext;
import edu.upenn.cis.drex.front.FrontParser.CharmapDefinitionContext;
import edu.upenn.cis.drex.front.FrontParser.CmdAssertContext;
import edu.upenn.cis.drex.front.FrontParser.CmdDefnCharmapContext;
import edu.upenn.cis.drex.front.FrontParser.CmdDefnLangContext;
import edu.upenn.cis.drex.front.FrontParser.CmdDefnPredContext;
import edu.upenn.cis.drex.front.FrontParser.CmdDefnRewriteContext;
import edu.upenn.cis.drex.front.FrontParser.CmdDefnStrContext;
import edu.upenn.cis.drex.front.FrontParser.CmdPrintContext;
import edu.upenn.cis.drex.front.FrontParser.CommandStarContext;
import edu.upenn.cis.drex.front.FrontParser.LanguageDefinitionContext;
import edu.upenn.cis.drex.front.FrontParser.PredicateDefinitionContext;
import edu.upenn.cis.drex.front.FrontParser.PrintCommandContext;
import edu.upenn.cis.drex.front.FrontParser.RewriteDefinitionContext;
import edu.upenn.cis.drex.front.FrontParser.StringDefinitionContext;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import javax.annotation.Nullable;
import theory.CharFunc;
import theory.CharPred;

public class CommandVisitor extends FrontBaseVisitor<Void> {

    public CommandVisitor(@Nullable String stdin, boolean onWebServer) {
        this.symbolTable = new SymbolTable(stdin, onWebServer);
    }

    public static SymbolTable eval(CommandStarContext ctx,
            @Nullable String stdin, boolean onWebServer) {
        CommandVisitor visitor = new CommandVisitor(stdin, onWebServer);
        visitor.visit(checkNotNull(ctx));
        return visitor.symbolTable;
    }

    @Override
    public Void visitCmdDefnStr(CmdDefnStrContext ctx) {
        StringDefinitionContext defn = checkNotNull(ctx).stringDefinition();
        String id = defn.ID().getText();
        String value = StringVisitor.interpret(defn.string(), symbolTable);
        symbolTable.assign(id, value, ctx.start);
        return null;
    }

    @Override
    public Void visitCmdDefnPred(CmdDefnPredContext ctx) {
        PredicateDefinitionContext defn = checkNotNull(ctx).predicateDefinition();
        String id = defn.ID().getText();
        CharPred value = PredicateVisitor.interpret(defn.predicate(), symbolTable);
        symbolTable.assign(id, value, ctx.start);
        return null;
    }

    @Override
    public Void visitCmdDefnCharmap(CmdDefnCharmapContext ctx) {
        CharmapDefinitionContext defn = checkNotNull(ctx).charmapDefinition();
        String id = defn.ID().getText();
        CharFunc value = CharmapVisitor.interpret(defn.charmap(), symbolTable);
        symbolTable.assign(id, value, ctx.start);
        return null;
    }

    @Override
    public Void visitCmdDefnLang(CmdDefnLangContext ctx) {
        LanguageDefinitionContext defn = checkNotNull(ctx).languageDefinition();
        String id = defn.ID().getText();
        Regex value = LanguageVisitor.interpret(defn.regex(), symbolTable);
        symbolTable.assign(id, value, ctx.start);
        return null;
    }

    @Override
    public Void visitCmdDefnRewrite(CmdDefnRewriteContext ctx) {
        RewriteDefinitionContext defn = checkNotNull(ctx).rewriteDefinition();
        String id = defn.ID().getText();
        Expression value = RewriteVisitor.interpret(defn.rewrite(), symbolTable);
        symbolTable.assign(id, value, ctx.start);
        return null;
    }

    @Override
    public Void visitCmdPrint(CmdPrintContext ctx) {
        PrintCommandContext cmd = checkNotNull(ctx).printCommand();
        PrintVisitor.interpret(cmd, symbolTable);
        return null;
    }

    @Override
    public Void visitCmdAssert(CmdAssertContext ctx) {
        AssertCommandContext cmd = checkNotNull(ctx).assertCommand();
        AssertVisitor.interpret(cmd, symbolTable);
        return null;
    }

    private final SymbolTable symbolTable;

}
