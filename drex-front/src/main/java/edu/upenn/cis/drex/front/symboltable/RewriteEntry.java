package edu.upenn.cis.drex.front.symboltable;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.expr.Expression;
import javax.annotation.Nullable;
import org.antlr.v4.runtime.Token;

public class RewriteEntry extends SymbolTableEntry {

    public RewriteEntry(Expression value, @Nullable Token token) {
        super(token);
        this.value = checkNotNull(value);
    }

    public final Expression value;

}
