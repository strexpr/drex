package edu.upenn.cis.drex.front.errors;

import edu.upenn.cis.drex.front.symboltable.Util;
import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

public class FrontErrorListener extends BaseErrorListener {

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol,
            int line, int charPositionInLine, String msg, RecognitionException e) {
        String errorMsg = String.format("%s. %s", Util.getLocation(line, 1 + charPositionInLine), msg);
        throw new ScriptError(errorMsg, e);
    }

}
