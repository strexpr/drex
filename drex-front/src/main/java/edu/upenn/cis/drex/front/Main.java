package edu.upenn.cis.drex.front;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.front.FrontParser.CommandStarContext;
import edu.upenn.cis.drex.front.errors.FrontErrorListener;
import edu.upenn.cis.drex.front.visitors.CommandVisitor;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.io.IOUtils;

public class Main {

    public static void main(String[] args) throws ParseException, IOException {
        Options options = new Options();
        options.addOption(Option.builder(HELP_CMD_OPTION)
                                .desc("Print this help message")
                                .required(false)
                                .build());

        DefaultParser parser = new DefaultParser();
        CommandLine cmdLine = parser.parse(options, args, true);
        List<String> cmdLineArgs = cmdLine.getArgList();

        if (cmdLine.hasOption(HELP_CMD_OPTION) || cmdLineArgs.size() != 1) {
            printHelp(options);
        } else {
            eval(cmdLineArgs.get(0));
        }
    }

    public static void eval(String scriptName) throws IOException {
        FileInputStream script = new FileInputStream(checkNotNull(scriptName));
        ANTLRInputStream inputStream = new ANTLRInputStream(script);
        FrontLexer lexer = new FrontLexer(inputStream);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FrontParser parser = new FrontParser(tokens);
        parser.removeErrorListeners();
        parser.addErrorListener(new FrontErrorListener());
        CommandStarContext ctx = parser.commandStar();

        String input = IOUtils.toString(System.in);

        System.out.print(CommandVisitor.eval(ctx, input, false).stdout.toString());
    }

    public static void printHelp(Options options) {
        HelpFormatter formatter = new HelpFormatter();
        String usage = "drex-front [options] [script-file]\n";
        String header = "Executes the DReX program defined in script-file.\n";
        String footer = "The DReX project is hosted at http://www.cis.upenn.edu/~rmukund/drex.html.\n";
        formatter.printHelp(usage, header, checkNotNull(options), footer);
    }

    public static String getBuildDate() throws IOException {
        ClassLoader classLoader = Main.class.getClassLoader();
        String fileName = String.format("BuildDate");
        return IOUtils.toString(classLoader.getResourceAsStream(fileName));
    }

    public static final String HELP_CMD_OPTION = "h";

}
