package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.AssertBoolContext;
import edu.upenn.cis.drex.front.FrontParser.AssertCommandContext;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.symboltable.Util;

public class AssertVisitor extends FrontBaseVisitor<Void> {

    AssertVisitor(SymbolTable symbolTable) {
        this.symbolTable = checkNotNull(symbolTable);
    }

    public static void interpret(AssertCommandContext ctx,
            SymbolTable symbolTable) {
        AssertVisitor visitor = new AssertVisitor(checkNotNull(symbolTable));
        visitor.visit(checkNotNull(ctx));
    }

    @Override
    public Void visitAssertBool(AssertBoolContext ctx) {
        checkNotNull(ctx);
        boolean result = BoolVisitor.interpret(ctx.bool(), symbolTable);
        if (!result) {
            String location = Util.getLocation(ctx);
            String errorMsg = String.format("%s. Assertion failed.", location);
            throw new ScriptError(errorMsg);
        }
        return null;
    }

    public final SymbolTable symbolTable;

}
