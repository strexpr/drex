package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.forkjoin.Evaluator;
import edu.upenn.cis.drex.core.forkjoin.ExpressionEvaluatorBuilder;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.StrCharmapAppContext;
import edu.upenn.cis.drex.front.FrontParser.StrConcatContext;
import edu.upenn.cis.drex.front.FrontParser.StrIdContext;
import edu.upenn.cis.drex.front.FrontParser.StrIntLitContext;
import edu.upenn.cis.drex.front.FrontParser.StrParenContext;
import edu.upenn.cis.drex.front.FrontParser.StrReadFileContext;
import edu.upenn.cis.drex.front.FrontParser.StrRewriteAppContext;
import edu.upenn.cis.drex.front.FrontParser.StrStdinContext;
import edu.upenn.cis.drex.front.FrontParser.StrStrLitContext;
import edu.upenn.cis.drex.front.FrontParser.StrSubstrContext;
import edu.upenn.cis.drex.front.FrontParser.StringContext;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.StringEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.symboltable.SymbolTableEntry;
import edu.upenn.cis.drex.front.symboltable.Util;
import java.io.FileInputStream;
import java.io.IOException;
import org.apache.commons.io.IOUtils;
import theory.CharFunc;

public class StringVisitor extends FrontBaseVisitor<String> {

    StringVisitor(SymbolTable symbolTable) {
        this.symbolTable = checkNotNull(symbolTable);
    }

    public static String interpret(StringContext context,
            SymbolTable symbolTable) {
        StringVisitor visitor = new StringVisitor(checkNotNull(symbolTable));
        String string = visitor.visit(checkNotNull(context));
        if (symbolTable.onWebServer) {
            int stringLen = string.length();
            if (stringLen > SymbolTable.ONLINE_LIMIT_STR_LEN) {
                String errorMsg = String.format("%s. String of length %d. "
                        + "Only submit strings of size <= %d please.",
                        Util.getLocation(context), stringLen,
                        SymbolTable.ONLINE_LIMIT_STR_LEN);
                throw new ScriptError(errorMsg);
            }
        }
        return string;
    }

    @Override
    public String visitStrId(StrIdContext context) {
        String id = checkNotNull(context).ID().getText();
        checkAssertion(symbolTable.containsKey(id));
        SymbolTableEntry entry = symbolTable.get(id);
        checkAssertion(entry instanceof StringEntry);
        return ((StringEntry)entry).value;
    }

    @Override
    public String visitStrStrLit(StrStrLitContext context) {
        return LiteralVisitor.processStringLiteral(checkNotNull(context).getText());
    }

    @Override
    public String visitStrIntLit(StrIntLitContext context) {
        int value = LiteralVisitor.processIntegerLiteral(checkNotNull(context).IntegerLiteral().getText());
        return String.format("%c", (char)value);
    }

    @Override
    public String visitStrStdin(StrStdinContext context) {
        return symbolTable.stdin.get();
    }

    @Override
    public String visitStrReadFile(StrReadFileContext context) {
        checkNotNull(context);

        String location = Util.getLocation(context);
        if (symbolTable.onWebServer) {
            String errorMsg = String.format("%s. Reading local files forbidden "
                    + "on demo server.", location);
            throw new ScriptError(errorMsg);
        }

        String fileName = StringVisitor.interpret(context, symbolTable);
        try {
            return IOUtils.toString(new FileInputStream(fileName));
        } catch (IOException ex) {
            String errorMsg = String.format("%s. Unable to read file %s.", location, fileName);
            throw new ScriptError(errorMsg, ex);
        }
    }

    @Override
    public String visitStrConcat(StrConcatContext context) {
        String str0 = visit(checkNotNull(context).string(0));
        String str1 = visit(context.string(1));

        if (str0 == null) {
            String location = Util.getLocation(context);
            String errorMsg = String.format("%s. String on left of "
                    + "concatenation expression evaluates to null.", location);
            throw new ScriptError(errorMsg);
        } else if (str1 == null) {
            String location = Util.getLocation(context);
            String errorMsg = String.format("%s. String on right of "
                    + "concatenation expression evaluates to null.", location);
            throw new ScriptError(errorMsg);
        }

        return str0.concat(str1);
    }

    @Override
    public String visitStrSubstr(StrSubstrContext context) {
        String str = visit(checkNotNull(context).string());
        int beginIndex = LiteralVisitor.processIntegerLiteral(context.IntegerLiteral(0).getText());
        int endIndex = LiteralVisitor.processIntegerLiteral(context.IntegerLiteral(1).getText());

        if (beginIndex < 0) {
            String location = Util.getLocation(context);
            String errorMsg = String.format("%s. Begin index evaluates to %d. "
                    + "Expected a non-negative number instead.", location,
                    beginIndex);
            throw new ScriptError(errorMsg);
        } else if (endIndex > str.length()) {
            String location = Util.getLocation(context);
            String errorMsg = String.format("%s. String evaluates to\n"
                    + "\"%s\","
                    + "and has length %d.\n"
                    + "End index evaluates to %d. Expected end index "
                    + "less-than-or-equal-to string length.", location, str,
                    str.length(), endIndex);
            throw new ScriptError(errorMsg);
        } else if (beginIndex > endIndex) {
            String location = Util.getLocation(context);
            String errorMsg = String.format("%s. Begin index evaluates to %d, "
                    + "and end index evaluates to %d. Expected begin index <= "
                    + "end index.", location, beginIndex, endIndex);
            throw new ScriptError(errorMsg);
        }

        return str.substring(beginIndex, endIndex);
    }

    @Override
    public String visitStrRewriteApp(StrRewriteAppContext context) {
        checkNotNull(context);

        Expression rewrite = RewriteVisitor.interpret(context.rewrite(), symbolTable);
        checkAssertion(rewrite.getDomainType() != null);

        String string = visit(context.string());
        if (string == null) {
            String location = Util.getLocation(context);
            String errorMsg = String.format("%s. String argument evaluates to "
                    + "null.", location);
            throw new ScriptError(errorMsg);
        }

        Evaluator evaluator = ExpressionEvaluatorBuilder.build(rewrite);
        checkAssertion(evaluator != null);
        return evaluator.eval(string);
    }

    @Override
    public String visitStrCharmapApp(StrCharmapAppContext context) {
        checkNotNull(context);

        CharFunc func = CharmapVisitor.interpret(context.charmap(), symbolTable);
        String str = StringVisitor.interpret(context.string(), symbolTable);

        if (str.length() != 1) {
            String location = Util.getLocation(context);
            String errorMsg = String.format("%s. String argument evaluates to\n"
                    + "\"%s\",\n"
                    + "and has length %d. Expected string of length 1.",
                    location, str, str.length());
            throw new ScriptError(errorMsg);
        }

        char result = func.instantiateWith(str.charAt(0));
        return Character.toString(result);
    }

    @Override
    public String visitStrParen(StrParenContext context) {
        return visit(checkNotNull(context).string());
    }

    public final SymbolTable symbolTable;

}
