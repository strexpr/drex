package edu.upenn.cis.drex.front.symboltable;

import javax.annotation.Nullable;
import org.antlr.v4.runtime.Token;

public abstract class SymbolTableEntry {

    public SymbolTableEntry(@Nullable Token token) {
        this.token = token;
    }

    public final @Nullable Token token;

}
