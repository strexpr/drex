package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.core.util.assertions.UnreachableCodeException;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser;
import edu.upenn.cis.drex.front.FrontParser.OffsetContext;
import edu.upenn.cis.drex.front.FrontParser.OffsetIntContext;
import edu.upenn.cis.drex.front.FrontParser.OffsetPlusMinusContext;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.symboltable.Util;

public class OffsetVisitor extends FrontBaseVisitor<Long> {
    
    OffsetVisitor(SymbolTable symbolTable) {
        this.symbolTable = checkNotNull(symbolTable);
    }

    @Override
    public Long visitOffsetInt(OffsetIntContext context) {
        int offset = LiteralVisitor.processIntegerLiteral(context.getText());
        return (long)offset;
    }
    
    public static Long interpret(OffsetContext context,
            SymbolTable symbolTable) {
        OffsetVisitor visitor = new OffsetVisitor(checkNotNull(symbolTable));
        return visitor.visit(checkNotNull(context));
    }

    
    @Override
    public Long visitOffsetPlusMinus(OffsetPlusMinusContext context) {
        String charLeft = StringVisitor.interpret(checkNotNull(context).string(0), symbolTable);
        String charRight = StringVisitor.interpret(checkNotNull(context).string(1), symbolTable);

        if (charLeft.length() != 1) {
            String location = Util.getLocation(context.string(0));
            String errorMsg = String.format("%s. String on left of "
                    + "arithmetic operator evaluates to\n"
                    + "\"%s\".\n"
                    + "Expected string of length 1.", location, charLeft);
            throw new ScriptError(errorMsg);
        } else if (charRight.length() != 1) {
            String location = Util.getLocation(context.string(1));
            String errorMsg = String.format("%s. String on right of "
                    + "arithmetic operator evaluates to\n"
                    + "\"%s\".\n"
                    + "Expected string of length 1.", location, charRight);
            throw new ScriptError(errorMsg);
        }

        long result = 0;
        switch (context.op.getType()) {
            case FrontParser.PLUS:
                result = ((long)charLeft.charAt(0) + (long)charRight.charAt(0));
                break;
            case FrontParser.MINUS:
                result = ((long)charLeft.charAt(0) - (long)charRight.charAt(0));
                break;
            default:
                throw new UnreachableCodeException();
        }
        return result;
    }
    
    public final SymbolTable symbolTable;

}
