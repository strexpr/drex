package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import edu.upenn.cis.drex.core.domaintype.RegexAmbiguity;
import edu.upenn.cis.drex.core.expr.Expression;
import edu.upenn.cis.drex.core.regex.Concat;
import edu.upenn.cis.drex.core.regex.Epsilon;
import edu.upenn.cis.drex.core.regex.Regex;
import edu.upenn.cis.drex.core.regex.RegexUtil;
import edu.upenn.cis.drex.core.regex.Star;
import edu.upenn.cis.drex.core.regex.Symbol;
import edu.upenn.cis.drex.core.regex.Union;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.RegexConcatContext;
import edu.upenn.cis.drex.front.FrontParser.RegexContext;
import edu.upenn.cis.drex.front.FrontParser.RegexDomainContext;
import edu.upenn.cis.drex.front.FrontParser.RegexIdContext;
import edu.upenn.cis.drex.front.FrontParser.RegexLabelledContext;
import edu.upenn.cis.drex.front.FrontParser.RegexOptContext;
import edu.upenn.cis.drex.front.FrontParser.RegexParenContext;
import edu.upenn.cis.drex.front.FrontParser.RegexPlusContext;
import edu.upenn.cis.drex.front.FrontParser.RegexPredicateContext;
import edu.upenn.cis.drex.front.FrontParser.RegexStarContext;
import edu.upenn.cis.drex.front.FrontParser.RegexStringContext;
import edu.upenn.cis.drex.front.FrontParser.RegexUnionContext;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.RegexEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.symboltable.SymbolTableEntry;
import edu.upenn.cis.drex.front.symboltable.Util;
import org.antlr.v4.runtime.ParserRuleContext;
import org.apache.commons.lang3.tuple.ImmutablePair;
import theory.CharPred;

public class LanguageVisitor extends FrontBaseVisitor<Regex> {

    LanguageVisitor(SymbolTable symbolTable) {
        this.symbolTable = checkNotNull(symbolTable);
        this.topLevelSplit = ImmutableList.of();
        this.topLevelIdentifiers = ImmutableSet.of();
    }

    public static Regex interpret(RegexContext context,
            SymbolTable symbolTable) {
        LanguageVisitor visitor = new LanguageVisitor(checkNotNull(symbolTable));
        Regex regex = visitor.visit(checkNotNull(context));
        if (symbolTable.onWebServer) {
            int regexSize = RegexUtil.getSizeOf(regex);
            if (regexSize > SymbolTable.ONLINE_LIMIT_EXPR_SIZE) {
                String errorMsg = String.format("%s. Expression of size %d. "
                        + "Only submit expressions of size <= %d please.",
                        Util.getLocation(context), regexSize,
                        SymbolTable.ONLINE_LIMIT_EXPR_SIZE);
                throw new ScriptError(errorMsg);
            }
        }
        return regex;
    }

    public ImmutableList<ImmutablePair<String, Regex>> getTopLevelSplit() {
        return topLevelSplit;
    }

    public ImmutableSet<String> getTopLevelIdentifiers() {
        return topLevelIdentifiers;
    }

    public static void checkAmbiguity(Regex regex, ParserRuleContext context) {
        checkNotNull(regex);
        checkNotNull(context);

        RegexAmbiguity ambiguity = regex.getAmbiguity();
        if (ambiguity != null) {
            String location = Util.getLocation(context);
            String errorMsg = String.format("%s. Ambiguous regular expression "
                    + "%s.\n%s", location, regex, ambiguity.errorMsg);
            throw new ScriptError(errorMsg);
        }
    }

    @Override
    public Regex visitRegexPredicate(RegexPredicateContext ctx) {
        CharPred pred = PredicateVisitor.interpret(checkNotNull(ctx).predicate(), symbolTable);
        Regex regex = new Symbol(pred);
        checkAmbiguity(regex, ctx);

        topLevelSplit = ImmutableList.of(ImmutablePair.of((String)null, regex));
        topLevelIdentifiers = ImmutableSet.of();
        return regex;
    }

    @Override
    public Regex visitRegexString(RegexStringContext ctx) {
        String str = StringVisitor.interpret(checkNotNull(ctx).string(), symbolTable);
        Regex regex = RegexUtil.stringToRegex(str);
        checkAssertion(regex.getAmbiguity() == null);

        topLevelSplit = ImmutableList.of(ImmutablePair.of((String)null, regex));
        topLevelIdentifiers = ImmutableSet.of();
        return regex;
    }

    @Override
    public Regex visitRegexId(RegexIdContext ctx) {
        String id = checkNotNull(ctx).getText();
        SymbolTableEntry entry = symbolTable.get(id);
        checkAssertion(entry instanceof RegexEntry);
        Regex regex = ((RegexEntry)entry).value;
        checkAssertion(regex.getAmbiguity() == null);

        topLevelSplit = ImmutableList.of(ImmutablePair.of((String)null, regex));
        topLevelIdentifiers = ImmutableSet.of();
        return regex;
    }

    @Override
    public Regex visitRegexDomain(RegexDomainContext ctx) {
        Expression expr = RewriteVisitor.interpret(checkNotNull(ctx).rewrite(), symbolTable);
        Regex regex = expr.getDomainType();
        checkAssertion(regex != null && regex.getAmbiguity() == null);

        topLevelSplit = ImmutableList.of(ImmutablePair.of((String)null, regex));
        topLevelIdentifiers = ImmutableSet.of();
        return regex;
    }

    @Override
    public Regex visitRegexStar(RegexStarContext ctx) {
        Regex r = visit(checkNotNull(ctx).regex());
        Regex regex = new Star(r);
        checkAmbiguity(regex, ctx);

        topLevelSplit = ImmutableList.of(ImmutablePair.of((String)null, regex));
        topLevelIdentifiers = ImmutableSet.of();
        return regex;
    }

    @Override
    public Regex visitRegexPlus(RegexPlusContext ctx) {
        Regex r = visit(checkNotNull(ctx).regex());
        Regex regex = Star.plus(r);
        checkAmbiguity(regex, ctx);

        topLevelSplit = ImmutableList.of(ImmutablePair.of((String)null, regex));
        topLevelIdentifiers = ImmutableSet.of();
        return regex;
    }

    @Override
    public Regex visitRegexOpt(RegexOptContext ctx) {
        Regex r = visit(checkNotNull(ctx).regex());
        Regex regex = new Union(r, new Epsilon());
        checkAmbiguity(regex, ctx);

        topLevelSplit = ImmutableList.of(ImmutablePair.of((String)null, regex));
        topLevelIdentifiers = ImmutableSet.of();
        return regex;
    }

    @Override
    public Regex visitRegexLabelled(RegexLabelledContext ctx) {
        String id = checkNotNull(ctx).ID().getText();
        Regex regex = visit(ctx.regex());
        checkAssertion(regex.getAmbiguity() == null);
        checkAssertion(!symbolTable.containsKey(id));

        topLevelSplit = ImmutableList.of(ImmutablePair.of(id, regex));
        topLevelIdentifiers = ImmutableSet.of(id);
        return regex;
    }

    @Override
    public Regex visitRegexConcat(RegexConcatContext ctx) {
        Regex r0 = visit(checkNotNull(ctx).regex(0));
        ImmutableList<ImmutablePair<String, Regex>> topLevelSplit0 = topLevelSplit;
        ImmutableSet<String> topLevelIdentifiers0 = topLevelIdentifiers;

        Regex r1 = visit(checkNotNull(ctx).regex(1));
        ImmutableList<ImmutablePair<String, Regex>> topLevelSplit1 = topLevelSplit;
        ImmutableSet<String> topLevelIdentifiers1 = topLevelIdentifiers;

        Regex regex = new Concat(r0, r1);
        checkAmbiguity(regex, ctx);
        ImmutableSet<String> commonIdentifiers = Sets.intersection(topLevelIdentifiers0, topLevelIdentifiers1)
                .immutableCopy();
        checkAssertion(commonIdentifiers.isEmpty());

        topLevelSplit = ImmutableList.<ImmutablePair<String, Regex>>builder()
                                     .addAll(topLevelSplit0)
                                     .addAll(topLevelSplit1)
                                     .build();
        topLevelIdentifiers = Sets.union(topLevelIdentifiers0, topLevelIdentifiers1).immutableCopy();
        return regex;
    }

    @Override
    public Regex visitRegexUnion(RegexUnionContext ctx) {
        Regex r0 = visit(checkNotNull(ctx).regex(0));
        Regex r1 = visit(checkNotNull(ctx).regex(1));
        Regex regex = new Union(r0, r1);
        checkAmbiguity(regex, ctx);

        topLevelSplit = ImmutableList.of(ImmutablePair.of((String)null, regex));
        topLevelIdentifiers = ImmutableSet.of();
        return regex;
    }

    @Override
    public Regex visitRegexParen(RegexParenContext ctx) {
        Regex regex = visit(checkNotNull(ctx).regex());
        checkAssertion(regex.getAmbiguity() == null);
        return regex;
    }

    private final SymbolTable symbolTable;
    private /* nonfinal */ ImmutableList<ImmutablePair<String, Regex>> topLevelSplit;
    private /* nonfinal */ ImmutableSet<String> topLevelIdentifiers;

}
