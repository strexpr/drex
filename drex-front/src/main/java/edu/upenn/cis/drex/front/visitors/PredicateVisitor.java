package edu.upenn.cis.drex.front.visitors;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import static edu.upenn.cis.drex.core.util.assertions.Assertion.checkAssertion;
import edu.upenn.cis.drex.front.FrontBaseVisitor;
import edu.upenn.cis.drex.front.FrontParser.PredAndContext;
import edu.upenn.cis.drex.front.FrontParser.PredIdContext;
import edu.upenn.cis.drex.front.FrontParser.PredNotContext;
import edu.upenn.cis.drex.front.FrontParser.PredOrContext;
import edu.upenn.cis.drex.front.FrontParser.PredParenContext;
import edu.upenn.cis.drex.front.FrontParser.PredRangeContext;
import edu.upenn.cis.drex.front.FrontParser.PredRosterContext;
import edu.upenn.cis.drex.front.FrontParser.PredStrContext;
import edu.upenn.cis.drex.front.FrontParser.PredTrueContext;
import edu.upenn.cis.drex.front.FrontParser.PredicateContext;
import edu.upenn.cis.drex.front.FrontParser.StringContext;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.PredicateEntry;
import edu.upenn.cis.drex.front.symboltable.SymbolTable;
import edu.upenn.cis.drex.front.symboltable.SymbolTableEntry;
import edu.upenn.cis.drex.front.symboltable.Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.apache.commons.lang3.tuple.ImmutablePair;
import theory.CharPred;
import theory.CharSolver;

public class PredicateVisitor extends FrontBaseVisitor<CharPred> {

    PredicateVisitor(SymbolTable symbolTable) {
        this.symbolTable = checkNotNull(symbolTable);
    }

    public static CharPred interpret(PredicateContext ctx,
            SymbolTable symbolTable) {
        PredicateVisitor visitor = new PredicateVisitor(checkNotNull(symbolTable));
        return visitor.visit(checkNotNull(ctx));
    }

    @Override
    public CharPred visitPredStr(PredStrContext ctx) {
        String str = StringVisitor.interpret(checkNotNull(ctx).string(), symbolTable);
        if (str.length() != 1) {
            String location = Util.getLocation(ctx);
            String errorMsg = String.format("%s. String evaluates to\n"
                    + "\"%s\",\n"
                    + "and has length %d. Expected string of length 1.",
                    location, str, str.length());
            throw new ScriptError(errorMsg);
        }
        return new CharPred(str.charAt(0));
    }

    @Override
    public CharPred visitPredRange(PredRangeContext ctx) {
        checkNotNull(ctx);
        String s0 = StringVisitor.interpret(ctx.string(0), symbolTable);
        String s1 = StringVisitor.interpret(ctx.string(1), symbolTable);

        if (s0.length() != 1) {
            String location = Util.getLocation(ctx);
            String errorMsg = String.format("%s. Left endpoint of range "
                    + "evaluates to\n"
                    + "\"%s\",\n"
                    + "and has length %d. Expected string of length 1.",
                    location, s0, s0.length());
            throw new ScriptError(errorMsg);
        } else if (s1.length() != 1) {
            String location = Util.getLocation(ctx);
            String errorMsg = String.format("%s. Right endpoint of range "
                    + "evaluates to\n"
                    + "\"%s\",\n"
                    + "and has length %d. Expected string of length 1.",
                    location, s1, s1.length());
            throw new ScriptError(errorMsg);
        }

        char c0 = s0.charAt(0);
        char c1 = s1.charAt(0);
        if (c0 > c1) {
            String location = Util.getLocation(ctx);
            String errorMsg = String.format("%s. Left endpoint of range "
                    + "evaluates to %s(%d), and right endpoint evaluates to "
                    + "%s(%d). Expected left <= right.", location,
                    CharPred.printChar(c0), (int) c0, CharPred.printChar(c1),
                    (int) c1);
            throw new ScriptError(errorMsg);
        }

        return new CharPred(c0, c1);
    }

    @Override
    public CharPred visitPredRoster(PredRosterContext ctx) {
        List<Character> roster = new ArrayList<>();
        for (StringContext elemCtx : checkNotNull(ctx).string()) {
            String elem = StringVisitor.interpret(checkNotNull(elemCtx), symbolTable);
            if (elem.length() != 1) {
                String location = Util.getLocation(elemCtx);
                String errorMsg = String.format("%s. Element evaluates to\n"
                        + "\"%s\",\n"
                        + "and has length %d. Expected string of length 1.",
                        location, elem, elem.length());
                throw new ScriptError(errorMsg);
            }
            roster.add(elem.charAt(0));
        }
        Collections.sort(roster);

        ImmutableList.Builder<ImmutablePair<Character, Character>> builder = ImmutableList.builder();
        for (char elem : roster) {
            builder.add(ImmutablePair.of(elem, elem));
        }

        return new CharPred(builder.build());
    }

    @Override
    public CharPred visitPredId(PredIdContext ctx) {
        String id = checkNotNull(ctx).getText();
        SymbolTableEntry entry = symbolTable.get(id);
        checkAssertion(entry instanceof PredicateEntry);
        return ((PredicateEntry)entry).value;
    }

    @Override
    public CharPred visitPredTrue(PredTrueContext ctx) {
        checkNotNull(ctx);
        return new CharSolver().True();
    }

    @Override
    public CharPred visitPredNot(PredNotContext ctx) {
        CharSolver solver = new CharSolver();
        return solver.MkNot(visit(checkNotNull(ctx).predicate()));
    }

    @Override
    public CharPred visitPredAnd(PredAndContext ctx) {
        CharPred pred0 = visit(checkNotNull(ctx).predicate(0));
        CharPred pred1 = visit(ctx.predicate(1));
        CharSolver solver = new CharSolver();
        return solver.MkAnd(pred0, pred1);
    }

    @Override
    public CharPred visitPredOr(PredOrContext ctx) {
        CharPred pred0 = visit(checkNotNull(ctx).predicate(0));
        CharPred pred1 = visit(ctx.predicate(1));
        CharSolver solver = new CharSolver();
        return solver.MkOr(pred0, pred1);
    }

    @Override
    public CharPred visitPredParen(PredParenContext ctx) {
        return visit(ctx.predicate());
    }

    private final SymbolTable symbolTable;

}
