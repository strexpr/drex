grammar Front;

@parser::header {

import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.drex.front.errors.ScriptError;
import edu.upenn.cis.drex.front.symboltable.Util;
import edu.upenn.cis.drex.front.visitors.RegexSplitVisitor;
import java.util.HashMap;
import java.util.Map;

}

@parser::members {

public enum IdType {
    CHARACTER,
    STRING,
    PREDICATE,
    CHARMAP,
    LANGUAGE,
    REWRITE,

    CHARACTER_COMBINATOR,
    STRING_COMBINATOR,
    PREDICATE_COMBINATOR,
    CHARMAP_COMBINATOR,
    LANGUAGE_COMBINATOR,
    REWRITE_COMBINATOR
}

private Map<String, IdType> protoSymbolTable = new HashMap<>();

private boolean idIsOfType(Token token, IdType expected) {
    String id = checkNotNull(token).getText();
    return protoSymbolTable.containsKey(id) && protoSymbolTable.get(id) == expected;
}

private boolean idDeclared(Token token) {
    String id = checkNotNull(token).getText();
    return protoSymbolTable.containsKey(id);
}

private void checkRedeclaration(Token token) {
    if (idDeclared(checkNotNull(token))) {
        String id = token.getText();
        String location = Util.getLocation(token);
        String errorMsg = String.format("%s. Attempting to redefine identifier %s.",
                location, id);
        throw new ScriptError(errorMsg);
    }
}

}

commandStar
    : command*;
command
    : stringDefinition                                                          # cmdDefnStr
    | predicateDefinition                                                       # cmdDefnPred
    | charmapDefinition                                                         # cmdDefnCharmap
    | languageDefinition                                                        # cmdDefnLang
    | rewriteDefinition                                                         # cmdDefnRewrite
    | printCommand                                                              # cmdPrint
    | assertCommand                                                             # cmdAssert
    ;

stringDefinition
    : STRING
      { checkRedeclaration(getCurrentToken()); }
      ID EQUALS string PERIOD
      { protoSymbolTable.put($ID.getText(), IdType.STRING); }
    ;
string
    : StringLiteral                                                             # strStrLit
    | UTF16 LPAREN IntegerLiteral RPAREN                                        # strIntLit

    | STDIN                                                                     # strStdin
    | READ LPAREN string RPAREN                                                 # strReadFile

    | { idIsOfType(getCurrentToken(), IdType.STRING) }? ID                      # strId

    | string string                                                             # strConcat
    | SUBSTR LPAREN string COMMA IntegerLiteral COMMA IntegerLiteral RPAREN     # strSubstr 
    | rewrite LPAREN string RPAREN                                              # strRewriteApp
    | charmap LPAREN string RPAREN                                              # strCharmapApp
    | LPAREN string RPAREN                                                      # strParen
    ;

predicateDefinition
    : PREDICATE
      { checkRedeclaration(getCurrentToken()); }
      ID EQUALS predicate PERIOD
      { protoSymbolTable.put($ID.getText(), IdType.PREDICATE); }
    ;
predicate
    : string                                                                    # predStr
    | string ELLIPSIS string                                                    # predRange
    | { idIsOfType(getCurrentToken(), IdType.PREDICATE) }? ID                   # predId
    | LBRACE ( string (COMMA string)* )? RBRACE                                 # predRoster
    | U                                                                         # predTrue
    | NOT predicate                                                             # predNot
    | predicate AND predicate                                                   # predAnd
    | predicate OR predicate                                                    # predOr
    | LPAREN predicate RPAREN                                                   # predParen
    ;

charmapDefinition
    : CHARMAP
      { checkRedeclaration(getCurrentToken()); }
      ID EQUALS charmap PERIOD
      { protoSymbolTable.put($ID.getText(), IdType.CHARMAP); }
    ;
charmap
    : { idIsOfType(getCurrentToken(), IdType.CHARMAP) }? ID                     # charmapId
    | PLUSWORD LPAREN offset RPAREN                                             # charmapPlus
    ;

offset
    : IntegerLiteral                                                            # offsetInt
    | string op = (PLUS | MINUS) string                                         # offsetPlusMinus
    ;

languageDefinition
    : LANGUAGE
      { checkRedeclaration(getCurrentToken()); }
      ID EQUALS regex PERIOD
      { protoSymbolTable.put($ID.getText(), IdType.LANGUAGE); }
    ;
regex
    : string                                                                    # regexString
    | predicate                                                                 # regexPredicate
    | { idIsOfType(getCurrentToken(), IdType.LANGUAGE) }? ID                    # regexId
    | DOMAIN rewrite                                                            # regexDomain
    | regex STAR                                                                # regexStar
    | regex PLUS                                                                # regexPlus
    | regex QUESMARK                                                            # regexOpt
    | { checkRedeclaration(getCurrentToken()); } ID COLON regex                 # regexLabelled
    | regex regex                                                               # regexConcat
    | regex OR regex                                                            # regexUnion
    | LPAREN regex RPAREN                                                       # regexParen
    ;

rewriteDefinition
    : REWRITE
      { checkRedeclaration(getCurrentToken()); }
      ID EQUALS rewrite PERIOD
      { protoSymbolTable.put($ID.getText(), IdType.REWRITE); }
    ;
rewrite
    : BOTTOM                                                                    # rewriteBottom
    | { idIsOfType(getCurrentToken(), IdType.REWRITE) }? ID                     # rewriteId
    | baseRewrite                                                               # rewriteBase
    | TRY rewrite ELSE rewrite                                                  # rewriteElse
    | SPLIT LPAREN rewrite (COMMA rewrite)* RPAREN                              # rewriteSplit
    | LEFTSPLIT LPAREN rewrite (COMMA rewrite)* RPAREN                          # rewriteLeftSplit
    | COMBINE LPAREN rewrite (COMMA rewrite)* RPAREN                            # rewriteCombine
    | ITER rewrite                                                              # rewriteIter
    | LEFTITER rewrite                                                          # rewriteLeftIter
    | CHAIN rewrite                                                             # rewriteChainInferred
    | CHAIN LPAREN rewrite COMMA regex RPAREN                                   # rewriteChainExplicit
    | LEFTCHAIN rewrite                                                         # rewriteLeftChainInferred
    | LEFTCHAIN LPAREN rewrite COMMA regex RPAREN                               # rewriteLeftChainExplicit
    | LPAREN rewrite RPAREN                                                     # rewriteParen
    ;

baseRewrite locals [Map<String, IdType> outerProtoSymbolTable]
    : LSQUARE regex
      SLASH
      {
          $outerProtoSymbolTable = protoSymbolTable;
          protoSymbolTable = new HashMap<>(protoSymbolTable);
          for (String id : RegexSplitVisitor.getTopLevelSplit($regex.ctx)) {
              protoSymbolTable.put(id, IdType.STRING);
          }
      }
      string RSQUARE
      { protoSymbolTable = $outerProtoSymbolTable; }
    ;

printCommand
    : PRINT string PERIOD                                                       # printStdout
    | PRINT LPAREN string COMMA string RPAREN PERIOD                            # printFile
    ;

assertCommand
    : ASSERT LPAREN bool RPAREN PERIOD                                          # assertBool
    ;
bool
    : string DOUBLE_EQUALS string                                               # boolStrEq
    | regex DOUBLE_EQUALS regex                                                 # boolRegexEq
    | regex LESS_EQUALS regex                                                   # boolRegexLessEquals
    | TRIPLE LPAREN regex COMMA rewrite COMMA regex RPAREN                      # boolTriple
    | NOT bool                                                                  # boolNot
    | bool AND bool                                                             # boolAnd
    | bool OR bool                                                              # boolOr
    | LPAREN bool RPAREN                                                        # boolParen
    ;

////////////////////////////////////////////////////////////////////////////////
// Lexer
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// * Top-level objects
////////////////////////////////////////////////////////////////////////////////

STRING: 'string';
PREDICATE: 'predicate';
CHARMAP: 'charfn';
LANGUAGE: 'language';
REWRITE: 'strfn';
PRINT: 'print';
ASSERT: 'assert';

////////////////////////////////////////////////////////////////////////////////
// * Keywords
////////////////////////////////////////////////////////////////////////////////

PLUSWORD: 'plus';

UTF16: 'utf16';
STDIN: 'stdin';
READ: 'read';
SUBSTR: 'substr';

U: 'U';
NOT: 'not';
AND: 'and';
OR: 'or';

DOMAIN: 'domain';

BOTTOM: 'bottom';
TRY: 'try';
ELSE: 'else';
SPLIT: 'split';
LEFTSPLIT: 'left-split';
COMBINE: 'combine';
ITER: 'iter';
LEFTITER: 'left-iter';
CHAIN: 'chain';
LEFTCHAIN: 'left-chain';

TRIPLE: 'triple';

////////////////////////////////////////////////////////////////////////////////
// * Punctuation
////////////////////////////////////////////////////////////////////////////////

LPAREN: '(';
RPAREN: ')';
LBRACE: '{';
RBRACE: '}';
LSQUARE: '[';
RSQUARE: ']';

EQUALS: '=';
PERIOD: '.';
QUESMARK: '?';
COMMA: ',';

SLASH: '/';
STAR: '*';
PLUS: '+';
MINUS: '-';

ELLIPSIS: '..';

COLON: ':';

DOUBLE_EQUALS: '==';
LESS_EQUALS: '<=';

////////////////////////////////////////////////////////////////////////////////
// * Identifiers
////////////////////////////////////////////////////////////////////////////////

ID: Letter LetterOrDigit*;
fragment Letter: [a-zA-Z_];
fragment LetterOrDigit: [0-9]
                      | Letter
                      ;

////////////////////////////////////////////////////////////////////////////////
// * Integers and strings
////////////////////////////////////////////////////////////////////////////////

IntegerLiteral: ('+' | '-')? [0-9]+;

fragment SingleCharacter: ~['\\];
fragment EscapeSequence
    : '\\' [btnfr"'\\]
    | UnicodeEscape
    ;
fragment UnicodeEscape
    : '\\' 'u' HexDigit HexDigit HexDigit HexDigit;
fragment HexDigit: [0-9a-fA-F];

StringLiteral: DOUBLEQUOTE StringCharacters? DOUBLEQUOTE;
fragment StringCharacters: StringCharacter+;
fragment StringCharacter: ~["\\]
                        | EscapeSequence
                        ;

SINGLEQUOTE: '\'';
DOUBLEQUOTE: '\"';

////////////////////////////////////////////////////////////////////////////////
// * Whitespace
////////////////////////////////////////////////////////////////////////////////

WS: [ \t\r\n]+ -> skip;
COMMENT: '/*' .*? '*/' -> skip;
LINE_COMMENT: '//' ~[\r\n]* -> skip;
