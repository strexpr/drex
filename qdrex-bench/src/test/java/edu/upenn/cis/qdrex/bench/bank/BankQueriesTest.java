package edu.upenn.cis.qdrex.bench.bank;

import com.google.common.base.Optional;
import com.google.common.collect.ImmutableList;
import static edu.upenn.cis.qdrex.bench.bank.BankStream.getMonths;
import edu.upenn.cis.qdrex.core.fj.Evaluator;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Approx;
import java.util.Iterator;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class BankQueriesTest {

    @Test
    public void test1() {
        int numMonths = 110320;
        Approx.setEpsilon(0.01);
        System.out.printf("Stream length: %d\n", ImmutableList.copyOf(getMonths(numMonths, 1000)).size());
        // System.out.println(BankQueries.AVG_MONTHLY_DEPOSIT.eval(getMonths(numMonths)));
        // System.out.println(BankQueries.MEDIAN_MONTHLY_DEPOSIT.eval(getMonths(numMonths, 1000)));
        // System.out.println(BankQueries.MEDIAN_CLOSING_BALANCE.eval(getMonths(numMonths)));
    }

    public void graphLogUNSpace(QRE<BankValuePredicate, BankValue, Double> query) {
        int[] N = new int[] { 11032, 110246, 1100848 };
        System.out.printf("LogU, n0=%d, n1=%d, n2=%d\n", N[0], N[1], N[2]);
        for (int logU = 0; logU <= 20; logU++) {
            System.out.printf("%d, ", logU);
            for (int n : N) {
                Iterator<BankValue> iterator = getMonths(n, Math.pow(2, logU));
                Evaluator.setMemLogging(true);
                Evaluator.resetPeakMemUsage();
                Optional<Term<Double>> ans = query.eval(iterator);
                assertTrue(ans.isPresent());
                System.out.printf("%d, ", Evaluator.getPeakMemUsage());
            }
            System.out.printf("\n");
        }
    }

    @Test
    public void graphLogUNSpaceMedianMonthlyDeposit() {
        graphLogUNSpace(BankQueries.MEDIAN_MONTHLY_DEPOSIT);
    }

    @Test
    public void graphLogUNSpaceMedianClosingBalance() {
        graphLogUNSpace(BankQueries.MEDIAN_CLOSING_BALANCE);
    }

}
