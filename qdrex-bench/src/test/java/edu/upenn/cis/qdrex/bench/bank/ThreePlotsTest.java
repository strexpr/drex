package edu.upenn.cis.qdrex.bench.bank;

import com.google.common.base.Optional;
import edu.upenn.cis.qdrex.core.fj.Evaluator;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.util.collect.Iterators;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Approx;
import java.util.Iterator;
import org.junit.Test;

public class ThreePlotsTest {

    public static final double DEPOSIT_HI = 1000;

    public void plot1(QRE<BankValuePredicate, BankValue, Double> query) {
        System.out.printf("streamLen(tuples), space(bytes), referenceOut\n");
        Approx.setEpsilon(0.01);
        for (int numMonths = 10; numMonths <= 110246; numMonths += 1000) {
            Evaluator.setMemLogging(true);
            Evaluator.resetPeakMemUsage();

            Iterator<BankValue> iterator = BankStream.getMonths(numMonths, DEPOSIT_HI);
            Optional<Term<Double>> ans = query.eval(iterator);

            int streamLen = Iterators.length(BankStream.getMonths(numMonths, DEPOSIT_HI));
            System.out.printf("%d, %d, %f\n", streamLen, Evaluator.getPeakMemUsage(), ans.get().eval());
        }
    }

    @Test
    public void plot1Query1() {
        plot1(BankQueries.QUERY_1);
    }

    @Test
    public void plot1Query2() {
        plot1(BankQueries.QUERY_2);
    }

    @Test
    public void plot1Query3() {
        plot1(BankQueries.QUERY_3);
    }

    public void plot2(QRE<BankValuePredicate, BankValue, Double> query) {
        int numMonths = 110246;
        System.out.printf("epsilon, space(bytes), referenceOut\n");
        for (double epsilon = 0.01; epsilon <= 0.1; epsilon += 0.01) {
            Evaluator.setMemLogging(true);
            Evaluator.resetPeakMemUsage();

            Approx.setEpsilon(epsilon);
            Iterator<BankValue> iterator = BankStream.getMonths(numMonths, DEPOSIT_HI);
            Optional<Term<Double>> ans = query.eval(iterator);

            System.out.printf("%f, %d, %f\n", epsilon, Evaluator.getPeakMemUsage(), ans.get().eval());
        }
    }

    @Test
    public void plot2Query1() {
        plot2(BankQueries.QUERY_1);
    }

    @Test
    public void plot2Query2() {
        plot2(BankQueries.QUERY_2);
    }

    @Test
    public void plot2Query3() {
        plot2(BankQueries.QUERY_3);
    }

    public void plot3(QRE<BankValuePredicate, BankValue, Double> query) {
        int numMonths = 110246;
        int streamLen = Iterators.length(BankStream.getMonths(numMonths, DEPOSIT_HI));

        System.out.printf("streamLen: %d\n", streamLen);
        System.out.printf("epsilon, fullTime(ns), secondHalfTime(ns), referenceOut\n");

        Evaluator.setTimeLogging(streamLen);
        for (double epsilon = 0.01; epsilon <= 0.1; epsilon += 0.01) {
            Evaluator.setMemLogging(true);
            Evaluator.resetPeakMemUsage();

            Approx.setEpsilon(epsilon);
            Iterator<BankValue> iterator = BankStream.getMonths(numMonths, DEPOSIT_HI);
            Optional<Term<Double>> ans = query.eval(iterator);

            long fullTime = Evaluator.getFullNanoTime();
            long secondHalfTime = Evaluator.getSecondHalfNanoTime();
            System.out.printf("%f, %d, %d, %f\n", epsilon, fullTime, secondHalfTime, ans.get().eval());
        }
    }

    @Test
    public void plot3Query1() {
        plot3(BankQueries.QUERY_1);
    }

    @Test
    public void plot3Query2() {
        plot3(BankQueries.QUERY_2);
    }

    @Test
    public void plot3Query3() {
        plot3(BankQueries.QUERY_3);
    }

}
