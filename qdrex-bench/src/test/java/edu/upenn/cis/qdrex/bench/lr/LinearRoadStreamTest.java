package edu.upenn.cis.qdrex.bench.lr;

import com.google.common.base.Optional;
import edu.upenn.cis.qdrex.bench.lr.value.CarLoc;
import edu.upenn.cis.qdrex.bench.lr.value.LinearRoadValue;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import java.io.FileNotFoundException;
import java.util.Iterator;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class LinearRoadStreamTest {

    public static final String FILENAME = "/scratch/home1/r/rmukund/prog/stream/linear-road/scsq-lr/data/cardatapoints10.out";
    public static final int TUPLE_COUNT = Integer.MAX_VALUE; // 1000000;

    public int streamLen(int tupleCount) throws FileNotFoundException {
        Iterator<LinearRoadValue> iterator = new LinearRoadStream(FILENAME, tupleCount).iterator();
        int ans = 0;
        while (iterator.hasNext()) {
            LinearRoadValue value = iterator.next();
            ans++;
        }
        return ans;
    }

    @Test
    public void test1() throws FileNotFoundException {
        double expectedLength = streamLen(TUPLE_COUNT);
        LinearRoadStream stream = new LinearRoadStream(FILENAME, TUPLE_COUNT);
        Optional<Term<Double>> optLen = Expressions.STREAM_LEN.eval(stream);
        double actualLength = optLen.get().eval();
        assertEquals(expectedLength, actualLength, 0.1);
    }

    public double medianSpeed(int tupleCount) throws FileNotFoundException {
        Iterator<LinearRoadValue> iterator = new LinearRoadStream(FILENAME, tupleCount).iterator();
        Multiset m = Multiset.EMPTY;
        while (iterator.hasNext()) {
            LinearRoadValue value = iterator.next();
            if (value instanceof CarLoc) {
                m = m.insert(((CarLoc)value).speed);
            }
        }
        return m.rank(0.5);
    }

    @Test
    public void test2() throws FileNotFoundException {
        double expectedMedianSpeed = medianSpeed(TUPLE_COUNT);
        LinearRoadStream stream = new LinearRoadStream(FILENAME, TUPLE_COUNT);
        Optional<Term<Double>> optSpeed = Expressions.STREAM_MDN_SPEED.eval(stream);
        double actualMedianSpeed = optSpeed.get().eval();
        assertEquals(expectedMedianSpeed, actualMedianSpeed, 0.1);
    }

    @Test
    public void test3() throws FileNotFoundException {
        LinearRoadStream stream = new LinearRoadStream(FILENAME, TUPLE_COUNT);
        Optional<Term<Double>> optMedTBF = Expressions.MEDTBF.eval(stream);
        System.out.printf("%s\n", optMedTBF);
    }

    @Test
    public void test4() throws FileNotFoundException {
        LinearRoadStream stream = new LinearRoadStream(FILENAME, TUPLE_COUNT);
        Optional<Term<Double>> mdnLane3Speed = Expressions.medianLaneSpeed(3).eval(stream);
        System.out.printf("%s\n", mdnLane3Speed);
    }

    @Test
    public void test5() throws FileNotFoundException {
        LinearRoadStream stream = new LinearRoadStream(FILENAME, TUPLE_COUNT);
        Optional<Term<Double>> avgMdnLaneSpeed = Expressions.AVG_MDN_LANE_SPEED.eval(stream);
        System.out.printf("%s\n", avgMdnLaneSpeed);
    }

}
