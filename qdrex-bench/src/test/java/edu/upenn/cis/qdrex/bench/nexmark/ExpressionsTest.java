package edu.upenn.cis.qdrex.bench.nexmark;

import static edu.upenn.cis.qdrex.bench.nexmark.Expressions.V0_STAR;
import static edu.upenn.cis.qdrex.bench.nexmark.Expressions.V0_STAR_B;
import static edu.upenn.cis.qdrex.bench.nexmark.Expressions.V0_STAR_B_AVG;
import static edu.upenn.cis.qdrex.bench.nexmark.Expressions.V0_STAR_B_MDN;
import edu.upenn.cis.qdrex.bench.nexmark.value.Bid;
import edu.upenn.cis.qdrex.bench.nexmark.value.NexmarkValue;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ExpressionsTest {

    public final NexmarkValue b01 = new NexmarkValue(new Bid(0, null, 0, 0, null, 1));
    public final NexmarkValue b02 = new NexmarkValue(new Bid(0, null, 0, 0, null, 2));
    public final NexmarkValue b11 = new NexmarkValue(new Bid(1, null, 0, 0, null, 1));
    public final NexmarkValue b12 = new NexmarkValue(new Bid(1, null, 0, 0, null, 2));

    @Test
    public void test1() {
        assertEquals(0.0, V0_STAR.eval(b11, b12, b11).get().eval(), 0.01);
        assertEquals(2.0, V0_STAR_B.eval(b02).get().eval(), 0.01);
        assertEquals(2.0, V0_STAR_B.eval(b11, b02).get().eval(), 0.01);
    }

    @Test
    public void v0StarBAvgTest1() {
        assertEquals(2.0, V0_STAR_B_AVG.eval(b02, b02, b11, b02, b02).get().eval(), 0.01);
        assertEquals(2.0, V0_STAR_B_MDN.eval(b02, b02, b11, b02, b02).get().eval(), 0.02);
    }

}
