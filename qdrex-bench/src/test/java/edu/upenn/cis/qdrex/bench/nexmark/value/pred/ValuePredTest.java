package edu.upenn.cis.qdrex.bench.nexmark.value.pred;

import edu.upenn.cis.qdrex.bench.nexmark.Expressions;
import edu.upenn.cis.qdrex.bench.nexmark.value.Bid;
import edu.upenn.cis.qdrex.bench.nexmark.value.NexmarkValue;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ValuePredTest {

    @Test
    public void testAuctionPred() {
        AuctionPred truePred = new AuctionPred(true);
        assertTrue(truePred.getWitness().isPresent());

        AuctionPred falsePred = new AuctionPred(false);
        assertFalse(falsePred.getWitness().isPresent());

        assertTrue(Predicate.areEquivalent(truePred.not(), falsePred));
        assertTrue(Predicate.areEquivalent(truePred, falsePred.not()));

        assertTrue(Predicate.areEquivalent(truePred, truePred.and(truePred)));
        assertTrue(Predicate.areEquivalent(falsePred, truePred.and(falsePred)));
        assertTrue(Predicate.areEquivalent(falsePred, falsePred.and(truePred)));
        assertTrue(Predicate.areEquivalent(falsePred, falsePred.and(falsePred)));
    }

    @Test
    public void testPersonPred() {
        PersonPred truePred = new PersonPred(true);
        assertTrue(truePred.getWitness().isPresent());

        PersonPred falsePred = new PersonPred(false);
        assertFalse(falsePred.getWitness().isPresent());

        assertTrue(Predicate.areEquivalent(truePred.not(), falsePred));
        assertTrue(Predicate.areEquivalent(truePred, falsePred.not()));

        assertTrue(Predicate.areEquivalent(truePred, truePred.and(truePred)));
        assertTrue(Predicate.areEquivalent(falsePred, truePred.and(falsePred)));
        assertTrue(Predicate.areEquivalent(falsePred, falsePred.and(truePred)));
        assertTrue(Predicate.areEquivalent(falsePred, falsePred.and(falsePred)));
    }

    @Test
    public void testBidPred1() {
        BidPred truePred = new BidPred(true, true);
        assertTrue(truePred.getWitness().isPresent());

        BidPred falsePred = new BidPred(false, false);
        assertFalse(falsePred.getWitness().isPresent());

        assertTrue(Predicate.areEquivalent(truePred.not(), falsePred));
        assertTrue(Predicate.areEquivalent(truePred, falsePred.not()));

        assertTrue(Predicate.areEquivalent(truePred, truePred.and(truePred)));
        assertTrue(Predicate.areEquivalent(falsePred, truePred.and(falsePred)));
        assertTrue(Predicate.areEquivalent(falsePred, falsePred.and(truePred)));
        assertTrue(Predicate.areEquivalent(falsePred, falsePred.and(falsePred)));
    }

    @Test
    public void testBidPred2() {
        BidPred bidPred0 = new BidPred(true, false);
        BidPred bidPredN0 = bidPred0.not();

        Bid bid0 = new Bid(0, null, 0, 0, null, 0);
        assertTrue(bidPred0.test(bid0));
        assertFalse(bidPredN0.test(bid0));
    }

    @Test
    public void testValuePred1() {
        NexmarkValue bid0 = new NexmarkValue(new Bid(0, null, 0, 0, null, 0));
        assertTrue(Expressions.VALUE_PRED_0.test(bid0));
        assertFalse(Expressions.VALUE_PRED_N0.test(bid0));

        NexmarkValue bid1 = new NexmarkValue(new Bid(1, null, 0, 0, null, 0));
        assertFalse(Expressions.VALUE_PRED_0.test(bid1));
        assertTrue(Expressions.VALUE_PRED_N0.test(bid1));
    }

}
