package approx3;

public class ITermParam implements ITerm {
    final private int pId; // parameter id
    
    public ITermParam(int p) {
        if (p < 0)
            throw new IllegalArgumentException("parameter id cannot be negative");
        
        this.pId = p;
    }
    
    public int getSize() {
        return 4;
    }
    
    public double getValue() {
        throw new RuntimeException("parameter has no value");
    }
    
    public boolean isGround() {
        return false;
    }
    
    public ITerm subst(ITerm t, int p) {
        if (this.pId == p)
            return t;
        else
            return new ITermParam(pId);
    }
    
    public ITerm subst(MTerm N, int P) {
        return this;
    }

    public String toString() {
        return "p" + this.pId;
    }
}
