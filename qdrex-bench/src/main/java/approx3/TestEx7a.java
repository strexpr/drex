package approx3;
import java.util.Date;
import java.util.Random;

public class TestEx7a {
    
    public static void main(String[] args) {
        System.out.println("***** SLIDING WINDOW - SUM example *****");
        
        int N = 100 * 1000; // stream size
        int w = 10000; // window
        Random rnd = new Random(new Date().getTime());
        
        double[] stream = new double[N];
        for (int i=0; i<N; i++) {
            stream[i] = 500 * 1000 * rnd.nextDouble();
        }
        
        System.out.println("Using exponential histogram:");
        long startTime1 = System.nanoTime();

        AHistWnd histW = new AHistWnd(w);
        for (int i=0; i<stream.length; i++) {
            if (i % 10000 == 0) System.out.println(i);
            histW = histW.ins(stream[i]);
            //System.out.print(EH.getCount());
            histW = histW.tick();
            //System.out.println(" tick " + EH.getCount());
        }
        double output1 = histW.getSum();
        System.out.println("output = " + output1);
        
        long endTime1 = System.nanoTime();
        long duration1 = (endTime1 - startTime1) / 1000000;
        System.out.println("duration = " + duration1);
        
        
        System.out.println();
        
        System.out.println("Using just running sum/count:");
        long startTime2 = System.nanoTime();
        
        double output2 = Double.NaN;
        System.out.println("output = " + output2);
        
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2) / 1000000;
        System.out.println("duration = " + duration2);
        
        System.out.println();
        System.out.println("ratio: " + ((double) duration1 / (double) duration2));
    }
    
}
