package approx3.lr;

import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.bench.lr.LinearRoadStream;
import edu.upenn.cis.qdrex.bench.lr.value.CarLoc;
import edu.upenn.cis.qdrex.bench.lr.value.LinearRoadValue;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermAvg;
import edu.upenn.cis.qdrex.core.term.arith.ITermRank;
import edu.upenn.cis.qdrex.core.term.arith.MTermSingleton;
import edu.upenn.cis.qdrex.core.term.arith.MTermUnion;
import edu.upenn.cis.qdrex.core.util.collect.Iterators;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Approx;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class LRAlg1 {

    public static final String FILENAME = "/scratch/home1/r/rmukund/prog/stream/linear-road/scsq-lr/data/cardatapoints10.out";
    public static final int TUPLE_COUNT = Integer.MAX_VALUE; // 1000000;

    private final List<Term<Multiset>> lanes;
    public int size;
    public /* mutable */ Term<Double> ans;

    public LRAlg1() {
        lanes = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            lanes.add(ConstantTerm.of(Multiset.EMPTY));
        }
    }

    public void update(LinearRoadValue d) {
        if (d instanceof CarLoc) {
            CarLoc carLoc = (CarLoc)d;
            Term<Multiset> speed = MTermSingleton.of(ConstantTerm.of(carLoc.speed));
            lanes.set(carLoc.lane, MTermUnion.of(speed, lanes.get(carLoc.lane)));
        }
    }

    public double output() {
        size = 0;
        Term<Multiset> allLanes = ConstantTerm.of(Multiset.EMPTY);
        for (Term<Multiset> lane : lanes) {
            Term<Double> mdn = ITermRank.of(lane, 0.5);
            allLanes = MTermUnion.of(MTermSingleton.of(mdn), allLanes);
            size = size + lane.getBytes();
        }
        return ITermAvg.of(allLanes).eval();
    }

    private static void varyEps() throws FileNotFoundException {
        int N = 10 * 1000 * 1000;
        for (double e=0.1; e>=0.01; e-=0.01) {
            Approx.setEpsilon(e);
            
            LinearRoadStream lrStream = new LinearRoadStream(FILENAME, TUPLE_COUNT);
            Iterator<LinearRoadValue> it = lrStream.iterator();
            LRAlg1 alg = new LRAlg1();
            for (int i=0; i<N; i++) {
                alg.update(it.next());
            }
            //System.out.println("eps = " + Approx.eps);
            //System.out.println("stream length = " + N);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            System.out.println((1/e) + " " + alg.output() + " " + alg.size);
        }
    }

    private static void measureTime() throws FileNotFoundException {
        int N = 10 * 1000 * 1000;
        LinearRoadStream lrStream = new LinearRoadStream(FILENAME, TUPLE_COUNT);
        ImmutableList<LinearRoadValue> lrList = ImmutableList.copyOf(Iterators.keep(lrStream, N));

        System.out.printf("1/epsilon, Length, RefOut1, Time\n");
        for (double e = 0.01; e <= 0.1; e += 0.01) {
            Approx.setEpsilon(e);
            Iterator<LinearRoadValue> it = lrList.iterator();
            LRAlg1 alg = new LRAlg1();
            long startTime = System.nanoTime();
            int count = 0;
            while (it.hasNext()) {
                alg.update(it.next());
                count++;
            }
            long endTime = System.nanoTime();
            System.out.printf("%f, %d, %f, %d\n", 1 / e, count, alg.output(), endTime - startTime);
        }
    }

    public static void main(String[] args) throws FileNotFoundException {
        System.out.println("***** LinearRoad *****");
        
        // varyEps();
        measureTime();
    }

}
