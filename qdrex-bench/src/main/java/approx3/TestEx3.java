package approx3;
import java.util.Random;
import java.util.Date;

public class TestEx3 {
    
    public static void main(String[] args) {
        System.out.println("***** MAX-MIN-PLUS example (with parameters) *****");
        
        int N = 400;
        Random rnd = new Random(new Date().getTime());
        
        double[][][] stream = new double[N][][];
        for (int i=0; i<N; i++) {
            stream[i] = new double[N][];
            for (int j=0; j<N; j++) {
                stream[i][j] = new double[N];
                for (int k=0; k<N; k++)
                    stream[i][j][k] = Math.abs(10.0 * rnd.nextDouble());                
            }
        }
        
        System.out.println("Using terms:");
        long startTime1 = System.nanoTime();

        final int p = 0;
        final int q = 1;
        final int r = 2;
        final ITerm pTerm = new ITermParam(p);
        final ITerm qTerm = new ITermParam(q);
        final ITerm rTerm = new ITermParam(r);
        ITerm t = ITermMax.create(ITermMin.create(pTerm,qTerm),rTerm);
        ITerm x = t;
        //System.out.println(x);
        for (int i=0; i<stream.length; i++) {
            for (int j=0; j<stream[i].length; j++) {
                for (int k=0; k<stream[i][j].length; k++) {
                    ITerm c = new ITermConst(stream[i][j][k]);
                    x = x.subst(ITermSum.create(c,pTerm),p);
                    //System.out.println(x);
                }
                x = x.subst(new ITermConst(0),p);
                x = x.subst(ITermMin.create(pTerm,qTerm),q);
                //System.out.println(x);                
            }
            x = x.subst(new ITermConst(Double.POSITIVE_INFINITY),p);
            x = x.subst(new ITermConst(Double.POSITIVE_INFINITY),q);
            x = x.subst(t,r);
            //System.out.println(x);
        }
        x = x.subst(new ITermConst(Double.NEGATIVE_INFINITY),p);
        x = x.subst(new ITermConst(Double.NEGATIVE_INFINITY),q);
        x = x.subst(new ITermConst(Double.NEGATIVE_INFINITY),r);
        double output1 = x.getValue();
        System.out.println("output = " + output1);
        
        long endTime1 = System.nanoTime();
        long duration1 = (endTime1 - startTime1) / 1000000;
        System.out.println("duration = " + duration1);
        
        
        System.out.println();
        
        System.out.println("Using values:");
        long startTime2 = System.nanoTime();
        
        double max = Double.NEGATIVE_INFINITY;
        for (int i=0; i<stream.length; i++) {
            double min = Double.POSITIVE_INFINITY;
            for (int j=0; j<stream[i].length; j++) {
                double sum = 0.0;
                for (int k=0; k<stream[i][j].length; k++) sum += stream[i][j][k];
                min = Math.min(min,sum);
            }
            max = Math.max(max,min);
        }
        double output2 = max;
        System.out.println("output = " + output2);
        
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2) / 1000000;
        System.out.println("duration = " + duration2);
        
        System.out.println();
        System.out.println("ratio: " + ((double) duration1 / (double) duration2));
    }
    
}