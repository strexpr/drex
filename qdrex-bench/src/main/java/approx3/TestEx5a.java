package approx3;
import java.util.Date;
import java.util.Random;

public class TestEx5a {
    
    public static void main(String[] args) {
        System.out.println("***** AVERAGE example *****");
        
        int N = 100 * 1000 * 1000;
        Random rnd = new Random(new Date().getTime());
        
        double[] stream = new double[N];
        for (int i=0; i<N; i++) {
            stream[i] = Math.abs(1000 * rnd.nextDouble());
        }
        
        System.out.println("Using terms:");
        long startTime1 = System.nanoTime();

        MSet mu = new MSet();
        System.out.println(mu);
        for (int i=0; i<stream.length; i++) {
            //System.out.println("\t" + Double.toString(stream[i]));
            mu = mu.ins(stream[i]);
            //System.out.println(mu);
        }
        double output1 = mu.avg();
        System.out.println("output = " + output1);
        
        long endTime1 = System.nanoTime();
        long duration1 = (endTime1 - startTime1) / 1000000;
        System.out.println("duration = " + duration1);
        
        
        System.out.println();
        
        System.out.println("Using just running sum/count:");
        long startTime2 = System.nanoTime();
        
        double rSum = 0;
        double rCount = 0;
        for (int i=0; i<stream.length; i++) {
            rSum += stream[i];
            rCount++;
        }
        double output2 = rSum / rCount;
        System.out.println("output = " + output2);
        
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2) / 1000000;
        System.out.println("duration = " + duration2);
        
        System.out.println();
        System.out.println("ratio: " + ((double) duration1 / (double) duration2));
    }
    
}
