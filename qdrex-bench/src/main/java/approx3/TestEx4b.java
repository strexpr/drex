package approx3;
import java.util.Date;
import java.util.Random;

public class TestEx4b {
    
    public static void main(String[] args) {
        System.out.println("***** MEDIAN example (with params) *****");
        
        int N = 10 * 1000;
        int U = 1000;
        Random rnd = new Random(new Date().getTime());
        
        double[] stream = new double[N];
        for (int i=0; i<N; i++) {
            stream[i] = 100 + U * rnd.nextDouble();
        }
        
        System.out.println("Using terms:");
        long startTime1 = System.nanoTime();

        int P = 0;
        MTermParam termP = new MTermParam(P);
        MTerm x = termP;
        System.out.println(x);
        for (int i=0; i<stream.length; i++) {
            //System.out.println("\t" + Double.toString(stream[i]));
            x = x.subst(MTermIns.create(new ITermConst(stream[i]),termP),P);
            //System.out.println(x);
        }
        x = x.subst(new MTermConst(),P);
        //System.out.println(x);
        double output1 = x.getValue().rnk(0.5);
        System.out.println("output = " + output1);
        
        long endTime1 = System.nanoTime();
        long duration1 = (endTime1 - startTime1) / 1000000;
        System.out.println("duration = " + duration1);
        
        
        System.out.println();
        
        System.out.println("Using arrays:");
        long startTime2 = System.nanoTime();
        
        AHist h = new AHist();
        for (int i=0; i<stream.length; i++) {
            h.add(stream[i],1);
        }
        double output2 = h.getRank(0.5);
        System.out.println("output = " + output2);
        
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2) / 1000000;
        System.out.println("duration = " + duration2);
        
        System.out.println();
        System.out.println("ratio: " + ((double) duration1 / (double) duration2));
    }
    
}