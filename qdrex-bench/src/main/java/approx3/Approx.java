package approx3;

public class Approx {
    public static double eps = 0.01;
    public static double lnOneEps = Math.log(1.0+eps); // ln(1+eps)

    // Only deals with val > 0.
    public static int valToIndex(double val) {
        if (val <= 0.0)
            throw new IllegalArgumentException("val <= 0.0");
        
        if (val >= 1.0) {
            // log_{1+eps}(val) = ln(val) / ln(1+eps)
            double indexDbl = Math.log(val) / lnOneEps;
            int index = (int) Math.floor(indexDbl);
            return index;
        }
        
        // log_{1+eps}(1/val) = -log_{1+eps}(val) = -ln(val) / ln(1+eps)
        double indexDbl = -Math.log(val) / lnOneEps;
        int index = -((int) Math.floor(indexDbl))-1;
        return index;
    }
    
    public static double indexToVal(int index) {
        return Math.exp(lnOneEps * index);
    }
}