package approx3;
import java.util.*;

// epsilon-approximate histogram
public class AHist {
    //public static final double eps = 0.001;
    //public static final double lnOneEps = Math.log(1.0+eps); // ln(1+eps)
    
    private int totalCount; // count of recorded values
    // hist1(i) = count for numbers in interval [L,R)
    // where L = (1+eps)^i
    // and   R = (1+eps)^(i+1)
    // for i=0,1,...
    private List<Integer> hist1; // histogram for values >= 1
    // hist2(i) = count for numbers in interval [L,R)
    // where L = (1+eps)^(-i-1)
    // and   R = (1+eps)^(-i)
    // for i=0,1,...
    private List<Integer> hist2; // histogram for values in (0,1)
    private int zeroCount; // count for 0
    
    public AHist() {
        hist1 = new ArrayList<Integer>();
        hist2 = new ArrayList<Integer>();
        zeroCount = 0;
        totalCount = 0;
    }
    
    public int getSize() {
        return hist1.size() + hist2.size();
    }    
    
    public boolean isEmpty() {
        return (totalCount == 0);
    }    
    
    // Deep copy.
    public AHist cloneDeep() {
        AHist newHist = new AHist();
        newHist.merge(this);
        return newHist;
    }
    
    public void add(double val, int n) {
        if (n < 1)
            throw new IllegalArgumentException("n < 1");
        if (val < 0)
            throw new IllegalArgumentException("val < 0");
        
        if (val == 0) {
            zeroCount += n;
            return;
        }
        
        int index = Approx.valToIndex(val);
        if (index >= 0) {
            addH1(index,n);
        } else {
            addH2(-index,n);
        }
    }

    // add to hist1 at position index
    private void addH1(int index, int n) {
        for (int i=hist1.size(); i<=index; i++) {
            // extend hist1 up to index with 0s.
            hist1.add(0);
        }
        int count = hist1.get(index)+n;
        hist1.set(index,count);
        totalCount += n;
    }
    
    // add to hist2 at position index
    private void addH2(int index, int n) {
        for (int i=hist2.size(); i<=index; i++) {
            // extend hist2 up to index with 0s.
            hist2.add(0);
        }
        int count = hist2.get(index)+n;
        hist2.set(index,count);
        totalCount += n;
    }    
    
    public void merge(AHist mu) {
        for (int i=0; i<mu.hist1.size(); i++) {
            int count = mu.hist1.get(i);
            if (count > 0) addH1(i,count);
        }
        for (int i=0; i<mu.hist2.size(); i++) {
            int count = mu.hist2.get(i);
            if (count > 0) addH2(i,count);
        }
        this.zeroCount += mu.zeroCount;
    }
    
    public double getRank(double o) {
        if (o < 0 || o > 1)
            throw new IllegalArgumentException("o out of interval [0,1]");
        if (totalCount == 0)
            throw new RuntimeException("empty multiset");
        
        int index = (int) Math.floor(((double) totalCount-1) * o);
        int curr = zeroCount;
        if (index < curr) return 0.0;
        
        for (int i=hist2.size()-1; i>=0; i--) {
            curr += hist2.get(i);
            if (index < curr) return Approx.indexToVal(-i);
        }
        
        for (int i=0; i<hist1.size(); i++) {
            curr += hist1.get(i);
            if (index < curr) return Approx.indexToVal(i);
        }
        
        assert false;
        return -1.0;
    }    
    
    public String toString() {
        ArrayList<String> l = new ArrayList<String>();
        
        if (zeroCount != 0) l.add(0.0 + ":" + zeroCount);
        for (int i=hist2.size()-1; i>=0; i--) {
            double val = Approx.indexToVal(-i);
            int count = hist2.get(i);
            if (count > 0) l.add(val + ":" + count);
        }
        for (int i=0; i<hist1.size(); i++) {
            double val = Approx.indexToVal(i);
            int count = hist1.get(i);
            if (count > 0) l.add(val + ":" + count);
        }
        
        if (l.size() == 0) return "<0>";
        String str = "<" + l.size() + " non-zero buckets> " + l.get(0);
        for (int i=1; i<l.size(); i++) str += ", " + l.get(i);      
        return str;
    }
}
