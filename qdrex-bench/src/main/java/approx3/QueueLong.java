package approx3;

// Immutable FIFO data structure.
public class QueueLong {
    private final int size;
    private final Node list1; // list for adding
    private final Node list2; // list for removing
    // Invariant: list1 is null only if size=0.
    // This is necessary so that get() does not need to return a modified queue.
    
    public QueueLong() {
        size = 0;
        list1 = null;
        list2 = null;
    }
    
    private QueueLong(int s, Node l1, Node l2) {
        size = s;
        list1 = l1;
        list2 = l2;
    }
    
    public int size() {
        return size;
    }
    
    // Get the first element.
    public long get() {
        if (size == 0) throw new RuntimeException("get(): queue empty");
        
        // Invariant: list1 != null
        return list1.getData();
    }
    
    // Remove the first element.
    public QueueLong remove() {
        if (size == 0) throw new RuntimeException("get(): queue empty");
        
        int newSize = size - 1;
        Node l1New = list1.getNext();
        Node l2New = list2;
        if ((l1New == null) && (newSize > 0)) {
            assert (list2 != null);
            l1New = list2.reverse();
            l2New = null;
        }
        return new QueueLong(newSize,l1New,l2New);
    }
    
    public QueueLong add(long d) {
        if (list1 == null) return new QueueLong(size+1,new Node(d),list2);
        return new QueueLong(size+1,list1,new Node(d,list2));
    }
    
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("(size = " + size);
        builder.append(", remove list: " + ((list1==null) ? "---" : list1.toStringRec()));
        builder.append(", add list: " + ((list2==null) ? "---" : list2.toStringRec()) + ")");
        return builder.toString();
    }
    
    public static void main(String[] args) {
        System.out.println("***** QueueLong *****");
        
        QueueLong q = new QueueLong();
        System.out.println(q);
        q = q.add(3);
        System.out.println(q);
        q = q.add(5);
        System.out.println(q);
        q = q.add(0);
        System.out.println(q);
        q = q.add(6);
        System.out.println(q);

        System.out.println(q.get());
        q = q.remove();
        System.out.println(q);
        q = q.add(10);
        System.out.println(q);
        q = q.add(12);
        System.out.println(q);
        q = q.remove();
        System.out.println(q);
        q = q.remove();
        System.out.println(q);
        q = q.add(4);
        System.out.println(q);
        q = q.remove();
        System.out.println(q);
    }
    
    private static class Node {
        private final long data;
        private final Node next;
        
        public Node(long d) {
            data = d;
            next = null;
        }
        
        private Node(long d, Node n) {
            data = d;
            next = n;
        }
        
        public long getData() {
            return data;
        }
        
        public Node getNext() {
            return next;
        }
        
        public Node reverse() {
            Node result = null;
            
            Node current = this;
            while (current != null) {
                result = new Node(current.data,result);
                current = current.next;
            }
            
            return result;
        }
        
        public String toString() {
            return Long.toString(data);
        }
        
        public String toStringRec() {
            StringBuilder builder = new StringBuilder();
            builder.append(Long.toString(this.data));
            Node current = this.next;
            while (current != null) {
                builder.append(" " + Long.toString(current.data));
                current = current.next;
            }
            return builder.toString();
        }
    } // end of Node
    
} // end of QueueImmut
