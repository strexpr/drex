package approx3;
import java.util.Random;
import java.util.Date;

public class TestEx2 {
    
    public static void main(String[] args) {
        System.out.println("***** MIN-PLUS example (with parameters) *****");
        
        int N = 10 * 1000;
        Random rnd = new Random(new Date().getTime());
        
        double[][] stream = new double[N][];
        for (int i=0; i<N; i++) {
            stream[i] = new double[N];
            for (int j=0; j<N; j++)
                stream[i][j] = Math.abs(10.0 * rnd.nextDouble());
        }
        
        System.out.println("Using terms:");
        long startTime1 = System.nanoTime();

        final int p = 0;
        final int q = 1;
        final ITerm pTerm = new ITermParam(p);
        final ITerm qTerm = new ITermParam(q);
        ITerm x = ITermMin.create(pTerm,qTerm);
        System.out.println(x);
        for (int i=0; i<stream.length; i++) {
            for (int j=0; j<stream[i].length; j++) {
                ITerm c = new ITermConst(stream[i][j]);
                x = x.subst(ITermSum.create(c,pTerm),p);
                //System.out.println(x);
            }
            x = x.subst(new ITermConst(0),p);
            x = x.subst(ITermMin.create(pTerm,qTerm),q);
            //System.out.println(x);
        }
        x = x.subst(new ITermConst(Double.POSITIVE_INFINITY),p);
        x = x.subst(new ITermConst(Double.POSITIVE_INFINITY),q);
        System.out.println("output = " + x.getValue());
        
        long endTime1 = System.nanoTime();
        long duration1 = (endTime1 - startTime1) / 1000000;
        System.out.println("duration = " + duration1);
        
        
        System.out.println();
        
        System.out.println("Using values:");
        long startTime2 = System.nanoTime();
        
        double min = Double.POSITIVE_INFINITY;
        for (int i=0; i<stream.length; i++) {
            double sum = 0.0;
            for (int j=0; j<stream[i].length; j++) sum += stream[i][j];
            min = Math.min(min,sum);
        }
        System.out.println("output = " + min);
        
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2) / 1000000;
        System.out.println("duration = " + duration2);
        
        System.out.println();
        System.out.println("ratio: " + ((double) duration1 / (double) duration2));
    }
    
}