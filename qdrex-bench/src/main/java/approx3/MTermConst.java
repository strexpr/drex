package approx3;

public class MTermConst implements MTerm {
    private final MSet mu;
    
    public MTermConst() {
        mu = new MSet();
    }
    
    protected MTermConst(MSet mu) {
        this.mu = mu;
    }
    
    public int getSize() {
        return 4 + mu.getSize();
    }

    public boolean isGround() {
        return true;
    }
    
    public MSet getValue() {
        return mu;
    }
    
    public MTerm subst(ITerm t, int p) {
        return this;
    }
    
    public MTerm subst(MTerm N, int P) {
        return this;
    }
    
    public String toString() {
        return "MSET-CONST " + mu.toString();
    }
}
