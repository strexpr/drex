package approx3;

public class MTermParam implements MTerm {
    private final int PId; // parameter id
    
    public MTermParam(int P) {
        this.PId = P;
    }
    
    public int getSize() {
        return 4;
    }
    
    public MSet getValue() {
        throw new RuntimeException("parameter has no value");
    }    
        
    public boolean isGround() {
        return false;
    }
    
    public MTerm subst(ITerm t, int p) {
        return this;
    }
    
    public MTerm subst(MTerm N, int P) {
        if (this.PId == P)
            return N;
        else
            return this;
    }
    
    public String toString() {
        return "P" + this.PId;
    }
}
