package approx3;

public class BSTNodeInt {
    protected static final int nOfFields = 7; // number of fields
    private final int value; // value (e.g., bucket index)
    private final int count; // count for node
    private final int size; // size of tree
    private final int height; // height of tree
    private final int totalCount; // count for entire tree
    private final BSTNodeInt left;
    private final BSTNodeInt right;
    
    public BSTNodeInt(int v) {
        value = v;
        count = 1;
        height = 1;
        size = 1;
        totalCount = 1;
        left = null;
        right = null;
    }
    
    private BSTNodeInt(int v, int c) {
        value = v;
        count = c;
        height = 1;
        size = 1;
        totalCount = c;
        left = null;
        right = null;
    }    
    
    private BSTNodeInt(int v, int c, int s, int h, int t, BSTNodeInt l, BSTNodeInt r) {
        value = v;
        count = c;
        size = s;
        height = h;
        totalCount = t;
        left = l;
        right = r;
    }
    
    public void checkConsistency() {
        if (left != null) left.checkConsistency();
        if (right != null) right.checkConsistency();
        // check consistency here
        int sizeLeft = (left==null) ? 0 : left.size;
        int sizeRight = (right==null) ? 0 : right.size;
        int s = sizeLeft + sizeRight + 1;
        if (s != size) {
            System.err.println(this.toString());
            System.err.println("calculated size = " + s);
        }
        int heightLeft = (left==null) ? 0 : left.height;
        int heightRight = (right==null) ? 0 : right.height;
        int h = Math.max(heightLeft,heightRight) + 1;
        if (h != height) {
            System.err.println(this.toString());
            System.err.println("calculated height = " + h);
        }
        int tcountLeft = (left==null) ? 0 : left.totalCount;
        int tcountRight = (right==null) ? 0 : right.totalCount;
        int tcount = tcountLeft + tcountRight + count;
        if (tcount != totalCount) {
            System.err.println(this.toString());
            System.err.println(left.toString());
            System.err.println(right.toString());
            System.err.println("calculated total count = " + tcount);
        }
    }
    
    public int getTotalCount() {
        return totalCount;
    }
    
    protected int getHeight() {
        return height;
        //int hLeft = (left == null) ? 0 : left.getHeight();
        //int hRight = (right == null) ? 0 : right.getHeight();
        //return Math.max(hLeft,hRight)+1;
    }

    protected int getSize() {
        return size;
        //int sLeft = (left == null) ? 0 : left.getSize();
        //int sRight = (right == null) ? 0 : right.getSize();
        //return sLeft + sRight + 1;
    }
    
    public int getRank(double o) {
        if (o < 0 || o > 1)
            throw new IllegalArgumentException("o out of interval [0,1]");
        
        int index = (int) Math.floor(((double) totalCount-1) * o);
        return this.getElement(index);
    }
    
    public int getElement(int index) {
        if (index < 0 || index >= totalCount)
            throw new IllegalArgumentException("index out of range");
        
        int leftCount = (left==null) ? 0 : left.totalCount;
        if (index < leftCount) return left.getElement(index);
        // index >= leftCount
        if (index < leftCount+count) return value;

        assert (index > leftCount);
        return right.getElement(index-leftCount-count);
    }
    
    //private int getCount() {
    //    int sum = count;
    //    if (left != null) sum += left.getCount();
    //    if (right != null) sum += right.getCount();
    //    return sum;
    //}
    
    public BSTNodeInt merge(BSTNodeInt t) {
        if (t == null) return this;

        BSTNodeInt root1 = this.insert(t.value,t.count);
        root1 = root1.merge(t.left);
        root1 = root1.merge(t.right);
        return root1;
    }
    
    public BSTNodeInt insert(int v) {
        return insert(v,1);
    }
    
    // Insert additional n copies of value v.
    private BSTNodeInt insert(int v, int n) {
        BSTNodeInt newNode = null;
        
        if (value == v) {
            newNode = new BSTNodeInt(v,count+n,size,height,totalCount+n,left,right);
        } else if (v < value) {
            if (left == null) {
                BSTNodeInt newLeft = new BSTNodeInt(v,n);
                int rightHeight = (right == null) ? 0 : right.height;
                assert (newLeft.height == 1);
                int newHeight = Math.max(newLeft.height,rightHeight) + 1;
                newNode = new BSTNodeInt(value,count,size+1,newHeight,totalCount+n,newLeft,right);
            } else { // left != null
                BSTNodeInt newLeft = left.insert(v,n);
                assert (newLeft != left);
                int rightHeight = (right == null) ? 0 : right.height;
                int newHeight = Math.max(newLeft.height,rightHeight)+1;
                int newSize = newLeft.size + ((right == null) ? 0 : right.size) + 1;
                newNode = new BSTNodeInt(value,count,newSize,newHeight,totalCount+n,newLeft,right);
            }
        } else { // v > this.v
            if (right == null) {
                BSTNodeInt newRight = new BSTNodeInt(v,n);
                int leftHeight = (left == null) ? 0 : left.height;
                int newHeight = Math.max(leftHeight,newRight.height)+1;
                newNode = new BSTNodeInt(value,count,size+1,newHeight,totalCount+n,left,newRight);
            } else {
                BSTNodeInt newRight = right.insert(v,n);
                assert (newRight != right);
                int leftHeight = (left == null) ? 0 : left.height;
                int newHeight = Math.max(leftHeight,newRight.height)+1;
                int newSize = ((left == null) ? 0 : left.size) + newRight.size + 1;
                newNode = new BSTNodeInt(value,count,newSize,newHeight,totalCount+n,left,newRight);
            }
        }
        
        //return newNode; // no rebalancing
        //System.out.println("rebalance:");
        //System.out.println(newNode.toStringRec());
        
        
        // Balancing: start with lowest node that falsifies the property of being balanced.
        int lHeight = (newNode.left == null) ? 0 : newNode.left.height;
        int rHeight = (newNode.right == null) ? 0 : newNode.right.height;
        
        // Left subtree overweight
        if (lHeight > rHeight+1) {
            BSTNodeInt x = newNode; // x != null
            BSTNodeInt y = x.left; // y != null
            BSTNodeInt z = x.right;
            int llHeight = (y.left == null) ? 0 : y.left.height;
            int lrHeight = (y.right == null) ? 0 : y.right.height;
            BSTNodeInt y1 = y.left;
            BSTNodeInt y2 = y.right;
            
            // First case
            if (llHeight > lrHeight) {
                int h = lrHeight;
                // x, y, y1 != null
                //              x<h+3>                    y<h+2>
                //        y<h+2>      z<h>   ===>  y1<h+1>      x<h+1>
                // y1<h+1>      y2<h>                      y2<h>      z<h>
                int xNewSize = ((y2==null) ? 0 : y2.size) + ((z==null) ? 0 : z.size) + 1;
                int xNewTotalCount = ((y2==null) ? 0 : y2.totalCount) + ((z==null) ? 0 : z.totalCount) + x.count;
                BSTNodeInt xNew = new BSTNodeInt(x.value,x.count,xNewSize,h+1,xNewTotalCount,y2,z);
                //System.out.println("xNew:");
                //System.out.println(xNew.toStringRec());
                BSTNodeInt yNew = new BSTNodeInt(y.value,y.count,y1.size+xNew.size+1,h+2,y1.totalCount+xNew.totalCount+y.count,y1,xNew);
                return yNew;
            }
            
            // Second case
            if (lrHeight > llHeight) {
                // x, y, y2 != null
                int h = (z == null) ? 0 : z.height;
                BSTNodeInt y21 = y2.left;
                BSTNodeInt y22 = y2.right;
                //                     x<h+3>                         y2
                //       y<h+2>                   z<h>     ===>   y        x
                // y1<h>        y2<h+1>                         y1 y21  y22 z
                //        y21<h/h-1> y22<h/h-1>
                int yNewSize = ((y1==null) ? 0 : y1.size) + ((y21==null) ? 0 : y21.size) + 1;
                int yNewTotalCount = ((y1==null) ? 0 : y1.totalCount) + ((y21==null) ? 0 : y21.totalCount) + y.count;
                BSTNodeInt yNew = new BSTNodeInt(y.value,y.count,yNewSize,h+1,yNewTotalCount,y1,y21);
                //System.out.println("yNew:");
                //System.out.println(yNew.toStringRec());
                int xNewSize = ((y22==null) ? 0 : y22.size) + ((z==null) ? 0 : z.size) + 1;
                int xNewTotalCount = ((y22==null) ? 0 : y22.totalCount) + ((z==null) ? 0 : z.totalCount) + x.count;
                BSTNodeInt xNew = new BSTNodeInt(x.value,x.count,xNewSize,h+1,xNewTotalCount,y22,z);
                //System.out.println("xNew:");
                //System.out.println(xNew.toStringRec());
                BSTNodeInt y2New = new BSTNodeInt(y2.value,y2.count,yNew.size+xNew.size+1,h+2,yNew.totalCount+xNew.totalCount+y2.count,yNew,xNew);
                return y2New;
            }
        }
        
        // Right subtree overweight
        if (rHeight > lHeight+1) {
            BSTNodeInt x = newNode; // x != null
            BSTNodeInt y = x.left;
            BSTNodeInt z = x.right; // z != null
            int rlHeight = (z.left == null) ? 0 : z.left.height;
            int rrHeight = (z.right == null) ? 0 : z.right.height;
            BSTNodeInt z1 = z.left;
            BSTNodeInt z2 = z.right; 
            
            // First case
            if (rrHeight > rlHeight) {
                int h = rlHeight;
                // x, z, z2 != null
                //     x<h+3>                            z<h+2>      
                // y<h>      z<h+2>        ===>    x<h+1>      z2<h+1>   
                //       z1<h>    z2<h+1>       y<h>    z1<h>
                int xNewSize = ((y==null) ? 0 : y.size) + ((z1==null) ? 0 : z1.size) + 1;
                int xNewTotalCount = ((y==null) ? 0 : y.totalCount) + ((z1==null) ? 0 : z1.totalCount) + x.count;
                BSTNodeInt xNew = new BSTNodeInt(x.value,x.count,xNewSize,h+1,xNewTotalCount,y,z1);
                //System.out.println("xNew:");
                //System.out.println(xNew.toStringRec());
                BSTNodeInt zNew = new BSTNodeInt(z.value,z.count,xNew.size+z2.size+1,h+2,xNew.totalCount+z2.totalCount+z.count,xNew,z2);
                return zNew;
            }
            
            // Second case
            if (rlHeight > rrHeight) {
                // x, z, z1 != null
                int h = (y == null) ? 0 : y.height;
                BSTNodeInt z11 = z1.left;
                BSTNodeInt z12 = z1.right;
                //        x<h+3>                               z1
                // y<h>                z<h+2>        ===>  x        z
                //             z1<h+1>        z2<h>       y z11  z12 z2
                //      z11<h/h-1> z12<h/h-1>
                int xNewSize = ((y==null) ? 0 : y.size) + ((z11==null) ? 0 : z11.size) + 1;
                int xNewTotalCount = ((y==null) ? 0 : y.totalCount) + ((z11==null) ? 0 : z11.totalCount) + x.count;
                BSTNodeInt xNew = new BSTNodeInt(x.value,x.count,xNewSize,h+1,xNewTotalCount,y,z11);
                //System.out.println("xNew:");
                //System.out.println(xNew.toStringRec());
                int zNewSize = ((z12==null) ? 0 : z12.size) + ((z2==null) ? 0 : z2.size) + 1;
                int zNewTotalCount = ((z12==null) ? 0 : z12.totalCount) + ((z2==null) ? 0 : z2.totalCount) + z.count;
                BSTNodeInt zNew = new BSTNodeInt(z.value,z.count,zNewSize,h+1,zNewTotalCount,z12,z2);
                //System.out.println("zNew:");
                //System.out.println(zNew.toStringRec());
                BSTNodeInt z1New = new BSTNodeInt(z1.value,z1.count,xNew.size+zNew.size+1,h+2,xNew.totalCount+zNew.totalCount+z1.count,xNew,zNew);
                return z1New;
            }            
        }
        
        return newNode;
        
    }
    
    public String toString() {
        return "value = " + value + ", count = " + count + ", size = " + size + ", height = " + height + ", total count = " + totalCount;
    }
    
    public String toStringRec() {
        return toStringRecAux(0);
    }
    
    private String toStringRecAux(int level) {
        String indent = "    ";
        String totalIndent = "";
        for (int i=0; i<level; i++) totalIndent += indent;
        
        String str = totalIndent + this.toString() + "\n";
        
        if (left != null) str += left.toStringRecAux(level+1);
        if ((left == null) && (right != null)) str += totalIndent+indent+"-----\n";
        if (right != null) str += right.toStringRecAux(level+1);
        
        return str;
    }

}
