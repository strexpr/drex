package approx3;

// Interface for real-valued terms.
public interface ITerm {

	public double getValue() throws NotGround;
	public boolean isGround();
	public ITerm subst(ITerm t, int p);
	public ITerm subst(MTerm N, int P);
	
	// space usage in bytes
	public int getSize();
    
    // op(a|s1,...,op(b|t1,...,tn),...,sm) = op(op(a,b)|s1,...,t1,...,tn,...,sm)
    // op(a|c1,...,ck,t1,...,tn) = op(op(a,c1,...,ck)|t1,...,tn)
    // op(c1,...,ck) -> constant
    //
    // U(mu|s1,...,U(nu|t1,...,tn),...,sm) = U(U(mu,nu)|s1,...,t1,...,tn,...,sm)
    // rank_o( U(mu|) ) = rank_o(mu)
    //
    // U(s1<c1>|s1,...,U(s2<c2>|t1,...,tn),...,sm) =
    // U(s1+s2<c1+c2>|s1,...,t1,...,tn,...,sm)
    // avg( U(s<c>| ) ) = s/c
    // avg( U(s<c>|{t1},...,{tn}) ) = (1/(c+n))*sum(s|t1,...,tn)
    //public ITerm reduce();
    
    // ORDER: max > min > sum > mult
    // 1) "min over max"
    // min(m,max(m',t1,...,tn)) = max(min(m,m'),min(m,t1),...,min(m,t_n))
    // 2) "sum over max, min"
    // sum(a,max(m,t1,...,tn)) = max(a+m,a+t1,...,a+tn)
    // sum(a,min(m,t1,...,tn)) = min(a+m,a+t1,...,a+tn)
    // 3) "mult over max, min, sum"
    // mult(c,max(m,t1,...,tn)) = max(c*m,c*t1,...,c*tn)
    // mult(c,min(m,t1,...,tn)) = min(c*m,c*t1,...,c*tn)
    // mult(c,sum(a,t1,...,tn) = sum(c*a,c*t1,...,c*tn)
    //
    // PRECONDITION: input is reduced.
    // POSTCONDITION: output is reduced & reordered.
    //public ITerm reorder();

}
