package approx3;

public class ITermConst implements ITerm {
    final private double c;
    
    public ITermConst(double c) {
        this.c = c;
    }
    
    public int getSize() {
        return 8;
    }
    
    public double getValue() {
        return this.c;
    }
    
    public boolean isGround() {
        return true;
    }
    
    public ITerm subst(ITerm t, int p) {
        return this;
    }
    
    public ITerm subst(MTerm N, int P) {
        return this;
    }
    
    public String toString() {
        return Double.toString(this.c);
    }
}
