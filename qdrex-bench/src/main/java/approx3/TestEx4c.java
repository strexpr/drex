package approx3;
import java.util.Date;
import java.util.Random;

public class TestEx4c {
    
    public static void main(String[] args) {
        System.out.println("***** MEDIAN example - with terms/no params *****");
        
        int N = 100 * 1000 * 1000;
        System.out.println("N = " + N);
        Random rnd = new Random(new Date().getTime());
        double[] stream = new double[N];
        long U = 1;
        for (int logU=0; logU<=32; logU++) {
            // initialize stream
            for (int i=0; i<N; i++) {
                stream[i] = 1 + U*Math.abs(rnd.nextDouble());
            }
            
            //System.out.println("Using terms:");
            long startTime1 = System.nanoTime();

            MTerm M = new MTermConst();
            for (int i=0; i<stream.length; i++) {
                //System.out.println(M);
                //System.out.println("size (in bytes) = " + M.getSize());
                //System.out.println(M.getSize());
                
                //System.out.println("\t" + Double.toString(stream[i]));
                M = MTermIns.create(new ITermConst(stream[i]),M); 
                //System.out.println(M);
            }
            ITerm t = new ITermRank(M,0.5);
            //System.out.println("size (in bytes) = " + t.getSize());
            //double output1 = t.getValue();
            //System.out.println("output = " + output1);
            //System.out.println(root.toStringRec());
            
            //long endTime1 = System.nanoTime();
            //long duration1 = (endTime1 - startTime1) / 1000000;
            //System.out.println("duration = " + duration1);
            //System.out.println("tree height = " + root.getHeight());
            //System.out.println("tree size = " + root.getSize());
            System.out.println(logU + " " + M.getSize());
            
            /*
            System.out.println();
            
            System.out.println("Using arrays:");
            long startTime2 = System.nanoTime();
            
            AHist h = new AHist();
            for (int i=0; i<stream.length; i++) {
                h.add(stream[i],1);
            }
            double output2 = h.getRank(0.5);
            System.out.println("output = " + output2);
            
            long endTime2 = System.nanoTime();
            long duration2 = (endTime2 - startTime2) / 1000000;
            System.out.println("duration = " + duration2);
            System.out.println("array size = " + h.getSize());
            
            System.out.println();
            System.out.println("ratio: " + ((double) duration1 / (double) duration2));
            */
            
            U = U*2;
        } // end of logU loop
        
    }
    
}
