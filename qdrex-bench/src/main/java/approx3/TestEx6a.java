package approx3;
import java.util.Date;
import java.util.Random;

public class TestEx6a {
    
    public static void main(String[] args) {
        System.out.println("***** SLIDING WINDOW - BITS example *****");
        
        int N = 100 * 1000 * 1000; // stream size
        int w = 2000 * 1000; // window
        Random rnd = new Random(new Date().getTime());
        
        boolean[] bitStream = new boolean[N];
        for (int i=0; i<N; i++) {
            bitStream[i] = (rnd.nextDouble() < 0.5) ? false : true;
        }
        
        System.out.println("Using exponential histogram:");
        long startTime1 = System.nanoTime();

        long now = 0;
        BitCountWnd EH = new BitCountWnd(w,now);
        long sum = 0;
        long count = 0;
        for (int i=0; i<bitStream.length; i++) {
            if (bitStream[i]) EH = EH.add();
            if (i >= w) { sum += EH.getCount(); count++; }
            //System.out.print(EH.getCount());
            EH = EH.tick();
            //System.out.println(" tick " + EH.getCount());
        }
        int output1 = EH.getCount();
        System.out.println("output = " + output1);
        System.out.println("mean = " + ((double) sum/count));
        
        long endTime1 = System.nanoTime();
        long duration1 = (endTime1 - startTime1) / 1000000;
        System.out.println("duration = " + duration1);
        System.out.println("EH: " + EH);
        
        
        System.out.println();
        
        System.out.println("Using just running sum/count:");
        long startTime2 = System.nanoTime();
        
        double output2 = Double.NaN;
        System.out.println("output = " + output2);
        
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2) / 1000000;
        System.out.println("duration = " + duration2);
        
        System.out.println();
        System.out.println("ratio: " + ((double) duration1 / (double) duration2));
    }
    
}
