package approx3.nexmark;
import com.google.common.base.Optional;

import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.bench.nexmark.NexmarkStreamIterator;
import edu.upenn.cis.qdrex.bench.nexmark.value.Auction;
import edu.upenn.cis.qdrex.bench.nexmark.value.Bid;
import edu.upenn.cis.qdrex.bench.nexmark.value.NexmarkValue;
import edu.upenn.cis.qdrex.bench.nexmark.value.Person;
import edu.upenn.cis.qdrex.bench.nexmark.value.Person.Address;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermMax;
import edu.upenn.cis.qdrex.core.term.arith.ITermRank;
import edu.upenn.cis.qdrex.core.term.arith.MTermSingleton;
import edu.upenn.cis.qdrex.core.term.arith.MTermUnion;
import edu.upenn.cis.qdrex.core.util.Either3;
import edu.upenn.cis.qdrex.core.util.collect.Iterators;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Approx;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import java.util.Iterator;
import java.util.List;

public class Query2 {
    private static final String US = "United States";
    private Term<Multiset> m1; // multiset of bids from US
    private Term<Multiset> m2; // multiset of bids from non-US
    
    // initialize
    public Query2() {
        m1 = ConstantTerm.of(Multiset.EMPTY);
        m2 = ConstantTerm.of(Multiset.EMPTY);
    }
    
    public void update(Bid b) {
        Optional<Address> optAddr = b.person.address;
        if (optAddr.isPresent()) {
            Address addr = optAddr.get();
            String c = addr.country;
            if (c.equals(US)) {
                m1 = MTermUnion.of(MTermSingleton.of(ConstantTerm.of(b.price)),m1);
            } else { // non-US
                m2 = MTermUnion.of(MTermSingleton.of(ConstantTerm.of(b.price)),m2);
            }
        }
    }
    
    public double output() {
        Term<Double> med1 = ITermRank.of(m1,0.5);
        Term<Double> med2 = ITermRank.of(m2,0.5);
        Term<Double> out = ITermMax.of(med1,med2);
        return out.eval();
    }
    
    private static void varyN() {
        int NMAX = 1 * 1000 * 1000;
        System.out.println("eps = " + Approx.getEpsilon());

        for (int N=1000; N<=NMAX; N+=90000) {
            NexmarkStreamIterator it = new NexmarkStreamIterator();
            Query2 alg = new Query2();
            int bidCount = 0;
            for (int i=0; i<N; i++) {
                Either3<Person,Auction,Bid> val = it.next().value;
                //System.out.println(val);
                if (val.direction == Either3.Direction.LEFT) {
                    Person p = val.getLeft();
                } else if (val.direction == Either3.Direction.MIDDLE) {
                    Auction a = val.getMiddle();
                } else { // val.direction == Either3.Direction.RIGHT)
                    bidCount++;
                    Bid b = val.getRight();
                    //System.out.println("bid = " + b);
                    //System.out.println("category = " + b.item.category);
                    //System.out.println("price = " + b.price);
                    alg.update(b);
                }
            }
            //System.out.println("stream length = " + N);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            //System.out.println("multiset: " + alg.m.toString());
            int space = alg.m1.getBytes() + alg.m2.getBytes();
            System.out.println(N + " " + alg.output() + " " + space);
        }
    }    
    
    private static void varyEps() {
        int N = 10 * 1000 * 1000;
        List<NexmarkValue> list = ImmutableList.copyOf(Iterators.keep(new NexmarkStreamIterator(), N));
        System.out.println("***** Finished generating data *****");
        System.out.printf("1/e, len, refOut, space, time\n");
        for (double e=0.1; e>=0.01; e-=0.01) {
            Approx.setEpsilon(e);
            
            // NexmarkStreamIterator it = new NexmarkStreamIterator();
            Iterator<NexmarkValue> it = list.iterator();
            Query2 alg = new Query2();
            int bidCount = 0;
            
            long startTime = System.nanoTime();
            
            for (int i=0; i<N; i++) {
                Either3<Person,Auction,Bid> val = it.next().value;
                //System.out.println(val);
                if (val.direction == Either3.Direction.LEFT) {
                    Person p = val.getLeft();
                } else if (val.direction == Either3.Direction.MIDDLE) {
                    Auction a = val.getMiddle();
                } else { // val.direction == Either3.Direction.RIGHT)
                    bidCount++;
                    Bid b = val.getRight();
                    //System.out.println("bid = " + b);
                    //System.out.println("category = " + b.item.category);
                    //System.out.println("price = " + b.price);
                    alg.update(b);
                }
            }
            double output = alg.output();
            long endTime = System.nanoTime();
            //System.out.println("eps = " + Approx.eps);
            //System.out.println("stream length = " + N);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            int space = alg.m1.getBytes() + alg.m2.getBytes();
            System.out.printf("%f, %d, %f, %d, %d\n", 1 / e, N, output, space, endTime - startTime);
        }
    }        
    
    public static void main(String[] args) {
        System.out.println("***** NexMark Query2 *****");
        
        varyEps();
    }
    
}
