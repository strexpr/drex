package approx3.nexmark;
import approx3.*;
import edu.upenn.cis.qdrex.bench.nexmark.NexmarkStreamIterator;
import edu.upenn.cis.qdrex.bench.nexmark.value.Auction;
import edu.upenn.cis.qdrex.bench.nexmark.value.Bid;
import edu.upenn.cis.qdrex.bench.nexmark.value.Person;
import edu.upenn.cis.qdrex.core.util.Either3;
import java.util.Random;

public class Query1obs {
    private static final int N_CAT = 303; // category ids: 0..302
    private static final Random rnd = new Random(0);
    private final int P = 0;
    private final MTermParam Pt = new MTermParam(P);
    private ITerm x;
    
    // initialize
    public Query1obs() {
        // max(p_i) for every category i
        x = new ITermRank(Pt,0.5);
        for (int i=0; i<N_CAT; i++) {
            ITerm t = ITermMax.create(Double.NEGATIVE_INFINITY,new ITermParam(i));
            x = x.subst(MTermIns.create(t,Pt),P);
        }
    }

    public void update(int cat, int amount) {
        if ((cat<0) || (cat>=N_CAT))
            throw new RuntimeException("category out of bounds");
        x = x.subst(ITermMax.create(amount,new ITermParam(cat)),cat);
    }
    
    public void update(Bid b) {
        int i = b.item.category;
        if ((i<0) || (i>=N_CAT))
            throw new RuntimeException("category out of bounds");
        x = x.subst(ITermMax.create(b.price,new ITermParam(i)),i);
    }
    
    public double output() {
        ITerm t = x;
        for (int i=0; i<N_CAT; i++) {
            t = t.subst(new ITermConst(Double.NEGATIVE_INFINITY),i);
        }
        return t.getValue();
    }
    
    public static void main(String[] args) {
        System.out.println("***** NexMark Query1 *****");
        
        Query1obs alg = new Query1obs();
        NexmarkStreamIterator it = new NexmarkStreamIterator();
        int N = 1000 * 1000;
        int bidCount = 0;
        for (int i=0; i<N; i++) {
            Either3<Person,Auction,Bid> val = it.next().value;
            if (val.direction == Either3.Direction.LEFT) {
                Person p = val.getLeft();
            } else if (val.direction == Either3.Direction.MIDDLE) {
                Auction a = val.getMiddle();
            } else { // val.direction == Either3.Direction.RIGHT)
                bidCount++;
                Bid b = val.getRight();
                //System.out.println("bid = " + b);
                //System.out.println("category = " + b.item.category);
                //System.out.println("price = " + b.price);
                alg.update(b);
                //System.out.println("TERM: " + alg.x);
            }

            //int cat = rnd.nextInt(N_CAT);
            //int amount = rnd.nextInt(1000);
            //alg.update(cat,amount);
        }
        System.out.println("term: " + alg.x);
        System.out.println("N = " + N);
        System.out.println("Bid count = " + bidCount);
        System.out.println("output = " + alg.output());
        System.out.println("space = " + alg.x.getSize());
    }
    
}