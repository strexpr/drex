package approx3.nexmark;
import approx3.*;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.bench.nexmark.NexmarkStreamIterator;
import edu.upenn.cis.qdrex.bench.nexmark.value.Auction;
import edu.upenn.cis.qdrex.bench.nexmark.value.Bid;
import edu.upenn.cis.qdrex.bench.nexmark.value.NexmarkValue;
import edu.upenn.cis.qdrex.bench.nexmark.value.Person;
import edu.upenn.cis.qdrex.core.util.Either3;
import edu.upenn.cis.qdrex.core.util.collect.Iterators;
import java.util.Iterator;
import java.util.List;

// Median of bids for items for category CAT.
public class Query1 {
    private static final int N_CAT = 303;
    private static final int CAT = 100;
    private MTerm m; // multiset of bids for items of category CAT
    
    // initialize
    public Query1() {
        m = new MTermConst();
    }
    
    public void update(Bid b) {
        int i = b.item.category;
        if ((i<0) || (i>=N_CAT))
            throw new RuntimeException("category out of bounds");
        
        if (i == CAT) {
            m = MTermIns.create(new ITermConst(b.price),m);
        }
    }
    
    public double output() {
        return (new ITermRank(m,0.5)).getValue();
    }
    
    private static void varyN() {
        int NMAX = 1 * 1000 * 1000;
        System.out.println("eps = " + Approx.eps);

        for (int N=1000; N<=NMAX; N+=90000) {
            NexmarkStreamIterator it = new NexmarkStreamIterator();
            Query1 alg = new Query1();
            int bidCount = 0;
            for (int i=0; i<N; i++) {
                Either3<Person,Auction,Bid> val = it.next().value;
                //System.out.println(val);
                if (val.direction == Either3.Direction.LEFT) {
                    Person p = val.getLeft();
                } else if (val.direction == Either3.Direction.MIDDLE) {
                    Auction a = val.getMiddle();
                } else { // val.direction == Either3.Direction.RIGHT)
                    bidCount++;
                    Bid b = val.getRight();
                    //System.out.println("bid = " + b);
                    //System.out.println("category = " + b.item.category);
                    //System.out.println("price = " + b.price);
                    alg.update(b);
                }
            }
            //System.out.println("stream length = " + N);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            //System.out.println("multiset: " + alg.m.toString());
            System.out.println(N + " " + alg.output() + " " + alg.m.getSize());
        }
    }    
    
    private static void varyEps() {
        int N = 10 * 1000 * 1000;
        List<NexmarkValue> list = ImmutableList.copyOf(Iterators.keep(new NexmarkStreamIterator(), N));
        System.out.println("***** Finished generating data *****");
        System.out.printf("1/e, len, refOut, space, time\n");
        for (double e=0.1; e>=0.01; e-=0.01) {
            Approx.eps = e;
            Approx.lnOneEps = Math.log(1.0+e);
            
            // NexmarkStreamIterator it = new NexmarkStreamIterator();
            Iterator<NexmarkValue> it = list.iterator();
            Query1 alg = new Query1();
            int bidCount = 0;
            
            long startTime = System.nanoTime();
            
            for (int i=0; i<N; i++) {
                Either3<Person,Auction,Bid> val = it.next().value;
                //System.out.println(val);
                if (val.direction == Either3.Direction.LEFT) {
                    Person p = val.getLeft();
                } else if (val.direction == Either3.Direction.MIDDLE) {
                    Auction a = val.getMiddle();
                } else { // val.direction == Either3.Direction.RIGHT)
                    bidCount++;
                    Bid b = val.getRight();
                    //System.out.println("bid = " + b);
                    //System.out.println("category = " + b.item.category);
                    //System.out.println("price = " + b.price);
                    alg.update(b);
                }
            }
            double output = alg.output();
            long endTime = System.nanoTime();
            //System.out.println("eps = " + Approx.eps);
            //System.out.println("stream length = " + N);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            System.out.printf("%f, %d, %f, %d, %d\n", 1 / e, N, output, alg.m.getSize(), endTime - startTime);
        }
    }        
    
    public static void main(String[] args) {
        System.out.println("***** NexMark Query1 *****");
        
        varyEps();
    }
    
}
