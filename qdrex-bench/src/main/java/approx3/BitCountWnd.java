package approx3;

// Bit counting for sliding windows.
public class BitCountWnd {
    private static final double eps = 0.01; // maximum relative error (1+eps)
    private static final int k = (int) Math.ceil(1.0/eps); // k = ceiling(1/eps)
    //private static final int k2 = (int) Math.ceil(0.5/eps); // k2 = ceiling(0.5/eps)
    private final int window; // size of sliding window
    private final long now;
    private final ArrayExt<QueueLong> buckets;
    
    public BitCountWnd(int w, long n) {
        if (w <= 0) throw new IllegalArgumentException("w <= 0");
        if (n < 0) throw new IllegalArgumentException("n < 0");
            
        window = w;
        now = n;
        buckets = new ArrayExt<QueueLong>(5, new QueueLong());
    }
    
    private BitCountWnd(int w, long n, ArrayExt<QueueLong> b) {
        window = w;
        now = n;
        buckets = b;
    }
    
    public long getNow() {
        return now;
    }
    
    public BitCountWnd add() {
        // index = 0
        // Add to bucket of size 2^0 = 1
        // non-last # buckets of size 1 (y): k+1 <= y <= k+2
        QueueLong q = buckets.get(0); // get queue with buckets of size 1
        q = q.add(now); // add bucket with timestamp now
        if (q.size() <= k+2) {
            // maximum # buckets of size 1 not exceeded
            ArrayExt<QueueLong> bucketsNew = buckets.put(0,q);
            return new BitCountWnd(window,now,bucketsNew);
        }
        
        // q.size() > k+2
        // Start merging cascade.
        int index = 1;
        boolean ok = false;
        ArrayExt<QueueLong> bucketsNew = buckets;
        while (!ok) {
            // q is the queue of buckets of size 2^(index-1)
            long oldest1 = q.get();
            q = q.remove();
            long oldest2 = q.get();
            q = q.remove();
            bucketsNew = bucketsNew.put(index-1,q); // update buckets of size 2^(index-1)
            long mergedTime = Math.max(oldest1,oldest2); // keep most recent timestamp
            
            if (index >= bucketsNew.size()) bucketsNew = bucketsNew.extend();
            q = bucketsNew.get(index);
            q = q.add(mergedTime); // add bucket of size 2^index
            
            if (q.size() <= k+1) {
                // maximum # buckets of size 2^index NOT exceeded
                bucketsNew = bucketsNew.put(index,q);
                ok = true;
            } else { // q.size() > k+1
                // maximum # buckets of size 2^index exceeded
                index++;
                if (index > bucketsNew.size()) bucketsNew = bucketsNew.extend();
            }
        }
        
        return new BitCountWnd(window,now,bucketsNew);
    }
    
    // One time step elapses.
    public BitCountWnd tick() {
        long newNow = now + 1;
        ArrayExt<QueueLong> bucketsNew = buckets;
        
        int oldest = 0;
        for (; oldest<buckets.size(); oldest++) {
            QueueLong q = buckets.get(oldest);
            if (q.size() == 0) break;
        }
        if (oldest > 0) oldest--;
        
        QueueLong q = buckets.get(oldest);
        if ((q.size() > 0) && (newNow - q.get() >= window)) {
            // discard oldest bucket
            q = q.remove();
            bucketsNew = bucketsNew.put(oldest,q);
        }
        
        return new BitCountWnd(window,newNow,bucketsNew);
    }
    
    public int getCount() {
        int sum = 0;
        int coeff = 1;
        for (int i=0; i<buckets.size(); i++) {
            QueueLong q = buckets.get(i);
            if (q.size() == 0) break;
            sum += coeff * q.size();
            coeff *= 2;
        }
        coeff /= 2; // coefficient for last bucket counted
        // Adjust: we should have added only 1 for the last bucket.
        // This adjustment should only be done for last buckets of size >= 2.
        if (coeff >= 2) sum = sum-coeff+1;
        return sum;
    }
    
    public String toString() {
        return "eps = " + eps + ", k = " + k + ", window = " + window + ", now = " + now + "\n buckets:\n" + buckets;
    }
    
    public static void main(String[] args) {
        System.out.println("***** BitCountWnd *****");
        
        int N = 20;
        int w = 10;
        long now = 0;
        BitCountWnd o = new BitCountWnd(w,now);
        for (int i=0; i<N; i++) {
            o = o.add();
            o = o.add();
            o = o.add();
            o = o.tick();
            System.out.println(o.buckets.toString());
        }
    }
    
}