package approx3;
import java.util.Date;
import java.util.Random;

public class TestEx4a {
    
    public static void main(String[] args) {
        System.out.println("***** MEDIAN example *****");
        
        int N = 1000 * 1000;
        int U = 1000;
        Random rnd = new Random(new Date().getTime());
        
        double[] stream = new double[N];
        for (int i=0; i<N; i++) {
            stream[i] = 1 + U * rnd.nextDouble();
        }
        
        long initFreeMem = Runtime.getRuntime().freeMemory();
        long initTotalMem = Runtime.getRuntime().totalMemory();
        System.out.println("total memory = " + initTotalMem);
        System.out.println("free memory = " + initFreeMem);
        long initUsedMem = initTotalMem - initFreeMem;
        System.out.println("used memory = " + initUsedMem);
        
        System.out.println("Using terms:");
        long startTime1 = System.nanoTime();

        BSTNodeInt root = new BSTNodeInt(Approx.valToIndex(stream[0]));
        System.out.println(root);
        for (int i=1; i<stream.length; i++) {
            //long freeMem = Runtime.getRuntime().freeMemory();
            //long totalMem = Runtime.getRuntime().totalMemory();
            //long usedMem = totalMem - freeMem;
            //System.out.println("additional memory = " + (usedMem - initUsedMem));
            //System.out.println("size = " + root.getSize());
            
            //System.out.println("\t" + Double.toString(stream[i]));
            root = root.insert(Approx.valToIndex(stream[i]));
            //System.out.println(x);
        }
        double output1 = Approx.indexToVal(root.getRank(0.5));
        System.out.println("output = " + output1);
        //System.out.println(root.toStringRec());
        
        long endTime1 = System.nanoTime();
        long duration1 = (endTime1 - startTime1) / 1000000;
        System.out.println("duration = " + duration1);
        System.out.println("tree height = " + root.getHeight());
        System.out.println("tree size = " + root.getSize());
        
        
        System.out.println();
        
        System.out.println("Using arrays:");
        long startTime2 = System.nanoTime();
        
        AHist h = new AHist();
        for (int i=0; i<stream.length; i++) {
            h.add(stream[i],1);
        }
        double output2 = h.getRank(0.5);
        System.out.println("output = " + output2);
        
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2) / 1000000;
        System.out.println("duration = " + duration2);
        System.out.println("array size = " + h.getSize());
        
        System.out.println();
        System.out.println("ratio: " + ((double) duration1 / (double) duration2));
        
        if (output1 != output2) {
            root.checkConsistency();
            
            System.out.println(root.toStringRec());
            
            System.out.println(h.toString());
        }
    }
    
}