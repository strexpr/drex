package approx3.med;
import approx3.*;

public class MedAlg1 {
    private MTerm m;
    
    public MedAlg1() {
        m = new MTermConst();
    }
    
    public void update(double d) {
        m = MTermIns.create(new ITermConst(d),m);
    }
    
    public double output() {
        return (new ITermRank(m,0.5)).getValue();
    }
    
    public static void main(String[] args) {
        System.out.println("***** MedAlg1 *****");
        
        int N = 100 * 1000 * 1000;
        int U = 1;
        System.out.println("N = " + N);
        System.out.println("logU  space(bytes)");
        for (int logU=0; logU<=20; logU++) {
            Stream s = new Stream(N,U);
            MedAlg1 alg = new MedAlg1();
            while (s.hasNext()) {
                double d = s.next();
                alg.update(d);
            }
            System.out.println(alg.m.getSize());
            //System.out.println(logU + " " + alg.m.getSize());
            //System.out.println("output = " + alg.output());
            
            U *= 2;
        }
    }
}
