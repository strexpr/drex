package approx3.med;
import java.util.Random;
import java.util.Iterator;

public class Stream implements Iterator<Double> {
    private static final Random rnd = new Random(0);
    private final int N;
    private final int U;
    private int i;
    
    public Stream(int N, int U) {
        this.N = N;
        this.U = U;
        i = 0;
    }

    public boolean hasNext() {
        return i<N;
    }
    
    public Double next() {
        if (!hasNext()) throw new RuntimeException("stream has ended");
        
        i++;
        return 1 + U * rnd.nextDouble();
    }
}
