package approx3;

// Extensible immutable array.
public class ArrayExt<T> {
    private final int size;
    private final T def; // default value for initialization
    private final Node<T> root;
    
    public ArrayExt(int s, T d) {
        if (s <= 0) throw new IllegalArgumentException("s <= 0");
        size = s;
        def = d;
        root = Node.create(size,d);
    }
    
    private ArrayExt(int s, T d, Node<T> r) {
        size = s;
        def = d;
        root = r;
    }
    
    public int size() {
        return size;
    }
    
    // Double the size of the array.
    // Initialize the new cells with null.
    public ArrayExt<T> extend() {
        return new ArrayExt<T>(2*size,def,root.extend(def));
    }
    
    public T get(int index) {
        if ((index < 0) || (index >= size))
            throw new IllegalArgumentException("ArrayExt: index " + index + " out of range");
        
        return root.get(index);
    }
    
    public ArrayExt<T> put(int index, T o) {
        if ((index < 0) || (index >= size))
            throw new IllegalArgumentException("ArrayExt: index " + index + " out of range");
        
        return new ArrayExt<T>(size,def,root.put(index,o));
    }
    
    public String toString() {
        return "size = " + size + "\n" + root.toStringRec();
    }
    
    public static void main(String[] args) {
        Node<Integer> root = Node.create(5,null);
        System.out.println(root.toStringRec());
        root = root.put(1,3);
        System.out.println(root.toStringRec());
        root = root.extend(null);
        System.out.println(root.toStringRec());
    }
    
    private static class Node<T> {
        private final int start; // start index
        private final int size; // size of tree
        private final int index; // index for this node
        private final T data;
        private final Node<T> left; // subtree for [start..index-1]
        private final Node<T> right; // subtree for [index+1..start+size-1]
        
        // Extend the tree by doubling its size.
        public Node<T> extend(T init) {
            // Right subtree has size (this.size-1).
            Node<T> newRight = create(start+size+1,size-1,init);
            return new Node<T>(start,2*size,start+size,init,this,newRight);
        }
        
        // Create a tree of size sz, initiallized with init.
        public static <T> Node<T> create(int size, T init) {
            return create(0,size,init);
        }
        
        private static <T> Node<T> create(int start, int size, T init) {
            if (size <= 0) throw new IllegalArgumentException("sz <= 0");
            
            if (size == 1) return new Node<T>(start,size,start,init,null,null);
            
            // size >= 2
            int half = size / 2;
            Node<T> left = create(start,half,init);
            Node<T> right = null;
            int rightSize = size-half-1;
            if (rightSize > 0) right = create(start+half+1,rightSize,init);
            return new Node<T>(start,size,start+half,init,left,right);
        }
        
        private Node(int st, int sz, int i, T d, Node<T> l, Node<T> r) {
            start = st;
            size = sz;
            index = i;
            data = d;
            left = l;
            right = r;
        }
        
        public T get(int i) {
            if ((i < start) || (i >= start+size))
                throw new IllegalArgumentException("index out of range");
            
            if (i == index) return data;
            if (i < index) return left.get(i);
            
            // i > index
            return right.get(i);
        }

        public Node<T> put(int i, T o) {
            if ((i < start) || (i >= start+size))
                throw new IllegalArgumentException("index out of range");
            
            if (i == index) 
                return new Node<T>(start,size,index,o,left,right);
            
            if (i < index) {
                Node<T> leftNew = left.put(i,o);
                return new Node<T>(start,size,index,data,leftNew,right);
            }
            
            // i > index
            Node<T> rightNew = right.put(i,o);
            return new Node<T>(start,size,index,data,left,rightNew);
        }
        
        public String toString() {
            return "start: " + start + ", size: " + size + ", index: " + index + ", data: " + data;
        }
        
        public String toStringRec() {
            return toStringRecAux(0);
        }
        
        private String toStringRecAux(int level) {
            String indent = "    ";
            String totalIndent = "";
            for (int i=0; i<level; i++) totalIndent += indent;
            
            String str = totalIndent + this.toString() + "\n";
            
            if (left != null) str += left.toStringRecAux(level+1);
            if ((left == null) && (right != null)) str += totalIndent+indent+"-----\n";
            if (right != null) str += right.toStringRecAux(level+1);
            
            return str;
        }
    } // end of Node<T>
    
} // end of ArrayImmut<T>
