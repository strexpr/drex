package approx3;

public class Test {
    
    public static void main(String[] args) {
        System.out.println("----- TEST -----");
        
        //System.out.println(Math.log(2)/Math.log(1.1));
        
        double p = 1.0 / 91.0;
        double q = 90.0 / 91.0;
        int I = 62;
        double x =  0;
        for (int i=0; i<10000; i++)
            x += i*Math.exp(Math.log(q) * i) * p;
        System.out.println(x);
        
        ITerm c1 = new ITermConst(1);
        ITerm c2 = new ITermConst(2);
        ITerm c3 = new ITermConst(3);
        ITerm c4 = new ITermConst(4);
        ITerm c5 = new ITermConst(5);
        ITerm c6 = new ITermConst(6);
        ITerm c7 = new ITermConst(7);
        ITerm c8 = new ITermConst(8);
        ITerm c9 = new ITermConst(9);        
        
        ITerm p0 = new ITermParam(0);
        ITerm p1 = new ITermParam(1);
        ITerm p2 = new ITermParam(2);
        ITerm p3 = new ITermParam(3);
        ITerm p4 = new ITermParam(4);

        MTerm P0 = new MTermParam(0);
        MTerm P1 = new MTermParam(1);
        MTerm P2 = new MTermParam(2);
        MTerm P3 = new MTermParam(3);
        MTerm P4 = new MTermParam(4);
        
        MSet mu = new MSet();
        mu = mu.ins(0.0);
        System.out.println(mu);
        mu = mu.ins(1.0);
        System.out.println(mu);
        mu = mu.ins(2.0);
        System.out.println(mu);
        mu = mu.ins(3.0);
        System.out.println(mu);
        mu = mu.ins(4.0);
        System.out.println(mu);
        mu = mu.ins(5.0);
        System.out.println(mu);
        mu = mu.ins(6.0);
        System.out.println(mu);
        mu = mu.ins(-1.0);
        System.out.println(mu);
        mu = mu.ins(-2.0);
        System.out.println(mu);
        mu = mu.ins(-3.0);
        System.out.println(mu);
        mu = mu.ins(-4.0);
        System.out.println(mu);
        mu = mu.ins(-5.0);
        System.out.println(mu);
        mu = mu.ins(-6.0);
        System.out.println(mu);
        mu = mu.ins(-7.0);
        System.out.println(mu);
        mu = mu.ins(-8.0);
        System.out.println(mu);
        mu = mu.ins(-9.0);
        System.out.println(mu);
        mu = mu.ins(-10.0);
        System.out.println(mu);
        mu = mu.ins(-11.0);
        System.out.println(mu);
        mu = mu.ins(-12.0);
        System.out.println(mu);
        mu = mu.ins(-13.0);
        System.out.println(mu);
        
        System.out.println("median = " + mu.rnk(0.5));
    }
}
