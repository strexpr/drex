package approx3;

public class ITermMax implements ITerm {
    final private double acc; // accumulator
    final private ITerm left; // left child
    final private ITerm right; // right child
    // Invariant: left != null
    
    private ITermMax(ITerm l, ITerm r) {
        this(Double.NEGATIVE_INFINITY,l,r);
    }
    
    public int getSize() {
        int res = 8;
        res += 4 + ((left==null) ? 0 : left.getSize());
        res += 4 + ((right==null) ? 0 : right.getSize());
        return res;
    }
    
    private ITermMax(double a, ITerm l, ITerm r) {
        if (l==null) {
            throw new IllegalArgumentException("left == null");
        }
        
        acc = a;
        left = l;
        right = r;
    }
    
    // Precondition: l & r are reduced.
    // Postcondition: created term is reduced.
    public static ITerm create(ITerm l, ITerm r) {
        return create(Double.NEGATIVE_INFINITY,l,r);
    }
    
    // Precondition: r is reduced.
    // Postcondition: created term is reduced.
    public static ITerm create(double l, ITerm r) {
        return create(l,r,null);
    }    
    
    // Precondition: r is reduced.
    // Postcondition: created term is reduced.
    public static ITerm create(ITerm l, double r) {
        return create(r,l,null);
    }
    
    // Precondition: l & r are reduced.
    // Postcondition: created term is reduced.
    protected static ITerm create(double a, ITerm l, ITerm r) {
        double acc = a;

        if (l instanceof ITermConst) {
            ITermConst c = (ITermConst) l;
            acc = Math.max(acc,c.getValue());
            l = null;
        } else if (l instanceof ITermMax) {
            ITermMax t = (ITermMax) l;
            if (t.right == null) {
                // t = max(a|v,_)
                acc = Math.max(acc,t.acc);
                l = t.left;
            }
        }
        
        if (r instanceof ITermConst) {
            ITermConst c = (ITermConst) r;
            acc = Math.max(acc,c.getValue());
            r = null;
        } else if (r instanceof ITermMax) {
            ITermMax t = (ITermMax) r;
            if (t.right == null) {
                // t = max(a|v,_)
                acc = Math.max(acc,t.acc);
                r = t.left;
            }
        }

        if (l == null) {
            l = r;
            r = null;
        }
        
        if ((r == null) && (l instanceof ITermMax)) {
            // max(a|max(b|t1,t2),_) -> max(a+b|t1,t2)
            ITermMax t = (ITermMax) l;
            acc = Math.max(acc,t.acc);
            l = t.left;
            r = t.right;
        }
        
        if (l != null) {
            return new ITermMax(acc,l,r);
        } else if (r != null) {
            // l == null & r != null
            return new ITermMax(acc,r,null);
        } else {
            // newLeft == newRight == null
            return new ITermConst(acc);
        }
    }    
    
    protected double getAcc() {
        return acc;
    }

    protected ITerm getLeft() {
        return left;
    }
    
    protected ITerm getRight() {
        return right;
    }    
    
    public double getValue() {
        double res = this.acc;
        if (left != null) res = Math.max(res,left.getValue());
        if (right != null) res = Math.max(res,right.getValue());
        return res;
    }
    
    public boolean isGround() {
        if (!left.isGround()) return false;
        if (!right.isGround()) return false;
        return true;
    }
    
    // Precondition: this & t are reduced
    // Postcondition: result is reduced
    public ITerm subst(ITerm t, int p) {
        boolean change = false;
        
        ITerm newLeft = null;
        if (left != null) newLeft = left.subst(t,p);
        if (left != newLeft) change = true;

        ITerm newRight = null;
        if (right != null) newRight = right.subst(t,p);
        if (right != newRight) change = true;

        if (change) {
            return create(acc,newLeft,newRight);
        }
        
        return this;
    }
    
    // Only create new node if there has been a change below.
    public ITerm subst(MTerm N, int P) {
        boolean change = false;
        
        ITerm newLeft = null;
        if (left != null) newLeft = left.subst(N,P);
        if (left != newLeft) change = true;

        ITerm newRight = null;
        if (right != null) newRight = right.subst(N,P);
        if (right != newRight) change = true;

        if (change) {
            return create(acc,newLeft,newRight);
        }
        
        return this;
    }
    
    public String toString() {
        String a = (acc == Double.NEGATIVE_INFINITY) ? "" : Double.toString(acc);
        String l = (left == null) ? "_" : left.toString();
        String r = (right == null) ? "_" : right.toString();
        
        return "max(" + a + "|" + l + "," + r + ")";
    }
}
