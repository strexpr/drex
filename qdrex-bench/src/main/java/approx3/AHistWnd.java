package approx3;
import java.util.function.Function;

// Epsilon-approximate histogram for sliding window
public class AHistWnd {
    private final int window; // size of sliding window
    private final long now;
    private final BSTNode<BitCountWnd> hist; // BST for histogram
    // We allow the elements of hist to be lagging in term of now.
    // But whenever they are read or updated, they will have to catch up.
    
    public AHistWnd(int w) {
        window = w;
        now = 0;
        hist = null;
    }
    
    private AHistWnd(int w, long n, BSTNode<BitCountWnd> h) {
        window = w;
        now = n;
        hist = h;
    }
    
    // Insert element val in histogram.
    public AHistWnd ins(double val) {
        int index = Approx.valToIndex(val);
        BSTNode<BitCountWnd> newHist = null;
        if (hist == null) {
            BitCountWnd empty = new BitCountWnd(window,now);
            newHist = new BSTNode<BitCountWnd>(index,empty.add());
        } else {
            // Get object from histogram, update it, then put it back in.
            BitCountWnd o = hist.get(index);
            if (o == null) {
                BitCountWnd empty = new BitCountWnd(window,now);
                newHist = hist.put(index,empty.add());
            } else { // o != null
                o = o.add();
                newHist = hist.put(index,o);
            }
        }
        return new AHistWnd(window,now,newHist);
    }
    
    public AHistWnd tick() {
        Function<BitCountWnd,BitCountWnd> f = o -> o.tick();
        return new AHistWnd(window,now+1,hist.map(f));
    }
    
    // Get running sum.
    public double getSum() {
        Function<BitCountWnd,Boolean> f = o -> unequalNow(now,o);
        if (hist.exists(f)) throw new RuntimeException("current times do not agree");
        
        Function<BitCountWnd,Function<Integer,Double>> g = o -> i -> o.getCount() * Approx.indexToVal(i);
        return hist.sum(g);
    }
    // Get running maximum.
    public double getMax() {
        Function<BitCountWnd,Boolean> f = o -> unequalNow(now,o);
        if (hist.exists(f)) throw new RuntimeException("current times do not agree");
        
        Function<BitCountWnd,Function<Integer,Double>> g =
                o -> i -> (o.getCount()==0) ? Double.NEGATIVE_INFINITY : Approx.indexToVal(i);
        return hist.max(g);
    }
    // Get running minimum.
    public double getMin() {
        Function<BitCountWnd,Boolean> f = o -> unequalNow(now,o);
        if (hist.exists(f)) throw new RuntimeException("current times do not agree");
        
        Function<BitCountWnd,Function<Integer,Double>> g =
                o -> i -> (o.getCount()==0) ? Double.POSITIVE_INFINITY : Approx.indexToVal(i);
        return hist.min(g);
    }
    private static boolean unequalNow(long now, BitCountWnd o) {
        return now != o.getNow();
    }
    
    public String toString() {
        return null;
    }
    
    public static void main(String[] args) {
        System.out.println("***** AHistWnd *****");

        int N = 1000;
        int w = 2;
        AHistWnd o = new AHistWnd(w);
        for (int i=1; i<=N; i++) {
            if (1001-i == 200) {
                System.out.println("stop");
            }
            System.out.println("current = " + (1001-i) + ", previous = " + ((i==1) ? "none" : (1002-i)));
            o = o.ins(1001-i);
            System.out.print(o.getMax());
            o = o.tick();
            System.out.println(" tick " + o.getMax());
        }
        o = o.tick();
        System.out.print("tick " + o.getMax());
    }
}
