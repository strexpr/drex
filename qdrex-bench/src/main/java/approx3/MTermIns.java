package approx3;

public class MTermIns implements MTerm {
    final private MSet acc; // accumulator (non-null only at top-level ins)
    final private ITerm t;
    final private MTerm M;
    
    private MTermIns(MSet a, ITerm t, MTerm M) {
        this.acc = a;
        this.t = t;
        this.M = M;
    }
    
    public int getSize() {
        int res = 4 + ((acc==null) ? 0 : acc.getSize());
        res += 4 + ((t==null) ? 0 : t.getSize());
        res += 4 + ((M==null) ? 0 : M.getSize());
        return res;
    }

    // Precondition: t & M are reduced
    // Postcondition: result is reduced
    public static MTerm create(ITerm t, MTerm M) {
        return create(null,t,M);
    }
    
    // Precondition: t & M are reduced
    // Postcondition: result is reduced
    private static MTerm create(MSet a, ITerm t, MTerm M) {
        if (a == null) a = new MSet();
        
        if (t instanceof ITermConst) {
            // ins(a|c,M)
            if (M instanceof MTermConst) {
                // ins(a|c,mu) -> ins(a,mu,c|_,_)
                MSet b = M.getValue().ins(t.getValue());
                return new MTermConst(a.merge(b));
            } else if (M instanceof MTermParam) {
                // ins(a|c,P)
                return new MTermIns(a.ins(t.getValue()),null,M);
            } else {
                assert (M instanceof MTermIns);
                // ins(a|c,ins(mu|t',M')) -> ins(a,mu,c|t',M')
                MTermIns insM = (MTermIns) M;
                MSet b = insM.acc.ins(t.getValue());
                if ((insM.t == null) && (insM.M == null)) return new MTermConst(a.merge(b));
                return new MTermIns(a.merge(b),insM.t,insM.M);
            }
        }
        
        // t is reduced & not a constant (has parameter)
        if (M instanceof MTermConst) {
            // ins(a|t,mu) -> ins(a,mu|t,_)
            if (t == null) return new MTermConst(a.merge(M.getValue()));
            return new MTermIns(a.merge(M.getValue()),t,null);
        } else if (M instanceof MTermParam) {
            // ins(a|t,P)
            return new MTermIns(a,t,M);
        } else {
            assert (M instanceof MTermIns);
            MTermIns insM = (MTermIns) M;
            if (insM.t == null) {
                // ins(a|t,ins(mu|null,M')) -> ins(a,mu|t,M')
                return new MTermIns(a.merge(insM.acc),t,insM.M);
            } else {
                // ins(a|t,ins(mu|t',M')) -> ins(a,mu|t,ins(t',M'))
                return new MTermIns(a.merge(insM.acc),t,new MTermIns(null,insM.t,insM.M));
            }
        }
    }  
    
    public MSet getValue() {
        // TODO
        return acc;
    }
    
    public MTerm subst(ITerm t, int p) {
        boolean change = false;
        
        ITerm newT = this.t.subst(t,p);
        if (newT != this.t) change = true;
        
        MTerm newM = this.M.subst(t,p);
        if (newM != M) change = true;
        
        if (change) return create(acc,newT,newM);
        return this;
    }
    
    public MTerm subst(MTerm N, int P) {
        boolean change = false;
        
        ITerm newT = (this.t==null) ? null : this.t.subst(N,P);
        if (newT != this.t) change = true;
        
        MTerm newM = (this.M==null) ? null: this.M.subst(N,P);
        if (newM != M) change = true;
        
        if (change) return create(acc,newT,newM);
        return this;
    } 
    
    public boolean isGround() {
        if (!t.isGround()) return false;
        if (!M.isGround()) return false;
        return true;
    }
    
    public String toString() {
        String str1 = (acc==null || acc.isEmpty()) ? "" : (acc.toString()+"|");
        String str2 = (t==null) ? "_" : t.toString();
        String str3 = (M==null) ? "_" : M.toString();
        
        return "ins(" + str1 + str2 + "," + str3 + ")";
    }    
}
