package approx3;
import java.util.function.Function;

// Binary search tree that describes a partial map from keys to data.
public class BSTNode<T> {
    private final int key; // the key for binary search
    private final T data; // stored data
    private final int size; // size of tree
    private final int height; // height of tree
    private final BSTNode<T> left; // left subtree
    private final BSTNode<T> right; // right subtree
    
    // Create tree with a single node.
    protected BSTNode(int k, T d) {
        key = k;
        data = d;
        height = 1;
        size = 1;
        left = null;
        right = null;
    }    
    
    private BSTNode(int k, T d, int s, int h, BSTNode<T> l, BSTNode<T> r) {
        key = k;
        data = d;
        size = s;
        height = h;
        left = l;
        right = r;
    }
    
    protected int getHeight() {
        return height;
        //int hLeft = (left == null) ? 0 : left.getHeight();
        //int hRight = (right == null) ? 0 : right.getHeight();
        //return Math.max(hLeft,hRight)+1;
    }

    protected int getSize() {
        return size;
        //int sLeft = (left == null) ? 0 : left.getSize();
        //int sRight = (right == null) ? 0 : right.getSize();
        //return sLeft + sRight + 1;
    }
    
    public boolean exists(Function<T,Boolean> f) {
        if (f.apply(data)) return true;
        if ((left!=null) && (left.exists(f))) return true;
        return (right!=null) && right.exists(f);
    }
    
    public BSTNode<T> map(Function<T,T> f) {
        BSTNode<T> leftNew = (left==null) ? null : left.map(f);
        BSTNode<T> rightNew = (right==null) ? null : right.map(f);
        T dataNew = f.apply(data);
        
        if ((leftNew != left) || (rightNew != right) || (dataNew != data)) {
            return new BSTNode<T>(key,dataNew,size,height,leftNew,rightNew);
        }
        
        return this;
    }
    
    // f: (data,key) -> value
    public double sum(Function<T,Function<Integer,Double>> f) {
        double res = f.apply(data).apply(key);
        if (left != null) res += left.sum(f);
        if (right != null) res += right.sum(f);
        return res;
    }

    // f: (data,key) -> value
    public double max(Function<T,Function<Integer,Double>> f) {
        double res = f.apply(data).apply(key);
        if (left != null) res = Math.max(res,left.max(f));
        if (right != null) res = Math.max(res,right.max(f));
        return res;
    }

    // f: (data,key) -> value
    public double min(Function<T,Function<Integer,Double>> f) {
        double res = f.apply(data).apply(key);
        if (left != null) res = Math.min(res,left.min(f));
        if (right != null) res = Math.min(res,right.min(f));
        return res;
    }
    
    // Get element at position k, null if there is none.
    public T get(int k) {
        if (k == key) return data;
        
        if (k < key) {
            if (left == null) return null;
            return left.get(k);
        }
        
        // k > key
        if (right == null) return null;
        return right.get(k);
    }
    
    // Insert element e at position k.
    public BSTNode<T> put(int k, T e) {
        BSTNode<T> newNode = null;
        
        if (k == key) {
            // replace this node with element e
            newNode = new BSTNode<T>(k,e,size,height,left,right);
        } else if (k < key) {
            if (left == null) {
                BSTNode<T> newLeft = new BSTNode<T>(k,e);
                int rightHeight = (right == null) ? 0 : right.height;
                assert (newLeft.height == 1);
                int newHeight = Math.max(newLeft.height,rightHeight) + 1;
                newNode = new BSTNode<T>(key,data,size+1,newHeight,newLeft,right);
            } else { // left != null
                BSTNode<T> newLeft = left.put(k,e);
                assert (newLeft != left);
                int rightHeight = (right == null) ? 0 : right.height;
                int newHeight = Math.max(newLeft.height,rightHeight)+1;
                int newSize = newLeft.size + ((right == null) ? 0 : right.size) + 1;
                newNode = new BSTNode<T>(key,data,newSize,newHeight,newLeft,right);
            }
        } else { // k > key
            if (right == null) {
                BSTNode<T> newRight = new BSTNode<T>(k,e);
                int leftHeight = (left == null) ? 0 : left.height;
                int newHeight = Math.max(leftHeight,newRight.height)+1;
                newNode = new BSTNode<T>(key,data,size+1,newHeight,left,newRight);
            } else {
                BSTNode<T> newRight = right.put(k,e);
                assert (newRight != right);
                int leftHeight = (left == null) ? 0 : left.height;
                int newHeight = Math.max(leftHeight,newRight.height)+1;
                int newSize = ((left == null) ? 0 : left.size) + newRight.size + 1;
                newNode = new BSTNode<T>(key,data,newSize,newHeight,left,newRight);
            }
        }
        
        //System.out.println("rebalance:");
        //System.out.println(newNode.toStringRec());
        
        // Balancing: start with lowest node that falsifies the property of being balanced.
        int lHeight = (newNode.left == null) ? 0 : newNode.left.height;
        int rHeight = (newNode.right == null) ? 0 : newNode.right.height;
        
        // Left subtree overweight
        if (lHeight > rHeight+1) {
            BSTNode<T> x = newNode; // x != null
            BSTNode<T> y = x.left; // y != null
            BSTNode<T> z = x.right;
            int llHeight = (y.left == null) ? 0 : y.left.height;
            int lrHeight = (y.right == null) ? 0 : y.right.height;
            BSTNode<T> y1 = y.left;
            BSTNode<T> y2 = y.right;
            
            assert (llHeight != lrHeight);
            
            // First case
            if (llHeight > lrHeight) {
                int h = lrHeight;
                // x, y, y1 != null
                //              x<h+3>                    y<h+2>
                //        y<h+2>      z<h>   ===>  y1<h+1>      x<h+1>
                // y1<h+1>      y2<h>                      y2<h>      z<h>
                int xNewSize = ((y2==null) ? 0 : y2.size) + ((z==null) ? 0 : z.size) + 1;
                BSTNode<T> xNew = new BSTNode<T>(x.key,x.data,xNewSize,h+1,y2,z);
                //System.out.println("xNew:");
                //System.out.println(xNew.toStringRec());
                BSTNode<T> yNew = new BSTNode<T>(y.key,y.data,y1.size+xNew.size+1,h+2,y1,xNew);
                return yNew;
            }
            
            // Second case
            if (lrHeight > llHeight) {
                // x, y, y2 != null
                int h = (z == null) ? 0 : z.height;
                BSTNode<T> y21 = y2.left;
                BSTNode<T> y22 = y2.right;
                //                     x<h+3>                         y2
                //       y<h+2>                   z<h>     ===>   y        x
                // y1<h>        y2<h+1>                         y1 y21  y22 z
                //        y21<h/h-1> y22<h/h-1>
                int yNewSize = ((y1==null) ? 0 : y1.size) + ((y21==null) ? 0 : y2.size) + 1;
                BSTNode<T> yNew = new BSTNode<T>(y.key,y.data,yNewSize,h+1,y1,y21);
                //System.out.println("yNew:");
                //System.out.println(yNew.toStringRec());
                int xNewSize = ((y22==null) ? 0 : y22.size) + ((z==null) ? 0 : z.size) + 1;
                BSTNode<T> xNew = new BSTNode<T>(x.key,x.data,xNewSize,h+1,y22,z);
                //System.out.println("xNew:");
                //System.out.println(xNew.toStringRec());
                BSTNode<T> y2New = new BSTNode<T>(y2.key,y2.data,yNew.size+xNew.size+1,h+2,yNew,xNew);
                return y2New;
            }
        }
        
        // Right subtree overweight
        if (rHeight > lHeight+1) {
            BSTNode<T> x = newNode; // x != null
            BSTNode<T> y = x.left;
            BSTNode<T> z = x.right; // z != null
            int rlHeight = (z.left == null) ? 0 : z.left.height;
            int rrHeight = (z.right == null) ? 0 : z.right.height;
            BSTNode<T> z1 = z.left;
            BSTNode<T> z2 = z.right; 
            
            assert (rlHeight != rrHeight);
            
            // First case
            if (rrHeight > rlHeight) {
                int h = rlHeight;
                // x, z, z2 != null
                //     x<h+3>                            z<h+2>      
                // y<h>      z<h+2>        ===>    x<h+1>      z2<h+1>   
                //       z1<h>    z2<h+1>       y<h>    z1<h>
                int xNewSize = ((y==null) ? 0 : y.size) + ((z1==null) ? 0 : z1.size) + 1;
                BSTNode<T> xNew = new BSTNode<T>(x.key,x.data,xNewSize,h+1,y,z1);
                //System.out.println("xNew:");
                //System.out.println(xNew.toStringRec());
                BSTNode<T> zNew = new BSTNode<T>(z.key,z.data,xNew.size+z2.size+1,h+2,xNew,z2);
                return zNew;
            }
            
            // Second case
            if (rlHeight > rrHeight) {
                // x, z, z1 != null
                int h = (y == null) ? 0 : y.height;
                BSTNode<T> z11 = z1.left;
                BSTNode<T> z12 = z1.right;
                //        x<h+3>                               z1
                // y<h>                z<h+2>        ===>  x        z
                //             z1<h+1>        z2<h>       y z11  z12 z2
                //      z11<h/h-1> z12<h/h-1>
                int xNewSize = ((y==null) ? 0 : y.size) + ((z11==null) ? 0 : z11.size) + 1;
                BSTNode<T> xNew = new BSTNode<T>(x.key,x.data,xNewSize,h+1,y,z11);
                //System.out.println("xNew:");
                //System.out.println(xNew.toStringRec());
                int zNewSize = ((z12==null) ? 0 : z12.size) + ((z2==null) ? 0 : z2.size) + 1;
                BSTNode<T> zNew = new BSTNode<T>(z.key,z.data,zNewSize,h+1,z12,z2);
                //System.out.println("zNew:");
                //System.out.println(zNew.toStringRec());
                BSTNode<T> z1New = new BSTNode<T>(z1.key,z1.data,xNew.size+zNew.size+1,h+2,xNew,zNew);
                return z1New;
            }            
        }
        
        return newNode;
    }
    
    public String toString() {
        return "key = " + key + ", data = " + data + ", size = " + size + ", height = " + height;
    }
    
    public String toStringRec() {
        return toStringRecAux(0);
    }
    
    private String toStringRecAux(int level) {
        String indent = "    ";
        String totalIndent = "";
        for (int i=0; i<level; i++) totalIndent += indent;
        
        String str = totalIndent + this.toString() + "\n";
        
        if (left != null) str += left.toStringRecAux(level+1);
        if ((left == null) && (right != null)) str += totalIndent+indent+"-----\n";
        if (right != null) str += right.toStringRecAux(level+1);
        
        return str;
    }

}
