package approx3;

public class ITermSet {
    private final int size;
    private final int height;
    private final ITerm term;
    private final ITermSet left;
    private final ITermSet right;
    
    private ITermSet(int s, int h, ITerm t, ITermSet l, ITermSet r) {
        size = s;
        height = h;
        term = t;
        left = l;
        right = r;
    }
    
    public static ITermSet create(ITerm t) {
        return new ITermSet(1,1,t,null,null);
    }
    
    public ITermSet add(ITerm t) {
        if (left == null) {
            ITermSet newLeft = create(t);
            int h2 = (right == null) ?  0 : right.height;
            int newHeight = Math.max(1,h2) + 1;
            return new ITermSet(size+1,newHeight,term,newLeft,right);
        }
        
        // left != null
        if (right == null) {
            ITermSet newRight = create(t);
            int newHeight = Math.max(left.height,1) + 1;
            return new ITermSet(size+1,newHeight,term,left,newRight);
        }
        
        // left != null && right != null
        
        if (left.size <= right.size) {
            ITermSet newLeft = left.add(t);
            int newSize = newLeft.size + right.size + 1;
            assert (newSize == size+1);
            int newHeight = Math.max(newLeft.height,right.height) + 1;
            assert (newHeight <= height+1);
            return new ITermSet(newSize,newHeight,term,newLeft,right);
        } else {
            // right.size < left.size
            ITermSet newRight = right.add(t);
            int newSize = left.size + newRight.size + 1;
            assert (newSize == size+1);
            int newHeight = Math.max(left.height,newRight.height) + 1;
            assert (newHeight <= height+1);
            return new ITermSet(newSize,newHeight,term,left,newRight);
        }
    }
    
    public String toString() {
        return "size = " + size + ", height = " + height + ", data = " + term.toString();
    }
    
    public String toStringRec() {
        return toStringRecAux(0);
    }
    
    private String toStringRecAux(int level) {
        String indent = "    ";
        String str = "";
        for (int i=0; i<level; i++) str += indent;
        
        str += this.toString() + "\n";
        
        if (left != null) str += left.toStringRecAux(level+1);
        if (right != null) str += right.toStringRecAux(level+1);
        
        return str;
    }
    
}
