package approx3;

public class ITermRank implements ITerm {
    final private MTerm M;
    final private double o; // in interval [0,1]
    
    public ITermRank(MTerm M, double o) {
        this.M = M;
        this.o = o;
    }
    
    public int getSize() {
        return 4 + M.getSize() + 8;
    }
    
    public double getValue() {
        MSet mu = M.getValue();
        return mu.rnk(this.o);
    }
    
    public boolean isGround() {
        return M.isGround();
    }
    
    public ITerm subst(ITerm t, int p) {
        MTerm newM = M.subst(t,p);
        if (newM != M) return new ITermRank(newM,this.o);
        return this;
    }
    
    public ITerm subst(MTerm N, int P) {
        MTerm newM = M.subst(N,P);
        if (newM != M) return new ITermRank(newM,this.o);
        return this;
    }
    
    public String toString() {
        return "rank_" + o + " " + M.toString();
    }
}
