package approx3.bank;
import java.util.Random;
import java.util.Iterator;

public class BankStream implements Iterator<BankValue> {

    private static final double DEPOSIT_PROB = 60.0 / (60 + 30 + 1);
    private static final double END_DAY_PROB = 30.0 / (60 + 30 + 1);
    //private static final double END_MONTH_PROB = 1.0 / (60 + 30 + 1);
    //private static final double DEPOSIT_HI = 1000;

    private final Random rnd;
    private final int N;
    private final int U;
    private int i;
    
    public BankStream(int N, int U) {
        this.N = N;
        this.U = U;
        i = 0;
        rnd = new Random(0);
    }

    public boolean hasNext() {
        return (i<N);
    }
    
    public BankValue next() {
        if (!hasNext()) throw new RuntimeException("stream has ended");
        
        double r = rnd.nextDouble();

        if (r < DEPOSIT_PROB) {
            double amount = U * rnd.nextDouble();
            return BankValue.deposit(amount);
        }
        r = r - DEPOSIT_PROB;

        if (r < END_DAY_PROB) {
            return BankValue.END_DAY;
        } else {
            i++;
            return BankValue.END_MONTH;
        }
    }

}
