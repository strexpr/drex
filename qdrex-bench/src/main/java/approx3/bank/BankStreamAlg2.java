package approx3.bank;

import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermRank;
import edu.upenn.cis.qdrex.core.term.arith.ITermSum;
import edu.upenn.cis.qdrex.core.term.arith.MTermSingleton;
import edu.upenn.cis.qdrex.core.term.arith.MTermUnion;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Approx;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;

class BankStreamAlg2 {
    private enum State { START, FINAL }
    private State q;
    private Term<Double> x;
    public Term<Multiset> m;
    
    public BankStreamAlg2() {
        q = State.START;
        x = ConstantTerm.of(0.0);
        m = ConstantTerm.of(Multiset.EMPTY);
    }
    
    public void update(BankValue d) {
        if (q == State.START) {
            if (d.type == BankValue.Type.DEPOSIT) {
                x = ITermSum.of(x,d.amount);
            } else if (d.type == BankValue.Type.END_MONTH) {
                m = MTermUnion.of(MTermSingleton.of(x),m);
                //x = ConstantTerm.of(0.0);
                q = State.FINAL;
            }
        } else { // q == State.FINAL
            if (d.type == BankValue.Type.DEPOSIT) {
                x = ITermSum.of(x,d.amount);
                q = State.START;
            } else if (d.type == BankValue.Type.END_DAY) {
                q = State.START;
            } else if (d.type == BankValue.Type.END_MONTH) {
                m = MTermUnion.of(MTermSingleton.of(x),m);
                //x = ConstantTerm.of(0.0);
            }
        }
    }
    
    public double output() {
        if (q != State.FINAL)
            throw new RuntimeException("not final state");
        
        return ITermRank.of(m,0.5).eval();
    }
    
    private static void varyN() {
        // N=11,032 -> stream length 999,941
        //int N = 11032;
        // N=110,246 -> stream length 9,999,952
        //int N = 110246;
        // N=1,100,848 -> stream length 100,000,033
        //int N = 1100848;
        
        int NMAX = 110246;
        System.out.println("eps = " + Approx.getEpsilon());
        int U = 1000;
        for (int N=10; N<=NMAX; N+=1000) {
            BankStream stream = new BankStream(N,U);
            BankStreamAlg2 alg = new BankStreamAlg2();
            int count = 0;
            while (stream.hasNext()) {
                BankValue v = stream.next();
                alg.update(v);
                //System.out.println(v);
                count++;
            }
            //System.out.println("stream length = " + count);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            System.out.println(count + " " + alg.output() + " " + alg.m.getBytes());
        }
    }    
    
    private static void varyEps() {
        // N=11,032 -> stream length 999,941
        //int N = 11032;
        // N=110,246 -> stream length 9,999,952
        //int N = 110246;
        // N=1,100,848 -> stream length 100,000,033
        //int N = 1100848;
        
        int N = 110246;
        int U = 1000;
        for (double e=0.1; e>=0.01; e-=0.01) {
            Approx.setEpsilon(e);
            BankStream stream = new BankStream(N,U);
            BankStreamAlg2 alg = new BankStreamAlg2();
            int count = 0;
            while (stream.hasNext()) {
                BankValue v = stream.next();
                alg.update(v);
                //System.out.println(v);
                count++;
            }
            //System.out.println("eps = " + Approx.eps);
            //System.out.println("stream length = " + count);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            System.out.println((1/e) + " " + alg.output() + " " + alg.m.getBytes());
        }
    }        
    
    private static void measureTime() {
        int N = 110246, U = 1000;
        System.out.printf("1/epsilon, Length, RefOut1, Time\n");
        for (double e = 0.01; e <= 0.1; e += 0.01) {
            Approx.setEpsilon(e);
            BankStream stream = new BankStream(N, U);
            BankStreamAlg2 alg = new BankStreamAlg2();
            long startTime = System.nanoTime();
            int count = 0;
            while (stream.hasNext()) {
                alg.update(stream.next());
                count++;
            }
            long endTime = System.nanoTime();
            System.out.printf("%f, %d, %f, %d\n", 1 / e, count, alg.output(), endTime - startTime);
        }
    }
    
    public static void main(String[] args) {
        System.out.println("***** BankStreamAlg2 *****");
        
        // varyEps();
        measureTime();
    }
}
