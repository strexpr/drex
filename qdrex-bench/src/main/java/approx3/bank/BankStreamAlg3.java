package approx3.bank;

import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermMax;
import edu.upenn.cis.qdrex.core.term.arith.ITermRank;
import edu.upenn.cis.qdrex.core.term.arith.ITermSum;
import edu.upenn.cis.qdrex.core.term.arith.MTermSingleton;
import edu.upenn.cis.qdrex.core.term.arith.MTermUnion;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Approx;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;

class BankStreamAlg3 {
    private enum State { START, ENDD, FINAL }
    private State q;
    private Term<Double> x,y;
    public Term<Multiset> m;
    
    public BankStreamAlg3() {
        q = State.START;
        x = ConstantTerm.of(0.0);
        y = ConstantTerm.of(0.0);
        m = ConstantTerm.of(Multiset.EMPTY);
    }
    
    public void update(BankValue d) {
        if (q == State.START) {
            if (d.type == BankValue.Type.DEPOSIT) {
                x = ITermSum.of(x,ConstantTerm.of(1));
            } else if (d.type == BankValue.Type.END_DAY) {
                y = ITermMax.of(y,x);
                x = ConstantTerm.of(0.0);
                q = State.ENDD;
            } else if (d.type == BankValue.Type.END_MONTH) {
                m = MTermUnion.of(MTermSingleton.of(ITermMax.of(y,x)),m);
                x = ConstantTerm.of(0.0);
                y = ConstantTerm.of(0.0);
                q = State.FINAL;
            }
        } else if (q == State.ENDD) {
            // Invariant: x=0.
            if (d.type == BankValue.Type.DEPOSIT) {
                x = ITermSum.of(x,ConstantTerm.of(1));
                q = State.START;
            } else if (d.type == BankValue.Type.END_DAY) {
                // do nothing, because x=0 and max(y,x) = max(y,0) = y.
            } else if (d.type == BankValue.Type.END_MONTH) {
                m = MTermUnion.of(MTermSingleton.of(y),m);
                y = ConstantTerm.of(0.0);
                q = State.FINAL;
            }            
        } else { // q == State.FINAL
            // Invariant: x=y=0.
            if (d.type == BankValue.Type.DEPOSIT) {
                x = ITermSum.of(x,ConstantTerm.of(1));
                q = State.START;
            } else if (d.type == BankValue.Type.END_DAY) {
                q = State.ENDD;
            } else if (d.type == BankValue.Type.END_MONTH) {
                m = MTermUnion.of(MTermSingleton.of(ConstantTerm.of(0)),m);
            }
        }
    }
    
    public double output() {
        if (q != State.FINAL)
            throw new RuntimeException("not final state");
        
        return (ITermRank.of(m,0.5)).eval();
    }
    
    private static void varyN() {
        // N=11,032 -> stream length 999,941
        //int N = 11032;
        // N=110,246 -> stream length 9,999,952
        //int N = 110246;
        // N=1,100,848 -> stream length 100,000,033
        //int N = 1100848;
        
        int NMAX = 110246;
        System.out.println("eps = " + Approx.getEpsilon());
        int U = 1000;
        for (int N=10; N<=NMAX; N+=1000) {
            BankStream stream = new BankStream(N,U);
            BankStreamAlg3 alg = new BankStreamAlg3();
            int count = 0;
            while (stream.hasNext()) {
                BankValue v = stream.next();
                alg.update(v);
                //System.out.println(v);
                count++;
            }
            //System.out.println("stream length = " + count);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            System.out.println(count + " " + alg.output() + " " + alg.m.getBytes());
        }
    }    
    
    private static void varyEps() {
        // N=11,032 -> stream length 999,941
        //int N = 11032;
        // N=110,246 -> stream length 9,999,952
        //int N = 110246;
        // N=1,100,848 -> stream length 100,000,033
        //int N = 1100848;
        
        int N = 110246;
        int U = 1000;
        for (double e=0.1; e>=0.01; e-=0.01) {
            Approx.setEpsilon(e);
            BankStream stream = new BankStream(N,U);
            BankStreamAlg3 alg = new BankStreamAlg3();
            int count = 0;
            while (stream.hasNext()) {
                BankValue v = stream.next();
                alg.update(v);
                //System.out.println(v);
                count++;
            }
            //System.out.println("eps = " + Approx.eps);
            //System.out.println("stream length = " + count);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            System.out.println((1/e) + " " + alg.output() + " " + alg.m.getBytes());
        }
    }
    
    private static void measureTime() {
        int N = 1100848, U = 1000;
        System.out.printf("1/epsilon, Length, RefOut1, Time\n");
        for (double e = 0.01; e <= 0.1; e += 0.01) {
            Approx.setEpsilon(e);
            BankStream stream = new BankStream(N, U);
            BankStreamAlg3 alg = new BankStreamAlg3();
            long startTime = System.nanoTime();
            int count = 0;
            while (stream.hasNext()) {
                alg.update(stream.next());
                count++;
            }
            long endTime = System.nanoTime();
            System.out.printf("%f, %d, %f, %d\n", 1 / e, count, alg.output(), endTime - startTime);
        }
    }
    
    public static void main(String[] args) {
        System.out.println("***** BankStreamAlg3 *****");
        
        // varyEps();
        measureTime();
    }
}
