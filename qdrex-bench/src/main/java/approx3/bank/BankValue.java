package approx3.bank;

public class BankValue {

    public enum Type { DEPOSIT, END_DAY, END_MONTH }

    public final Type type;
    public final double amount;

    private BankValue(Type type, double amount) {
        this.type = type;
        this.amount = amount;
    }

    public static BankValue deposit(double amount) {
        //checkArgument(amount >= 0);
        return new BankValue(Type.DEPOSIT, amount);
    }

    public static final BankValue END_DAY = new BankValue(Type.END_DAY, 0);
    public static final BankValue END_MONTH = new BankValue(Type.END_MONTH, 0);
    
    public String toString() {
        if (type == Type.DEPOSIT) {
            return Double.toString(amount);
        } else if (type == Type.END_DAY) {
            return "END_D";
        } else {
            return "END_M";
        }
    }
}
