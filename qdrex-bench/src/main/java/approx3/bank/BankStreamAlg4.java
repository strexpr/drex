package approx3.bank;

import static com.google.common.base.Verify.verify;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;
import edu.upenn.cis.qdrex.core.term.Term;
import edu.upenn.cis.qdrex.core.term.arith.ITermRank;
import edu.upenn.cis.qdrex.core.term.arith.ITermSum;
import edu.upenn.cis.qdrex.core.term.arith.MTermSingleton;
import edu.upenn.cis.qdrex.core.term.arith.MTermUnion;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Approx;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;

class BankStreamAlg4 {
    private enum State { START, FINAL }
    private State q;
    private Term<Double> x;
    public Term<Multiset> m;
    public Term<Double> hi;
    public Term<Double> lo;
    
    public BankStreamAlg4() {
        q = State.START;
        x = ConstantTerm.of(0);
        m = ConstantTerm.of(Multiset.EMPTY);
        hi = ConstantTerm.of(0);
        lo = ConstantTerm.of(0);
    }
    
    public void update(BankValue d) {
        if (q == State.START) {
            if (d.type == BankValue.Type.DEPOSIT) {
                x = ITermSum.of(x, d.amount);
            } else if (d.type == BankValue.Type.END_DAY) {
                // do nothing
            } else if (d.type == BankValue.Type.END_MONTH) {
                m = MTermUnion.of(MTermSingleton.of(x), m);
                updateLoHi();
                x = ConstantTerm.of(0);
                q = State.FINAL;
            }
        } else { // q == State.FINAL
            verify(x.eval() == 0.0);
            if (d.type == BankValue.Type.DEPOSIT) {
                x = ITermSum.of(x, d.amount);
                q = State.START;
            } else if (d.type == BankValue.Type.END_DAY) {
                q = State.START;
            } else if (d.type == BankValue.Type.END_MONTH) {
                m = MTermUnion.of(MTermSingleton.of(x), m);
                updateLoHi();
                verify(x.eval() == 0.0);
            }
        }
    }
    
    private void updateLoHi() {
        double c1 = ITermRank.of(m,0.25).eval();
        double c2 = ITermRank.of(m,0.75).eval();
        double v = x.eval();
        if (v < c1) { // bottom quartile
            hi = ConstantTerm.of(0);
            lo = ITermSum.of(lo,1.0);
        } else if (v > c2) { // top quartile
            lo = ConstantTerm.of(0);
            hi = ITermSum.of(hi,1);
        } else { // middle
            lo = ConstantTerm.of(0);
            hi = ConstantTerm.of(0);
        }
    }
    
    public double output() {
        if (q != State.FINAL)
            throw new RuntimeException("not final state");
        
        return ITermRank.of(m,0.5).eval();
    }

    public double outputLo() {
        if (q != State.FINAL)
            throw new RuntimeException("not final state");
        
        return lo.eval();
    }

    public double outputHi() {
        if (q != State.FINAL)
            throw new RuntimeException("not final state");
        
        return hi.eval();
    }
    
    private static void varyN() {
        // N=11,032 -> stream length 999,941
        //int N = 11032;
        // N=110,246 -> stream length 9,999,952
        //int N = 110246;
        // N=1,100,848 -> stream length 100,000,033
        //int N = 1100848;
        
        int NMAX = 110246;
        System.out.println("eps = " + Approx.getEpsilon());
        int U = 1000;
        for (int N=10; N<=NMAX; N+=1000) {
            BankStream stream = new BankStream(N,U);
            BankStreamAlg4 alg = new BankStreamAlg4();
            int count = 0;
            while (stream.hasNext()) {
                BankValue v = stream.next();
                alg.update(v);
                //System.out.println(v);
                count++;
            }
            //System.out.println("stream length = " + count);
            //System.out.println("outputLo = " + alg.outputLo());
            //System.out.println("outputHi = " + alg.outputHi());
            //System.out.println("space = " + alg.m.getSize());
            //System.out.println(alg.m.toString());
            System.out.println(count + " " + alg.outputLo() + " " + alg.outputHi() + " " + alg.m.getBytes());
        }
        
    }    

    private static void varyEps() {
        // N=11,032 -> stream length 999,941
        //int N = 11032;
        // N=110,246 -> stream length 9,999,952
        //int N = 110246;
        // N=1,100,848 -> stream length 100,000,033
        //int N = 1100848;
        
        int N = 110246;
        int U = 1000;
        for (double e=0.1; e>=0.01; e-=0.01) {
            Approx.setEpsilon(e);
            BankStream stream = new BankStream(N,U);
            BankStreamAlg4 alg = new BankStreamAlg4();
            int count = 0;
            while (stream.hasNext()) {
                BankValue v = stream.next();
                alg.update(v);
                //System.out.println(v);
                count++;
            }
            //System.out.println("eps = " + Approx.eps);
            //System.out.println("stream length = " + count);
            //System.out.println("output = " + alg.output());
            //System.out.println("space = " + alg.m.getSize());
            System.out.println((1/e) + " " + alg.outputLo() + " " + alg.outputHi() + " " + alg.m.getBytes());
        }
    }    
    
    private static void measureTime() {
        int N = 110246, U = 1000;
        System.out.printf("1/epsilon, Length, RefOut1, Time\n");
        for (double e = 0.01; e <= 0.1; e += 0.01) {
            Approx.setEpsilon(e);
            BankStream stream = new BankStream(N, U);
            BankStreamAlg4 alg = new BankStreamAlg4();
            long startTime = System.nanoTime();
            int count = 0;
            while (stream.hasNext()) {
                alg.update(stream.next());
                count++;
            }
            long endTime = System.nanoTime();
            System.out.printf("%f, %d, %f, %d\n", 1 / e, count, alg.output(), endTime - startTime);
        }
    }
    
    public static void main(String[] args) {
        System.out.println("***** BankStreamAlg4 *****");
        
        // varyEps();
        measureTime();
    }
}
