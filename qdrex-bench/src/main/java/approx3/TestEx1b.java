package approx3;
import java.util.Random;
import java.util.Date;

public class TestEx1b {
    
    public static void main(String[] args) {
        System.out.println("***** PLUS example (no params) *****");
        
        int N = 100 * 1000 * 1000;
        Random rnd = new Random(new Date().getTime());
        
        double[] stream = new double[N];
        for (int i=0; i<N; i++) {
            stream[i] = Math.abs(rnd.nextDouble());
        }
        
        System.out.println("Using terms:");
        long startTime1 = System.nanoTime();

        ITerm x = new ITermConst(0.0);
        System.out.println(x);
        for (int i=0; i<stream.length; i++) {
            //System.out.println("\t" + Double.toString(stream[i]));
            x = ITermSum.create(x,stream[i]);
            //System.out.println(x);
        }
        System.out.println("output = " + x.getValue());
        
        long endTime1 = System.nanoTime();
        long duration1 = (endTime1 - startTime1) / 1000000;
        System.out.println("duration = " + duration1);
        
        
        System.out.println();
        
        System.out.println("Using values:");
        long startTime2 = System.nanoTime();
        
        double sum = 0.0;
        for (int i=0; i<stream.length; i++) {
            sum += stream[i];
        }
        System.out.println("output = " + sum);
        
        long endTime2 = System.nanoTime();
        long duration2 = (endTime2 - startTime2) / 1000000;
        System.out.println("duration = " + duration2);
        
        System.out.println();
        System.out.println("ratio: " + ((double) duration1 / (double) duration2));
    }
    
}
