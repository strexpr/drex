package approx3;

public class RSumCnt {
    private final double sum; // running sum
    private final int count; // running count
    
    public RSumCnt() {
        sum = 0;
        count = 0;
    }
    
    private RSumCnt(double s, int c) {
        sum = s;
        count = c;
    }
    
    public double getSum() {
        return sum;
    }
    
    public int getCount() {
        return count;
    }
    
    public boolean isEmpty() {
        return (count == 0);
    }
    
    public RSumCnt add(double val) {
        return new RSumCnt(sum+val,count+1);
    }
    
    public RSumCnt merge(RSumCnt r) {
        return new RSumCnt(sum+r.sum,count+r.count);
    }
    
    public double getAvg() {
        return sum / count;
    }
    
    public String toString() {
        return Double.toString(sum) + "<" + count + ">";
    }    
}
