package approx3;

public class MSet {
    private static final int nOfFields = 4; // number of fields
    private final BSTNodeInt hist; // approximate histogram for positive values
    private final BSTNodeInt histNeg; // approximate histogram for negative values
    private final int zeroCount; // count for zero values
    private final RSumCnt avgSummary; // summary for average
    
    public MSet() {
        hist = null;
        histNeg = null;
        zeroCount = 0;
        avgSummary = new RSumCnt();
    }
    
    public int getSize() {
        int res = 4 * nOfFields; // 3 reference fields
        res += (hist==null) ? 0 : 4 * BSTNodeInt.nOfFields * hist.getSize(); // every BSTNodeInt has 7 fields, 4 bytes each
        res += (histNeg==null) ? 0 : 4 * BSTNodeInt.nOfFields * histNeg.getSize();
        return res;
    }
    
    private MSet(BSTNodeInt h, BSTNodeInt hNeg, int z, RSumCnt a) {
        hist = h;
        histNeg = hNeg;
        zeroCount = z;
        avgSummary = a;
    }
    
    public boolean isEmpty() {
        return (hist == null) && (histNeg == null) && (zeroCount == 0);
    }
    
    public MSet ins(double val) {
        BSTNodeInt newHist = hist;
        BSTNodeInt newHistNeg = histNeg;
        int newZeroCount = zeroCount;
        
        if (val == 0) {
            newZeroCount++;
        } else if (val > 0) {
            int index = Approx.valToIndex(val);
            newHist = (hist==null) ? new BSTNodeInt(index) : hist.insert(index);            
        } else { // val < 0
            int index = Approx.valToIndex(-val);
            newHistNeg = (histNeg==null) ? new BSTNodeInt(index) : histNeg.insert(index);            
        }

        RSumCnt newAvgS = avgSummary.add(val);
        return new MSet(newHist,newHistNeg,newZeroCount,newAvgS);
    }
    
    public MSet merge(MSet mu) {
        if (mu.isEmpty()) return this;
        if (this.isEmpty()) return mu;
        
        // Both this & mu are NOT empty.
        
        BSTNodeInt hNew = null;
        if (hist != null) {
            hNew = hist.merge(mu.hist);
        } else if (mu.hist != null) {
            hNew = mu.hist.merge(hist);
        }
        BSTNodeInt hNegNew = null;
        if (histNeg != null) {
            hNegNew = histNeg.merge(mu.histNeg);
        } else if (mu.histNeg != null) {
            hNegNew = mu.histNeg.merge(histNeg);
        }
        RSumCnt sNew = avgSummary.merge(mu.avgSummary);
        return new MSet(hNew,hNegNew,zeroCount+mu.zeroCount,sNew);
    }
    
    public double avg() {
        return avgSummary.getAvg();
    }
    
    public double rnk(double o) {
        if (this.isEmpty()) {
            throw new RuntimeException("rank of empty multiset");
        }
        
        int totalCount = (hist==null) ? 0 : hist.getTotalCount();
        totalCount += zeroCount;
        totalCount += (histNeg==null) ? 0 : histNeg.getTotalCount();
        int totalCount2 = avgSummary.getCount();
        if (totalCount != totalCount2) {
            System.err.println("totalCount=" + totalCount + ", totalCount2="+totalCount2);
        }
        
        int index = (int) Math.floor(((double) totalCount-1) * o);
        
        if ((histNeg != null) && (index < histNeg.getTotalCount())) {
            int indexNeg = histNeg.getTotalCount()-1-index;
            return -Approx.indexToVal(histNeg.getElement(indexNeg));
        }
        if (histNeg != null) {
            index -= histNeg.getTotalCount();
        }
        
        if (index < zeroCount) {
            return 0.0;
        }
        index -= zeroCount;
        
        if (index < 0 || index >= hist.getTotalCount()) {
            System.err.println("index = " + index + ", total-count = " + hist.getTotalCount());
        }        
        return Approx.indexToVal(hist.getElement(index));
    }
    
    public String toString() {
        if (this.isEmpty()) return "empty";
        String str = "<POS: " + ((hist==null) ? "--" : hist.toString()) + ">";
        str += " & ";
        str += "zeroCount=" + zeroCount;
        str += " & ";
        str += "<NEG: " + ((histNeg==null) ? "--" : histNeg.toString()) + ">";
        return str + " & " + avgSummary.toString();
    }
}
