package approx3;

public class ITermAvg implements ITerm {
    final private MTerm M;
    
    public ITermAvg(MTerm M) {
        this.M = M;
    }
    
    public int getSize() {
        return 4 + M.getSize();
    }
    
    public double getValue() {
        MSet mu = M.getValue();
        return mu.avg();
    }
    
    public boolean isGround() {
        return M.isGround();
    }
    
    public ITerm subst(ITerm t, int p) {
        MTerm newM = M.subst(t,p);
        if (newM != M) return new ITermAvg(newM);
        return this;
    }
    
    public ITerm subst(MTerm N, int P) {
        MTerm newM = M.subst(N,P);
        if (newM != M) return new ITermAvg(newM);
        return this;
    }
    
    public String toString() {
        return "avg" + " " + M.toString();
    }
}
