package edu.upenn.cis.qdrex.bench.nexmark.value;

import com.google.common.base.VerifyException;
import edu.upenn.cis.qdrex.core.util.Either3;
import java.util.Objects;

public final class NexmarkValue {

    public final Either3<Person, Auction, Bid> value;

    public NexmarkValue(Person person) {
        this.value = Either3.left(person);
    }

    public NexmarkValue(Auction auction) {
        this.value = Either3.middle(auction);
    }

    public NexmarkValue(Bid bid) {
        this.value = Either3.right(bid);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof NexmarkValue)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        NexmarkValue other = (NexmarkValue)obj;
        return Objects.equals(value, other.value);
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public String toString() {
        switch (value.direction) {
            case LEFT: return String.valueOf(value.getLeft());
            case MIDDLE: return String.valueOf(value.getMiddle());
            case RIGHT: return String.valueOf(value.getRight());
            default: throw new VerifyException("Unreachable code!");
        }
    }

}
