package edu.upenn.cis.qdrex.bench.nexmark;

import edu.upenn.cis.qdrex.bench.nexmark.value.NexmarkValue;

public class NexmarkStream implements Iterable<NexmarkValue> {

    @Override
    public NexmarkStreamIterator iterator() {
        return new NexmarkStreamIterator();
    }

}
