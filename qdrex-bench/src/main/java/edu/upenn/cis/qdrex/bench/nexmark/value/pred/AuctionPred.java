package edu.upenn.cis.qdrex.bench.nexmark.value.pred;

import com.google.common.base.Optional;
import edu.upenn.cis.qdrex.bench.nexmark.value.Auction;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class AuctionPred extends Predicate<AuctionPred, Auction> {

    public final boolean value;

    public AuctionPred(boolean value) {
        this.value = value;
    }

    public static final AuctionPred TRUE = new AuctionPred(true);
    public static final AuctionPred FALSE = new AuctionPred(false);

    @Override
    public boolean test(Auction t) {
        return value;
    }

    @Override
    public Optional<Auction> getWitness() {
        if (value) {
            return Optional.of(new Auction(0, Optional.absent(),
                    Optional.absent(), 0, 0, null, 0, 0, "", 0, 0));
        } else {
            return Optional.absent();
        }
    }

    @Override
    public AuctionPred not() {
        return new AuctionPred(!value);
    }

    @Override
    public AuctionPred and(AuctionPred other) {
        return new AuctionPred(value && other.value);
    }

}
