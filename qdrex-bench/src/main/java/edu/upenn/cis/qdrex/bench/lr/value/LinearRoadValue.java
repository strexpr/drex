package edu.upenn.cis.qdrex.bench.lr.value;

import com.google.common.base.VerifyException;
import static com.google.common.base.Verify.verify;

public abstract class LinearRoadValue {

    public static LinearRoadValue of(String str) {
        String[] fields = str.split(" ");
        verify(fields.length == 15);
        fields[14] = fields[14].substring(0, fields[14].length() - 1);

        int timeStamp = Integer.parseInt(fields[1]);
        int carId = Integer.parseInt(fields[2]);
        switch (fields[0]) {
            case "#(0": {
                int speed = Integer.parseInt(fields[3]);
                int expressway = Integer.parseInt(fields[4]);
                int lane = Integer.parseInt(fields[5]);
                int direction = Integer.parseInt(fields[6]);
                int segment = Integer.parseInt(fields[7]);
                int xpos = Integer.parseInt(fields[8]);
                return new CarLoc(timeStamp, carId, speed, expressway, lane,
                        direction, segment, xpos);
            } case "#(2": {
                int queryId = Integer.parseInt(fields[9]);
                return new AccBalQuery(timeStamp, carId, queryId);
            } case "#(3": {
                int expressway = Integer.parseInt(fields[4]);
                int queryId = Integer.parseInt(fields[9]);
                int day = Integer.parseInt(fields[14]);
                return new DailyExpenditureQuery(timeStamp, carId, expressway,
                        queryId, day);
            } case "#(4": {
                int expressway = Integer.parseInt(fields[4]);
                int queryId = Integer.parseInt(fields[9]);
                int sinit = Integer.parseInt(fields[10]);
                int send = Integer.parseInt(fields[11]);
                int dayOfWeek = Integer.parseInt(fields[12]);
                int timeOfDay = Integer.parseInt(fields[13]);
                return new TravelTimeQuery(timeStamp, carId, expressway,
                        queryId, sinit, send, dayOfWeek, timeOfDay);
            } default:
                throw new VerifyException("Unrecognized tuple");
        }
    }

}
