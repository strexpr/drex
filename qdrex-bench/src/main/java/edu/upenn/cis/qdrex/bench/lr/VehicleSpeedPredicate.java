package edu.upenn.cis.qdrex.bench.lr;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.qdrex.bench.lr.value.AccBalQuery;
import edu.upenn.cis.qdrex.bench.lr.value.CarLoc;
import edu.upenn.cis.qdrex.bench.lr.value.LinearRoadValue;
import edu.upenn.cis.qdrex.core.symbol.EqualityPred;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class VehicleSpeedPredicate extends Predicate<VehicleSpeedPredicate, LinearRoadValue> {

    public final boolean acceptNonLoc;
    public final EqualityPred<Integer> vidAcceptor;
    public final EqualityPred<Integer> speedAcceptor;

    public VehicleSpeedPredicate(boolean acceptNonLoc, EqualityPred<Integer> vidAcceptor,
            EqualityPred<Integer> speedAcceptor) {
        this.acceptNonLoc = acceptNonLoc;
        this.vidAcceptor = checkNotNull(vidAcceptor);
        this.speedAcceptor = checkNotNull(speedAcceptor);
    }

    public static VehicleSpeedPredicate of(int carId, int speed) {
        EqualityPred<Integer> vidAcceptor = EqualityPred.equalToPositiveInt(carId);
        EqualityPred<Integer> speedAcceptor = EqualityPred.equalToPositiveInt(speed);
        return new VehicleSpeedPredicate(false, vidAcceptor, speedAcceptor);
    }

    @Override
    public boolean test(LinearRoadValue t) {
        if (t instanceof CarLoc) {
            CarLoc loc = (CarLoc)t;
            return vidAcceptor.test(loc.carId) && speedAcceptor.test(loc.speed);
        } else {
            return acceptNonLoc;
        }
    }

    @Override
    public Optional<LinearRoadValue> getWitness() {
        if (acceptNonLoc) {
            return Optional.of(new AccBalQuery(0, 0, 0));
        } else {
            Optional<Integer> ovid = vidAcceptor.getWitness();
            Optional<Integer> ospeed = speedAcceptor.getWitness();
            if (ovid.isPresent() && ospeed.isPresent()) {
                int vid = ovid.get();
                int speed = ospeed.get();
                return Optional.of(new CarLoc(0, vid, speed, 0, 0, 0, 0, 0));
            } else {
                return Optional.absent();
            }
        }
    }

    @Override
    public VehicleSpeedPredicate not() {
        boolean anlPrime = !acceptNonLoc;
        EqualityPred<Integer> vidAcceptorPrime = vidAcceptor.not();
        EqualityPred<Integer> speedAcceptorPrime = speedAcceptor.not();
        return new VehicleSpeedPredicate(anlPrime, vidAcceptorPrime, speedAcceptorPrime);
    }

    @Override
    public VehicleSpeedPredicate and(VehicleSpeedPredicate other) {
        boolean anlPrime = acceptNonLoc && other.acceptNonLoc;
        EqualityPred<Integer> vidAcceptorPrime = vidAcceptor.and(other.vidAcceptor);
        EqualityPred<Integer> speedAcceptorPrime = speedAcceptor.and(other.speedAcceptor);
        return new VehicleSpeedPredicate(anlPrime, vidAcceptorPrime, speedAcceptorPrime);
    }

}
