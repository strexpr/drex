package edu.upenn.cis.qdrex.bench.nexmark;

import edu.upenn.cis.qdrex.bench.nexmark.value.Person;
import edu.upenn.cis.qdrex.bench.nexmark.value.NexmarkValue;
import edu.upenn.cis.qdrex.bench.nexmark.value.Bid;
import edu.upenn.cis.qdrex.bench.nexmark.value.Auction;
import com.google.common.collect.ImmutableList;
import edu.upenn.cis.qdrex.bench.nexmark.data.BigValueGen;
import edu.upenn.cis.qdrex.bench.nexmark.data.FirstnamesGen;
import edu.upenn.cis.qdrex.bench.nexmark.data.LastnamesGen;
import edu.upenn.cis.qdrex.bench.nexmark.value.Person.Address;
import edu.upenn.cis.qdrex.bench.nexmark.value.Person.Profile;
import java.util.ArrayDeque;
import com.google.common.base.Optional;
import com.google.common.collect.AbstractIterator;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Random;
import org.apache.commons.lang3.tuple.ImmutableTriple;

public class NexmarkStreamIterator extends AbstractIterator<NexmarkValue> {

    public static final int NUM_CATEGORIES = 1000;

    private final Random rnd = new Random(103984);
    private final Random m_rnd = new Random(20934);

    private final SimpleCalendar cal = new SimpleCalendar(rnd);
    private final Persons persons = new Persons(); // for managing person ids
    private final OpenAuctions openAuctions = new OpenAuctions(cal);

    private final Queue<NexmarkValue> queue = new ArrayDeque<>();
    private /* mutable */ long numElem = 0;

    private final Map<Integer, Auction> auctionMap = new HashMap<>();
    private final Map<Integer, Person> personMap = new HashMap<>();

    @Override
    public NexmarkValue computeNext() {
        numElem++;

        if (numElem <= 50) {
            cal.incrementTime();
            Person person = makePerson();
            return new NexmarkValue(person);
        } else if (numElem <= 100) {
            cal.incrementTime();
            Auction auction = makeAuction();
            return new NexmarkValue(auction);
        }

        while (queue.isEmpty()) {
            fillQueue();
        }

        return queue.remove();
    }

    private void fillQueue() {
        if(rnd.nextInt(10) == 0) {
            cal.incrementTime();
            queue.add(new NexmarkValue(makePerson()));
        }

        cal.incrementTime();
        int numItems = rnd.nextInt(3); // should average 1
        for (int i = 0; i < numItems; i++) {
            queue.add(new NexmarkValue(makeAuction()));
        }

        cal.incrementTime();
        int numBids = rnd.nextInt(21); // should average 10
        for (int i = 0; i < numBids; i++) {
            queue.add(new NexmarkValue(makeBid()));
        }
    }

    private Person makePerson() {
        int personId = persons.getNewId();

        int ifn = m_rnd.nextInt(FirstnamesGen.FIRSTNAMES.length);
        String firstName = FirstnamesGen.FIRSTNAMES[ifn];
        int iln = m_rnd.nextInt(LastnamesGen.LASTNAMES.length);
        String lastName = LastnamesGen.LASTNAMES[iln];
        int iem = m_rnd.nextInt(BigValueGen.EMAILS.length);
        String emailAddress = BigValueGen.EMAILS[iem];

        Optional<ImmutableTriple<Integer, Integer, Integer>> phoneNumber = Optional.absent();
        if (m_rnd.nextBoolean()) {
            int p1 = m_rnd.nextInt(98) + 1;
            int p2 = m_rnd.nextInt(989) + 10;
            int p3 = m_rnd.nextInt(9864196) + 123457;
            phoneNumber = Optional.of(ImmutableTriple.of(p1, p2, p3));
        }

        Optional<Address> address = Optional.absent();
        if (m_rnd.nextBoolean()) {
            int ist = m_rnd.nextInt(LastnamesGen.LASTNAMES.length); // street
            int ict = m_rnd.nextInt(BigValueGen.CITIES.length); // city
            int icn = (m_rnd.nextInt(4) != 0) ? 0 : m_rnd.nextInt(BigValueGen.COUNTRIES.length);
            int ipv = (icn == 0) ? m_rnd.nextInt(BigValueGen.PROVINCES.length) :
                    m_rnd.nextInt(LastnamesGen.LASTNAMES.length);  // provinces are really states

            String street = String.format("%d %s St", m_rnd.nextInt(99) + 1,
                    LastnamesGen.LASTNAMES[ist]);
            String city = BigValueGen.CITIES[ict];
            String country = icn == 0 ? "United States" : BigValueGen.COUNTRIES[icn];
            String province = icn == 0 ? BigValueGen.PROVINCES[ipv] : LastnamesGen.LASTNAMES[ipv];
            String zipcode = String.valueOf(m_rnd.nextInt(99999) + 1);

            address = Optional.of(new Address(street, city, province, country, zipcode));
        }

        Optional<String> homepage = Optional.absent();
        if (m_rnd.nextBoolean()) {
            homepage = Optional.of(String.format("http://www.%s/~%s",
                    BigValueGen.EMAILS[iem], LastnamesGen.LASTNAMES[iln]));
        }

        Optional<String> creditCard = Optional.absent();
        if (m_rnd.nextBoolean()) {
            creditCard = Optional.of(String.format("%d %d %d %d",
                    m_rnd.nextInt(9000) + 1000, m_rnd.nextInt(9000) + 1000,
                    m_rnd.nextInt(9000) + 1000, m_rnd.nextInt(9000) + 1000));
        }

        Optional<Profile> profile = Optional.absent();
        if (m_rnd.nextBoolean()) {
            Optional<String> education = m_rnd.nextBoolean() ?
                    Optional.of(BigValueGen.EDUCATION[m_rnd.nextInt(BigValueGen.EDUCATION.length)]) :
                    Optional.absent();

            Optional<String> gender = m_rnd.nextBoolean() ?
                    m_rnd.nextInt(2) == 0 ? Optional.of("female") :
                    Optional.of("male") : Optional.absent();

            String business = (m_rnd.nextInt(2) == 1) ? "Yes" : "No";

            Optional<Integer> age = m_rnd.nextBoolean() ?
                    Optional.of(m_rnd.nextInt(15) + 30) : Optional.absent();

            double income = 40000 + m_rnd.nextInt(30000);
            income = income + m_rnd.nextInt(99) / 100.0;

            ImmutableList.Builder<Integer> interests = ImmutableList.builder();
            int nInterests = m_rnd.nextInt(5);
            for (int i = 0; i < nInterests; i++) {
                interests.add(m_rnd.nextInt(NUM_CATEGORIES));
            }

            profile = Optional.of(new Profile(education, gender, business, age,
                    income, interests.build()));
        }

        Person ans = new Person(personId, firstName, lastName, emailAddress,
                phoneNumber, address, homepage, creditCard, profile);
        personMap.put(personId, ans);
        return ans;
    }

    private Auction makeAuction() {
        int auctionId = openAuctions.getNewId();

        Optional<Integer> reserve = Optional.absent();
        if (rnd.nextBoolean()) {
            int resVal = (int)Math.round((openAuctions.getCurrPrice(auctionId)) * (1.2 + (rnd.nextDouble() + 1)));
            reserve = Optional.of(resVal);
        }

        Optional<String> privacy = Optional.absent();
        if (rnd.nextBoolean()) {
            String privVal = rnd.nextInt(2) == 0 ? "YES" : "NO";
            privacy = Optional.of(privVal);
        }

        int itemId = auctionId;

        int sellerId = persons.getExistingId();
        Person seller = personMap.get(sellerId);

        int category = rnd.nextInt(303);
        int quantity = 1 + rnd.nextInt(10);

        String type = rnd.nextInt(2) == 0 ? "REGULAR" : "FEATURED";
        if (quantity > 1 && rnd.nextBoolean()) {
            type = type + ", Dutch";
        }

        int startTime = cal.getTimeInSecs();
        long endTime = openAuctions.getEndTime(auctionId);

        Auction ans = new Auction(auctionId, reserve, privacy, itemId, sellerId,
                seller, category, quantity, type, startTime, endTime);
        auctionMap.put(auctionId, ans);
        return ans;
    }

    private Bid makeBid() {
        int itemId = openAuctions.getExistingId();
        Auction item = auctionMap.get(itemId);

        int time = cal.getTimeInSecs();

        int personId = persons.getExistingId();
        Person person = personMap.get(personId);

        int price = openAuctions.increasePrice(itemId);

        return new Bid(itemId, item, time, personId, person, price);
    }

}
