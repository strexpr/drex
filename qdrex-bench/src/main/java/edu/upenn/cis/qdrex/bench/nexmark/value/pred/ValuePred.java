package edu.upenn.cis.qdrex.bench.nexmark.value.pred;

import edu.upenn.cis.qdrex.bench.nexmark.value.Auction;
import edu.upenn.cis.qdrex.bench.nexmark.value.Bid;
import edu.upenn.cis.qdrex.bench.nexmark.value.Person;
import edu.upenn.cis.qdrex.bench.nexmark.value.NexmarkValue;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;

public class ValuePred extends Predicate<ValuePred, NexmarkValue> {

    public final AuctionPred auctionPred;
    public final BidPred bidPred;
    public final PersonPred personPred;

    public ValuePred(AuctionPred auctionPred, BidPred bidPred, PersonPred personPred) {
        this.auctionPred = checkNotNull(auctionPred);
        this.bidPred = checkNotNull(bidPred);
        this.personPred = checkNotNull(personPred);
    }

    public static final ValuePred TRUE = new ValuePred(AuctionPred.TRUE,
            BidPred.TRUE, PersonPred.TRUE);
    public static final ValuePred FALSE = new ValuePred(AuctionPred.FALSE,
            BidPred.FALSE, PersonPred.FALSE);

    @Override
    public boolean test(NexmarkValue t) {
        return t.value.map(p -> personPred.test(p), a -> auctionPred.test(a), b -> bidPred.test(b));
    }

    @Override
    public Optional<NexmarkValue> getWitness() {
        Optional<Auction> aw = auctionPred.getWitness();
        if (aw.isPresent()) {
            return Optional.of(new NexmarkValue(aw.get()));
        }

        Optional<Bid> bw = bidPred.getWitness();
        if (bw.isPresent()) {
            return Optional.of(new NexmarkValue(bw.get()));
        }

        Optional<Person> pw = personPred.getWitness();
        if (pw.isPresent()) {
            return Optional.of(new NexmarkValue(pw.get()));
        }

        return Optional.absent();
    }

    @Override
    public ValuePred not() {
        return new ValuePred(auctionPred.not(), bidPred.not(), personPred.not());
    }

    @Override
    public ValuePred and(ValuePred other) {
        return new ValuePred(auctionPred.and(other.auctionPred),
                bidPred.and(other.bidPred), personPred.and(other.personPred));
    }

}
