package edu.upenn.cis.qdrex.bench.lr;

import static com.google.common.base.Preconditions.checkArgument;
import edu.upenn.cis.qdrex.bench.lr.value.AccBalQuery;
import edu.upenn.cis.qdrex.bench.lr.value.CarLoc;
import edu.upenn.cis.qdrex.bench.lr.value.LinearRoadValue;
import edu.upenn.cis.qdrex.core.qre.Base;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.qre.arith.Avg;
import edu.upenn.cis.qdrex.core.qre.arith.Plus;
import edu.upenn.cis.qdrex.core.qre.arith.Rank;
import edu.upenn.cis.qdrex.core.qre.arith.Union;
import edu.upenn.cis.qdrex.core.symbol.EqualityPred;
import edu.upenn.cis.qdrex.core.symbol.TrivialPred;
import static edu.upenn.cis.qdrex.core.util.collect.CollectionUtil.ALL_POSITIVE_INTEGERS;
import edu.upenn.cis.qdrex.core.util.collect.multisets.Multiset;
import edu.upenn.cis.qdrex.core.qre.TryElse;
import static com.google.common.base.Verify.verify;
import com.google.common.collect.ImmutableSet;
import edu.upenn.cis.qdrex.core.symbol.EqualityPred.Type;

public class Expressions {

    public static final TrivialPred<LinearRoadValue> TRIVIAL_TRUE =
            new TrivialPred(true, new AccBalQuery(0, 0, 0));

    public static final Base<TrivialPred<LinearRoadValue>, LinearRoadValue, Double>
            TRIV_BASE_1 = new Base<>(TRIVIAL_TRUE, v -> 1.0, Double.class);
    public static final Base<TrivialPred<LinearRoadValue>, LinearRoadValue, Multiset>
            TRIV_BASE_SPEED = new Base<>(TRIVIAL_TRUE, v -> {
                if (v instanceof CarLoc) {
                    return Multiset.of(((CarLoc)v).speed);
                } else {
                    return Multiset.EMPTY;
                }
            }, Multiset.class);

    public static final QRE<TrivialPred<LinearRoadValue>, LinearRoadValue, Double>
            STREAM_LEN = Plus.iterPlus(TRIV_BASE_1, true);
    public static final QRE<TrivialPred<LinearRoadValue>, LinearRoadValue, Double>
            STREAM_MDN_SPEED = new Rank<>(Union.iterUnion(TRIV_BASE_SPEED, false), 0.5);

    /**
     * Benchmark query 1: Mean time between accidents
     */

    public static final int CAR_ID = 98;
    public static final EqualityPred<Integer> CAR_ID_ACCEPTOR =
            EqualityPred.equalToPositiveInt(CAR_ID);

    /* public static final EqualityPred<Integer> ZERO_SPEED_ACCEPTOR =
            EqualityPred.equalToPositiveInt(0); */
    public static final EqualityPred<Integer> LO_SPEED_ACCEPTOR =
            new EqualityPred<Integer>(Type.MATCH_SOME, ImmutableSet.of(0), ALL_POSITIVE_INTEGERS) {
                @Override
                public boolean test(Integer v) {
                    return v < 20;
                }
            };

    public static final VehicleSpeedPredicate VSP_TRUE = // Accepts every value
            new VehicleSpeedPredicate(true,
                    EqualityPred.truePred(ALL_POSITIVE_INTEGERS),
                    EqualityPred.truePred(ALL_POSITIVE_INTEGERS));
    public static final VehicleSpeedPredicate NV_PRED = // Not car of interest
            new VehicleSpeedPredicate(true, CAR_ID_ACCEPTOR.not(),
                    EqualityPred.truePred(ALL_POSITIVE_INTEGERS));

    public static final VehicleSpeedPredicate VSZ_PRED = // Car of interest, 0 speed
            new VehicleSpeedPredicate(false, CAR_ID_ACCEPTOR, LO_SPEED_ACCEPTOR);
    public static final VehicleSpeedPredicate VSNZ_PRED = // Car of interest, non-zero speed
            new VehicleSpeedPredicate(false, CAR_ID_ACCEPTOR, LO_SPEED_ACCEPTOR.not());

    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> NV0 =
            new Base<>(NV_PRED, v -> 0.0, Double.class);
    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> VSNZ0 =
            new Base<>(VSNZ_PRED, v -> 0.0, Double.class);
    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> VSNZ1 =
            new Base<>(VSNZ_PRED, v -> 1.0, Double.class);
    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> VSZ1 =
            new Base<>(VSZ_PRED, v -> 1.0, Double.class);

    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> VZ_PERIOD_0 =
            new Plus.SplitPlus(Plus.iterPlus(NV0, true), VSNZ0);
    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> VNZ_PERIOD =
            new Plus.SplitPlus(Plus.iterPlus(NV0, true), VSZ1);
    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> VZ_PERIOD_1 =
            new Plus.SplitPlus(Plus.iterPlus(NV0, true), VSNZ1);

    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> T0 =
            Plus.iterPlus(new Base<>(VSNZ_PRED.not(), v -> 0.0, Double.class), true);
    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> T1 =
            Plus.iterPlus(new Base<>(VSZ_PRED.not(), v -> 0.0, Double.class), true);
    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> TAIL =
            new TryElse<>(T0, Plus.splitPlus(T0, VSNZ0, T1));

    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> COUNT_TIME =
            Plus.splitPlus(Plus.iterPlus(VZ_PERIOD_0, true), Plus.iterPlus(VNZ_PERIOD, false), VZ_PERIOD_1);
    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> MTBF =
            Plus.splitPlus(Avg.iter(COUNT_TIME), TAIL);
    public static final QRE<VehicleSpeedPredicate, LinearRoadValue, Double> MEDTBF =
            Plus.splitPlus(Rank.iterRank(COUNT_TIME, 0.5), TAIL);

    static {
        verify(VZ_PERIOD_0.fjAnalyze().isUnambiguous());
        verify(VNZ_PERIOD.fjAnalyze().isUnambiguous());
        verify(VZ_PERIOD_1.fjAnalyze().isUnambiguous());
        verify(COUNT_TIME.fjAnalyze().isUnambiguous());
        verify(MTBF.fjAnalyze().isUnambiguous());
        verify(MEDTBF.fjAnalyze().isUnambiguous());
    }

    /*
     * Benchmark query 2: Average over all lanes(Median lane speed)
     */

    public static QRE<LanePredicate, LinearRoadValue, Multiset>
    allLaneSpeeds(int lane) {
        checkArgument(0 <= lane && lane <= 4);
        LanePredicate lanePred = new LanePredicate(false, EqualityPred.equalToPositiveInt(lane));

        QRE<LanePredicate, LinearRoadValue, Multiset> el =
                new Base<>(lanePred, v -> Multiset.of(((CarLoc)v).speed), Multiset.class);
        QRE<LanePredicate, LinearRoadValue, Multiset> enl =
                new Base<>(lanePred.not(), v -> Multiset.EMPTY, Multiset.class);
        QRE<LanePredicate, LinearRoadValue, Multiset> e = new TryElse<>(el, enl);
        QRE<LanePredicate, LinearRoadValue, Multiset> eu = Union.iterUnion(e, true);
        return new Union.SplitUnion<>(Union.iterUnion(enl, true), new Union.SplitUnion<>(el, eu));
    }

    public static QRE<LanePredicate, LinearRoadValue, Double>
    medianLaneSpeed(int lane) {
        return new Rank<>(allLaneSpeeds(lane), 0.5);
    }

    public static final QRE<LanePredicate, LinearRoadValue, Double> AVG_MDN_LANE_SPEED
            = Avg.of(medianLaneSpeed(0), medianLaneSpeed(1), medianLaneSpeed(2),
                    medianLaneSpeed(3), medianLaneSpeed(4));

}
