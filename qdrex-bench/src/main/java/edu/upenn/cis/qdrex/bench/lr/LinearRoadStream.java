package edu.upenn.cis.qdrex.bench.lr;

import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.collect.AbstractIterator;
import edu.upenn.cis.qdrex.bench.lr.value.LinearRoadValue;
import edu.upenn.cis.qdrex.core.util.collect.Iterators;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

public class LinearRoadStream implements Iterable<LinearRoadValue> {

    private final BufferedReader input;
    private final int tupleCount;

    public LinearRoadStream(String fileName) throws FileNotFoundException {
        this(fileName, Integer.MAX_VALUE);
    }

    public LinearRoadStream(String fileName, int tupleCount) throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream(fileName);
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        this.input = new BufferedReader(inputStreamReader);
        this.tupleCount = tupleCount;
        checkArgument(tupleCount >= 0);
    }

    @Override
    public Iterator<LinearRoadValue> iterator() {
        Iterator<LinearRoadValue> abstractIterator = new AbstractIterator<LinearRoadValue>() {
            @Override
            protected LinearRoadValue computeNext() {
                try {
                    String nextLine = input.readLine();
                    return nextLine != null ? LinearRoadValue.of(nextLine) : endOfData();
                } catch (IOException ex) {
                    throw new RuntimeException(ex);
                }
            }
        };
        return Iterators.keep(abstractIterator, tupleCount);
    }

}
