package edu.upenn.cis.qdrex.bench.lr.value;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import static com.google.common.base.Preconditions.checkArgument;

public final class CarLoc extends LinearRoadValue {

    public static final int L = 2;

    public final int timeStamp;
    public final int carId;
    public final int speed;
    public final int expressway;
    public final int lane;
    public final int direction;
    public final int segment;
    public final int xpos;

    public CarLoc(int timeStamp, int carId, int speed, int expressway, int lane,
            int direction, int segment, int xpos) {
        this.timeStamp = timeStamp;
        this.carId = carId;
        this.speed = speed;
        this.expressway = expressway;
        this.lane = lane;
        this.direction = direction;
        this.segment = segment;
        this.xpos = xpos;

        checkArgument(0 <= timeStamp && timeStamp <= 10799);
        checkArgument(0 <= carId);
        checkArgument(0 <= speed && speed <= 100);
        checkArgument(0 <= expressway && expressway <= L - 1);
        checkArgument(0 <= lane && lane <= 4);
        checkArgument(direction == 0 || direction == 1);
        checkArgument(0 <= segment && segment <= 99);
        checkArgument(0 <= xpos && xpos <= 527999);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof CarLoc)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        CarLoc other = (CarLoc)obj;
        return other.timeStamp == timeStamp && other.carId == carId &&
                other.speed == speed && other.expressway == expressway &&
                other.lane == lane && other.direction == direction &&
                other.segment == segment && other.xpos == xpos;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(timeStamp)
                                    .append(carId)
                                    .append(speed)
                                    .append(expressway)
                                    .append(lane)
                                    .append(direction)
                                    .append(segment)
                                    .append(xpos)
                                    .build();
    }

    @Override
    public String toString() {
        return String.format("(CarLoc timeStamp: %d carId: %d speed: %d "
                + "expressway: %d lane: %d direction: %d segment: %d xpos: %d)",
                timeStamp, carId, speed, expressway, lane, direction, segment,
                xpos);
    }

}
