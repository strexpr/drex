package edu.upenn.cis.qdrex.bench.bank;

import com.google.common.base.VerifyException;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import com.google.common.base.Optional;

public class BankValuePredicate extends Predicate<BankValuePredicate, BankValue> {

    public final boolean acceptDeposit;
    public final boolean acceptEOD;
    public final boolean acceptEOM;

    public BankValuePredicate(boolean acceptDeposit, boolean acceptEOD, boolean acceptEOM) {
        this.acceptDeposit = acceptDeposit;
        this.acceptEOD = acceptEOD;
        this.acceptEOM = acceptEOM;
    }

    public static final BankValuePredicate IS_DEPOSIT = new BankValuePredicate(true, false, false);
    public static final BankValuePredicate IS_EOD = new BankValuePredicate(false, true, false);
    public static final BankValuePredicate IS_EOM = new BankValuePredicate(false, false, true);

    @Override
    public boolean test(BankValue t) {
        switch(t.type) {
            case DEPOSIT:
                return acceptDeposit;
            case END_DAY:
                return acceptEOD;
            case END_MONTH:
                return acceptEOM;
            default:
                throw new VerifyException("Unreachable code!");
        }
    }

    @Override
    public Optional<BankValue> getWitness() {
        if (acceptDeposit) {
            return Optional.of(BankValue.deposit(1.0));
        } else if (acceptEOD) {
            return Optional.of(BankValue.END_DAY);
        } else if (acceptEOM) {
            return Optional.of(BankValue.END_MONTH);
        } else {
            return Optional.absent();
        }
    }

    @Override
    public BankValuePredicate not() {
        return new BankValuePredicate(!acceptDeposit, !acceptEOD, !acceptEOM);
    }

    @Override
    public BankValuePredicate and(BankValuePredicate other) {
        return new BankValuePredicate(acceptDeposit && other.acceptDeposit,
                acceptEOD && other.acceptEOD, acceptEOM && other.acceptEOM);
    }
    
}
