package edu.upenn.cis.qdrex.bench.nexmark.value;

import static com.google.common.base.Preconditions.checkNotNull;
import java.util.Objects;
import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkArgument;
import javax.annotation.Nullable;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class Auction {

    public final int id;
    public final Optional<Integer> reserve;
    public final Optional<String> privacy;
    public final int itemId;

    public final int sellerId;
    public final @Nullable Person seller;

    public final int category;
    public final int quantity;
    public final String type;
    public final int startTime;
    public final long endTime;

    public Auction(int id, Optional<Integer> reserve, Optional<String> privacy,
            int itemId, int sellerId, Person seller, int category, int quantity,
            String type, int startTime, long endTime) {
        this.id = id;
        this.reserve = checkNotNull(reserve);
        this.privacy = checkNotNull(privacy);
        this.itemId = itemId;

        this.sellerId = sellerId;
        this.seller = seller;
        checkArgument(seller == null || seller.id == sellerId);

        this.category = category;
        this.quantity = quantity;
        this.type = checkNotNull(type);
        this.startTime = startTime;
        this.endTime = endTime;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Auction)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        Auction other = (Auction)obj;
        return id == other.id && Objects.equals(reserve, other.reserve) &&
                Objects.equals(privacy, other.privacy) &&
                itemId == other.itemId && sellerId == other.sellerId &&
                category == other.category && quantity == other.quantity &&
                Objects.equals(type, other.type) &&
                startTime == other.startTime && endTime == other.endTime;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id)
                                    .append(reserve)
                                    .append(privacy)
                                    .append(itemId)
                                    .append(sellerId)
                                    .append(category)
                                    .append(quantity)
                                    .append(type)
                                    .append(endTime)
                                    .build();
    }

    @Override
    public String toString() {
        return String.format("(Auction id: %d reserve: %s privacy: %s "
                + "itemId: %d sellerId: %d category: %d quantity: %d type: %s "
                + "startTime: %d endTime: %d)",
                id, reserve, privacy, itemId, sellerId, category, quantity,
                type, startTime, endTime);
    }

}
