package edu.upenn.cis.qdrex.bench.nexmark;

import edu.upenn.cis.qdrex.bench.nexmark.value.NexmarkValue;
import edu.upenn.cis.qdrex.bench.nexmark.value.pred.AuctionPred;
import edu.upenn.cis.qdrex.bench.nexmark.value.pred.BidPred;
import edu.upenn.cis.qdrex.bench.nexmark.value.pred.PersonPred;
import edu.upenn.cis.qdrex.bench.nexmark.value.pred.ValuePred;
import edu.upenn.cis.qdrex.core.qre.Base;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.qre.arith.Avg;
import edu.upenn.cis.qdrex.core.qre.arith.Plus;
import edu.upenn.cis.qdrex.core.qre.arith.Rank;

public class Expressions {

    public static final BidPred BID_PRED_0 = new BidPred(true, false);
    public static final ValuePred VALUE_PRED_0 =
            new ValuePred(AuctionPred.FALSE, BID_PRED_0, PersonPred.FALSE);
    public static final ValuePred VALUE_PRED_N0 = VALUE_PRED_0.not();

    public static final QRE<ValuePred, NexmarkValue, Double> V0 =
            Base.arith(VALUE_PRED_N0, v -> 0.0);
    public static final QRE<ValuePred, NexmarkValue, Double> VB =
            Base.arith(VALUE_PRED_0, v -> (double)v.value.getRight().price);

    public static final QRE<ValuePred, NexmarkValue, Double> V0_STAR =
            Plus.iterPlus(V0, true);
    public static final QRE<ValuePred, NexmarkValue, Double> V0_STAR_B =
            new Plus.SplitPlus<>(V0_STAR, VB);

    public static final QRE<ValuePred, NexmarkValue, Double> V0_STAR_B_AVG =
            Avg.iter(V0_STAR_B);
    public static final QRE<ValuePred, NexmarkValue, Double> V0_STAR_B_MDN =
            Rank.iterRank(V0_STAR_B, 0.5);

}
