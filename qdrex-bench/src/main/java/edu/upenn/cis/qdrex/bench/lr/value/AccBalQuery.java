package edu.upenn.cis.qdrex.bench.lr.value;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import static com.google.common.base.Preconditions.checkArgument;

public final class AccBalQuery extends LinearRoadValue {

    public final int timeStamp;
    public final int carId;
    public final int queryId;

    public AccBalQuery(int timeStamp, int carId, int queryId) {
        this.timeStamp = timeStamp;
        this.carId = carId;
        this.queryId = queryId;

        checkArgument(0 <= timeStamp && timeStamp <= 10799);
        checkArgument(0 <= carId);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AccBalQuery)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        AccBalQuery other = (AccBalQuery)obj;
        return other.timeStamp == timeStamp && other.carId == carId &&
                other.queryId == queryId;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(timeStamp)
                                    .append(carId)
                                    .append(queryId)
                                    .build();
    }

    @Override
    public String toString() {
        return String.format("(AccBalQuery timeStamp: %d carId: %d queryId: %d)",
                timeStamp, carId, queryId);
    }

}
