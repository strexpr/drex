package edu.upenn.cis.qdrex.bench.lr;

import com.google.common.base.Optional;
import static com.google.common.base.Preconditions.checkNotNull;
import edu.upenn.cis.qdrex.bench.lr.value.AccBalQuery;
import edu.upenn.cis.qdrex.bench.lr.value.CarLoc;
import edu.upenn.cis.qdrex.bench.lr.value.LinearRoadValue;
import edu.upenn.cis.qdrex.core.symbol.EqualityPred;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class LanePredicate extends Predicate<LanePredicate, LinearRoadValue> {

    public final boolean acceptNonLoc;
    public final EqualityPred<Integer> lanePred;

    public LanePredicate(boolean acceptNonPos, EqualityPred<Integer> lanePred) {
        this.acceptNonLoc = acceptNonPos;
        this.lanePred = checkNotNull(lanePred);
    }

    @Override
    public boolean test(LinearRoadValue t) {
        if (t instanceof CarLoc) {
            return lanePred.test(((CarLoc)t).lane);
        } else {
            return acceptNonLoc;
        }
    }

    @Override
    public Optional<LinearRoadValue> getWitness() {
        if (acceptNonLoc) {
            return Optional.of(new AccBalQuery(0, 0, 0));
        } else {
            Optional<Integer> witness = lanePred.getWitness();
            if (!witness.isPresent() || witness.get() >= 5) {
                return Optional.absent();
            }
            return Optional.of(new CarLoc(0, 0, 0, 0, witness.get(), 0, 0, 0));
        }
    }

    @Override
    public LanePredicate not() {
        boolean anlPrime = !acceptNonLoc;
        EqualityPred<Integer> lpPrime = lanePred.not();
        return new LanePredicate(anlPrime, lpPrime);
    }

    @Override
    public LanePredicate and(LanePredicate other) {
        boolean anlPrime = acceptNonLoc && other.acceptNonLoc;
        EqualityPred<Integer> lpPrime = lanePred.and(other.lanePred);
        return new LanePredicate(anlPrime, lpPrime);
    }

}
