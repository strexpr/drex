package edu.upenn.cis.qdrex.bench.lr.value;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import static edu.upenn.cis.qdrex.bench.lr.value.CarLoc.L;
import static com.google.common.base.Preconditions.checkArgument;

public final class TravelTimeQuery extends LinearRoadValue {

    public final int timeStamp;
    public final int carId;
    public final int expressway;
    public final int queryId;
    public final int sinit, send;
    public final int dayOfWeek;
    public final int tod;

    public TravelTimeQuery(int timeStamp, int carId, int expressway,
            int queryId, int sinit, int send, int dayOfWeek, int timeOfDay) {
        this.timeStamp = timeStamp;
        this.carId = carId;
        this.expressway = expressway;
        this.queryId = queryId;
        this.sinit = sinit;
        this.send = send;
        this.dayOfWeek = dayOfWeek;
        this.tod = timeOfDay;

        checkArgument(0 <= timeStamp && timeStamp <= 10799);
        checkArgument(0 <= carId);
        checkArgument(0 <= expressway && expressway <= L - 1);
        checkArgument(0 <= sinit && sinit <= 99);
        checkArgument(0 <= send && send <= 99);
        checkArgument(sinit <= send);
        checkArgument(0 <= dayOfWeek && dayOfWeek <= 6);
        checkArgument(0 <= timeOfDay && timeOfDay <= 1439); // 1..1440
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TravelTimeQuery)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        TravelTimeQuery other = (TravelTimeQuery)obj;
        return other.timeStamp == timeStamp && other.carId == carId &&
                other.expressway == expressway && other.queryId == queryId &&
                other.sinit == sinit && other.send == send && other.dayOfWeek == dayOfWeek &&
                other.tod == tod;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(timeStamp)
                                    .append(carId)
                                    .append(expressway)
                                    .append(queryId)
                                    .append(dayOfWeek)
                                    .build();
    }

    @Override
    public String toString() {
        return String.format("(TravelTimeQuery timeStamp: %d carId: %d "
                + "expressway: %d queryId: %d sinit: %d send: %d day: %d "
                + "tod: %d)", timeStamp, carId, expressway, queryId, sinit,
                send, dayOfWeek, tod);
    }

}
