package edu.upenn.cis.qdrex.bench.bank;

import static edu.upenn.cis.qdrex.bench.bank.BankValuePredicate.IS_DEPOSIT;
import static edu.upenn.cis.qdrex.bench.bank.BankValuePredicate.IS_EOD;
import static edu.upenn.cis.qdrex.bench.bank.BankValuePredicate.IS_EOM;
import edu.upenn.cis.qdrex.core.qre.Base;
import edu.upenn.cis.qdrex.core.qre.Epsilon;
import edu.upenn.cis.qdrex.core.qre.QRE;
import edu.upenn.cis.qdrex.core.qre.Stream;
import edu.upenn.cis.qdrex.core.qre.TryElse;
import edu.upenn.cis.qdrex.core.qre.arith.Avg;
import edu.upenn.cis.qdrex.core.qre.arith.Max;
import edu.upenn.cis.qdrex.core.qre.arith.Plus;
import edu.upenn.cis.qdrex.core.qre.arith.Rank;
import edu.upenn.cis.qdrex.core.symbol.TrivialPred;
import edu.upenn.cis.qdrex.core.term.ConstantTerm;

public class BankQueries {

    public static final QRE<BankValuePredicate, BankValue, Double>
            ECHO_DEPOSIT = Base.arith(IS_DEPOSIT, v -> v.amount);
    public static final QRE<BankValuePredicate, BankValue, Double>
            EOD_0 = Base.arith(IS_EOD, v -> 0.0);
    public static final QRE<BankValuePredicate, BankValue, Double>
            SUM_MONTHLY_DEPOSITS = Plus.iterPlus(new TryElse<>(ECHO_DEPOSIT, EOD_0), true);
    public static final QRE<BankValuePredicate, BankValue, Double>
            EOM_0 = Base.arith(IS_EOM, v -> 0.0);
    public static final QRE<BankValuePredicate, BankValue, Double>
            TOTAL_DEPOSIT_MONTH = new Plus.SplitPlus(SUM_MONTHLY_DEPOSITS, EOM_0);

    public static final QRE<BankValuePredicate, BankValue, Double>
            AVG_MONTHLY_DEPOSIT = Avg.iter(TOTAL_DEPOSIT_MONTH);
    public static final QRE<BankValuePredicate, BankValue, Double>
            MEDIAN_MONTHLY_DEPOSIT = Rank.iterRank(TOTAL_DEPOSIT_MONTH, 0.5);

    public static final QRE<BankValuePredicate, BankValue, Double>
            TOTAL_DEPOSIT_MANY_MONTHS = Plus.iterPlus(TOTAL_DEPOSIT_MONTH, false);
    public static final QRE<TrivialPred<Double>, Double, Double>
            MANY_NUMBERS_MEDIAN = Rank.iterRank(Base.arith(new TrivialPred<>(true, 0.0), v -> v), 0.5);
    public static final QRE<TrivialPred<Double>, Double, Double>
            STREAM_RIGHT = new TryElse<>(new Epsilon<>(ConstantTerm.of(0)), MANY_NUMBERS_MEDIAN);
    public static final QRE<BankValuePredicate, BankValue, Double>
            MEDIAN_CLOSING_BALANCE = new Stream<>(TOTAL_DEPOSIT_MANY_MONTHS, STREAM_RIGHT);

    public static final QRE<BankValuePredicate, BankValue, Double>
            SUM_SINGLE_DAY_TRANSACTION_COUNT = Plus.iterPlus(Base.arith(IS_DEPOSIT, v -> 1.0), true);
    public static final QRE<BankValuePredicate, BankValue, Double>
            DAY_TRANS_COUNT = new Plus.SplitPlus<>(SUM_SINGLE_DAY_TRANSACTION_COUNT,
                    Base.arith(IS_EOD, v -> 0.0));
    public static final QRE<BankValuePredicate, BankValue, Double>
            LAST_DAY_TRANS_COUNT = new Plus.SplitPlus(SUM_SINGLE_DAY_TRANSACTION_COUNT,
                    Base.arith(IS_EOM, v -> 0.0));
    public static final QRE<BankValuePredicate, BankValue, Double>
            MONTHLY_MAX_TRANS_COUNT = new Max.SplitMax(Max.iterMax(DAY_TRANS_COUNT, true),
                    LAST_DAY_TRANS_COUNT);
    public static final QRE<BankValuePredicate, BankValue, Double>
            BUSY_DAY = Rank.iterRank(MONTHLY_MAX_TRANS_COUNT, 0.5);

    public static final QRE<BankValuePredicate, BankValue, Double>
            QUERY_1 = MEDIAN_MONTHLY_DEPOSIT;
    public static final QRE<BankValuePredicate, BankValue, Double>
            QUERY_2 = MEDIAN_CLOSING_BALANCE;
    public static final QRE<BankValuePredicate, BankValue, Double>
            QUERY_3 = BUSY_DAY;

}
