package edu.upenn.cis.qdrex.bench.bank;

import static com.google.common.base.Preconditions.checkArgument;
import com.google.common.collect.AbstractIterator;
import edu.upenn.cis.qdrex.bench.bank.BankValue.Type;
import java.util.Iterator;
import java.util.Random;

public class BankStream implements Iterable<BankValue> {

    public static final double DEPOSIT_WT = 60.0;
    public static final double EOD_WT = 30.0;
    public static final double EOM_WT = 1.0;

    public static final double DEPOSIT_PROB = DEPOSIT_WT / (DEPOSIT_WT + EOD_WT + EOM_WT);
    public static final double EOD_PROB = EOD_WT / (DEPOSIT_WT + EOD_WT + EOM_WT);
    public static final double EOM_PROB = EOM_WT / (DEPOSIT_WT + EOD_WT + EOM_WT);

    public final double depositHi;

    public BankStream(double depositHi) {
        this.depositHi = depositHi;
        checkArgument(depositHi > 0);
    }

    @Override
    public AbstractIterator<BankValue> iterator() {
        Random random = new Random(0);
        return new AbstractIterator<BankValue>() {
            @Override
            protected BankValue computeNext() {
                double r = random.nextDouble();

                if (r < DEPOSIT_PROB) {
                    double amount = random.nextDouble() * depositHi;
                    return BankValue.deposit(amount);
                }
                r = r - DEPOSIT_PROB;

                if (r < EOD_PROB) {
                    return BankValue.END_DAY;
                } else {
                    return BankValue.END_MONTH;
                }
            }
        };
    }

    /**
     * @param n number of months (n is required to be non-negative)
     * @param depositHi largest deposit
     * @return transactions for n months
     */
    public static AbstractIterator<BankValue> getMonths(int n, double depositHi) {
        checkArgument(n >= 0);
        Iterator<BankValue> iterator = new BankStream(depositHi).iterator();

        return new AbstractIterator<BankValue>() {
            int i = 0;
            @Override
            protected BankValue computeNext() {
                if (i < n) {
                    BankValue next = iterator.next();
                    if (next.type == Type.END_MONTH) {
                        i++;
                    }
                    return next;
                } else {
                    return endOfData();
                }
            }
        };
    }

}
