package edu.upenn.cis.qdrex.bench.lr.value;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import static edu.upenn.cis.qdrex.bench.lr.value.CarLoc.L;
import static com.google.common.base.Preconditions.checkArgument;

public final class DailyExpenditureQuery extends LinearRoadValue {

    public final int timeStamp;
    public final int carId;
    public final int expressway;
    public final int queryId;
    public final int day;

    public DailyExpenditureQuery(int timeStamp, int carId, int expressway,
            int queryId, int day) {
        this.timeStamp = timeStamp;
        this.carId = carId;
        this.expressway = expressway;
        this.queryId = queryId;
        this.day = day;

        checkArgument(0 <= timeStamp && timeStamp <= 10799);
        checkArgument(0 <= carId);
        checkArgument(0 <= expressway && expressway <= L - 1);
        checkArgument(0 <= day && day <= 69); // 1..69
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof DailyExpenditureQuery)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        DailyExpenditureQuery other = (DailyExpenditureQuery)obj;
        return other.timeStamp == timeStamp && other.carId == carId &&
                other.expressway == expressway && other.queryId == queryId &&
                other.day == day;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(timeStamp)
                                    .append(carId)
                                    .append(expressway)
                                    .append(queryId)
                                    .append(day)
                                    .build();
    }

    @Override
    public String toString() {
        return String.format("(DailyExpenditureQuery timeStamp: %d carId: %d "
                + "expressway: %d queryId: %d day: %d)", timeStamp, carId,
                expressway, queryId, day);
    }

}
