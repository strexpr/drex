package edu.upenn.cis.qdrex.bench.nexmark.value;

import static com.google.common.base.Preconditions.checkArgument;
import javax.annotation.Nullable;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public final class Bid {

    public final int itemId;
    public final @Nullable Auction item;

    public final int time;

    public final int personId;
    public final @Nullable Person person;

    public final int price;

    public Bid(int itemId, Auction item, int time, int personId, Person person, int price) {
        this.itemId = itemId;
        this.item = item;
        checkArgument(item == null || item.id == itemId);

        this.time = time;

        this.personId = personId;
        this.person = person;
        checkArgument(person == null || person.id == personId);

        this.price = price;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Bid)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        Bid other = (Bid)obj;
        return itemId == other.itemId &&
                time == other.time &&
                personId == other.personId &&
                price == other.price;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(itemId)
                                    .append(time)
                                    .append(personId)
                                    .append(price)
                                    .build();
    }

    @Override
    public String toString() {
        return String.format("(Bid itemId: %d time: %d personId: %d price: %d)",
                itemId, time, personId, price);
    }

}
