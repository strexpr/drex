package edu.upenn.cis.qdrex.bench.nexmark.value;

import static com.google.common.base.Preconditions.checkNotNull;
import com.google.common.collect.ImmutableList;
import java.util.Objects;
import com.google.common.base.Optional;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.tuple.ImmutableTriple;

public final class Person {

    public final int id;
    public final String firstName, lastName;
    public final String emailAddress;
    public final Optional<ImmutableTriple<Integer, Integer, Integer>> phoneNumber;
    public final Optional<Address> address;
    public final Optional<String> homepage;
    public final Optional<String> creditCard;
    public final Optional<Profile> profile;

    public Person(int id, String firstName, String lastName, String emailAddress,
            Optional<ImmutableTriple<Integer, Integer, Integer>> phoneNumber,
            Optional<Address> address, Optional<String> homepage,
            Optional<String> creditCard, Optional<Profile> profile) {
        this.id = id;
        this.firstName = checkNotNull(firstName);
        this.lastName = checkNotNull(lastName);
        this.emailAddress = checkNotNull(emailAddress);
        this.phoneNumber = checkNotNull(phoneNumber);
        this.address = checkNotNull(address);
        this.homepage = checkNotNull(homepage);
        this.creditCard = checkNotNull(creditCard);
        this.profile = checkNotNull(profile);
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Person)) {
            return false;
        } else if (obj == this) {
            return true;
        }

        Person other = (Person)obj;
        return id == other.id && Objects.equals(firstName, other.firstName) &&
                Objects.equals(lastName, other.lastName) &&
                Objects.equals(emailAddress, other.emailAddress) &&
                Objects.equals(phoneNumber, other.phoneNumber) &&
                Objects.equals(address, other.address) &&
                Objects.equals(homepage, other.homepage) &&
                Objects.equals(creditCard, other.creditCard) &&
                Objects.equals(profile, other.profile);
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id)
                                    .append(firstName)
                                    .append(lastName)
                                    .append(emailAddress)
                                    .append(phoneNumber)
                                    .append(address)
                                    .append(homepage)
                                    .append(creditCard)
                                    .append(profile)
                                    .build();
    }

    @Override
    public String toString() {
        return String.format("(Person id: %d firstName: %s lastName: %s "
                + "email: %s phone: %s address: %s homepage: %s creditCard: %s "
                + "profile: %s)", id, firstName, lastName, emailAddress,
                phoneNumber, address, homepage, creditCard, profile);
    }

    public static class Address {

        public final String street, city, province, country, zipcode;

        public Address(String street, String city, String province, String country,
                String zipcode) {
            this.street = checkNotNull(street);
            this.city = checkNotNull(city);
            this.province = checkNotNull(province);
            this.country = checkNotNull(country);
            this.zipcode = checkNotNull(zipcode);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Address)) {
                return false;
            } else if (obj == this) {
                return true;
            }

            Address other = (Address)obj;
            return Objects.equals(street, other.street) &&
                    Objects.equals(city, other.city) &&
                    Objects.equals(province, other.province) &&
                    Objects.equals(country, other.country) &&
                    Objects.equals(zipcode, other.zipcode);
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(street)
                                        .append(city)
                                        .append(province)
                                        .append(country)
                                        .append(zipcode)
                                        .build();
        }

        @Override
        public String toString() {
            return String.format("(Address %s %s %s %s %s)", street, city, province,
                    country, zipcode);
        }

    }

    public static class Profile {

        public final Optional<String> education;
        public final Optional<String> gender;
        public final String business;
        public final Optional<Integer> age;
        public final double income;
        public final ImmutableList<Integer> interests;

        public Profile(Optional<String> education, Optional<String> gender,
                String business, Optional<Integer> age, double income,
                ImmutableList<Integer> interests) {
            this.education = checkNotNull(education);
            this.gender = checkNotNull(gender);
            this.business = checkNotNull(business);
            this.age = checkNotNull(age);
            this.income = income;
            this.interests = checkNotNull(interests);
        }

        @Override
        public boolean equals(Object obj) {
            if (!(obj instanceof Profile)) {
                return false;
            } else if (obj == this) {
                return true;
            }

            Profile other = (Profile)obj;
            return Objects.equals(education, other.education) &&
                    Objects.equals(gender, other.gender) &&
                    Objects.equals(business, other.business) &&
                    Objects.equals(age, other.age) &&
                    income == other.income &&
                    Objects.equals(interests, other.interests);
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(education)
                                        .append(gender)
                                        .append(business)
                                        .append(age)
                                        .append(income)
                                        .append(interests)
                                        .build();
        }

        @Override
        public String toString() {
            return String.format("(Profile education: %s gender: %s "
                    + "business: %s age: %s income: %f interests: %s)",
                    education, gender, business, age, income, interests);
        }

    }

}
