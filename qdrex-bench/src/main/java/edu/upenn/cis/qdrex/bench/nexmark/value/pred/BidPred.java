package edu.upenn.cis.qdrex.bench.nexmark.value.pred;

import com.google.common.base.Optional;
import edu.upenn.cis.qdrex.bench.nexmark.value.Bid;
import edu.upenn.cis.qdrex.core.symbol.Predicate;

public class BidPred extends Predicate<BidPred, Bid> {

    public final boolean allow0, allowN0;

    public BidPred(boolean allow0, boolean allowN0) {
        this.allow0 = allow0;
        this.allowN0 = allowN0;
    }

    public static final BidPred TRUE = new BidPred(true, true);
    public static final BidPred FALSE = new BidPred(false, false);

    public boolean test(int itemId) {
        return itemId == 0 ? allow0 : allowN0;
    }

    @Override
    public boolean test(Bid t) {
        return test(t.itemId);
    }

    @Override
    public Optional<Bid> getWitness() {
        if (allow0) {
            return Optional.of(new Bid(0, null, 0, 0, null, 0));
        } else if (allowN0) {
            return Optional.of(new Bid(1, null, 0, 0, null, 0));
        } else {
            return Optional.absent();
        }
    }

    @Override
    public BidPred not() {
        return new BidPred(!allow0, !allowN0);
    }

    @Override
    public BidPred and(BidPred other) {
        return new BidPred(allow0 && other.allow0, allowN0 && other.allowN0);
    }

}
