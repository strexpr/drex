package edu.upenn.cis.qdrex.bench.nexmark.value.pred;

import edu.upenn.cis.qdrex.bench.nexmark.value.Person;
import edu.upenn.cis.qdrex.core.symbol.Predicate;
import com.google.common.base.Optional;

public class PersonPred extends Predicate<PersonPred, Person> {

    public final boolean value;

    public PersonPred(boolean value) {
        this.value = value;
    }

    public static final PersonPred TRUE = new PersonPred(true);
    public static final PersonPred FALSE = new PersonPred(false);

    @Override
    public boolean test(Person t) {
        return value;
    }

    @Override
    public Optional<Person> getWitness() {
        if (value) {
            return Optional.of(new Person(0, "fnu", "lnu", "fnu@lnu.com",
                    Optional.absent(), Optional.absent(), Optional.absent(),
                    Optional.absent(), Optional.absent()));
        } else {
            return Optional.absent();
        }
    }

    @Override
    public PersonPred not() {
        return new PersonPred(!value);
    }

    @Override
    public PersonPred and(PersonPred other) {
        return new PersonPred(value && other.value);
    }

}
