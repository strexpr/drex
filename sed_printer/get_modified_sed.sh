#!/bin/bash

DIR=./sed_clone
ORIG_DIR=./sed_orig

function clone() {
	if ! [ -d "$1" ]; then
		git clone git://git.savannah.gnu.org/sed.git "$1"
	fi
	cd "$1" && \
		git reset --hard 31c84cbcfd2516e278a2a75523c7d5ad78f7bc57 && \
		./bootstrap --skip-po && \
		echo "Cloned sed and bootstrapped into $1"
}

function build() {
	clone "$DIR"
	pwd && \
	patch -p1 < ../sed_patch.patch && \
	patch -p2 < ../regex_lib.patch && \
	./configure --with-included-regex && \
	make && \
	readlink -f ./sed/sed > ../../drex-sa/src/main/resources/msed
        readlink -f ./sed/sed > ../../drex-sa/src/test/resources/msed
}

function make_patch() {
	clone "$ORIG_DIR"
	cd "../$DIR"
	git diff -- sed > ../sed_patch.patch
	cd ".."
	diff -pu3 "$ORIG_DIR/lib" "$DIR/lib" | grep -v 'Only in ' > regex_lib.patch
}

if [ "x$1" == "xbuild" ]; then
	build
elif [ "x$1" == "xmake_patch" ]; then
	make_patch
else
	echo "Usage: $0 build"
	echo "Usage: $0 make_patch"
fi
