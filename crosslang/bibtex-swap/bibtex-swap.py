#!/usr/bin/env python

"""
DReX POPL 2015 Benchmark: bibtex-swap
The intent is that the implementation is ``natural'' and close to the
first attempt of a typical developer.
"""

import re
from sys import stdin, stdout

sigma = stdin.read()
entryRE = "@\w+\s*\{\s*\}\s*";
# entryRE = "@\w+\s*\{\s*(\w+\s*=\s*\{[^\{\}]*\},?\s*)*\}\s*";
entries = re.findall(entryRE, sigma)
entries.reverse()
tau = ";".join(entries) + ";"
stdout.write(tau)

