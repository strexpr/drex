#!/usr/bin/env python

"""
DReX POPL 2015 Benchmark: prefix-decode
The intent is that the implementation is ``natural'' and close to the
first attempt of a typical developer.
"""

import re
from sys import stdin, stdout

sigma = stdin.read()
if not re.match("^[0-1]*$", sigma):
    raise ValueError("Ill-formed input")

tau = ""
while len(sigma) > 0:
    if sigma.startswith("0"):
        tau = tau + "a"
        sigma = sigma[1:]
    elif sigma.startswith("10"):
        tau = tau + "b"
        sigma = sigma[2:]
    elif sigma.startswith("110"):
        tau = tau + "c"
        sigma = sigma[3:]
    elif sigma.startswith("111"):
        tau = tau + "d"
        sigma = sigma[3:]

stdout.write(tau)
