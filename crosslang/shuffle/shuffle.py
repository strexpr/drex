#!/usr/bin/env python

"""
DReX POPL 2015 Benchmark: shuffle
The intent is that the implementation is ``natural'' and close to the
first attempt of a typical developer.
"""

import re
from sys import stdin, stdout

sigma = stdin.read()
if not re.match(".*\;.*\;$", sigma):
    raise ValueError("Ill-formed input")
    
entries = sigma.split(";")

list1 = list(entries)
list1 = list1[1:]

list2 = list(entries)
list2 = list2[0:(len(list2) - 2)]

merge = zip(list1, list2)
merge = map(lambda x: x[0] + ";" + x[1], merge)
tau = reduce(lambda x, y: x + ";" + y, merge) + ";"
stdout.write(tau)
