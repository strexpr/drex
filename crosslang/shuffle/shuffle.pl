#!/usr/bin/env perl
use strict;
use warnings;

# shuffle: Natural implementation

sub zip {
    # This function has been copied from http://stackoverflow.com/a/71895
    my $p = @_ / 2;
    return @_[ map { $_, $_ + $p } 0 .. $p - 1 ];
}

my $in = do { local $/; <> };
unless ($in =~ /\;.*\;$/s) {
    die "Ill-formed input";
}

my @entries = split(";", $in);

my @list1 = @entries;
shift @list1;

my @list2 = @entries;
pop @list2;

my @merge = zip @list1, @list2;
my $out = join(";", @merge) . ";";
print $out;
