#!/usr/bin/env perl
use strict;
use warnings;

# get_tags: Natural implementation

my $in = do { local $/; <> };
$in =~ s/^[^<]*//;
$in =~ s/[^>]*$//;
$in =~ s/>[^<>]*</></g;
print $in;
