#!/usr/bin/env python

"""
DReX POPL 2015 Benchmark: get-tags
The intent is that the implementation is ``natural'' and close to the
first attempt of a typical developer.
"""

import re
from sys import stdin, stdout

sigma = stdin.read()
sigma = re.sub("^[^<]*", "", sigma)
sigma = re.sub("[^>]*$", "", sigma)
sigma = re.sub(">[^<>]*<", "><", sigma)
stdout.write(sigma)
