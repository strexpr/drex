//Insert quotes
sed -r 's/[a-zA-Z]+/"&"/g'

//Delete comments
sed -r '/^\/\/a/ d'

//Reverse a string (not exactly the same)
sed '/\n/!G;s/\(.\)\(.*\n\)/&\2\1/;//D;s/.//'
