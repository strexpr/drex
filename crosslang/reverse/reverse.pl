#!/usr/bin/env perl
use strict;
use warnings;

# reverse: Natural implementation

my $in = do { local $/; <> };
if ($in =~ /\;$/) {
    my @entries = split(";", $in);
    my $out = join(";", reverse @entries) . ";";
    print $out;
} else {
    die "Ill-formed input";
}
