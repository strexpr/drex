#!/usr/bin/env python

"""
DReX POPL 2015 Benchmark: reverse
The intent is that the implementation is ``natural'' and close to the
first attempt of a typical developer.
"""

import re
from sys import stdin, stdout

sigma = stdin.read()
if not sigma.endswith(";"):
    raise ValueError("Ill-formed input")
    
entries = sigma.split(";")
entries.pop()
entries.reverse()
tau = ";".join(entries) + ";"
stdout.write(tau)
