#!/usr/bin/env perl
use strict;
use warnings;

# insert_quotes: Natural implementation

while (<>) {
    s/[a-zA-Z]+/\"$&\"/g;
    print;
}
