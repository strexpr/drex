#!/usr/bin/env python

"""
DReX POPL 2015 Benchmark: insert-quotes
The intent is that the implementation is ``natural'' and close to the
first attempt of a typical developer.
"""

import re
from sys import stdin, stdout

for line in stdin:
    outline = re.sub("[a-zA-Z]+", "\"\g<0>\"", line)
    stdout.write(outline)
