#!/usr/bin/env python

"""
DReX POPL 2015 Benchmark: delete-comm.
The intent is that the implementation is ``natural'' and close to the
first attempt of a typical developer.
"""

from sys import stdin, stdout

for line in stdin:
    if not line.startswith("//"):
        stdout.write(line)
