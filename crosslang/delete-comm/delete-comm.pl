#!/usr/bin/env perl
use strict;
use warnings;

# delete-comm: Natural implementation

while (<>) {
    unless (/^\/\//) {
        # The condition /^\/\// matches any line beginning with a C++-style comment
        # Testing the condition /\/\// instead matches any line containing a C++-style comment
        print;
    }
}
